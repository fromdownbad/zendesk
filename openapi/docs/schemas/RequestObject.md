#### Request Comments
Comments represent the public conversation between requesters, collaborators and agents on a request.

Request comments have the following properties:

| Name            | Type    | Read-only | Comment
| --------------- | ------- | --------- | -------
| id              | integer | yes       | Automatically assigned when the comment is created
| type            | string  | yes       | "Comment" or "VoiceComment"
| request_id      | integer | yes       | The id of the request
| body            | string  | no        | The actual comment made by the author
| html_body       | string  | yes       | The actual comment made by the author formatted as HTML
| plain_body      | string  | yes       | The comment formatted as plain text
| public          | boolean | yes       | If true, the comment is public
| author_id       | integer | yes       | The id of the author
| attachments     | array   | yes       | Read-only list of attachments to the comment. See [Attaching files](https://develop.zendesk.com/hc/en-us/articles/360059146153#attaching-files)
| uploads         | array   | no*       | \*On create only. List of tokens received after [uploading files](https://developer.zendesk.com/rest_api/docs/support/attachments#upload-files) to attach
| created_at      | date    | yes       | When this comment was created

#### Request Comment Example
```js
{
  "id": 1274,
  "type": "Comment",
  "body": "Thanks for your help!",
  "html_body": "<p>Thanks for your help!</p>",
  "author_id": 1,
  "attachments": [
    {
      "id":           498483,
      "name":         "crash.log",
      "content_url":  "https://company.zendesk.com/attachments/crash.log",
      "content_type": "text/plain",
      "size":         2532,
      "thumbnails":   []
    }
  ],
  "created_at": "2009-07-20T22:55:29Z"
}
```
