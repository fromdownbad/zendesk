You can access the following data describing the settings of an account. You can update the settings marked as not read-only.

#### Branding

| Name                  | Type   | Read-only | Comment                                                                     |
| --------------------- | ------ | --------- | --------------------------------------------------------------------------- |
| header_color          | string | no        | HEX of the header color                                                     |
| page_background_color | string | no        | HEX of the page background color                                            |
| tab_background_color  | string | no        | HEX of tab background color                                                 |
| text_color            | string | no        | HEX of the text color, usually matched to contrast well with `header_color` |
| header_logo_url       | string | yes       | The URL for the custom header logo                                          |
| favicon_url           | string | yes       | The URL for the custom favicon                                              |

#### Apps

| Name           | Type    | Read-only | Comment                             |
| -------------- | ------- | --------- | ----------------------------------- |
| use            | boolean | yes       | The account can use apps            |
| create_private | boolean | yes       | The account can create private apps |

#### Tickets

| Name                             | Type    | Read-only | Comment                                                                         |
| -------------------------------- | ------- | --------- | ------------------------------------------------------------------------------- |
| comments_public_by_default       | boolean | no        | Comments from agents are public by default                                      |
| is_first_comment_private_enabled | boolean | no        | Allow first comment on tickets to be private                                    |
| list_newest_comments_first       | boolean | no        | When viewing a ticket, show the newest comments and events first                |
| collaboration                    | boolean | yes       | CCs may be added to a ticket                                                    |
| private_attachments              | boolean | no        | Users must sign in to access attachments                                        |
| email_attachments                | boolean | no        | Attachments should be sent as real attachments when under the size limit        |
| agent_collision                  | boolean | yes       | Clients should provide an indicator when a ticket is being viewed by ther agent |
| list_empty_views                 | boolean | no        | Clients should display views with no matching tickets in menus                  |
| maximum_personal_views_to_list   | number  | yes       | Maximum number of personal views clients should display in menus                |
| tagging                          | boolean | no        | Tickets may be tagged                                                           |
| markdown_ticket_comments         | boolean | no        | Whether agent comments should be processed with Markdown                        |
| emoji_autocompletion             | boolean | no        | Whether agent comments should allow for Emoji rendering                         |
| agent_ticket_deletion            | boolean | no        | Whether agents can delete tickets                                               |

#### Agents

| Name            | Type    | Read-only | Comment                                |
| --------------- | ------- | --------- | -------------------------------------- |
| agent_workspace | boolean | no        | Toggles the Agent Workspace experience |

#### Chat

| Name             | Type    | Read-only | Comment                                                                         |
| ---------------- | ------- | --------- | ------------------------------------------------------------------------------- |
| enabled          | boolean | yes       | Chat is enabled                                                                 |
| maximum_requests | number  | yes       | The maximum number of chat requests an agent may handle at one time             |
| welcome_message  | string  | yes       | The message automatically sent to end-users when they begin chatting with agent |

#### Twitter

| Name        | Type   | Read-only | Comment                                        |
| ----------- | ------ | --------- | ---------------------------------------------- |
| shorten_url | string | yes       | Possible values: 'always', 'optional', 'never' |

#### G Suite

| Name                  | Type    | Read-only | Comment                                |
| --------------------- | ------- | --------- | -------------------------------------- |
| has_google_apps       | boolean | yes       |
| has_google_apps_admin | boolean | no        | Account has at least one G Suite admin |

#### Voice

| Name                               | Type    | Read-only | Comment          |
| ---------------------------------- | ------- | --------- | ---------------- |
| enabled                            | boolean | yes       | Voice is enabled |
| maintenance                        | boolean | yes       |
| logging                            | boolean | yes       |
| outbound_enabled                   | boolean | yes       |
| agent_confirmation_when_forwarding | boolean | yes       |
| agent_wrap_up_after_calls          | boolean | yes       |
| maximum_queue_size                 | number  | yes       |
| maximum_queue_wait_time            | number  | yes       |
| only_during_business_hours         | boolean | yes       |
| recordings_public                  | boolean | yes       |
| uk_mobile_forwarding               | boolean | yes       |

#### Users

| Name                             | Type    | Read-only | Comment                                                     |
| -------------------------------- | ------- | --------- | ----------------------------------------------------------- |
| tagging                          | boolean | no        | Users may be tagged                                         |
| time_zone_selection              | boolean | yes       | Whether user can view time zone for profile                 |
| language_selection               | boolean | yes       | Whether to display language drop down for a user            |
| agent_created_welcome_emails     | boolean | no        | Whether a user created by an agent receives a welcome email |
| end_user_phone_number_validation | boolean | no        | Whether a user's phone number is validated                  |
| have_gravatars_enabled           | boolean | no        | Whether user gravatars are displayed in the UI              |

#### GoodData Advanced Analytics

| Name    | Type    | Read-only | Comment                                |
| ------- | ------- | --------- | -------------------------------------- |
| enabled | boolean | yes       | GoodData Advanced Analytics is enabled |

#### Brands

| Name                         | Type    | Read-only | Comment                                                    |
| ---------------------------- | ------- | --------- | ---------------------------------------------------------- |
| default_brand_id             | number  | no        | The id of the brand that is assigned to tickets by default |
| require_brand_on_new_tickets | boolean | no        | Require agents to select a brand before saving tickets     |

#### Statistics

| Name   | Type    | Read-only | Comment                               |
| ------ | ------- | --------- | ------------------------------------- |
| forum  | boolean | yes       | Allow users to view forum statistics  |
| search | boolean | yes       | Allow users to view search statistics |

#### Billing

| Name    | Type   | Read-only | Comment                                             |
| ------- | ------ | --------- | --------------------------------------------------- |
| backend | string | yes       | Backend Billing system either 'internal' or 'zuora' |

#### Active Features

| Name                          | Type    | Read-only | Comment                                                  |
| ----------------------------- | ------- | --------- | -------------------------------------------------------- |
| on_hold_status                | boolean | yes       | Account can use status hold                              |
| user_tagging                  | boolean | no        | Enable user tags                                         |
| ticket_tagging                | boolean | no        | Allow tagging tickets                                    |
| topic_suggestion              | boolean | yes       | Allow topic suggestions in tickets                       |
| voice                         | boolean | yes       | Voice support                                            |
| facebook_login                | boolean | yes       |
| google_login                  | boolean | yes       |
| twitter_login                 | boolean | yes       |
| forum_analytics               | boolean | yes       | Forum and search analytics                               |
| business_hours                | boolean | no        |
| agent_forwarding              | boolean | yes       |
| chat                          | boolean | yes       |
| chat_about_my_ticket          | boolean | yes       |
| customer_satisfaction         | boolean | no        |
| satisfaction_prediction       | boolean | no        |
| csat_reason_code              | boolean | yes       |
| markdown                      | boolean | yes       | Markdown in ticket comments                              |
| bcc_archiving                 | boolean | yes       | Account has a bcc_archive_address set                    |
| allow_ccs                     | boolean | yes       | Allow CCs on tickets                                     |
| advanced_analytics            | boolean | yes       |
| insights                      | boolean | yes       |
| explore                       | boolean | yes       | Has account plan setting 'explore'                       |
| good_data_and_explore         | boolean | yes       | Has account plan setting 'good_data_and_explore'         |
| explore_on_support_pro_plan   | boolean | yes       | Allowed to show explore role controls                    |
| sandbox                       | boolean | yes       | Account has a sandbox                                    |
| suspended_ticket_notification | boolean | yes       |
| twitter                       | boolean | yes       | Account monitors at least one Twitter handle             |
| facebook                      | boolean | yes       | Account is actively linked to at least one Facebook page |
| feedback_tabs                 | boolean | yes       | Feedback tab has been configured before                  |
| dynamic_contents              | boolean | yes       | Account has at least one dynamic content                 |
| light_agents                  | boolean | yes       | Account has at least one light agent                     |
| is_abusive                    | boolean | yes       | Account exceeded trial limits                            |
| rich_content_in_email         | boolean | yes       | Account supports incoming HTML email                     |
| fallback_composer             | boolean | yes       | Fallback composer for Asian languages                    |

#### API

| Name                   | Type    | Read-only | Comment                                                 |
| ---------------------- | ------- | --------- | ------------------------------------------------------- |
| accepted_api_agreement | boolean | no        | Account has accepted the API agreement                  |
| api_password_access    | boolean | no        | Allow the account to use the API with username/password |
| api_token_access       | boolean | no        | Allow the account to use the API with API tokens        |

#### Ticket Form

| Name                      | Type   | Read-only | Comment |
| ------------------------- | ------ | --------- | ------- |
| ticket_forms_instructions | string | no        |

#### Lotus

| Name         | Type    | Read-only | Comment                                                                    |
| ------------ | ------- | --------- | -------------------------------------------------------------------------- |
| prefer_lotus | boolean | yes       | Prefers the current version of Zendesk Support rather than Zendesk Classic |

#### Ticket Sharing Partners

| Name              | Type   | Read-only | Comment                                                                |
| ----------------- | ------ | --------- | ---------------------------------------------------------------------- |
| support_addresses | object | yes       | Array of support addresses for all accepted sharing agreement accounts |

#### Rules

| Name                       | Type    | Read-only | Comment                                                                       |
| -------------------------- | ------- | --------- | ----------------------------------------------------------------------------- |
| macro_most_used            | boolean | no        | Display the most-used macros in the `Apply macro` list. Defaults to true      |
| macro_order                | string  | no        | Default macro display order. Possible values are 'alphabetical' or 'position' |
| skill_based_filtered_views | object  | no        | Array of view ids                                                             |

#### Limits

| Name            | Type   | Read-only | Comment                                            |
| --------------- | ------ | --------- | -------------------------------------------------- |
| attachment_size | number | yes       | The maximum ticket attachment file size (in bytes) |

#### Metrics

| Name         | Type   | Read-only | Comment                                                              |
| ------------ | ------ | --------- | -------------------------------------------------------------------- |
| account_size | string | yes       | An account size category computed from the number of billable agents |

#### Localization

| Name       | Type  | Read-only | Comment                                                                                   |
| ---------- | ----- | --------- | ----------------------------------------------------------------------------------------- |
| locale_ids | array | no        | Array of locale IDs enabled for the account. See [Locales](./locales) for possible values |
