Note that the name & count object described here is only returned in the List Tags endpoint. 
In all other GET, POST, PUT, and DELETE operations, tags are represented by an array of strings.
