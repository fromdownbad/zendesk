You can also include a `comment_count` property in the JSON objects returned by GET requests by sideloading it. Example:

`GET /api/v2/tickets.json?include=comment_count`
