#### Email Address

You can specify a user's primary email address when you create the user.
See [Specifying email and verified attributes]
(./users#specifying-email-and-verified-attributes).

To update a user's primary email address, use the
[Make Identity Primary](./user_identities#make-identity-primary)
endpoint.

#### Time Zone

A `time_zone` name consists of a string such as "Eastern Time (US & Canada)".
For a list of valid names, see the **Time zone** list menu on the Localization page
in the Support admin interface (**Admin** > **Account** > **Localization**).
For example, if the menu lists "(GMT+02:00) Berlin", then use "Berlin" as the
`time_zone` name.

Request body in User API:

```javascript
{
 "user": {
   "id":   35436,
   "name": "Johnny Agent",
   "time_zone": "Berlin",
   ...
 }
}
```

#### User Fields

You can use the `user_fields` object to set the value of one or more custom fields in the user's profile.
Specify [field keys](./user_fields#json-format) as the properties to set. Example:

```javascript
 "user_fields": {
   "membership_level": "silver",
   "membership_expires": "2019-07-23T00:00:00Z"
 }
```

For more information, see [User Fields](./user_fields) and [Adding custom fields to users](https://support.zendesk.com/hc/en-us/articles/203662066).

#### Phone Number

The phone number should comply with the E.164 international [telephone numbering plan](https://en.wikipedia.org/wiki/E.164). Example `+15551234567`.
E164 numbers are international numbers with a country dial prefix, usually an area code and a subscriber number.
A valid E.164 phone number must include a [country calling code](https://en.wikipedia.org/wiki/List_of_country_calling_codes).

A phone number can be one of the following types:

* A direct line linked to a single user, which is indicated by a `shared_phone_number` attribute of false. A direct line can be used as a [user identity](./user_identities)
* A shared number linked to multiple users, indicated by a `shared_phone_number` attribute of true. A shared number can not be used as a [user identity](./user_identities)

See [Understanding how phone numbers are linked to end-user profiles](https://support.zendesk.com/hc/en-us/articles/230553307#topic_hz3_5nm_sx) in the Support Help Center.

#### JSON Format for End Users


When an end user makes the request, the user is returned with the following attributes:

| Name                  | Type                           | Read-only | Mandatory | Comment
| --------------------- | ------------------------------ | --------- | --------- | -------
| id                    | integer                        | yes       | no        | Automatically assigned when creating users
| email                 | string                         | no        | no        | The primary email address of this user
| name                  | string                         | no        | yes       | The name of the user
| created_at            | date                           | yes       | no        | The time the user was created
| locale                | string                         | yes       | no        | The locale for this user
| locale_id             | integer                        | no        | no        | The language identifier for this user
| organization_id       | integer                        | no        | no        | The id of the user's organization. If the user has more than one [organization memberships](./organization_memberships), the id of the user's default organization
| phone                 | string                         | no        | no        | The primary phone number of this user. See [Phone Number](./users#phone-number) in the Users API
| shared_phone_number   | boolean                        | yes       | no        | Whether the `phone` number is shared or not. See [Phone Number](./users#phone-number) in the Users API
| photo                 | [Attachment](attachments.html) | no        | no        | The user's profile picture represented as an [Attachment](./attachments) object
| role                  | string                         | no        | no        | The role of the user. Possible values: "end-user", "agent", "admin"
| time_zone             | string                         | no        | no        | The time-zone of this user
| updated_at            | date                           | yes       | no        | The time of the last update of the user
| url                   | string                         | yes       | no        | The API url of this user
| verified              | boolean                        | no        | no        | Any of the user's identities is verified. See [User Identities](./user_identities)

Responses will vary depending on the role of the client making the request. The following example is a response for an end-user role.

#### Example

```json
{
  "id":                    35436,
  "url":                   "https://company.zendesk.com/api/v2/end_users/35436.json",
  "name":                  "Johnny End User",
  "created_at":            "2009-07-20T22:55:29Z",
  "updated_at":            "2011-05-05T10:38:52Z",
  "time_zone":             "Copenhagen",
  "email":                 "johnny@example.com",
  "phone":                 "+15551234567",
  "locale":                "en-US",
  "locale_id":             1,
  "organization_id":       57542,
  "role":                  "end-user",
  "verified":              true,
  "photo": {
    "id":           928374,
    "name":         "my_funny_profile_pic.png",
    "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic.png",
    "content_type": "image/png",
    "size":         166144,
    "thumbnails": [
      {
      "id":           928375,
      "name":         "my_funny_profile_pic_thumb.png",
      "content_url":  "https://company.zendesk.com/photos/my_funny_profile_pic_thumb.png",
      "content_type": "image/png",
      "size":         58298
      }
    ]
  }
}
```

This example is a response for an admin or agent request.
