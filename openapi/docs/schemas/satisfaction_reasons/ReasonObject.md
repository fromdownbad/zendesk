#### Reason codes

The follow-up question has the following default reasons the user
can select for giving a negative rating:

| Code  | Reason
| ----- | ------
| 0     | No reason provided (the user didn't select a reason from the list menu)
| 5     | The issue took too long to resolve
| 6     | The issue was not resolved
| 7     | The agent's knowledge is unsatisfactory
| 8     | The agent's attitude is unsatisfactory
| 1000  | Some other reason

An admin in Zendesk Support can create custom reasons. Any custom reason
is assigned a code of 1000 or higher when the reason is created. See
[Customizing and localizing satisfaction reasons](https://support.zendesk.com/hc/en-us/articles/223152967#topic_u2m_c4b_rw) in the Support Help Center.