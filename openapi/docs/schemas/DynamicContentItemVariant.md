A few items are worth noting:

* `locale_id` - Must be an active locale in the account. To get a list, see [/api/v2/locales](locales#list-locales)

* `default` - Used as the fallback if Zendesk Support can't find an appropriate variant to match the locale of the user the content is being displayed for

* `outdated`- Indicates the default variant for this item has been updated, but the other variants were not changed. The content may be out of date

* `active` - If false, Zendesk Support will not use the variant even if the user's locale matches the variant
