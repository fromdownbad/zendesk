| Name       | Type                  | Description
| ---------- | --------------------- | -----------
| url        | string                | The API url of this revision
| id         | integer               | ID of the revision
| author_id  | integer               | ID of the user who made the revision
| created_at | date                  | Time the revision was created
| snapshot   | object                | State of the trigger at time of revision. See [Snapshot](#snapshot)
| diff       | object                | Changes introduced in this revision. See [Rule Diff](#rule-diff)
