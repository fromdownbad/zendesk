You can also include the brand for each Support address in the JSON objects returned by GET requests by sideloading it. Example: `GET /api/v2/recipient_addresses.json?include=brands`
