#### Results

The "results" array in a response lists the resources that were successfully and unsuccessfully updated or created after processing.

The results differ depending on the type of job.

#### Bulk create

If the job was to bulk create resources, each result specifies the following:

- the id of the new resource (`"id": 245`)
- the index number of the result (`"index": 1`)

#### Example

```json
{
  "job_status": {
    "id": "dd9321f29967688b27bc9499ebb4ae8d",
    "url": "https://example.zendesk.com/api/v2/job_statuses/dd9321f299676c9499ebb4ae8d.json",
    "total": 2,
    "progress": 2,
    "status": "completed",
    "message": "Completed at 2018-03-08 06:07:49 +0000",
    "results": [
      {
        "id": 244,
        "index": 0
      },
      {
        "id": 245,
        "index": 1
      }
    ]
  }
}
```

#### Bulk update

If the job was to bulk update resources, each result specifies the following:

- the id of the resource the job attempted to update (`"id": 255`)
- the action the job attempted (`"action": "update"`)
- whether the action was successful or not (`"success": true`)
- the status (`"status": "Updated"`)
