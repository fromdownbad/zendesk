### Execution
A view's `execution` object is a read-only object that describes how to display a collection of tickets in the view.

| Name                    | Type    | Comment
| ----------------------- | ------- | -------
| group_by, sort_by       | string  | An item from the [View columns](#view-columns) table
| group_order, sort_order | string  | Either "asc" or "desc"
| columns                 | array   | The ticket fields to display. Custom fields have an id, title, type, and url referencing the [ticket field](ticket_fields.html)
| group                   | object  | When present, the structure indicating how the tickets are grouped
| sort                    | object  | The column structure of the field used for sorting

#### Example
```js
{
    "execution": {
      "columns": [
        { "id": "status",  "title": "Status" },
        { "id": "updated", "title": "Updated" },
        {
          "id": 5, "title": "Account", "type": "text",
          "url": "https://example.zendesk.com/api/v2/ticket_fields/5.json"
        },
        ...
      ]
      "group": { "id": "status", "title": "Status", "order": "desc" },
      "sort": { "id": "updated", "title": "Updated", "order": "desc" }
    }
}
```
