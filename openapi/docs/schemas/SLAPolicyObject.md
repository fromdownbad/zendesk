#### Filter

A filter checks the value of ticket fields and selects the ticket if the conditions are met. Filter is represented as a JSON object with two arrays of one or more conditions.

**Example**

```js
{
   "filter": {
     "all": [
       { "field": "type", "operator": "is", "value": "incident" },
       { "field": "via_id", "operator": "is", "value": 4 }
     ],
     "any": [
     ]
   }
}
```

The first array lists all the conditions that must be met. The second array lists any condition that must be met.

| Name     | Type  | Description
| -------- | ----- | -----------
| `all`    | array | Logical AND. Tickets must fulfill all of the conditions to be considered matching
| `any`    | array | Logical OR. Tickets may satisfy any of the conditions to be considered matching

Each condition in an array has the following properties:

| Name            | Type                       | Description
| --------------- | ---------------------------| -------------------
| field           | string                     | The name of a ticket field
| operator        | string                     | A comparison operator
| value           | string                     | The value of a ticket field

**Example**

```js
{ "field": "type", "operator": "is", "value": "incident" }
```

See [Conditions reference](https://develop.zendesk.com/hc/en-us/articles/360056760914) for the list of fields, allowed operators, and values of the conditions.

#### Policy Metric

An object that describes the metric targets for each value of the priority field.

Policy Metrics are represented as simple flat JSON objects which have the following keys:

| Name            | Type                       | Comment
| --------------- | ---------------------------| -------------------
| priority        | string                     | Priority that a ticket must match
| metric          | string                     | The definition of the time that is being measured. See [Metrics](#metrics)
| target          | integer                    | The time within which the end-state for a metric should be met
| business_hours  | boolean                    | Whether the metric targets are being measured in business hours or calendar hours

**Example**
```js
{
  "priority": "low",
  "metric": "first_reply_time",
  "target": 60,
  "business_hours": false
}
```

#### Metrics

| Metric                   | Value                  |
| ------------------------ | ---------------------- |
| Agent Work Time          | agent_work_time        |
| First Reply Time         | first_reply_time       |
| Next Reply Time          | next_reply_time        |
| Pausable Update Time     | pausable_update_time   |
| Periodic Update Time     | periodic_update_time   |
| Requester Wait Time      | requester_wait_time    |
