#### Author id

If you set the `author_id`, the user with the id is shown as the author of the comment. However, this user is not considered the updater of the ticket. The authenticated user making the API request is the updater. This has implications for business rules and views, such as the requester updated attribute and current user conditions.

#### Comment flags

Each comment can be flagged by Zendesk for several reasons. If the comment is flagged, the `metadata` property will have a `flags` array with any of the following values:

| Value | Reason for flag
| ------| ---------------
| 0     | Zendesk is unsure the comment should be trusted
| 2     | The comment author was not part of the conversation. [Learn more](https://support.zendesk.com/hc/en-us/articles/203661606#topic_d32_mzc_3r)
| 3     | The comment author was not signed in when the comment was submitted. [Learn more](https://support.zendesk.com/hc/en-us/articles/203663756#topic_nr4_4s5_cq)
| 4     | The comment was automatically generated. Automatic email notifications have been suppressed
| 5     | The attached file was rejected because it's too big
| 11    | This comment was submitted by the user on behalf of the author. See [Requesters and submitters](./tickets#requesters-and-submitters)

A `flags_options` object will also be included with additional information about the flags.

The `flags` and `flags_options` properties are omitted if there are no flags.

**Example**

```javascript
metadata: {
  system: { ... },
  flags: [2,5],
  "flags_options": {
    "2": {
      "trusted": false
    },
    "5": {
      "message": {
        "file": "printer_manual.pdf",
        "account_limit": "20"
      },
      "trusted": false
    }
  },
  "trusted": false,
  "suspension_type_id": null
}
```
