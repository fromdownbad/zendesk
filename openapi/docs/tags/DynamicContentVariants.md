Dynamic content item variants are locale-specific versions of a dynamic content item. To learn more, see [Dynamic Content Items](./dynamic_content).
