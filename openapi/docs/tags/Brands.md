Brands are your customer-facing identities. They might represent multiple products or services, or they might literally be multiple brands owned and represented by your company.

The default brand is the one that tickets get assigned to if the ticket is generated from a non-branded channel. You can update the default brand using the [Update Account Settings](https://developer.zendesk.com/rest_api/docs/support/account_settings.html#update-account-settings) endpoint.
