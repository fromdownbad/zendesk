You can use this API to get system and custom ticket fields. For a list of system fields, see [About ticket fields](https://support.zendesk.com/hc/en-us/articles/203661506) in Help Center.

You can also use the API to create custom ticket fields. See [About custom field types](https://support.zendesk.com/hc/en-us/articles/203661866) in the Zendesk Help Center. New custom ticket fields become available in the Tickets API. See [Setting custom field values](./tickets#setting-custom-field-values) in Tickets.

You can make ticket fields visible on the request form in Help Center for end users. To make a ticket field visible to end users, make it both visible and editable in Help Center. Example:

```json
{
  "visible_in_portal": true,
  "editable_in_portal": true
}
```
