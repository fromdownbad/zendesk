This API is for tickets attachments. For attachments in Help Center articles, see [Article Attachments](/rest_api/docs/help_center/article_attachments) in the Help Center API documentation.
