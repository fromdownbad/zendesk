A _request_ is an end-user's perspective on a ticket. End-users can only see public comments and certain fields of a ticket. Use this API to let end-users view, update, and create tickets they have access to.

You can sideload some resources with requests. See Requests in [Supported Endpoints](./side_loading#supported-endpoints) in Side-loading.

#### Authentication

End users can use the Requests API.

**Note**: An end user won't be able to view their requests if the end user added an email identity (an email address associated with a Zendesk profile) after September 17, 2017, and didn't verify the email address. The problem is flagged by the API with a 403 response. See [Verifying a user's email address](https://support.zendesk.com/hc/en-us/articles/203663786) in the Support Help Center.

You must use an API token or an OAuth token with this API. Basic authentication is not supported.

Anonymous requests are supported for ticket creation. See [Create Request](#create-request) below.

##### API token

Use the following authentication format with the end-user's email address and an API token:

```{enduser_email_address}/token:{api_token}```

Example:

```bash
-u joe_enduser@example.com/token:{YOUR_API_TOKEN}
```

To get an API token, go to **Admin** > **Channels** > **API** in the Zendesk Support admin interface, then click the **Add New Token** link.

Notes:

* Use the end-user's email address, _not_ an agent's email address as is the case with all other API endpoints
* Append the string `/token` to the email address and use the API token as the password

##### OAuth token

The Requests API supports OAuth authorization flows. [Learn more](https://support.zendesk.com/hc/en-us/articles/203663836). In your requests, specify the access token in an Authorization header as follows:

`Authorization: Bearer {access_token}`

Example:

 ```bash
curl -H "Authorization: Bearer gErypPlm4dOVgGRvA1ZzMH5MQ3nLo8bo" https://obscura.zendesk.com/api/v2/requests.json
```

#### Multibrand accounts

On the Enterprise plan, a Support account can have more than one brand. See [Understanding how Multibrand works in your account](https://support.zendesk.com/hc/en-us/articles/204108983#topic_hyq_l1v_cr) in the Support Help Center.

If you have multiple brands in your account, the Requests API only returns tickets for the brand specified in the API path. It doesn't return all tickets in the account. In the API path, the brand is specified by the subdomain. For example, a GET request to `https://omniwear.zendesk.com/api/v2/requests.json` only returns tickets for the Omniwear brand, even if Omniwear is the default brand.
