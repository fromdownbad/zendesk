An automation consists of one or more actions that are performed if certain conditions are met after a period of time. The conditions are checked every hour. For example, an automation can notify an agent when a ticket remains unresolved after 24 hours.

Even if the actions are performed once, they'll be performed again later if the conditions still apply. To ensure the actions are performed only once, include an action in the automation that cancels one of the conditions.

For more information, see [Creating and managing automations for time-based events](https://support.zendesk.com/hc/en-us/articles/203662126).
