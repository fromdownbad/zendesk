Dynamic content is a combination of a default version of some text and variants of the text in other languages. The combined content is represented by a placeholder such as <tt>{{dc.my_placeholder}}</tt>. Dynamic content is available only in the Professional and Enterprise plans. [Learn more](https://support.zendesk.com/hc/en-us/articles/203663356) in the Support Help Center.

This page contains the API reference for dynamic content items. See [Dynamic Content Item Variants](./dynamic_content_item_variants) for the reference for variants.

You can use dynamic content with the API to set some properties of [ticket fields](./ticket_fields), [ticket forms](./ticket_forms), [user fields](./user_fields), and [organization fields](./organization_fields). For example, you can use dynamic content for the title of a ticket field. In that case, use the dynamic content placeholder for the value of the field's `raw_title` property and the the default version of the text as the value of the `title` property. Example:

`
"title":     "Flight Number",
"raw_title": "{{dc.my_field_title}}",
...
`

If dynamic content is not used for the property, the two values are identical:

`
"title":     "Flight Number",
"raw_title": "Flight Number",
...
`

### Data hierarchy

The data structure for dynamic content is a parent-child hierarchy where variants belong to items.

* **Items**

    These are the dynamic content placeholders defined by content creators. Each item defines a namespace for a specific piece of content, such as instructions on how to reset your password or upgrade your plan. An item's only content is the title of the item itself (defined by you) and a placeholder for that item (defined by Zendesk Support). The content itself is contained in the variants.

* **Variants**

    These are pieces of locale-specific content, where the context is based on the item they belong to. You can only have one variant per locale, per item. For example, if an item has 3 variants, they must each have a unique locale such as English, Spanish and French. They can't be English, English, and Spanish (though it is possible to have English-US and English-UK).

This hierarchy is illustrated in the following example:

![](https://support.zendesk.com/attachments/token/ckHOrvRU0CTC5U8lna5tUHT0b/?name=Screen+Shot+2014-07-18+at+9.38.12+AM.png)

Each item has a unique ID and a unique dynamic content placeholder. Below each item are a number of variants, all with their unique ID and locale. Items can contain any number of variants.

### Paginating and sorting

This API uses standard pagination and sorting parameters. See [Pagination](./introduction#pagination).

You can sort the results of any list endpoint (both asc and desc) by the following properties: `locale`, `outdated`, `active`, `updated_at`, and `created_at`. The default sorting is by `id` in descending order.

### Specifying item variants

When [creating](#create-item) or [updating](#update-item) an item, specify the variants in the item's `variants` array. Each variant consists of a `locale_id`, `default`, and `content` property. See [Dynamic Content Item Variants](./dynamic_content_item_variants) for the variants API reference.

Zendesk Support uses the `default` variant if it can't find a variant that matches the user's locale.

Example, formatted for clarity:

```json
{
  "item": {
    "name": "Snowboard Problem",
    "default_locale_id": 1,
    "variants": [
      {"locale_id": 1, "default": true, "content": "C'est mon contenu dynamique en français"},
      {"locale_id": 2, "default": false, "content": "Este es mi contenido dinámico en español"}
    ]
  }
}
```

If you have only one variant, you can use the `content` property instead of the `variants` property:

```json
{
  "item": {
    "name": "Snowboard Problem",
    "default_locale_id": 1,
    "content": "C'est mon contenu dynamique en français"
  }
}
```

However, we recommend passing an array of variants even if you only have one variant.
