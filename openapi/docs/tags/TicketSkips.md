A skip is a record of when an agent skips over a ticket without responding to the end user.
Skips are typically recorded while a play-only agent is in Guided mode.