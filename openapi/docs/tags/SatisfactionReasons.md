When [satisfaction reasons](https://support.zendesk.com/hc/en-us/articles/223152967) are enabled in your Zendesk Support account, and a user gives a negative rating to a solved ticket, a follow-up question is presented to the user. The question includes a list menu of possible reasons for the negative rating. You can use this API to inspect the list of reasons.

You must use the admin interface to add or remove reasons. See [Customizing and localizing satisfaction reasons](https://support.zendesk.com/hc/en-us/articles/223152967#topic_u2m_c4b_rw) in the Support Help Center.
