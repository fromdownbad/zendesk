You can use the imports API to move tickets in bulk from legacy systems into Zendesk Support.

Ticket imports support the normal ticket properties outlined in [Tickets](./tickets#json-format), but have some additional features and restrictions.

##### Timestamps
You can set the following time stamps on the tickets: `solved_at`, `updated_at`, and `created_at`. However, providing a value for `created_at` or `updated_at` before 1970 will be automatically rounded up to 1970.

##### Comments
You can include one or more comments with a ticket. See [Ticket Comments](./ticket_comments#json-format) for the properties. You can use `value`, `body`, or `html_body` for the comment body. You can also set the comment's `created_at` time stamp. However, you can't set the time stamp before 1970 or in the future.

##### Direct to archive
Ticket imports support the additional request parameter of `archive_immediately`. If true, any ticket created with a `closed` status bypasses the normal ticket lifecycle and will be created directly in your ticket archive.  It's important to note that archived tickets have a handful of restrictions around their use. [You can read more about it here](https://support.zendesk.com/hc/en-us/articles/203657756-About-ticket-archiving).

You may want to explore this option if you are importing a large amount of historical data (750,000+ tickets) and don't want to impact the performance of your active tickets.

##### Attachments

Attachments are handled the same way as in the tickets API. You upload the files then supply the token in the comment parameters. See [Attaching files](./tickets#attaching-files).

##### Triggers

No triggers are run on the imported tickets. As a result, there won't be any detailed ticket metrics for the tickets. Zendesk recommends setting a tag to signify that these tickets were added to Zendesk Support using import.

##### Notifications

No notifications are sent to users cc'ed on the imported tickets. Notifications are sent on subsequent updates to the tickets.