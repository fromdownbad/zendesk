Ticket forms allow an admin to define a subset of ticket fields for display to both agents and end users.

Ticket forms are only available on Enterprise accounts or accounts with the [Productivity Pack add-on](https://support.zendesk.com/hc/en-us/articles/360021065653-What-does-the-Productivity-Pack-add-on-include-).  Accounts are limited to 300 ticket forms.
