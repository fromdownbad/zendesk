You can use the Incremental Skill-based Routing API to export data on the creation, update, and deletion of skill types, skills, and instance values. To learn more about the feature, see [Using skills-based routing](https://support.zendesk.com/hc/en-us/articles/360000789788) in the Support Help Center.

In this API, skill types are named *attributes* and skills are named *attribute values*.

Skill-based routing is only available on the Enterprise plan.

### JSON Format

The exported items are represented as JSON objects. The format depends on the exported resource, but all have the following additional common attributes:

| Name       | Type    | Comment
| ---------- | ------- | -------
| end_time   | integer | The most recent resource creation time present in this result set in Unix epoch time
| next_page  | string  | The URL that should be called to get the next set of results
| count      | integer | The number of results returned for the current request

For complete lists of attributes, see the JSON format sections below.

### Pagination

The endpoints of the skill-based routing API return a maximum of 3000 records per page.

When the response exceeds the per-page maximum, you can paginate to the next page via the `next_page` URL in the response body:

```js
{
  "instance_values": [ ... ],
  "count": 1234,
  "end_time": "1535671451"
  "next_page": "https://{subdomain}.zendesk.com/api/v2/incremental/routing/instance_values.json?cursor=cccf1b69-acab-11e8-9d65-f1b3d4e2d609"
}
```

Stop paging when the `count` attribute is 0.

The pagination for skill-based incremental export endpoints works slightly differently from other endpoints. The `per_page` parameter doesn't apply because of the time-based way these endpoints return records. The `limit` parameter doesn't apply because of the way a single record can generate multiple events.

Unlike other endpoints, skill-based incremental export endpoints uses cursor-based pagination. The `cursor` parameter is a non-human-readable argument you can use to move forward or backward in time. The cursor is a read-only URL parameter that's only available in API responses.

### JSON Format for Routing Attributes
A routing attribute is a skill type. Routing attributes have the following format:

| Name  | Type    | Comment
| ----- | ------- | -------
| id    | string  | Automatically assigned when an attribute is created
| name  | string  | The name of the attribute
| time  | date    | The time the attribute was created, updated, or deleted
| type  | string  | One of "create", "update", or "delete"

#### Example
```js
{
  "id":   "7c43bca9-8c7b-11e8-b808-b99aed889f62",
  "name": "Languages",
  "time": "2018-07-21T07:17:42Z",
  "type": "create"
}
```

### JSON Format for Routing Attribute Values
A routing attribute value is a skill. Routing attribute values have the following format:

| Name         | Type    | Comment
| ------------ | ------- | -------
| id           | string  | Automatically assigned when an attribute value is created
| attribute_id | string  | Id of the associated attribute
| name         | string  | The name of the attribute value
| time         | date    | The time the attribute value was created, updated, or deleted
| type         | string  | One of "create", "update", or "delete"

#### Example
```js
{
  "id": "19ed17fb-7326-11e8-b07e-9de44e7e7f20",
  "attribute_id": "7c43bca9-8c7b-11e8-b808-b99aed889f62",
  "name": "English",
  "time": "2018-06-19T01:33:26Z",
  "type": "create"
}
```
### JSON Format for Routing Instance Values
Routing instance values have the following format:

| Name               | Type    | Comment
| ------------------ | ------- | -------
| id                 | string  | Automatically assigned when an instance value is created
| attribute_value_id | string  | Id of the associated attribute value
| instance_id        | string  | Id of the associated agent or ticket
| time               | date    | The time the instance value was created or deleted
| type               | string  | One of "associate_agent", "unassociate_agent", "associate_ticket", or "unassociate_ticket"

#### Example
```js
{
  "id": "62055cad-7326-11e8-b07e-73653560136b",
  "attribute_value_id": "19ed17fb-7326-11e8-b07e-9de44e7e7f20",
  "instance_id": "10001",
  "time": "2018-06-19T01:35:27Z",
  "type": "associate_agent"
}
```

