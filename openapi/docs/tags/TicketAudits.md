Audits are a *read-only* history of all updates to a ticket. When a ticket is updated in Zendesk Support, an audit is stored. Each audit represents a single update to the ticket. An update can consist of one or more events. Examples:

* The value of a ticket field was changed
* A new comment was added
* Tags were added or removed
* A notification was sent

For a complete list, see the [Ticket Audit events reference](https://develop.zendesk.com/hc/en-us/articles/360059038133).
