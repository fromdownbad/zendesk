Zendesk Support has three types of users: end users (your customers), agents, and administrators.

#### End users

End users request support through tickets. End users have access to Help Center where they can view knowledge base articles and community content, access their ticket history, and submit new tickets.

#### Agents

Agents work in Zendesk Support to solve tickets. Agents can be divided into multiple groups and can also belong to multiple groups. Agents don't have access to administrative configuration in Zendesk Support such as business rules or automations, but can configure their own macros and views.

#### Administrators

Administrators have all the abilities of agents, plus administrative abilities. Accounts on the Enterprise plan can configure custom roles to give agents varying degrees of administrative access.

#### Listing Requested Tickets and CC'ed Tickets

Use the Tickets API to list:

* [tickets requested by a user](./tickets#listing-tickets)
* [tickets on which a user is cc'ed](./tickets#listing-tickets)

#### Listing Help Center Content by a User

Use the Help Center API to list:

* [articles](https://developer.zendesk.com/rest_api/docs/help_center/articles#list-articles) and [article comments](https://developer.zendesk.com/rest_api/docs/help_center/comments#list-comments) created by a user
* [community posts](https://developer.zendesk.com/rest_api/docs/help_center/posts#list-posts) and [post comments](https://developer.zendesk.com/rest_api/docs/help_center/post_comments#list-comments) created by a user
