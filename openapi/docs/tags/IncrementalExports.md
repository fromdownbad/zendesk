Use the incremental export API to get items that changed or were created in Zendesk Support since the last request. It works something like this:

- **Request at 5pm**: "Give me all the tickets that changed since noon today."
- **Response**: "Here are the tickets that changed since noon up until, and including, 5pm."
- **Request at 7pm**: "Give me the tickets that changed since 5pm."
- **Response**: "Here are the tickets that changed since 5pm up until, and including, 7pm."

To learn more, see the following topics in [Using the Incremental Export API](https://develop.zendesk.com/hc/en-us/articles/1500000071922):

- [Incremental export workflow](https://develop.zendesk.com/hc/en-us/articles/1500000071922#incremental-export-workflow)
- [Cursor-based incremental exports](https://develop.zendesk.com/hc/en-us/articles/1500000071922#cursor-based-incremental-exports)
- [Time-based incremental exports](https://develop.zendesk.com/hc/en-us/articles/1500000071922#time-based-incremental-exports)
- [Exporting tickets](https://develop.zendesk.com/hc/en-us/articles/1500000071922#exporting-tickets)
- [Exporting ticket events](https://develop.zendesk.com/hc/en-us/articles/1500000071922#exporting-ticket-events)

#### Rate limits

You can make up to 10 requests per minute to these endpoints.

The rate limiting mechanism behaves identically to the one described in [Usage limits](./usage_limits#monitoring-your-request-activity). We recommend using the `Retry-After` header value as described in [Catching errors caused by rate limiting](https://develop.zendesk.com/hc/en-us/articles/360001074328#catch).

If you find yourself bumping into the rate limit when testing the API, see [Incremental Sample Export](#incremental-sample-export) to test the API without being rate-limited.


### JSON Format

The exported items are represented as JSON objects. The format depends on the exported resource and pagination type, but all have the following additional common attribute:

| Name          | Type    | Comment          |
| ------------- | ------- | ---------------- |
| end_of_stream | boolean | true if the current request has returned all the results up to the current time; false otherwise |

#### Cursor-based Pagination JSON Format

| Name          | Type    | Comment          |
| ------------- | ------- | ---------------- |
| after_url     | string  | URL to fetch the next page of results |
| after_cursor  | string  | Cursor to fetch the next page of results |
| before_url    | string  | URL to fetch the previous page of results. If there's no previous page, the value is null |
| before_cursor | string  | Cursor to fetch the previous page of results. If there's no previous page, the value is null |

#### Time-based Pagination JSON Format

| Name          | Type    | Comment          |
| ------------- | ------- | ---------------- |
| end_time      | date    | The most recent time present in the result set expressed as a Unix epoch time. Use as the `start_time` to fetch the next page of results |
| next_page     | string  | URL to fetch the next page of results |
| count         | integer | The number of results returned for the current request |


### Query String Parameters

| Name          | Required    | Comment          |
| ------------- | ----------- | ---------------- |
| start_time    | yes         | A start time expressed as a Unix epoch time. See [start_time](#start_time) |
| cursor        | cursor only | A cursor pointer. See [cursor](#cursor) |
| per_page      | no          | Number of results to return per page, up to a maximum of 1,000. If the parameter is not specified, the default number is 1,000. See [per_page](#per_page) |
| include       | no          | The name of a resource to side-load. See [include](#include)          |

#### start_time

Specifies a time expressed as a [Unix epoch time](http://www.epochconverter.com/). All endpoints require a `start_time` parameter for the initial export. Example:

`GET /api/v2/incremental/tickets/cursor.json?start_time=1532034771`

The `start_time` of the initial export is arbitrary. The time must be more than one minute in the past or it will be rejected.

When using [time-based incremental exports](https://develop.zendesk.com/hc/en-us/articles/1500000071922#time-based-incremental-exports), subsequent pages and exports use the `start_time` parameter.

When using [cursor-based incremental exports](#cursor--incremental-exports), the `start_time` parameter is only used in the initial request. Subsequent pages and exports use the [`cursor`](#cursor) parameter.

#### cursor

Specifies a cursor pointer when using cursor-based exports. The `cursor` parameter is used to fetch the next page of results or to make the next export. The `start_time` parameter is used once to fetch the initial page of the initial export, then `cursor` is used for subsequent pages and exports.

The `cursor` parameter is only supported for [incremental ticket exports](#incremental-ticket-export).

Example:

`GET /api/v2/incremental/tickets/cursor.json?cursor=MTU3NjYxMzUzOS4wfHw0Njd8`

See [Cursor-based pagination](#cursor-based-pagination) for more details.

#### per_page

Specifies the number of results to be returned per page, up to a maximum of 1,000. The default is 1,000. The following incremental exports support the `per_page` parameter:

- [tickets](#incremental-ticket-export)
- [ticket events](#incremental-ticket-event-export)

Example:

```
https://{subdomain}.zendesk.com/api/v2/incremental/tickets.json?per_page=100&start_time=1332034771
```

If requests are slow or begin to time out, the page size might be too large. Use the `per_page` parameter to reduce the number of results per page.

**Note**: In time-based exports, the system may exceed the 1000-item default return limit if items share the same timestamp. If exporting tickets, using cursor-based pagination can fix this issue.

#### include

Side-loads other resources. The following incremental exports support the `include` parameter:

- [tickets](#incremental-ticket-export)
- [ticket events](#incremental-ticket-event-export)
- [users](#incremental-user-export)
- [organizations](#incremental-organization-export)

Add an `include` query parameter specifying the associations you want to load. Example:

```
https://{subdomain}.zendesk.com/api/v2/incremental/tickets.json?start_time=1332034771&include=metric_sets
```

See [Side-Loading](./side_loading) in the core API docs as well as any specific sideloading information in the endpoint docs below.

**Note**: The `last_audits` side-load is not supported on incremental endpoints for performance reasons.

### Incremental Ticket Metric Event Export

See [List Ticket Metric Events](./ticket_metric_events#list-ticket-metric-events).

### NPS Incremental Export

See [NPS Incremental Exports](https://developer.zendesk.com/rest_api/docs/nps-api/nps_incremental_export) in the NPS API docs.

### Incremental Article Export

See [List Articles](https://developer.zendesk.com/rest_api/docs/help_center/articles#list-articles) in the Help Center API docs.
