Tickets are the means through which your end users (customers) communicate with agents in Zendesk Support. Tickets can originate from a number of channels, including email, Help Center, chat, phone call, Twitter, Facebook, or the API. All tickets have a core set of properties.

#### Tickets and Requests

Zendesk has both a Tickets API and a Requests API. A *ticket* is an agent's perspective on a ticket. A *request* is an end user's perspective on a ticket. End users can only see public comments and certain fields of a ticket. Therefore, use the [Requests API](./requests) to let end users view, update, and create tickets. Use the Tickets API described in the rest of this document to let agents and admins manage tickets.

#### Requesters and submitters

Every ticket has a requester and submitter. The user who is asking for support through a ticket is the requester. For most businesses that use Zendesk Support, the requester is a customer, but requesters can also be agents in your Zendesk Support instance.

The submitter is the user who created a ticket.  By default, the requester of a ticket is the submitter. For example, if your customer emails your support address, this creates a ticket with the customer as both the requester and submitter. The requester will also appear as the author of the ticket's first comment.

However, a support agent can also create a ticket on behalf of a customer. If an agent creates a ticket through the web interface, the agent is set as the submitter. This can be accomplished equivalently through the API by passing the agent's user ID as the ``submitter_id`` when creating a ticket. In this case, the agent, who is the submitter, becomes the author of the ticket's first comment and the ticket shows that the agent created the ticket "on behalf of" the customer.

#### The submitter is always the first comment author

In the description above, we see that a ticket's first comment author can differ depending on who created the ticket. In both examples, whomever is the submitter becomes the first comment author. This will hold true for all tickets created in Zendesk Support with one exception.

Exception: If the ticket is created as a follow-up ticket (i.e., if the ticket is created using `via_followup_source_id`), then any `submitter_id` attribute is ignored. The API sets whoever created the follow-up ticket (for the API, always the authenticated user) as the first comment author.

#### Description and first comment

When creating a ticket, use the `comment` property to set the ticket description, which is also the first comment. Example:

````js
{"ticket": {"subject": "My printer is on fire!", "comment": {"body": "The smoke is very colorful."}}}
````

**Important**: Do not use the `description` property to set the first comment. The property is for reading purposes only. While it's possible to use the property to set the first comment, the functionality has limitations and is provided to support existing implementations.

#### Groups and assignees

Tickets in Zendesk Support can be passed to a group of agents unassigned, or to a specific agent in a specific group. A ticket can only be assigned to one assignee at a time.

#### Collaborators

Aside from the requester, a ticket can include other people in its communication, known as collaborators or cc's.  Collaborators receive email notifications when tickets are updated.  Collaborators can be either end users or agents.

#### Status

All tickets in Zendesk Support start out as New and progress through Open, Pending, Solved, and Closed states.  A ticket must have an assignee in order to be solved.