When you set up Zendesk Support, you have one email address: support@example.zendesk.com. Emails received at this address become tickets.

You can provide your users with additional email addresses for submitting tickets. The additional addresses are called _support addresses_. You can add as many support addresses as you need. They can be Zendesk addresses or external addresses. If adding external addresses, additional steps are required to set up forwarding from your email server to your Zendesk account.

Support addresses allow you to customize the "sender" address for your outgoing notifications. When an email is received at a support address, Zendesk responds from the same address. For example, if an email is sent to help@example.zendesk.com, Zendesk sends a notification from help@example.zendesk.com.

For more information, see [Adding support addresses for users to submit tickets](https://support.zendesk.com/hc/en-us/articles/203663336).
