The Search API is a unified search API that returns tickets, users, and organizations. You can define filters to narrow your search results according to resource type, dates, and object properties, such as ticket requester or tag.

To search articles in Help Center, see [Search](/rest_api/docs/help_center/search) in the Help Center API documentation.

To use the API with Python or Perl, see [Searching with the Zendesk API](https://develop.zendesk.com/hc/en-us/articles/360001074228).

**Note**: It can take up to a few minutes for new tickets, users, and other resources to be indexed for search. If new resources don't appear in your search results, wait a few minutes and try again.

### Results limit

The Search API returns up to 1,000 results per query, with a maximum of 100 results per page. See [Pagination](/rest_api/docs/support/introduction#pagination). If you request a page past the limit (`page=11` at 100 results per page), a 422 Insufficient Resource Error is returned.

If you need to retrieve large datasets, Zendesk recommends breaking up the search into smaller chunks by limiting results to a specific date range. You can also use the [Export Search Results](/rest_api/docs/support/search#export-search-results) endpoint.

Alternatively, if you only want incremental changes that are ordered by `updated_at`, consider using one of the [Incremental Export](https://developer.zendesk.com/rest_api/docs/support/incremental_export) endpoints.

The `count` property shows the actual number of results. For example, if a query has 5,000 results, the `count` value will be 5,000, even if the API only returns the first 1,000 results.

### Query basics

You specify search queries in a URL parameter named `query`:

`.../api/v2/search.json?query={search_string}`

#### Syntax examples

The query syntax for `{search_string}` is detailed in the [Zendesk Support search reference](https://support.zendesk.com/hc/en-us/articles/203663226), but this section gives a quick overview. The syntax gives you a lot of flexibility. Examples:

| Query                                                                | Returns
| -------------------------------------------------------------------- | ----------
| `query=3245227`                                                      | The ticket with the id 3245227
| `query=Greenbriar`                                                   | Any record with the word Greenbriar
| `query=type:user "Jane Doe"`                                         | User records with the exact string "Jane Doe"
| `query=type:ticket status:open`                                      | Open tickets
| `query=type:organization created<2015-05-01`                         | Organizations created before May 1, 2015
| `query=status<solved requester:user@domain.com type:ticket`          | Unsolved tickets requested by user@domain.com
| `query=type:user tags:premium_support`                               | Users tagged with "premium_support"
| `query=created>2012-07-17 type:ticket organization:"MD Photo"`       | Tickets created in the MD Photo org after July 17, 2012

How it works:

* The `:` character is the equality operator. Other operators include `<` and `>`, the minus sign `-`, and the wildcard character `*`. [Learn more about the operators](https://support.zendesk.com/hc/en-us/articles/203663226#topic_ngr_frb_vc).

* Double quotes, `""`, specify a search phrase. The API uses exact word matches. For example, the search results for "top" will include data that match the string "top" as a single word, or a single word in a longer phrase ("top tier"). It will not include the prefix of a longer word ("topology"), or data where the string "top" appears in the middle or end of a word ("stopwatch", "desktop").

* The `type` property returns records of the specified resource type. Possible values include `ticket`, `user`, `organization`, or `group`. [Learn more about types](https://support.zendesk.com/hc/en-us/articles/203663226#topic_qtr_avw_ld).

* The `status` property returns tickets set to the specified status. Possible values include `new`, `open`, `pending`, `hold`, `solved`, or `closed`. [Learn more about ticket properties](https://support.zendesk.com/hc/en-us/articles/203663226#topic_crj_yev_uc). You can also search by user, organization, and group properties.

* Date-time properties such as `created`, `updated`, and `solved` can return records for a specific date, on or before a certain date, and on or after a certain date. The date format is YYYY-MM-DD. [Learn more about dates](https://support.zendesk.com/hc/en-us/articles/203663226#topic_gbg_dvw_ld).

#### URL-encoding the search string

Because a search string is sent in a HTTP request, the string must be url-encoded. Example:

`?query=type%3Aticket+status%3Aopen`

Most HTTP libraries provide tools for url-encoding strings. In cURL, you can use the `--data-urlencode` option with the `-G` flag:

```
curl "https://{subdomain}.zendesk.com/api/v2/search.json" \
-G --data-urlencode "query=type:ticket status:open" \
-v -u {email_address}:{password}
```

In Python 3, you can use the `urlencode()` method in the [urllib.parse](https://docs.python.org/3.5/library/urllib.parse.html#urllib.parse.urlencode) module.

For examples, see [Searching with the Zendesk API](https://develop.zendesk.com/hc/en-us/articles/360001074228).
