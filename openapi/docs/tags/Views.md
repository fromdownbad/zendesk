A view consists of one or more conditions that define a collection of tickets to display. If the conditions are met, the ticket is included in the view. For example, a view can display all open tickets that were last updated more than 24 hours ago.

For more information, see [Using views to manage ticket workflow](https://support.zendesk.com/entries/20103667).
