You can use the skill-based routing API to list skill types and skills, as well as list and set skills for tickets and agents. To learn more about the feature, see [Using skills-based routing](https://support.zendesk.com/hc/en-us/articles/360000789788) in the Support Help Center.

In this API, skill types are named *attributes* and skills are named *attribute values*.

Skill-based routing is only available on the Enterprise plan.

#### Attribute

An *attribute* in this API refers to a skill type. Skill types are categories of skills.

| Name       | Type    | Comment
| ---------- | ------- | -------
| id         | string  | Automatically assigned when an attribute is created
| name       | string  | The name of the attribute

#### Attribute Values

An *attribute value* in this API refers to a skill. Skills are associated with an agent and determine the agent's suitability to solve a ticket.

| Name          | Type    | Comment
| ------------- | ------- | -------
| id            | string  | Automatically assigned when an attribute value is created
| name          | string  | The name of the attribute value
| attribute_id  | string  | Id of the associated attribute
