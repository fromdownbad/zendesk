Zendesk partners can use the Reseller API to create Zendesk Support trial accounts. Partners can also configure the provisioned accounts with certain settings.
