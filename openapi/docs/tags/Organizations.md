Just as agents can be segmented into groups in Zendesk Support, your customers
(end-users) can be segmented into organizations. You can manually assign customers
to an organization or automatically assign them to an organization by their email
address domain. Organizations can be used in business rules to route tickets
to groups of agents or to send email notifications.
