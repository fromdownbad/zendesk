A Service Level Agreement is a documented agreement between a support provider and their customers that specifies performance measures for support. SLAs are often expressed as follows:

  *For urgent incidents, we will respond to tickets in 10 minutes and resolve the ticket within 2 hours.*
  *For high priority incidents, we will respond to tickets in 30 minutes and resolved the ticket within 8 hours.*

Because there may be different SLAs per customer (or group of customers) that the provider supports, the provider will define an SLA policy to support each unique SLA's requirements.

A SLA policy is the unique (not enforced) combination of criteria along with assigned metric targets for each value of the priority field. There can be multiple SLA policies per Zendesk Support account.
