You can use the ticket metric events API to track reply times, agent work times, and requester wait times.

For example, if you want to measure reply times, you can get the time a ticket was created and the time an agent first replied to it. If you want to measure requester wait times, you can get the time the ticket was created and the time its status was changed to solved.

The times are reported as *metric events*, or events that are related to each of the three metrics: reply time, agent work time, and requester wait time. You can access the following six types of metric events, with different events for each type depending on the metric:

* **activate events**, such as when a ticket is created in the case of all three metrics
* **fulfill events**, such as when an agent first replies to a ticket in the case of the reply time metric, or when a ticket is solved in the case of the requester wait time metric
* **pause events**, such as when a ticket status is changed to pending or on-hold in the case of the agent work time metric
* **apply_sla events**, such as when a SLA policy is applied to a ticket or when an SLA policy or target is changed on a ticket
* **breach events**, such as when a SLA target is breached
* **update_status events**, such as when a metric is fulfilled

For details on each type, see [Ticket metric event types reference](https://develop.zendesk.com/hc/en-us/articles/360057489694).
