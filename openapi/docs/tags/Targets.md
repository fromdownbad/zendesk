Targets are pointers to cloud-based applications and services such as Twitter and Twilio, as well as to HTTP and email addresses. You can use targets with triggers and automations to send a notification to the target when a ticket is created or updated. For a list of targets, see [Notifying external targets](https://support.zendesk.com/hc/en-us/articles/203662136).

To specify a target notification as an action for a trigger or automation, use the action's `notification_target` field. The field's value is an array of two strings specifying the numeric ID of the target and the message body. Example:

```js
{
  "actions": [
    {"field": "notification_target", "value": ["32322", "Ticket {{ticket.id}} has been updated."]}
  ]
}
```

For more information, see [Triggers](triggers.html) or [Automations](automations.html).
