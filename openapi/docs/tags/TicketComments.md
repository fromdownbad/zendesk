Ticket comments represent the conversation between requesters, collaborators, and agents. Comments can be public or private.

For information on comments in requests as opposed to tickets, see [Request comments](requests#request-comments).

#### Creating ticket comments

Ticket comments, including voice comments, are created with the Tickets API, not the Ticket Comments API described in this document. The Tickets Comments API has no endpoint to create comments.

Ticket comments are created by including a `comment` object in the `ticket` object when creating or updating the ticket. Example:

```
curl https://{subdomain}.zendesk.com/api/v2/tickets/{id}.json \
  -d '{"ticket": {"comment": { "body": "The smoke is very colorful.", "author_id": 494820284 }}}' \
  -H "Content-Type: application/json" \
  -v -u {email_address}:{password} -X PUT
```

To learn more, see [Adding voice comments to tickets](https://develop.zendesk.com/hc/en-us/articles/360057259274).

See also the following reference documentation:

* [Create Ticket](./tickets#create-ticket)
* [Update Ticket](./tickets#update-ticket)
