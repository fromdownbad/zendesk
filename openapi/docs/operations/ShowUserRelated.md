#### JSON Format

The JSON returned by this endpoint includes the following properties.

**Note:** Depending on the user's permissions, the count results may not match the actual number of tickets returned.

| Name                       | Type    | Comment
| -------------------------- | ------- | -------
| assigned_tickets           | integer | Count of assigned tickets
| requested_tickets          | integer | Count of requested tickets
| ccd_tickets                | integer | Count of collaborated tickets
| organization_subscriptions | integer | Count of organization subscriptions

It also includes the following additional properties about the Web portal, the self-help solution offered by Zendesk
before Help Center. The Web portal is deprecated but the API still returns data about the user's activity if they used it.
See the [Help Center API](https://developer.zendesk.com/rest_api/docs/help_center/introduction) to get comparable
data for Help Center. Articles, posts, comments, subscriptions, and votes each have a list-by-user endpoint.

| Name                       | Type    | Comment
| -------------------------- | ------- | -------
| topics                     | integer | Count of topics (Web portal only)
| topic_comments             | integer | Count of comments on topics (Web portal only)
| votes                      | integer | Count of votes (Web portal only)
| subscriptions              | integer | Count of subscriptions (Web portal only)
| entry_subscriptions        | integer | Count of entry subscriptions (Web portal only)
| forum_subscriptions        | integer | Count of forum subscriptions (Web portal only)

#### Allowed For:

* Agents
