Bulk or batch updates up to 100 users.

#### Bulk update

To make the same change to multiple users, use the following endpoint and data format:

`https://{subdomain}.zendesk.com/api/v2/users/update_many.json?ids=1,2,3`

```js
{
  "user": {
    "organization_id": 1
  }
}
```

#### Batch update

To make different changes to multiple users, use the following endpoint and data format:

`https://{subdomain}.zendesk.com/api/v2/users/update_many.json`

```js
{
  "users": [
    { "id": 10071, "name": "New Name", "organization_id": 1 },
    { "id": 12307, "verified": true }
  ]
}
```

#### Allowed For

* Agents, with restrictions

Agents can only update end users. Administrators can update end users, agents, and administrators.

#### Response

This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.
