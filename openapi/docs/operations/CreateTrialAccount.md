Creates a trial account.

A valid token is required to access this endpoint. Tokens are reseller-specific. Please contact [Zendesk Customer Support](https://support.zendesk.com/hc/en-us/articles/360026614173) to get a token.

To manage the account, set the `partner[name]` and `partner[url]` fields.
To enable SAML on the account, set all the `account[remote_authentication]` fields.

You can create trial accounts without these options, but you always need a token.

#### JSON Format
Trial accounts are represented as JSON objects with the following properties:

Name                    | Type    | Required | Comment |
----------------------- | ------- | -------- | ------- |
owner[name]             | string  | yes      | Full name of account owner |
owner[email]            | string  | yes      | Email of account owner |
owner[password]         | string  | yes      | Password of account owner |
address[phone]          | integer | yes      | Phone number of account owner |
account[name]           | string  | yes      | Name of company |
account[subdomain]      | string  | yes      | Desired subdomain. Example: "mydomain". Must be at least 3 characters and cannot contain underscores, hyphens, periods, or spaces |
account[help_desk_size] | string  | yes      | Size of help desk. Partners should always specify "Large group" |
language                | string  | no       | Language of account expressed as ISO-639 format. Default is English (US). Example: “en-us”. See [Supported Languages](#supported-languages) |
utc_offset              | integer | no       | Example: "-9". Default is "-8", or UTC-8 (PST). |
partner[name]           | string  | no       | Name of partner reseller. Example: "BCSG". Field is required if provisioned by reseller |
partner[url]            | string  | no       | URL customers should access to manage their account. Field is required if provisioned by reseller |
account[remote_authentication][type]              | string | no | SSO type. Example: "saml". Field is required if SAML authentication is desired |
account[remote_authentication][remote_login_url]  | string | no | URL users will be redirected to for SAML authentication. Field is required if `[type]` field is provided |
account[remote_authentication][remote_logout_url] | string | no | URL users will be redirected to when logging out through SAML. Field is required if `[type]` field is provided |
account[remote_authentication][fingerprint]       | string | no | SAML hexadecimal fingerprint. See below. Field is required if `[type]` field is provided |

The remote authentication fingerprint must be a SAML hexadecimal fingerprint consisting of 40 or more characters with no spaces or colons. Example: "0123456789ABCDEF0123456789ABCDEF000000".

#### Rate limiting

Only 5 trial accounts can be created every 1 minute per IP address.

#### Supported languages

Zendesk Support supports the following languages: English, English (Canada), English (GB), French, French (Canada), Spanish, Japanese, Portuguese (Brazil), German, Latin American Spanish, Italian, Dutch, Russian, Traditional Chinese, Simplified Chinese, Korean, Danish, Norwegian, Turkish, Swedish, Polish, Arabic, Hebrew, Indonesian, Thai, Finnish.

See [Language codes for supported languages](https://support.zendesk.com/hc/en-us/articles/203761906).
