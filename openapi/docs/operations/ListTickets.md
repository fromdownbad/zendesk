`ccd` lists tickets that the specified user is cc'd on.

`recent` lists tickets that the requesting agent recently viewed in the agent interface, not recently created or updated tickets (unless by the agent recently in the agent interface).

To get a list of all tickets in your account, use the [Incremental Ticket Export endpoint](./incremental_export#incremental-ticket-export).

For more filter options, use the [Search API](./search).

You can also sideload related records with the tickets. See [Side-Loading](./side_loading).

Archived tickets are not included in the response. See [About archived tickets](https://support.zendesk.com/hc/en-us/articles/203657756) in the Support Help Center.

#### Allowed for
* Agents

#### Pagination
* Cursor pagination (recommended)
* Offset pagination

See [Pagination](./introduction#pagination).

Returns a maximum of 100 records per page.

#### Sorting

By default, tickets are sorted by id from smallest to largest.

When using cursor pagination, use the following parameter to change the sort order:

| Name   | Type   | Required | Comments
| ------ | ------ | -------- | --------
| `sort` | string | no       | Possible values are "updated_at", "id" (ascending order) or "-updated_at", "-id" (descending order)

When using offset pagination, use the following parameters to change the sort order:

| Name         | Type   | Required | Comments
| ------------ | ------ | -------- | --------
| `sort_by`    | string | no       | Possible values are "assignee", "assignee.name", "created_at", "group", "id", "locale", "requester", "requester.name", "status", "subject", "updated_at"
| `sort_order` | string | no       | One of `asc`, `desc`. Defaults to `asc`

When sorting by creation date, the first ticket listed may not be the absolute oldest ticket in your account due to [ticket archiving](https://support.zendesk.com/entries/28452998-Ticket-Archiving).

The `query` parameter is not supported for this endpoint. Use the [Search API](./search) to narrow your results with `query`.
