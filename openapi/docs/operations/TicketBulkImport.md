#### Example Response

This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work.
Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.