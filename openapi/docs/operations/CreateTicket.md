Takes a `ticket` object that specifies the ticket properties. The only required property is `comment`. See [Ticket Comments](./ticket_comments). All writable properties listed in [JSON Format](#json-format) are optional.

Submitting a ticket with HTML data will not result in the HTML being stripped out.

An `audit` object is generated and included in the response when you create or update a ticket. The `audit` object has an `events` array listing all the updates made to the new ticket. For more information, see [Ticket Audits](./ticket_audits).
You can also add your own metadata to the `audit` object. See [Setting metadata](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-metadata).

For more information on creating tickets, see:

- [Creating a follow-up ticket](https://develop.zendesk.com/hc/en-us/articles/360059146153#creating-a-follow-up-ticket)
- [Creating a ticket asynchronously](https://develop.zendesk.com/hc/en-us/articles/360059146153#creating-a-ticket-asynchronously)
- [Setting collaborators](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-collaborators)
- [Setting followers](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-followers)
- [Setting email CCs](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-email-ccs)
- [Setting metadata](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-metadata)
- [Attaching files](https://develop.zendesk.com/hc/en-us/articles/360059146153#attaching-files)
- [Creating a ticket with a new requester](https://develop.zendesk.com/hc/en-us/articles/360059146153#creating-a-ticket-with-a-new-requester)
- [Setting custom field values](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-custom-field-values)

#### Allowed For
* Agents
