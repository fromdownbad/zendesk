To protect against collisions when updating or bulk updating tickets, see [Protecting against ticket update collisions](https://develop.zendesk.com/hc/en-us/articles/360059146153#protecting-against-ticket-update-collisions).

#### Bulk updates

To make the same change to multiple tickets, use the following endpoint and data format:

`https://{subdomain}.zendesk.com/api/v2/tickets/update_many.json?ids=1,2,3`

```js
{
  "ticket": {
    "status": "solved"
  }
}
```

#### Batch updates

To make different changes to multiple tickets, use the following endpoint and data format:

`https://{subdomain}.zendesk.com/api/v2/tickets/update_many.json`

```js
{
  "tickets": [
    { "id": 1, "status": "solved" },
    { "id": 2, "status": "pending" }
  ]
}
```

Batch updates are automatically safe and fail when there's another update to the same tickets after the job started. See [Protecting against ticket update collisions](https://develop.zendesk.com/hc/en-us/articles/360059146153#protecting-against-ticket-update-collisions).

#### Updating tag lists

You can use the bulk update format to add or remove tags to the tag list of each ticket without overwriting the existing tags. To do so, include the `additional_tags` or `remove_tags` property in the `ticket` object. Example:

```bash
curl https://{subdomain}.zendesk.com/api/v2/tickets/update_many.json?ids=1,2,3 \
  -d '{"ticket": {"additional_tags":["a_new_tag"]}}' \
  -H "Content-Type: application/json" \
  -v -u {email_address}:{password} -X PUT
```

The `additional_tags` or `remove_tags` properties only work with bulk updates, not batch updates.

This endpoint returns a `job_status` [JSON object](./job_statuses#json-format) and queues a background job to do the work. Use the [Show Job Status](./job_statuses#show-job-status) endpoint to check for the job's completion.

#### Allowed For

- Agents