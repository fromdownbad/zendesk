#### Sideloads

The following sideloads are supported. The usage sideloads are only supported on Professional and Enterprise plans.

| Name             | Will sideload
| ---------------- | -------------
| app_installation | The app installation that requires each macro, if present
| categories       | The macro categories
| permissions      | The permissions for each macro
| usage_1h         | The number of times each macro has been used in the past hour
| usage_24h        | The number of times each macro has been used in the past day
| usage_7d         | The number of times each macro has been used in the past week
| usage_30d        | The number of times each macro has been used in the past thirty days