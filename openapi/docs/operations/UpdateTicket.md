The PUT request takes a `ticket` object that lists the values to update. All properties are optional.

See also [Protecting against ticket update collisions](https://develop.zendesk.com/hc/en-us/articles/360059146153#protecting-against-ticket-update-collisions).

| Name                      | Description                                          |
| ------------------------- | ---------------------------------------------------- |
| subject                   | The subject of the ticket |
| comment                   | An object that adds a comment to the ticket. See [Ticket comments](./ticket_comments). To include an attachment with the comment, see [Attaching files](https://develop.zendesk.com/hc/en-us/articles/360059146153#attaching-files) |
| requester\_id             | The numeric ID of the user asking for support through the ticket |
| assignee\_id              | The numeric ID of the agent to assign the ticket to |
| assignee\_email           | The email address of the agent to assign the ticket to |
| group\_id                 | The numeric ID of the group to assign the ticket to |
| organization\_id          | The numeric ID of the organization to assign the ticket to. The requester must be an end user and a member of the specified organization  |
| collaborator\_ids         | An array of the numeric IDs of agents or end users to CC. Note that this replaces any existing collaborators. An email notification is sent to them when the ticket is created |
| collaborators             | An array of numeric IDs, emails, or objects containing `name` and `email` properties. See [Setting Collaborators](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-collaborators). An email notification is sent to them when the ticket is updated |
| additional\_collaborators | An array of numeric IDs, emails, or objects containing `name` and `email` properties. See [Setting Collaborators](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-collaborators). An email notification is sent to them when the ticket is updated |
| followers                 | An array of objects that represent agent followers to add or delete from the ticket. See [Setting followers](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-followers) |
| email_ccs                 | An array of objects that represent agent or end users email CCs to add or delete from the ticket. See [Setting email CCs](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-email-ccs)  |
| type                      | Allowed values are "problem", "incident", "question", or "task" |
| priority                  | Allowed values are "urgent", "high", "normal", or "low" |
| status                    | Allowed values are "open", "pending", "hold", "solved" or "closed" |
| tags                      | An array of tags to add to the ticket. Note that the tags replace any existing tags. To keep existing tags, see [Updating tag lists](#updating-tag-lists) |
| external\_id              | An ID to link tickets to local records |
| problem\_id               | For tickets of type "incident", the numeric ID of the problem the incident is linked to, if any |
| due\_at                   | For tickets of type "task", the due date of the task. Accepts the ISO 8601 date format (yyyy-mm-dd) |
| custom\_fields            | An array of the custom field objects consisting of ids and values. Any tags defined with the custom field replace existing tags |
| updated\_stamp            | Datetime of last update received from API. See the `safe_update` property
| safe\_update              | Optional boolean. Prevents updates with outdated ticket data (`updated_stamp` property required when true)
| sharing\_agreement\_ids   | An array of the numeric IDs of sharing agreements. Note that this replaces any existing agreements
| macro\_ids                | An array of macro IDs to be recorded in the ticket audit
| attribute\_value\_ids     | An array of the IDs of attribute values to be associated with the ticket

#### Audit Events

An `audit` object is generated and included in the response when you update a ticket. See [Audit events](#audit-events).

#### Allowed For
* Agents