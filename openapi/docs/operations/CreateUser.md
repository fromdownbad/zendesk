#### Skip verification email

If you need to create users without sending out a verification email, include a `"verified": true` property.


#### User role

If you don't specify a `role` parameter, the new user is assigned the role of end user.

If you need to create agents with a specific role, the `role`
property only accepts three possible values: "end-user", "agent", and
"admin". Therefore, set `role` to "agent" as well as add a new
property called `custom_role_id` and give it the actual desired
role ID from your Zendesk Support account. This applies to the
built-in light agent role of Zendesk Support as well.

#### Create User with Multiple Identities

If you have a user with multiple identities, such as email addresses and Twitter accounts, you can also include
these values at creation time by including an `identities` array where each identity in the array has a `type` and `value` property. Example: `{"type": "email", "value": "test@user.com"}`. This is especially useful when importing users from another system.

#### Allowed For

* Agents, with restrictions on certain actions
