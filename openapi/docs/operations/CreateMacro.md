#### Request Parameters

The POST request takes one parameter, a `macro` object that lists the values to set when the macro is created.

| Name        | Description
| ----------- | -----------
| actions     | Required. An object describing what the macro will do. See [Actions reference](https://develop.zendesk.com/hc/en-us/articles/360056760874-Support-API-Actions-reference)
| active      | Allowed values are true or false. Determines if the macro is displayed or not
| attachments | An array of macro attachment IDs to be associated with the macro
| description | The description of the macro
| restriction | An object that describes who can access the macro. To give all agents access to the macro, omit this property
| title       | Required. The title of the macro

The `restriction` object has the following properties.

| Name | Comment
| ---- | -------
| type | Allowed values are "Group" or "User"
| id   | The numeric ID of the group or user
| ids  | The numeric IDs of the groups
