#### Specifying email and verified attributes

* If both `email` and `verified` attributes are provided, the provided email is added to the user as a
  secondary email with the `verified` property set to the specified value. The primary email remains unmodified
* If only `email` is provided, the provided email is added to the user as a secondary email with the
    `verified` property set property set to false. The primary email remains unmodified
* If only `verified` is provided, the `verified` property of the primary email identity is set to the
   given value

#### Suspending a user

You can suspend a user by setting its `suspended` attribute to true.

When a user is suspended, the user is not allowed to sign in to Help Center and
all further tickets are suspended.

#### Updating a user's profile image

You can update a user's profile image by uploading a local file or by
referring to an image hosted on a different website. The second option
may take a few minutes to process.

#### Allowed For

* Agents, with restrictions on certain actions

Agents can only update end users. Administrators can update end users, agents, and administrators.
