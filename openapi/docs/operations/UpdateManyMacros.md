#### Request Parameters

The PUT request expects a `macros` object that lists the triggers to update.

Each macro may have the following properties

| Name     | Type      | Mandatory | Description
| -------- | --------- | --------- | -----------
| id       | integer   | yes       | The ID of the macro to update
| position | integer   | no        | The new position of the macro
| active   | boolean   | no        | The active status of the macro (true or false)
