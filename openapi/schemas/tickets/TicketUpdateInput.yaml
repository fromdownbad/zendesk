type: object
properties:
  external_id:
    description: An id you can use to link Zendesk Support tickets to local records
    type: string
  type:
    description: The type of this ticket.
    type: string
    enum:
      - problem
      - incident
      - question
      - task
  subject:
    description: The value of the subject field for this ticket
    type: string
  comment:
    $ref: ./TicketCommentObject.yaml
  priority:
    description: The urgency with which the ticket should be addressed.
    type: string
    enum:
      - urgent
      - high
      - normal
      - low
  status:
    description: The state of the ticket.
    type: string
    enum: 
      - new
      - open
      - pending
      - hold
      - solved
      - closed
  requester_id:
    description: The user who requested this ticket
    type: integer
  assignee_id:
    description: The agent currently assigned to the ticket
    type: integer
  assignee_email:
    description: The email address of the agent to assign the ticket to
    type: string
    format: email
  organization_id:
    description: The organization of the requester. You can only specify the ID of
      an organization associated with the requester. See [Organization Memberships](./organization_memberships)
    type: integer
  group_id:
    description: The group this ticket is assigned to
    type: integer
  collaborator_ids:
    description: The ids of users currently CC'ed on the ticket
    type: array
  additional_collaborators:
    description: An array of numeric IDs, emails, or objects containing name and email properties. See [Setting Collaborators](https://developer.zendesk.com/rest_api/docs/support/tickets#setting-collaborators). An email notification is sent to them when the ticket is updated
    type: array
    items:
      $ref: ./CollaboratorObject.yaml
  problem_id:
    description: For tickets of type "incident", the ID of the problem the incident
      is linked to
    type: integer
  due_at:
    description: If this is a ticket of type "task" it has a due date.  Due date format
      uses [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601) format.
    type: string
    nullable: true
    format: date-time
  tags:
    description: The array of tags applied to this ticket
    type: array
  custom_fields:
    description: Custom fields for the ticket. See [Setting custom field values](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-custom-field-values)
    type: array
  followers:
    description: An array of objects that represent agent followers to add or delete from the ticket. See [Setting followers](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-followers)
    type: array
    items:
      $ref: ./FollowerObject.yaml
  email_ccs:
    description: An array of objects that represent agent or end users email CCs to add or delete from the ticket. See [Setting email CCs](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-email-ccs)
    type: array
    items:
      $ref: ./EmailCCObject.yaml
  updated_stamp:
    description: Datetime of last update received from API. See the safe_update property
    type: string
    format: date-time
  safe_update:
    description: Optional boolean. Prevents updates with outdated ticket data (`updated_stamp` property required when true)
    type: boolean
  sharing_agreement_ids:
    description: An array of the numeric IDs of sharing agreements. Note that this replaces any existing agreements
    type: array
    items:
      type: integer
  attribute_value_ids:
    description: An array of the IDs of attribute values to be associated with the ticket
    type: array
    items:
      type: integer
example:
  subject: My printer is on fire!
  comment:
    body: The smoke is very colorful.
  priority: urgent
