type: object
x-resourceId: Tickets
properties:
  id:
    description: Automatically assigned when the ticket is created
    type: integer
    readOnly: true
  url:
    description: The API url of this ticket
    type: string
    readOnly: true
  external_id:
    description: An id you can use to link Zendesk Support tickets to local records
    type: string
  type:
    description: The type of this ticket.
    type: string
    enum:
      - problem
      - incident
      - question
      - task
  subject:
    description: The value of the subject field for this ticket
    type: string
  raw_subject:
    description: |
      The dynamic content placeholder, if present, or the "subject" value, if not. See [Dynamic Content](dynamic_content.html)
    type: string
  description:
    description: |
      Read-only first comment on the ticket. When [creating a ticket](#create-ticket), use `comment` to set the description. See [Description and first comment](#description-and-first-comment)
    type: string
    readOnly: true
  priority:
    description: The urgency with which the ticket should be addressed.
    type: string
    enum:
      - urgent
      - high
      - normal
      - low
  status:
    description: The state of the ticket.
    type: string
    enum:
      - new
      - open
      - pending
      - hold
      - solved
      - closed
  recipient:
    description: The original recipient e-mail address of the ticket
    type: string
  requester_id:
    description: The user who requested this ticket
    type: integer
  submitter_id:
    description: The user who submitted the ticket. The submitter always becomes the
      author of the first comment on the ticket
    type: integer
  assignee_id:
    description: The agent currently assigned to the ticket
    type: integer
  organization_id:
    description: The organization of the requester. You can only specify the ID of
      an organization associated with the requester. See [Organization Memberships](./organization_memberships)
    type: integer
  group_id:
    description: The group this ticket is assigned to
    type: integer
  collaborator_ids:
    description: The ids of users currently CC'ed on the ticket
    type: array
  collaborators:
    description: POST requests only. Users to add as cc's when creating a ticket.
      See [Setting Collaborators](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-collaborators)
    type: array
    items:
      $ref: ./CollaboratorObject.yaml
  email_cc_ids:
    description: The ids of agents or end users currently CC'ed on the ticket. See
      [CCs and followers resources](https://support.zendesk.com/hc/en-us/articles/360020585233)
      in the Support Help Center
    type: array
  follower_ids:
    description: The ids of agents currently following the ticket. See [CCs and followers
      resources](https://support.zendesk.com/hc/en-us/articles/360020585233)
    type: array
  forum_topic_id:
    description: The topic in the Zendesk Web portal this ticket originated from,
      if any. The Web portal is deprecated
    type: integer
    readOnly: true
  problem_id:
    description: For tickets of type "incident", the ID of the problem the incident
      is linked to
    type: integer
  has_incidents:
    description: Is true if a ticket is a problem type and has one or more incidents
      linked to it. Otherwise, the value is false.
    type: boolean
    readOnly: true
  due_at:
    description: If this is a ticket of type "task" it has a due date.  Due date format
      uses [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601) format.
    type: string
    nullable: true
    format: date-time
  tags:
    description: The array of tags applied to this ticket
    type: array
  via:
    type: object
    description: |-
      For more information, see the [Via object reference](https://develop.zendesk.com/hc/en-us/articles/360059019473)
    properties:
      channel:
        type: string
        description: |
          This tells you how the ticket or event was created. Examples: "web", "mobile", "rule", "system"
      source:
        additionalProperties: true
        type: object
        description: |
          For some channels a source object gives more information about how or why the ticket or event was created
  custom_fields:
    description: Custom fields for the ticket. See [Setting custom field values](https://develop.zendesk.com/hc/en-us/articles/360059146153#setting-custom-field-values)
    type: array
  satisfaction_rating:
    additionalProperties: true
    description: The satisfaction rating of the ticket, if it exists, or the state
      of satisfaction, "offered" or "unoffered"
    type: object
    readOnly: true
  sharing_agreement_ids:
    description: The ids of the sharing agreements used for this ticket
    type: array
    readOnly: true
  followup_ids:
    description: The ids of the followups created from this ticket. Ids are only visible
      once the ticket is closed
    type: array
    readOnly: true
  via_followup_source_id:
    description: POST requests only. The id of a closed ticket when creating a follow-up
      ticket. See [Creating a follow-up ticket](https://develop.zendesk.com/hc/en-us/articles/360059146153#creating-a-follow-up-ticket)
    type: integer
  macro_ids:
    description: POST requests only. List of macro IDs to be recorded in the ticket
      audit
    type: array
  ticket_form_id:
    description: Enterprise only. The id of the ticket form to render for the ticket
    type: integer
  brand_id:
    description: Enterprise only. The id of the brand this ticket is associated with
    type: integer
  allow_channelback:
    description: Is false if channelback is disabled, true otherwise. Only applicable
      for channels framework ticket
    type: boolean
    readOnly: true
  allow_attachments:
    description: Permission for agents to add add attachments to a comment. Defaults to true
    type: boolean
    readOnly: true
  is_public:
    description: Is true if any comments are public, false otherwise
    type: boolean
    readOnly: true
  created_at:
    description: When this record was created
    type: string
    format: date-time
    readOnly: true
  updated_at:
    description: When this record last got updated
    type: string
    format: date-time
    readOnly: true
required:
- requester_id
externalDocs:
  url: ../../docs/schemas/tickets/comment_count.md
example:
  id: 35436
  url: https://company.zendesk.com/api/v2/tickets/35436.json
  external_id: ahg35h3jh
  created_at: '2009-07-20T22:55:29Z'
  updated_at: '2011-05-05T10:38:52Z'
  type: incident
  subject: Help, my printer is on fire!
  raw_subject: "{{dc.printer_on_fire}}"
  description: The fire is very colorful.
  priority: high
  status: open
  recipient: support@company.com
  requester_id: 20978392
  submitter_id: 76872
  assignee_id: 235323
  organization_id: 509974
  group_id: 98738
  collaborator_ids:
  - 35334
  - 234
  follower_ids:
  - 35334
  - 234
  problem_id: 9873764
  has_incidents: false
  due_at:
  tags:
  - enterprise
  - other_tag
  via:
    channel: web
  custom_fields:
  - id: 27642
    value: '745'
  - id: 27648
    value: 'yes'
  satisfaction_rating:
    id: 1234
    score: good
    comment: Great support!
  sharing_agreement_ids:
  - 84432
