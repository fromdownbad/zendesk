type: object
x-resourceId: Ticket Fields
properties:
  id:
    description: Automatically assigned when created
    type: integer
    readOnly: true
  url:
    description: The URL for this resource
    type: string
    readOnly: true
  type:
    description: System or custom field type. Editable for custom field types and only on creation. See [Create Ticket Field](#create_ticket_field)
    type: string
    readOnly: false
  title:
    description: The title of the ticket field
    type: string
    readOnly: false
  raw_title:
    description: The dynamic content placeholder if present, or the `title` value if not. See [Dynamic Content](./dynamic_content)
    type: string
    readOnly: false
  description:
    description: Describes the purpose of the ticket field to users
    type: string
    readOnly: false
  raw_description:
    description: The dynamic content placeholder if present, or the `description` value if not. See [Dynamic Content](./dynamic_content)
    type: string
    readOnly: false
  position:
    description: The relative position of the ticket field on a ticket. Note that for accounts with ticket forms, positions are controlled by the different forms
    type: integer
    readOnly: false
  active:
    description: Whether this field is available
    type: boolean
    readOnly: false
  required:
    description: If true, agents must enter a value in the field to change the ticket status to solved
    type: boolean
    readOnly: false
  collapsed_for_agents:
    description: If true, the field is shown to agents by default. If false, the field is hidden alongside infrequently used fields. Classic interface only
    type: boolean
    readOnly: false
  regexp_for_validation:
    description: For "regexp" fields only. The validation pattern for a field value to be deemed valid
    type: string
    readOnly: false
    nullable: true
  title_in_portal:
    description: The title of the ticket field for end users in Help Center
    type: string
    readOnly: false
  raw_title_in_portal:
    description: The dynamic content placeholder if present, or the "title_in_portal" value if not. See [Dynamic Content](./dynamic_content)
    type: string
    readOnly: false
  visible_in_portal:
    description: Whether this field is visible to end users in Help Center
    type: boolean
    readOnly: false
  editable_in_portal:
    description: Whether this field is editable by end users in Help Center
    type: boolean
    readOnly: false
  required_in_portal:
    description: If true, end users must enter a value in the field to create the request
    type: boolean
    readOnly: false
  tag:
    description: For "checkbox" fields only. A tag added to tickets when the checkbox field is selected
    type: string
    readOnly: false
    nullable: true
  created_at:
    description: The time the custom ticket field was created
    type: string
    format: date-time
    readOnly: true
  updated_at:
    description: The time the custom ticket field was last updated
    type: string
    format: date-time
    readOnly: true
  system_field_options:
    description: Presented for a system ticket field of type "tickettype", "priority" or "status"
    type: array
    items:
      $ref: SystemFieldOptionObject.yaml
    readOnly: true
  custom_field_options:
    description: Required and presented for a custom ticket field of type "multiselect" or "tagger"
    type: array
    items:
      $ref: CustomFieldOptionObject.yaml
    readOnly: false
  sub_type_id:
    description: For system ticket fields of type "priority" and "status". Defaults to 0. A "priority" sub type of 1 removes the "Low" and "Urgent" options. A "status" sub type of 1 adds the "On-Hold" option
    type: integer
    readOnly: false
  removable:
    description: If false, this field is a system field that must be present on all tickets
    type: boolean
    readOnly: true
  agent_description:
    description: A description of the ticket field that only agents can see
    type: string
    readOnly: false
required:
- type
- title
example:
  id: 34
  url: https://company.zendesk.com/api/v2/ticket_fields/34.json
  type: subject
  title: Subject
  raw_title: "{{dc.my_title}}"
  description: This is the subject field of a ticket
  raw_description: This is the subject field of a ticket
  position: 21
  active: true
  required: true
  collapsed_for_agents: false
  regexp_for_validation:
  title_in_portal: Subject
  raw_title_in_portal: "{{dc.my_title_in_portal}}"
  visible_in_portal: true
  editable_in_portal: true
  required_in_portal: true
  tag:
  created_at: '2009-07-20T22:55:29Z'
  updated_at: '2011-05-05T10:38:52Z'
  removable: false
  agent_description: This is the agent only description for the subject field
