---
# keep all files in sync ... check test/integration/kubernetes_role_test.rb
apiVersion: apps/v1
kind: Deployment
metadata:
  name: classic-cell18-app-server
  namespace: classic
  labels:
    project: classic
    role: cell18-app-server
    team: support
    product: support
    role_group: app-server
  annotations:
    # https://github.com/zendesk/samson/tree/master/plugins/kubernetes#changing-templates-via-env
    samson/set_via_env_json: |
      spec.strategy.rollingUpdate.maxSurge: DEPLOYMENT_MAX_SURGE
      spec.template.metadata.labels.temp-auth: K8S_TEMP_AUTH_LABEL
    # https://github.com/zendesk/samson/tree/master/plugins/kubernetes#poddisruptionbudget
    samson/minAvailable: disabled
spec:
  replicas: 1
  selector:
    matchLabels:
      project: classic
      role: cell18-app-server
  strategy:
    rollingUpdate:
      maxUnavailable: 10%
      maxSurge: DEPLOYMENT_MAX_SURGE
  template:
    metadata:
      name: classic-cell18-app-server
      labels:
        project: classic
        role: cell18-app-server
        team: support
        product: support
        role_group: app-server
        temp-auth: disabled
      annotations:
        ad.datadoghq.com/tags: '{"cell": "cell18"}'
        container-raingutter-samson/dockerfile: none
        container-raingutter-samson/preStop: disabled
        container-iptables-init-samson/dockerfile: none
        sidecar.istio.io/proxyCPU: 500m
        sidecar.istio.io/proxyCPULimit: 500m
        sidecar.istio.io/proxyMemory: 1Gi
        sidecar.istio.io/proxyMemoryLimit: 1Gi
        secret/MYSQL_USER: classic_mysql_user
        secret/MYSQL_PASSWORD: classic_mysql_password
        # application.rb
        secret/ZENDESK_SESSION_SALT: zendesk_session_salt
        secret/ZENDESK_PERMANENT_COOKIE_SECRET: zendesk_permanent_cookie_secret
        secret/ZENDESK_SHARED_SESSION_SECRET: zendesk_shared_session_secret
        secret/DKIM_PRIVATE_KEY: zendesk_dkim_private_key
        # initializers/attachment_fu.rb
        secret/ATTACHMENT_FU_S3_ACCESS_KEY: attachment_fu_s3_access_key
        secret/ATTACHMENT_FU_S3_SECRET_KEY: attachment_fu_s3_secret_key
        # initializers/oauth2.rb
        secret/ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_DATA_DELETION_OAUTH: zendesk_system_user_auth_account_data_deletion_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_FRAUD_SERVICE_OAUTH: zendesk_system_user_auth_account_fraud_service
        secret/ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_SERVICE_OAUTH: zendesk_system_user_auth_account_service_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_AUDIT_LOG_SERVICE_OAUTH: zendesk_system_user_auth_audit_log_service_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ANSWER_BOT_FLOW_COMPOSER_OAUTH: zendesk_system_user_auth_answer_bot_flow_composer_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ANSWER_BOT_FLOW_DIRECTOR_OAUTH: zendesk_system_user_auth_answer_bot_flow_director_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ARTURO_OAUTH: zendesk_system_user_auth_arturo_service_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_AWS_INTEGRATION_OAUTH: zendesk_system_user_auth_aws_integration_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_BILLING_OAUTH: zendesk_system_user_auth_billing_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_BIME_OAUTH: zendesk_system_user_auth_bime_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_CENTRALADMINAPP_OAUTH: zendesk_system_user_centraladmin_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_CERTIFICATES_OAUTH: zendesk_system_user_auth_certificates_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_CERTIFICATES_SIGNED: zendesk_system_user_auth_certificates_signed
        secret/ZENDESK_SYSTEM_USER_AUTH_CHAT_AUTOMATIONS_OAUTH: zendesk_system_user_auth_chat_automations_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_CHAT_WIDGET_MEDIATOR_OAUTH: zendesk_system_user_auth_chat_widget_mediator_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_COLLABORATION_OAUTH: zendesk_system_user_auth_collaboration_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_CUSTOM_RESOURCES_APP_OAUTH: zendesk_system_user_auth_custom_resources_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_DUMPLING_OAUTH: zendesk_system_user_auth_dumpling_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_EMBEDDABLE_OAUTH: zendesk_system_user_auth_embeddable_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ENTITY_REPUBLISHER_OAUTH: zendesk_system_user_auth_entity_republisher_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_EXPERIMENTATION_OAUTH: zendesk_system_user_experimentation_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_GOODDATA_SIGNED: zendesk_system_user_auth_gooddata_signed
        secret/ZENDESK_SYSTEM_USER_AUTH_GUIDE_CLIENT_OAUTH: zendesk_system_user_auth_guide_client_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_GUIDE_REST_OAUTH: zendesk_system_user_auth_guide_rest_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_GUIDE_SIGNALS_OAUTH: zendesk_system_user_auth_guide_signals_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_HAWAII_OAUTH: zendesk_system_user_auth_hawaii_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_HELP_CENTER_OAUTH: zendesk_system_user_auth_help_center_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_INBOX_OAUTH: zendesk_system_user_auth_inbox_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ITAPPS_OAUTH: zendesk_system_user_itapps_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_KNOWLEDGE_API_OAUTH: zendesk_system_user_auth_knowledge_api_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_MAC_KEY: zendesk_system_user_auth_mac_key
        secret/ZENDESK_SYSTEM_USER_AUTH_MAXWELL_FILTERS_OAUTH: zendesk_system_user_auth_maxwell_filters_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_MAXWELL_SMARTS_OAUTH: zendesk_system_user_auth_maxwell_smarts_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_METROPOLIS_OAUTH: zendesk_system_user_auth_metropolis_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_MOBILE_SDK_API_OAUTH: zendesk_system_user_auth_mobile_sdk_api_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_MONITOR_OAUTH: zendesk_system_user_auth_monitor_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_OUTBOUND_OAUTH: zendesk_system_user_auth_outbound_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_PASSPORT_OAUTH: zendesk_system_user_auth_passport_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_PIGEON_OAUTH: zendesk_system_user_auth_pigeon_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_SALESFORCE_INTEGRATION_OAUTH: zendesk_system_user_auth_sfdc_integration_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_SANDBOX_ORCHESTRATOR_OAUTH: zendesk_system_user_auth_sandbox_orchestrator_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_SELL_OAUTH: zendesk_system_user_auth_sell_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_SINGULARITY_OAUTH: zendesk_system_user_auth_singularity_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_SLACK_INTEGRATION_OAUTH: zendesk_system_user_auth_slack_integration_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_STANCHION_OAUTH: zendesk_system_user_auth_stanchion_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_SUPPORT_GRAPH_OAUTH: zendesk_system_user_auth_support_graph_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_SUNCO_CONFIGURATOR_OAUTH: zendesk_system_user_auth_sunco_configurator_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_SYMPHONY_OAUTH: zendesk_system_user_auth_symphony_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_TALK_EMBEDDABLES_SERVICE_OAUTH: zendesk_system_user_talk_embeddables_service_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_THEMING_CENTER_OAUTH: zendesk_system_user_auth_theming_center_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_XFILES_OAUTH: zendesk_system_user_xfiles_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ZENDESK_NPS_OAUTH: zendesk_system_user_auth_zendesk_nps_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ZENDESK_OAUTH: zendesk_system_user_auth_zendesk_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ZENDESK_SIGNED: zendesk_system_user_auth_zendesk_signed
        secret/ZENDESK_SYSTEM_USER_AUTH_ZIS_ENGINE_OAUTH: zendesk_system_user_auth_zis_engine_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ZOPIM_OAUTH: zendesk_system_user_auth_zopim_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_TWITTER_PROXY_OAUTH: zendesk_system_user_auth_twitter_proxy_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_PROFILE_SERVICE_OAUTH: zendesk_system_user_auth_profile_service_oauth
        secret/ZENDESK_SYSTEM_USER_AUTH_ZDP_METADATA_OAUTH: zendesk_system_user_auth_zdp_metadata_oauth
        # initializers/recaptcha.rb
        secret/ZENDESK_RECAPTCHA_V2_SITE_KEY: zendesk_recaptcha_v2_site_key
        secret/ZENDESK_RECAPTCHA_V2_SECRET_KEY: zendesk_recaptcha_v2_secret_key
        # lib/zendesk/captcha.rb
        secret/ZENDESK_RECAPTCHA_ENTERPRISE_API_KEY: zendesk_recaptcha_enterprise_api_key
        secret/ZENDESK_RECAPTCHA_ENTERPRISE_SITE_KEY: zendesk_recaptcha_enterprise_site_key
        # initializers/remote_files.rb
        secret/ZENDESK_AWS_ACCESS_KEY: zendesk_aws_access_key
        secret/ZENDESK_AWS_SECRET_KEY: zendesk_aws_secret_key
        # zendesk_auth engine
        secret/ZENDESK_TWITTER_KEY: zendesk_twitter_key
        secret/ZENDESK_TWITTER_SECRET: zendesk_twitter_secret
        secret/ZENDESK_TWITTER_TOKEN: zendesk_twitter_token
        secret/ZENDESK_TWITTER_TOKEN_SECRET: zendesk_twitter_token_secret
        secret/ZENDESK_TWITTER_READONLY_KEY: zendesk_twitter_readonly_key
        secret/ZENDESK_TWITTER_READONLY_SECRET: zendesk_twitter_readonly_secret
        secret/ZENDESK_FACEBOOK_INTEGRATION_SECRET: zendesk_facebook_integration_secret
        secret/ZENDESK_FACEBOOK_INTEGRATION_CLIENT_ID: zendesk_facebook_integration_client_id
        secret/ZENDESK_ZENDESK_ZOPIM_MOBILE_SSO_KEY: zendesk_zopim_mobile_sso_key
        secret/ZENDESK_TWO_FACTOR_AUTH_TWILIO_TOKEN: zendesk_two_factor_auth_twilio_token
        secret/ZENDESK_MONITOR_KEY: zendesk_monitor_key
        secret/ZENDESK_GOOGLE_KEY: zendesk_google_key
        secret/ZENDESK_GOOGLE_CLIENT_ID: zendesk_google_client_id
        secret/ZENDESK_GOOGLE_CLIENT_SECRET: zendesk_google_client_secret
        secret/ZENDESK_OFFICE_365_KEY: zendesk_office_365_key
        secret/ZENDESK_GOOGLE_APP_MARKET_JWE_KEY: google_app_market_jwe_key
        # lib/zendesk/salesforce/session.rb
        secret/ZENDESK_SALESFORCE_CONSUMER_KEY: zendesk_salesforce_consumer_key
        secret/ZENDESK_SALESFORCE_CONSUMER_SECRET: zendesk_salesforce_consumer_secret
        secret/ZENDESK_SALESFORCE_INTERNAL_PASSWORD: zendesk_salesforce_internal_password
        secret/ZENDESK_SALESFORCE_INTERNAL_CONSUMER_KEY: zendesk_salesforce_internal_consumer_key
        secret/ZENDESK_SALESFORCE_INTERNAL_CONSUMER_SECRET: zendesk_salesforce_internal_consumer_secret
        # app/models/salesforce_integration.rb
        secret/ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY: ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY
        # lib/zendesk/gooddata_integrations/configuration.rb
        secret/GOODDATA_PASSWORD: gooddata_password
        secret/GOODDATA_AUTH_TOKEN: gooddata_auth_token
        secret/GOODDATA_SIGNER_PRIVATE_KEY: gooddata_signer_private_key
        # app/models/jobs/push_notification_send_job.rb
        secret/ZENDESK_URBAN_AIRSHIP_APP_KEY: zendesk_urban_airship_app_key
        secret/ZENDESK_URBAN_AIRSHIP_APP_SECRET: zendesk_urban_airship_app_secret
        secret/ZENDESK_URBAN_AIRSHIP_MASTER_SECRET: zendesk_urban_airship_master_secret
        secret/ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_APP_KEY: zendesk_urban_airship_com_zendesk_agent_app_key
        secret/ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_APP_SECRET: zendesk_urban_airship_com_zendesk_agent_app_secret
        secret/ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_MASTER_SECRET: zendesk_urban_airship_com_zendesk_agent_master_secret
        secret/ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_APP_KEY: zendesk_urban_airship_com_zendesk_inbox_app_key
        secret/ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_APP_SECRET: zendesk_urban_airship_com_zendesk_inbox_app_secret
        secret/ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_MASTER_SECRET: zendesk_urban_airship_com_zendesk_inbox_master_secret
        secret/ZENDESK_GOOGLE_CLOUD_MESSAGING_COM.ZENDESK.INBOX_API_KEY: zendesk_google_cloud_messaging_com_zendesk_inbox_api_key
        # app/models/accounts/fabric/account_creator.rb
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_ACCOUNTSERVICES: zendesk_account_creation_tokens_accountservices
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_FABRIC: zendesk_account_creation_tokens_fabric
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_INBOX: zendesk_account_creation_tokens_inbox
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_MAGENTO: zendesk_account_creation_tokens_magento
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_MANUALTEST: zendesk_account_creation_tokens_manualtest
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_MARKETINGTRAVIS: zendesk_account_creation_tokens_marketingtravis
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_NEWZENDESKAUTOMATIONS: zendesk_account_creation_tokens_newzendeskautomations
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_QA: zendesk_account_creation_tokens_qa
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_SHOPIFY: zendesk_account_creation_tokens_shopify
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_STATUS_MONITOR: zendesk_account_creation_tokens_status_monitor
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_VOYAGER: zendesk_account_creation_tokens_voyager
        secret/ZENDESK_ACCOUNT_CREATION_TOKENS_ZENDESKWEBSITE: zendesk_account_creation_tokens_zendeskwebsite
        # app/models/jobs/fraud_report_job.rb
        secret/ZENDESK_MAXMIND_MINFRAUD_LICENSE_KEY: zendesk_maxmind_minfraud_license_key
        # app/models/ticket/id_masking.rb
        secret/ZENDESK_TICKET_ENCODING_KEY: zendesk_ticket_encoding_key
        secret/ZENDESK_TICKET_VALIDATION_KEY: zendesk_ticket_validation_key
        secret/ZENDESK_EMAIL_ENCODED_ID_V2_SALT : zendesk_email_encoded_id_v2_salt
        # app/models/screenr/business_partner_client.rb
        secret/ZENDESK_SCREENR_PASSWORD: zendesk_screenr_password
        # app/helpers/radar_helper.rb
        secret/ZENDESK_NODE_PUBSUB_SECRET_KEY: zendesk_node_pubsub_secret_key
        # lib/zendesk/account_readiness.rb
        secret/ZENDESK_DOORMAN_SECRET: doorman_secret
        # zendesk_billing_core
        secret/BILLING_ZUORA_PASSWORD: zuora_password
        secret/BILLING_ZUORA_API_PASSWORD: zuora_password
        # lib/zendesk/attachments/token.rb
        secret/ZENDESK_ATTACHMENT_TOKEN_KEY: zendesk_attachment_token_key
        # config/initializers/automatic_answers_jwt_secret.rb
        secret/AUTOMATIC_ANSWERS_JWT_SECRET: automatic_answers_jwt_secret
        # app/models/certificate.rb
        secret/ZENDESK_CERTIFICATES_SSL_KEY_PASSWORD: ZENDESK_CERTIFICATES_SSL_KEY_PASSWORD
        # app/controllers/external_email_credentials_controller.rb
        secret/ZENDESK_EXTERNAL_EMAIL_CREDENTIAL_CLIENT_SECRET: zendesk_external_email_credential_client_secret
        # app/models/external_email_credential.rb
        secret/EXTERNAL_EMAIL_CREDENTIALS_ENCRYPTION_KEY_A: external_email_credentials_encryption_key_a
        # zendesk_channels
        secret/ZENDESK_TWITTER_CACHE_S3_ACCESS_KEY: zendesk_twitter_cache_s3_access_key
        secret/ZENDESK_TWITTER_CACHE_S3_SECRET_KEY: zendesk_twitter_cache_s3_secret_key
        secret/ZENDESK_TWITTER_PROXY_SHARED_SECRET: zendesk_twitter_proxy_shared_secret
        secret/CHANNELS_FB_ENCRYPT_KEY_A: CHANNELS_FB_ENCRYPT_KEY_A
        secret/CHANNELS_FB_ENCRYPT_KEY_B: CHANNELS_FB_ENCRYPT_KEY_B
        secret/CHANNELS_TWITTER_CREDENTIAL_KEY_A: CHANNELS_TWITTER_CREDENTIAL_KEY_A
        secret/CHANNELS_TWITTER_CREDENTIAL_KEY_B: CHANNELS_TWITTER_CREDENTIAL_KEY_B
        # zendesk_comment_markup
        secret/ZENDESK_IMAGE_PROXY_SECRET: zendesk_image_proxy_secret
        # config/initializers/remote_files.rb
        secret/REMOTE_FILES_AWS_ACCESS_KEY: remote_files_aws_access_key
        secret/REMOTE_FILES_AWS_SECRET_KEY: remote_files_aws_secret_key
        # lib/zendesk/revere/api_client.rb
        secret/REVERE_API_KEY: revere_api_key
        # app/helpers/lotus_bootstrap_helper.rb
        secret/ROLLBAR_EU_LOTUS_ACCESS_TOKEN: rollbar_eu_lotus_access_token
        secret/ROLLBAR_US_LOTUS_ACCESS_TOKEN: rollbar_us_lotus_access_token
        # app/views/layouts/lotus_bootstrap.html.erb
        secret/DD_RUM_APPLICATION_ID: DD_RUM_APPLICATION_ID
        secret/DD_RUM_TOKEN: DD_RUM_TOKEN
        # app/models/targets/target.rb
        secret/TARGET_CREDENTIALS_ENCRYPTION_KEY_A: TARGET_CREDENTIALS_ENCRYPTION_KEY_A
        secret/TARGET_CREDENTIALS_ENCRYPTION_KEY_B: TARGET_CREDENTIALS_ENCRYPTION_KEY_B
        # config/initializers/11_uploader_configuration.rb
        secret/UPLOADER_AWS_ACCESS_KEY_ID: zendesk_aws_access_key
        secret/UPLOADER_AWS_SECRET_ACCESS_KEY: zendesk_aws_secret_key
        secret/REPLICATED_UPLOADER_AWS_ACCESS_KEY_ID: replicated_aws_access_key_id
        secret/REPLICATED_UPLOADER_AWS_SECRET_ACCESS_KEY: replicated_aws_secret_access_key
        samson/required_env: >
          RAILS_ENV
          ZENDESK_HOST
          ZENDESK_POD_ID
          ZENDESK_DATACENTER_DOMAIN
          EDGE_PROXY_INTERNAL_NLB_URL
          ROLLBAR_REGION
          ROLLBAR_US_ACCESS_TOKEN
          ROLLBAR_EU_ACCESS_TOKEN
          REDIS_CLUSTER
          RESQUE_CLUSTER
          ZENDESK_EXTERNAL_EMAIL_CREDENTIAL_CLIENT_ID
          EXTERNAL_EMAIL_CREDENTIALS_ENCRYPTION_KEY
          ZENDESK_GLOBAL_UID_OPTIONS_QUERY_TIMEOUT
          ZENDESK_GLOBAL_UID_OPTIONS_CONNECTION_TIMEOUT
          ZENDESK_GLOBAL_UID_OPTIONS_INCREMENT_BY
          ZENDESK_GLOBAL_UID_OPTIONS_ID_SERVERS
          ZENDESK_OUTBOUND_MAIL_DELAY
          ZENDESK_OUTBOUND_MAIL_BATCH_SIZE
          ZENDESK_OUTBOUND_MAIL_RENDERING_JOB_TIMEOUT
          ZENDESK_OUTBOUND_MAIL_LOG_HEADER
          ZENDESK_OUTBOUND_MAIL_SMTP_SETTINGS_ADDRESS
          ZENDESK_OUTBOUND_MAIL_SMTP_SETTINGS_PORT
          ZENDESK_OUTBOUND_MAIL_SMTP_SETTINGS_DOMAIN
          ZENDESK_OUTBOUND_MAIL_SMTP_SETTINGS_TLS
          ZENDESK_DKIM_DOMAIN
          ZENDESK_DKIM_SELECTOR
          ZENDESK_DKIM_PRIVATE_KEY
          ZENDESK_ROSETTA_URL
          ZENDESK_TWO_FACTOR_AUTH_TWILIO_ACCOUNT_SID
          ZENDESK_TWO_FACTOR_AUTH_TWILIO_NUMBERS
          ZENDESK_GEO_LOCATION_DOWNLOAD_URL
          ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_CIPHER_NAME
          ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY_NAME
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_SITE
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_SCHEME
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_REQUEST_TOKEN_PATH
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_AUTHORIZE_PATH
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_ACCESS_TOKEN_PATH
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_SANDBOX_SITE
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_SANDBOX_SCHEME
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_SANDBOX_REQUEST_TOKEN_PATH
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_SANDBOX_AUTHORIZE_PATH
          ZENDESK_SALESFORCE_OAUTH_OPTIONS_SANDBOX_ACCESS_TOKEN_PATH
          ZENDESK_SALESFORCE_LOGIN_URL
          ZENDESK_SALESFORCE_LOGIN_URL_SANDBOX
          ZENDESK_SALESFORCE_INTERNAL_SITE
          ZENDESK_SALESFORCE_INTERNAL_LOGIN
          GOODDATA_LOGIN
          GOODDATA_ORGANIZATION
          GOODDATA_SSO_PROVIDER
          GOODDATA_SIGNER_EMAIL
          GOODDATA_API_ENDPOINT
          GOODDATA_TOUR_PROJECT_ID
          GOODDATA_TOUR_PROJECT_DASHBOARD_ID
          GOODDATA_TOUR_PROJECT_EMAIL
          ZENDESK_OCCAM_K8S_URL
          ZENDESK_OCCAM_OPEN_TIMEOUT
          ZENDESK_SAAS_PULSE_ID
          ZENDESK_SYSTEM_GLOBAL_CLIENTS_GOODDATA
          ZENDESK_SYSTEM_GLOBAL_CLIENTS_ZOPIM
          ZENDESK_SCREENR_API_TIMEOUT
          ZENDESK_SCREENR_BASE_URI
          ZENDESK_SCREENR_USERNAME
          VOICE_VIP_HOST
          VOICE_VIP_PORT
          ZENDESK_GOOGLE_SITE
          ZENDESK_GOOGLE_AUTHORIZE_URL
          ZENDESK_GOOGLE_REDIRECT_URI
          ZENDESK_GOOGLE_TOKEN_URL
          ZOPIM_SCRIBE_API_BASE_URL
          ZOPIM_SCRIBE_API_PORT
          VOICE_VIP_HOST
          VOICE_VIP_PORT
          RIAK_BUCKET
          RIAK_PB_PORT
          ZENDESK_ZENDESK_VOYAGER_HOST
          ZENDESK_ZENDESK_VOYAGER_PORT
          ZENDESK_ZENDESK_VOYAGER_TIMEOUT
          ZENDESK_OFFICE_365_CLIENT_ID
          ZENDESK_OFFICE_365_REDIRECT_URI
          ZENDESK_IMAGE_PROXY_HOST
          ZENDESK_PIGEON_URL
          ZENDESK_TWITTER_CACHE_S3_BUCKET
          ZENDESK_TWITTER_CACHE_S3_REGION
          ACCOUNT_SERVICE_URL
          STAFF_SERVICE_URL
          SEARCH_SERVICE_URL_BLUE
          SEARCH_SERVICE_URL_GREEN
          SEARCH_SERVICE_HUB_URL
          CHANNELS_CFC_DOMAIN
          CHANNELS_FB_ENCRYPTION_CIPHER_NAME
          CHANNELS_FB_ENCRYPTION_KEY_NAME
          CHANNELS_TWITTER_ENCRYPTION_CIPHER_NAME
          CHANNELS_TWITTER_ENCRYPTION_KEY_NAME
          TWITTER_PROXY_URL
          TARGET_CREDENTIALS_ENCRYPTION_KEY
          TARGET_CREDENTIALS_ENCRYPTION_CIPHER_NAME
          UPLOADER_STORAGE
          UPLOADER_BUCKET_NAME
          UPLOADER_REGION
          REPLICATED_UPLOADER_BUCKET_NAME
          REPLICATED_UPLOADER_REGION
          OCCAM_REDIS_TIMEOUT
          OCCAM_REDIS_PASSWORD
          OCCAM_REDIS_CLUSTER
    spec:
      # This Configmap networks is created by the Paas Team
      # see: https://zendesk.atlassian.net/browse/PAAS-1138
      volumes:
      - name: network-config
        configMap:
          name: networks
      - name: radar
        configMap:
          name: radar
      - name: attachments
        configMap:
          name: attachments
      initContainers:
      - name: iptables-init
        image: gcr.io/docker-images-180022/apps/kube-debug-console@sha256:20ba8aa96b3b8d23faf108a95adfeb4d7fe43ea67e48df1cd2a28f8bc3ca6b6c
        command:
          - '/bin/sh'
          - '-c'
          - 'iptables -t filter -I OUTPUT -d 169.254.169.254 -j REJECT'
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
        resources:
          requests:
            cpu: 100m
            memory: 50Mi
          limits:
            cpu: 100m
            memory: 50Mi
      containers:
      - name: unicorn
        image: gcr.io/docker-images-180022/apps/classic:latest
        resources:
          requests:
            cpu: '3.0'
            memory: 10G
          limits:
            cpu: '3.0'
            memory: 10G
        ports:
        - name: main-port
          containerPort: 4080
          protocol: TCP
        readinessProbe:
          httpGet:
            path: /z/ping
            port: 4080
          initialDelaySeconds: 60
          timeoutSeconds: 10
          successThreshold: 2
          failureThreshold: 20
        livenessProbe:
          httpGet:
            path: /z/ping
            port: 4080
          initialDelaySeconds: 90
          timeoutSeconds: 120
          failureThreshold: 5
        lifecycle:
          preStop:
            exec:
              command: ['sleep', '31'] # DNS TTL; make sure we have a chance to handle any lagging requests
        volumeMounts:
        - name: network-config
          mountPath: /app/config/networks.d
        - name: radar
          mountPath: /app/config/radar.d
        - name: attachments
          mountPath: /app/config/attachments.d
        env:
        - name: ROLLBAR_ENABLED
          value: 'true'
        - name: NETWORKS_CONFIG_PATH
          value: '/app/config/networks.d/networks.yml'
        - name: PROXYSQL_ACCOUNT_MASTER_ENABLED
          value: 'true'
        - name: PROXYSQL_SHARD_MASTER_ENABLED
          value: 'true'
        - name: MYSQL_READ_TIMEOUT
          value: '57'
        - name: ENABLE_QUERY_KILLER
          value: 'true'
        - name: COMPUTE_CELL_NAME
          value: 'cell18'
      - name: raingutter
        # https://github.com/zendesk/raingutter
        image: gcr.io/docker-images-180022/apps/raingutter:v54
        resources:
          requests:
            cpu: 100m
            memory: 100M
          limits:
            cpu: 100m
            memory: 100M
        env:
        - name: RG_USE_SOCKET_STATS
          value: 'true'
        - name: RG_UNICORN_PORT
          value: '4080'
        - name: RG_STATSD_HOST
          value: 169.254.1.1
        - name: RG_STATSD_PORT
          value: '8125'
        - name: RG_FREQUENCY
          value: '500'
        - name: RG_STATSD_NAMESPACE
          value: zendesk.unicorn.raindrops.agg.
        - name: RG_STATSD_EXTRA_TAGS
          value: 'cell:cell18,kube_role:cell18-app-server'
        securityContext:
          runAsNonRoot: true
          readOnlyRootFilesystem: true

---
apiVersion: v1
kind: Service
metadata:
  name: cell18-classic
  namespace: classic
  labels:
    project: classic
    role: cell18-app-server
    team: support
    product: support
    consul-tag/diagnostic-http: "true"
    kube-service-watcher/namespace_suffix: "false"
  annotations:
    samson/persistent_fields: >
      metadata.labels.proxy
spec:
  type: ClusterIP
  ports:
  - name: http
    port: 80
    targetPort: main-port
  selector:
    project: classic
    role: cell18-app-server

---
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: classic-cell18-app-server
  namespace: classic
  labels:
    project: classic
    role: cell18-app-server
    team: support
    product: support
spec:
  maxUnavailable: 10%
  selector:
    matchLabels:
      project: classic
      role: cell18-app-server

---
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: classic-cell18-app-server-hpa
  namespace: classic
  labels:
    project: classic
    role: cell18-app-server
    role_group: app-server-hpa
    team: support
    product: support
    cell: cell18
  annotations:
    samson/keep_name: 'true'
    samson/set_via_env_json: |
      spec.minReplicas: CELL_APP_SERVER_HPA_MIN_REPLICAS
      spec.maxReplicas: CELL_APP_SERVER_HPA_MAX_REPLICAS
      spec.metrics.0.resource.target.averageUtilization: HPA_TARGET_CPU
      spec.metrics.1.external.metric.selector.matchLabels.kube_cluster: HPA_METRICS_KUBE_CLUSTER
      spec.metrics.1.external.metric.selector.matchLabels.pod_namespace: HPA_METRICS_KUBE_NAMESPACE
      spec.metrics.1.external.target.value: HPA_TARGET_ACTIVE_UNICORNS
spec:
  minReplicas: CELL_APP_SERVER_HPA_MIN_REPLICAS
  maxReplicas: CELL_APP_SERVER_HPA_MAX_REPLICAS
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: classic-cell18-app-server
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: HPA_TARGET_CPU
  - type: External
    external:
      metric:
        name: zendesk.unicorn.raindrops.agg.active.max
        selector:
          matchLabels:
            kube_cluster: HPA_METRICS_KUBE_CLUSTER
            pod_namespace: HPA_METRICS_KUBE_NAMESPACE
            project: classic
            cell: cell18
      target:
        type: Value
        value: HPA_TARGET_ACTIVE_UNICORNS
