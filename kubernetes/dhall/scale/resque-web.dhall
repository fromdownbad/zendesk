let Scale = ./package.dhall

in  Scale::{
    , replicas = Some 1
    , cpuRequests = "0.5"
    , memoryRequests = "1G"
    , cpuLimit = Some "1.0"
    }
