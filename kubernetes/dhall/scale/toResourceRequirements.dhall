let k8s = ../dependencies/dhall-kubernetes.dhall

let Scale = ./package.dhall

in  \(requirements : Scale.Type) ->
      k8s.ResourceRequirements::{
      , requests = Some
          ( toMap
              { cpu = requirements.cpuRequests
              , memory = requirements.memoryRequests
              }
          )
      , limits = Some (toMap (Scale.matchLimitsToRequests requirements))
      }
