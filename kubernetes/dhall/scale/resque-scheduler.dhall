let Scale = ./package.dhall

in  Scale::{
    , replicas = Some 3
    , cpuRequests = "1.0"
    , memoryRequests = "1G"
    , cpuLimit = Some "3.0"
    , memoryLimit = Some "3G"
    }
