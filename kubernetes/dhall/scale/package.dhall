let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let Scale =
      { replicas : Optional Natural
      , cpuRequests : Text
      , memoryRequests : Text
      , cpuLimit : Optional Text
      , memoryLimit : Optional Text
      }

let defaultTo = Prelude.Optional.default Text

let matchLimitsToRequests =
      \(requirements : Scale) ->
        { cpu = defaultTo requirements.cpuRequests requirements.cpuLimit
        , memory =
            defaultTo requirements.memoryRequests requirements.memoryLimit
        }

in  { Type = Scale
    , default =
      { replicas = None Natural, cpuLimit = None Text, memoryLimit = None Text }
    , matchLimitsToRequests
    }
