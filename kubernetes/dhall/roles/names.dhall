let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let AppServerConfig = ./app-server.dhall

let ConsumerConfig = ./consumer.dhall

let MiscProcessConfig = ./misc-process.dhall

let WorkerConfig = ./worker.dhall

let console = "console.yml"

let resqueScheduler = "resque-scheduler.yml"

let resqueWeb = "resque-web.yml"

let durableQueueMonitor = "durable-queue-monitor.yml"

let mailTicketCreator = "mail-ticket-creator.yml"

let resqueOverflowHpa = "resque-overflow-hpa.yml"

let appServerHpa = "app-server-hpa.yml"

let priorityAppServerHpa = "priority-app-server-hpa.yml"

let archiverDynamoDbResource = "archiver-dynamodb-resource.yml"

let envConfigMap = "env.yml"

let secretKeysConfigMap = "secret-keys.yml"

let configMaps = [ envConfigMap, secretKeysConfigMap ]

let deploys =
      \(pod : Pod.Type) ->
          AppServerConfig.names pod
        # ConsumerConfig.names pod
        # MiscProcessConfig.names pod
        # WorkerConfig.names pod
        # [ console, resqueScheduler, durableQueueMonitor ]

in  { deploys
    , configMaps
    , console
    , resqueScheduler
    , resqueWeb
    , durableQueueMonitor
    , mailTicketCreator
    , resqueOverflowHpa
    , appServerHpa
    , priorityAppServerHpa
    , archiverDynamoDbResource
    , envConfigMap
    , secretKeysConfigMap
    }
