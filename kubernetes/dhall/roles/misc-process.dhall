let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let Scale = ../scale/package.dhall

let Config = { name : Text, args : List Text } //\\ Scale.Type

let role = \(config : Config) -> "classic-${config.name}"

let defaultRequirements = { replicas = Some 1 }

let default = defaultRequirements

let MiscProcess = { Type = Config, default, role }

let fileName = \(config : MiscProcess.Type) -> "${config.name}.yml"

let files =
      \(pod : Pod.Type) ->
        [ MiscProcess::{
          , name = "durable-backfiller"
          , args = [ "script/durable_backfiller", "--retry-on-crash" ]
          , memoryRequests = "3000Mi"
          , memoryLimit = Some "6000Mi"
          , cpuRequests = "0.5"
          , cpuLimit = Some "1.0"
          }
        , MiscProcess::{
          , name = "archiver-1"
          , args =
            [ "script/archiver"
            , "--worker_mod=1,2"
            , "--delete"
            , "--retry-on-crash"
            ]
          , memoryRequests = "3000Mi"
          , memoryLimit = Some "6000Mi"
          , cpuRequests = "0.5"
          , cpuLimit = Some "3.0"
          }
        , MiscProcess::{
          , name = "archiver-2"
          , args =
            [ "script/archiver"
            , "--worker_mod=2,2"
            , "--delete"
            , "--retry-on-crash"
            ]
          , memoryRequests = "3000Mi"
          , memoryLimit = Some "6000Mi"
          , cpuRequests = "0.5"
          , cpuLimit = Some "3.0"
          }
        , MiscProcess::{
          , name = "sync-attachments"
          , args =
            [ "script/sync_attachment_stores", "--loop", "--retry-on-crash" ]
          , memoryRequests = "3000Mi"
          , memoryLimit = Some "6000Mi"
          , cpuRequests = "1.0"
          , cpuLimit = Some "3.0"
          }
        , MiscProcess::{
          , name = "session-store-cleanup"
          , args = [ "rake", "shared_sessions:cleanup_expired" ]
          , memoryRequests = "3000Mi"
          , memoryLimit = Some "6000Mi"
          , cpuRequests = "1.0"
          , cpuLimit = Some "3.0"
          }
        , MiscProcess::{
          , name = "account-deletion"
          , args = [ "script/data_deletion_runner" ]
          , memoryRequests = "1000Mi"
          , memoryLimit = Some "3000Mi"
          , cpuRequests = "0.2"
          , cpuLimit = Some "1.0"
          }
        ]

let names =
      \(pod : Pod.Type) ->
        Prelude.List.map MiscProcess.Type Text fileName (files pod)

in  { Config = MiscProcess, name = fileName, names, files }
