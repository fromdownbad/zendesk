let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let Scale = ../scale/package.dhall

let defaultRequirements =
      Scale::{ replicas = Some 1, cpuRequests = "3.0", memoryRequests = "10G" }

let Config =
          { role : Text
          , serviceName : Text
          , preStopSleep : Text
          , inlineHpa : Bool
          , isPriority : Bool
          }
      //\\  Scale.Type

let default =
          { role = "app-server"
          , serviceName = "classic"
          , preStopSleep = "11"
          , inlineHpa = False
          , isPriority = False
          }
      /\  defaultRequirements

let AppServer = { Type = Config, default }

let fileName = \(config : AppServer.Type) -> "${config.role}.yml"

let AppServer/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 200
          , Pod14 = 40
          , Pod15 = 25
          , Pod17 = 180
          , Pod18 = 220
          , Pod19 = 180
          , Pod20 = 160
          , Pod23 = 180
          , Pod25 = 120
          , Pod26 = 5
          , Pod27 = 70
          , Pod998 = 5
          , Pod999 = 5
          }
          pod

let Priority/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 8
          , Pod14 = 3
          , Pod15 = 3
          , Pod17 = 3
          , Pod18 = 3
          , Pod19 = 3
          , Pod20 = 15
          , Pod23 = 8
          , Pod25 = 50
          , Pod26 = 8
          , Pod27 = 5
          , Pod998 = 5
          , Pod999 = 5
          }
          pod

let files =
      \(pod : Pod.Type) ->
        let base = AppServer::{ replicas = Some (AppServer/replicas pod) }

        let priority =
              AppServer::{
              , replicas = Some (Priority/replicas pod)
              , role = "priority-app-server"
              , serviceName = "classic-priority"
              , preStopSleep = "31"
              , isPriority = True
              }

        in  [ base, priority ]

let names =
      \(pod : Pod.Type) ->
        Prelude.List.map AppServer.Type Text fileName (files pod)

in  { Config = AppServer, name = fileName, names, files }
