let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let Scale = ../scale/package.dhall

let Config = { queue : Text } //\\ Scale.Type

let defaultRequirements =
      Scale::{
      , cpuRequests = "1.0"
      , memoryRequests = "5000Mi"
      , cpuLimit = Some "5.0"
      , memoryLimit = Some "10000Mi"
      }

let default = defaultRequirements

let name = \(config : Config) -> Text/replace "_" "-" config.queue ++ "-worker"

let role = \(config : Config) -> "classic-${name config}"

let Worker = { Type = Config, default, name, role }

let fileName = \(config : Worker.Type) -> "${Worker.name config}.yml"

let AccountDeletion/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 4
          , Pod14 = 4
          , Pod15 = 2
          , Pod17 = 4
          , Pod18 = 4
          , Pod19 = 4
          , Pod20 = 4
          , Pod23 = 4
          , Pod25 = 2
          , Pod26 = 2
          , Pod27 = 2
          , Pod998 = 3
          , Pod999 = 3
          }
          pod

let Automations/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 36
          , Pod14 = 36
          , Pod15 = 10
          , Pod17 = 36
          , Pod18 = 36
          , Pod19 = 36
          , Pod20 = 36
          , Pod23 = 36
          , Pod25 = 36
          , Pod26 = 8
          , Pod27 = 18
          , Pod998 = 3
          , Pod999 = 3
          }
          pod

let Channels/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 90
          , Pod14 = 30
          , Pod15 = 16
          , Pod17 = 70
          , Pod18 = 70
          , Pod19 = 70
          , Pod20 = 70
          , Pod23 = 70
          , Pod25 = 30
          , Pod26 = 5
          , Pod27 = 30
          , Pod998 = 3
          , Pod999 = 32
          }
          pod

let CloudStorage/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 30
          , Pod14 = 30
          , Pod15 = 5
          , Pod17 = 20
          , Pod18 = 30
          , Pod19 = 30
          , Pod20 = 40
          , Pod23 = 30
          , Pod25 = 20
          , Pod26 = 5
          , Pod27 = 10
          , Pod998 = 2
          , Pod999 = 2
          }
          pod

let External/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 6
          , Pod14 = 6
          , Pod15 = 6
          , Pod17 = 6
          , Pod18 = 6
          , Pod19 = 6
          , Pod20 = 6
          , Pod23 = 6
          , Pod25 = 6
          , Pod26 = 6
          , Pod27 = 6
          , Pod998 = 1
          , Pod999 = 1
          }
          pod

let High/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 85
          , Pod14 = 50
          , Pod15 = 20
          , Pod17 = 52
          , Pod18 = 52
          , Pod19 = 85
          , Pod20 = 100
          , Pod23 = 85
          , Pod25 = 30
          , Pod26 = 10
          , Pod27 = 30
          , Pod998 = 3
          , Pod999 = 3
          }
          pod

let Intermediate/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 60
          , Pod14 = 40
          , Pod15 = 8
          , Pod17 = 60
          , Pod18 = 60
          , Pod19 = 60
          , Pod20 = 60
          , Pod23 = 60
          , Pod25 = 50
          , Pod26 = 5
          , Pod27 = 20
          , Pod998 = 3
          , Pod999 = 1
          }
          pod

let Low/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 4
          , Pod14 = 4
          , Pod15 = 2
          , Pod17 = 4
          , Pod18 = 4
          , Pod19 = 4
          , Pod20 = 4
          , Pod23 = 4
          , Pod25 = 4
          , Pod26 = 4
          , Pod27 = 4
          , Pod998 = 2
          , Pod999 = 2
          }
          pod

let Maintenance/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 4
          , Pod14 = 4
          , Pod15 = 4
          , Pod17 = 4
          , Pod18 = 4
          , Pod19 = 4
          , Pod20 = 4
          , Pod23 = 4
          , Pod25 = 4
          , Pod26 = 4
          , Pod27 = 4
          , Pod998 = 3
          , Pod999 = 3
          }
          pod

let Medium/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 30
          , Pod14 = 30
          , Pod15 = 30
          , Pod17 = 30
          , Pod18 = 30
          , Pod19 = 30
          , Pod20 = 30
          , Pod23 = 30
          , Pod25 = 40
          , Pod26 = 10
          , Pod27 = 20
          , Pod998 = 2
          , Pod999 = 2
          }
          pod

let Overflow/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 10
          , Pod14 = 10
          , Pod15 = 10
          , Pod17 = 10
          , Pod18 = 10
          , Pod19 = 10
          , Pod20 = 50
          , Pod23 = 10
          , Pod25 = 10
          , Pod26 = 3
          , Pod27 = 10
          , Pod998 = 5
          , Pod999 = 5
          }
          pod

let RuleCount/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 10
          , Pod14 = 10
          , Pod15 = 4
          , Pod17 = 10
          , Pod18 = 10
          , Pod19 = 10
          , Pod20 = 10
          , Pod23 = 10
          , Pod25 = 10
          , Pod26 = 4
          , Pod27 = 4
          , Pod998 = 2
          , Pod999 = 2
          }
          pod

let StatsBackfill/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 2
          , Pod14 = 2
          , Pod15 = 2
          , Pod17 = 2
          , Pod18 = 2
          , Pod19 = 2
          , Pod20 = 2
          , Pod23 = 2
          , Pod25 = 2
          , Pod26 = 2
          , Pod27 = 2
          , Pod998 = 1
          , Pod999 = 1
          }
          pod

let StatsGeneric/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 10
          , Pod14 = 10
          , Pod15 = 4
          , Pod17 = 10
          , Pod18 = 10
          , Pod19 = 10
          , Pod20 = 10
          , Pod23 = 10
          , Pod25 = 10
          , Pod26 = 5
          , Pod27 = 5
          , Pod998 = 10
          , Pod999 = 10
          }
          pod

let TwoFactorAuth/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 2
          , Pod14 = 2
          , Pod15 = 2
          , Pod17 = 2
          , Pod18 = 2
          , Pod19 = 2
          , Pod20 = 2
          , Pod23 = 2
          , Pod25 = 2
          , Pod26 = 2
          , Pod27 = 2
          , Pod998 = 1
          , Pod999 = 1
          }
          pod

let Stats/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 2
          , Pod14 = 2
          , Pod15 = 2
          , Pod17 = 2
          , Pod18 = 2
          , Pod19 = 2
          , Pod20 = 2
          , Pod23 = 2
          , Pod25 = 2
          , Pod26 = 2
          , Pod27 = 2
          , Pod998 = 4
          , Pod999 = 4
          }
          pod

let OutboundMail/replicas =
      \(pod : Pod.Type) ->
        merge
          { Pod13 = 80
          , Pod14 = 30
          , Pod15 = 20
          , Pod17 = 100
          , Pod18 = 75
          , Pod19 = 80
          , Pod20 = 80
          , Pod23 = 80
          , Pod25 = 50
          , Pod26 = 10
          , Pod27 = 30
          , Pod998 = 4
          , Pod999 = 4
          }
          pod

let files =
      \(pod : Pod.Type) ->
        [ Worker::{
          , queue = "account_deletion"
          , replicas = Some (AccountDeletion/replicas pod)
          }
        , Worker::{
          , queue = "automations"
          , replicas = Some (Automations/replicas pod)
          }
        , Worker::{
          , queue = "channels"
          , replicas = Some (Channels/replicas pod)
          }
        , Worker::{
          , queue = "cloud_storage"
          , replicas = Some (CloudStorage/replicas pod)
          }
        , Worker::{
          , queue = "external"
          , replicas = Some (External/replicas pod)
          }
        , Worker::{ queue = "high", replicas = Some (High/replicas pod) }
        , Worker::{
          , queue = "immediate"
          , replicas = Some (Intermediate/replicas pod)
          }
        , Worker::{ queue = "low", replicas = Some (Low/replicas pod) }
        , Worker::{
          , queue = "maintenance"
          , replicas = Some (Maintenance/replicas pod)
          }
        , Worker::{ queue = "medium", replicas = Some (Medium/replicas pod) }
        , Worker::{
          , queue = "overflow"
          , replicas = Some (Overflow/replicas pod)
          }
        , Worker::{
          , queue = "rule_count"
          , replicas = Some (RuleCount/replicas pod)
          }
        , Worker::{
          , queue = "stats_backfill"
          , replicas = Some (StatsBackfill/replicas pod)
          }
        , Worker::{
          , queue = "stats_generic"
          , replicas = Some (StatsGeneric/replicas pod)
          }
        , Worker::{
          , queue = "two_factor_auth"
          , replicas = Some (TwoFactorAuth/replicas pod)
          }
        , Worker::{ queue = "stats", replicas = Some (Stats/replicas pod) }
        , Worker::{
          , queue = "outbound_mail"
          , replicas = Some (OutboundMail/replicas pod)
          }
        ]

let names =
      \(pod : Pod.Type) ->
        Prelude.List.map Worker.Type Text fileName (files pod)

in  { Config = Worker, name = fileName, names, files }
