let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let Scale = ../scale/package.dhall

let Config = { owner : Text, name : Text, consumerClass : Text } //\\ Scale.Type

let default =
      Scale::{
      , replicas = Some 2
      , cpuRequests = "500m"
      , memoryRequests = "512M"
      , cpuLimit = Some "1000m"
      , memoryLimit = Some "1024M"
      }

let name = \(config : Config) -> "${config.name}-consumer"

let Consumer = { Type = Config, default, name }

let fileName = \(config : Consumer.Type) -> "${Consumer.name config}.yml"

let files =
      \(pod : Pod.Type) ->
        [ Consumer::{
          , name = "brand-account-move"
          , owner = "piratos"
          , consumerClass = "BrandAccountMoveConsumer"
          }
        , Consumer::{
          , name = "user-account-move"
          , owner = "piratos"
          , consumerClass = "UserAccountMoveConsumer"
          }
        , Consumer::{
          , name = "account-audits"
          , owner = "vortex"
          , consumerClass = "AccountAuditsConsumer"
          , replicas = Some 0
          }
        ]

let names =
      \(pod : Pod.Type) ->
        Prelude.List.map Consumer.Type Text fileName (files pod)

in  { Config = Consumer, name = fileName, names, files }
