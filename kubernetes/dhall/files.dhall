{-
Use dhall-render and build up a list of all files that need to be generated.

TODO: explain this further
-}
let Render = ../../dhall-config/dependencies/render.dhall

let Prelude = ../../dhall-config/dependencies/prelude.dhall

let Pod = (../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let EnvVarsConfigMap = ./objects/config-maps/env.dhall

let SecretKeysConfigMap = ./objects/config-maps/secret-keys.dhall

let Console = ./manifests/console.dhall

let ArchiverDynamoDb = ./manifests/archiver-dynamodb.dhall

let ResqueOverflowHpa = ./manifests/resque-overflow-hpa.dhall

let ResqueScheduler = ./manifests/resque-scheduler.dhall

let ResqueWeb = ./manifests/resque-web.dhall

let DurableQueueMonitor = ./manifests/durable-queue-monitor.dhall

let AppServerFiles = ./files/app-server.dhall

let AppServerHpa = ./manifests/app-server-hpa.dhall

let PriorityAppServerHpa = ./manifests/priority-app-server-hpa.dhall

let ConsumerFiles = ./files/consumer.dhall

let MiscProcessFiles = ./files/misc-process.dhall

let WorkerFiles = ./files/worker.dhall

let fileTypes = ./files/types.dhall

let K8sListYAML = fileTypes.K8sListYAML

let K8sListOrResource = fileTypes.K8sListOrResource

let K8sDeploymentYAML = fileTypes.K8sDeploymentYAML

let K8sConfigMapYAML = fileTypes.K8sConfigMapYAML

let K8sStorageRequestYAML = fileTypes.K8sStorageRequestYAML

let K8sHpaYAML = fileTypes.K8sHpaYAML

let names = ./roles/names.dhall

let Console/fromPod
    : Pod.Type -> K8sListOrResource
    = \(pod : Pod.Type) ->
        K8sListOrResource.Deployment
          K8sDeploymentYAML::{
          , path = Some names.console
          , install = Render.Install.None
          , contents = Console.render pod
          }

let ResqueScheduler/fromPod =
      \(pod : Pod.Type) ->
        K8sListOrResource.ResourceList
          K8sListYAML::{
          , path = Some names.resqueScheduler
          , install = Render.Install.None
          , contents = ResqueScheduler.render pod
          }

let ResqueWeb/fromPod =
      \(pod : Pod.Type) ->
        K8sListOrResource.ResourceList
          K8sListYAML::{
          , path = Some names.resqueWeb
          , install = Render.Install.None
          , contents = ResqueWeb.render pod
          }

let DurableQueueMonitor/fromPod =
      \(pod : Pod.Type) ->
        K8sListOrResource.ResourceList
          K8sListYAML::{
          , path = Some names.durableQueueMonitor
          , install = Render.Install.None
          , contents = DurableQueueMonitor.render pod
          }

let EnvVarsConfigMap/fromPod =
      \(pod : Pod.Type) ->
        K8sListOrResource.ConfigMap
          K8sConfigMapYAML::{
          , path = Some names.envConfigMap
          , install = Render.Install.None
          , contents = EnvVarsConfigMap.fromPod pod
          }

let SecretKeysConfigMapFile/fromPod =
      \(pod : Pod.Type) ->
        K8sListOrResource.ConfigMap
          K8sConfigMapYAML::{
          , path = Some names.secretKeysConfigMap
          , install = Render.Install.None
          , contents = SecretKeysConfigMap.render pod
          }

let ResqueOverflowHpaFile/fromPod =
      \(pod : Pod.Type) ->
        K8sListOrResource.Hpa
          K8sHpaYAML::{
          , path = Some names.resqueOverflowHpa
          , install = Render.Install.None
          , contents = ResqueOverflowHpa.render pod
          }

let AppServerHpaFile/fromPod =
      \(pod : Pod.Type) ->
        K8sListOrResource.Hpa
          K8sHpaYAML::{
          , path = Some names.appServerHpa
          , install = Render.Install.None
          , contents = AppServerHpa.render pod
          }

let PriorityAppServerHpaFile/fromPod =
      \(pod : Pod.Type) ->
        K8sListOrResource.Hpa
          K8sHpaYAML::{
          , path = Some names.priorityAppServerHpa
          , install = Render.Install.None
          , contents = PriorityAppServerHpa.render pod
          }

let ArchiverDynamoDbFile =
      K8sListOrResource.StorageRequest
        K8sStorageRequestYAML::{
        , path = Some names.archiverDynamoDbResource
        , install = Render.Install.None
        , contents = ArchiverDynamoDb.render
        }

let SingleFiles/fromPod
    : Pod.Type -> List K8sListOrResource
    = \(pod : Pod.Type) ->
        Prelude.List.map
          (Pod.Type -> K8sListOrResource)
          K8sListOrResource
          (\(toYAML : Pod.Type -> K8sListOrResource) -> toYAML pod)
          [ Console/fromPod
          , ResqueScheduler/fromPod
          , ResqueWeb/fromPod
          , DurableQueueMonitor/fromPod
          , EnvVarsConfigMap/fromPod
          , AppServerHpaFile/fromPod
          , PriorityAppServerHpaFile/fromPod
          , ResqueOverflowHpaFile/fromPod
          , SecretKeysConfigMapFile/fromPod
          ]

let Files/fromPod
    : Pod.Type -> List K8sListOrResource
    = \(pod : Pod.Type) ->
          SingleFiles/fromPod pod
        # AppServerFiles.fromPod pod
        # MiscProcessFiles.fromPod pod
        # WorkerFiles.fromPod pod
        # ConsumerFiles.fromPod pod
        # [ ArchiverDynamoDbFile ]

let Files/perPod
    : Pod.All.Type (List K8sListOrResource)
    = Pod.All.generate (List K8sListOrResource) Files/fromPod

in  { options = Render.Options::{ destination = "../generated" }
    , files = Files/perPod
    }
