{-
Add some custom resource definitions to the dhall-kubernetes schemas record.
-}
let k8s = ../dependencies/dhall-kubernetes.dhall

let ResourceList =
    {-
    A helper type to create a single object containing multiple Kubernetes resources. The Kubernetes API
    understands the `kind: List` and will iterate each object within it and update them all accordingly.

    This is an alternative to multiple YAML documents in a single file to make it easier to transition
    to and from Dhall and YAML without using multiple documents.
    -}   { Type = { apiVersion : Text, kind : Text, items : List k8s.Resource }
         , default = { apiVersion = "v1", kind = "List" }
         }

let ZendeskTag = { Type = { key : Text, value : Text }, default = {=} }

let TableDefinition =
      { Type =
          { attributeDefinitions :
              List { attributeName : Text, attributeType : Text }
          , billingMode : Text
          , keySchema : List { attributeName : Text, keyType : Text }
          }
      , default = {=}
      }

let S3LifecyclePolicies =
      { Type =
          { prefix : Text
          , expire_objects_after_days : Optional Natural
          , infrequent_access_after_days : Optional Natural
          }
      , default =
        { expire_objects_after_days = None Natural
        , infrequent_access_after_days = None Natural
        }
      }

let StorageRequestPodSpec =
      { Type =
          { tableDefinition : Optional TableDefinition.Type
          , lifecycle_policies : Optional (List S3LifecyclePolicies.Type)
          , tags : Optional (List ZendeskTag.Type)
          }
      , default =
        { tableDefinition = None TableDefinition.Type
        , lifecycle_policies = None (List S3LifecyclePolicies.Type)
        , tags = None (List ZendeskTag.Type)
        }
      }

let StorageRequestPodTemplateSpec =
      { Type =
          { metadata : Optional k8s.ObjectMeta.Type
          , spec : Optional StorageRequestPodSpec.Type
          }
      , default =
        { metadata = None k8s.ObjectMeta.Type
        , spec = None StorageRequestPodSpec.Type
        }
      }

let StorageRequestSpec =
      { Type =
          { deployTarget : Text
          , name : Text
          , template : StorageRequestPodTemplateSpec.Type
          , type : Text
          , tags : Optional (List ZendeskTag.Type)
          }
      , default.tags = None (List ZendeskTag.Type)
      }

let StorageRequest =
      { Type =
          { apiVersion : Text
          , kind : Text
          , metadata : k8s.ObjectMeta.Type
          , spec : Optional StorageRequestSpec.Type
          }
      , default =
        { apiVersion = "storage.zende.sk/v1alpha1"
        , kind = "StorageRequest"
        , spec = None StorageRequestSpec.Type
        }
      }

in      k8s
    /\  { StorageRequest
        , StorageRequestSpec
        , StorageRequestPodTemplateSpec
        , StorageRequestPodSpec
        , S3LifecyclePolicies
        , TableDefinition
        , ResourceList
        }
