let Render = ../../../dhall-config/dependencies/render.dhall

let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let Worker = ../manifests/worker.dhall

let fileTypes = ./types.dhall

let K8sListOrResource = fileTypes.K8sListOrResource

let K8sListYAML = fileTypes.K8sListYAML

let FileConfig = ../roles/worker.dhall

let fileName =
      \(config : Worker.Config.Type) ->
        "${Worker.Config.name config}.yml"

let workerToYAML
    : Pod.Type -> Worker.Config.Type -> K8sListOrResource
    = \(pod : Pod.Type) ->
      \(config : Worker.Config.Type) ->
        K8sListOrResource.ResourceList
          K8sListYAML::{
          , path = Some (fileName config)
          , install = Render.Install.None
          , contents = Worker.render config pod
          }

let fromPod =
      \(pod : Pod.Type) ->
        Prelude.List.map
          Worker.Config.Type
          fileTypes.K8sListOrResource
          (workerToYAML pod)
          (FileConfig.files pod)

in  { fromPod }
