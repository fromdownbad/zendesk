let Render = ../../../dhall-config/dependencies/render.dhall

let k8s = ../zenk8s/schemas.dhall

let K8sListYAML = Render.YAMLFile (List k8s.Resource)

let K8sDeploymentYAML = Render.YAMLFile k8s.Deployment.Type

let K8sConfigMapYAML = Render.YAMLFile k8s.ConfigMap.Type

let K8sStorageRequestYAML = Render.YAMLFile k8s.StorageRequest.Type

let K8sHpaYAML = Render.YAMLFile k8s.HorizontalPodAutoscaler.Type

let K8sListOrResource =
      < ResourceList : K8sListYAML.Type
      | Deployment : K8sDeploymentYAML.Type
      | ConfigMap : K8sConfigMapYAML.Type
      | StorageRequest : K8sStorageRequestYAML.Type
      | Hpa : K8sHpaYAML.Type
      >

in  { K8sListOrResource
    , K8sStorageRequestYAML
    , K8sDeploymentYAML
    , K8sConfigMapYAML
    , K8sListYAML
    , K8sHpaYAML
    }
