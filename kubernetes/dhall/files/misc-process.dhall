let Render = ../../../dhall-config/dependencies/render.dhall

let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let MiscProcess = ../manifests/misc-process.dhall

let fileTypes = ./types.dhall

let K8sListOrResource = fileTypes.K8sListOrResource

let K8sListYAML = fileTypes.K8sListYAML

let FileConfig = ../roles/misc-process.dhall

let fileName = \(config : MiscProcess.Config.Type) -> "${config.name}.yml"

let miscProcessToYAML
    : Pod.Type -> MiscProcess.Config.Type -> K8sListOrResource
    = \(pod : Pod.Type) ->
      \(config : MiscProcess.Config.Type) ->
        K8sListOrResource.ResourceList
          K8sListYAML::{
          , path = Some (fileName config)
          , install = Render.Install.None
          , contents = MiscProcess.render config pod
          }

let fromPod =
      \(pod : Pod.Type) ->
        Prelude.List.map
          MiscProcess.Config.Type
          K8sListOrResource
          (miscProcessToYAML pod)
          (FileConfig.files pod)

in  { fromPod }
