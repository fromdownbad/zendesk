let Render = ../../../dhall-config/dependencies/render.dhall

let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let Consumer = ../manifests/consumer.dhall

let fileTypes = ./types.dhall

let K8sListOrResource = fileTypes.K8sListOrResource

let K8sDeploymentYAML = fileTypes.K8sDeploymentYAML

let FileConfig = ../roles/consumer.dhall

let fileName = \(config : Consumer.Config.Type) -> "${config.name}-consumer.yml"

let consumerToYAML
    : Pod.Type -> Consumer.Config.Type -> K8sListOrResource
    = \(pod : Pod.Type) ->
      \(config : Consumer.Config.Type) ->
        K8sListOrResource.Deployment
          K8sDeploymentYAML::{
          , path = Some (fileName config)
          , install = Render.Install.None
          , contents = Consumer.render config pod
          }

let fromPod =
      \(pod : Pod.Type) ->
        Prelude.List.map
          Consumer.Config.Type
          K8sListOrResource
          (consumerToYAML pod)
          (FileConfig.files pod)

in  { fromPod }
