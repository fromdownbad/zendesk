let Render = ../../../dhall-config/dependencies/render.dhall

let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let AppServer = ../manifests/app-server.dhall

let fileTypes = ./types.dhall

let K8sListOrResource = fileTypes.K8sListOrResource

let K8sListYAML = fileTypes.K8sListYAML

let FileConfig = ../roles/app-server.dhall

let fileName = \(config : AppServer.Config.Type) -> "${config.role}.yml"

let appServerResources
    : Pod.Type -> AppServer.Config.Type -> K8sListOrResource
    = \(pod : Pod.Type) ->
      \(config : AppServer.Config.Type) ->
        K8sListOrResource.ResourceList
          K8sListYAML::{
          , path = Some (fileName config)
          , install = Render.Install.None
          , contents = AppServer.render config pod
          }

let fromPod =
      \(pod : Pod.Type) ->
        Prelude.List.map
          AppServer.Config.Type
          K8sListOrResource
          (appServerResources pod)
          (FileConfig.files pod)

in  { fromPod }
