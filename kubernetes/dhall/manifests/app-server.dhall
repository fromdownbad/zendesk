{-
This takes all of the Kubernetes resources that make up what we call a "role", and combines them
into a Kubernetes resource list. For example, the app-server role will contain a deployment,
service, pod disruption budget, and horizontal pod autoscaler. All of these objects get put into
a list which can then be applied to the API servers.
-}
let k8s = ../zenk8s/schemas.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let deployment = ../objects/deployments/app-server.dhall

let service = ../objects/services/app-server.dhall

let pdb = ../objects/pod-disruption-budgets/app-server.dhall

let hpa = ./app-server-hpa.dhall

let Config = (../roles/app-server.dhall).Config

let Annotations = ../objects/shared/annotations.dhall

let render
    : Config.Type -> Pod.Type -> List k8s.Resource
    = \(config : Config.Type) ->
      \(pod : Pod.Type) ->
          [ k8s.Resource.Deployment (deployment.render config pod)
          , k8s.Resource.Service (service config.serviceName config.role)
          , Annotations.replace (pdb config.role)
          ]
        # ( if    config.inlineHpa
            then  [ k8s.Resource.HorizontalPodAutoscaler (hpa.render pod) ]
            else  [] : List k8s.Resource
          )

in  { Config, render }
