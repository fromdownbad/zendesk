let schemas = ../zenk8s/schemas.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let role = "app-server-hpa"

let name = "classic-app-server-hpa"

let cpuUtilizationMetric =
      schemas.MetricSpec::{
      , resource = Some schemas.ResourceMetricSource::{
        , name = "cpu"
        , target = schemas.MetricTarget::{
          , averageUtilization = Some 65
          , type = "Utilization"
          }
        }
      , type = "Resource"
      }

let activeUnicornsMetric =
      \(pod : Pod.Type) ->
        schemas.MetricSpec::{
        , type = "External"
        , external = Some schemas.PodsMetricSource::{
          , metric = schemas.MetricIdentifier::{
            , name = "zendesk.unicorn.raindrops.agg.active.max"
            , selector = Some schemas.LabelSelector::{
              , matchLabels = Some
                  ( toMap
                      { kube_cluster = Pod.name pod
                      , pod_namespace = "classic"
                      , project = "classic"
                      }
                  )
              }
            }
          , target = schemas.MetricTarget::{
            , type = "Value"
            , value = Some "HPA_TARGET_ACTIVE_UNICORNS"
            }
          }
        }

let render =
      \(pod : Pod.Type) ->
        schemas.HorizontalPodAutoscaler::{
        , metadata = schemas.ObjectMeta::{
          , labels = Some
              ( toMap
                  { project = "classic"
                  , role
                  , role_group = "app-server-hpa"
                  , team = "support"
                  }
              )
          , name
          , namespace = Some "classic"
          }
        , spec = Some schemas.HorizontalPodAutoscalerSpec::{
          , maxReplicas = 12
          , minReplicas = Some 5
          , scaleTargetRef = schemas.CrossVersionObjectReference::{
            , apiVersion = "apps/v1"
            , kind = "Deployment"
            , name
            }
          , metrics = Some [ cpuUtilizationMetric, activeUnicornsMetric pod ]
          }
        }

in  { render }
