{-
This takes all of the Kubernetes resources that make up what we call a "role", and combines them
into a Kubernetes resource list. For example, the app-server role will contain a deployment,
service, pod disruption budget, and horizontal pod autoscaler. All of these objects get put into
a list which can then be applied to the API servers.
-}
let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let k8s = ../zenk8s/schemas.dhall

let zk8s = ../dependencies/zenk8s.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let service = ../objects/services/resque-web.dhall

let project = ../../../dhall-config/project.dhall

let Deployment = ../objects/deployments/resque-web.dhall

let Annotations = ../objects/shared/annotations.dhall

let render =
      \(pod : Pod.Type) ->
        let metadata =
              zk8s.Metadata.fromProject
                (zk8s.Metadata.Role.Custom { name = "resque-scheduler" })
                project

        let deployment = Deployment.render pod

        in    [ k8s.Resource.Deployment deployment
              , k8s.Resource.Service service
              ]
            # Prelude.Optional.toList
                k8s.Resource
                ( Prelude.Optional.map
                    k8s.PodDisruptionBudget.Type
                    k8s.Resource
                    Annotations.replace
                    ( zk8s.Resource.PodDisruptionBudget.fromDeployment
                        metadata.(zk8s.Metadata.Type)
                        deployment
                    )
                )

in  { render }
