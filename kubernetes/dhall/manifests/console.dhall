{-
A wrapper around the console deployment resources into the Zendesk concept of a role. In the console
role, there are no other resources, so this exists purely to maintain consistency with other roles
that comprise many resources.
-}
let deployment = ../objects/deployments/console.dhall

in  { render = deployment.render }
