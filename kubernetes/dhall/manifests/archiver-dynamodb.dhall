let zk8s = ../zenk8s/schemas.dhall

let render =
      zk8s.StorageRequest::{
      , metadata = zk8s.ObjectMeta::{
        , labels = Some
            ( toMap
                { project = "classic"
                , role = "archiver-dynamodb-resource"
                , team = "support"
                , product = "support"
                }
            )
        , name = "classic-archiver-dynamodb-resource"
        , namespace = Some "classic"
        }
      , spec = Some zk8s.StorageRequestSpec::{
        , deployTarget = "EachPartition"
        , name = "tickets"
        , template = zk8s.StorageRequestPodTemplateSpec::{
          , metadata = Some zk8s.ObjectMeta::{
            , labels = Some
                ( toMap
                    { project = "classic"
                    , role = "archiver-dynamodb-resource"
                    , team = "support"
                    , product = "support"
                    }
                )
            , name = "classic-archiver-dynamodb-resource"
            }
          , spec = Some zk8s.StorageRequestPodSpec::{
            , tableDefinition = Some zk8s.TableDefinition::{
              , attributeDefinitions =
                [ { attributeName = "PK", attributeType = "S" }
                , { attributeName = "SK", attributeType = "S" }
                ]
              , billingMode = "PAY_PER_REQUEST"
              , keySchema =
                [ { attributeName = "PK", keyType = "HASH" }
                , { attributeName = "SK", keyType = "RANGE" }
                ]
              }
            }
          }
        , type = "dynamodbtable"
        }
      }

in  { render }
