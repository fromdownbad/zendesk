let k8s = ../zenk8s/schemas.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let render =
      \(pod : Pod.Type) ->
        k8s.HorizontalPodAutoscaler::{
        , metadata = k8s.ObjectMeta::{
          , name = "classic-resque-overflow-hpa"
          , namespace = Some "classic"
          , labels = Some
              ( toMap
                  { project = "classic"
                  , role = "resque-overflow-hpa"
                  , team = "support"
                  }
              )
          }
        , spec = Some k8s.HorizontalPodAutoscalerSpec::{
          , minReplicas = Some 10
          , maxReplicas = 20
          , scaleTargetRef = k8s.CrossVersionObjectReference::{
            , apiVersion = "apps/v1"
            , kind = "Deployment"
            , name = "classic-overflow-worker"
            }
          , metrics = Some
            [ k8s.MetricSpec::{
              , type = "External"
              , external = Some k8s.ExternalMetricSource::{
                , metric = k8s.MetricIdentifier::{ name = "resque.size" }
                , target = k8s.MetricTarget::{
                  , type = "AverageValue"
                  , value = Some "10"
                  }
                }
              }
            ]
          }
        }

in  { render }
