let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let schemas = ../zenk8s/schemas.dhall

let cpuUtilizationMetric =
      schemas.MetricSpec::{
      , resource = Some schemas.ResourceMetricSource::{
        , name = "cpu"
        , target = schemas.MetricTarget::{
          , averageUtilization = Some 65
          , type = "Utilization"
          }
        }
      , type = "Resource"
      }

let activeUnicornMetric =
      \(pod : Pod.Type) ->
        schemas.MetricSpec::{
        , external = Some schemas.PodsMetricSource::{
          , metric = schemas.MetricIdentifier::{
            , name = "classic_priority.unicorn.raindrops.agg.active.max"
            , selector = Some schemas.LabelSelector::{
              , matchLabels = Some
                  ( toMap
                      { kube_cluster = Pod.name pod
                      , pod_namespace = "classic"
                      , project = "classic"
                      }
                  )
              }
            }
          , target = schemas.MetricTarget::{ type = "Value", value = Some "12" }
          }
        , type = "External"
        }

let render =
      \(pod : Pod.Type) ->
        schemas.HorizontalPodAutoscaler::{
        , metadata = schemas.ObjectMeta::{
          , labels = Some
              ( toMap
                  { project = "classic"
                  , role = "priority-app-server-hpa"
                  , role_group = "app-server-hpa"
                  , team = "support"
                  , product = "support"
                  }
              )
          , name = "classic-priority-app-server-hpa"
          , namespace = Some "classic"
          }
        , spec = Some schemas.HorizontalPodAutoscalerSpec::{
          , minReplicas = Some 10
          , maxReplicas = 20
          , metrics = Some [ cpuUtilizationMetric, activeUnicornMetric pod ]
          , scaleTargetRef = schemas.CrossVersionObjectReference::{
            , apiVersion = "apps/v1"
            , kind = "Deployment"
            , name = "classic-priority-app-server"
            }
          }
        }

in  { render }
