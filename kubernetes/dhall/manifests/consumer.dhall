{-
A wrapper around the consumer deployment resources into the Zendesk concept of a role. In the consumer
role, there are no other resources, so this exists purely to maintain consistency with other roles
that comprise many resources.
-}
let deployment = ../objects/deployments/consumer.dhall

in  { Config = deployment.Config, render = deployment.render }
