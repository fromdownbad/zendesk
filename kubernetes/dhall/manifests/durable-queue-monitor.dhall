let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let k8s = ../dependencies/dhall-kubernetes.dhall

let zk8s = ../dependencies/zenk8s.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let project = ../../../dhall-config/project.dhall

let ipInitContainer = ../objects/containers/iptables-init.dhall

let defaultContainer = ../objects/containers/default.dhall

let durableQueueContainer = ../objects/containers/durable-queue-monitor.dhall

let scale = ../scale/durable-queue-monitor.dhall

let Annotations = ../objects/shared/annotations.dhall

let role = "durable-queue-monitor"

let labels =
      { project = "classic"
      , role = "durable-queue-monitor"
      , role_group = "singleton"
      , team = "support"
      }

let annotations =
      toMap
        { samson/minAvailable = "1"
        , samson/set_via_env_json =
            ''
            spec.template.metadata.labels.temp-auth: K8S_TEMP_AUTH_LABEL
            ''
        }

let podAnnotations =
      toMap { container-iptables-init-samson/dockerfile = "none" }

let Deployment/fromPod =
      \(pod : Pod.Type) ->
        k8s.Deployment::{
        , metadata = k8s.ObjectMeta::{
          , annotations = Some annotations
          , labels = Some (toMap labels)
          , name = "classic-durable-queue-monitor"
          , namespace = Some "classic"
          }
        , spec = Some k8s.DeploymentSpec::{
          , replicas = scale.replicas
          , selector = k8s.LabelSelector::{
            , matchLabels = Some (toMap { project = "classic", role })
            }
          , template = k8s.PodTemplateSpec::{
            , metadata = k8s.ObjectMeta::{
              , annotations = Some podAnnotations
              , labels = Some (toMap (labels /\ { temp-auth = "disabled" }))
              , name = "classic-durable-queue-monitor"
              }
            , spec = Some k8s.PodSpec::{
              , containers =
                [ durableQueueContainer.render
                    (defaultContainer.render pod role)
                ]
              , initContainers = Some [ ipInitContainer.render ]
              }
            }
          }
        }

let render =
      \(pod : Pod.Type) ->
        let metadata =
              zk8s.Metadata.fromProject
                (zk8s.Metadata.Role.Custom { name = "durable-queue-monitor" })
                project

        let deployment = Deployment/fromPod pod

        in    [ k8s.Resource.Deployment deployment ]
            # Prelude.Optional.toList
                k8s.Resource
                ( Prelude.Optional.map
                    k8s.PodDisruptionBudget.Type
                    k8s.Resource
                    Annotations.replace
                    ( zk8s.Resource.PodDisruptionBudget.fromDeployment
                        metadata.(zk8s.Metadata.Type)
                        deployment
                    )
                )

in  { render }
