{-
A wrapper around the misc-process deployment resources into the Zendesk concept of a role. In the console
role, there are no other resources, so this exists purely to maintain consistency with other roles
that comprise many resources.
-}
let Prelude = ../../../dhall-config/dependencies/prelude.dhall

let k8s = ../dependencies/dhall-kubernetes.dhall

let zk8s = ../dependencies/zenk8s.dhall

let Pod = (../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let project = ../../../dhall-config/project.dhall

let Deployment = ../objects/deployments/misc-process.dhall

let Annotations = ../objects/shared/annotations.dhall

let render =
      \(config : Deployment.Config.Type) ->
      \(pod : Pod.Type) ->
        let metadata =
              zk8s.Metadata.fromProject
                ( zk8s.Metadata.Role.Custom
                    { name = Deployment.Config.role config }
                )
                project

        let deployment = Deployment.render config pod

        in    [ k8s.Resource.Deployment deployment ]
            # Prelude.Optional.toList
                k8s.Resource
                ( Prelude.Optional.map
                    k8s.PodDisruptionBudget.Type
                    k8s.Resource
                    Annotations.replace
                    ( zk8s.Resource.PodDisruptionBudget.fromDeployment
                        metadata.(zk8s.Metadata.Type)
                        deployment
                    )
                )

in  { Config = Deployment.Config, render }
