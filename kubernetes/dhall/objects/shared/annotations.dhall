let k8s = ../../dependencies/dhall-kubernetes.dhall

let replace =
      \(pdb : k8s.PodDisruptionBudget.Type) ->
        k8s.Resource.PodDisruptionBudget
          ( pdb
            with metadata.annotations
                 = Some
                (toMap { `strategy.spinnaker.io/replace` = "true" })
          )

in { replace }
