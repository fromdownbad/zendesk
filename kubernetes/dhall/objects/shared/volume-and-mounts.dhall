let k8s = ../../dependencies/dhall-kubernetes.dhall

let volumes =
      [ k8s.Volume::{
        , name = "network-config"
        , configMap = Some k8s.ConfigMapVolumeSource::{ name = Some "networks" }
        }
      , k8s.Volume::{
        , name = "radar"
        , configMap = Some k8s.ConfigMapVolumeSource::{ name = Some "radar" }
        }
      , k8s.Volume::{
        , name = "attachments"
        , configMap = Some k8s.ConfigMapVolumeSource::{
          , name = Some "attachments"
          }
        }
      ]

let mounts =
      [ k8s.VolumeMount::{
        , name = "network-config"
        , mountPath = "/app/config/networks.d"
        }
      , k8s.VolumeMount::{ name = "radar", mountPath = "/app/config/radar.d" }
      , k8s.VolumeMount::{
        , name = "attachments"
        , mountPath = "/app/config/attachments.d"
        }
      ]

in  { volumes, mounts }
