let Prelude = ../../../../dhall-config/dependencies/prelude.dhall

let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let project = ../../../../dhall-config/project.dhall

let version = ../../../../dhall-config/version.dhall

let Pod/samsonDeployGroup =
      \(pod : Pod.Type) ->
        let id = Natural/show (Pod.id pod)

        let podName = Pod.name pod

        in  merge
              { Pod998 = "staging${id}"
              , Pod999 = "staging${id}"
              , Pod13 = podName
              , Pod14 = podName
              , Pod15 = podName
              , Pod17 = podName
              , Pod18 = podName
              , Pod19 = podName
              , Pod20 = podName
              , Pod23 = podName
              , Pod25 = podName
              , Pod26 = podName
              , Pod27 = podName
              }
              pod

let default =
      \(pod : Pod.Type) ->
      \(role : Text) ->
        [ k8s.EnvVar::{
          , name = "POD_NAME"
          , valueFrom = Some k8s.EnvVarSource::{
            , fieldRef = Some k8s.ObjectFieldSelector::{
              , fieldPath = "metadata.name"
              }
            }
          }
        , k8s.EnvVar::{
          , name = "POD_NAMESPACE"
          , valueFrom = Some k8s.EnvVarSource::{
            , fieldRef = Some k8s.ObjectFieldSelector::{
              , fieldPath = "metadata.namespace"
              }
            }
          }
        , k8s.EnvVar::{
          , name = "POD_IP"
          , valueFrom = Some k8s.EnvVarSource::{
            , fieldRef = Some k8s.ObjectFieldSelector::{
              , fieldPath = "status.podIP"
              }
            }
          }
        , k8s.EnvVar::{ name = "REVISION", value = Some version }
        , k8s.EnvVar::{ name = "TAG", value = Some version }
        , k8s.EnvVar::{
          , name = "DEPLOY_GROUP"
          , value = Some (Pod/samsonDeployGroup pod)
          }
        , k8s.EnvVar::{ name = "PROJECT", value = Some project.name }
        , k8s.EnvVar::{ name = "ROLE", value = Some role }
        ]

let new =
      \(name : Text) ->
      \(value : Text) ->
        k8s.EnvVar::{ name, value = Some value }

let addToContainer =
      \(envVars : List k8s.EnvVar.Type) ->
      \(container : k8s.Container.Type) ->
        Prelude.Optional.map
          (List k8s.EnvVar.Type)
          (List k8s.EnvVar.Type)
          (\(existing : List k8s.EnvVar.Type) -> existing # envVars)
          container.env

in  { default, addToContainer, new }
