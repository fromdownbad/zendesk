{-
The Service for the resque-web deployment.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Prelude = ../../../../dhall-config/dependencies/prelude.dhall

let serviceMeta
    : k8s.ObjectMeta.Type
    = k8s.ObjectMeta::{
      , name = "resque-web"
      , labels = Some
          ( toMap
              { project = "classic"
              , role = "resque-web"
              , team = "support"
              , product = "support"
              , kube-service-watcher/namespace_suffix = "false"
              }
          )
      }

let resqueWebContainerPort = (../containers/resque-web.dhall).mainPort

let targetPort =
      Prelude.Optional.map
        Text
        k8s.IntOrString
        (\(portName : Text) -> k8s.IntOrString.String portName)

let serviceSpec
    : k8s.ServiceSpec.Type
    = k8s.ServiceSpec::{
      , type = Some "ClusterIP"
      , ports = Some
        [ k8s.ServicePort::{
          , name = Some "http"
          , port = 9292
          , targetPort = targetPort resqueWebContainerPort.name
          }
        ]
      , selector = Some (toMap { project = "classic", role = "resque-web" })
      }

let render
    : k8s.Service.Type
    = k8s.Service::{ metadata = serviceMeta, spec = Some serviceSpec }

in  render
