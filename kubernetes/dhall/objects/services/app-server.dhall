{-
The Service for the app-server deployment.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Prelude = ../../../../dhall-config/dependencies/prelude.dhall

let serviceMeta
    : Text -> Text -> k8s.ObjectMeta.Type
    = \(name : Text) ->
      \(role : Text) ->
        k8s.ObjectMeta::{
        , name
        , namespace = Some "classic"
        , labels = Some
            ( toMap
                { project = "classic"
                , role
                , team = "support"
                , product = "support"
                , consul-tag/diagnostic-http = "true"
                , kube-service-watcher/namespace_suffix = "false"
                }
            )
        , annotations = Some
            (toMap { samson/persistent_fields = "metadata.labels.proxy" })
        }

let unicornContainerPort = (../containers/unicorn.dhall).mainPort

let targetPort =
      Prelude.Optional.map
        Text
        k8s.IntOrString
        (\(portName : Text) -> k8s.IntOrString.String portName)

let serviceSpec
    : Text -> k8s.ServiceSpec.Type
    = \(role : Text) ->
        k8s.ServiceSpec::{
        , type = Some "ClusterIP"
        , ports = Some
          [ k8s.ServicePort::{
            , name = Some "http"
            , port = 80
            , targetPort = targetPort unicornContainerPort.name
            }
          ]
        , selector = Some (toMap { project = "classic", role })
        }

let render
    : Text -> Text -> k8s.Service.Type
    = \(name : Text) ->
      \(role : Text) ->
        k8s.Service::{
        , metadata = serviceMeta name role
        , spec = Some (serviceSpec role)
        }

in  render
