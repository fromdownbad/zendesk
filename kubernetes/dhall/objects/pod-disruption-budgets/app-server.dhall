{-
The PodDisruptionBudget for the app-server deployment.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let pdbMeta
    : Text -> k8s.ObjectMeta.Type
    = \(role : Text) ->
        k8s.ObjectMeta::{
        , name = "classic-${role}"
        , namespace = Some "classic"
        , labels = Some
            ( toMap
                { project = "classic"
                , team = "support"
                , product = "support"
                , role
                }
            )
        }

let pdbSpec
    : Text -> k8s.PodDisruptionBudgetSpec.Type
    = \(role : Text) ->
        k8s.PodDisruptionBudgetSpec::{
        , maxUnavailable = Some (k8s.IntOrString.String "10%")
        , selector = Some k8s.LabelSelector::{
          , matchLabels = Some (toMap { project = "classic", role })
          }
        }

let render
    : Text -> k8s.PodDisruptionBudget.Type
    = \(role : Text) ->
        k8s.PodDisruptionBudget::{
        , metadata = pdbMeta role
        , spec = Some (pdbSpec role)
        }

in  render
