{-
Build the console container by supplementing the default container definition.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let render =
      \(defaultContainer : k8s.Container.Type) ->
        defaultContainer
        with command = Some [ "tail", "-f", "/dev/null" ]

in  { render }
