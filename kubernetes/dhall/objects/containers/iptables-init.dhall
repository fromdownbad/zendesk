{-
A security container to modify the iptables and reject requests to specific addresses.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let image =
      "gcr.io/docker-images-180022/apps/kube-debug-console@sha256:20ba8aa96b3b8d23faf108a95adfeb4d7fe43ea67e48df1cd2a28f8bc3ca6b6c"

let render
    : k8s.Container.Type
    = k8s.Container::{
      , name = "iptables-init"
      , image = Some image
      , command = Some
        [ "/bin/sh"
        , "-c"
        , "iptables -t filter -I OUTPUT -d 169.254.169.254 -j REJECT"
        ]
      , resources = Some k8s.ResourceRequirements::{
        , requests = Some (toMap { cpu = "100m", memory = "50Mi" })
        , limits = Some (toMap { cpu = "100m", memory = "50Mi" })
        }
      , securityContext = Some k8s.SecurityContext::{
        , capabilities = Some k8s.Capabilities::{ add = Some [ "NET_ADMIN" ] }
        }
      }

in  { render }
