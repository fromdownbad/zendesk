{-
A container to run the [raingutter](https://github.com/zendesk/raingutter) application.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let unicornContainerPort = (./unicorn.dhall).mainPort

let Env = ../shared/env.dhall

let EnvVars = ../../../../dhall-config/env-vars/package.dhall

let raingutterVersion =
      "sha256:2168d9b135527c670af4234050fbe44b33a46085b789ba42aa7509e1027f5d13"

let containerEnv =
      \(pod : Pod.Type) ->
      \(statsDNamespace : Text) ->
      \(role : Text) ->
          Env.default pod role
        # [ k8s.EnvVar::{ name = "RG_USE_SOCKET_STATS", value = Some "true" }
          , k8s.EnvVar::{
            , name = "RG_UNICORN_PORT"
            , value = Some (Natural/show unicornContainerPort.containerPort)
            }
          , k8s.EnvVar::{ name = "RG_STATSD_HOST", value = Some "169.254.1.1" }
          , k8s.EnvVar::{ name = "RG_STATSD_PORT", value = Some "8125" }
          , k8s.EnvVar::{ name = "RG_FREQUENCY", value = Some "500" }
          , k8s.EnvVar::{
            , name = "RG_STATSD_NAMESPACE"
            , value = Some "${statsDNamespace}.unicorn.raindrops.agg."
            }
          , k8s.EnvVar::{
            , name = "RG_STATSD_EXTRA_TAGS"
            , value = Some "kube_role:${role}"
            }
          , k8s.EnvVar::{
            , name = "UNICORN_WORKERS"
            , value = Some (EnvVars.asRecord pod).UNICORN_WORKERS
            }
          ]

let render =
      \(pod : Pod.Type) ->
      \(statsDNamespace : Text) ->
      \(role : Text) ->
        k8s.Container::{
        , name = "raingutter"
        , image = Some
            "gcr.io/docker-images-180022/apps/raingutter@${raingutterVersion}"
        , resources = Some k8s.ResourceRequirements::{
          , requests = Some (toMap { cpu = "100m", memory = "100M" })
          , limits = Some (toMap { cpu = "100m", memory = "100M" })
          }
        , env = Some (containerEnv pod statsDNamespace role)
        , securityContext = Some k8s.SecurityContext::{
          , runAsNonRoot = Some True
          , readOnlyRootFilesystem = Some True
          }
        }

in  { render }
