{-
Build the misc-process container by supplementing the default container definition.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Config = (../../roles/misc-process.dhall).Config

let Scale = ../../scale/package.dhall

let Scale/toResourceRequirements = ../../scale/toResourceRequirements.dhall

let render
    : Config.Type -> k8s.Container.Type -> k8s.Container.Type
    = \(config : Config.Type) ->
      \(defaultContainer : k8s.Container.Type) ->
        defaultContainer
        with name = config.name
        with command = Some ([ "bundle", "exec" ] # config.args)
        with resources = Some (Scale/toResourceRequirements config.(Scale.Type))

in  { render, Config }
