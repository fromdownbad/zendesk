{-
Build the resque web container by supplementing the default container definition.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Scale/toResourceRequirements = ../../scale/toResourceRequirements.dhall

let requirements =
      Scale/toResourceRequirements ../../scale/mail-ticket-creator.dhall

let mainPort =
      k8s.ContainerPort::{
      , name = Some "main-port"
      , containerPort = 9292
      , protocol = Some "TCP"
      }

let render =
      \(defaultContainer : k8s.Container.Type) ->
        defaultContainer
        with name = "resque-web"
        with command = Some [ "bundle", "exec", "rackup" ]
        with args = Some
          [ "--host", "0.0.0.0", "--port", "9292", "/app/resque.ru" ]
        with resources = Some requirements
        with ports = Some [ mainPort ]

in  { render, mainPort }
