{-
Build the unicorn container by supplementing the default container definition.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let EnvVars = ../shared/env.dhall

let Config = (../../roles/app-server.dhall).Config

let Scale = ../../scale/package.dhall

let Scale/toResourceRequirements = ../../scale/toResourceRequirements.dhall

let mainPort =
      k8s.ContainerPort::{
      , name = Some "main-port"
      , containerPort = 4080
      , protocol = Some "TCP"
      }

let containerEnv =
      [ EnvVars.new "MYSQL_READ_TIMEOUT" "57"
      , EnvVars.new "ENABLE_QUERY_KILLER" "true"
      ]

let render =
      \(config : Config.Type) ->
      \(defaultContainer : k8s.Container.Type) ->
        defaultContainer
        with name = "unicorn"
        with resources = Some (Scale/toResourceRequirements config.(Scale.Type))
        with env = EnvVars.addToContainer containerEnv defaultContainer
        with ports = Some [ mainPort ]
        with readinessProbe = Some k8s.Probe::{
          , httpGet = Some k8s.HTTPGetAction::{
            , path = Some "/z/ping"
            , port = k8s.IntOrString.Int mainPort.containerPort
            }
          , initialDelaySeconds = Some 60
          , timeoutSeconds = Some 10
          , successThreshold = Some 2
          , failureThreshold = Some 20
          }
        with livenessProbe = Some k8s.Probe::{
          , httpGet = Some k8s.HTTPGetAction::{
            , path = Some "/z/ping"
            , port = k8s.IntOrString.Int mainPort.containerPort
            }
          , initialDelaySeconds = Some 90
          , timeoutSeconds = Some 120
          , failureThreshold = Some 5
          }
        with lifecycle = Some k8s.Lifecycle::{
          , preStop = Some k8s.Handler::{
            , exec = Some k8s.ExecAction::{
              , command = Some [ "sleep", config.preStopSleep ]
              }
            }
          }

in  { render, mainPort }
