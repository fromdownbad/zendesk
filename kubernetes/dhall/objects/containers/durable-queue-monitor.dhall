let k8s = ../../dependencies/dhall-kubernetes.dhall

let Scale/toResourceRequirements = ../../scale/toResourceRequirements.dhall

let requirements =
      Scale/toResourceRequirements ../../scale/durable-queue-monitor.dhall

let render =
      \(defaultContainer : k8s.Container.Type) ->
        defaultContainer
        with name = "durable-queue-monitor"
        with command = Some [ "bundle", "exec", "script/durable_queue_monitor" ]
        with resources = Some requirements

in  { render }
