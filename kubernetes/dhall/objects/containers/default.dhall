{-
Build the base container for any Classic deployment. Never use this directly, but use it as a base
to build the desired container.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let image = ../../docker-image.dhall

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let Env = ../shared/env.dhall

let version = ../../../../dhall-config/version.dhall

let render =
      \(pod : Pod.Type) ->
      \(role : Text) ->
        k8s.Container::{
        , name = "default"
        , image = Some image
        , resources = Some k8s.ResourceRequirements::{
          , requests = Some (toMap { cpu = "0.2", memory = "1Gi" })
          , limits = Some (toMap { cpu = "2.0", memory = "4Gi" })
          }
        , volumeMounts = Some volumeAndMounts.mounts
        , env = Some (Env.default pod role)
        , envFrom = Some
          [ k8s.EnvFromSource::{
            , configMapRef = Some k8s.ConfigMapEnvSource::{
              , name = Some "classic-${Pod.name pod}-env-${version}"
              }
            }
          ]
        }

in  { render }
