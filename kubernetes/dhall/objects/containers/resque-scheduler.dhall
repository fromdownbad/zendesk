{-
Build the resque scheduler container by supplementing the default container definition.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let EnvVars = ../shared/env.dhall

let Scale/toResourceRequirements = ../../scale/toResourceRequirements.dhall

let requirements =
      Scale/toResourceRequirements ../../scale/resque-scheduler.dhall

let containerEnv =
      [ EnvVars.new
          "RESQUE_SCHEDULER_MASTER_LOCK_PREFIX"
          "classic-resque-scheduler:"
      ]

let container
    : k8s.Container.Type -> k8s.Container.Type
    = \(defaultContainer : k8s.Container.Type) ->
        defaultContainer
        with name = "resque-scheduler"
        with command = Some [ "bundle", "exec", "script/resque_scheduler" ]
        with resources = Some requirements
        with env = EnvVars.addToContainer containerEnv defaultContainer

in  container
