{-
Build the mail-ticket-creator container by supplementing the default container definition.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Scale/toResourceRequirements = ../../scale/toResourceRequirements.dhall

let requirements =
      Scale/toResourceRequirements ../../scale/mail-ticket-creator.dhall

let render =
      \(defaultContainer : k8s.Container.Type) ->
        defaultContainer
        with name = "classic-mail-ticket-creator"
        with command = Some [ "ruby", "script/mail_ticket_creator.rb" ]
        with resources = Some requirements
        with livenessProbe = Some k8s.Probe::{
          , exec = Some k8s.ExecAction::{
            , command = Some [ "script/kubernetes_mtc_liveness" ]
            }
          , initialDelaySeconds = Some 3600
          , periodSeconds = Some 3600
          , timeoutSeconds = Some 30
          , successThreshold = Some 1
          , failureThreshold = Some 1
          }

in  { render }
