{-
Build the worker container by supplementing the default container definition.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let EnvVars = ../shared/env.dhall

let Config = (../../roles/worker.dhall).Config

let Scale = ../../scale/package.dhall

let Scale/toResourceRequirements = ../../scale/toResourceRequirements.dhall

let containerEnv =
      \(config : Config.Type) ->
        [ EnvVars.new "GRACEFUL_TERM" "true", EnvVars.new "QUEUE" config.queue ]

let render =
      \(config : Config.Type) ->
      \(defaultContainer : k8s.Container.Type) ->
        defaultContainer
        with name = Config.role config
        with command = Some
          [ "bundle", "exec", "bin/resque-pool", "--term-graceful-wait" ]
        with env = EnvVars.addToContainer (containerEnv config) defaultContainer
        with resources = Some (Scale/toResourceRequirements config.(Scale.Type))
        with livenessProbe = Some k8s.Probe::{
          , exec = Some k8s.ExecAction::{
            , command = Some [ "script/kubernetes_resque_liveness" ]
            }
          , initialDelaySeconds = Some 3600
          , periodSeconds = Some 3600
          , timeoutSeconds = Some 30
          , successThreshold = Some 1
          , failureThreshold = Some 1
          }
        with lifecycle = Some k8s.Lifecycle::{
          , preStop = Some k8s.Handler::{
            , exec = Some k8s.ExecAction::{
              , command = Some [ "script/kubernetes_resque_prestop" ]
              }
            }
          }

in  { render }
