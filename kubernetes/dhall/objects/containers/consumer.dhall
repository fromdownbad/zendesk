{-
Build the console container by supplementing the default container definition.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let EnvVars = ../shared/env.dhall

let Config = (../../roles/consumer.dhall).Config

let Scale = ../../scale/package.dhall

let Scale/toResourceRequirements = ../../scale/toResourceRequirements.dhall

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let containerEnv =
      [ EnvVars.new "GRACEFUL_TERM" "true"
      , EnvVars.new "QUEUE" "automations"
      , EnvVars.new "ZENDESK_CONFIG_FROM_ENV" "all"
      , EnvVars.new "PORT" "5000"
      , EnvVars.new "RACECAR_MAX_BYTES" "1048576"
      , EnvVars.new "RACECAR_SESSION_TIMEOUT" "600"
      ]

let render =
      \(config : Config.Type) ->
      \(defaultContainer : k8s.Container.Type) ->
        defaultContainer
        with name = "classic-consumer"
        with command = Some
          [ "bundle", "exec", "racecar", config.consumerClass ]
        with resources = Some (Scale/toResourceRequirements config.(Scale.Type))
        with env = EnvVars.addToContainer containerEnv defaultContainer
        with securityContext = Some k8s.SecurityContext::{
          , readOnlyRootFilesystem = Some True
          }
        with volumeMounts = Some
            (   [ k8s.VolumeMount::{ name = "app-tmp", mountPath = "/app/tmp" }
                ]
              # volumeAndMounts.mounts
            )

in  { render }
