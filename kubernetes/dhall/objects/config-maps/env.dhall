let Prelude = ../../../../dhall-config/dependencies/prelude.dhall

let Map = Prelude.Map

let z = ../../../../dhall-config/dependencies/zendesk-dhall.dhall

let Pod = z.Pod

let k8s = ../../dependencies/dhall-kubernetes.dhall

let EnvVars = ../../../../dhall-config/env-vars/package.dhall

let version = ../../../../dhall-config/version.dhall

let configMap
    : Pod.Type -> Map.Type Text Text -> k8s.ConfigMap.Type
    = \(pod : Pod.Type) ->
      \(envVars : Map.Type Text Text) ->
        k8s.ConfigMap::{
        , metadata = k8s.ObjectMeta::{
          , name = "classic-${Pod.name pod}-env-${version}"
          , namespace = Some "classic"
          , labels = Some
              ( toMap
                  { project = "classic"
                  , role = "env"
                  , team = "support"
                  , product = "support"
                  }
              )
          , annotations = Some (toMap { samson/prerequisite = "true" })
          }
        , data = Some envVars
        }

let fromPod
    : Pod.Type -> k8s.ConfigMap.Type
    = \(pod : Pod.Type) -> configMap pod (EnvVars.asMap pod)

in  { fromPod }
