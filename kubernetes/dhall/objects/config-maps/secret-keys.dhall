let Prelude = ../../../../dhall-config/dependencies/prelude.dhall

let Map = Prelude.Map

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let k8s = ../../dependencies/dhall-kubernetes.dhall

let Secrets = ../../../../dhall-config/secrets/package.dhall

let version = ../../../../dhall-config/version.dhall

let data
    : Secrets.Vault.Environment -> Text
    = \(env : Secrets.Vault.Environment) ->
        Prelude.Text.concatSep
          "\n"
          ( Prelude.List.map
              (Map.Entry Text Text)
              Text
              ( \(item : Map.Entry Text Text) ->
                  "${item.mapKey}=\"${item.mapValue}\""
              )
              (Secrets.asMap env)
          )

let render =
      \(pod : Pod.Type) ->
        let env =
              if    Pod.isProduction pod
              then  Secrets.Vault.Environment.Production
              else  Secrets.Vault.Environment.Staging

        in  k8s.ConfigMap::{
            , metadata = k8s.ObjectMeta::{
              , name = "classic-secret-keys-${version}"
              , namespace = Some "classic"
              , labels = Some
                  ( toMap
                      { project = "classic"
                      , role = "secret-keys"
                      , team = "support"
                      , product = "support"
                      }
                  )
              , annotations = Some (toMap { samson/prerequisite = "true" })
              }
            , data = Some (toMap { annotations = data env })
            }

in  { render }
