{-
Build up the consumer deployment object. Define the container, secrets, and environment variables.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let zk8s = ../../dependencies/zenk8s.dhall

let iptablesInitContainer = ../containers/iptables-init.dhall

let defaultContainer = ../containers/default.dhall

let consumerContainer = ../containers/consumer.dhall

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let podAnnotations =
      toMap { container-iptables-init-samson/dockerfile = "none" }

let Config = (../../roles/consumer.dhall).Config

let labelsFromConfig =
      \(config : Config.Type) ->
        { project = "classic"
        , role = config.name ++ "-consumer"
        , team = config.owner
        , product = "support"
        , role_group = "kafka-consumer"
        }

let podSpec
    : Config.Type -> Pod.Type -> k8s.PodSpec.Type
    = \(config : Config.Type) ->
      \(pod : Pod.Type) ->
        let role = Config.name config

        in  k8s.PodSpec::{
            , initContainers = Some [ iptablesInitContainer.render ]
            , containers =
              [ consumerContainer.render
                  config
                  (defaultContainer.render pod role)
              ]
            , volumes = Some
                (   [ k8s.Volume::{
                      , name = "app-tmp"
                      , emptyDir = Some k8s.EmptyDirVolumeSource::{=}
                      }
                    ]
                  # volumeAndMounts.volumes
                )
            }

let render
    : Config.Type -> Pod.Type -> k8s.Deployment.Type
    = \(config : Config.Type) ->
      \(pod : Pod.Type) ->
        let labels = labelsFromConfig config

        in  zk8s.Resource.Deployment.withSpinnakerReplaceStrategy
              ( zk8s.Resource.Deployment.withSecrets
                  "classic-secret-keys-${../../../../dhall-config/version.dhall}"
                  k8s.Deployment::{
                  , metadata = k8s.ObjectMeta::{
                    , name = "classic-${config.name}-consumer"
                    , namespace = Some "classic"
                    , labels = Some (toMap labels)
                    , annotations = Some
                        ( toMap
                            { samson/set_via_env_json =
                                ''
                                spec.template.metadata.labels.temp-auth: K8S_TEMP_AUTH_LABEL
                                ''
                            }
                        )
                    }
                  , spec = Some k8s.DeploymentSpec::{
                    , strategy = Some k8s.DeploymentStrategy::{
                      , type = Some "Recreate"
                      }
                    , selector = k8s.LabelSelector::{
                      , matchLabels = Some
                          ( toMap
                              { project = "classic"
                              , role = config.name ++ "-consumer"
                              }
                          )
                      }
                    , template = k8s.PodTemplateSpec::{
                      , metadata = k8s.ObjectMeta::{
                        , name = "classic-${config.name}-consumer"
                        , labels = Some
                            (toMap (labels with temp-auth = "disabled"))
                        , annotations = Some podAnnotations
                        }
                      , spec = Some (podSpec config pod)
                      }
                    }
                  }
              )

in  { Config, render }
