{-
Build up the app-server deployment object. Bring in the desired containers, shared secrets and
environment variables, and set the correct metadata.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let zk8s = ../../dependencies/zenk8s.dhall

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let iptablesInitContainer = ../containers/iptables-init.dhall

let unicornContainer = ../containers/unicorn.dhall

let raingutterContainer = ../containers/raingutter.dhall

let defaultContainer = ../containers/default.dhall

let Config = (../../roles/app-server.dhall).Config

let podAnnotations =
      toMap
        { container-raingutter-samson/dockerfile = "none"
        , container-raingutter-samson/preStop = "disabled"
        , container-iptables-init-samson/dockerfile = "none"
        , `sidecar.istio.io/proxyCPU` = "500m"
        , `sidecar.istio.io/proxyCPULimit` = "500m"
        , `sidecar.istio.io/proxyMemory` = "1Gi"
        , `sidecar.istio.io/proxyMemoryLimit` = "1Gi"
        }

let podMeta
    : Text -> k8s.ObjectMeta.Type
    = \(role : Text) ->
        k8s.ObjectMeta::{
        , name = "classic-${role}"
        , labels = Some
            ( toMap
                { project = "classic"
                , role
                , team = "support"
                , product = "support"
                , role_group = "app-server"
                , temp-auth = "disabled"
                }
            )
        , annotations = Some podAnnotations
        }

let podSpec
    : Config.Type -> Pod.Type -> k8s.PodSpec.Type
    = \(config : Config.Type) ->
      \(pod : Pod.Type) ->
        let statsDNamespace =
              if config.isPriority then "classic_priority" else "zendesk"
        in  k8s.PodSpec::{
            , initContainers = Some [ iptablesInitContainer.render ]
            , containers =
              [ unicornContainer.render config (defaultContainer.render pod config.role)
              , raingutterContainer.render pod statsDNamespace config.role
              ]
            , volumes = Some volumeAndMounts.volumes
            }

let deploymentMeta
    : Text -> k8s.ObjectMeta.Type
    = \(role : Text) ->
        k8s.ObjectMeta::{
        , name = "classic-${role}"
        , namespace = Some "classic"
        , labels = Some
            ( toMap
                { project = "classic"
                , role
                , team = "support"
                , product = "support"
                , role_group = "app-server"
                }
            )
        }

let deploymentStrategy =
      k8s.DeploymentStrategy::{
      , rollingUpdate = Some k8s.RollingUpdateDeployment::{
        , maxUnavailable = Some (k8s.IntOrString.String "10%")
        , maxSurge = Some (k8s.IntOrString.Int 6)
        }
      }

let render
    : Config.Type -> Pod.Type -> k8s.Deployment.Type
    = \(config : Config.Type) ->
      \(pod : Pod.Type) ->
        zk8s.Resource.Deployment.withSpinnakerReplaceStrategy
          ( zk8s.Resource.Deployment.withSecrets
              "classic-secret-keys-${../../../../dhall-config/version.dhall}"
              k8s.Deployment::{
              , metadata = deploymentMeta config.role
              , spec = Some k8s.DeploymentSpec::{
                , replicas = config.replicas
                , selector = k8s.LabelSelector::{
                  , matchLabels = Some
                      (toMap { project = "classic", role = config.role })
                  }
                , template = k8s.PodTemplateSpec::{
                  , metadata = podMeta config.role
                  , spec = Some (podSpec config pod)
                  }
                , strategy = Some deploymentStrategy
                }
              }
          )

in  { render }
