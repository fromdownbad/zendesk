{-
Build up the misc-process deployment object. Define the container, secrets, and environment variables.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let zk8s = ../../dependencies/zenk8s.dhall

let iptablesInitContainer = ../containers/iptables-init.dhall

let defaultContainer = ../containers/default.dhall

let miscContainer = ../containers/misc-process.dhall

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let Config = (../../roles/misc-process.dhall).Config

let podAnnotations =
      toMap { container-iptables-init-samson/dockerfile = "none" }

let labelsFromConfig =
      \(config : Config.Type) ->
        { project = "classic"
        , role = config.name
        , team = "support"
        , product = "support"
        , role_group = "singleton"
        }

let podSpec =
      \(config : Config.Type) ->
      \(pod : Pod.Type) ->
        k8s.PodSpec::{
        , initContainers = Some [ iptablesInitContainer.render ]
        , containers =
          [ miscContainer.render
              config
              (defaultContainer.render pod config.name)
          ]
        , volumes = Some volumeAndMounts.volumes
        }

let render =
      \(config : Config.Type) ->
      \(pod : Pod.Type) ->
        let labels = labelsFromConfig config

        in  zk8s.Resource.Deployment.withSpinnakerReplaceStrategy
              ( zk8s.Resource.Deployment.withSecrets
                  "classic-secret-keys-${../../../../dhall-config/version.dhall}"
                  k8s.Deployment::{
                  , metadata = k8s.ObjectMeta::{
                    , name = "classic-${config.name}"
                    , namespace = Some "classic"
                    , labels = Some (toMap labels)
                    , annotations = Some
                        ( toMap
                            { samson/set_via_env_json =
                                ''
                                spec.template.metadata.labels.temp-auth: K8S_TEMP_AUTH_LABEL
                                ''
                            , samson/minAvailable = "1"
                            }
                        )
                    }
                  , spec = Some k8s.DeploymentSpec::{
                    , replicas = config.replicas
                    , selector = k8s.LabelSelector::{
                      , matchLabels = Some
                          (toMap { project = "classic", role = config.name })
                      }
                    , template = k8s.PodTemplateSpec::{
                      , metadata = k8s.ObjectMeta::{
                        , name = "classic-${config.name}"
                        , labels = Some
                            (toMap (labels /\ { temp-auth = "disabled" }))
                        , annotations = Some podAnnotations
                        }
                      , spec = Some (podSpec config pod)
                      }
                    }
                  }
              )

in  { Config, render }
