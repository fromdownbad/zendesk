{-
Build up the console deployment object. Define the container, secrets, and environment variables.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let zk8s = ../../dependencies/zenk8s.dhall

let defaultContainer = ../containers/default.dhall

let consoleContainer = ../containers/console.dhall

let podAnnotations =
      toMap { container-iptables-init-samson/dockerfile = "none" }

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let role = "console"

let podMeta
    : k8s.ObjectMeta.Type
    = k8s.ObjectMeta::{
      , name = "classic-console"
      , labels = Some
          ( toMap
              { project = "classic"
              , role = "console"
              , team = "support"
              , product = "support"
              , temp-auth = "disabled"
              }
          )
      , annotations = Some podAnnotations
      }

let deploymentMeta
    : k8s.ObjectMeta.Type
    = k8s.ObjectMeta::{
      , name = "classic-console"
      , namespace = Some "classic"
      , labels = Some
          ( toMap
              { project = "classic"
              , role = "console"
              , team = "support"
              , product = "support"
              }
          )
      , annotations = Some
          ( toMap
              { samson/set_via_env_json =
                  ''
                    spec.template.metadata.labels.temp-auth: K8S_TEMP_AUTH_LABEL
                  ''
              }
          )
      }

let render
    : Pod.Type -> k8s.Deployment.Type
    = \(pod : Pod.Type) ->
      zk8s.Resource.Deployment.withSpinnakerReplaceStrategy (
        zk8s.Resource.Deployment.withSecrets
          "classic-secret-keys-${../../../../dhall-config/version.dhall}"
          k8s.Deployment::{
          , metadata = deploymentMeta
          , spec = Some k8s.DeploymentSpec::{
            , replicas = Some 0
            , selector = k8s.LabelSelector::{
              , matchLabels = Some
                  (toMap { project = "classic", role = "console" })
              }
            , template = k8s.PodTemplateSpec::{
              , metadata = podMeta
              , spec = Some k8s.PodSpec::{
                , containers =
                  [ consoleContainer.render (defaultContainer.render pod role) ]
                , volumes = Some volumeAndMounts.volumes
                }
              }
            }
          }
      )

in  { render }
