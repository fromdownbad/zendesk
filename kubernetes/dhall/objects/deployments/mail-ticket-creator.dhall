let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let zk8s = ../../dependencies/zenk8s.dhall

let podAnnotations =
      toMap { container-iptables-init-samson/dockerfile = "none" }

let iptablesInitContainer = ../containers/iptables-init.dhall

let defaultContainer = ../containers/default.dhall

let mailTicketCreatorContainer = ../containers/mail-ticket-creator.dhall

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let scale = ../../scale/mail-ticket-creator.dhall

let podSpec =
      \(pod : Pod.Type) ->
        k8s.PodSpec::{
        , containers =
          [ mailTicketCreatorContainer.render (defaultContainer.render pod "mail-ticket-creator") ]
        , initContainers = Some [ iptablesInitContainer.render ]
        , volumes = Some volumeAndMounts.volumes
        }

let render =
      \(pod : Pod.Type) ->
        zk8s.Resource.Deployment.withSpinnakerReplaceStrategy
          ( zk8s.Resource.Deployment.withSecrets
              "classic-secret-keys-${../../../../dhall-config/version.dhall}"
              k8s.Deployment::{
              , metadata = k8s.ObjectMeta::{
                , name = "classic-mail-ticket-creator"
                , namespace = Some "classic"
                , labels = Some
                    ( toMap
                        { project = "classic"
                        , role = "mail-ticket-creator"
                        , team = "support"
                        , product = "support"
                        }
                    )
                , annotations = Some
                    ( toMap
                        { samson/set_via_env_json =
                            ''
                            spec.template.metadata.labels.temp-auth: K8S_TEMP_AUTH_LABEL
                            ''
                        }
                    )
                }
              , spec = Some k8s.DeploymentSpec::{
                , template = k8s.PodTemplateSpec::{
                  , metadata = k8s.ObjectMeta::{
                    , name = "classic-mail-ticket-creator"
                    , labels = Some
                        ( toMap
                            { project = "classic"
                            , role = "mail-ticket-creator"
                            , team = "support"
                            , product = "support"
                            , temp-auth = "disabled"
                            }
                        )
                    , annotations = Some podAnnotations
                    }
                  , spec = Some (podSpec pod)
                  }
                , selector = k8s.LabelSelector::{
                  , matchLabels = Some
                      ( toMap
                          { project = "classic", role = "mail-ticket-creator" }
                      )
                  }
                , replicas = scale.replicas
                }
              }
          )

in  { render }
