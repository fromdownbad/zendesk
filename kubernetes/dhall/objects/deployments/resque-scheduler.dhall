let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let zk8s = ../../dependencies/zenk8s.dhall

let scale = ../../scale/resque-scheduler.dhall

let podAnnotations =
      toMap { container-iptables-init-samson/dockerfile = "none" }

let iptablesInitContainer = ../containers/iptables-init.dhall

let defaultContainer = ../containers/default.dhall

let resqueSchedulerContainer = ../containers/resque-scheduler.dhall

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let podSpec =
      \(pod : Pod.Type) ->
        k8s.PodSpec::{
        , containers =
          [ resqueSchedulerContainer (defaultContainer.render pod "resque-scheduler") ]
        , initContainers = Some [ iptablesInitContainer.render ]
        , volumes = Some volumeAndMounts.volumes
        }

let render =
      \(pod : Pod.Type) ->
        zk8s.Resource.Deployment.withSpinnakerReplaceStrategy
          ( zk8s.Resource.Deployment.withSecrets
              "classic-secret-keys-${../../../../dhall-config/version.dhall}"
              k8s.Deployment::{
              , metadata = k8s.ObjectMeta::{
                , name = "classic-resque-scheduler"
                , namespace = Some "classic"
                , labels = Some
                    ( toMap
                        { project = "classic"
                        , role = "resque-scheduler"
                        , team = "support"
                        , product = "support"
                        , role_group = "singleton"
                        }
                    )
                , annotations = Some
                    ( toMap
                        { samson/set_via_env_json =
                            "spec.template.metadata.labels.temp-auth: K8S_TEMP_AUTH_LABEL"
                        }
                    )
                }
              , spec = Some k8s.DeploymentSpec::{
                , minReadySeconds = Some 30
                , template = k8s.PodTemplateSpec::{
                  , metadata = k8s.ObjectMeta::{
                    , name = "classic-resque-scheduler"
                    , labels = Some
                        ( toMap
                            { project = "classic"
                            , role = "resque-scheduler"
                            , team = "support"
                            , product = "support"
                            , role_group = "singleton"
                            , temp-auth = "disabled"
                            }
                        )
                    , annotations = Some podAnnotations
                    }
                  , spec = Some (podSpec pod)
                  }
                , selector = k8s.LabelSelector::{
                  , matchLabels = Some
                      (toMap { project = "classic", role = "resque-scheduler" })
                  }
                , replicas = scale.replicas
                , strategy = Some k8s.DeploymentStrategy::{
                  , rollingUpdate = Some k8s.RollingUpdateDeployment::{
                    , maxUnavailable = Some (k8s.IntOrString.Int 1)
                    , maxSurge = Some (k8s.IntOrString.String "100%")
                    }
                  }
                }
              }
          )

in  { render }
