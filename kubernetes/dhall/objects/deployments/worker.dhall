let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let zk8s = ../../dependencies/zenk8s.dhall

let podAnnotations =
      toMap { container-iptables-init-samson/dockerfile = "none" }

let iptablesInitContainer = ../containers/iptables-init.dhall

let defaultContainer = ../containers/default.dhall

let workerContainer = ../containers/worker.dhall

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let Config = (../../roles/worker.dhall).Config

let podSpec
    : Config.Type -> Pod.Type -> k8s.PodSpec.Type
    = \(config : Config.Type) ->
      \(pod : Pod.Type) ->
        let role = Config.name config

        in  k8s.PodSpec::{
            , containers =
              [ workerContainer.render config (defaultContainer.render pod role)
              ]
            , initContainers = Some [ iptablesInitContainer.render ]
            , volumes = Some volumeAndMounts.volumes
            }

let render
    : Config.Type -> Pod.Type -> k8s.Deployment.Type
    = \(config : Config.Type) ->
      \(pod : Pod.Type) ->
        let name = Config.name config

        in  zk8s.Resource.Deployment.withSpinnakerReplaceStrategy
              ( zk8s.Resource.Deployment.withSecrets
                  "classic-secret-keys-${../../../../dhall-config/version.dhall}"
                  k8s.Deployment::{
                  , metadata = k8s.ObjectMeta::{
                    , name = Config.role config
                    , namespace = Some "classic"
                    , labels = Some
                        ( toMap
                            { project = "classic"
                            , role = name
                            , team = "support"
                            , product = "support"
                            , role_group = "resque-worker"
                            }
                        )
                    , annotations = Some
                        ( toMap
                            { samson/set_via_env_json =
                                ''
                                spec.template.metadata.labels.temp-auth: K8S_TEMP_AUTH_LABEL
                                ''
                            }
                        )
                    }
                  , spec = Some k8s.DeploymentSpec::{
                    , template = k8s.PodTemplateSpec::{
                      , metadata = k8s.ObjectMeta::{
                        , name = Config.role config
                        , labels = Some
                            ( toMap
                                { project = "classic"
                                , role = name
                                , team = "support"
                                , product = "support"
                                , role_group = "resque-worker"
                                , temp-auth = "disabled"
                                }
                            )
                        , annotations = Some podAnnotations
                        }
                      , spec = Some (podSpec config pod)
                      }
                    , selector = k8s.LabelSelector::{
                      , matchLabels = Some
                          (toMap { project = "classic", role = name })
                      }
                    , replicas = config.replicas
                    , strategy = Some k8s.DeploymentStrategy::{
                      , rollingUpdate = Some k8s.RollingUpdateDeployment::{
                        , maxUnavailable = Some (k8s.IntOrString.String "30%")
                        , maxSurge = Some (k8s.IntOrString.String "100%")
                        }
                      }
                    }
                  }
              )

in  { render, Config }
