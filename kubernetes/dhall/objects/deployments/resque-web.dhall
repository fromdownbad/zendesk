{-
Build up the resque-web deployment object. Bring in the desired containers, shared secrets and
environment variables, and set the correct metadata.
-}
let k8s = ../../dependencies/dhall-kubernetes.dhall

let Pod = (../../../../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let zk8s = ../../dependencies/zenk8s.dhall

let defaultContainer = ../containers/default.dhall

let iptablesInitContainer = ../containers/iptables-init.dhall

let resqueWebContainer = ../containers/resque-web.dhall

let volumeAndMounts = ../shared/volume-and-mounts.dhall

let scale = ../../scale/resque-web.dhall

let podAnnotations =
      toMap { container-iptables-init-samson/dockerfile = "none" }

let podMeta
    : k8s.ObjectMeta.Type
    = k8s.ObjectMeta::{
      , name = "classic-resque-web"
      , labels = Some
          ( toMap
              { project = "classic"
              , role = "resque-web"
              , team = "support"
              , product = "support"
              , role_group = "singleton"
              , temp-auth = "disabled"
              }
          )
      , annotations = Some podAnnotations
      }

let podSpec =
      \(pod : Pod.Type) ->
        k8s.PodSpec::{
        , initContainers = Some [ iptablesInitContainer.render ]
        , containers =
          [ resqueWebContainer.render (defaultContainer.render pod "resque-web") ]
        , volumes = Some volumeAndMounts.volumes
        }

let deploymentMeta
    : Pod.Type -> k8s.ObjectMeta.Type
    = \(pod : Pod.Type) ->
        k8s.ObjectMeta::{
        , name = "classic-resque-web"
        , namespace = Some (Pod.name pod)
        , labels = Some
            ( toMap
                { project = "classic"
                , role = "resque-web"
                , team = "support"
                , product = "support"
                , role_group = "singleton"
                }
            )
        , annotations = Some (toMap { samson/minAvailable = "1" })
        }

let render =
      \(pod : Pod.Type) ->
        zk8s.Resource.Deployment.withSpinnakerReplaceStrategy
          ( zk8s.Resource.Deployment.withSecrets
              "classic-secret-keys-${../../../../dhall-config/version.dhall}"
              k8s.Deployment::{
              , metadata = deploymentMeta pod
              , spec = Some k8s.DeploymentSpec::{
                , replicas = scale.replicas
                , selector = k8s.LabelSelector::{
                  , matchLabels = Some
                      (toMap { project = "classic", role = "resque-web" })
                  }
                , template = k8s.PodTemplateSpec::{
                  , metadata = podMeta
                  , spec = Some (podSpec pod)
                  }
                }
              }
          )

in  { render }
