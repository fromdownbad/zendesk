# Classic + Kubernetes + Dhall

Welcome to the place where all of our Kubernetes manifests are generated using [Dhall](https://dhall-lang.org/).

Before getting too far into this code, a basic understanding of Dhall is required. A quick resource
for getting started is [Learn Dhall in Y minutes](https://learnxinyminutes.com/docs/dhall/).

## Development

```bash
./dhall-render
```

This will output a whole lot of k8s YAML documents into the `./generated/` directory!

## Why Dhall?

There are many reasons Dhall is being used, and those are all well explained in two resources:

1. The [ADR for Dhall in Support](https://techmenu.zende.sk/adrs/use-dhall-for-configuration-in-support/)
1. The [Dhall landing page](https://dhall-lang.org/)

## Terminology

### `dependency`

Dependencies within the Kubernetes Dhall scope specifically refer to remote imports.

### `object`

This is a Kubernetes concept, a [Kubernetes Object](https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/).

In the case of Classic, the most common resources found are

- `Deployment`
- `PodDisruptionBudget`
- `Service`

## Directory Structure

There are a number of directories that have specific meanings.

### `dependencies`

This directory contains all of the remote imports. Specifically, this is where the versioned imports
of the tools, libraries, and configuration we consume are declared. This directory uses frozen
and cached imports, to ensure the version, code, or config never changes out from underneath us.

### `generated`

This is a helper directory for `dhall-render`. Do not use this directory.

### `objects`

This is where all of the building blocks are defined. In a set of subdirectories we have each type
of resource defined and ready to be reused many times. For example, we define the base container
that references our docker image here. We then reuse that container to create all of the
role-specific containers where we change small things like the command being executed.

### `manifests`

A manifest can be either a single kubernetes `object` or a collection of objects all supporting a
single piece of functionality or the domain. For example, the `app-server` manifest consists of a
`Deployment`, `PodDisruptionBudget`, and a `Service`. In contrast, the `console` manifest only consists
of a `Deployment`.

It is most common to have all objects for a given manifest rendered into a single, multi-document YAML
file.

### `roles`

There are many `manifests` that have a very similar structure in Classic. With some
parameterization, we can generate variations of the same structure with only a few additonal
lines of code. The predominant roles for Classic are: `AppServer`, `Consumer`, `MiscProcess`,
and `Worker`.

### `zenk8s`

We've got some [custom k8s resources](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) in use at Zendesk! The schemas for these are intentionally loose, so we tightened them up for the resources we use. There's also some Samson-specific schema overrides that are included here.
