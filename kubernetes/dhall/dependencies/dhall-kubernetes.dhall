{-
This file must be re-frozen when `slow-dhall-kubernetes.dhall` gets updated.
```
dhall freeze --all --inplace ./dhall-kubernetes.dhall
```
This exists as a way to freeze the results from slow-dhall-kubernetes in a single place. It saves
about 0.5-1s of render time.
-}
./slow-dhall-kubernetes.dhall sha256:249953749b3f3862b2e32c9fb57e8156cef07577be2b070f7fe2b00ddac4840e
