let all =
      https://raw.githubusercontent.com/zendesk/zendesk-dhall/41df2ebb897470aea6cbfb61977a90e9fdfbbc90/ZenK8s/package.dhall using (../../../dhall-config/dependencies/github-headers.dhall) sha256:a0ba6a07e08dcbd8aeb94568f7b9c0911ce0f1cfc4200ab385afef7b7d441d49

let selection =
          all.{ Metadata }
      /\  { Resource.Deployment = all.Resource.Deployment
          , Resource.PodDisruptionBudget = all.Resource.PodDisruptionBudget
          }

in  selection
