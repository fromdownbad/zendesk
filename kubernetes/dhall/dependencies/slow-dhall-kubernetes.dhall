{-
IF YOU UPDATE THIS FILE, REMEMBER TO REFREEZE `dhall-kubernetes.dhall` using
```
dhall freeze --all --inplace ./dhall-kubernetes.dhall
```
We use record projection here to drastically reduce the kubernetes object that gets imported. This
helps with render speed.
We create our own k8s.Resource union here because the one generated in dhall-kubernetes is huge and
causes tremendous performance hits.
See https://github.com/dhall-lang/dhall-haskell/issues/1890#issuecomment-725758728 for an explanation
on why we're manipulating the dhall-kubernetes input.
-}
let kubernetes =
        https://raw.githubusercontent.com/dhall-lang/dhall-kubernetes/v4.0.0/package.dhall sha256:d9eac5668d5ed9cb3364c0a39721d4694e4247dad16d8a82827e4619ee1d6188
      ? https://raw.githubusercontent.com/dhall-lang/dhall-kubernetes/v4.0.0/package.dhall

let selection =
      kubernetes.{ Capabilities
                 , ConfigMap
                 , ConfigMapEnvSource
                 , ConfigMapVolumeSource
                 , Container
                 , ContainerPort
                 , CrossVersionObjectReference
                 , Deployment
                 , DeploymentSpec
                 , DeploymentStrategy
                 , DownwardAPIVolumeFile
                 , DownwardAPIVolumeSource
                 , EmptyDirVolumeSource
                 , EnvFromSource
                 , EnvVar
                 , EnvVarSource
                 , ExecAction
                 , ExternalMetricSource
                 , HTTPGetAction
                 , Handler
                 , HorizontalPodAutoscaler
                 , HorizontalPodAutoscalerSpec
                 , HorizontalPodAutoscalerStatus
                 , IntOrString
                 , LabelSelector
                 , Lifecycle
                 , LocalObjectReference
                 , MetricIdentifier
                 , MetricSpec
                 , MetricTarget
                 , ObjectFieldSelector
                 , ObjectMeta
                 , ObjectMetricSource
                 , PodDisruptionBudget
                 , PodDisruptionBudgetSpec
                 , PodSpec
                 , PodTemplateSpec
                 , PodsMetricSource
                 , Probe
                 , ResourceMetricSource
                 , ResourceRequirements
                 , RollingUpdateDeployment
                 , SecretVolumeSource
                 , SecurityContext
                 , Service
                 , ServicePort
                 , ServiceSpec
                 , Volume
                 , VolumeMount
                 }

let ResourceOverride =
      < Deployment : selection.Deployment.Type
      | HorizontalPodAutoscaler : selection.HorizontalPodAutoscaler.Type
      | Service : selection.Service.Type
      | PodDisruptionBudget : selection.PodDisruptionBudget.Type
      | ConfigMap : selection.ConfigMap.Type
      >

in  selection /\ { Resource = ResourceOverride }
