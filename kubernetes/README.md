# Kubernetes Role Configuration

There are two main parts to building our Kubernetes role configurations:

1. Role Templates
1. Role Definitions

## Role Templates

Each functionally different role should be represented by a template. For example, right now we have
a template for the processes the run on misc servers because they all share the exact same config
with the exception of the command that runs. We also have a template for our resque workers, because
every role will be the same with the exception of the `QUEUE` environment variable value. Currently,
we use templates even for roles which are only used once (like the console role).

### Adding a Template

Templates are ERB-templated YAML files. In other words, they should be named `{template}.yml.erb`.
It is probably easiest to start with an existing template, and then look for all the ERB usages,
i.e. look for `<% ... %>` parts of the file, and then make the changes you need.

We parse our templates with [ERB's `-` mode](https://ruby-doc.org/stdlib-2.4.6/libdoc/erb/rdoc/ERB.html#method-c-new),
so be sure to use `<%- ... -%>` tags for ruby that doesn't produce a result in the YAML file, like
a control statement.

**Example**

Assume we have a variable `collection` that contains an array of strings, `["foo", "bar"]`.

The following YAML:

```yaml
some_list:
  <%- collection.each do |item| -%>
  - <%= item %>
  <%- end -%>
other_key: value
```

will produce nice output:

```yaml
some_list:
  - foo
  - bar
other_key: value
```

If we neglect the `-` modify on our ERB control statments, `each` and `end`, the output will look
something closer to this

```yaml
some_list:

  - foo
  - bar

  other_key: value
```

which is actually invalid YAML and hard to read.

## Role Definitions

Each instance of a role must use a template. Once the template exists, you can create one or more
versions of each template by adding entries to `config/roles.yml`.

### Adding a Role Definition

Every entry in the role config must be a Hash with the `template` and `role` keys defined. The
`template` key should be the name of the template file, _without the extension_, used to evaluate
the role. The `role` key will be used to populate `labels.role` and as the name of the output file.
All keys, except `template`, will be available as attributes to the ERB templates.

**Example**

Assuming we have the `misc_process.yml.erb` template defined, we could start running a new misc
process by adding the following entry to `roles.yml`.

```yaml
- template: misc_proces
  role: fancy-cleanup
  consul_lock_prefix: my/consul/lock
  command_args:
    - script/fancy-cleanup
```

## File Generation

Once changes are made to templates and role definitions, you need to re-generate the files. This is
done by running a rake task:

```
bundle exec rake k8s
```

After the generate command is done, verify the output files at `kubernetes/*yml`. In our example
above, the output file would live at `kubernetes/fancy-cleanup.yml`.

**Example**

```
➔ bundle exec rake k8s
Generating /Users/cday/Code/zendesk/zendesk/kubernetes/durable-backfiller.yml
Generating /Users/cday/Code/zendesk/zendesk/kubernetes/archiver-1.yml
Generating /Users/cday/Code/zendesk/zendesk/kubernetes/archiver-2.yml
Generating /Users/cday/Code/zendesk/zendesk/kubernetes/sync-attachments.yml
```
