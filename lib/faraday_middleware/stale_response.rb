require 'faraday'

module FaradayMiddleware
  class StaleResponse
    def initialize(app, cache, statsd_key = 'stale_response_cache')
      @app = app
      @cache = cache
      @statsd_key = statsd_key
    end

    def call(env)
      cache_key = "stale_response:#{env.url}"

      begin
        response = @app.call(env)

        @cache.write(cache_key, response)
        statsd_client.increment(@statsd_key, tags: ['action:write'])

        response
      rescue Kragle::ResponseError, Faraday::Error::ClientError => ex
        Rails.logger.error "[StaleResponse] SR0: Error during request: attempting to fetch stale response from #{cache_key}: #{ex.message}"

        if @cache.exist?(cache_key)
          Rails.logger.error "[StaleResponse] SR1: Error during request, using old cached response from #{cache_key}."
          statsd_client.increment(@statsd_key, tags: ['action:read'])
          @cache.read(cache_key)
        else
          Rails.logger.error "[StaleResponse] SR2: Error during request, no old cached response found from #{cache_key}. Raising exception..."
          statsd_client.increment(@statsd_key, tags: ['action:miss'])
          raise ex
        end
      end
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["voice_classic"])
    end
  end
end
