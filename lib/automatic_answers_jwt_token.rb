class AutomaticAnswersJwtToken
  def self.encrypted_token(account_id, user_id, ticket_id, deflection_id, articles, token = nil)
    token = {
      account_id: account_id,
      user_id: user_id,
      ticket_id: ticket_id,
      deflection_id: deflection_id,
      articles: articles,
      token: token,
      exp: 30.days.from_now.to_i
    }

    JWT.encode(token, AUTOMATIC_ANSWERS_JWT_SECRET)
  end

  def self.decode(token)
    decoded_params, = JWT.decode(token, AUTOMATIC_ANSWERS_JWT_SECRET)
    decoded_params
  rescue JWT::VerificationError, JWT::DecodeError, JWT::ExpiredSignature
    nil
  end
end
