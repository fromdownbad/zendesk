if RAILS4
  # The problem addressed by DelayTouching is fixed in ActiveRecord 5, so
  # the library (and calls to it) should no longer be necessary.  See
  # https://github.com/rails/rails/issues/18606

  module ActiveRecord
    module DelayTouching
      class State
        # This fixes a confusing bug in DelayTouching.
        #
        # DelayTouching works by enqueueing `touch` events.  Rather than
        # recording to the DB when a `touch` happens, it keeps record of pending
        # `touch`es.  Then, when the `ActiveRecord::Base.delay_touching` block
        # ends, it consolidates the `touch`es and writes them to the DB.
        # However, touching an object at that time may cause other related
        # objects to also be `touch`ed, adding to the queue of items to `touch`.
        # To handle this, DelayTouching keeps a queue of items to `touch`, and
        # loops, servicing them, until the queue is empty.
        # Internally, DelayTouching stores the items to `touch` as items
        # in a Set, which internally is implemented as the keys in a Hashmap.
        # When DelayTouching handles an object, it removes the object from the
        # Set.  Internally, this finds the object in the Hashmap by looking
        # up by `hash`, and removing it.
        #
        # ActiveRecord has this confusing behavior, which is illustrated in the
        # relation between User objects and Zopim::Agent objects.
        # User has `after_save :enable_chat_identity!`, and the
        # `enable_chat_identity!` method attempts to create a Zopim::Agent
        # object.  That fails if the Zopim subscription isn't serviceable.
        # When the creation of the Zopim::Agent fails, it rolls back the DB
        # transaction.  ActiveRecord smartly realizes that even though `save`
        # was successful for the Agent, the Agent is not actually in the DB.
        # ActiveRecord resets the `id` of the Agent to `nil` to indicate that
        # the Agent is not in the database.
        #
        # In ActiveRecord, the `hash` of an object is based on its `id`.  Thus,
        # when the `id` of the User is reset to nil, the hash of the User is
        # modified.
        #
        # This creates an infinite loop in DelayTouching.
        #
        # DelayTouching put the User in the queue of objects to `touch`.  When
        # it did so, the User had a non-nil `id`.  Under the covers, the queue
        # is a Set backed by a Hashmap, so the User is indexed in that queue
        # by its hash.
        # Later, DelayTouching attempts to service that queue.  It touches the
        # User, and attempts to remove the User from the queue.  Removal from
        # the queue is by hash, but the hash of the User has changed since
        # ActiveRecord has reset the `id`.  DelayTouching fails to remove the
        # User from the queue.  Since the queue is never empty, DelayTouching
        # continues to loop forever.
        #
        # One solution is for DelayTouching to ignore objects that are "new",
        # i.e. which don't have an `id`.  They're not in the DB anyway, so they
        # don't need to be touched.
        #
        # Original definition:
        # def more_records?
        #   @records.present?
        # end
        def more_records?
          @records.each_value do |v|
            return true if v.any? { |item| !item.new_record? }
          end
          false
        end
      end
    end
  end
else
  puts "DELETE THIS FILE: #{__FILE__} and the associated test."
end
