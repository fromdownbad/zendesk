module SlugIds
  def slug_id(model)
    normalized_name = normalize_model_name(model.name)
    normalized_name ? "#{model.id}-#{normalized_name.truncate(100, omission: '')}" : model.id.to_s
  end

  private

  def normalize_model_name(model_name)
    return unless model_name
    model_name.gsub(/[^[:alnum:]]+/, "-")
  end
end
