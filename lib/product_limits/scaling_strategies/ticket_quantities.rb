class ProductLimits::ScalingStrategies::TicketQuantities
  class << self
    MAX_TICKET_COUNT = 100000

    def calculate(limiter, _definition, account)
      (multiplier(account) * limiter.send(:throttle_base_value)).to_i
    end

    # 10,000,000 per year is 28571 per day
    # multiplier for:
    # below 100 => 1
    # below 250 => 1.25
    # below 500 => 2
    # below 1000 => 3
    # below 5000 => 5
    # below  10000 => 8
    # below  20000 => 13
    # below  50000 => 21
    # below  100000 => 34
    # over  100000 => 55
    #  NOTE This rate is close to the fibonacci sequence
    def multiplier(account)
      ticket_count = averaged_ticket_count(account)
      return 1.0 if ticket_count <= 100
      return 1.25 if ticket_count <= 250
      return 2.0 if ticket_count <= 500
      return 3.0 if ticket_count <= 1000
      return 5.0 if ticket_count <= 5000
      return 8.0 if ticket_count <= 10000
      return 13.0 if ticket_count <= 20000
      return 21.0 if ticket_count <= 50000
      return 34.0 if ticket_count <= MAX_TICKET_COUNT
      55.0
    end

    def averaged_ticket_count(account)
      one_day_avg = tickets_in_last_day(account)
      return one_day_avg if one_day_avg >= MAX_TICKET_COUNT
      five_day_avg = tickets_in_last_week(account) / 5.0
      return five_day_avg if five_day_avg >= MAX_TICKET_COUNT
      twenty_day_avg = tickets_in_last_4weeks(account) / 20.0
      [one_day_avg, five_day_avg, twenty_day_avg].max
    end

    def tickets_in_last_day(account)
      @tickets_in_last_day ||= Rails.cache.fetch("#{account.id}/tickets_in_last_day", expires_in: 1.day, race_condition_ttl: 30.seconds) do
        tickets_in_last(account, 2.days.ago, 1.day.ago, MAX_TICKET_COUNT)
      end
    end

    def tickets_in_last_week(account)
      @tickets_in_last_week ||= Rails.cache.fetch("#{account.id}/tickets_in_last_week", expires_in: 1.day, race_condition_ttl: 30.seconds) do
        tickets_in_last(account, 8.days.ago, 1.day.ago, MAX_TICKET_COUNT * 5)
      end
    end

    def tickets_in_last_4weeks(account)
      @tickets_in_last_4weeks ||= Rails.cache.fetch("#{account.id}/tickets_in_last_4weeks", expires_in: 1.day, race_condition_ttl: 30.seconds) do
        tickets_in_last(account, 29.days.ago, 1.day.ago, MAX_TICKET_COUNT * 20)
      end
    end

    def tickets_in_last(account, from, to, limit)
      Ticket.with_deleted do
        account.tickets.
          use_index(Ticket::ACCOUNT_AND_STATUS_AND_CREATED_INDEX).
          where.not(status_id: StatusType.ARCHIVED).
          where("created_at >= ? AND created_at <= ?", from, to).
          limit(limit).count
      end
    end
  end
end
