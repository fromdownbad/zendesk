class ProductLimits::ScalingStrategies::Stationary
  # definitions => { threshold: 100, interval: 10.minutes }
  class << self
    def calculate(limiter, _definition, _account)
      limiter.send(:throttle_base_value)
    end

    def multiplier(_account)
      1.0
    end
  end
end
