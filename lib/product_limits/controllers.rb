class ProductLimits::Controllers
  DEFAULT_LIMITS = {
    # v2_tickets_controller is the identifier for Api::V2::TicketsController
    v2_tickets_controller: {
      # update is the name of the action in the controller
      update: {
        # identifier => identifier for the specific endpoint within the controller (required)
        identifier: :v2_tickets_update,
        # name of the account setting that overrides the threshold
        account_setting_threshold: 'ticket_updates_threshold',
        # arturo => optional arturo used to turn on limiting the API
        #           NOTE: this arturo does not impact grandfathered accounts.
        #                 Use arturo_master_switch for using arturo for grandfathered accounts.
        #           NOTE: this arturo should be prepended with "throttle_endpoint_" in capabilities.rb
        arturo: 'limit_ticket_api',
        # arturo_master_switch => optional arturo used to turn on limiting the API
        #           if you have the `arturo` flag on and `arturo_master_switch` off
        #              => ALL account are OFF
        #           if you have the `arturo` flag OFF and `arturo_master_switch` OFF
        #              => ALL account are OFF
        #           if you have the `arturo` flag OFF and `arturo_master_switch` ON
        #              => grandfathered accounts are OFF
        #              => new (non-grandfathered) accounts are ON
        #           if you have the `arturo` flag ON and `arturo_master_switch` ON
        #              => ALL account are ON
        #           if either `arturo` or `arturo_master_switch` is absent
        #              => the default is ON
        #
        # NOTE: this arturo should be prepended with "throttle_endpoint_" in capabilities.rb
        arturo_master_switch: 'limit_ticket_api_master',
        # after this date we ignore the arturo and always have the rate limiting turned on for a given acount
        grandfathered_date: '2019-07-01',
        interval: 1.minute,
        # optionally set trial_limit if you want different limits for accounts on trial
        limits: {
          sandbox: { # interval => in minutes (required)
            definitions: [
              { threshold: 20 }
            ],
            high_volume_definitions: [
              { threshold: 100, scaling_strategy: ::ProductLimits::ScalingStrategies::Stationary }
            ],
            description: "txt.admin.controllers.tickets_controller.limited_ticket_updates"
          },
          trial: {
            definitions: [
              { threshold: 20 }
            ],
            high_volume_definitions: [
              { threshold: 100, scaling_strategy: ProductLimits::ScalingStrategies::Stationary }
            ],
            description: "txt.admin.controllers.tickets_controller.limited_ticket_updates"
          },
          grandfathered_default: {
            definitions: [
              { threshold: 100, scaling_strategy: ProductLimits::ScalingStrategies::TicketQuantities }
            ],
            high_volume_definitions: [
              { threshold: 300, scaling_strategy: ProductLimits::ScalingStrategies::TicketQuantities }
            ],
            description: "txt.admin.controllers.tickets_controller.limited_ticket_updates"
          },
          # required to set default_limit for each action
          default: {
            definitions: [
              { threshold: 100, scaling_strategy: ProductLimits::ScalingStrategies::Stationary }
            ],
            high_volume_definitions: [
              { threshold: 300, scaling_strategy: ProductLimits::ScalingStrategies::Stationary }
            ],
            description: "txt.admin.controllers.tickets_controller.limited_ticket_updates"
          }
        }
      },
      index: {
        # identifier => identifier for the specific endpoint within the controller (required)
        identifier: :v2_tickets_index,
        # name of the account setting that overrides the threshold
        account_setting_threshold: 'ticket_index_threshold',
        account_setting_deep_threshold: 'ticket_index_deep_threshold',
        account_setting_shallow_threshold: 'ticket_index_shallow_threshold',
        # arturo => optional arturo used to turn on limiting the API
        #           NOTE: this arturo should be prepended with "throttle_endpoint_" in capabilities.rb
        arturo: 'limit_ticket_index_api',
        arturo_master_switch: 'limit_ticket_index_api_master',
        grandfathered_date: '2020-07-01',
        interval: 1.minute,
        # optionally set trial_limit if you want different limits for accounts on trial
        limits: {
          sandbox: { # interval => in minutes (required)
            definitions: [
              { threshold: 100 }
            ],
            high_volume_definitions: [
              { threshold: 300 }
            ],
            description: "txt.admin.controllers.tickets_controller.limited_ticket_index"
          },
          paginated: {
            definitions: [
              # pagination
              { page: 50, threshold: 700 },
              # deep pagination
              { page: 500, threshold: 50 }
            ],
            high_volume_definitions: [
              # pagination
              { page: 50, threshold: 700 },
              # deep pagination
              { page: 500, threshold: 50 }
            ],
            description: "txt.admin.controllers.tickets_controller.limited_ticket_index_pagination"
          },
          trial: {
            definitions: [
              { threshold: 100 }
            ],
            high_volume_definitions: [
              { threshold: 300 }
            ],
            description: "txt.admin.controllers.tickets_controller.limited_ticket_index"
          },
          grandfathered_default: {
            definitions: [
              { threshold: 20000, scaling_strategy: ProductLimits::ScalingStrategies::TicketQuantities }
            ],
            high_volume_definitions: [
              { threshold: 30000, scaling_strategy: ProductLimits::ScalingStrategies::TicketQuantities }
            ],
            description: "txt.admin.controllers.tickets_controller.limited_ticket_index"
          },
          # required to set default_limit for each action
          default: {
            definitions: [
              { threshold: 100000, scaling_strategy: ProductLimits::ScalingStrategies::Stationary }
            ],
            high_volume_definitions: [
              { threshold: 300000, scaling_strategy: ProductLimits::ScalingStrategies::Stationary }
            ],
            description: "txt.admin.controllers.tickets_controller.limited_ticket_index"
          }
        }
      }
    },
    # v2_tags_controller is the identifier for Api::V2::TagsController
    v2_tags_controller: {
      # index is the name of the action in the controller
      index: {
        # identifier => identifier for the specific endpoint within the controller (required)
        identifier: :v2_tags_index,
        # name of the account setting that overrides the threshold
        account_setting_threshold: 'tags_index_threshold',
        account_setting_deep_threshold: 'tags_index_deep_threshold',
        account_setting_shallow_threshold: 'tags_index_shallow_threshold',
        # arturo => optional arturo used to turn on limiting the API
        #           NOTE: this arturo should be prepended with "throttle_endpoint_" in capabilities.rb
        arturo: 'limit_tag_index_api',
        arturo_master_switch: 'limit_tag_index_api_master',
        grandfathered_date: '2020-10-01',
        interval: 1.minute,
        # optionally set trial_limit if you want different limits for accounts on trial
        limits: {
          sandbox: { # interval => in minutes (required)
            definitions: [
              { threshold: 100 }
            ],
            high_volume_definitions: [
              { threshold: 300 }
            ],
            description: "txt.admin.controllers.tags_controller.limited_tags_index"
          },
          paginated: {
            definitions: [
              # pagination
              { page: 50, threshold: 3000 },
              # deep pagination
              { page: 500, threshold: 100 }
            ],
            high_volume_definitions: [
              # pagination
              { page: 50, threshold: 3000 },
              # deep pagination
              { page: 500, threshold: 100 }
            ],
            description: "txt.admin.controllers.tags_controller.limited_tags_index_pagination"
          },
          trial: {
            definitions: [
              { threshold: 100 }
            ],
            high_volume_definitions: [
              { threshold: 300 }
            ],
            description: "txt.admin.controllers.tags_controller.limited_tags_index"
          },
          grandfathered_default: {
            definitions: [
              { threshold: 2000, scaling_strategy: ProductLimits::ScalingStrategies::Stationary }
            ],
            high_volume_definitions: [
              { threshold: 2000, scaling_strategy: ProductLimits::ScalingStrategies::Stationary }
            ],
            description: "txt.admin.controllers.tags_controller.limited_tags_index"
          },
          # required to set default_limit for each action
          default: {
            definitions: [
              { threshold: 2000, scaling_strategy: ProductLimits::ScalingStrategies::Stationary }
            ],
            high_volume_definitions: [
              { threshold: 2000, scaling_strategy: ProductLimits::ScalingStrategies::Stationary }
            ],
            description: "txt.admin.controllers.tags_controller.limited_tags_index"
          }
        }
      }
    }
  }.freeze
end
