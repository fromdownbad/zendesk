require 'google/protobuf'
require 'google/protobuf/well_known_types'

# Following line fixes a production namespace muddle for generated files.
# https://gist.github.com/ciaranarcher/4273e7340b8c2fff19e545457fa6ddbc
Zendesk::Google::Protobuf = ::Google::Protobuf
