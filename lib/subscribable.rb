module Subscribable
  def self.included(model)
    model.has_many :watchers, through: :watchings, source: :user
    model.has_many :watchings, as: :source, dependent: :destroy
  end

  def subscribe(user)
    if subscribed?(user) || deleted? || !user.can?(:watch, self)
      true
    else
      watchings.create(user: user, account: account, source: self)
    end
  end

  def unsubscribe(user)
    watching = watchings.where(user_id: user.id).first
    watching.destroy
  end

  def subscribed?(user)
    watchings.find_by_user_id(user).present?
  end

  def subscribed_with_posts?(user)
    !watchings.find_by_user_id_and_include_posts(user, true).nil?
  end
end
