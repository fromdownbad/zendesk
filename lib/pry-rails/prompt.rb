module PryRails
  class Prompt
    class << self
      def formatted_env
        if Rails.env.production?
          bold_env = Pry::Helpers::Text.bold(Rails.env)
          Pry::Helpers::Text.red(bold_env)
        elsif Rails.env.development?
          Pry::Helpers::Text.green(Rails.env)
        else
          Rails.env
        end
      end

      def project_name
        # Customized from pry-rails to not depend on directory structure
        Rails.application.class.parent_name.underscore
      end
    end
  end

  require "pry"

  desc = "Includes the current Rails environment and project folder name.\n" \
          "[1] [project_name][Rails.env] pry(main)>"
  Pry::Prompt.add 'rails', desc, %w[> *] do |target_self, nest_level, pry, sep|
    "[#{pry.input_ring.size}] " \
      "[#{Prompt.project_name}][#{Prompt.formatted_env}] " \
      "#{pry.config.prompt_name}(#{Pry.view_clip(target_self)})" \
      "#{":#{nest_level}" unless nest_level.zero?}#{sep} "
  end
end
