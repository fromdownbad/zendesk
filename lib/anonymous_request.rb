class AnonymousRequest
  attr_reader   :email, :set_tags, :via_id, :priority_id, :ticket_type_id, :custom_fields
  attr_accessor :client

  def initialize(args = {})
    @account        = args[:account]
    @email          = args[:email].to_s
    @name           = args[:name].to_s
    @locale_id      = args[:locale_id].to_i
    @subject        = args[:subject].to_s.strip
    @description    = args[:description].to_s.strip
    @set_tags       = args[:set_tags]
    @via_id         = args[:via_id] || ViaType.WEB_FORM
    @brand_id       = args[:brand_id]
    @priority_id    = args[:priority_id] if priority_editable_by_end_user?
    @ticket_type_id = args[:ticket_type_id] if ticket_type_editable_by_end_user?
    @custom_fields  = retain_only_active_visible_editable_custom_fields(args[:fields] || {})
  end

  def author
    return @author if @author

    @author = @account.find_user_by_email(@email)

    if @author.nil?
      if @name.blank?
        @name = Zendesk::Mail::Address.new(address: @email).name || "Unknown"
      end
      @author = @account.users.new(
        name: @name,
        email: @email,
        locale_id: @locale_id
      )
    end

    @author
  end

  def subject
    @account.field_subject.is_active? ? @subject : ''
  end

  def description
    @account.field_subject.is_active? ? @description : "#{@subject}#{':' unless @subject.blank?} #{@description}".strip
  end

  def to_ticket
    ticket = Ticket.new(
      account: @account,
      via_id: via_id,
      brand_id: @brand_id,
      subject: subject,
      description: description,
      client: client,
      priority_id: priority_id,
      ticket_type_id: ticket_type_id,
      fields: custom_fields,
      locale_id: @locale_id
    )

    ticket.additional_tags = set_tags
    ticket.fix_tags
    ticket.add_flags!([EventFlagType.DEFAULT_UNTRUSTED])

    ticket
  end

  def to_suspended_ticket
    suspended_ticket = SuspendedTicket.new(
      account: @account,
      subject: subject,
      content: description,
      cause: suspension_type(author.email),
      from_name: author.name,
      from_mail: author.email,
      via_id: via_id,
      author: author,
      client: client,
      properties: {
        fields: custom_fields,
        priority_id: priority_id,
        ticket_type_id: ticket_type_id,
        set_tags: set_tags,
        locale_id: @locale_id
      }
    )

    suspended_ticket
  end

  def suspension_type(email)
    return SuspensionType.SIGNUP_REQUIRED if @account.whitelists?(email)
    return SuspensionType.BLOCKLISTED if @account.blacklists?(email)
    SuspensionType.SIGNUP_REQUIRED
  end

  protected

  def priority_editable_by_end_user?
    @account && field_editable_by_end_user?(@account.field_priority)
  end

  def ticket_type_editable_by_end_user?
    @account && field_editable_by_end_user?(@account.field_ticket_type)
  end

  def field_editable_by_end_user?(field)
    field && field.is_active? && field.is_visible_in_portal? && field.is_editable_in_portal?
  end

  def retain_only_active_visible_editable_custom_fields(fields)
    return {} if @account.blank?
    allowed_field_ids = @account.ticket_fields.active.custom.visible_in_portal.editable_in_portal.map(&:id).map(&:to_s)
    fields.dup.delete_if do |field_id, _|
      !allowed_field_ids.include?(field_id.to_s)
    end
  end
end
