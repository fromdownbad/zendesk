# TODO: replace with ActiveSupport::Cache::LibmemcachedLocalStore
# (libmemcached_store gem) which serializes values when adding them to memory,
# so tests will reflect what runs in production.

# An in-memory cache store that returns duped data to behave like old libmemcached_store
class TestMemoryStore < ActiveSupport::Cache::MemoryStore
  def read(*args)
    value = super
    value.duplicable? ? value.dup : value
  end

  def fetch(*args, &block)
    if args.last.is_a?(Hash) && race = args.last[:race_condition_ttl]
      args.last[:expires_in] += race # simulate what memcached does to avoid running into errors only in non-test
    end
    super
  end

  def write_entry(key, entry, options) # :nodoc:
    if options[:unless_exist] && @data.key?(key) && @data[key].expired?
      @data.delete(key)
    end
    super
  end
end
