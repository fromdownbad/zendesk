module CiaControllerSupport
  # This constant only need to exist until we rollout :new_actor_via_requester_actions.
  # Once that is done, we should rename NEW_ACTOR_VIA_REQUESTER_ACTIONS to
  # ACTOR_VIA_REQUESTER_ACTIONS
  OLD_ACTOR_VIA_REQUESTER_ACTIONS = {
    'api/v2/internal/security_settings' => ['update']
  }.freeze

  NEW_ACTOR_VIA_REQUESTER_ACTIONS = {
    'api/v2/internal/emails' => ['create', 'deliver_verify', 'destroy', 'update'],
    'api/v2/internal/passwords' => ['create', 'reset', 'update'],
    'api/v2/internal/security_settings' => ['update'],
    'api/v2/internal/staff' => ['create', 'destroy', 'update']
  }.freeze

  def self.included(base)
    base.around_action :audit_action
  end

  protected

  def audit_action
    options = {
      ip_address: (@audit_ip_address || request.remote_ip)
    }

    # a request via system-user auth
    if request.headers['X-Zendesk-Via']
      # prevent external users from freely seting via
      unless current_user.is_system_user?
        # rubocop:disable Style/GlobalVars
        $statsd.increment(
          'cia.audit_action.error',
          tags: ["error:non_system_user", "controller:#{controller_name}", "action:#{action_name}"]
        )
        # rubocop:enable Style/GlobalVars

        return head(:bad_request)
      end

      # rather blow up then ignore invalid headers
      if via = ViaType.list.assoc(request.headers['X-Zendesk-Via'])
        options[:via_id] = via.last
      else
        # rubocop:disable Style/GlobalVars
        $statsd.increment(
          'cia.audit_action.error',
          tags: ["error:invalid_via", "controller:#{controller_name}", "action:#{action_name}"]
        )
        # rubocop:enable Style/GlobalVars

        return head(:bad_request)
      end

      options[:actor] = User.system
      options[:effective_actor] = current_user
      options[:via_reference_id] = request.headers['X-Zendesk-Via-Reference-ID'].to_i

      if options[:via_reference_id].zero?
        # rubocop:disable Style/GlobalVars
        $statsd.increment(
          'cia.audit_action.invalid_via_reference_id',
          tags: ["error:invalid_via_reference_id", "controller:#{controller_name}", "action:#{action_name}"]
        )
        # rubocop:enable Style/GlobalVars

        return head(:bad_request)
      end

    # a request by a user assuming someone via monitor
    elsif is_assuming_user_from_monitor?
      options[:actor] = User.system
      options[:effective_actor] = current_user
      options[:via_id] = ViaType.MONITOR_EVENT
      options[:via_reference_id] = authentication.assuming_monitor_user_id
    # a request by a system user to an internal endpoint in behalf of a normal user
    elsif actor_via_requester_action? && params[:requester_id]
      options[:actor] = current_account.users.find_by_id(params[:requester_id]) || current_user
      options[:effective_actor] = current_user
    # regular request by normal user or assuming admin (don't save the anonymous user)
    elsif current_user_exists?
      options[:actor] = original_user || current_user
      options[:effective_actor] = current_user
    end

    CIA.audit options do
      yield
    end
  end

  def actor_via_requester_actions
    if Arturo.feature_enabled_for?(:new_actor_via_requester_actions, current_account)
      NEW_ACTOR_VIA_REQUESTER_ACTIONS
    else
      OLD_ACTOR_VIA_REQUESTER_ACTIONS
    end
  end

  def actor_via_requester_action?
    (actor_via_requester_actions[controller_path] || []).include?(action_name)
  end
end
