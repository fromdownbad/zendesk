require 'zendesk_protobuf_clients/zendesk/protobuf/account_audits/audit_event_pb'

module CiaEventCreator
  EVENT_ATTRIBUTES = [
    :account_id,
    :actor_id,
    :actor_type,
    :action,
    :ip_address,
    :source_id,
    :source_type,
    :message,
    :visible,
    :status_bits,
    :source_display_name,
  ].freeze

  class ProtoDecoder
    ##
    # @return [Hash] decoded params
    # @raise [Google::Protobuf::ParseError]
    #
    def self.decode(payload)
      audit_event = Zendesk::Protobuf::AccountAudits::AuditEvent.decode(payload)
      actor_id = audit_event&.actor_id || 0
      {
        account_id: audit_event&.account_id,
        actor_id: actor_id,
        actor_type: actor_id > 0 ? 'User' : 'System',
        action: audit_event&.action,
        ip_address: audit_event&.ip_address,
        source_id: audit_event&.source_id&.to_i,
        source_type: audit_event&.source_type,
        message: audit_event&.raw_message&.text,
        visible: audit_event&.visible,
        source_display_name: audit_event&.source_display_name,
        changes: decode_changes(audit_event&.changes)
      }
    end

    ##
    # This decodes the 'changes' of audit_event
    # @param :changes List[Zendesk::Protobuf::AccountAudits::AuditEvent::AttributeChange]
    # changes =>
    #     Zendesk::Protobuf::AccountAudits::AuditEvent::AttributeChange.new(
    #       attribute_name: 'via_id',
    #       old_value:      '1',
    #       new_value:      '2',
    #     )
    # @return [Hash, nil] { 'via_id' => ['1', '2']}
    #
    def self.decode_changes(change_list)
      change_list&.each_with_object({}) do |change, map|
        attrib_name = change&.attribute_name
        old_value   = change&.old_value
        new_value   = change&.new_value

        map[attrib_name] = [old_value, new_value]
      end
    end
    private_class_method :decode_changes
  end

  ##
  # @param :payload [Hash, ProtoClass]
  # @param :decoder [#decode]
  #
  def create!(payload, decoder: nil)
    params = decoder.present? ? decoder.decode(payload) : payload
    event_params = params.slice(*EVENT_ATTRIBUTES).compact

    event = CIA::Event.new(event_params)
    Zendesk::Monitor::AuditLogsHandler.new(event).add_attribute_changes(params)
    event.save!
  end
  module_function :create!
end
