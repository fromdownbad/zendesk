# This module provides methods for adding, removing and setting tags.
module TagManagement
  def self.included(model)
    model.extend(ClassMethods)

    model.has_many :taggings, dependent: :destroy, as: :taggable, inherit: :account_id
    model.after_save :update_taggings
  end

  def set_tags=(tag_attributes) # rubocop:disable Naming/AccessorMethodName
    self.current_tags = normalize_tags(tag_attributes).join(' ')
    self.current_tags = nil if current_tags.blank? && (is_a?(Ticket) || is_a?(Entry))
  end
  alias :tags= :set_tags=

  def additional_tags=(tag_attributes)
    self.set_tags = "#{current_tags} #{normalize_tags(tag_attributes).join(' ')}"
  end

  def remove_tags=(tag_attributes)
    if current_tags.present? && tag_attributes.present?
      self.current_tags = (normalize_tags(current_tags) - normalize_tags(tag_attributes)).join(' ')
    end
  end

  def truncate_tags
    return unless current_tags
    column = self.class.columns_hash["current_tags"]
    return unless column
    return unless current_tags.size > column.limit

    tag_list = normalize_tags(current_tags)
    truncated = []
    truncated.unshift tag_list.pop while tag_list.join(' ').size > column.limit
    self.current_tags = tag_list.join(' ')
    truncated
  end

  # These two readers are just required to fake the presence of additional_tags and remove_tags fields on the model such that
  # form builders and such have an easier time
  def additional_tags
    ''
  end

  def remove_tags
    ''
  end

  def tag_array
    if is_a?(Ticket)
      Rails.logger.warn("WARNING: Reading taggings through tag_array is deprecated! Account: #{account_id}, Ticket: #{id}")
      raise 'Reading ticket tags through TagManagement#tag_array is deprecated, please use Ticket#tag_array instead' if %w[development test].include?(Rails.env) || account&.has_build_tag_array_from_current_tags?
    end

    taggings.map(&:tag_name)
  end

  # This reader is pretty ugly because: It has a name of a setting action "set" is a verb. And it doesn't
  # add value, only confusion.
  def set_tags
    current_tags
  end

  # Returns an array of well formed tags
  def normalize_tags(arg, external_account = nil)
    result = if arg.is_a?(Array)
      arg.flat_map { |tag| normalize_tag_string(tag) }
    elsif arg.is_a?(String)
      normalize_tag_string(arg)
    else
      []
    end

    result.compact.map { |tag| downcase_tag_string(tag, external_account) }.uniq
  end

  def self.equals?(tag_string, other_tag_string)
    normalize_tags(tag_string) == normalize_tags(other_tag_string)
  end

  def update_taggings?
    true
  end

  # Aims for consistency between self.current_tags and the records in the taggings table
  def update_taggings(force: false)
    return unless update_taggings? || force

    memory = normalize_tags(current_tags)
    current_taggings = taggings.reload.to_a
    stored = current_taggings.map(&:tag_name)
    dirty = false

    # Add tags
    (memory - stored).each do |tag_name|
      dirty = true
      taggings << Tagging.new(tag_name: tag_name, taggable: self, account: account)
    end

    # Remove tags
    desired_tag_set = Set.new(memory)
    current_taggings.clone.each do |tagging|
      unless desired_tag_set.include?(tagging.tag_name)
        dirty = true
        taggings.delete(tagging)
      end
      desired_tag_set.delete(tagging.tag_name) # to clean up dupe taggings
    end

    smart_touch if dirty && changes["updated_at"].nil?
    self
  end

  def add_redaction_tag!
    if tag_array.exclude?("system_credit_card_redaction")
      self.additional_tags = "system_credit_card_redaction"
    end
  end

  def tags_changes
    existing_tags = normalize_tags(current_tags)
    {
      removed: tags - existing_tags,
      added: existing_tags - tags,
    }
  end

  def tags_changed?
    changes = tags_changes
    changes[:removed].any? || changes[:added].any?
  end

  private

  def normalize_tag_string(arg)
    arg.tokenize.map { |x| x.gsub(SPECIAL_CHARS, '') } if arg.is_a?(String)
  end

  def downcase_tag_string(string, external_account = nil)
    if respond_to?(:account) && account&.has_downcase_tags_as_ascii?
      string.downcase(:ascii)
    elsif external_account&.has_downcase_tags_as_ascii?
      string.downcase(:ascii)
    else
      string.downcase
    end
  end

  module_function :normalize_tags, :normalize_tag_string, :downcase_tag_string

  module ClassMethods
    def cloud(tags, category_list)
      max = tags.map { |tag| tag.score.to_i }.max || 0
      min = tags.map { |tag| tag.score.to_i }.min || 0

      divisor = ((max - min) / category_list.size) + 1

      tags.each do |tag|
        yield tag.tag_name, tag.score, category_list[(tag.score.to_i - min) / divisor]
      end
    end
  end # ClassMethods
end # TagManagement
