class AcmeReporter
  SUPPORT_T2_GROUP_NAME = 'Customer Care'.freeze
  SUPPORT_T2_GROUP_ID = 360008759733
  SQID_USER_ID = 6474505348

  ABOUT_CUSTOM_FIELD_ID = 336767
  BUSINESS_IMPACT_CUSTOM_FIELD_ID = 32142077
  SUBDOMAIN_CUSTOM_FIELD_ID = 22694523

  attr_reader :subdomain, :ticket_id

  def initialize(subdomain, ticket_id:)
    @subdomain = subdomain
    @ticket_id = ticket_id
  end

  def report_error(account, message, exception: nil)
    body_content = body(account, message, exception)

    if ticket_id
      if ticket.status == 'closed'
        create_ticket(account, body_content, via_followup_source_id: ticket_id)
      else
        author_id = ZendeskAPI::User.find(internal_client, id: SQID_USER_ID) ? SQID_USER_ID : ticket.requester_id
        ZendeskAPI::Ticket.update!(internal_client, id: ticket_id, comment: { body: body_content, public: false, author_id: author_id })
      end
    else
      create_ticket(account, body_content)
    end
  end

  private

  def create_ticket(account, body_content, extra_params = {})
    ticket_attributes = {
      requester: {
        name: 'SQID',
        email: 'sqid+le@zendesk.com'
      },
      subject: "Let's Encrypt Error for #{account.subdomain}",
      comment: {
        body: body_content,
        public: false
      },
      type: 'task',
      priority: 'normal',
      status: 'open',
      group_id: support_tier2_group_id,
      custom_fields: [
        business_impact_custom_field,
        about_custom_field,
        subdomain_custom_field(account)
      ],
      tags: %w[lets_encrypt],
    }.merge(extra_params)

    internal_client.tickets.create(ticket_attributes).tap { |t| @ticket_id = t.id }
  end

  def body(account, message, exception)
    text = message.to_s
    text += <<-TEXT


      Account: #{account.subdomain}
      Account id: #{account.id}
      Hostmapping: #{account.routes.map(&:host_mapping).join(', ')}
      Shard: #{account.shard_id}
    TEXT

    if exception
      text += <<-TEXT

        Exception: `#{exception}`
        Backtrace:

        ```
        #{exception.backtrace[0..10].join("\n")}
        ```

      TEXT
    end

    text.each_line.map(&:strip).join("\n")
  end

  def about_custom_field
    {
      "id": ABOUT_CUSTOM_FIELD_ID,
      "value": "about_domains_ssl"
    }
  end

  def business_impact_custom_field
    {
      "id": BUSINESS_IMPACT_CUSTOM_FIELD_ID,
      "value": "3_moderate"
    }
  end

  def subdomain_custom_field(account)
    {
      "id": SUBDOMAIN_CUSTOM_FIELD_ID,
      "value": account.subdomain
    }
  end

  def support_tier2_group_id
    if group = internal_client.groups.detect { |g| g.name == SUPPORT_T2_GROUP_NAME }
      group.id
    elsif Rails.env.production?
      SUPPORT_T2_GROUP_ID
    else
      ZendeskExceptions::Logger.record(Exception.new, location: self, message: "Can't find tier 2 group", fingerprint: 'c5d90c7d2950b5f78bcbfbaa8b70a792e5669198')
    end
  end

  def ticket
    @ticket ||= ZendeskAPI::Ticket.find!(internal_client, id: ticket_id)
  end

  def internal_client
    @internal_client ||= Zendesk::Accounts::InternalApiClient.new(subdomain)
  end
end
