module TimeZoneValidation
  def validates_time_zone(attribute, options = {})
    validate(options) do |instance|
      if !(value = instance.read_attribute(:time_zone)).nil? && ActiveSupport::TimeZone[value.to_s].nil?
        instance.errors.add(attribute, :invalid)
      end
    end
  end
end
