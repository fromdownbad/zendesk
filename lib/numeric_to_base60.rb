# Base60 encoding functions ported from moment.js library utils for moment js DST data compression
# https://github.com/moment/moment-timezone/blob/e3eb54c6b0a6ed901d1e331a2f3a6d11295c8d92/moment-timezone-utils.js
module NumericToBase60
  BASE60  = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWX".freeze
  EPSILON = 0.000001; # Used to fix floating point rounding errors

  class << self
    def encode60(number, precision = 0)
      raise TypeError, "#{number} is not a Numeric" unless number.is_a?(Numeric)
      return '0' if number == 0

      output   = ""
      absolute = number.abs
      whole    = absolute.floor
      fraction = encode60_fraction(absolute - whole, [precision, 10].min)

      while whole > 0
        output = BASE60[whole % 60] + output
        whole  = (whole / 60).floor
      end

      output = '-' + output if number < 0

      output + fraction
    end

    private

    def encode60_fraction(fraction, precision)
      buffer = "."
      output = ""

      while precision > 0
        precision -= 1;
        fraction  *= 60;
        current = (fraction + EPSILON).floor;
        buffer    += BASE60[current];
        fraction  -= current;

        # Only add buffer to output once we have a non-zero value.
        # This makes '.000' output '', and '.100' output '.1'
        if current > 0
          output += buffer
          buffer = ''
        end
      end

      output
    end
  end
end
