# This module provides methods for via for tickets and events
module ViaManagement
  def via
    ViaType.to_s(via_id)
  end

  def via_twitter?
    via_tweet? || via_twitter_dm?
  end

  def via_tweet?
    via?(:twitter) || via?(:twitter_favorite)
  end

  def via_twitter_dm?
    via?(:twitter_dm)
  end

  def via_facebook?
    via?(:facebook_post) || via?(:facebook_message)
  end

  def via_any_channel?
    via?(:any_channel)
  end

  def via_channel?
    via_facebook? || via_twitter? || via_any_channel?
  end

  def via?(value)
    via_id == ViaType.fuzzy_find(value)
  end

  def current_via?(value)
    current_via_id == ViaType.fuzzy_find(value)
  end

  def via_kind?(kind_symbol)
    ViaType.kind(via_id) == kind_symbol
  end

  def via_description(locale = ENGLISH_BY_ZENDESK)
    I18n.with_locale(locale) do
      return '' if via?(:web_form)

      if via?(:rule) && !via_reference_id.nil?
        if rule = Rule.find_by_id(via_reference_id)
          rule_name = I18n.t("txt.admin.views.rules.analysis.show.label_singular_#{rule.class.name.downcase}") << " \"#{rule.title}\""
          I18n.t("txt.via_types.by_via_type", via_type: rule_name)
        else
          I18n.t("txt.via_types.by_deleted_rule")
        end
      # elsif via?(:web_form) && is_a?(Audit)
      #  'By web form'
      elsif via?(:mail) && is_a?(Audit)
        value.blank? ? '' : I18n.t("txt.via_types.by_email_to", user: value)
      elsif via?(:linked_problem)
        linked_problem = Ticket.find_by_id(via_reference_id, select: 'nice_id, id, account_id', stub_only: true)
        if linked_problem
          I18n.t("txt.via_types.by_via_type", via_type: translated_via_type) + "##{linked_problem.nice_id}"
        else
          ''
        end
      elsif via?(:merge)
        target = Ticket.find_by_id(via_reference_id, select: 'nice_id, id, account_id', stub_only: true)
        if target
          I18n.t('txt.admin.lib.zendesk.lib.via_management.merged_into_ticket', ticket_id: target.nice_id)
        else
          ''
        end
      elsif via?(:helpcenter)
        I18n.t('txt.via_types.help_center')
      elsif via?(:logmein_rescue)
        I18n.t("txt.via_types.by_via_type", via_type: translated_via_type)
      elsif via?(:admin_setting)
        via_type = I18n.t("txt.via_types.by_via_type", via_type: translated_via_type.downcase)
        "#{via_type}#{setting_name(via_reference_id)}"
      elsif !is_a?(Comment)
        I18n.t("txt.via_types.by_via_type", via_type: translated_via_type)
      else
        ''
      end
    end
  end

  def translated_via_type
    I18n.t("txt.via_types.#{via.downcase.split(" ").join("_")}")
  end

  def setting_name(via_reference_id)
    return unless via_reference_id && setting = AccountSetting.find(via_reference_id)
    " \"#{I18n.t("txt.admin.views.reports.tabs.audits.property.object.#{setting.name}")}\""
  end
end
