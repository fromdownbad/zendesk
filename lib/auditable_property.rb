# A module to audit changes on property sets.
#
# Example:
# class Foo
#
#   property_set :settings do
#     property :foo_setting, :type => :boolean, :default => false
#     property :bar_setting, :type => :boolean, :default => false
#     property :single_text_setting, :type => :boolean, :default => false
#   end
#
#   include AuditableProperty
#
#   # * Use the configuration method below to define the list of properties to audit
#   # * Use the :single option to define the list of auditable properties that need to be presented
#   #   with a single and always the same text in the audit log
#   #   This is the translation used for that text:
#   #   I18n.t("txt.admin.views.reports.tabs.audits.property.attribute.#{setting_name}")
#
#   auditable_properties :foo_setting, :single => [:single_text_setting]
#
#   # By default we use this text to present enabled/disabled values:
#   # I18n.t("txt.admin.views.reports.tabs.audits.property.#{enabled/disabled}")
#   # But you can customize those texts:
#
#  auditable_properties :suspended, translations: {
#    suspended_enabled: "txt.admin.views.reports.tabs.audits.property.attribute.suspended",
#    suspended_disabled: "txt.admin.views.reports.tabs.audits.property.attribute.unsuspended"
#  }
#
# end
module AuditableProperty
  AUDIT_ALL = [] # rubocop:disable Style/MutableConstant

  def self.included(base)
    base.class_attribute :audited_properties, :audited_properties_options
    base.send :extend, ClassMethods

    base.class_eval do
      include CIA::Auditable
      audit_attribute :value, if: :auditable_property?
    end
  end

  module ClassMethods
    def auditable_properties(*properties)
      options = properties.extract_options!

      self.audited_properties_options = options
      self.audited_properties = properties
      ::AuditableProperty::AUDIT_ALL << name if options[:audit_all]
    end
  end

  def audit_description(presenter)
    AuditablePropertyPresenter.new(presenter).description
  end

  def visible_property?
    audited_properties.include?(name.to_sym)
  end

  private

  def auditable_property?
    return false if !audited_properties_options[:audit_all] && !visible_property?
    return false if (audited_properties_options[:except] || []).include?(name.to_sym)

    # When a text property is defined for the first time as part of a general form submission,
    # we don't want to audit changes like: from nil to ""
    return false if value.blank? && value_was.blank?

    # When a text property is defined for the first time as part of a general form submission,
    # and the default value is false,
    # we don't want to audit changes: from nil to "false"
    return false if value_was.blank? && default_is_false? && false?

    true
  end

  def default_is_false?
    ["false", "0"].include?(self.class.default(name).to_s)
  end

  class AuditablePropertyPresenter
    attr_accessor :presenter

    delegate :old_value, :new_value, :source,
      :link_to_function, :multiline_description, :audit_value_not_set,
      to: :presenter

    def initialize(presenter)
      @presenter = presenter
    end

    def description
      if boolean?
        boolean_description
      else
        text_description
      end
    end

    private

    def boolean_change
      enabled? ? "enabled" : "disabled"
    end

    def boolean_description
      if key = source.audited_properties_options.fetch(:translations, {})[:"#{source.name}_#{boolean_change}"]
        I18n.t(key)
      else
        default_boolean_description
      end
    end

    def default_boolean_description
      I18n.t("txt.admin.views.reports.tabs.audits.property.#{boolean_change}")
    end

    def text_description
      link_name = I18n.t('txt.admin.views.reports.tabs.audits.change_without_details')

      tooltip = multiline_description(old_value.presence, new_value.presence)

      link_to_function(link_name, nil, rel: "tooltip", title: tooltip)
    end

    def boolean?
      enabled? || disabled?
    end

    def enabled?
      ["1", "true"].include?(new_value)
    end

    def disabled?
      ["0", "false"].include?(new_value)
    end
  end
end
