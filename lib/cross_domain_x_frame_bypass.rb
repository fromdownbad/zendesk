# See https://github.com/zendesk/zendesk/pull/16559 and https://support.zendesk.com/agent/tickets/1040434 for details. Safari 8.0.4 respects X-Frame-Options on a 302 redirect.

module CrossDomainXFrameBypass
  EXEMPTED_ACTIONS = {'registration' => %w[register], 'password_reset_requests' => %w[index create]}.freeze

  private

  def set_x_frame_options_header
    routes = current_account.routes.map(&:url)
    origin = Addressable::URI.parse(request.referer).try(:origin) rescue Addressable::URI::InvalidURIError
    exempted_actions = EXEMPTED_ACTIONS[controller_name]

    if routes.include?(origin) && exempted_actions.try(:include?, action_name)
      override_x_frame_options(::SecureHeaders::OPT_OUT)
    else
      super
    end
  end
end
