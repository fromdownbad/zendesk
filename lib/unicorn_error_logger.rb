module UnicornErrorLogger
  def log_error(logger, prefix, exc)
    super

    ZendeskExceptions::Logger.record(exc, location: self.class, message: prefix, fingerprint: '8981f9ce02310979aa9d81875751d14098bcea8f')
  end
end

Unicorn.singleton_class.prepend(UnicornErrorLogger)
