require 'active_record/coders/json'
require 'active_record/coders/yaml_column'

# Coder to use while converting from YAML to JSON
#
# You've been using `serialize` or `store` without arguments, and inadvertently
# stored your data as YAML. Before doing a backfill and converting all these
# data to JSON, you need this serializer which will write as JSON, but will read
# from both YAML and JSON.
class JsonWithYamlFallbackCoder
  def initialize
    @json_coder = ActiveRecord::Coders::JSON
    @yaml_coder = ActiveRecord::Coders::YAMLColumn.new(Object)
  end

  def dump(obj)
    @json_coder.dump(obj)
  end

  def load(json_or_yaml)
    @json_coder.load(json_or_yaml)
  rescue JSON::ParserError
    @yaml_coder.load(json_or_yaml)
  end
end
