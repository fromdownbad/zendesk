module Zopim
  class BaseType
    include Comparable

    attr_reader :name, :weight, :finance_name, :description

    def initialize(name:, weight:, finance_name: nil, description: nil)
      @name         = name
      @weight       = weight
      @finance_name = finance_name
      @description  = description || name
    end

    def self.types(attribute = :name)
      __types.sort.map { |type| type.send(attribute) }
    end

    def self.[](index)
      attribute = index.respond_to?(:downcase) ? :name : :weight
      value     = index.respond_to?(:downcase) ? index.downcase : index
      __types.detect { |type| type.send(attribute) == value }
    end

    # The name parameter here is what finance would designate as the custom/code
    # name for the derived type. In Zuora, for example, this would be equivalent
    # to plan_type__c
    def self.from_finance_name(name)
      __types.detect { |type| type.try(:finance_name) == name }
    end

    def self.finance_types
      __types.select { |type| type if type.finance_name.present? }
    end

    def <=>(other)
      weight <=> other.weight
    end

    def to_str
      name
    end

    def to_h
      {
        name: name,
        weight: weight,
        description: description,
        finance_name: finance_name
      }
    end

    def self.__types
      const_get(:TYPES)
    end

    def method_missing(name, *args)
      return (self.name == $1) if name =~ /^is_(.+)\?$/

      super(name, *args)
    end

    private_class_method :__types
  end
end
