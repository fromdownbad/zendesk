require 'zendesk/configuration'

module Zopim
  class Configuration < Zendesk::Configuration
    class << self
      private

      def config_file
        'zopim.yml'
      end
    end
  end

  def self.config
    Configuration
  end
end
