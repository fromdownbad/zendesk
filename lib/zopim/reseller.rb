require 'zopim_reseller_api'

module Zopim
  class Reseller
    include Singleton

    attr_reader :client

    def self.client
      instance.client
    end

    def initialize
      %w[base_url base_path rest_auth_prefix].each do |key|
        ZopimResellerApi::Client.send(key.to_sym, settings[key])
      end

      token, secret = *credentials.values_at('token', 'secret')
      @client = ZopimResellerApi.client(token: token, secret: secret)
    end

    private

    def credentials
      @credentials ||= Hash(Zopim.config[:reseller_api_credentials])
    end

    def settings
      @settings ||= Hash(Zopim.config[:reseller_api_client_settings])
    end
  end
end
