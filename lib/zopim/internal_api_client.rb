module Zopim
  class InternalApiClient
    EMOTICONS_MAP = {
      thumbs_up: '👍',
      thumbs_down: '👎',
    }.freeze

    def initialize(account_id)
      @account_id = account_id

      @client = Kragle.new('zopim').tap do |conn|
        conn.url_prefix = Zopim::Configuration.dig :scribe_api, :base_url
        conn.port = Zopim::Configuration.dig :scribe_api, :port
      end
    end

    def notify_account_settings_change(notification_type)
      # This is to make sure it still works for ip_settings_changed
      # TODO: Move Scribe to process ip_settings_changed with the new endpoint
      url = if notification_type == 'ip_settings_changed'
        '/system/notification/account'
      else
        '/system/notification/classic'
      end

      body = {
        account_id: @account_id,
        notification_type: notification_type,
      }
      @client.post(url, body)
    rescue Kragle::ResponseError, Faraday::Error => e
      message = "Failed to update Chat for account settings changes #{notification_type} for account #{@account_id}: #{e.message}"
      Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
      false
    end

    def update_agent_setting_avatar(agent_id, avatar_url)
      url = "/system/admin/accounts/#{@account_id}/agents/#{agent_id}/settings"
      body = {
        "avatar_path": avatar_url
      }
      @client.put(url, body)
    rescue Kragle::ResponseError, Faraday::Error => e
      message = "Failed update Chat avatar setting for agent #{agent_id} account #{@account_id}: #{e.message}"
      Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
      false
    end

    def get_migration_options(migration_name)
      url = "/system/migrations/#{migration_name}/options"
      with_kragle_wrapper do
        @client.get(url) do |request|
          request.params = { account_id: @account_id }
        end
      end
    end

    def start_migration(migration_name, migration_options)
      url = "/system/migrations/#{migration_name}"
      with_kragle_wrapper do
        @client.post(url, account_id: @account_id, **migration_options)
      end
    end

    def send_messaging_csat(chat_started_event)
      url = "/system/notification/messaging_csat_requests"
      with_kragle_wrapper do
        @client.post(url, csat_body(chat_started_event.ticket, chat_started_event.visitor_id))
      end
    end

    private

    def csat_body(ticket, visitor_id)
      I18n.with_locale(ticket.requester.translation_locale) do
        {
          account_id: @account_id,
          ticket_id: ticket.nice_id,
          visitor_id: visitor_id,
          rating_request_text: I18n.t('txt.chat.csat.rating_request_text'),
          comment_request_text: I18n.t('txt.chat.csat.comment_request_text'),
          feedback_text: I18n.t('txt.chat.csat.feedback_text'),
          rating_labels: {
            good: I18n.t('txt.chat.csat.rating_labels.good', EMOTICONS_MAP),
            bad: I18n.t('txt.chat.csat.rating_labels.bad', EMOTICONS_MAP),
          },
        }
      end
    end

    def with_kragle_wrapper
      yield
    rescue Kragle::ResponseError => e
      e.response
    end
  end
end
