module Zopim
  class AgentDeactivator
    def initialize(zopim_identity)
      @zopim_identity     = zopim_identity
      @user               = zopim_identity.user
      @account            = zopim_identity.account
      @zopim_subscription = zopim_identity.zopim_subscription
    end

    # Will implement proper deactivation for voltron_chat accounts in follow-up PR.
    def deactivate!
      return if !zopim_identity.is_linked? || zopim_identity.phase_three?
      disconnect_from_zopim!
    end

    private

    attr_reader :user, :zopim_identity, :account, :zopim_subscription

    delegate :zopim_account_id, to: :zopim_subscription
    delegate :zopim_agent_id,   to: :zopim_identity

    def disconnect_from_zopim!
      zopim_identity.is_owner? ? disable_record! : drop_record!
    rescue ZopimResellerApi::Middleware::Exception::NotFound
      # REF: https://zendesk.atlassian.net/browse/BILL-876
      Rails.logger.warn "Failed to remove Zopim identity for agent #{user.id}."
    end

    def disable_record!
      payload = { email: trashed_email }
      Reseller.client.update_account_agent!(
        id: zopim_account_id,
        agent_id: zopim_agent_id,
        data: payload
      )
    end

    def drop_record!
      Reseller.client.delete_account_agent!(
        id: zopim_account_id,
        agent_id: zopim_agent_id
      )
      Rails.logger.warn "Removed Zopim identity for agent #{user.id}."
    end

    def trashed_email
      email            = zopim_identity.email
      restrict_retrial = zopim_subscription.retrial_restricted?
      ZBC::Zopim::SubscriptionDeactivator.trashed_email(
        account.id,
        user.id,
        email,
        restrict_retrial
      )
    end
  end
end
