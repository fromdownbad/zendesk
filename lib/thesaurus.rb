class Thesaurus
  TAGGING_LIMIT = 4
  cattr_accessor(:autocomplete_limit, instance_accessor: false) { 1000 } # overwritten in tests

  class << self
    def autotag(text, account)
      return [] unless account.settings.ticket_auto_tagging?

      if words = extract_words(text)
        ActiveRecord::Base.on_slave do
          TagScore.where(account_id: account.id, tag_name: words).order("score DESC").limit(3).pluck(:tag_name).uniq
        end
      else
        []
      end
    end

    def complete(start, account, user)
      tag_names = []
      return [] if start.blank?
      if user.account.has_es_tags_autocomplete?
        options = { endpoint: 'tags/autocomplete' }
        options[:arturo_flags] = get_arturo_flags_for_user(user)
        results = Zendesk::Search.search(user, start, options)
        results.each do |tags|
          tag_names << tags['tag_name']
        end
      else
        tag_names = ActiveRecord::Base.on_slave { scored_tags(start.downcase, account) }
      end
      tag_names
    end

    private

    def scored_tags(start, account)
      TagScore.where(account_id: account.id).where("tag_name LIKE ?", "#{start}%").order("score DESC").limit(autocomplete_limit).pluck(:tag_name)
    end

    def extract_words(line)
      line  = line.to_s.strip_tags.gsub(/\S*:\/\/\S*/, '').strip
      words = {}

      line.downcase.scan(/[[:alnum:]_]+/).uniq.select { |w| w.size > 2 && w.to_i == 0 }.sort.each do |word|
        words[word[0..3]] ||= word
      end

      words.present? ? words.values : nil
    end

    def get_arturo_flags_for_user(user)
      arturo_flags = []
      if user.account.has_search_performance_monitoring?
        arturo_flags << :search_performance_monitoring
      end
      if user.account.has_time_based_indexing?
        arturo_flags << :time_based_indexing
      end
      if user.account.has_search_advanced_tags_autocomplete?
        arturo_flags << :search_advanced_tags_autocomplete
      end

      if user.account.has_search_return_limit_in_search_service?
        arturo_flags << :search_return_limit_in_search_service
      end

      arturo_flags
    end
  end
end
