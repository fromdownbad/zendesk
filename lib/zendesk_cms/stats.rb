module ZendeskCms
  module Stats
    def category_stats(account)
      category_stats = ActiveSupport::OrderedHash.new
      no_category = []

      Cms::Text.for_account(account.id).sort { |a, b| a.title.downcase <=> b.title.downcase }.each do |text|
        if text.title.index('::')
          category = text.title.split('::').first
          category_stats[category] = [] if category_stats[category].nil?
          category_stats[category] << text.id
        else
          no_category << text.id
        end
      end
      category_stats[I18n.t('txt.admin.models.cms.text.no_category')] = no_category if no_category.any?
      category_stats = {} if category_stats.length < 2
      category_stats
    end
  end
end
