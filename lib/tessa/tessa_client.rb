require_relative 'tessa_manifest_cache_entry'

module TessaClient
  class V1
    TESSA_PORT = ENV.fetch('TESSA_SERVICE_PORT', '1337')
    @statsd_clients = {}
    @project_fallbacks = ActiveSupport::HashWithIndifferentAccess.new

    class << self
      attr_reader :project_fallbacks
      # For fetches that occur as the result of a cache miss, there is no need to handle 304s since
      # there is by definition no cached manifest to use. However, when TessaCacheUpdater
      # periodically makes requests to update current, most requests will return the manifest that's
      # already in the cache. For this reason, Tessa will return a 304 if a qualifying
      # If-Modified-Since header is passed in the request.
      def fetch_and_cache_manifest(cache_key, uri, project, is_for_current: false, is_cache_update: false)
        response = nil
        manifest = nil

        if is_cache_update
          cache_entry = Rails.cache.read(cache_key)

          # We should not make a network request to update the cache entry in the following cases:
          #
          # * Another app server is currently making an attempt (that is still considered alive) to
          #   update the cache entry.
          # * The cache entry has just been updated by one of the app servers. There is no need to
          #   update an entry that was updated within the last period (which is specified as a
          #   constant in the TessaCacheUpdater).
          #
          if cache_entry && cache_entry.instance_of?(TessaManifestCacheEntry::V1) && !cache_entry.update_in_progress? && !cache_entry.fetched_recently?
            # NOTE: The return value for Rails.cache.write is only a boolean.
            updating_cache_entry = cache_entry.mark_as_being_updated!
            Rails.cache.write(cache_key, updating_cache_entry)

            response = connection.get do |req|
              req.url(uri)
              if updating_cache_entry && updating_cache_entry.last_modified
                req.headers['If-Modified-Since'] = updating_cache_entry.last_modified
              end
            end

            if response.status == 304
              updated_cache_entry = updating_cache_entry.unmark_as_being_updated!
              Rails.cache.write(cache_key, updated_cache_entry)
              dispatch_fetched_current_manifest_metrics(project, updated_cache_entry.manifest)
              return updated_cache_entry.manifest
            else
              # Just in case a race condition has occurred with a competing request response, we
              # should make sure that the response manifest timestamp is greater than or equal to
              # that of the manifest currently in the cache before updating it.
              manifest = JSON.parse(response.body)
              response_date = Date.parse(manifest['createdAt'])
              cached_date = Date.parse(updating_cache_entry.manifest['createdAt'])
              response_is_newest = response_date >= cached_date

              unless response_is_newest
                updated_cache_entry = updating_cache_entry.unmark_as_being_updated!
                Rails.cache.write(cache_key, updated_cache_entry)
                dispatch_fetched_current_manifest_metrics(project, updated_cache_entry.manifest)
                return updated_cache_entry.manifest
              end
            end
          elsif cache_entry && cache_entry.instance_of?(TessaManifestCacheEntry::V1)
            return cache_entry.manifest
          else
            response = connection.get(uri)
            manifest = JSON.parse(response.body)
          end
        else
          response = connection.get(uri)
          manifest = JSON.parse(response.body)
        end

        last_modified = response.headers['Last-Modified']
        Rails.cache.write(cache_key, TessaManifestCacheEntry::V1.new(manifest, last_modified))
        if is_for_current
          @project_fallbacks[project] = manifest
          dispatch_fetched_current_manifest_metrics(project, manifest)
        end
        manifest
      end

      def fetch_and_cache_latest_deployed_manifests(uri)
        Rails.cache.fetch(uri, expires_in: 20.seconds) do
          connection.get(uri).body
        end
      end

      def fetch_health
        connection.get('/z/ping')
      end

      def generate_uri_cache_key(uri)
        "tessa:v1:#{uri}"
      end

      def generate_uri_prefix(project)
        pod_id = Zendesk::Configuration.fetch(:pod_id)
        "/v1/projects/#{project}/pods/#{pod_id}"
      end

      def generate_current_uri(project)
        prefix = generate_uri_prefix(project)
        "#{prefix}/current"
      end

      def handle_faraday_error(project, error, module_name)
        error_class_type = error.class.to_s.gsub(/^.*::/, '')
        error_class_tag = "error:#{error_class_type}"
        statsd_client(module_name).increment('faraday_error', tags: [generate_project_tag(project), error_class_tag])
      end

      def generate_project_tag(project)
        "project:#{project}"
      end

      def statsd_client(module_name)
        cached_client = @statsd_clients[module_name]
        return cached_client if cached_client

        @statsd_clients[module_name] = Zendesk::StatsD::Client.new(namespace: ['tessa', module_name])
      end

      def tessa_host_url
        @tessa_host_url ||= begin
          pod_id = Zendesk::Configuration.fetch(:pod_id)
          host = ENV.fetch('TESSA_SERVICE_HOST', "pod-#{pod_id}.tessa.service.consul")

          "http://#{host}:#{TESSA_PORT}"
        end
      end

      private

      def connection
        Faraday.new(url: tessa_host_url) do |conn|
          conn.request :retry, max: 5, interval: 0.05, interval_randomness: 0.5, backoff_factor: 2
          # Raising is critical so that we don't end up caching JSON from 400+ responses.
          conn.use Faraday::Response::RaiseError
          conn.adapter Faraday.default_adapter
        end
      end

      def dispatch_fetched_current_manifest_metrics(project, manifest)
        # All "current" manifests should have a "version" attribute value. A default of "none" is
        # used here to prevent failures in the case that a regression appears in Tessa that allows
        # for a "current" manifest to lack a version.
        version_tag = "manifest_version:#{manifest['version'] || 'none'}"
        statsd_client(project).increment('fetched_current_manifest', tags: [generate_project_tag(project), version_tag])
      end
    end

    def initialize(project)
      @project = project
      @uri_prefix = self.class.generate_uri_prefix(@project)
    end

    def fetch_current_manifest
      cached_connection_fetch(current_uri, is_for_current: true)
    rescue StandardError
      # In the worst case scenario, we should still be able to serve SOME manifest, even if slightly
      # stale.
      handle_project_fallback
    end

    def fetch_manifest_by_sha(sha)
      unless sha.length == 40
        raise ArgumentError, 'Expected sha reference to be a full 40 character string, but it was not.'
      end
      cached_connection_fetch(sha_uri(sha))
    end

    def fetch_manifest_by_version(version)
      cached_connection_fetch(version_uri(version))
    end

    def fetch_latest_deployed_manifests
      uri = self.class.generate_uri_prefix(@project)
      self.class.fetch_and_cache_latest_deployed_manifests(uri)
    end

    private

    # The class method is needed for the cache updater script. Here we choose to use the same exact
    # method for the sake of consistency.
    def current_uri
      self.class.generate_current_uri(@project)
    end

    def sha_uri(sha)
      "#{@uri_prefix}/sha/#{sha}"
    end

    def version_uri(version)
      "#{@uri_prefix}/versions/#{version}"
    end

    def cached_connection_fetch(uri, is_for_current: false)
      cache_key = self.class.generate_uri_cache_key(uri)
      cache_entry = Rails.cache.read(cache_key)

      return cache_entry.manifest if cache_entry

      begin
        self.class.fetch_and_cache_manifest(cache_key, uri, @project, is_for_current: is_for_current)
      rescue StandardError => error
        self.class.handle_faraday_error(@project, error, 'client')
        raise
      end
    end

    def handle_project_fallback
      fallback = self.class.project_fallbacks[@project]

      handle_fallback_metric_dispatch(success: fallback.is_a?(Hash))

      fallback
    end

    def handle_fallback_metric_dispatch(success: true)
      project_tag = self.class.generate_project_tag(@project)
      fallback_tag = success ? 'fallback:success' : 'fallback:failure'
      self.class.statsd_client('client').increment('current_manifest_fallback', tags: [project_tag, fallback_tag])
    end
  end
end
