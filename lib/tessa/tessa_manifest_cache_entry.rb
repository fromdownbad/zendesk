module TessaManifestCacheEntry
  class V1
    attr_reader :manifest, :last_modified

    def initialize(manifest, last_modified)
      @manifest = manifest
      @last_modified = last_modified
      @update_attempt_start_time = nil
      @last_fetch_time = nil
    end

    def mark_as_being_updated!
      @update_attempt_start_time = Time.now
      self
    end

    def unmark_as_being_updated!
      @update_attempt_start_time = nil
      @last_fetch_time = Time.now
      self
    end

    def update_in_progress?
      return false if @update_attempt_start_time.nil?
      update_request_still_considered_alive?
    end

    def fetched_recently?
      return false if @last_fetch_time.nil?
      last_fetch_was_recent?
    end

    private

    def update_request_still_considered_alive?
      @update_attempt_start_time > (Time.now - TessaCacheUpdater::V1::PERIOD_IN_SECONDS)
    end

    def last_fetch_was_recent?
      @last_fetch_time >= (Time.now - TessaCacheUpdater::V1::PERIOD_IN_SECONDS)
    end
  end
end
