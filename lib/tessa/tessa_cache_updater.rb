require_relative 'tessa_client'

module TessaCacheUpdater
  class V1
    PERIOD_IN_SECONDS = 20

    class Runner
      def initialize(projects)
        @projects = projects
      end

      def run
        @projects.each do |project|
          continually_update_current_manifest_for_project(project)
        end
      end

      private

      def continually_update_current_manifest_for_project(project)
        # Spinning off a thread here allows for requests to be made concurrently. Note that there is
        # nothing synchronizing the requests to occur at the same time.
        Thread.new do
          Kernel.loop do
            Kernel.sleep(PERIOD_IN_SECONDS)
            update_current_manifest_for_project(project)
          end
        end
      end

      def update_current_manifest_for_project(project)
        uri = TessaClient::V1.generate_current_uri(project)
        cache_key = TessaClient::V1.generate_uri_cache_key(uri)
        TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, project, is_for_current: true, is_cache_update: true)
      rescue StandardError => error
        TessaClient::V1.handle_faraday_error(project, error, 'cache_updater')
      end
    end
  end
end
