# RAILS5UPGRADE: remove this
# This exists only to prevent code like params[:user].is_a?(Hash)
# from getting into master which is NOT true in Rails 5 but is true
# in rails 4
module Rails5ParamHashRegression
  def is_a?(klass)
    if klass.eql?(Hash) && !caller.first.match?(/gems/)
      ActiveSupport::Deprecation.warn("Oops! In Rails 5, Parameters no longer inherit from Hash. Try using HashParam.ish?(...)")
    end
    super
  end
end
unless Rails.env.production?
  ::ActionController::Parameters.include(Rails5ParamHashRegression)
end
