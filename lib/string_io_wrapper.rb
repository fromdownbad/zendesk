# A wrapper for the StringIO object. Wraps strings in a CGI file upload API. Usage:
#  s = StringIOWrapper.new(:data => 'hello', :filename => 'hello.txt', :content_type => 'text/plain')
#
#  s = StringIOWrapper.new
#  s << 'hello'
#  s.content_type = 'text/plain'
class StringIOWrapper < StringIO
  attr_accessor :content_type
  attr_accessor :original_filename

  def initialize(args = {})
    args[:data] ? super(args[:data]) : super()
    @content_type      = args[:content_type] || 'application/octet-stream'
    @original_filename = args[:filename] || 'unknown'
  end
end
