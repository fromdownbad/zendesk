# Uses Redis to store two sets of information about JobWithStatus jobs an
# account has running.
#
# The first is a list of the 100 last job UUIDs that an account has enqueued.
# The second is a set of job UUIDs that are in-flight (enqueued+running).
#
# To make a job class count against the accounts in-flight job limit, simply
# call `enforce_in_flight_limit` in the desired job class. By default, all job
# classes are grouped together and the limit is set by the account setting
# `total_in_flight_jobs_limit`.
#
# If you want to impose more granular controls, such as a job class specific
# limit, you can override the `in_flight_tracking_redis_key` and
# `in_flight_limit_for_account` class methods. Both of those methods will be
# passed the Account object the job was enqueued with.
#
# When a job class has `enforce_in_flight_limit` set, it will enqueue jobs up to
# the limit specified and then raise a `JobWithStatusTracking::TooManyJobs`
# exception. That exception has two attributes:
#
#  - job_class -> the class name of the job that enqueue was called for
#  - job_ids   -> the list of job UUIDs preventing enqueue
#
module JobWithStatusTracking
  IN_FLIGHT_TTL = ENV.fetch('JOBS_IN_FLIGHT_TTL', 3.hours).to_i

  def self.included(base)
    base.extend ClassMethods
  end

  class TooManyJobs < StandardError
    attr_reader :job_ids, :job_class

    def initialize(job_class, job_ids)
      @job_class = job_class
      @job_ids = job_ids
      super(job_class)
    end

    def to_s
      "Too many #{@job_class} jobs are currently queued or running. Try again later."
    end
  end

  module ClassMethods
    def enforce_in_flight_limit
      define_singleton_method(:enforce_in_flight_limit?) { true }
      private_class_method(:enforce_in_flight_limit?)
    end

    def enforce_in_flight_job_with_status_limit(job_options)
      return yield unless enforce_in_flight_limit?

      account   = find_account_from_options(job_options)
      redis_key = in_flight_tracking_redis_key(account)
      used = Zendesk::RedisStore.client.scard(redis_key)
      limit = in_flight_limit_for_account(account)

      set_inflight_job_limit_span(used, limit)
      if used >= limit
        unless bad_jobs?(account, redis_key)
          if job_options[:raise_on_too_many?]
            raise TooManyJobs.new(name, Zendesk::RedisStore.client.smembers(redis_key))
          else
            Rails.logger.error("JobWithStatusTracking::TooManyJobs would have been raised for account: #{account.id}, job class: #{name}")
          end
        end
      end

      uuid = yield
      Zendesk::RedisStore.client.sadd(redis_key, uuid)
      Zendesk::RedisStore.client.expire(redis_key, IN_FLIGHT_TTL)
      uuid
    end

    # If they have bad jobs don't raise and just remove the nil jobs in the redis store
    def bad_jobs?(account, redis_key)
      return false unless account
      has_bad_jobs = false
      Zendesk::RedisStore.client.smembers(redis_key).each do |uuid|
        # if the uuid does not exist in redis delete it in the RedisStore and flag for having bad jobs
        unless Resque.redis.exists(Resque::Plugins::Status::Hash.status_key(uuid))
          has_bad_jobs = true
          Zendesk::RedisStore.client.srem(redis_key, uuid)
        end
      end
      statsd_client.increment('bad_jobs_in_redis_memory', tags: ["subdomain:#{account&.subdomain},has_bad_jobs:#{has_bad_jobs}"])
      has_bad_jobs
    rescue StandardError
      false
    end

    def in_flight_tracking_redis_key(account)
      "Zendesk-JobWithStatus-InFlight-#{account.id}"
    end

    def in_flight_limit_for_account(account)
      account.settings.total_in_flight_jobs_limit || Float::INFINITY
    end

    private

    def statsd_client
      Zendesk::StatsD::Client.new(namespace: 'job_with_status_tracking')
    end

    def find_account_from_options(options)
      if options[:account]
        options[:account]
      elsif options[:account_id]
        Account.with_deleted { Account.find(options[:account_id]) }
      end
    end

    def enforce_in_flight_limit?
      false
    end

    def set_inflight_job_limit_span(used, limit)
      return unless active_span
      return unless active_span&.parent
      return unless limit.finite?
      return if limit <= 0

      parent = active_span&.parent
      parent.set_tag('http.rate_limit.inflight_job.limit', limit)
      parent.set_tag('http.rate_limit.inflight_job.used', used)
      parent.set_tag('http.rate_limit.inflight_job.remaining', limit - used)
      parent.set_tag('http.rate_limit.inflight_job.used_percentage', used * 100 / limit)
    end
  end

  private

  # Resque-status entries consume memory in Redis. They have a TTL, but a spike
  # in traffic could exhaust Redis's memory. Track every status that's created,
  # and delete statuses before they'd normally expire if the account exceeds
  # their limit.
  def enforce_per_account_job_status_limit
    remove_expired_job_statuses

    # Track the key for this job's status
    Zendesk::RedisStore.client.rpush(job_status_tracking_redis_key, uuid)

    # Delete other statuses for this account until they're under the limit
    if Zendesk::RedisStore.client.llen(job_status_tracking_redis_key) > current_account.settings.job_statuses_limit
      if popped_uuid = Zendesk::RedisStore.client.lpop(job_status_tracking_redis_key)
        Resque.redis.del(Resque::Plugins::Status::Hash.status_key(popped_uuid))
      end
    end
  end

  def remove_job_from_in_flight_list
    redis_key = self.class.in_flight_tracking_redis_key(current_account)

    Zendesk::RedisStore.client.srem(redis_key, uuid)
    Zendesk::RedisStore.client.expire(redis_key, IN_FLIGHT_TTL)
  end

  def job_status_tracking_redis_key
    "Zendesk-JobWithStatusQueue-#{current_account.id}"
  end

  # Remove entries from the head of our tracking list Redis has EXPIRE'd
  def remove_expired_job_statuses
    while key = next_expired_key_in_redis
      Zendesk::RedisStore.client.lrem(job_status_tracking_redis_key, 1, key)
    end
  end

  def next_expired_key_in_redis
    return unless key = Zendesk::RedisStore.client.lindex(job_status_tracking_redis_key, 0)
    return if Resque.redis.exists(Resque::Plugins::Status::Hash.status_key(key))
    key
  end
end
