module Billing
  module JWT
    # Create a JWT token (in string format) using :content as its payload.
    # NOTE: This token created is intended for consumption in Billing.
    def self.encrypt(issuer, payload)
      sender_key   = ENV.fetch('CLASSIC_RSA_PRIVATE_KEY')
      receiver_key = ENV.fetch('BILLING_RSA_PUBLIC_KEY')
      claims       = { iss: issuer, iat: Time.now, content: payload }
      encryptor    = Encryptor.new(sender_key, receiver_key)
      encryptor.encrypt(claims).to_s
    end

    # Extract payload/content from an encrypted JWT :token.
    # NOTE: The token is expected to be from Billing.
    def self.decrypt(token)
      sender_key   = ENV.fetch('BILLING_RSA_PUBLIC_KEY')
      receiver_key = ENV.fetch('CLASSIC_RSA_PRIVATE_KEY')
      decryptor    = Decryptor.new(sender_key, receiver_key)
      decryptor.decrypt(token)
    end
  end
end
