module Billing
  module JWT
    class Decryptor
      class Error < StandardError; end

      EXPIRY_LEEWAY = 15.seconds.freeze

      # Decrypt the token then unencode to unpack and return the claims when it
      # passes verification.
      def decrypt(token)
        jwe = JSON::JWT.decode(token, receiver_key)        # decrypt token
        jwt = JSON::JWT.decode(jwe.plain_text, sender_key) # unpack claims
        verify(jwe, jwt) && jwt # verify and return claims
      end

      private

      attr_reader :sender_key, :receiver_key

      def initialize(sender_key, receiver_key)
        @sender_key   = OpenSSL::PKey::RSA.new(sender_key)
        @receiver_key = OpenSSL::PKey::RSA.new(receiver_key)
      end

      JWE_ALG = 'RSA1_5'.freeze # JSON Web Encryption Algorithm
      JWT_ALG = 'RS256'.freeze  # JSON Web Token Algorithm

      def verify(jwe, jwt)
        fail Error unless (jwe.alg == JWE_ALG) && (jwt.alg == JWT_ALG)

        now    = Time.now.to_i
        before = now - EXPIRY_LEEWAY
        after  = now + EXPIRY_LEEWAY
        fail Error unless jwt[:iat].between?(before, after)

        true
      end
    end
  end
end
