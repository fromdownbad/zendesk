module Billing
  module JWT
    class Encryptor
      def encrypt(claims)
        jwt        = JSON::JWT.new(claims)
        signed_jwt = jwt.sign(sender_key, :RS256)
        signed_jwt.encrypt(receiver_key, :RSA1_5, :'A128CBC-HS256')
      end

      private

      attr_reader :sender_key,
        :receiver_key

      def initialize(sender_key, receiver_key)
        @sender_key   = OpenSSL::PKey::RSA.new(sender_key)
        @receiver_key = OpenSSL::PKey::RSA.new(receiver_key)
      end
    end
  end
end
