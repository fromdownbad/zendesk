module Billing
  class CustomerOrder::Client
    private_class_method :new

    PATH = '/billing/api/v1/orders/classic'.freeze
    TOKEN_ISSUER = 'zendesk'.freeze

    attr_reader :account,
      :user,
      :zendesk_max_agents

    def self.post(account, user, zendesk_max_agents)
      new(account, user, zendesk_max_agents).post
    end

    def initialize(account, user, zendesk_max_agents)
      @account            = account
      @user               = user
      @zendesk_max_agents = zendesk_max_agents
    end

    def post
      response = connection.post(PATH, params)
      response.success?
    end

    private

    def params
      {
        issuer: TOKEN_ISSUER,
        token: encrypted_token
      }.merge(customer_order_payload)
    end

    def customer_order_payload
      Billing::CustomerOrder::Payload.build(account, zendesk_max_agents)
    end

    def encrypted_token
      ZBC::Billing::JWT::Token.build(
        TOKEN_ISSUER, payload
      ).to_s
    end

    def payload
      {
        master_account_id: account.id,
        user_id:           user.id,
      }
    end

    def connection
      @connection ||= Kragle.new('billing', retry_options: { max: 2 }).tap do |connection|
        host = ZBC::Environment::Host.build('billing')
        connection.url_prefix = "https://#{host}"
        connection.headers['Host'] = host
        connection.options.timeout = 60
      end
    end
  end
end
