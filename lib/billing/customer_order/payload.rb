module Billing
  class CustomerOrder::Payload
    private_class_method :new

    def self.build(account, zendesk_max_agents)
      new(account, zendesk_max_agents).build
    end

    TOKEN_ISSUER = 'zendesk'.freeze

    attr_reader :account,
      :zendesk_max_agents

    def build
      {
        data: {
          type: "customer_order",
          attributes: {
            items: [
              {
                target: {
                  type: "product",
                  id: "zendesk"
                },
                operation: {
                  type: "update",
                  value: {
                    quantity: zendesk_max_agents
                  }
                }
              }
            ]
          }
        },
        meta: {
          zuora_subscription_id: current_subscription_id
        }
      }.freeze
    end

    private

    def initialize(account, zendesk_max_agents)
      @account            = account
      @zendesk_max_agents = zendesk_max_agents
    end

    def current_subscription_id
      zuora_client.get_current_subscription!.try(:id)
    end

    def zuora_client
      ZBC::Zuora::Client.new(account.billing_id)
    end
  end
end
