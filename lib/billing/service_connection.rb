module Billing
  class ServiceConnection
    private_class_method :new

    def self.build(account, user, options = {})
      new(account, user, options).build
    end

    def build
      builder.build(*params)
    end

    private

    attr_reader :account,
      :user,
      :options

    def initialize(account, user, options)
      @account = account
      @user    = user
      @options = use_v2? ? options : options[:params]
    end

    def params
      [account, user, options]
    end

    def builder
      use_v2? ? V2 : V1
    end

    def use_v2?
      billing_primary_sync? || sales_assisted? || multi_product?
    end

    def multi_product?
      @multi_product ||= account.billing_multi_product_participant? &&
        user.is_verified?
    end

    def billing_primary_sync?
      account.has_billing_primary_sync?
    end

    # NOTE: We avoid changing the account.billing_multi_product_participant?
    # logic because it could affect sync. So instead of removing the
    # self_service requirement on billing_multi_product_participant
    # we circumvent the call altogether, here, if it is sales_assisted?,
    # leaving the other logic intact.
    def sales_assisted?
      account.subscription.assisted?
    end
  end
end
