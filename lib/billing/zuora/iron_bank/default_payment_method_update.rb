module Billing
  module Zuora
    module IronBank
      # IronBank default payment method update service
      class DefaultPaymentMethodUpdate
        def self.call(opts)
          request  = Request.build(opts)
          response = ::IronBank::Update.call(request)

          Response.build(response)
        end

        # Request builder for DefaultPaymentMethodUpdate
        class Request
          include ZBC::Zuora::HelperMethods

          private_class_method :new

          CREDIT_CARD_BATCH = ENV.fetch(
            'BILLING_ZUORA_CREDIT_CARD_BATCH', 'Batch20'
          ).freeze

          def self.build(opts)
            new(opts).build
          end

          def build
            { type: :account, objects: [request_object] }
          end

          private

          attr_reader :zuora_account_id, :payment_method_id, :currency

          def initialize(zuora_account_id:, payment_method_id:, currency:)
            @zuora_account_id  = zuora_account_id
            @payment_method_id = payment_method_id
            @currency          = currency
          end

          def request_object
            {
              id:                           zuora_account_id,
              auto_pay:                     true,
              default_payment_method_id:    payment_method_id,
              batch:                        CREDIT_CARD_BATCH,
              invoice_template_id:          invoice_template_id,
              invoice_delivery_prefs_email: true,
              dunning_state__c:             'OK',
              communication_profile_id:     communication_profile_id
            }
          end

          def invoice_template_id
            selector = ZBC::Zuora::InvoiceSelector.new(
              env:      Rails.env,
              currency: currency
            )

            selector.invoice_template_id
          end
        end

        # Response builder for DefaultPaymentMethodUpdate
        class Response < SimpleDelegator
          private_class_method :new

          def self.build(response)
            new(response).build
          end

          def build
            [__getobj__].flatten.first.fetch(:success, false)
          end
        end
      end
    end
  end
end
