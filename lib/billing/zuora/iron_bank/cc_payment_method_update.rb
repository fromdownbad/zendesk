module Billing
  module Zuora
    module IronBank
      class CCPaymentMethodUpdate
        private_class_method :new

        def self.call(opts)
          new(opts).call
        end

        def call
          update_bill_to_from_payment
          append_log_attributes
          update_default_payment_method
          synchronize_zuora_account!
        end

        private

        attr_reader :zuora_account_id, :payment_method_reference_id, :currency

        def initialize(zuora_account_id:, payment_method_reference_id:, currency:)
          @zuora_account_id            = zuora_account_id
          @payment_method_reference_id = payment_method_reference_id
          @currency                    = currency
        end

        def update_default_payment_method
          Billing::Zuora::IronBank::DefaultPaymentMethodUpdate.call(
            zuora_account_id:  zuora_account_id,
            payment_method_id: payment_method_reference_id,
            currency:          currency
          )
        end

        def update_bill_to_from_payment
          ZBC::Zuora::ContactsService.update_bill_to_from_payment!(
            zuora_account_id,
            payment_method_reference_id
          )
        end

        def append_log_attributes
          Rails.logger.info(reference_id: payment_method_reference_id)
        end

        def synchronize_zuora_account!
          ZBC::Zuora::Synchronizer.synchronize!(zuora_account_id)
        end
      end
    end
  end
end
