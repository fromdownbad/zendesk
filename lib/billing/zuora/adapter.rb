module Billing
  module Zuora
    class Adapter
      def initialize(params = {})
        @params = params
      end

      def update_cc_payment_method(payment_method_reference_id)
        Billing::Zuora::IronBank::CCPaymentMethodUpdate.call(
          zuora_account_id:            zuora_account_id,
          payment_method_reference_id: payment_method_reference_id,
          currency:                    currency
        )
      end

      private

      attr_reader :params

      def current_account
        params.fetch(:current_account)
      end

      def zuora_client
        params.fetch(:zuora_client)
      end

      def zuora_account_id
        params.fetch(:zuora_account_id)
      end

      def currency
        current_account.subscription.currency_type
      end
    end
  end
end
