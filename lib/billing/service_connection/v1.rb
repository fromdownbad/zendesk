module Billing
  class ServiceConnection::V1
    private_class_method :new

    def self.build(account, user, params = {})
      new(account, user, Hash(params)).build
    end

    def build
      {
        url:     url,
        token:   token,
        payload: payload
      }.freeze
    end

    private

    def initialize(account, user, params = {})
      @account = account
      @user    = user
      @params  = params
    end

    attr_reader :account,
      :user,
      :params

    # TODO: Create a Zendesk::Types::ProductType for the various product types
    # that we support.
    def payload
      {
        product_type:      2, # 1: zopim, 2: classic
        product_id:        product_id,
        user_id:           user.id,
        user_name:         account.owner.name,
        user_email:        account.owner.email,
        organization_name: account.subdomain
      }
    end

    TOKEN_ISSUER = 'zendesk'.freeze

    def token
      @token ||= Billing::JWT.encrypt(TOKEN_ISSUER, payload)
    end

    def host
      ZBC::Environment::Host.build(account.subdomain)
    end

    def query_values
      { issuer: TOKEN_ISSUER, token: token }.reverse_merge(params)
    end

    def url
      Addressable::URI.new(
        scheme:       'https',
        host:         host,
        path:         '/billing',
        query_values: query_values
      ).to_s
    end

    def product_id
      account.id.to_s
    end
  end
end
