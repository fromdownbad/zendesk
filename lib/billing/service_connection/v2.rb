module Billing
  # Usage:
  #
  #   service_connection = Billing::ServiceConnection::V2.build(
  #     account,
  #     base_url: 'https://trial.zd-dev.com',
  #     params: {
  #       foo: 'bar',
  #       ...
  #     }
  #   )
  #   service_connection.build # => { url: ..., payload: ... }
  #
  class ServiceConnection::V2
    private_class_method :new

    def self.build(account, user, options = {})
      new(account, user, Hash(options)).build
    end

    def build
      {
        url:     url,
        token:   token,
        payload: payload,
      }.freeze
    end

    private

    BILLING_SERVICE_FQDN           = ENV.fetch('BILLING_SERVICE_FQDN').freeze
    TOKEN_ISSUER                   = 'zendesk'.freeze
    ADMIN_CENTER_SUBSCRIPTION_PAGE = '/admin/billing/overview'.freeze
    LOTUS_SUBSCRIPTION_PAGE        = '/agent/admin/subscription'.freeze

    attr_reader :account,
      :user,
      :options,
      :account_url,
      :notifications_url

    delegate :subscription,
      to: :account

    delegate :currency_type,
      to: :subscription

    delegate :base_url,
      :params,
      to: :options

    def initialize(account, user, options = {})
      @account           = account
      @user              = user
      @options           = OpenStruct.new(options)
      @account_url       = "#{base_url}#{subscription_page_url}"
      @notifications_url = "#{base_url}/api/v2/billing/notification"
    end

    def subscription_page_url
      account.multiproduct? ? ADMIN_CENTER_SUBSCRIPTION_PAGE : LOTUS_SUBSCRIPTION_PAGE
    end

    def url
      Addressable::URI.new(
        scheme:       'https',
        host:         BILLING_SERVICE_FQDN,
        path:         '/billing/api/purchase/product',
        query_values: query_values
      ).to_s
    end

    def payload
      {
        master_account_id: account.id,
        account_url:       account_url,
        user_id:           user.id,
        currency_id:       currency_type,

        # NOTE: DEPRECATED. Temporarily retained so Billing can send out
        # notifications to the service that owns the product (in this case
        # Classic)
        #
        # Ideally Classic (and similar services) should just subscribe to
        # Maxwell to detect if there are changes to the account that needs to be
        # syncd.
        #
        notifications_url: notifications_url
      }
    end

    # NOTE: The issuer param is included for backwards-compatibility. We should
    # be able to get rid of it once we have GA'd Billig MultiProduct
    # (Arturo: :billing_multi_product_participation)
    def query_values
      Hash(params).merge(
        token:  token,
        issuer: TOKEN_ISSUER,
        origin: TOKEN_ISSUER
      )
    end

    def token
      @token ||= Billing::JWT.encrypt(TOKEN_ISSUER, payload)
    end
  end
end
