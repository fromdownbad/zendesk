module MobileSessionPreservation
  private

  # This makes sure that when logging out through the mobile ui
  # you are not redirected back to the desktop site.
  def preserve_mobile_preference
    is_mobile = session[Schmobile::IS_MOBILE]
    yield
  ensure
    session[Schmobile::IS_MOBILE] = is_mobile
  end
end
