class ContentUrlBuilder
  def url_for(attachment, options = {})
    return "" unless attachment
    attachment.account.url(options) + filename(attachment)
  end

  def filename(attachment)
    if public_filename?(attachment)
      options = { raw_id_in_path: attachment.account.has_no_partitioning_of_id_in_attachment_url? }

      attachment.public_filename(nil, options)
    else
      attachment.url(relative: true)
    end
  end

  private

  def public_filename?(attachment)
    attachment.is_a?(Photo) || attachment.is_a?(BrandLogo)
  end
end
