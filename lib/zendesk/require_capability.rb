module Zendesk
  module RequireCapability
    def require_capability(name, options = {})
      before_action(options) do |controller|
        controller.deny_access unless controller.send(:current_account).send("has_#{name}?")
      end
    end
  end
end
