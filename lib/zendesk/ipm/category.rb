require 'active_support/concern'

module Zendesk::Ipm
  module Category
    extend ActiveSupport::Concern

    CATEGORIES = [
      ['New Feature',    1],
      ['Announcement',   2],
      ['Tips & Tricks',  3],
      ['Upcoming Event', 4],
      ['VIP Offer',      5]
    ].freeze

    included do
      attr_accessible :category_code

      validates :category_code,
        presence: true,
        numericality: {
          only_integer: true,
          greater_than_or_equal_to: CATEGORIES.first.last,
          less_than_or_equal_to: CATEGORIES.last.last
        }
    end
  end
end
