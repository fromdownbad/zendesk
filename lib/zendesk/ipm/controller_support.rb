module Zendesk
  module Ipm
    module ControllerSupport
      def self.included(base)
        base.helper_method :current_user_alerts
      end

      protected

      def current_user_alerts(options = {})
        options = { interface: options[:interface], custom: build_custom_factors }
        ::Ipm::Alert.for_user(current_user, options)
      end

      def build_custom_factors
        custom_factors_found = []

        # Add the different custom factors here
        custom_factors_found << :remote_auth_retired if auth_via?(:remote) && current_user.is_admin?

        custom_factors_found << :account_needs_ssl_certificate if current_user.is_admin? && current_account.brand_host_mappings.any? && current_account.certificates.active.empty? && !current_account.is_ssl_enabled?

        custom_factors_found << :owner_email_verification_required if current_user.is_account_owner? && !current_user.is_verified?
        custom_factors_found << :user_account_has_fb_pages_with_messages_enabled if user_account_has_fb_pages_with_messages_enabled?

        custom_factors_found
      end

      private

      def user_account_has_fb_pages_with_messages_enabled?
        ::Facebook::Page.where(account_id: current_user.account_id, state: ::Facebook::Page::State::ACTIVE).
          any? { |page| page.page_settings[:include_messages] }
      end
    end
  end
end
