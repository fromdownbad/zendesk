module Zendesk::Ipm
  module ActsAsConstrainable
    extend ActiveSupport::Concern

    included do
      has_many :ipm_constraints,
        as:         :constrainable,
        dependent:  :destroy,
        class_name: 'Ipm::Constraint'

      class << self
        def constraint(name, options = {})
          (@constraints ||= []) << name
          if options.key? :source
            masked_attr name, *options.values_at(:source, :view, :store)
          else
            options.reverse_merge!(type: :String)
            unmasked_attr name, *options.values_at(:type)
          end
        end

        private

        def masked_attr(name, source, view = nil, store = nil)
          define_masked_reader(name, source, view)
          define_masked_writer(name, source, store)
        end

        def unmasked_attr(name, type)
          define_unmasked_reader(name, type)
          define_unmasked_writer(name, type)
        end

        def define_masked_reader(name, source, view = nil)
          define_method name do
            view ||= :"#{name}_view"
            send view, send(source)
          end
        end

        def define_masked_writer(name, source, store = nil)
          define_method "#{name}=" do |value|
            store ||= :"#{name}_store"
            send("#{source}=", send(store, value))
          end
        end

        def define_unmasked_reader(name, type)
          define_method name do
            data = peek(name.to_s)
            if data.new_record?
              fetch_and_store_legacy_data(name)
            else
              data = data.value.to_s
              data = inflate(data) unless data.blank?
              send(type, data)
            end
          end
        end

        def define_unmasked_writer(name, type)
          name = name.to_s
          define_method "#{name}=" do |value|
            data = (type == :String ? value : normalize(value))
            if data.blank?
              clear(name)
            else
              poke(name, deflate(data))
            end
          end
        end
      end

      def self.const_missing(name)
        return @constraints if name == :CONSTRAINTS
        super(name)
      end
    end

    protected

    def subdomains_view(source)
      Account.where(id: source).pluck(:subdomain).join(', ')
    end

    def subdomains_store(source)
      subdomain = source.split(/,| /).uniq.delete_if(&:blank?)
      Route.where(subdomain: subdomain).pluck(:account_id)
    end

    alias_method :exclude_subdomains_view,  :subdomains_view
    alias_method :exclude_subdomains_store, :subdomains_store

    private

    def peek(name)
      ipm_constraint_by_name(name) || ipm_constraints.build(name: name)
    end

    def poke(name, value)
      if new_record?
        ipm_constraints.build(name: name, value: value)
      else
        peek(name).update_attribute(:value, value)
      end
    end

    def clear(name)
      constraint = ipm_constraint_by_name(name)
      constraint.destroy if constraint && constraint.persisted?
    end

    def ipm_constraint_by_name(name)
      ipm_constraints.detect { |c| c.name == name }
    end

    def IntArray(value) # rubocop:disable Naming/MethodName
      value.to_s.split(',').map(&:to_i)
    end

    def StrArray(value) # rubocop:disable Naming/MethodName
      value.to_s.split(',')
    end

    def normalize(string_or_array)
      case string_or_array
      when String   then string_or_array.split(/,| /)
      when Array    then string_or_array
      when NilClass then Array(string_or_array)
      else raise "String or Array expected but was #{string_or_array.class}"
      end.uniq.delete_if(&:blank?).join(',')
    end

    def inflate(value)
      Zlib::Inflate.inflate(Base64.decode64(value))
    rescue Zlib::BufError => e
      Rails.logger.warn("Zendesk Monitor error while inflating value: #{e}")
      ''
    end

    def deflate(value)
      Base64.encode64(Zlib::Deflate.deflate(value))
    end

    def fetch_and_store_legacy_data(name)
      fetch_legacy_data(name).tap do |data|
        send("#{name}=", data)
      end
    end

    def fetch_legacy_data(name)
      (name == :custom_factors) ? send(:[], name) : constraints.send(name)
    end
  end
end
