module Zendesk::Ipm
  module ConstraintsMatcher
    def matches_user_and_interface?(user, interface)
      return false if account_excluded?(user)

      user.is_agent?                  &&
        pod_matches?                  &&
        shard_matches?(user)          &&
        role_matches?(user)           &&
        account_type_matches?(user)   &&
        plan_matches?(user)           &&
        interface_matches?(interface) &&
        language_matches?(user)       &&
        account_id_matches?(user)
    end

    def account_excluded?(user)
      return false if exclude_account_ids.blank?
      exclude_account_ids.include?(user.account_id)
    end

    def account_id_matches?(user)
      account_ids.blank? || account_ids.include?(user.account_id)
    end

    def language_matches?(user)
      languages.blank? || languages.include?(user.translation_locale.id)
    end

    def role_matches?(user)
      roles.blank? ||
        roles.include?(user.role) ||
        roles.include?('Owner') && user.is_account_owner?
    end

    def account_type_matches?(user)
      account = user.account
      account_types.blank? ||
        account.subscription &&
        account_types.include?(account.subscription.account_type)
    end

    def plan_matches?(user)
      account = user.account
      plans.blank? ||
        (account &&
         account.subscription &&
         plans.include?(account.subscription.plan_type))
    end

    def shard_matches?(user)
      shards.blank? || shards.include?(user.account.shard_id)
    end

    def pod_matches?
      pods.blank? || pods.include?(Zendesk::Configuration.fetch(:pod_id))
    end

    def interface_matches?(interface)
      interface.blank? || interfaces.blank? || interfaces.include?(interface)
    end
  end
end
