module Zendesk::Ipm
  class ConstraintSet
    INTEGER_CONSTRAINTS = [
      :pods,
      :shards,
      :plans,
      :languages,
      :account_ids,
      :exclude_account_ids
    ].freeze

    CONSTRAINTS = [
      :internal_name,
      :roles,
      :account_types,
      :interfaces
    ] + INTEGER_CONSTRAINTS

    ALLOWED_ACCOUNT_TYPES = ['trial', 'customer', 'sponsored', 'spoke', 'sandbox', 'unknown'].freeze
    ALLOWED_ROLES = ["Owner", "End-user", "Admin", "Agent", "Legacy Agent"].freeze
    ALLOWED_INTERFACES = ["classic", "lotus"].freeze

    module ClassMethods
      def define_string_set_constraint(constraint, allowed)
        allowed_values[constraint.to_sym] = allowed

        class_eval <<-METHOD
        def #{constraint}=(values)
          @#{constraint} = normalize_and_check_values(:#{constraint}, values)
        end
        METHOD
      end

      def define_integer_set_constraint(constraint)
        class_eval <<-METHOD
        def #{constraint}=(values)
          @#{constraint} = normalize_integer_set_values(values)
        end
        METHOD
      end

      def define_string_constraint(constraint)
        class_eval <<-METHOD
        def #{constraint}=(value)
          @#{constraint} = value
        end
        METHOD
      end

      def allowed_values
        @allowed_values ||= {}
      end

      def load(string)
        if string.blank?
          new
        else
          new JSON.parse(string)
        end
      end

      def dump(constraint_set)
        hash = {}

        CONSTRAINTS.each do |field|
          value = constraint_set.send(field)
          hash[field] = value unless value.nil?
        end

        JSON.dump(hash)
      end
    end

    extend ClassMethods
    include ConstraintsMatcher

    attr_reader *CONSTRAINTS

    def initialize(hash = {})
      hash = hash.with_indifferent_access

      CONSTRAINTS.each do |constraint|
        values_for_constraint = hash[constraint]
        send "#{constraint}=", values_for_constraint if values_for_constraint
      end
    end

    # integer array constraints
    INTEGER_CONSTRAINTS.each do |constraint|
      define_integer_set_constraint(constraint)
    end

    define_string_set_constraint :roles,         ALLOWED_ROLES
    define_string_set_constraint :account_types, ALLOWED_ACCOUNT_TYPES
    define_string_set_constraint :interfaces,    ALLOWED_INTERFACES
    define_string_constraint :internal_name

    private

    def normalize_and_check_values(constraint, values)
      unique_values = normalize_array_input(values).uniq
      if (unique_values - allowed_values_for_constraint(constraint)).any?
        raise InvalidConstraintValue
      end
      unique_values.presence
    end

    def normalize_integer_set_values(values)
      normalize_array_input(values).map { |i| Integer(i) }.uniq.sort.presence
    rescue ArgumentError
      raise InvalidConstraintValue
    end

    def allowed_values_for_constraint(constraint)
      self.class.allowed_values[constraint]
    end

    def normalize_array_input(values)
      if values.is_a?(String)
        values = values.split(/,|\s/)
      end

      values = Array.wrap(values)

      if values.is_a?(Array)
        values -= [""]
      end

      values
    end

    class InvalidConstraintValue < StandardError; end
  end
end
