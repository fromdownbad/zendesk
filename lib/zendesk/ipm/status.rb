module Zendesk::Ipm
  module Status
    extend ActiveSupport::Concern

    # supported possible values for the state column
    DISABLED  = 0
    LIVE      = 1
    SCHEDULED = 2

    ENABLED_NOTIFICATIONS_SCOPE = <<-SQL.freeze
      (enabled IS TRUE) OR
      (state = #{LIVE}) OR
      ((state = #{SCHEDULED}) AND
       (((? >= start_date) AND (end_date IS NULL)) OR
       ((? <= end_date) AND (start_date IS NULL)) OR
       (? BETWEEN start_date AND end_date)))
    SQL

    included do
      before_save :sync_enabled_flag, unless: 'state.nil?'
      before_save :reset_start_and_end_date, unless: "state == #{SCHEDULED}"

      attr_accessible :ipm_constraints

      validates :start_date,
        presence: true,
        if: "state == #{SCHEDULED}",
        unless: 'end_date.present?'
      validates :end_date,
        presence: true,
        if: "state == #{SCHEDULED}",
        unless: 'start_date.present?'

      validate :start_date_is_before_end_date,
        if: "state == #{SCHEDULED}"

      # PR:18120 - Not using NOW() because it is apparently quite slow.
      scope :enabled, -> { t = Time.now; where(ENABLED_NOTIFICATIONS_SCOPE, t, t, t) }
    end

    # TODO: we should eventually deprecate this column and replace all references
    # to #enabled? with calls to #active?
    def enabled?
      state.nil? ? !!self[:enabled] : active?
    end

    # TODO: should eventually override #enabled?
    def active?
      live? || scheduled?
    end

    def status
      case state
      when DISABLED  then :disabled
      when LIVE      then :live
      when SCHEDULED then scheduled? ? :scheduled_active : :scheduled_inactive
      else :unknown
      end
    end

    private

    def start_date_is_before_end_date
      if start_date.present? && end_date.present? && start_date > end_date
        errors.add(:start_date, 'must be before end date')
      end
    end

    def sync_enabled_flag
      self[:enabled] = active?
      true # important! otherwise the callback chain may be broken
    end

    def reset_start_and_end_date
      self[:start_date] = nil
      self[:end_date]   = nil
      true # important! otherwise the callback chain may be broken
    end

    def live?
      LIVE == state
    end

    def scheduled?
      (SCHEDULED == state) && in_schedule?
    end

    def in_schedule?
      now = Time.now
      min = start_date
      max = end_date

      if min && max
        (min..max).cover? now
      elsif min
        min <= now
      elsif max
        max >= now
      else
        false
      end
    end
  end
end
