# These are the common constraints among IPM's (ie: Alert,
# FeatureNotification)
#
# Constraints that are specific to certain IPMs should be declared in the
# specific IPM model, eg:
#
#    class Ipm::Alert
#      include Ipm::ActsAsConstrainable
#      include Ipm::CommonConstraints
#
#      constraint :custom_factors, :type => :StrArray
#    end
#
# To list the constraints that are declared in an IPM model, you can
# consult the CONSTRAINTS constant, eg:
#
#   Ipm::Alert::CONSTRAINTS # => [:internal_name, :pods, :shards, ...]
#
module Zendesk::Ipm
  module CommonConstraints
    extend ActiveSupport::Concern

    included do
      # note: this is so we can fetch legacy data (if needed)
      serialize :constraints, ConstraintSet

      constraint :internal_name
      constraint :pods,                type: :IntArray
      constraint :shards,              type: :IntArray
      constraint :roles,               type: :StrArray
      constraint :languages,           type: :IntArray
      constraint :account_types,       type: :StrArray
      constraint :interfaces,          type: :StrArray
      constraint :plans,               type: :IntArray
      constraint :account_ids,         type: :IntArray
      constraint :exclude_account_ids, type: :IntArray

      constraint :subdomains,          source: :account_ids
      constraint :exclude_subdomains,  source: :exclude_account_ids

      attr_accessible :internal_name, :pods, :shards, :roles, :languages,
        :account_types, :interfaces, :plans, :subdomains, :exclude_subdomains,
        :account_ids

      validates :constraints, presence: true
    end
  end
end
