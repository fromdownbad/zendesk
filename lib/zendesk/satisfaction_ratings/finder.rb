module Zendesk
  module SatisfactionRatings
    # Used in SatisfactionRatings controller to find ratings.
    # Takes account and params hash as arguments.
    #
    #   Zendesk::SatisfactionRatings::Finder.new(
    #     current_account,
    #     params
    #   ).satisfaction_ratings
    class Finder
      attr_reader :account, :params

      def initialize(account, params)
        @account = account
        @params = params
      end

      def satisfaction_ratings
        scope = account.satisfaction_ratings.non_deleted_with_archived
        scope = add_score_condition(scope)
        scope = add_reason_code_condition(scope)
        scope = add_start_time_condition(scope)
        scope = add_end_time_condition(scope)

        scope
      end

      private

      def score
        params[:score]
      end

      def reason_code
        params[:reason_code]
      end

      def start_time
        params[:start_time]
      end

      def end_time
        params[:end_time]
      end

      def add_reason_code_condition(scope)
        return scope unless account.csat_reason_code_enabled?
        return scope unless reason_code
        scope.where(reason_code: reason_code)
      end

      def add_score_condition(scope)
        return scope unless score
        scope.where(score: Satisfaction::Rating::SCORE_MAP.fetch(score))
      end

      def add_start_time_condition(scope)
        return scope unless start_time
        scope.newer_than(Time.at(start_time))
      end

      def add_end_time_condition(scope)
        return scope unless end_time
        scope.older_than(Time.at(end_time))
      end
    end
  end
end
