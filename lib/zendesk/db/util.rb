module Zendesk
  module DB
    module Util
      extend ZendeskRules::QueryPresenter::BooleanValue

      def self.quote_for_like_clause(string)
        return nil if string.nil?
        ActiveRecord::Base.connection.quote_string(string).gsub('%', '\%').gsub('_', '\_')
      end
    end
  end
end
