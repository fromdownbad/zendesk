module ActiveRecord
  module Reflection
    class AssociationReflection
      def columns(tbl_name, log_msg)
        @columns ||= begin
          if klass.on_slave_by_default?
            klass.on_slave.connection.columns(tbl_name, log_msg)
          else
            klass.connection.columns(tbl_name, log_msg)
          end
        end
      end
    end
  end
end
