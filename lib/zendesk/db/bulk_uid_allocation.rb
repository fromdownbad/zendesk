module Zendesk
  module DB
    module BulkUidAllocation
      # extending this module will override `generate_uid` provided by
      # the global_uid gem.
      # https://github.com/zendesk/global_uid/blob/4df1cc90ea91e4d83aa39b92497815874d6c13a1/lib/global_uid/active_record_extension.rb#L30-L34
      #
      # This allows you to reserve x amount of IDs for an ActiveRecord model using GlobalUID,
      # whenever a new record is created, the ID will be retrieved from the reserved pool, instead
      # of fetching one from the alloc DB.
      # https://zendesk.slack.com/files/T024F4EL1/F010BJRA5QQ
      #
      # Reserving IDs is only necessary for very write heavy operations.
      # The overhead for generating one ID is low:
      #   WithGlobalUID    923.498  (± 8.3%) i/s -      4.620k in   5.041687s
      #   WithoutGlobalUID   1.330k (± 6.5%) i/s -      6.731k in   5.081601s
      # That's 923 vs 1,330 records per second on a development machine.

      def reserve_global_uids(n)
        generate_many_uids(n).tap do |uids|
          @reserved_global_uids ||= []
          @reserved_global_uids.concat(Array(uids))
        end
      end

      def generate_uid
        @reserved_global_uids ||= []
        @reserved_global_uids.shift || super
      end
    end
  end
end
