module Zendesk
  module DB
    module RaceGuard
      class << self
        def guarded(retries = 1, &block)
          errors = [ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation]
          condition = -> (e) do
            if /Duplicate entry/.match?(e.message)
              clear_query_caches
              true
            end
          end
          Zendesk::Retrier.retry_on_error(errors, retries + 1, retry_if: condition, &block)
        end

        def cache_guarded(key, timeout = 30.seconds)
          if Rails.cache.write(key, true, expires_in: timeout, unless_exist: true)
            yield
          else
            Rails.logger.info("Skipping cache_guarded block for #{key}")
          end
        end

        def cache_locked(key, options = {})
          max_interval = options[:wait_max] || 1.seconds
          interval = options[:interval] || max_interval / 10.0
          sleepy_time = 0
          until Rails.cache.write(key, true, expires_in: max_interval * 10, unless_exist: true)
            sleepy_time += interval
            raise Timeout::Error if sleepy_time > max_interval
            sleep(interval)
          end
          Rails.logger.info("Executing cache_locked block for #{key}")
          yield
        rescue Timeout::Error => e
          raise e unless options[:execute_on_timeout]
          Rails.logger.info("Executing cache_locked block for #{key} despite timeout")
          yield
        ensure
          Rails.cache.delete(key)
        end

        def clear_query_caches
          Ticket.connection.clear_query_cache
          Account.connection.clear_query_cache
        end
      end

      module ControllerSupport
        private

        def race_guard
          Zendesk::DB::RaceGuard.guarded { yield }
        end
      end
    end
  end
end
