module Zendesk
  module Crm
    class Integration
      def self.parse_user_info_xml(xml, instance_host)
        # for some reason nokogiri removes &amp; that appears after
        # trademark symbols and break urls with params
        xml.gsub!("&reg;", "")
        doc = Nokogiri::XML(xml)

        {}.tap do |data|
          data[:records] = doc.xpath("/records/record").map do |elem|
            fields = elem.xpath("fields/field").map do |field|
              {
                label: field.at("label").text,
                value: field.at("value").try(:text) || ""
              }
            end

            record_url = if elem.at("url")
              build_user_url(elem.at("url").text, instance_host)
            end
            {
              id: elem.at("id").text,
              url: record_url,
              label: elem.at("label").text,
              record_type: elem.at("record_type").try(:text),
              fields: fields
            }
          end
        end
      end

      def self.build_user_url(path, instance_host)
        uri = URI.parse(instance_host)
        path = (uri.path + path).gsub("//", "/")
        if path.include? "?"
          path, query = path.split("?")
          uri.path = path
          uri.query = query
        else
          uri.path = path
        end
        uri.to_s
      end

      def self.ticket_xml(ticket)
        options = {
          include: {
            requester: {
              include: :organization
            },
            ticket_field_entries: {},
            comments: {
              include: Comment.new.send(:serialization_options)[:include],
              only: Comment.new.send(:serialization_options)[:only]
            },
            linkings: {}
          }
        }
        ticket.to_xml(options)
      end
    end
  end
end
