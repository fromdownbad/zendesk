module Zendesk
  module Crm
    class ZendeskUsers
      def self.update_primary_org_membership(user, organization_id)
        # Update current organization membership if any
        current_membership = user.organization_memberships.find_by(organization_id: user.organization_id)
        if current_membership
          current_membership.update_attributes(organization_id: organization_id)
        end
        user.organization_id = organization_id
      end

      def self.create_new_membership(current_account, user, organization_id)
        if organization_id && !user.organization_ids.include?(organization_id)
          organization = current_account.organizations.find(organization_id)
          user.organization_memberships.create!(user: user, organization: organization)
        end
      end

      def self.update_existing_or_initialize_new_user(current_account, current_user, params)
        user = current_account.find_user_by_params(user: params) || current_account.users.new
        is_new_user = user.new_record?

        perform_update = true
        create_membership = false

        unless is_new_user
          if params[:organization_id]
            if user.organization_id
              org_id_to_check = params[:organization_id]

              # if org id was changed, check the old org id
              old_organization_id = params.delete(:old_organization_id)
              if old_organization_id && old_organization_id != params[:organization_id]
                org_id_to_check = old_organization_id
              end

              if user.organization_id != org_id_to_check
                perform_update = false
                create_membership = true
              end
            else
              create_membership = true
            end
          else
            perform_update = false if user.organization_id
          end
        end

        new_organization_id = params.delete(:organization_id)
        if create_membership
          # When the user's organization_id changes an organization_membership is not always created
          if current_account.has_multiple_organizations_enabled? || user.organization_memberships.empty?
            create_new_membership(current_account, user, new_organization_id)
          else
            perform_update = true
          end
        end

        if perform_update
          params[:organization_id]  = new_organization_id   if current_account.has_org_id_in_crm_zendesk_users?    && is_new_user
          params[:organization_ids] = user.organization_ids if current_account.has_multiple_organizations_enabled? && !user.organization_ids.empty?

          ::Zendesk::Users::Initializer.new(current_account, current_user, user: params).apply(user)
          update_primary_org_membership(user, new_organization_id)
        end

        [user, is_new_user, perform_update]
      end
    end
  end
end
