module Zendesk
  module Crm
    module ExternalData
      module SyncStatus
        SYNC_OK      = 0
        SYNC_PENDING = 1
        SYNC_ERRORED = 2

        STATUS_DESCRIPTIONS = {
          SYNC_OK => "ok",
          SYNC_PENDING => "pending",
          SYNC_ERRORED => "errored"
        }.freeze
      end

      def data?
        data.present?
      end

      def sync_pending?
        sync_status == SyncStatus::SYNC_PENDING
      end

      def sync_errored?
        sync_status == SyncStatus::SYNC_ERRORED
      end

      def sync_pending!
        update_attribute(:sync_status, SyncStatus::SYNC_PENDING)
      end

      def sync_errored!(data = nil)
        if data
          self.sync_status = SyncStatus::SYNC_ERRORED
          self.data        = data
          save
        else
          update_attribute(:sync_status, SyncStatus::SYNC_ERRORED)
        end
      end

      def sync!(data)
        self.sync_status = SyncStatus::SYNC_OK
        self.data        = data
        save
      end

      def stale?
        false
      end

      def needs_sync?
        !data? || stale? || sync_errored?
      end

      def start_sync
        raise "Must be implemented by subclass"
      end

      def status
        SyncStatus::STATUS_DESCRIPTIONS[sync_status]
      end
    end
  end
end
