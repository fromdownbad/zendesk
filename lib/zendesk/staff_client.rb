module Zendesk
  class StaffClient
    include Zendesk::CoreServices::ErrorHandling

    RETRYABLE_ERRORS = [
      Faraday::ConnectionFailed,
      Faraday::TimeoutError,
      Faraday::ClientError,
      Kragle::ClientError,
      Kragle::ResponseError
    ].freeze

    SUPPORT_PRODUCT = 'support'.freeze
    GUIDE_PRODUCT = 'guide'.freeze
    TALK_PRODUCT = 'talk'.freeze

    attr_reader :account, :circuit_options, :connection_timeout, :retry_options, :audit_headers

    def initialize(account, options = {})
      @account = account
      @circuit_options = options.fetch(:circuit_options, failure_threshold: 10, reset_timeout: 5)
      @connection_timeout = options.fetch(:connection_timeout, 2)
      @retry_options = options.fetch(:retry_options, max: 2, exceptions: RETRYABLE_ERRORS, methods: Faraday::Request::Retry::IDEMPOTENT_METHODS + [:patch])
      @audit_headers = options.fetch(:audit_headers, actor_id: -1, ip_address: "")
    end

    # Roles API

    def create_role!(title:, description:, name:, max_agents_countable:, is_system_role: false, is_active: true, assignable: true, product: SUPPORT_PRODUCT, context: nil)
      with_instrumentation("create_role", ["context:#{context}", "product_name:#{SUPPORT_PRODUCT}"]) do
        request_body = {
          role: {
            name: name,
            title: title,
            description: description,
            max_agents_countable: max_agents_countable,
            is_system_role: is_system_role,
            is_active: is_active,
            assignable: assignable
          }
        }
        kragle_connection.post(roles_url(product), request_body)
      end
    end

    def toggle_role!(name:, assignable:, product: SUPPORT_PRODUCT, context: nil)
      with_instrumentation("toggle_role", ["context:#{context}", "product_name:#{product}"]) do
        response = kragle_connection.get("#{roles_url(product)}/name/#{name}")
        role_id  = response.body['role']['id']

        request_body = { role: { assignable: assignable } }
        kragle_connection.patch("#{roles_url(product)}/#{role_id}", request_body)
      end
    end

    def update_or_create_role!(title:, description:, name:, max_agents_countable:, assignable: true, is_active: true, context: nil)
      with_instrumentation("update_or_create_role", ["context:#{context}", "product_name:#{SUPPORT_PRODUCT}"]) do
        begin
          update_role!(
            title: title,
            description: description,
            name: name,
            max_agents_countable: max_agents_countable,
            is_active: is_active,
            assignable: assignable
          )
        rescue Kragle::ResourceNotFound
          create_role!(
            title: title,
            description: description,
            name: name,
            max_agents_countable: max_agents_countable,
            is_active: is_active,
            assignable: assignable,
            context: context
          )
        end
      end
    end

    def update_role!(name:, title: nil, description: nil, max_agents_countable: nil, is_active: true, assignable: true)
      response = kragle_connection.get("#{roles_url}/name/#{name}")
      role_id  = response.body['role']['id']

      request_body = {
        role: {
          name: name,
          title: title,
          description: description,
          max_agents_countable: max_agents_countable,
          is_active: is_active,
          assignable: assignable
        }.compact
      }
      kragle_connection.patch("#{roles_url}/#{role_id}", request_body)
    end

    def delete_role!(name:, context: nil)
      with_instrumentation("delete_role", ["context:#{context}", "product_name:#{SUPPORT_PRODUCT}"]) do
        response = kragle_connection.get("#{roles_url}/name/#{name}")
        role_id  = response.body['role']['id']
        kragle_connection.delete("#{roles_url}/#{role_id}")
      end
    end

    def seats_remaining_support_or_suite!
      return entitlements_seats_remaining!['support'] unless account.arturo_enabled?('staff_service_seats_remaining')

      response = seats_remaining!
      seat_type = seat_type_for_account(account)
      record = response.find { |entry| entry[:seat] == seat_type }

      if record.nil?
        Rails.logger.error("Failed to get seats remaining record for seat type '#{seat_type}' on account #{account.idsub}")
        return nil
      end

      record[:remaining]
    end

    def seats_remaining_spp_light_agents!
      if account.arturo_enabled?('staff_service_seats_remaining')
        seats_remaining!.find { |entry| entry[:seat] == 'light_agents' }[:remaining]
      else
        entitlements_seats_remaining!['support']
      end
    end

    # Entitlements API

    def entitlements_seats_remaining!
      kragle_connection.get("/api/services/entitlements/seats_remaining").body.with_indifferent_access[:seats_remaining]
    rescue Kragle::ResponseError, Faraday::ClientError => e
      record_metrics(e)
      Rails.logger.error("Failed to get seats remaining for account #{account.idsub} from staff service: #{e.message}\n#{e.backtrace.join('\n ')}")
      raise
    end

    def seats_remaining!
      kragle_connection.get("/api/services/seats/remaining").body.deep_symbolize_keys[:seats_remaining]
    rescue Kragle::ResponseError, Faraday::ClientError => e
      record_metrics(e)
      Rails.logger.error("Failed to get seats remaining for account #{account.idsub} from staff service: #{e.message}\n#{e.backtrace.join('\n ')}")
      raise
    end

    def seats_occupied!
      kragle_connection.get("/api/services/seats/occupied").body.deep_symbolize_keys[:seats_occupied]
    rescue Kragle::ResponseError, Faraday::ClientError => e
      Rails.logger.error("Failed to get seats occupied for account #{account.idsub} from staff service: #{e.message}\n#{e.backtrace.join('\n ')}")
      raise
    end

    def seats_occupied_support_or_suite!
      response = seats_occupied!
      seat_type = seat_type_for_account(account)
      record = response.find { |entry| entry[:seat] == seat_type }

      if record.nil?
        Rails.logger.error("Failed to get seats occupied record for seat type '#{seat_type}' on account #{account.idsub}")
        return nil
      end

      record[:occupied]
    end

    def get_entitlements!(staff_id, is_end_user = false)
      response = kragle_connection.get("/api/services/staff/#{staff_id}/entitlements#{'?is_end_user=true' if is_end_user}")
      response.body.with_indifferent_access[:entitlements]
    rescue Kragle::ResponseError, Faraday::ClientError => e
      record_metrics(e)
      Rails.logger.error("Failed to get entitlements for account #{account.idsub}, staff_id #{staff_id} from staff service: #{e.message}\n#{e.backtrace.join('\n ')}")
      raise
    end

    def admin?(staff_id)
      get_entitlements!(staff_id).values.detect { |v| v == 'admin' }
    end

    def update_entitlements!(staff_id, entitlements, is_end_user = false)
      response = kragle_connection.patch do |req|
        req.url "/api/services/staff/#{staff_id}/entitlements#{'?is_end_user=true' if is_end_user}"
        req.body = { entitlements: entitlements }
      end

      response.body.with_indifferent_access[:entitlements]
    rescue Kragle::ResponseError, Faraday::ClientError => e
      record_metrics(e, entitlements.keys.map { |k| "product:#{k}" })
      Rails.logger.info("Failed to update entitlements for account #{account.idsub}, staff_id #{staff_id} from staff service: #{e.message}\n#{e.backtrace.join('\n ')}")
      raise
    end

    def update_entitlements(staff_id, entitlements, is_end_user = false)
      update_entitlements!(staff_id, entitlements, is_end_user)
    rescue Kragle::ResponseError, Faraday::ClientError => _
    end

    def get_full_entitlements!(staff_id)
      kragle_connection.get("/api/services/staff/#{staff_id}/full_entitlements").body.with_indifferent_access[:entitlements]
    rescue Kragle::ResponseError, Faraday::ClientError => e
      record_metrics(e)
      Rails.logger.error("Failed to get full entitlements for account #{account.idsub}, staff_id #{staff_id} from staff service: #{e.message}\n#{e.backtrace.join('\n ')}")
      raise
    end

    def update_full_entitlements!(staff_id, entitlements, skip_validation: false, is_end_user: false)
      response = kragle_connection.patch do |req|
        req.url "/api/services/staff/#{staff_id}/full_entitlements?is_end_user=#{is_end_user}&skip_validation=#{skip_validation}"
        req.body = { entitlements: entitlements }
      end

      response.body.with_indifferent_access[:entitlements]
    rescue Kragle::ResponseError, Faraday::ClientError => e
      record_metrics(e, entitlements.keys.map { |k| "product:#{k}" })
      Rails.logger.error("Failed to update full entitlements for account #{account.idsub}, staff_id #{staff_id} from staff service: #{e.message}\n#{e.backtrace.join('\n ')}")
      raise
    end

    def staff_count(product:, role: nil, include_inactive: false)
      kragle_connection.get("/api/services/entitlements/#{product}/staff_count?include_inactive=#{include_inactive}#{"&role=#{role}" if role}").body.with_indifferent_access[:staff_count]
    end

    def get_staff!(product:, role: nil, include_inactive: false)
      url = "/api/services/staff?product=#{product}&include_inactive=#{include_inactive}"
      url += "&role=#{role}" if role.present?

      kragle_connection.get(url).body.with_indifferent_access[:users]
    rescue Kragle::ResponseError, Faraday::ClientError => e
      record_metrics(e)
      Rails.logger.error("Failed to get staff for account #{account.idsub} from staff service: #{e.message}\n#{e.backtrace.join('\n ')}")
      raise
    end

    private

    def kragle_connection
      @kragle_connection ||= Kragle.new('metropolis', circuit_options: circuit_options, retry_options: retry_options) do |faraday|
        faraday.url_prefix = ENV.fetch("STAFF_SERVICE_URL")
        faraday.headers['Host'] = staff_service_host
        faraday.headers['X-Zendesk-Internal-User'] = audit_headers[:actor_id].to_s if audit_headers && audit_headers[:actor_id]
        faraday.headers['X-Forwarded-For'] = audit_headers[:ip_address] if audit_headers && audit_headers[:ip_address]
        faraday.options.timeout = connection_timeout
      end
    end

    def staff_service_host
      "#{account.subdomain}.#{Zendesk::Configuration['host']}"
    end

    def roles_url(product = SUPPORT_PRODUCT)
      "/api/services/products/#{product}/roles"
    end

    def seat_type_for_account(account)
      if account.spp_suite?
        'zendesk_suite'
      elsif account.has_active_suite?
        'suite'
      else
        'support' # if SPP Support Plus, also read from 'support' key
      end
    end

    def record_metrics(error, tags = [])
      tags = ["error:#{error.class}"] + tags
      statsd_client.increment(:failed_sync, tags: tags)
    end
  end
end
