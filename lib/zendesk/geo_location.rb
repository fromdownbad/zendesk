require 'maxminddb'
require 'json'
require 'net/http'

module Zendesk
  class GeoLocation
    DATA_FILE = Bundler.root.join('config/GeoLiteCity.mmdb')
    DATA_FILE_EXTENSION = 'mmdb'.freeze
    class << self
      def update_data!
        url = Zendesk::Configuration.dig(:geo_location, :download_url)

        puts "Downloading db tar file"
        tempfile = OpenURI.open_uri(url)

        puts "Unzipping tar file"
        gzip_reader = Zlib::GzipReader.wrap(tempfile)
        Gem::Package::TarReader.new(gzip_reader) do |tar|
          tar.each do |entry|
            entry_extension = entry.full_name.to_s.split('.').last
            next if entry_extension != DATA_FILE_EXTENSION
            File.open(DATA_FILE, 'wb') { |file| file.write(entry.read) }
          end
        end

        puts "GeoDB updated!"
        tempfile.unlink
      end

      def locate(ip)
        return unless geo_ip
        return unless ip =~ /\d+\.\d+\.\d+\.\d+/

        # geo_ip takes a reference to the IP, I think.
        return unless lookup = geo_ip.lookup(sanitize_ip(ip))

        loc = {
          country_code: lookup.country.try(:iso_code),
          country:      lookup.country.try(:name),
          timezone:     lookup.location.try(:time_zone),
          latitude:     lookup.location.try(:latitude),
          longitude:    lookup.location.try(:longitude),
          city:         lookup.city.try(:name)
        }

        loc[:state] = lookup.subdivisions.first.try(:iso_code) unless lookup.subdivisions.empty?

        fill_region(loc) if loc[:latitude] && loc[:longitude]

        loc[:city] = lookup.city.name.encode('UTF-8', 'ISO-8859-1') if lookup.city.name

        if loc[:country] || loc[:state] || loc[:city]
          loc[:description] = [loc[:city], loc[:state], loc[:country]].compact.join(', ')
        end

        loc.values.any? ? loc : nil
      rescue StandardError => e # I know, but we've seen NoMethodError here when updating the data file
        return if e.class.name.starts_with?('IPAddr') # invalid ip addresses are not logged

        ZendeskExceptions::Logger.record(e, location: self, message: "Failed to locate IP #{ip}. If you just updated the data file, you also need to restart the app.", fingerprint: 'f9cc06f6b65d21d6117e6996cdc87cc934540daf')
        return nil
      end

      def locate_ticket_audit!(audit)
        description = nil
        latitude    = nil
        longitude   = nil

        if location = locate(audit.metadata[:system][:ip_address])
          description = location[:description]
          latitude    = location[:latitude]
          longitude   = location[:longitude]
        elsif audit.author
          description = audit.author.settings.location
          latitude    = audit.author.settings.latitude
          longitude   = audit.author.settings.longitude
        end

        audit.metadata[:system][:location] = description if description

        if latitude && longitude
          latitude  = latitude.to_f
          longitude = longitude.to_f

          audit.metadata[:system][:latitude]  = latitude
          audit.metadata[:system][:longitude] = longitude
        end
      end

      def locate_user!(user, ip)
        return if user.is_system_user?
        return unless location = locate(ip)

        location_changed = false

        if location[:description] && user.settings.location != location[:description]
          user.settings.location = location[:description]
          location_changed = true
        end

        if location[:latitude] && location[:longitude]
          [:latitude, :longitude].each do |type|
            if user.settings.send(type).nil? || (user.settings.send(type).to_f - location[type].to_f).abs > 0.000001
              user.settings.send("#{type}=", location[type].to_f)
              location_changed = true
            end
          end
        end
        user.settings.save if location_changed
      end

      protected

      def geo_ip
        @geo_ip ||= MaxMindDB.new(DATA_FILE)
      end

      def sanitize_ip(ip)
        # Strip leading zeros
        ip.split('.').map do |num|
          num[0] = '' if num[0] == '0'
          num
        end.join('.')
      end

      # just a rough approximation of region
      def fill_region(location)
        return unless location[:longitude]
        location[:region] =
          case location[:longitude]
          when -180..-15
            :us
          when -15..90
            :emea
          when 90..180
            :apac
          end
      end

      ## There is no defualt value in this function because it is used in too
      #  many places. the default will be set in the places calling this function
      def fill_region_with_country(location)
        country_name = location[:country_code]
        return unless country_name

        location[:region] = Zendesk::Accounts::CreationRegion.lookup(country_name)
      end
    end
  end
end
