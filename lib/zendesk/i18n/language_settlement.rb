module Zendesk::I18n
  class LanguageSettlement
    ACCEPT_LANGUAGE_FALLBACK = {
      "es-419" => ["es-AR", "es-BO", "es-CL", "es-CO", "es-CR", "es-DO", "es-EC", "es-SV", "es-GT", "es-HN", "es-MX", "es-NI", "es-PA", "es-PY", "es-PE", "es-PR", "es-UY", "es-VE"],
      "zh-TW" => ["zh-HANT", "zh-HK"],
      "zh-CN" => ["zh-HANS"]
    }.freeze

    def initialize(http_accept_language, account_available_languages)
      @http_accept_language = http_accept_language
      @account_available_languages = account_available_languages
    end

    # The matched_language will be ja-JP for example, which in Zendesk can be ja_JP
    def find_matching_zendesk_locale
      language = best_language_match
      return unless language
      language = fallback_locale(language) unless account_available_locales.include?(language)

      @account_available_languages.find do |l|
        normalized_locale = LanguageSettlement.normalize_locale(l.locale)
        normalized_locale == language || normalized_locale == language.tr('-', '_')
      end
    end

    # Finds the first match based on users most preferred primary language, matches secondly on region
    def best_language_match
      http_accept_language.language_region_compatible_from(available_locales)
    end

    # Will remove any arbitrary (-x-) parameters from a locale. i.e. "ja-x-iknow" will become "ja"
    def self.normalize_locale(locale)
      locale.split('-x-').first
    end

    def available_locales
      account_locale = account_available_locales
      ACCEPT_LANGUAGE_FALLBACK.each do |k, v|
        account_locale << v if account_locale.include? k
      end
      account_locale.flatten.uniq
    end

    protected

    attr_reader :http_accept_language

    def account_available_locales
      @account_available_languages.map { |l| LanguageSettlement.normalize_locale(l.locale) }
    end

    def fallback_locale(locale)
      ACCEPT_LANGUAGE_FALLBACK.each do |k, v|
        return k if v.include? locale
      end
      locale
    end
  end
end
