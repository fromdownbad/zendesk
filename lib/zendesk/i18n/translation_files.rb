# TODO: js in YAML files is deprecated. We want to select those keys in a different way (hard coded list of keys, or
# maybe a separate translation package).
module Zendesk
  module I18n
    class TranslationFiles
      attr_reader :js_keys, :translations

      def initialize(path)
        @translations            = {}
        @js_keys                 = []
        parse_files(path)
      end

      def parse_files(paths)
        Dir.glob("{#{paths.join(',')}}/*.yml") do |file_name|
          translation_file(file_name)['parts'].each do |translation|
            parse_translation(translation.values.first) if translation.keys.first == 'translation'
          end
        end
      end

      def translation_file(file_name)
        YAML.load_file(file_name)
      end

      def parse_translation(translation_parts)
        @translations[translation_parts['key']] = translation_parts['value']
        # The 'js' key is / was only used in Classic
        # See https://zendesk.atlassian.net/wiki/spaces/globalization/pages/742623081/Deprecated+js+YAML+Attribute+in+Classic
        @js_keys << translation_parts['key'] if translation_parts['js']
      end

      class Proxy
        def initialize(path)
          @path = path
        end

        def instance
          @instance ||= TranslationFiles.new(@path)
        end
      end
    end
  end
end
