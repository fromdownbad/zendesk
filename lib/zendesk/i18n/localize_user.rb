module Zendesk::I18n::LocalizeUser
  # You should include this module in every controller that uses ActionController::Base

  def self.included(base)
    base.around_action :localize_user
  end

  protected

  def localize_user
    time_zone = Time.zone
    i18n_locale = I18n.translation_locale

    if valid_locale_request?
      update_session_and_user
      set_localized_attributes
    end

    yield
  ensure
    if Rails.env.test?
      Time.previous_request_zone = Time.zone
      I18n.previous_request_locale = I18n.translation_locale
    end

    Time.zone = time_zone
    I18n.locale = i18n_locale
  end

  def update_session_and_user
    return unless requested_locale

    Rails.logger.info "Setting session locale: #localize_user --- user: #{current_user.try(:locale_id)} - session: #{requested_locale.id}"
    shared_session[:locale_id] = requested_locale.id
    current_user.translation_locale = requested_locale if current_user && requested_locale
  end

  def set_localized_attributes
    Time.zone = select_time_zone
    I18n.locale = select_locale
  end

  def select_time_zone
    current_user.try(:time_zone) || current_account.time_zone || 'UTC'
  end

  def select_locale
    if requested_locale
      requested_locale
    elsif current_user
      current_user.translation_locale
    end || ENGLISH_BY_ZENDESK # We || this instead of else to ensure that I18n.locale cannot get a nil value
  end

  def requested_locale
    @requested_locale ||= get_locale_from_params
  end

  def get_locale_from_params # rubocop:disable Naming/AccessorMethodName
    return unless params[:locale].present?

    locale_code_or_id = params[:locale]

    if params[:force_locale].to_s.casecmp('true').zero? || current_account.has_individual_language_selection?
      TranslationLocale.available_for_account(current_account).find_by_id_or_locale(locale_code_or_id)
    end
  end

  def valid_locale_request?
    # When updating a translation locale, the params[:locale] is present and it fails in the code below.
    # The purpose of `&& !params[:locale].is_a?(Parameters)` is to filter those requests.
    # param[:locale] coming from the I18n controller usually looks like this:
    # :locale =>{:name => "Test of locale", :locale =>"en-jm", :flag =>"am", :public=>"0", :localized_agent=>"0"}
    current_account && !params[:locale].is_a?(ActionController::Parameters)
  end
end
