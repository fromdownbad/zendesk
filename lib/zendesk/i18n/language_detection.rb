module Zendesk::I18n::LanguageDetection
  ACCEPT_LANGUAGE_FALLBACK = {"es" => ["es-ES"], "es-419" => ["es-AR", "es-BO", "es-CL", "es-CO", "es-CR", "es-DO", "es-EC", "es-SV", "es-GT", "es-HN", "es-MX", "es-NI", "es-PA", "es-PY", "es-PE", "es-PR", "es-UY", "es-VE"] }.freeze

  def self.included(base)
    base.before_action :perform_language_detection
  end

  protected

  def perform_language_detection
    return unless current_account.present?
    return unless user_needs_a_locale?

    locale_id = shared_session[:locale_id]

    matched_locale = if locale_id
      TranslationLocale.find_by_id(locale_id)
    else
      find_matching_zendesk_locale(best_language_match)
    end
    set_user_locale(matched_locale)

    if !request.get? && !current_user.is_anonymous_user?
      current_user.save || Rails.logger.error("Unable to save current locale #{current_user.errors.full_messages}")
    end
  end

  def account_available_locales
    current_account.available_languages.map { |l| normalize_locale(l.locale) }
  end

  def available_locales
    account_locale = account_available_locales
    ACCEPT_LANGUAGE_FALLBACK.each do |k, v|
      if account_locale.include? k
        account_locale << v
      end
    end
    account_locale.flatten.uniq
  end

  def user_needs_a_locale?
    current_user.present? && !current_user.is_agent? && !current_user.locale_id
  end

  # The matched_language will be ja-JP for example, which in Zendesk can be ja_JP
  def find_matching_zendesk_locale(language)
    return unless language
    language = fallback_locale(language) unless account_available_locales.include?(language)

    current_account.available_languages.find do |l|
      normalized_locale = normalize_locale(l.locale)
      normalized_locale == language || normalized_locale == language.tr('-', '_')
    end
  end

  def fallback_locale(locale)
    ACCEPT_LANGUAGE_FALLBACK.each do |k, v|
      return k if v.include? locale
    end
    locale
  end

  # Finds the first match based on users most preferred primary language, matches secondly on region
  def best_language_match
    http_accept_language.language_region_compatible_from(available_locales)
  end

  def set_user_locale(matched_locale) # rubocop:disable Naming/AccessorMethodName
    current_user.translation_locale = matched_locale || current_account.translation_locale
    shared_session[:locale_id] = current_user.translation_locale.id
    I18n.locale = current_user.translation_locale
  end

  # Will remove any arbitrary (-x-) parameters from a locale. i.e. "ja-x-iknow" will become "ja"
  def normalize_locale(locale)
    locale.split("-x-").first
  end
end
