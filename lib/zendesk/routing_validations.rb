module Zendesk
  module RoutingValidations
    def self.included(base)
      base.class_eval do
        validates_format_of     :subdomain, with: SUBDOMAIN_PATTERN, if: proc { |obj| obj.subdomain_changed? || obj.new_record? }
        validates_length_of     :subdomain, maximum: DNS_CHARACTER_LIMIT
        before_validation       :fix_subdomain
        validates_presence_of   :subdomain, on: :update
        validates_uniqueness_of :subdomain, case_sensitive: false, if: :subdomain_changed?
        validates_format_of     :host_mapping, with: DOMAIN_PATTERN, if: :host_mapping?
        validates_uniqueness_of :host_mapping, case_sensitive: false, if: :host_mapping?
        validate                :validate_reserved_subdomain, if: :subdomain_changed?
        validate                :host_mapping_available?, on: :update
        validate                :valid_cname_record?, on: :update
      end
    end

    def self.reserved_subdomain?(subdomain)
      return false if Rails.env.development? && DEV_SUBDOMAINS.include?(subdomain)

      ReservedSubdomain.reserved?(subdomain)
    end

    # Please cc @zendesk/account-service in your PR if you are changing subdomain availability related logic.
    # Currently this logic is ported in account service in Scala - https://github.com/zendesk/zendesk_core_scala
    def self.subdomain_available?(subdomain, internal: nil)
      subdomain.strip!

      !!(
        subdomain =~ SUBDOMAIN_PATTERN &&
        !Account.exists?(subdomain: subdomain) && # delete line after routes backfilled
        !Route.exists?(subdomain: subdomain) &&
        !reserved_subdomain?(subdomain) &&
        (internal || allowed_subdomain?(subdomain))
      )
    end

    def cname_valid?
      cname_status == :pass
    end

    def valid_cname_record?
      return if host_mapping.blank? || !host_mapping_changed?
      case cname_status
      when :not_cname
        errors.add(:host_mapping, ::I18n.t('txt.errors.account.host_mapping_errors.is_not_a_valid_cname_record'))
      when :wrong_cname
        errors.add(:host_mapping, "is a valid CNAME but does not reference #{default_host}. Please verify your DNS settings.")
      when :pass
        nil
      else
        Rails.logger.warn "unhandled cname_status: #{cname_status}"
      end
    end

    def cname_allowed_for_development?
      Rails.env.development? && host_mapping.end_with?(".dev", ".localhost", "localhost.com", "zd-dev.com")
    end

    def host_mapping_available?
      if host_mapping_changed? && host_mapping.present? && !subscription.has_host_mapping?
        errors.add(:host_mapping, 'is not available on your account plan')
      end
    end

    private

    def cname_status
      return if host_mapping.blank?
      resolved = Zendesk::Net::CNAMEResolver.resolve?(host_mapping)

      if !resolved && cname_allowed_for_development?
        :pass
      elsif !resolved
        :not_cname
      elsif !/#{default_host}/i.match?(resolved.name.to_s.gsub(".ssl.", ".")) # /\.zendesk\.com$/
        :wrong_cname
      else
        :pass
      end
    end

    def validate_reserved_subdomain
      errors.add(:subdomain, :exclusion) if Zendesk::RoutingValidations.reserved_subdomain?(subdomain)
    end

    def fix_subdomain
      self.subdomain = subdomain.strip.downcase if subdomain.present?
    end

    private_class_method def self.allowed_subdomain?(subdomain)
      !subdomain.starts_with?('xn--')
    end
  end
end
