# @account should be available for the module
module Zendesk::DynamicContentFieldsHelper
  CUSTOM_FIELD_DC_FIELD = "name".freeze

  def map_dc_parameters(raw_parameters, request_params, custom_field_options = false)
    return unless request_params
    raw_parameters.each do |attribute_name|
      map_dynamic_content_attribute(request_params, attribute_name)
    end
    map_dc_custom_field_options(request_params) if custom_field_options && request_params[:custom_field_options]
  end

  def map_dc_custom_field_options(request_params)
    request_params[:custom_field_options].each do |option|
      map_dynamic_content_attribute(option, CUSTOM_FIELD_DC_FIELD)
    end
  end

  def map_dynamic_content_attribute(request_params, attribute_name)
    raw_param = "raw_#{attribute_name}".to_sym
    if request_params[raw_param]
      request_params[attribute_name.to_sym] = request_params.delete(raw_param)
    elsif request_params[attribute_name.to_sym]
      Rails.logger.debug("Account: #{@account.id} writing to #{attribute_name} rather than #{raw_param}")
    end
  end
end
