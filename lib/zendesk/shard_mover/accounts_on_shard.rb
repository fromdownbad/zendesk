# Enumerable to traverse accounts on a shard for features that implement
# the Zendesk::ShardMover::Accounts#disable_feature.
#
# This works with the DisabledAccountHelper to traverse accounts on a shard
# while also periodically acknowledging disable requests (pending-disable)
# across all shards and filtering out accounts from current shard traversal
# if they become disabled during course of shard traversal.
#
module Zendesk
  module ShardMover
    class AccountsOnShard
      include Enumerable

      attr_reader :helper, :shard_id, :update_interval

      def initialize(helper, shard_id, update_interval, ordering = rotate_ids)
        @helper = helper
        @shard_id = shard_id
        @periodically = Zendesk::Periodical.new(update_interval)
        @ordering = ordering
      end

      def each
        ActiveRecord::Base.on_shard(shard_id) do
          account_ids.each do |account_id|
            next if disabled? account_id
            yield account_id
          end
        end
      end

      def disabled?(account_id)
        @periodically.perform { @disabled_account_ids = build_disabled_account_ids }
        @disabled_account_ids.include? account_id
      end

      private

      def account_ids
        ids = active_accounts_on_shard
        return ids if ids.empty?
        @ordering.call(ids)
      end

      def active_accounts_on_shard
        Account.shard_local.active.shard_unlocked.map(&:id)
      end

      # Acknowledges all pending-disable accounts (across all shards) by
      # flipping them to disabled state, then returns a list of accounts
      # that are disabled for given shard
      def build_disabled_account_ids
        helper.disabled_accounts_on_shard
      end

      def rotate_ids
        lambda do |ids|
          return ids if ids.empty?
          ids.rotate(Random.rand(ids.length))
        end
      end
    end
  end
end
