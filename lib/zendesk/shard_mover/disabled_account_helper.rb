# Helper for features that can be disabled through
# Zendesk::ShardMover::Accounts#disable_feature.
#
# Acknowledges pending-disable requests that are set by calling
# disable_feature (acknowledged by setting the feature's setting to a
# given disabled_value), and identifies disabled accounts.
#
# Currently, only supports identifying disabled accounts on a per-shard basis
# (acknowledgements happen across all shards).
#
# Currently, only the archiver and the sync-attachments (aka stores-backfiller)
# process support a disable/enable feature.
#
module Zendesk
  module ShardMover
    class DisabledAccountHelper
      attr_reader :feature, :disabled_value, :pending_disable_value, :statsd

      def initialize(feature, statsd = nil)
        @feature = feature
        @disabled_value = Zendesk::ShardMover::Accounts::DISABLED_SETTING_VALUE
        @pending_disable_value = Zendesk::ShardMover::Accounts::PENDING_DISABLE_VALUE
        @statsd = statsd
      end

      # Returns disabled accounts for current shard
      def disabled_accounts_on_shard
        acknowledge_all_pending_disable
        AccountSetting.where(name: @feature, value: @disabled_value).pluck(:account_id)
      end

      private

      def acknowledge_all_pending_disable
        found_pending = false
        shard_ids.each do |shard_id|
          ActiveRecord::Base.on_shard(shard_id) do
            AccountSetting.where(name: @feature, value: @pending_disable_value).each do |s|
              account = Account.find_by_id(s.account_id)
              Zendesk::ShardMover::Accounts.ack_disable_feature(@feature, account)
              found_pending ||= true
            end
          end
        end
        statsd.increment('disable_feature_checks', tags: %W[found_pending:#{found_pending}]) if statsd
      end

      def shard_ids
        ActiveRecord::Base.shard_names.map(&:to_i)
      end
    end
  end
end
