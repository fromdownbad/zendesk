require 'memcached'

module Zendesk
  module ShardMover
    module Accounts
      UNLOCKED = 0
      LOCKED   = 2

      # Setting value (internal) for an disabled feature (after ack)
      DISABLED_SETTING_VALUE = 'false'.freeze

      # API value for a disabled feature (after ack)
      DISABLED_API_VALUE = 'disabled'.freeze

      # Setting value for an enabled feature (there's no use case for
      # an API to return this)
      ENABLED_SETTING_VALUE = 'true'.freeze

      # Setting and API value for a feature for which disable
      # has been requested and is awaiting ack
      PENDING_DISABLE_VALUE = 'disable_pending'.freeze

      class << self
        # Used by Exodus(account move service) to get the account feature status
        def fetch_feature(feature, account)
          Account.without_locking do
            account.on_shard do
              status = account.settings.lookup(feature.to_sym).value
              return DISABLED_API_VALUE if status == DISABLED_SETTING_VALUE

              status
            end
          end
        end

        # Used in the handshake with exodus that allows services
        # like SyncAttachments and Archiver to be disabled on accounts
        # that are being moved.
        def disable_feature(feature, account)
          log("requesting disable for #{feature} for #{account.id}:#{account.subdomain}")
          Account.without_locking do
            account.on_shard do
              status = account.settings.lookup(feature.to_sym).value
              if status == PENDING_DISABLE_VALUE
                return status
              elsif status == DISABLED_SETTING_VALUE
                return DISABLED_API_VALUE
              else
                log("disable pending for #{feature} for #{account.id}:#{account.subdomain}")
                account.settings.set(feature.to_sym => PENDING_DISABLE_VALUE)
                account.save!(validate: false)
                return PENDING_DISABLE_VALUE
              end
            end
          end
        end

        # Acknowledges disable request
        def ack_disable_feature(feature, account)
          log("acknowleding disable of #{feature} for #{account.id}:#{account.subdomain}")
          Account.without_locking do
            account.on_shard do
              account.settings.set(feature.to_sym => DISABLED_SETTING_VALUE)
              account.save!(validate: false)
            end
          end
        end

        # Used in the handshake with exodus that allows services
        # like SyncAttachments and Archiver to be disabled on accounts
        # that are being moved.
        def enable_feature(feature, account)
          log("enabling #{feature} for #{account.id}:#{account.subdomain}")
          Account.without_locking do
            account.on_shard do
              account.settings.set(feature.to_sym => ENABLED_SETTING_VALUE)
              account.save!(validate: false)
            end
          end
        end

        def feature_capable_of_disablement?(feature)
          ['write_to_archive_v2_enabled', 'syncattachments_enabled'].include? feature
        end

        private

        def log(*messages)
          Kernel.warn("#{Time.now.strftime("%Y-%m-%d %H:%M:%S")} : #{messages.map(&:to_s).join(' ')}")
        end
      end
    end
  end
end
