module Zendesk
  module Coders
    # Use when migration a column that previously contained String values.
    # See ActiveRecord::Coders::JSON (Introduced in Rails 4)
    class JSONWithStringFallback
      def self.dump(obj)
        ActiveSupport::JSON.encode(obj)
      end

      def self.load(json)
        ActiveSupport::JSON.decode(json) unless json.blank?
      rescue MultiJson::ParseError, JSON::ParserError
        json
      end
    end
  end
end
