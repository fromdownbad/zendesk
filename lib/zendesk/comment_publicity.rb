module Zendesk
  module CommentPublicity
    def self.is_comment_public?(user, is_public_param, ticket, via_id) # rubocop:disable Naming/PredicateName
      return true unless user.is_agent?
      return false unless user.can?(:publicly, Comment)
      return is_public_param unless is_public_param.nil?

      if via_id == ViaType.MAIL
        mail_comment_is_public?(ticket)
      else
        !private_ticket_restricted?(ticket, via_id)
      end
    end

    def self.mail_comment_is_public?(ticket)
      return true if ticket.entry.nil? && ticket.account.is_email_comment_public_by_default && !private_ticket_restricted?(ticket, ViaType.MAIL)
      false
    end

    def self.private_ticket_restricted?(ticket, via_id)
      !ticket.is_public && [ViaType.WEB_SERVICE, ViaType.MAIL].include?(via_id)
    end

    def self.merge_comment_publicity_default(target_ticket, source_tickets)
      target_ticket.is_public && source_tickets.all?(&:is_public)
    end

    # Channel tickets:
    #   Public merge comments on a channels ticket (Twitter, Facebook, Anychannel, etc) cause a merge failure.
    #   This is because public comments on a channels ticket are not valid in most cases.
    #   https://github.com/zendesk/zendesk/pull/24767
    #
    # Private tickets:
    #   We don't let customers add public comments while merging private tickets.
    #   This is a safeguard because adding public comments by mistake can potentially leak internal information.
    #   https://github.com/zendesk/zendesk/pull/25760
    def self.merge_comment_can_be_public?(target_ticket, source_tickets)
      merging_channel_tickets = target_ticket.via_channel? || source_tickets.any?(&:via_channel?)
      merging_private_tickets = target_ticket.is_private? || source_tickets.any?(&:is_private?)

      !merging_channel_tickets && !merging_private_tickets
    end

    def self.last_public_comment_made_private?(ticket)
      audit = ticket.audit
      return false unless audit && audit.events.count > 0 && audit.events.last.type == "CommentPrivacyChange"
      ticket.public_comments.count == 1 && audit.events.last.value_reference.to_s == ticket.public_comments.first.id.to_s
    end
  end
end
