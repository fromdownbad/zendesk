module Zendesk
  module OrganizationSubscriptions
    class Finder
      attr_reader :account, :current_user, :params

      def initialize(account, current_user, params)
        @account = account
        @current_user = current_user
        @params = params
      end

      def scope
        if current_user.is_agent?
          if user_id
            user.watchings
          elsif organization_id
            organization_scope.find(organization_id).watchings
          else
            account.watchings
          end
        else
          current_user.watchings
        end
      end

      def organization_scope
        if current_user.is_end_user?
          current_user.end_user_viewable_organizations
        else
          account.organizations
        end
      end

      private

      def user_id
        params[:user_id]
      end

      def organization_id
        params[:organization_id]
      end

      def user
        @user ||= account.users.find(user_id)
      end
    end
  end
end
