require 'faraday'

module Zendesk
  class PivotalTracker
    BASE_URL = 'https://www.pivotaltracker.com'.freeze
    API_VERSION = '/services/v5'.freeze

    def initialize(token)
      @token = token
    end

    def project(project_id)
      get(project_path(project_id))
    end

    def project_memberships(project_id)
      get("#{project_path(project_id)}/memberships")
    end

    def project_labels(project_id)
      get("#{project_path(project_id)}/labels")
    end

    def project_integrations(project_id)
      get("#{project_path(project_id)}/integrations")
    end

    def create_story(project_id, params = {})
      post("#{project_path(project_id)}/stories", params)
    end

    private

    def project_path(project_id)
      "#{API_VERSION}/projects/#{project_id}"
    end

    def get(url)
      response = connection.get(url) do |req|
        req.headers.merge!(request_headers)
      end

      parse_response(response)
    end

    def post(url, params = {})
      response = connection.post do |req|
        req.url url
        req.headers.merge!(request_headers)
        req.body = params.to_json
      end

      parse_response(response)
    end

    def request_headers
      {'Content-Type' => 'application/json', 'X-TrackerToken' => @token}
    end

    def parse_response(response)
      # do something if response.status_code != 200
      JSON.parse(response.body)
    end

    def connection
      @connection ||= Faraday.new(url: BASE_URL)
    end
  end
end
