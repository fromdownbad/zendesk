module Zendesk
  module FieldCompare
    module Base
      def is_blank?(val) # rubocop:disable Naming/PredicateName
        # NOTE: checkbox considers false and "0" to be blank
        val.blank?
      end

      def cast_for_compare(val)
        return nil if is_blank?(val)

        val.is_a?(String) ? val.strip.downcase : val
      end

      def compare(field_value, operator, value)
        field_value = cast_for_compare(field_value)
        operator = operator.to_s

        # nil means unset; only [=, !=] works with nil
        if (field_value.nil? || value.nil?) &&
         !["is", "is_not", "present", "not_present"].include?(operator)
          return false
        end

        _compare(operator, value, field_value)
      end

      def _compare(operator, value, field_value)
        value = cast_for_compare(value)

        case operator
        when "is"
          field_value == value
        when "is_not"
          field_value != value
        when "less_than"
          field_value < value
        when "greater_than"
          field_value > value
        when "less_than_equal"
          field_value <= value
        when "greater_than_equal"
          field_value >= value
        when "present"
          !!field_value
        when "not_present"
          !field_value
        when "includes_words"
          Trigger.text_includes_words?(field_value, value, account)
        when "not_includes_words"
          !Trigger.text_includes_words?(field_value, value, account)
        when "includes_string"
          field_value.include?(value)
        when "not_includes_string"
          !field_value.include?(value)
        else
          # unknown operator
          false
        end
      end
    end
  end
end
