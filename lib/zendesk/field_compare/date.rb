module Zendesk
  module FieldCompare
    module Date
      def cast_for_compare(val)
        return nil if val.nil? || (val.is_a?(String) && val.blank?)
        return val if val.is_a?(Time)

        time_parse(val)
      end

      # by default, UTC
      def time_zone
        "UTC"
      end

      def compare(operator, value, field_value)
        Time.use_zone(time_zone) { super }
      end

      def _compare(operator, value, field_value)
        if operator == "within_next_n_days"
          Time.now <= field_value && field_value < value.to_i.days.from_now
        elsif operator == "within_previous_n_days"
          value.to_i.days.ago <= field_value && field_value < Time.now
        else
          super(operator, value, field_value)
        end
      end

      def time_parse(val)
        ret = Time.zone.parse(val, Time.at(0))
        # disallow wild year values (like -ve or 5 digit years). Going to regret this in about 7000 years from now.
        (ret.year < 0 || ret.year > 9999) ? nil : ret
      rescue
        nil
      end
    end
  end
end
