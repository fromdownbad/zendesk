module Zendesk
  module TicketAnonymizer
    class TicketScrubber
      EVENTS_FOR_SCRUBBING_RECIPIENTS = %w[
        Notification
        NotificationWithCcs
        Cc
        FollowerNotification
        AnswerBotNotification
        SmsNotification
      ].freeze

      COMMENTS_FOR_SCRUBBING_VALUE_PREVIOUS = %w[VoiceComment VoiceApiComment].freeze

      AUDIT_EVENTS_FOR_SCRUBBING_METADATA = %w[Audit TicketMergeAudit].freeze

      CREATE_CHANGE_EVENTS_FOR_SCRUBBING_VALUE = %w[Create Change].freeze

      VOICE_VIA_TYPES = [ViaType.PHONE_CALL_INBOUND, ViaType.PHONE_CALL_OUTBOUND,
                         ViaType.VOICEMAIL, ViaType.API_PHONE_CALL_INBOUND,
                         ViaType.API_PHONE_CALL_OUTBOUND, ViaType.API_VOICEMAIL].freeze

      VIA_TO_ANONYMIZE_SUBJECT = (VOICE_VIA_TYPES + [ViaType.CHAT, ViaType.SMS]).freeze
      VIA_TWITTER = [ViaType.TWITTER, ViaType.TWITTER_DM, ViaType.TWITTER_FAVORITE].freeze
      VIA_FACEBOOK = [ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE].freeze

      attr_reader :account, :ticket, :ticket_id, :account_id, :values_changed
      attr_accessor :rescue_errors_in_tests

      def initialize(ticket:)
        @account = ticket.account
        @ticket = ticket
        @ticket_id = ticket.id
        @account_id = @account.id
      end

      def scrub_ticket
        return unless scrub_proccess_enabled?
        Rails.logger.info "Ticket #{ticket_id} being anonymized. account_id: #{account_id}"
        self.class.statsd_client.time('time', tags: ["account_id:#{account_id}"]) do
          ::I18n.with_locale(account.translation_locale) do
            ActiveRecord::Base.transaction do
              scrub_pii_in_events
              switch_submitter
              switch_requester
              anonymize_ticket_pii_based_on_channel
              ticket.touch
            end
          end
          remove_remote_files
        end
        statsd_record_ticket_scrubbing_completed
      rescue StandardError => error
        statsd_record_ticket_scrubbing_failed(error)
        raise if Rails.env.test? && !rescue_errors_in_tests || Rails.env.development?
      end

      def scrub_proccess_enabled?
        ticket.closed? && account.has_delete_ticket_metadata_pii_enabled?
      end

      def self.statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['ticket', 'scrub'])
      end

      private

      def anonymous_user
        @scrubbed_ticket_anonymous_user ||= Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account)
      end

      def switch_requester
        return unless account.has_ticket_user_scrubbing_after_close?
        return unless ticket.requester.is_end_user?

        Rails.logger.info "Ticket #{ticket_id} requester replaced account_id: #{account_id}, ticket_id: #{ticket_id}, original_requester_id: #{ticket.requester.id}, new_requester_id: #{anonymous_user.id}."
        self.class.statsd_client.increment('ticket_requester_anonymized', tags: ["account_id:#{account_id}"])
        ticket.update_column(:requester_id, anonymous_user.id)
      end

      def switch_submitter
        return unless account.has_ticket_user_scrubbing_after_close?
        return unless ticket.submitter.is_end_user?

        Rails.logger.info "Ticket #{ticket_id} submitter replaced account_id: #{account_id}, ticket_id: #{ticket_id}, original_submitter_id: #{ticket.submitter.id}, new_submitter_id: #{anonymous_user.id}."
        self.class.statsd_client.increment('ticket_submitter_anonymized', tags: ["account_id:#{account_id}"])
        ticket.update_column(:submitter_id, anonymous_user.id)
      end

      def scrub_pii_in_events
        ticket.events.find_each do |event|
          event.mark_for_scrubbing = true
          if event.type.in?(AUDIT_EVENTS_FOR_SCRUBBING_METADATA)
            scrub_pii_metadata_in_audit_events(event)
          end

          next unless account.has_ticket_user_scrubbing_after_close?

          common_pii_tags = ["account_id:#{account_id}", "type:#{event.type}"]

          case event.type
          when *EVENTS_FOR_SCRUBBING_RECIPIENTS
            switch_notification_recipients(event)
            self.class.statsd_client.increment('event_anonymized', tags: common_pii_tags)
          when *COMMENTS_FOR_SCRUBBING_VALUE_PREVIOUS
            switch_voice_comment_user(event)
            self.class.statsd_client.increment('event_anonymized', tags: common_pii_tags)
          when *CREATE_CHANGE_EVENTS_FOR_SCRUBBING_VALUE
            anonymize_change_or_create_event(event)
            self.class.statsd_client.increment('event_anonymized', tags: common_pii_tags)
          when "ChannelBackEvent"
            anonymize_channel_back_event(event)
            self.class.statsd_client.increment('event_anonymized', tags: common_pii_tags)
          when *AUDIT_EVENTS_FOR_SCRUBBING_METADATA
            # metadata scrubbing is handle in the method scrub_pii_metadata_in_audit_events
            nil
          else
            self.class.statsd_client.increment('unhandle_event_type', tags: common_pii_tags)
          end

          switch_author_id_in_event(event)
          event.save! if event.changed?
        end
      end

      def switch_author_id_in_event(event)
        if event.author.is_end_user?
          old_value = event.author_id
          event.author_id = anonymous_user.id
          log_change_in_event(event, 'author_id', old_value, anonymous_user.id)
        end
      end

      def switch_notification_recipients(event)
        return unless account.has_ticket_user_scrubbing_after_close?
        old_value = event.recipients.join(",")

        end_user_ids = event.recipients.select do |recipient_id|
          User.find_by(id: recipient_id)&.is_end_user?
        end

        end_user_ids.each do |recipient_id|
          event.recipients.delete(recipient_id)
          event.recipients << anonymous_user.id
        end

        event.value = event.recipients
        log_change_in_event(event, 'recipients', old_value, event.recipients.join(","))
      end

      def switch_voice_comment_user(event)
        return unless account.has_ticket_user_scrubbing_after_close?
        return unless event.data[:answered_by_id]

        user = User.find_by(id: event.data[:answered_by_id])

        return unless user&.is_end_user?

        old_value = event.data[:answered_by_id]
        event.data[:answered_by_id] = anonymous_user.id
        log_change_in_event(event, 'answered_by_id', old_value, anonymous_user.id)

        old_value = event.data[:answered_by_name]
        event.data[:answered_by_name] = anonymous_user.name
        log_change_in_event(event, 'answered_by_name', old_value, anonymous_user.name)
      end

      def anonymize_channel_back_event(event)
        return unless HashParam.ish?(event.value)

        event.value[:requester].each { |k, _v| event.value[:requester][k] = "" } if HashParam.ish?(event.value[:requester])
        event.value[:external_id] = "" if event.value[:external_id].is_a? String
        event.value[:text] = "" if event.value[:text].is_a? String
      end

      def anonymize_change_or_create_event(event)
        return unless account.has_ticket_user_scrubbing_after_close?
        case event.value_reference
        when 'requester_id'
          switch_requester_in_change_or_create_event(event)
        when 'subject'
          anonymize_subject_in_change_or_create_event(event)
        end
      end

      def switch_requester_in_change_or_create_event(event)
        current_user = event.value && User.find_by_id(event.value)
        previous_user = event.value_previous && User.find_by_id(event.value_previous)
        if current_user && current_user.is_end_user?
          old_value = event.value
          event.value = anonymous_user.id.to_s
          log_change_in_event(event, 'value', old_value, anonymous_user.id)
        end

        if previous_user && previous_user.is_end_user?
          old_value = event.value_previous
          event.value_previous = anonymous_user.id.to_s
          log_change_in_event(event, 'value_previous', old_value, anonymous_user.id)
        end
      end

      def anonymize_subject_in_change_or_create_event(event)
        return unless VIA_TO_ANONYMIZE_SUBJECT.include?(ticket.via_id)
        event.value = ::I18n.t("txt.ticket_anonymize.redacted_subject") unless event.value.to_s.empty?
        event.value_previous = ::I18n.t("txt.ticket_anonymize.redacted_subject") unless event.value_previous.to_s.empty?
      end

      def log_change_in_event(event, value_changed, old_value, new_value)
        Rails.logger.info("Event #{event.id} anonymized. account_id: #{account_id}, ticket_id #{ticket_id}, type: #{event.type}, value_changed: #{value_changed}, old_value: #{old_value},new_value: #{new_value}")
      end

      def anonymize_ticket_pii_based_on_channel
        case ticket.via_id
        when *VIA_TO_ANONYMIZE_SUBJECT
          anonymize_ticket_subject
        when *VIA_TWITTER
          anonymize_ticket_created_via_twitter
        when *VIA_FACEBOOK
          anonymize_event_decoration_attachments
        end
      end

      def anonymize_ticket_subject
        ticket.update_column :subject, ::I18n.t("txt.ticket_anonymize.redacted_subject")
      end

      def anonymize_ticket_created_via_twitter
        ticket.event_decorations.find_each do |event|
          next if event.data.source.screen_name == event.data.author.screen_name
          original_data = event.data
          author_anonymized = {}

          original_data.external_id = ""

          event.data.author.each do |k, v|
            author_anonymized[k] = (v.is_a? String) ? "" : v
          end

          original_data.author = author_anonymized
          event.data = original_data
          event.save!
        end
      end

      def anonymize_event_decoration_attachments
        ticket.event_decorations.find_each do |event|
          next unless event.data&.links.present?
          original_data = event.data
          original_data.links = {}
          event.data = original_data
          event.save!
        end
      end

      def scrub_pii_metadata_in_audit_events(event)
        old_value = event.metadata
        event.scrub_metadata!
        log_change_in_event(event, 'metadata', old_value, event.metadata)
      end

      def remove_remote_files
        ticket.delete_attachments
        ticket.delete_raw_emails
      end

      def statsd_record_ticket_scrubbing_completed
        CIA.record(:scrub_pii_ticket_close, ticket)
        close_event = ticket.events.where(type: 'Change', value_reference: 'status_id', value: StatusType.CLOSED).last
        return unless close_event
        Rails.logger.info "Ticket #{ticket_id} anonymized. account_id: #{account_id}"
        time_to_complete_scrubbing = Time.now - close_event.created_at
        self.class.statsd_client.distribution('time_to_complete_scrubbing', time_to_complete_scrubbing.to_i, tags: ["account_id:#{account_id}"])
      end

      def statsd_record_ticket_scrubbing_failed(error)
        message = "Ticket scrubbing process failed  from ticket_id #{ticket_id} / account_id #{account_id} due #{error.message}"
        self.class.statsd_client.event("ticket scrubbing process failed", message, alert_type: 'warning')
        self.class.statsd_client.increment('failed_ticket_scrubbing', tags: ["error_type:#{error.class}", "account_id:#{account_id}", "ticket_id:#{ticket_id}"])
        Rails.logger.warn "Ticket #{ticket_id} anonymization failed. account_id: #{account_id}, ticket_id: #{ticket_id}, error: #{error.message}"
      end
    end
  end
end
