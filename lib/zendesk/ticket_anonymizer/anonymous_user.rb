module Zendesk
  module TicketAnonymizer
    class AnonymousUser
      NAME = 'Anonymous User'.freeze
      MAX_TICKETS_PER_ANONYMOUS_USER = ENV.fetch('MAX_TICKETS_PER_ANONYMOUS_USER', 1_000).to_i

      def self.find_or_create(account:)
        new(account: account).find_or_create_user
      end

      def initialize(account:)
        @account = account
      end

      def find_or_create_user
        if existing_user && existing_user.tickets.size < MAX_TICKETS_PER_ANONYMOUS_USER
          existing_user
        else
          create_user
        end
      end

      private

      attr_reader :account

      def existing_user
        ActiveRecord::Base.on_slave do
          account.scrubbed_ticket_anonymous_users.last
        end
      end

      def create_user
        account.users.create! do |u|
          u.name = NAME
          u.skip_verification = true
          u.settings.scrubbed_ticket_anonymous_user = true
        end
      end
    end
  end
end
