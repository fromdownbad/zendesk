require 'pid_controller'
require 'zendesk_database_support/sensors/aurora_cpu_sensor'
require 'zendesk_database_support/sensors/maxwell_filters_lag_sensor'
require 'zendesk_database_support/sensors/slave_delay_sensor'
require 'zendesk_database_support/sensors/aurora_trx_sensor'
require 'zendesk_database_support/sensors/aurora_delete_trx_sensor'

module Zendesk
  class DatabaseBackoff
    ControlSystem = Struct.new(:controller, :sensor) do
      attr_reader :measurement, :last_output

      def output
        if @measurement = sensor.measure
          # Why negate the value? In control, measurement + error = target.
          # If we're over the target (e.g. CPU is 70 when we want 60), then the
          # error is negative, with the implication we should do LESS work.
          # Because this output is passed to `sleep`, we change the output
          # to a positive duration in seconds.
          @last_output = if constant_duration?
            # Experiment: The output is used to sleep, which means the next
            # measurement is inherient scaled by the previous output. This
            # may be causing i_terms to burn off their accumulated error too
            # quickly after a longer sleep, causing flapping/instability.
            -1.0 * controller.update_with_duration(measurement, 1.0).round(3)
          else
            -1.0 * (controller << measurement).round(3)
          end
        end
      end

      def constant_duration?
        Arturo.feature_enabled_for_pod?(:database_backoff_constant_duration, Zendesk::Configuration.fetch(:pod_id))
      end

      # Observability for logging / metrics
      def to_hash
        {
          measurement: measurement.try(:round, 3),
          output:      last_output.try(:round, 3),
          p_term:      controller.p_term.round(3),
          i_term:      controller.i_term.round(3),
          d_term:      controller.d_term.round(3)
        }
      end
    end

    StaticSystem = Struct.new(:output) do
      def to_hash
        { output: output }
      end
    end

    # WHERE DO THESE NUMBERS COME FROM? WHAT DO THEY MEAN?
    #
    # PID parameter tuning is a dark art, not a science. It's seeded by
    # intution and refined by observation. First we intuit "P", e.g. if we
    # think it takes 1 second to process 10 seconds of backlog, we'll set the P
    # to 0.1. I is reasonable to set to a fraction of P. Too high and it will
    # fluxuate; too low, and it takes too long to respond. D is kind of a crap
    # shoot and not entirely necessary, it's just to prevent overshoot. You can
    # set it to the I value or lower if you see yourself overshooting often.
    AURORA_CPU_KP = ENV.fetch('DATABASE_BACKOFF_AURORA_CPU_KP', 0.125).to_f
    AURORA_CPU_KI = ENV.fetch('DATABASE_BACKOFF_AURORA_CPU_KI', 0.075).to_f
    AURORA_CPU_KD = ENV.fetch('DATABASE_BACKOFF_AURORA_CPU_KD', 0.025).to_f

    SLAVE_LAG_KP = ENV.fetch('DATABASE_BACKOFF_SLAVE_LAG_KP', 0.5).to_f
    SLAVE_LAG_KI = ENV.fetch('DATABASE_BACKOFF_SLAVE_LAG_KI', 0.1).to_f
    SLAVE_LAG_KD = ENV.fetch('DATABASE_BACKOFF_SLAVE_LAG_KD', 0.1).to_f

    DELETE_TRX_LAG_THRESHOLD = 1

    def initialize(minimum: 0.0, cpu_target: 40.0, maxwell_filters_lag_target: 20.0, slave_delay_target: 2.0, monitor_maxwell_filters: true, trx_target: 25.0, delete_trx_target: 3.0)
      raise ArgumentError, ':cpu_target must be between 1 and 100' unless (1..100).cover?(cpu_target)
      raise ArgumentError, ':maxwell_filters_lag_target must be at least 1.0 to avoid integral windup' if maxwell_filters_lag_target < 1.0
      raise ArgumentError, ':slave_delay_target must be at least 1.0 to avoid integral windup' if slave_delay_target < 1.0
      @systems = {
        minimum: StaticSystem.new(minimum),
        aurora_cpu: ControlSystem.new(
          # See comment in ControlSystem#output about why the max (instead of min) is 0
          PidController.new(
            setpoint: cpu_target,
            kp: AURORA_CPU_KP,
            ki: AURORA_CPU_KI,
            kd: AURORA_CPU_KD,
            output_max: 0.0,
            integral_max: 0.0
          ),
          ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.new
        ),
        slave_lag: ControlSystem.new(
          PidController.new(
            setpoint: slave_delay_target,
            kp: SLAVE_LAG_KP,
            ki: SLAVE_LAG_KI,
            kd: SLAVE_LAG_KD,
            output_max: 0.0,
            integral_max: 0.0
          ),
          ZendeskDatabaseSupport::Sensors::SlaveDelaySensor.new
        )
      }

      if monitor_maxwell_filters && Arturo.feature_enabled_for_pod?(:database_backoff_maxwell_filters_lag, Zendesk::Configuration.fetch(:pod_id))
        maxwell_lag_eq = Zendesk::PIDEqualizer.new(
          p_arturo: :database_backoff_maxwell_filters_lag_p,
          i_arturo: :database_backoff_maxwell_filters_lag_i,
          d_arturo: :database_backoff_maxwell_filters_lag_d
        )

        # Note this sensor is ~15ms/call
        @systems[:maxwell_filters_lag] = ControlSystem.new(
          PidController.new(
            setpoint: maxwell_filters_lag_target,
            kp: maxwell_lag_eq.p_gain,
            ki: maxwell_lag_eq.i_gain,
            kd: maxwell_lag_eq.d_gain,
            output_max: 0.0,
            integral_max: 0.0
          ),
          ZendeskDatabaseSupport::Sensors::MaxwellFiltersLagSensor.new(shard_id: ActiveRecord::Base.current_shard_id)
        )
      end

      if Arturo.feature_enabled_for_pod?(:aurora_database_backoff_trx_sensor, Zendesk::Configuration.fetch(:pod_id))
        trx_backoff_eq = Zendesk::PIDEqualizer.new(
          p_arturo: :aurora_database_backoff_trx_sensor_p,
          i_arturo: :aurora_database_backoff_trx_sensor_i,
          d_arturo: :aurora_database_backoff_trx_sensor_d
        )

        @systems[:aurora_trx] = ControlSystem.new(
          PidController.new(
            setpoint: trx_target,
            kp: trx_backoff_eq.p_gain,
            ki: trx_backoff_eq.i_gain,
            kd: trx_backoff_eq.d_gain,
            output_max: 0.0,
            integral_max: 0.0
          ),
          ZendeskDatabaseSupport::Sensors::AuroraTrxSensor.new
        )
      end

      if Arturo.feature_enabled_for_pod?(:aurora_database_backoff_delete_trx_sensor, Zendesk::Configuration.fetch(:pod_id))
        delete_trx_backoff_eq = Zendesk::PIDEqualizer.new(
          p_arturo: :aurora_database_backoff_delete_trx_sensor_p,
          i_arturo: :aurora_database_backoff_delete_trx_sensor_i,
          d_arturo: :aurora_database_backoff_delete_trx_sensor_d
        )

        @systems[:aurora_delete_trx] = ControlSystem.new(
          PidController.new(
            setpoint: delete_trx_target,
            kp: delete_trx_backoff_eq.p_gain,
            ki: delete_trx_backoff_eq.i_gain,
            kd: delete_trx_backoff_eq.d_gain,
            output_max: 0.0,
            integral_max: 0.0
          ),
          # trx_time_threshold represents number of seconds transaction is running.
          ZendeskDatabaseSupport::Sensors::AuroraDeleteTrxSensor.new(trx_time_threshold: DELETE_TRX_LAG_THRESHOLD)
        )
      end
    end

    # @return [Float] How long should the caller sleep for?
    def backoff_duration
      @last_backoff_duration = @systems.values.map(&:output).compact.max || 0.0
      @systems.each do |name, sys|
        statsd_client.histogram("#{name}.measurement", sys.measurement) if sys.is_a?(ControlSystem)
      end
      statsd_client.histogram('duration', @last_backoff_duration)
      @last_backoff_duration
    end

    # Observability for logging / metrics
    def to_hash
      Hash[@systems.map { |k, v| [k, v.to_hash] }].merge(backoff_duration: @last_backoff_duration.try(:round, 3))
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(
        namespace: ['database_backoff']
      )
    end
  end
end
