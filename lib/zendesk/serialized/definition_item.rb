class DefinitionItem
  TIME_BASED_ATTRIBUTES = [
    'new', 'open', 'pending', 'solved', 'closed', 'assigned_at',
    'updated_at', 'requester_updated_at', 'assignee_updated_at', 'due_date'
  ].freeze

  class InvalidSourceColumn < StandardError; end

  attr_accessor :value, :source, :operator

  def self.build(params)
    source = params[:source]
    source ||= ZendeskRules::RuleField.lookup(params[:field]).try(:db_column)
    source ||= params[:field]
    new(source.to_s, params[:operator], params[:value])
  end

  def initialize(source, operator, value = nil)
    @source     = source
    @operator   = operator
    value       = value.values if value.is_a?(ActionController::Parameters)
    @value      = Array.wrap(parse_value(scrub_input(value)))
  end

  def ==(other)
    return false unless other.is_a?(self.class)

    source     == other.source &&
      operator == other.operator &&
      value    == other.value
  end

  def cache_key
    Digest::MD5.hexdigest([@source, @operator, @value].join('/'))
  end

  def merge!(defn)
    if source == defn.source && operator == defn.operator
      @value = defn.value
    end
  end

  def user_specific?
    ([*(value || [])].first || '').to_s.strip == 'current_user'
  end

  def group_specific?
    v = ([*(value || [])].first || '').to_s.strip
    ['current_groups', 'group_id'].member?(v.downcase)
  end

  def is_time_based? # rubocop:disable Naming/PredicateName
    TIME_BASED_ATTRIBUTES.member?(source.downcase)
  end

  def first_value
    @first_value ||= value.is_a?(Array) ? value.compact.first : value
  end

  def second_value
    @second_value ||= value.is_a?(Array) ? value.compact[1] : nil
  end

  def humanize(account)
    [RuleDictionary.source_translation(source), RuleDictionary.operator_translation(operator), humanize_value(account)]
  end

  def humanize_value(account)
    lookup_properties = [
      'assignee_id', 'assignee_id#all_agents', 'group_id', 'group_id#current_groups', 'requester_id', 'requester_id#all_agents',
      'organization_id', 'via_id', 'current_via_id', 'status_id', 'priority_id', 'ticket_type_id'
    ]

    if lookup_properties.index(source) && (Integer(first_value) rescue nil)
      case source
      when 'assignee_id', 'assignee_id#all_agents' then lookup_name(account.agents, first_value)
      when 'group_id', 'group_id#current_groups' then lookup_name(account.groups, first_value)
      when 'requester_id', 'requester_id#all_agents' then lookup_name(account.users, first_value)
      when 'organization_id' then lookup_name(account.organizations, first_value)
      when 'via_id', 'current_via_id' then I18n.t("txt.via_types.#{ViaType.to_s(first_value.to_s.to_i).downcase.split(" ").join("_")}")
      when 'status_id' then StatusType.to_s(first_value.to_s.to_i)
      when 'priority_id' then PriorityType.to_s(first_value.to_s.to_i)
      when 'ticket_type_id' then TicketType.to_s(first_value.to_s.to_i)
      else first_value
      end
    elsif ['notification_user', 'notification_group'].index(source)
      if (Integer(first_value) rescue nil)
        lookup_name((source == 'notification_group' ? account.groups : account.users), first_value)
      elsif first_value.blank?
        I18n.t('txt.admin.lib.zendesk.serialized.definition.blank')
      else
        RuleDictionary.value_translation(first_value).to_s
      end
    elsif source == 'notification_target'
      lookup_name(account.targets, first_value)
    elsif source == 'macro_id' || source == 'macro_id_after'
      lookup_name(account.all_macros, first_value)
    elsif translation = RuleDictionary.value_translation(first_value)
      translation
    else
      first_value.blank? ? I18n.t('txt.admin.lib.zendesk.serialized.definition.blank') : value
    end
  end

  def lookup_name(records, id)
    return I18n.t('txt.admin.lib.zendesk.serialized.definition.na') unless record = records.find_by_id(id.to_i)
    return record.name if record.respond_to?(:name)
    record.title
  end

  def sanitized_source
    # Condition looks redundant, but allocs 0 objects when source excludes '#'
    @sanitized_source ||= if source.include?('#'.freeze)
      source.split('#'.freeze, 2).first
    else
      source
    end
  end

  MEMOIZABLE_COMMENT_FIELDS = ['description_includes_word'.freeze, 'comment_includes_word'.freeze, 'subject_includes_word'.freeze].freeze
  # Builds a key for storing memoized result if this condition matches the current state
  def memoization_key(ticket)
    if MEMOIZABLE_COMMENT_FIELDS.member?(sanitized_source)
      # Memoize comment matches as these are static
      "#{sanitized_source}/#{source.inspect}/#{operator.inspect}/#{value.inspect}/#{ticket.id || ticket.object_id}"
    elsif "current_tags".freeze == sanitized_source
      # Memoize tag matches by conditions and current ticket tags
      "#{sanitized_source}/#{source.inspect}/#{operator.inspect}/#{value.inspect}/#{ticket.current_tags}"
    else
      # Someone trying to memoize for an unsupported source
      raise "Cannot memoize triggers with source field #{source.inspect}"
    end
  end

  MEMOIZABLE_FIELDS = ['description_includes_word'.freeze, 'comment_includes_word'.freeze, 'current_tags'.freeze, 'subject_includes_word'.freeze].freeze
  def memoizable?
    MEMOIZABLE_FIELDS.include?(sanitized_source)
  end

  # Internal: Checks whether the current action, when applied, nullifies the given `condition`.
  # A condition is considered nullified if the action, when applied, will cause the same condition
  # to evaluate to false on the next run.  Both condition and action source are compared to see if
  # they're equal by stripping off any suffix beginning with #, e.g. group_id#current_groups
  #
  # condition - The DefinitionItem of the condition to check against
  # action    - The DefinitionItem of the action to apply
  #
  # Examples
  #
  #   status_id is ['2'] :: is ['3'] => true
  #   status_id is ['2'] :: is ['2'] => false
  #
  #   status_id is_not ['2'] :: is ['3'] => false
  #   status_id is_not ['2'] :: is ['2'] => true
  #
  #   status_id less_than ['2'] :: is ['3'] => true
  #   status_id less_than ['2'] :: is ['2'] => true
  #   status_id less_than ['2'] :: is ['0'] => false
  #
  #   status_id greater_than ['2'] :: is ['3'] => false
  #   status_id greater_than ['2'] :: is ['2'] => true
  #   status_id greater_than ['2'] :: is ['0'] => true
  #
  #   current_tags includes ['123'] :: set_tags ['123'] => false
  #   current_tags includes ['123'] :: set_tags ['abc'] => true
  #
  #   current_tags includes ['123'] :: add_tags ['123'] => false
  #   current_tags includes ['123'] :: add_tags ['abc'] => false
  #
  #   current_tags includes ['123'] :: remove_tags ['123'] => true
  #   current_tags includes ['123'] :: remove_tags ['abc'] => false
  #
  #   current_tags not_includes ['123'] :: set_tags ['123'] => true
  #   current_tags not_includes ['123'] :: set_tags ['abc'] => false
  #
  #   current_tags not_includes ['123'] :: add_tags ['123'] => true
  #   current_tags not_includes ['123'] :: add_tags ['abc'] => false
  #
  #   current_tags not_includes ['123'] :: remove_tags ['123'] => false
  #   current_tags not_includes ['123'] :: remove_tags ['abc'] => false
  #
  # Returns true if condition is nullified, false otherwise.
  def nullifies?(condition, action = self)
    if condition.source == 'current_tags'
      return nullifies_tags?(condition, action)
    end

    return unless same_source?(condition, action)
    return unless action.operator == 'is'

    if condition.source == 'satisfaction_score'
      return nullifies_satisfaction?(condition)
    end

    case condition.operator
    when 'is'
      action.value != condition.value
    when 'is_not'
      action.value == condition.value
    when 'not_present'
      !action.value.empty?
    when 'less_than'
      action.first_value >= condition.first_value
    when 'greater_than'
      action.first_value <= condition.first_value
    # for custom user/org fields
    when 'less_than_equal'
      action.first_value > condition.first_value
    when 'greater_than_equal'
      action.first_value < condition.first_value
    end
  end

  def to_h
    {source: source, operator: operator, value: value}
  end

  private

  def parse_value(value)
    mapping = Api::V2::Tickets::AttributeMappings::ATTRIBUTE_MAPPINGS[@source.to_sym]

    if mapping.try(:key?, :full)
      wrapped = Array.wrap(value).first.to_s.downcase

      mapping[:full].invert[wrapped.presence] || value
    elsif mapping.try(:key?, :using)
      wrapped = Array.wrap(value).first.to_s.downcase

      mapping[:using].invert[wrapped.presence] || value
    else
      value
    end
  end

  def scrub_input(arg)
    if arg.is_a?(String)
      # javascript has problems handling the non-breaking line characters
      arg.clean_separators
    elsif arg.is_a?(Array)
      arg.map { |r| scrub_input(r) }
    # The following supports the rule value syntax of
    # { "0" => "val", "1" => "val2" } instead of an Array
    elsif arg.is_a?(Hash)
      scrub_input(arg.values)
    else
      arg
    end
  end

  def nullifies_tags?(condition, action)
    return unless condition.source == 'current_tags'

    condition_tags = condition.value.first.split(" ")
    action_tags = action.value.first.split(" ")

    if condition.operator == 'includes'
      case action.source
      when 'set_tags'
        return (condition_tags & action_tags).empty?
      when 'remove_tags'
        return (condition_tags - action_tags).empty?
      when 'current_tags'
        return false
      end
    elsif condition.operator == 'not_includes'
      case action.source
      when 'set_tags'
        return (condition_tags & action_tags).any?
      when 'remove_tags'
        return false
      when 'current_tags'
        return (condition_tags & action_tags).any?
      end
    end
  end

  def nullifies_satisfaction?(condition)
    case condition.operator
    when 'is'
      condition.first_value.to_i == SatisfactionType.UNOFFERED
    when 'less_than'
      condition.first_value.to_i == SatisfactionType.OFFERED
    else
      false
    end
  end

  def same_source?(condition, action)
    action.source.split("#", 2).first == condition.source.split("#", 2).first
  end
end
