require 'color'
#
# Account branding
#
class Branding
  attr_accessor :header_color, :tab_background_color, :tab_hover_color, :text_color, :page_background_color, :sidebox_color
  def initialize
    # defaults
    @header_color = '03363D'
    @tab_background_color = '7FA239'
    @tab_hover_color = 'ACC76B'
    @text_color = 'FFFFFF'
    @page_background_color = '333333'
    @sidebox_color = 'F6F6F6'
  end

  def self.create(options)
    new_branding = Branding.new
    if options
      new_branding.tab_hover_color        = options[:tab_hover_color] if validate(options[:tab_hover_color])
      new_branding.header_color           = options[:header_color] if validate(options[:header_color])
      new_branding.tab_background_color   = options[:tab_background_color] if validate(options[:tab_background_color])
      new_branding.text_color             = options[:text_color] if validate(options[:text_color])
      new_branding.page_background_color  = options[:page_background_color] if validate(options[:page_background_color])
      new_branding.sidebox_color          = options[:sidebox_color] if validate(options[:sidebox_color])
    end
    new_branding
  end

  def to_h
    { header_color: header_color,
      tab_background_color: tab_background_color,
      tab_hover_color: tab_hover_color,
      text_color: text_color,
      page_background_color: page_background_color,
      sidebox_color: sidebox_color}
  end

  def mobile_border_color
    r, g, b = tab_background_color.scan(/../).map(&:hex)
    new_color = Color::RGB.new(r, g, b)

    new_color = new_color.darken_by(90)

    new_color.html
  end

  # Check that value is a valid hex
  private_class_method def self.validate(value)
    value.is_a?(String) && value.length == 6 && (value.downcase =~ /[0-9a-f]{6}/) == 0
  end
end
