# Rule definition format (YAML serialized when saved to DB). The rule definition contains the
# conditions which restrict the query executed by the definition, e.g. the requirements that
# results of executing this rule must abide, such as "status is open".
#
# The definition contains "all" conditions and "any" conditions for boolean flexibility. These
# end up as AND and OR parts in a SQL query.
#
# Lastly a definition may contain actions. The implication of these is usually that "execute these
# actions of the items that match the definition""
#

require 'digest/md5'

class Definition
  class CIASerializer
    class << self
      def serialize(definition)
        conditions_and_actions = conditions_and_actions_as_strings(definition)
        CIA::AttributeChange.serialize_for_storage(conditions_and_actions) do |ca|
          remove_largest_item(ca)
          ca
        end
      end

      private

      def remove_largest_item(conditions_and_actions)
        conditions_and_actions.sort_by do |strings|
          strings.map(&:size).sum
        end.last.pop
      end

      def conditions_and_actions_as_strings(definition)
        Definition::DEFINITION_ITEM_SETS.map do |scope|
          definition_items_to_string_for(definition.send(scope))
        end
      end

      def definition_items_to_string_for(scope)
        scope.map do |definition_item|
          [definition_item.source,
           definition_item.operator,
           definition_item.first_value.to_s.truncate(24)].join(" ")
        end
      end
    end
  end

  REQUIRED_ALL_ATTRIBUTES = [
    'OPEN', 'PENDING', 'HOLD', 'CLOSED', 'due_date', 'status_id', 'ticket_type_id',
    'group_id', 'assignee_id', 'requester_id', 'organization_id'
  ].freeze

  CONDITIONS_PRIORITY_MAP = { # rubocop:disable Style/MutableConstant
    "status_id"             => 10,
    "priority_id"           => 10,
    "ticket_type_id"        => 10,
    "via_id"                => 10,
    "current_via_id"        => 10,
    "group_id"              => 10,
    "organization_id"       => 10,
    "assignee_id"           => 10,
    "requester_id"          => 10,
    "subject_includes_word" => 100,
    "comment_includes_word" => 100
  }
  CONDITIONS_PRIORITY_MAP.default = 50
  CONDITIONS_PRIORITY_MAP.freeze

  IMMUTABLE_CONDITIONS_PRIORITY_MAP = {
    "agent_stations"     => 5,
    "ticket_is_public"   => 5,
    "comment_is_public"  => 5,
    "current_via_id"     => 5,
    "group_stations"     => 5,
    "in_business_hours"  => 5,
    "recipient"          => 5,
    "reopens"            => 5,
    "replies"            => 5,
    "requester_id"       => 5,
    "role"               => 5,
    "satisfaction_score" => 5,
    "update_type"        => 5,
    "via_id"             => 5,
    "requester_twitter_followers_count" => 5,
    "requester_twitter_statuses_count"  => 5,
    "requester_twitter_verified"        => 5,
    "collaboration_thread" => 5
  }.freeze

  DEFINITION_ITEM_SETS = [:conditions_all, :conditions_any, :actions].freeze

  attr_reader *DEFINITION_ITEM_SETS

  def self.build(params)
    new.tap do |definition|
      definition.conditions_all.push(*build_items(params[:conditions_all]))
      definition.conditions_any.push(*build_items(params[:conditions_any]))
      definition.actions.push(*build_items(params[:actions]))
    end
  end

  def self.build_items(items)
    definition_items = items.to_a.map do |item|
      next unless item[:source].present? || item[:field].present?
      next if ["PICKCONDITION", "PICKACTION"].include?(item[:source])

      item[:value] ||= []
      DefinitionItem.build(item)
    end
    definition_items.compact
  end

  def self.from_ticket(ticket, user)
    new.tap do |definition|
      unless ticket.status?(:new)
        definition.actions.push(
          DefinitionItem.new('status_id', 'is', ticket.status_id)
        )
      end

      if irrelevant_priority?(ticket)
        definition.actions.push(
          DefinitionItem.new('priority_id', 'is', ticket.priority_id)
        )
      end

      if irrelevant_ticket_type?(ticket)
        definition.actions.push(
          DefinitionItem.new('ticket_type_id', 'is', ticket.ticket_type_id)
        )
      end

      if ticket.assignee_id.present?
        definition.actions.push(
          DefinitionItem.new('assignee_id', 'is', ticket.assignee_id)
        )
      end

      if ticket.group_id.present?
        definition.actions.push(
          DefinitionItem.new('group_id', 'is', ticket.group_id)
        )
      end

      if ticket.current_tags.present?
        definition.actions.push(
          DefinitionItem.new('current_tags', 'is', ticket.current_tags)
        )
      end

      comment = ticket.comments.last

      if comment.present? && comment.author == user
        definition.actions.push(
          DefinitionItem.new('comment_mode_is_public', 'is', comment.is_public?.to_s)
        )

        if user.account.settings.rich_text_comments?
          definition.actions.push(
            DefinitionItem.new('comment_value_html', 'is', comment.html_body)
          )
        else
          definition.actions.push(
            DefinitionItem.new('comment_value', 'is', comment.body)
          )
        end
      end
    end
  end

  def self.irrelevant_priority?(ticket)
    return false unless ticket.priority_id.present?

    default_value = Zendesk::Types::PriorityType.find(
      Zendesk::Types::PriorityType.default_placeholder
    )

    Zendesk::Types::PriorityType.
      values(exclude: default_value).
      include?(ticket.priority_id)
  end

  def self.irrelevant_ticket_type?(ticket)
    return false unless ticket.extended_ticket_types?

    default_value = Zendesk::Types::TicketType.find(
      Zendesk::Types::TicketType.default_placeholder
    )

    Zendesk::Types::TicketType.
      values(exclude: default_value).
      include?(ticket.ticket_type_id)
  end

  def initialize(all = [], any = [], actions = [])
    @conditions_all = all
    @conditions_any = any
    @actions        = actions
  end

  def ==(other)
    return false unless other.is_a?(self.class)

    conditions_all   == other.conditions_all &&
      conditions_any == other.conditions_any &&
      actions        == other.actions
  end

  def cache_key
    cache_key = ""

    [conditions_all, conditions_any, actions].each do |defns|
      cache_key << Digest::MD5.hexdigest(defns.map(&:cache_key).join("/"))
      cache_key << ";"
    end

    Digest::MD5.hexdigest(cache_key)
  end

  def sorted_conditions_all
    @sorted_conditions_all ||= conditions_all.sort do |x, y|
      x_score = IMMUTABLE_CONDITIONS_PRIORITY_MAP[x.sanitized_source] ||
        CONDITIONS_PRIORITY_MAP[x.sanitized_source]
      y_score = IMMUTABLE_CONDITIONS_PRIORITY_MAP[y.sanitized_source] ||
        CONDITIONS_PRIORITY_MAP[y.sanitized_source]

      x_score <=> y_score
    end
  end

  def sorted_conditions_any
    @sorted_conditions_any ||= conditions_any.sort do |x, y|
      CONDITIONS_PRIORITY_MAP[x.sanitized_source] <=> CONDITIONS_PRIORITY_MAP[y.sanitized_source]
    end
  end

  def conditions_all
    @conditions_all ||= []
  end

  def conditions_any
    @conditions_any ||= []
  end

  def merge(defn)
    all = merge_conditions(conditions_all.map(&:dup), defn.conditions_all)
    any = merge_conditions(conditions_any.map(&:dup), defn.conditions_any)
    actions = merge_conditions(self.actions.map(&:dup), defn.actions)

    Definition.new(all, any, actions)
  end

  def merge!(defn)
    @conditions_all = merge_conditions(conditions_all, defn.conditions_all)
    @conditions_any = merge_conditions(conditions_any, defn.conditions_any)
    @actions = merge_conditions(actions, defn.actions)
  end

  def optimized_conditions_all
    if conditions_any.size == 1
      conditions
    else
      conditions_all
    end
  end

  def missing_required_condition?
    !conditions_all.detect { |c| REQUIRED_ALL_ATTRIBUTES.member?(c.source) }
  end

  def add_status_less_than_solved_definition
    definition = DefinitionItem.new('status_id', 'less_than', StatusType.SOLVED)
    conditions_all.push(definition)
  end

  def items
    items = conditions_all.to_a + conditions_any.to_a + actions.to_a
    items.compact!
    items
  end

  def grouped_actions
    actions.group_by(&:source).with_indifferent_access
  end

  def optimized_conditions_any
    conditions_any.size == 1 ? [] : conditions_any
  end

  def basic_comment_actions
    actions.select { |action| action.source == 'comment_value' }
  end

  def rich_comment_actions
    actions.select { |action| action.source == 'comment_value_html' }
  end

  def has_rich_comment_action? # rubocop:disable Naming/PredicateName
    actions.any? { |action| action.source == 'comment_value_html' }
  end

  def has_side_conversation_action? # rubocop:disable Naming/PredicateName
    actions.any? { |action| action.source.start_with?('side_conversation') }
  end

  def user_specific?
    conditions.any?(&:user_specific?)
  end

  def group_specific?
    conditions.any?(&:group_specific?)
  end

  def is_time_based? # rubocop:disable Naming/PredicateName
    conditions.any?(&:is_time_based?)
  end

  def serialize_for_cia
    CIASerializer.serialize(self)
  end

  def change_conditions?
    conditions.any? { |condition| change_condition?(condition) }
  end

  def satisfaction_prediction_conditions?
    conditions.any? { |condition| satisfaction_prediction_condition?(condition) }
  end

  def run_new_validations(account, type, containing_item, current_user)
    return unless account

    context = Zendesk::Rules::Context.new(account, current_user, rule_type: type.downcase.to_sym, validating: true)
    context.options.merge!(component_type: :condition, condition_type: :all)

    validate_item_list(context, ZendeskRules::Definitions::Conditions, conditions_all, account, containing_item)

    context.options.merge!(component_type: :condition, condition_type: :any)
    validate_item_list(context, ZendeskRules::Definitions::Conditions, conditions_any, account, containing_item)

    context.options[:component_type] = :action
    validate_item_list(context, ZendeskRules::Definitions::Actions, actions, account, containing_item)
  end

  protected

  def change_condition?(condition)
    condition.source == "update_type" && (condition.value.first == "Change" || condition.value == "Change")
  end

  def satisfaction_prediction_condition?(condition)
    condition.source == "satisfaction_probability" || (condition.source == "current_via_id" && condition.value.first == ViaType.SATISFACTION_PREDICTION.to_s)
  end

  def conditions
    conditions_all + conditions_any
  end

  def merge_conditions(ours, theirs)
    unused = theirs.dup

    ours.each do |left|
      theirs.each do |right|
        if left.merge!(right)
          unused.delete(right)
        end
      end
    end

    ours.concat(unused)
  end

  private

  def validate_item_list(context, component_base, item_list, account, containing_item)
    component_list = component_base.definition_list(context)
    item_list.each do |item|
      error_info = {}

      # allow people to still post old-style assignee_id#all_agents style rules
      source = item.source.to_s.split('#').first

      ret = component_list.valid_definition?(source, item.operator, item.value, error_info)
      next if ret
      key = "txt.admin.models.rules.rule.#{error_info[:error]}"
      error_info[:value] = ERB::Util.html_escape(error_info[:value].to_s)

      if error_info[:error] == :invalid_value && error_info[:value].present? && ["group_id", "assignee_id", "requester_id", "organization_id"].include?(error_info[:target_code])
        if error_info[:target_code] == "assignee_id" && account.users.find_by_id(error_info[:value])
          # allow downgraded agents to be queried on.
          next
        else
          # nice error messages for deleted users/groups/organizations
          key << "_#{build_error_target(error_info[:target_code])}"
        end
      end

      msg = ::I18n.t(key, error_info)
      containing_item.errors.add(:base, msg)
    end
  end

  def build_error_target(target_code)
    case target_code
    when "assignee_id", "requester_id"
      "user"
    else
      target_code.gsub("_id", "")
    end
  end
end
