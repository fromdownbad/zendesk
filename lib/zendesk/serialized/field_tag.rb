#
# Tag options for tagger ticket fields
#
class FieldTag
  attr_reader :name, :tag

  def initialize(name, tag)
    @name  = name
    @tag   = tag
  end
end
