#
# View sort, grouping and column definition (YAML'ed)
#
class Output
  attr_accessor :group_asc, :order_asc, :columns, :format
  attr_reader :order
  attr_writer :group, :type

  def self.create(options)
    new_output = Output.new
    if options
      new_output.format = options[:format]
      new_output.group = options[:group].blank? ? nil : options[:group].to_sym
      new_output.group_asc = options[:group_asc].is_a?(String) ? (options[:group_asc] == 'true') : options[:group_asc] if options[:group_asc]
      new_output.order = options[:order].blank? ? nil : options[:order].to_sym
      new_output.order_asc = options[:order_asc].is_a?(String) ? (options[:order_asc] == 'true') : options[:order_asc] if options[:order_asc]
      new_output.type = options[:type] if options[:type]
      if columns = options[:columns]
        # PT 17374237: strip leading and trailing commas which get converted to empty strings by split
        columns = columns.split(',').select { |s| s && !s.empty? }.uniq.collect(&:to_sym) if columns.is_a?(String)
        new_output.columns = columns
      end
    end
    new_output
  end

  def self.valid_columns(columns)
    Array.wrap(columns).reject do |column|
      column.to_s.casecmp('score').zero? || !ZendeskRules::RuleField.lookup(column)
    end
  end

  # Do NOT serialize local caches.  See https://support.zendesk.com/agent/tickets/908084.
  def to_yaml_properties
    instance_variables - %i[@rule_associations @fields_to_fom @rule_fields]
  end

  # Used for View previews
  # Columns are checked for custom fields by title or id
  # and converted into a usable format for CustomRuleField.lookup
  def self.default(options = {})
    options[:columns] = valid_columns(options[:columns]) if options[:columns]

    # Map input
    options[:order] ||= options.delete(:sort_by)
    options[:order_asc] ||= options.delete(:sort_order).to_s.casecmp("asc").zero?
    options[:group] ||= options.delete(:group_by)
    options[:group_asc] ||= options.delete(:group_order).to_s.casecmp("asc").zero?

    default = HashWithIndifferentAccess.new(
      group:     :status,
      group_asc: true,
      order:     :id,
      order_asc: true,
      type:      'table',
      columns:   %i[description requester created updated group assignee]
    )
    default.merge!(options)
    create(default)
  end

  def self.sanitized_type(type)
    return type if ['list', 'table'].member?(type)
    'list' # default type
  end

  def initialize
    # defaults
    @group = nil
    @group_asc = false
    @order = nil
    @order_asc = false
    @type = 'list'
    @columns = %i[satisfaction_score description requester created status assignee]
  end

  def sort_order
    order_to_sym(order_asc)
  end

  def group_order
    order_to_sym(group_asc)
  end

  def group
    @group unless @type == 'list'
  end

  def type
    Output.sanitized_type(@type)
  end

  def kind
    Output.sanitized_type(@type)
  end

  def columns_to_fom
    @columns_to_fom ||= columns.collect { |c| Zendesk::Fom::Field.for(c) }.compact
  end

  def column_identifiers
    @column_identifiers ||= columns_to_fom.collect(&:identifier)
  end

  def rule_associations
    @rule_associations ||= rule_fields.collect(&:association)
    if group && (group_field = ZendeskRules::RuleField.lookup(group))
      @rule_associations << group_field.association
    end
    @rule_associations.compact!
    @rule_associations
  end

  def fields_to_fom
    fields = [order]
    if type == 'table'
      fields = fields << group
      fields += columns
    else
      fields += %i[requester score]
    end
    @fields_to_fom ||= fields.uniq.compact.collect { |c| Zendesk::Fom::Field.for(c) }.compact
  end

  def remove_irrelevant_columns_for(account)
    t = columns.clone
    t.delete(:group) if account.groups.count(:all) < 2
    t.delete(:assignee) if account.agents.count(:all) < 2
    t.delete(:type) unless account.has_extended_ticket_types?
    t
  end

  def rule_fields
    @rule_fields ||= columns.map { |f| ZendeskRules::RuleField.lookup(f) }.compact
  end

  def set_order(params) # rubocop:disable Naming/AccessorMethodName
    # Passing sort_by=<field> and optionally sort_order=<asc|desc> is the prefered API
    if params[:sort_by].present? && field = Zendesk::Fom::Field.for(params[:sort_by].to_sym)
      field.order_by(params[:sort_order] == "desc")
    elsif params[:order].present? && field = Zendesk::Fom::Field.for(params[:order].to_sym)
      field.order_by(params[:desc].present?)
    end
  end

  def order=(order)
    @order = if order.to_s.casecmp('id').zero?
      :nice_id
    else
      order
    end
  end

  # Merges output with conditions
  # Takes the same parameters as Output.preview
  def merge!(options)
    self.columns = self.class.valid_columns(options[:columns]) if options[:columns]

    if options.key?(:sort_by) || options.key?(:sort_order)
      self.order = options.fetch(:sort_by, order)
      self.order_asc = !options[:sort_order].to_s.casecmp("desc").zero?
    end

    self.group = options[:group_by] if options.key?(:group_by)
    self.group_asc = options[:group_order].to_s.casecmp("asc").zero? if options.key?(:group_order)
  end

  def merge(options)
    dup.tap { |out| out.merge!(options) }
  end

  # this is not the same check as valid_columns because we need to preserve the 'score' column
  # in some cases.
  def strip_invalid_output_columns!(account)
    columns.reject! { |c| !ZendeskRules::RuleField.lookup(c, account_id: account.id) }
  end

  private

  def order_to_sym(field)
    field ? :asc : :desc
  end
end
