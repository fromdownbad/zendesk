module Zendesk::PermanentCookie
  class Store
    attr_reader :name, :cookies, :data, :options

    KEYS = ['device_tokens', Zendesk::Auth::Otp::Tracking::COOKIE_VALIDATION_KEY.to_s].freeze
    COOKIE_SECRETS  = [Zendesk::Configuration.fetch(:permanent_cookie_secret), Zendesk::Configuration.fetch(:session_salt)].freeze
    DEFAULT_OPTIONS = {
      path: '/',
      httponly: true,
      expires: (Time.zone.now + 20.years)
    }.freeze

    def initialize(name, cookies, opts = {})
      @name = name
      @cookies = cookies
      @data = read
      @options = DEFAULT_OPTIONS.merge(opts)
    end

    def [](key)
      data[key.to_s]
    end

    def []=(key, value)
      raise ArgumentError, "Invalid Key: #{key} is not in #{KEYS}" unless valid_key?(key)
      data[key.to_s] = value
      write
    end

    def delete(key)
      data.delete(key.to_s)
      write
    end

    private

    def read
      content = extract_value(cookies[name])
      decode(content) || {}
    end

    def write
      @data = data.select { |k, _| valid_key?(k.to_s) }
      cookies[name] = options.merge(value: sign(data))
    end

    def extract_value(content)
      content.is_a?(Hash) ? content[:value] : content
    end

    def decode(value)
      return false unless value

      value = escaped?(value) ? CGI.unescape(value) : unsign(CGI.unescape(value))

      value && JSON.parse(value)
    end

    def escaped?(value)
      value && !!value.match(/\A%7B.*%7D\z/)
    end

    def sign(data)
      ActiveSupport::MessageVerifier.new(COOKIE_SECRETS[0]).generate(data.to_json)
    end

    def unsign(message)
      value = nil
      COOKIE_SECRETS.each do |key|
        value = decrypt(message, key)
        break if value
      end
      value
    end

    def decrypt(message, key)
      ActiveSupport::MessageVerifier.new(key).verify(message)
    rescue ActiveSupport::MessageVerifier::InvalidSignature, ArgumentError
      nil
    end

    def valid_key?(key)
      KEYS.include?(key.to_s)
    end
  end
end
