module Zendesk::PermanentCookie
  ENV_COOKIE_KEY  = 'zendesk.cookie'.freeze
  COOKIE_KEY      = '_zendesk_cookie'.freeze

  module RequestMixin
    def permanent_cookies
      cookies = env['action_dispatch.cookies'] || self.cookies
      options = { secure: !!env['zendesk.account'].try(:ssl_should_be_used?) }

      env[ENV_COOKIE_KEY] ||= Store.new(COOKIE_KEY, cookies, options)
    end
  end

  module ControllerMixin
    def self.included(base)
      base.helper_method :permanent_cookies
    end

    def permanent_cookies
      request.permanent_cookies
    end
  end
end

if defined?(Rack::Request)
  Rack::Request.send(:include, Zendesk::PermanentCookie::RequestMixin)
end

if defined?(ActionController::Base)
  ActionController::Base.send(:include, Zendesk::PermanentCookie::ControllerMixin)
end

if defined?(ActionController::TestRequest)
  ActionController::TestRequest.send(:include, Zendesk::PermanentCookie::RequestMixin)
end

if defined?(ActionDispatch::Request)
  ActionDispatch::Request.send(:include, Zendesk::PermanentCookie::RequestMixin)
end
