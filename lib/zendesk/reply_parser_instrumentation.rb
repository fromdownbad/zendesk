require 'zendesk_reply_parser'
require 'nokogumbo'
require 'nokogiri'

module Zendesk
  class ReplyParserInstrumentation
    attr_reader :mail, :client, :locale_id, :delimiter

    def initialize(mail, client, locale_id, delimiter, compare_html: false)
      @mail = mail
      @client = client
      @locale_id = locale_id
      @delimiter = Zendesk::MtcMpqMigration::Content::Delimiter.new(delimiter)
      @compare_html = compare_html
    end

    def instrument
      run_no_delimiter_plain
      run_no_delimiter_html
      run_reply_parser_plain
      run_reply_parser_html
    rescue ArgumentError => e
      mail.logger.info("Unable to instrument email: #{mail.message_id}\n#{e}")
    end

    def run_no_delimiter_plain
      @no_delimiter_parsed_plain = content_no_delimiter.text(reply_only: true)

      instrument_match(@no_delimiter_parsed_plain, "no_delimiter", "plain")
    end

    def run_no_delimiter_html
      return unless compare_html?
      @no_delimiter_parsed_html = content_no_delimiter.html(reply_only: true)

      instrument_match(@no_delimiter_parsed_html, "no_delimiter", "html")
    end

    def run_reply_parser_plain
      return unless plain_content.present?

      @reply_parser_parsed_plain = nil
      realtime = Benchmark.realtime do
        @reply_parser_parsed_plain = ZendeskReplyParser::Parser.parse_plain(plain_content.dup)
      end

      instrument_match(@reply_parser_parsed_plain, "reply_parser", "plain")
      instrument_timing(realtime, "reply_parser", "plain")
    end

    def run_reply_parser_html
      return unless compare_html? && html_content.present?

      @reply_parser_parsed_html = nil
      realtime = Benchmark.realtime do
        @reply_parser_parsed_html = if suspected_forward?
          ZendeskReplyParser::Parser.parse_html(html_content.dup, mark_quoted_html: true)
        else
          ZendeskReplyParser::Parser.parse_html(html_content.dup)
        end
      end

      instrument_match(@reply_parser_parsed_html, "reply_parser", "html")
      instrument_timing(realtime, "reply_parser", "html")
    end

    private

    def instrument_match(content, parser_name, format)
      return unless content.present?

      @tags = generate_tags(format)
      @parser_name = parser_name
      @format = format

      record_delimiter_presence_in_content(content)

      if should_compare_text_content_matches?
        compare_text_content_matches
      else
        statsd_client.increment("#{@parser_name}.invalid_comparison", tags: @tags)
      end
    end

    def instrument_timing(realtime, parser_name, format)
      return unless realtime.present?
      tags = generate_tags(format)

      statsd_client.histogram("#{parser_name}.timing", realtime * 1000, tags: tags)
    end

    def content_no_delimiter
      # truncate reply content using only regex patterns in Content class (no delimiter)
      @content_no_delimiter ||= begin
        content_no_delimiter = Zendesk::MtcMpqMigration::Content.new(mail)
        content_no_delimiter
      end
    end

    def compare_html?
      @compare_html
    end

    def html_content
      @html_content ||= begin
        content = Zendesk::MtcMpqMigration::Content.new(mail)
        content.html(reply_only: false)
      end
    end

    def plain_content
      @plain_content ||= begin
        content = Zendesk::MtcMpqMigration::Content.new(mail)
        content.text(reply_only: false)
      end
    end

    def delimiter_found?(content)
      if mail.from_zendesk?
        delimiter.occurrences(content) > 1
      else
        !delimiter.index(content).nil?
      end
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["inbound_mail", "content"])
    end

    def client_tag
      @client_tag ||= client.to_s.gsub(/.*::/, "")
    end

    def generate_tags(format)
      @generated_tags ||= Hash.new do |h, key|
        h[key] = ["client:#{client_tag}", "account_locale:#{locale_id}", "format:#{key}"]
      end
      @generated_tags[format]
    end

    def should_compare_text_content_matches?
      if @format == 'html'
        @reply_parser_parsed_html.present? && @no_delimiter_parsed_html.present?
      else
        @reply_parser_parsed_plain.present? && @no_delimiter_parsed_plain.present?
      end
    end

    def compare_text_content_matches
      content_matches = if @format == 'html'
        Nokogiri::HTML(@reply_parser_parsed_html).text.strip == Nokogiri::HTML(@no_delimiter_parsed_html).text.strip
      else
        @reply_parser_parsed_plain&.strip == @no_delimiter_parsed_plain&.strip
      end
      record_compare_text_content_matches(content_matches)
    end

    def record_compare_text_content_matches(content_matches)
      if content_matches
        statsd_client.increment("no_delimiter.text_comparison.match", tags: @tags)
      else
        statsd_client.increment("no_delimiter.text_comparison.mismatch", tags: @tags)
        mail.logger.info("Content mismatch when comparing #{@format} content from #{mail.client.basename} with #{@parser_name}")
      end
    end

    def record_delimiter_presence_in_content(content)
      if delimiter_found?(content)
        statsd_client.increment("#{@parser_name}.comparison.mismatch", tags: @tags)
        mail.logger.info("Delimiter found when comparing #{@format} content from #{mail.client.basename} with #{@parser_name}")
      else
        statsd_client.increment("#{@parser_name}.comparison.match", tags: @tags)
      end
    end

    def suspected_forward?
      @suspected_forward ||= mail.suspected_forward?
    end
  end
end
