module Zendesk
  module MobileSdk
    module Monitoring
      ###########################
      # IMPORTANT!
      ###########################
      # This module has been duplicated in mobile_sdk_api/lib/mobile/sdk/api/monitoring.rb
      # If you update this file, YOU MUST UPDATE MOBILE_SDK_API as well
      # so that both services are consistent in what they are sending.
      # Once all the SDK APIs have been extracted from Classic we will delete this code.
      ###########################
      def self.included(base)
        base.after_action :notify_statsd
      end

      protected

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [])
      end

      def notify_statsd
        statsd_client.increment(base_metric_name, tags: request_tags)
      end

      def instrument_action
        statsd_client.time("#{base_metric_name}.time", tags: request_tags) do
          yield
        end
      end

      def user_agent
        @user_agent ||= Zendesk::MobileSdk::UserAgent.new(request.user_agent)
      end

      # format: api.mobile.controller_name.action_name
      def base_metric_name
        controller_path.gsub(/\//, '.') + ".#{action_name}"
      end

      def request_tags
        [
          "action:#{action_name}",
          "subdomain:#{current_account.subdomain}",
          "status:#{status}",
          "mobile_app:#{mobile_app_tag}",
          "mobile_app_version:#{mobile_app_version}",
          "mobile_app_cust_version:#{mobile_app_cust_version}",
          "mobile_app_os:#{mobile_app_os}",
          "mobile_app_os_version:#{mobile_app_os_version}",
          "mobile_app_env:#{mobile_app_env}",
          "mobile_app_unity_version:#{mobile_app_unity_version}",
          "mobile_app_variant:#{mobile_app_variant}"
        ]
      end

      def mobile_app_tag
        user_agent.name || "none"
      end

      def mobile_app_version
        user_agent.sdk_version || "none"
      end

      def mobile_app_cust_version
        user_agent.cust_sdk_version || "none"
      end

      def mobile_app_os
        user_agent.os || "none"
      end

      def mobile_app_os_version
        user_agent.os_version || "none"
      end

      def mobile_app_env
        user_agent.env || "none"
      end

      def mobile_app_unity_version
        user_agent.unity_version || "none"
      end

      def mobile_app_variant
        user_agent.variant || "none"
      end
    end
  end
end
