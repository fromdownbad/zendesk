module Zendesk
  module MobileSdk
    # Used from a users controller to stage users. Takes a current_user and params as
    # argument, e.g. user = Zendesk::Users::Initializer.new(current_user, params).user
    # The returned user instance is ready to save.
    class UserInitializer < Zendesk::Users::Initializer
      def apply(user)
        @user = user
        user.current_user = current_user

        user_fields = params.dig(:user, :user_fields)

        if user_fields
          sanitize_custom_fields
          user.custom_field_values.update_from_hash(user_fields)
        end

        user
      end

      def sanitize_custom_fields
        unless current_user.is_system_user?
          params[:user][:user_fields].delete_if do |k|
            current_account.system_user_custom_field_keys.include?(k)
          end
        end
      end
    end
  end
end
