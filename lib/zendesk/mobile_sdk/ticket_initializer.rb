module Zendesk
  module MobileSdk
    class TicketInitializer < Zendesk::Tickets::V2::Initializer
      def apply_changes_from_params
        @via_id = ViaType.MOBILE_SDK
        super
        if @request_params.access_token_has_sdk_scope?
          @ticket.audit.untrusted!
          @ticket.build_request_token if @ticket.new_record?
        end
      end

      protected

      def find_ticket_by_id(identifier, options)
        if identifier.to_i.to_s == identifier.to_s
          account.tickets.find_by_nice_id!(identifier.to_i, options)
        else
          account.tickets.find_by_id!(
            account.request_tokens.find_by_value!(identifier).source_id,
            options
          )
        end
      end
    end
  end
end
