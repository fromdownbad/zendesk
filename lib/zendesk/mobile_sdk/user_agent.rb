module Zendesk
  module MobileSdk
  ###########################
  # IMPORTANT!
  ###########################
  # This class has been dupicated in mobile_sdk_api/lib/mobile/sdk/api/user_agent.rb
  # If you update this file, YOU MUST UPDATE MOBILE_SDK_API as well
  # so that both services are consistent in what they are sending.
  # Once all the SDK APIs have been extracted from Classic we will delete this code.
  ###########################
    class UserAgent
      # These constants are defined in zendesk_core gem
      SDK_REGEXP_V1 = /\A#{Zendesk::Accounts::Source::SDK}/o
      SDK_REGEXP_V2 = /\A#{Zendesk::Accounts::Source::SDK_V2}/o
      SDK_REGEXP_V3 = /\A#{Zendesk::Accounts::Source::SDK_V3}/o
      SDK_REGEXP_V4 = /\A#{Zendesk::Accounts::Source::SDK_V4}/o
      SDK_REGEXP_V5 = /\A#{Zendesk::Accounts::Source::SDK_V5}/o
      SDK_REGEXP_V6 = /\A#{Zendesk::Accounts::Source::SDK_V6}/o
      SDK_REGEXP_V7 = /\A#{Zendesk::Accounts::Source::SDK_V7}/o

      attr_reader :name, :sdk_version, :cust_sdk_version, :os, :os_version, :env, :unity_version, :variant

      def initialize(user_agent_string)
        @user_agent_string = user_agent_string
        @name = nil
        @sdk_version = nil
        @cust_sdk_version = nil
        @os = nil
        @os_version = nil
        @env = nil
        @unity_version = nil
        @variant = nil
        @valid = false

        parse(user_agent_string)
      end

      def valid?
        @valid
      end

      def invalid?
        !@valid
      end

      private

      def parse(user_agent_string)
        if looks_like?(user_agent_string, SDK_REGEXP_V7)
          parse_v7(user_agent_string)
        elsif looks_like?(user_agent_string, SDK_REGEXP_V6)
          parse_v6(user_agent_string)
        elsif looks_like?(user_agent_string, SDK_REGEXP_V5)
          parse_v5(user_agent_string)
        elsif looks_like?(user_agent_string, SDK_REGEXP_V4)
          parse_v4(user_agent_string)
        elsif looks_like?(user_agent_string, SDK_REGEXP_V3)
          parse_v3(user_agent_string)
        elsif looks_like?(user_agent_string, SDK_REGEXP_V2)
          parse_v2(user_agent_string)
        elsif looks_like?(user_agent_string, SDK_REGEXP_V1)
          parse_v1(user_agent_string)
        end
      end

      def looks_like?(user_agent_string, regexp)
        user_agent_string =~ regexp
      end

      def parse_v1(user_agent_string)
        matches = SDK_REGEXP_V1.match(user_agent_string)

        @os = matches[1].downcase
        @name = "zendesk_sdk_for_#{@os}"
        @valid = true
      end

      def parse_v2(user_agent_string)
        matches = SDK_REGEXP_V2.match(user_agent_string)

        @sdk_version = matches[2]
        @os = matches[1].downcase
        @name = "zendesk_sdk_for_#{@os}"
        @valid = true
      end

      def parse_v3(user_agent_string)
        matches = SDK_REGEXP_V3.match(user_agent_string)

        @sdk_version = matches[1]
        @cust_sdk_version = matches[2]
        @os = matches[3].downcase
        @os_version = matches[4]
        @name = "zendesk_sdk_for_#{@os}"
        @valid = true
      end

      def parse_v4(user_agent_string)
        matches = SDK_REGEXP_V4.match(user_agent_string)

        @sdk_version = matches[1]
        @cust_sdk_version = matches[2]
        @os = matches[3].downcase
        @os_version = matches[4]
        @env = matches[5].downcase
        @name = "zendesk_sdk_for_#{@os}"
        @valid = true
      end

      def parse_v5(user_agent_string)
        matches = SDK_REGEXP_V5.match(user_agent_string)

        @sdk_version = matches[1]
        @cust_sdk_version = matches[2]
        @os = matches[3].downcase
        @os_version = matches[4]
        @env = matches[5].downcase
        @name = "zendesk_sdk_for_#{@os}"
        @unity_version = matches[6].downcase
        @valid = true
      end

      def parse_v6(user_agent_string)
        matches = SDK_REGEXP_V6.match(user_agent_string)

        @sdk_version = matches[1]
        @cust_sdk_version = matches[2]
        @os = matches[3].downcase
        @os_version = matches[4]
        @env = matches[5].downcase
        @name = "zendesk_sdk_for_#{@os}"
        @variant = matches[6].downcase
        @valid = true
      end

      def parse_v7(user_agent_string)
        matches = SDK_REGEXP_V7.match(user_agent_string)

        @sdk_version = matches[1]
        @cust_sdk_version = matches[2]
        @os = matches[3].downcase
        @os_version = matches[4]
        @name = "zendesk_sdk_for_#{@os}"
        @variant = matches[5].downcase
        @valid = true
      end
    end
  end
end
