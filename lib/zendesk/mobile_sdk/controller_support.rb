require 'active_support/concern'

module Zendesk
  module MobileSdk
    module ControllerSupport
      extend ActiveSupport::Concern

      included do
        extend ClassMethods
      end

      class_methods do
        def log_or_throttle_sdk_action_by_account(actions, throttle)
          actions.each do |action|
            key = "mobile_sdk_#{controller_name}_#{action}"
            emergency_arturo = "has_#{key}_emergency_limit?".to_sym
            throttle_arturo = "has_enable_throttle_for_#{key}?"

            before_action only: action do
              threshold_reached = Prop.throttle(throttle, [current_account.id, key])

              if threshold_reached
                if arturo_enabled?(throttle_arturo)
                  throttle_action_with_emergency_fallback(emergency_arturo, throttle, [current_account.id, key])
                  increment_metrics("#{controller_name}.#{action}.log_and_throttle", false, action)
                else
                  increment_metrics("#{controller_name}.#{action}.log_without_throttle", true, action)
                end
              else
                increment_metrics("#{controller_name}.#{action}.throttle_not_reached", true, action)
              end
            end
          end
        end
      end

      protected

      def anonymous_authenticated?
        request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN].scopes.include?("sdk")
      end

      def current_mobile_sdk_app
        @mobile_sdk_auth ||= current_account.mobile_sdk_auths.find_by_client_id(oauth_access_token.client.try(:id))

        # Previously this query was @mobile_sdk_auth.mobile_sdk_app.
        # We have updated to respect that query but additionally scope by account_id for better reliability.
        # Please refer to https://zendesk.atlassian.net/browse/MSS-708
        apps ||= current_account.mobile_sdk_apps.where(mobile_sdk_auth_id: @mobile_sdk_auth.id).limit(1)
        @mobile_sdk_app ||= apps.first
      end

      def allow_only_mobile_sdk_clients_access
        return true if current_account.has_mobile_sdk_old_support?
        oauth_client_id = params[:client_id] || request.headers["HTTP_CLIENT_IDENTIFIER"]
        client_id = current_account.clients.where(identifier: oauth_client_id).pluck(:id).first
        unless client_id && current_account.mobile_sdk_auths.find_by_client_id(client_id)
          Rails.logger.warn("Request blocked - only mobile SDK clients are allowed for #{controller_name}:#{action_name} #{request.url}")
          head 450
        end
      end

      def allow_only_mobile_sdk_tokens_access
        unless current_account.mobile_sdk_auths.find_by_client_id(oauth_access_token.client.try(:id))
          Rails.logger.warn("Request blocked - only mobile SDK tokens are allowed for #{controller_name}:#{action_name} #{request.url}")
          head 450
        end
      end

      private

      def throttle_action_with_emergency_fallback(emergency_arturo, prop_name, key)
        return unless Zendesk::Accounts::Source.zendesk_mobile_sdk_user_agent?(request.user_agent)

        if arturo_enabled?(emergency_arturo)
          Prop.throttle!(:mobile_sdk_emergency_limit, key)
        else
          Prop.throttle!(prop_name, key)
        end
      end

      def arturo_enabled?(arturo)
        current_account.respond_to?(arturo) && current_account.send(arturo)
      end

      def sdk_statsd_client
        @statsd_client = Zendesk::StatsD::Client.new(
          namespace: "mobile.sdk"
        )
      end

      def throttle_tags_with_account(log_only, action)
        [
          "log_only: #{log_only}",
          "account: #{current_account}",
          "controller_name: #{controller_name}",
          "action: #{action}"
        ]
      end

      def throttle_tags_without_account(log_only, action)
        [
          "log_only: #{log_only}",
          "controller_name: #{controller_name}",
          "action: #{action}"
        ]
      end

      def increment_metrics(key, log_only, action)
        if log_only
          sdk_statsd_client.increment(key, tags: throttle_tags_without_account(log_only, action))
        else
          sdk_statsd_client.increment(key, tags: throttle_tags_with_account(log_only, action))
        end
      end
    end
  end
end
