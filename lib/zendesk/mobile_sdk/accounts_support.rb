module Zendesk
  module MobileSdk
    module AccountsSupport
      protected

      def account_cannot_hide_closed_requests?(account)
        !account_can_hide_closed_requests?(account)
      end

      def account_can_hide_closed_requests?(account)
        account.subscription.plan_type > ZBC::Zendesk::PlanType::Essential.plan_type
      end

      def account_can_hide_referrer_logo?(account)
        enterprise_account?(account)
      end

      def account_cannot_hide_referrer_logo?(account)
        !account_can_hide_referrer_logo?(account)
      end

      def enterprise_account?(account)
        account.subscription.plan_type == ZBC::Zendesk::PlanType::Enterprise.plan_type
      end
    end
  end
end
