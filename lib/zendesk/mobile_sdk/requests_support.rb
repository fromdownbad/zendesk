module Zendesk
  module MobileSdk
    module RequestsSupport
      protected

      def ticket_scope(token)
        identifier = ticket_identifier(token)
        if identifier.is_a?(Numeric)
          current_user.tickets.find_by_nice_id!(identifier)
        else
          tickets_scope([identifier]).first
        end
      end

      # This is used by the show_many action that only uses tokens as identifiers
      def tickets_scope(tokens)
        tokens_crc = tokens.map { |t| Zlib.crc32(t) }
        # This forces the query to use the 'index_tokens_on_account_id_and_token_crc' key
        current_user.tickets.joins(:request_token).where(
          "tokens.value IN (?) AND tokens.token_crc IN (?) AND tokens.account_id = ?",
          tokens, tokens_crc, current_account.id
        )
      end

      def find_ticket
        identifier = ticket_identifier(params[:id] || params[:ticket_id])
        ticket = ticket_scope(identifier)

        unless ticket
          request_token = RequestToken.where(account_id: current_account.id, value: identifier).first
          # Remove orphan tokens, tokens not associated to any tickets.
          if request_token && !request_token.ticket.present?
            request_token.destroy
            Rails.logger.info("Purging mobile SDK request token: #{request_token.id}. No associated ticket found.")
          end
          raise ActiveRecord::RecordNotFound
        end

        ticket
      end

      private

      def ticket_identifier(identifier)
        if identifier.to_i.to_s == identifier.to_s
          identifier.to_i
        else
          identifier
        end
      end
    end
  end
end
