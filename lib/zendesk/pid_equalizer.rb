module Zendesk
  class PIDEqualizer
    def initialize(p_arturo:, i_arturo:, d_arturo:)
      @p_feature = Arturo::Feature.find_feature(p_arturo)
      @i_feature = Arturo::Feature.find_feature(i_arturo)
      @d_feature = Arturo::Feature.find_feature(d_arturo)
    end

    def p_gain
      gain(@p_feature)
    end

    def i_gain
      gain(@i_feature)
    end

    def d_gain
      gain(@d_feature)
    end

    private

    def gain(feature)
      return 0.0 unless feature
      case feature[:phase]
      when "rollout"
        feature[:deployment_percentage].to_f / 100
      when "on"
        1.0
      else
        0.0
      end
    end
  end
end
