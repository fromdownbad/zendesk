module Zendesk
  module PreventCollectionModificationMixin
    DEFAULT_ALLOWED_CHANGES = ['position'].freeze

    attr_writer :overwrite_resource_permission

    def self.included(base)
      base.before_destroy(:prevent_destroy_collection_resource)
      base.validate(:prevent_modify_collection_resource)
    end

    def is_apps_requirement? # rubocop:disable Naming/PredicateName
      !!resource_collection_resource
    end

    private

    # can be defined in model to allow changes to specific attributes
    def collection_allowed_changes
      []
    end

    def only_allowed_changes?
      allowed = DEFAULT_ALLOWED_CHANGES + collection_allowed_changes
      changed.to_set.subset?(allowed.to_set)
    end

    def can_destroy_collection?
      @overwrite_resource_permission || !is_apps_requirement?
    end

    def can_modify_collection?
      can_destroy_collection? || only_allowed_changes?
    end

    def prevent_destroy_collection_resource
      unless can_destroy_collection?
        errors.add(:base, ::I18n.t("txt.admin.models.apps_requirements.validation.cannot_modify_requirement"))
        false
      end
    end

    def prevent_modify_collection_resource
      if changed? && !can_modify_collection?
        errors.add(:base, ::I18n.t("txt.admin.models.apps_requirements.validation.cannot_modify_requirement"))
        false
      end
    end
  end
end
