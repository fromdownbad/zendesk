module Zendesk::SideConversations
  class InternalApiClient
    def initialize(account:)
      @account = account
    end

    def search_side_conversations(query, options)
      from = options[:per_page] * (options[:page] - 1)
      url = "/accounts/#{@account.id}/search_threads?entity_prefix=zen:ticket&size=#{options[:per_page]}&from=#{from}&q=#{CGI.escape(query)}"

      resp = connection.get(url)
      resp.body
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: "Could not search Side Conversations for query #{query}", fingerprint: 'bcd09d09afc720423a98dac41d1196ff')
      {}
    end

    private

    def connection
      @connection ||= KragleConnection.build_for_sc_search_service
    end
  end
end
