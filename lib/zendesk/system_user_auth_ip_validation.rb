module Zendesk
  module SystemUserAuthIPValidation
    def self.included(base)
      base.class_eval do
        before_action :validate_system_user_ip
      end
    end

    private

    def validate_system_user_ip
      return if Rails.env.development?
      return unless Arturo.feature_enabled_for?(:sys_auth_sec_inc_sep_2018_enable_logging, current_account)
      return unless current_user && current_user.is_system_user?

      Rails.logger.warn(system_user_auth_log_message)
      record_event_in_statsd unless system_user_ip_whitelisted?

      if request_is_blocked
        render plain: 'Request must originate from an authorized source', status: :forbidden
      end
    end

    def system_user_auth_log_message
      @system_user_auth_log_message ||= begin
        trace_id = ZendeskAPM.enabled? ? Datadog.tracer.active_span.try(:trace_id) : 'tracing-disabled'
        key_id = request.headers['Authorization'].to_s[/id="(.*?)"/, 1].to_s[0...5] # log first 5 characters of key_id for id purposes

        %W[
          [SYS-AUTH]
          service:'classic'
          request_id:'#{request.env['request_id']}'
          trace_id:'#{trace_id}'
          key_id:'#{key_id}'
          key_name:'#{current_user.subsystem_name}'
          request_url:'#{request.url}'
          request_method:'#{request.method}'
          x_forwarded_for:'#{request.headers['HTTP_X_FORWARDED_FOR']}'
          remote_addr:'#{request.headers['REMOTE_ADDR']}'
          cf_connecting_ip:'#{request.headers['HTTP_CF_CONNECTING_IP']}'
          cf_ipcountry:'#{request.headers['HTTP_CF_IPCOUNTRY']}'
          cf_ray:'#{request.headers['HTTP_CF_RAY']}'
          should_block:'#{!system_user_ip_whitelisted?}'
          did_block:'#{request_is_blocked}'
        ].join(' ')
      end
    end

    def system_user_ip_whitelisted?
      Zendesk::Net::IPTools.whitelist_handles_ip?(
        Zendesk::Net::IPTools.trusted_ip(request.env),
        Array(Arturo::Feature.find_feature(:sys_auth_sec_inc_sep_2018_whitelist).try(:external_beta_subdomains))
      )
    end

    def request_is_blocked
      Arturo.feature_enabled_for?(
        :sys_auth_sec_inc_sep_2018_enable_blocking,
        current_account
      ) && !system_user_ip_whitelisted?
    end

    def record_event_in_statsd
      tags = %W[
        service:classic
        system_user_key_name:#{current_user.subsystem_name}
        request_method:#{request.method}
        should_block:#{!system_user_ip_whitelisted?}
        did_block:#{request_is_blocked}
      ]

      Rails.application.config.statsd.client.event(
        'System User Auth attempt',
        system_user_auth_log_message,
        tags: tags
      )
    end
  end
end
