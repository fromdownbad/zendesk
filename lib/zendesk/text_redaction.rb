module Zendesk
  module TextRedaction
    class << self
      def redact(value, text)
        redact!(value.to_s.dup, text)
      end

      def redact!(value, text)
        value.
          encode!(Encoding::UTF_8, invalid: :replace, replace: "▇").
          gsub!(/#{Regexp.escape text}/i, text.gsub(/[^ ]/, "▇"))
      end

      def redact_part_or_all!(value, text, redact_full_comment)
        (text.nil? && redact_full_comment) ? "▇▇▇▇▇" : redact!(value, text)
      end
    end
  end
end
