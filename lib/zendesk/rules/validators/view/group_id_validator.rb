module Zendesk
  module Rules
    module Validators
      module View
        class GroupIdValidator < ActiveModel::Validator
          attr_reader :record
          delegate :group_owner_ids, :account, to: :record

          def validate(record)
            @record = record

            return unless group_view? &&
                          account.has_multiple_group_views_writes?

            check_for_invalid_group_ids
          end

          private

          def group_view?
            record.owner_type == 'Group'
          end

          def check_for_invalid_group_ids
            return unless group_owner_ids.present?

            invalid_ids = group_owner_ids - account.group_ids

            return if invalid_ids.empty?

            record.errors.add(
              :base,
              ::I18n.t(
                'txt.admin.models.rules.rule.invalid_group_ids',
                group_ids: invalid_ids.join(', ')
              )
            )
          end
        end
      end
    end
  end
end
