module Zendesk
  module Rules
    module Validators
      module View
        class DescriptionWordValidator < ActiveModel::Validator
          MAX_DESCRIPTION_WORD_CONDITIONS = 2
          DESCRIPTION_WORD_SOURCE = 'description_includes_word'.freeze

          attr_reader :record
          delegate :definition, :account, to: :record

          def validate(record)
            @record = record
            return unless account.has_views_strict_description_count_validation?
            return unless too_many_description_word_conditions?

            record.errors.add(:base, error_message)
          end

          private

          ##
          # All operators on `description_includes_word` are heavy.
          # They generate queries with `LIKE "%word%"`
          #
          def too_many_description_word_conditions?
            description_conditions_count > MAX_DESCRIPTION_WORD_CONDITIONS
          end

          def description_conditions_count
            conditions.count { |cond| cond.source == DESCRIPTION_WORD_SOURCE }
          end

          def conditions
            [
              definition.conditions_all,
              definition.conditions_any
            ].map(&:to_a).flatten.compact
          end

          def error_message
            'No more than two <strong>Description</strong> word conditions ' \
              'are allowed'.html_safe
          end
        end
      end
    end
  end
end
