require 'zendesk_rules/query'
require 'timecop'

module Zendesk
  module Rules
    class RuleQueryBuilder
      class UnsupportedRuleDefinition < StandardError; end

      AUTOMATION_STATUS_RANGE = (
        StatusType.fields - Zendesk::Types::StatusType::INOPERABLE_LIST
      ).freeze

      VIEW_STATUS_RANGE = AUTOMATION_STATUS_RANGE + [StatusType.CLOSED]

      attr_accessor :rule, :user, :account
      def initialize(
        account,
        user,
        rule,
        execution_options:         nil,
        skip_default_restrictions: false,
        context:                   nil
      )
        @account = account
        @user = user
        @rule = rule
        @skip_default_restrictions = skip_default_restrictions
        @execution_options = execution_options ||
                             ExecutionOptions.new(@rule, @account, {})
        @context = context
      end

      def self.query_for(rule, user, execution_options = nil, context = nil)
        user ||= rule.account.anonymous_user
        account = rule.account || user.account

        new(
          account,
          user,
          rule,
          execution_options: execution_options,
          context: context
        ).query
      end

      # This method is here so timecop isn't eager loaded with
      # Zendesk::Rules::Query
      def self.compiled_query_with_frozen_time_for(
        rule,
        user,
        execution_options = nil,
        context = nil
      )
        Timecop.freeze(Time.new(1970)) do
          query_for(rule, user, execution_options, context).compile(:sql)
        end
      end

      def query
        @query ||= build_query
      end

      def build_query
        q = ZendeskRules::Query.new('tickets', context)

        add_defaults(q) unless skip_default_restrictions?

        @rule.definition.optimized_conditions_all.each do |item|
          q.all_condition(item.source, item.operator, item.value)
        end

        @rule.definition.optimized_conditions_any.each do |item|
          q.any_condition(item.source, item.operator, item.value)
        end

        @execution_options.ordering_array.each do |field, direction|
          q.ordering(field, direction)
        end

        q
      end

      delegate :joins, to: :@query

      private

      # rubocop:disable Naming/MethodParameterName
      def add_defaults(q)
        q.all_condition('account_id', 'is', [@account.id, -1000])

        if @rule.is_a?(Automation)
          q.all_condition('status_id', 'is', AUTOMATION_STATUS_RANGE)
        else
          q.all_condition('status_id', 'is', VIEW_STATUS_RANGE)
        end

        q.all_condition('agent_restrictions', 'is', '')
      end
      # rubocop:enable Naming/MethodParameterName

      def skip_default_restrictions?
        @skip_default_restrictions
      end

      def context
        @context ||= Zendesk::Rules::Context.new(@account, @user, rule_type: :view, condition_type: :all, component_type: :condition, executing: true)
      end
    end
  end
end
