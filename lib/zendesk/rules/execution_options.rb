require 'occam_client'

module Zendesk
  module Rules
    module OccamParams
      def occam_query_params
        {
          order: ordering_array.map { |o| o.join(' ') },
          limit: limit,
          offset: offset,
          rule: {
            id: rule.id,
            type: rule.type,
            preview_type: preview_type(rule),
            updated_at: rule.updated_at
          },
          options: occam_query_options
        }
      end

      def preview_type(rule)
        return 'none' unless rule.id.nil?

        case rule.title
        when ::View::INCOMING_TITLE
          ::View::INCOMING
        when ::View::MY_GROUPS_TITLE
          ::View::MY_GROUPS
        when ::View::MY_TITLE
          ::View::MY
        else
          'none'
        end
      end

      def occam_query_options
        {
          with_archived: with_archived?,
          since: since,
          find_time: find_time,
          force_key: rule.force_key
        }
      end

      def occam_options
        {
          cache: hanlon_cache?,
          watch: watch?,
          timeout: timeout,
          simulate_slow: simulate_slow,
          hanlon_parity_check: hanlon_parity_check?,
          request_origin: request_origin.merge(caller: params[:caller])
        }
      end
    end

    class ExecutionOptions
      include OccamParams

      ASC  = :asc
      DESC = :desc

      VALIDATE_INT = ->(i) { i.to_s.strip.to_i.to_s == i.to_s.strip }
      VALIDATIONS = {
        output_type: ->(t) { t = t.to_sym if t.is_a?(String); [:list, :table, nil].include?(t) },
        order: ->(o) { o.nil? || o =~ /^\s*\w+\s*$/ },
        page: VALIDATE_INT,
        per_page: VALIDATE_INT,
        limit: VALIDATE_INT
      }.freeze

      def self.boolean_param(*names)
        names.each do |name|
          define_method(name.to_s + '?') do
            !!params[name.to_sym]
          end
        end
      end

      attr_reader :rule, :account, :params

      boolean_param :paginate, :ids_only, :disable_caching, :occam_raw_output

      def initialize(rule, account, params)
        @rule = rule
        @account = account
        @params = params
      end

      def output_type
        @output_type ||= choose_option(:output_type, params[:output_type], rule.output.try(:type), :list).to_sym
      end

      def order
        return nil if rule.is_a?(Automation)

        @order ||= begin
          # order is unselectable for :list output views.  If list, do not honor
          params_order = params[:order] if table?
          choose_option(:order, params_order, rule.output.try(:order), :nice_id)
        end
      end

      def order_direction
        @order_direction ||= begin
          if table? && params[:order]
            params_order_dir = (params[:desc] == '1' ? DESC : ASC)
          end

          if rule.output
            rule_order_dir = (rule.output.order_asc ? ASC : DESC)
          end

          choose_option(
            :order_direction,
            params_order_dir,
            rule_order_dir,
            DESC
          )
        end
      end

      def order_asc?
        order_direction == ASC
      end

      def order_desc?
        order_direction == DESC
      end

      def ordering_array
        orders = [
          [group, group_direction],
          [order, order_direction]
        ]
        orders << ['nice_id', 'desc'] unless orders.last.first.to_s == 'nice_id'

        orders.select { |o| o.first.present? }
      end

      def group
        return nil unless table?
        return nil if params[:order]

        rule.output.try(:group)
      end

      def group_direction
        if group
          rule.output.group_asc ? ASC : DESC
        end
      end

      def since
        Time.parse(params[:updated_at] + 'Z') if params[:updated_at]
      end

      def page
        choose_int_option(:page, params[:page], 1) if paginate?
      end

      def per_page
        choose_int_option(:per_page, params[:per_page], rule.per_page, 15) if paginate?
      end

      def limit
        if paginate?
          per_page
        else
          choose_int_option(:limit, params[:limit], 50)
        end
      end

      def offset
        if paginate?
          # we never will_paginate run the query, instead we do offset
          # calculations ourselves
          (page - 1) * per_page
        end
      end

      def api?
        ['v1', 'v2', 'preview'].include?(params[:caller]) && !params[:lotus]
      end

      def includes
        if table?
          fields = rule.output.rule_fields
          fields += [:nice_id, :group, :status, :type].
            map { |f| ZendeskRules::RuleField.lookup(f) }

          fields.uniq!
        else
          fields = [:subject, :status, :requester, :score].
            map { |f| ZendeskRules::RuleField.lookup(f) }
        end

        fields.compact!

        associations = fields.map(&:association).compact.uniq
        add_additional_user_includes(associations)
      end

      def hanlon_cache?
        if !params[:hanlon_cache].nil? # only accept true or false here
          !!params[:hanlon_cache]
        else
          account.has_hanlon_cache?
        end
      end

      # Should add a rule-watch watch along with the count?
      def watch?
        if params.key?(:watch)
          params[:key]
        else
          account.has_hanlon_watch_count?
        end
      end

      def hanlon_parity_check?
        account.has_hanlon_parity_check?
      end

      def find_time
        params[:find_time]
      end

      def with_archived?
        rule.can_find_tickets_on_archive?
      end

      def timeout
        params[:timeout]
      end

      def simulate_slow
        params[:simulate_slow]
      end

      def request_origin
        origin = rule&.request_origin || params[:request_origin]
        return {} unless origin

        origin.to_h.
          slice(:lotus_version, :referer, :request_source, :app_id).
          compact
      end

      private

      def add_additional_user_includes(associations)
        associations.each_with_object({}) do |assoc, options|
          case assoc
          when :assignee, :submitter, :requester
            options.update(assoc => {photo: {}, identities: {}, taggings: {}, groups: {}})

          else
            options.update(assoc => {})
          end
        end
      end

      def table?
        output_type == :table
      end

      def choose_option(opt_name, *inputs)
        inputs.each do |i|
          next unless i
          if !VALIDATIONS.key?(opt_name) || VALIDATIONS[opt_name].call(i)
            return i
          else
            Rails.logger.info("warning: skipped invalid option '#{i}' for :#{opt_name}")

          end
        end
      end

      def choose_int_option(opt_name, *inputs)
        choose_option(opt_name, *inputs).to_i
      end
    end
  end
end
