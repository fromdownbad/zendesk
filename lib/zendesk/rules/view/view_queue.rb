require_relative './collision_cache'

# A modified version of ViewQueue with changes to logic that fetches tickets
# from occam.
module Zendesk
  module Rules
    module View
      class ViewQueue
        attr_reader :user, :client, :view, :play_span, :collision_key

        MAX_TICKETS_PER_FETCH        = 1000
        SKILLS_FILTER_MAX_PAGE_LIMIT = 3
        TICKET_LOCK                  = 15.seconds
        MIN_CHECK_INTERVAL           = 15.seconds
        USER_QUEUE_EXPIRATION        = 60.minutes
        TICKETS_CACHE_EXPIRATION     = 5.minutes
        OCCAM_REQUEST_TIMEOUT        = 30.seconds

        OCCAM_TICKET_VERIFICATION_TIMEOUT = 4.seconds

        PLAY_TICKETS_REFILL       = 'play-tickets-refill'.freeze
        PLAY_TICKETS_VERIFICATION = 'play-tickets-verification'.freeze

        APM_SERVICE_NAME = 'classic-play'.freeze
        PREVIEW = 'preview'.freeze

        class Locked < RuntimeError
        end

        def initialize(view, user, options = {})
          @view = view
          @user = user
          @options = options
          @client = self.class.store

          user_key = "#{view.id}-#{view.updated_at}-#{user.id}"
          @seen_key = "Zendesk-ViewQueue-seen-#{user_key}"
          @skipped_key = "Zendesk-ViewQueue-skipped-#{user_key}"
          @skip_checked_key = "Zendesk-ViewQueue-skip-checked-#{user_key}"
          @cached_queue_key = "api/v2/views/#{view.id}/ticket_ids/#{view.query_cache_key(user, options)}"
          @collision_key = "play/#{user.account.id}/ticket-agent-collision"
          @radar_total_tickets_verified = 0
          @radar_total_users_collided   = 0

          if options[:rewind]
            client.del(@seen_key)
            client.del(@skipped_key)
          end
        end

        def pop
          @play_span = ZendeskAPM.trace('view_queue.pop', service: APM_SERVICE_NAME)
          set_default_tags
          ticket = pop_from_skipped_queue if check_skipped_queue?
          ticket || pop_user_ticket_from_shared_queue
        ensure
          register_final_tags_for_pop
          @play_span.finish
        end

        def self.store
          @store ||= Zendesk::RedisFactory.create_from_consul(
            ENV['OCCAM_REDIS_CLUSTER'],
            password: ENV['OCCAM_REDIS_PASSWORD'],
            ssl: true,
            connect_timeout: ENV['OCCAM_REDIS_TIMEOUT']
          )
        end

        private

        def check_skipped_queue?
          !skip_checked? || @options['debug']
        end

        def pop_from_skipped_queue
          skipped_queue_span = ZendeskAPM.trace('view_queue.pop_from_skipped_queue', service: APM_SERVICE_NAME)
          play_span.set_tag('zendesk.view_queue.skipped_queue.checked', true)
          play_span.set_tag(
            'zendesk.view_queue.skipped_queue.refreshed',
            true
          ) if skipped.present?

          skipped_queue = refresh_tickets(
            skipped,
            key: @skipped_key,
            expiry: USER_QUEUE_EXPIRATION
          )

          # Remove previously collided tickets if any
          skipped_queue -= collisions

          play_span.set_tag(
            'zendesk.view_queue.skipped_queue.counts.after_collision_removal',
            skipped_queue.size
          )

          # Check any previously skipped tickets
          skipped_queue.each do |ticket_id|
            increment_tag('zendesk.view_queue.skipped_queue.counts.iteration')
            # Handle skipped ticket the same way as regular list
            next unless ticket_in_filtered_views?(ticket_id)
            next unless (ticket = ticket_with_lock(ticket_id))

            unskipped!(ticket_id)
            Rails.logger.info "ViewQueue: ticket : #{ticket_id} served from the skipped queue"
            play_span.set_tag('zendesk.view_queue.skipped_queue.served_from_skipped_tickets', true)
            play_span.set_tag('zendesk.view_queue.served_ticket_id', ticket&.id)

            return ticket
          end
          nil
        ensure
          skip_checked
          skipped_queue_span.finish
        end

        # Check the shared cached queue for tickets
        def pop_user_ticket_from_shared_queue
          user_queue_span = ZendeskAPM.trace('view_queue.pop_from_user_queue', service: APM_SERVICE_NAME)
          user_queue = shared_cached_queue - seen

          play_span.set_tag('zendesk.view_queue.user_queue.counts.seen', seen.size)
          play_span.set_tag('zendesk.view_queue.user_queue.counts.cached_tickets', user_queue&.size)
          play_span.set_tag('zendesk.view_queue.user_queue.checked', true)

          if investigation_mode?
            Rails.logger.info "ViewQueue: user_queue cached_tickets (#{user_queue.size}): #{user_queue.first(30)}"
          end

          loop do
            if user.account.has_view_queue_prevent_removing_seen_tickets_from_shared_queue?
              refreshed_user_queue = if user_queue.empty?
                refill_user_queue || []
              else
                refresh_tickets(
                  shared_cached_queue,
                  key: @cached_queue_key, expiry: TICKETS_CACHE_EXPIRATION
                )
              end
              user_queue = refreshed_user_queue - seen
            else
              user_queue = if user_queue.empty?
                refill_user_queue || []
              else
                refresh_tickets(
                  user_queue,
                  key: @cached_queue_key, expiry: TICKETS_CACHE_EXPIRATION
                )
              end
            end

            # Remove previously collided tickets if any
            user_queue -= collisions

            play_span.set_tag(
              'zendesk.view_queue.user_queue.counts.after_collision_removal',
              user_queue.size
            )

            return unless user_queue.present?

            while (ticket_id = user_queue.shift)
              increment_tag('zendesk.view_queue.user_queue.counts.iteration')

              # We still need to add filtered_view check to make sure we
              # support verification for tickets in skills based filter view
              if ticket_in_filtered_views?(ticket_id)
                ticket = ticket_with_lock(ticket_id)

                seen!(ticket_id)

                if ticket.present?
                  play_span.set_tag('zendesk.view_queue.served_ticket_id', ticket&.id)
                  return ticket
                end
              end

              # skip ticket for now, will look at it again next time
              skipped!(ticket_id)
            end
          end
        ensure
          user_queue_span.finish
        end

        def refill_queue_from_occam(page, tickets)
          refill_span = ZendeskAPM.trace('view_queue.refill_queue_from_occam', service: APM_SERVICE_NAME)

          new_tickets = view.find_tickets(user, view_params.merge(page: page, caller: PLAY_TICKETS_REFILL, timeout: OCCAM_REQUEST_TIMEOUT))
          play_span.set_tag('zendesk.view_queue.user_queue.counts.tickets_refilled_from_occam', new_tickets&.size)
          play_span.set_tag('zendesk.view_queue.user_queue.refilled_from_occam', true)

          if apply_filter?
            new_tickets = matching_tickets(new_tickets)
            play_span.set_tag('zendesk.view_queue.user_queue.counts.tickets_after_skills_filter', new_tickets&.size)
          end

          return unless new_tickets.presence

          # Remove previously present ticket ids and duplicates or
          # set it to nil when its empty
          ticket_list = (new_tickets | tickets).presence

          client.pipelined do
            client.del(@cached_queue_key)
            client.rpush(@cached_queue_key, ticket_list)
            client.expire(@cached_queue_key, TICKETS_CACHE_EXPIRATION)
          end

          if investigation_mode?
            Rails.logger.info "ViewQueue: Writing #{new_tickets.size} tickets to cache #{@cached_queue_key}: #{new_tickets.first(30)}"
          end

          new_tickets
        ensure
          refill_span.finish
        end

        # Refill the shared cache queue and replenish the empty user queue
        def refill_user_queue
          user_queue_span = ZendeskAPM.trace('view_queue.refill_user_queue', service: APM_SERVICE_NAME)
          page = 1
          tickets = []
          user_queue = nil

          lock do
            client.del(@cached_queue_key)

            begin
              new_tickets = refill_queue_from_occam(page, tickets)
              increment_tag('zendesk.view_queue.user_queue.counts.refill_attempts')
              # increment the page if it's a skills-based filter to ensure
              # we are sending all tickets (max 3000) to deco for skills-matching
              page += 1
              next if apply_filter? &&
                new_tickets.blank? &&
                page <= SKILLS_FILTER_MAX_PAGE_LIMIT

              return unless new_tickets.present?

              tickets.concat(new_tickets)
              user_queue = tickets - seen
            end while user_queue.blank? # rubocop:disable Lint/Loop
          end

          play_span.set_tag('zendesk.view_queue.user_queue.counts.tickets_after_refill_complete', user_queue&.size)
          user_queue
        ensure
          user_queue_span.finish
        end

        def ticket_with_lock(ticket_id)
          ticket_with_lock_span = ZendeskAPM.trace('view_queue.ticket_with_lock', service: APM_SERVICE_NAME)

          ticket = user.account.tickets.find(ticket_id)

          if viewed_by_other_agent?(ticket)
            increment_tag('zendesk.view_queue.counts.agent_collisions')

            update_collisions(ticket.id)
            return
          end

          if acquire_ticket_lock(ticket_id)
            increment_tag('zendesk.view_queue.counts.successfully_acquired_lock')
          else
            increment_tag('zendesk.view_queue.counts.failed_to_acquire_lock')
            return
          end

          ticket
        rescue ActiveRecord::RecordNotFound
          Rails.logger.info(
            "ViewQueue: Ticket not found: #{ticket_id},"\
            "account: #{user.account.id},"\
            "user: #{user.id}"
          )

          return nil # rubocop:disable Style/RedundantReturn
        ensure
          ticket_with_lock_span.finish
        end

        def skipped
          @skipped = client.lrange(@skipped_key, 0, -1).reject(&:blank?).map(&:to_i)
        end

        def shared_cached_queue
          client.lrange(@cached_queue_key, 0, -1).reject(&:blank?).map(&:to_i)
        end

        def skipped!(ticket_id)
          increment_tag('zendesk.view_queue.counts.skipped')

          client.pipelined do
            client.rpush(@skipped_key, ticket_id)
            client.expire(@skipped_key, USER_QUEUE_EXPIRATION)
          end
        end

        def unskipped!(ticket_id)
          increment_tag('zendesk.view_queue.skipped_queue.counts.unskipped')

          client.pipelined do
            client.lrem(@skipped_key, 1, ticket_id)
            client.expire(@skipped_key, USER_QUEUE_EXPIRATION)
          end
        end

        def skip_checked?
          client.exists(@skip_checked_key)
        end

        def skip_checked
          client.set(@skip_checked_key, true, ex: MIN_CHECK_INTERVAL)
        end

        def seen
          @seen ||= client.lrange(@seen_key, 0, -1).map(&:to_i).uniq
        end

        def seen!(ticket_id)
          if investigation_mode?
            Rails.logger.info "ViewQueue: marking #{ticket_id} as seen"
          end

          seen.push(ticket_id)
          client.pipelined do
            client.rpush(@seen_key, ticket_id)
            client.expire(@seen_key, USER_QUEUE_EXPIRATION)
          end
        end

        def acquire_ticket_lock(ticket_id)
          acquire_lock_span = ZendeskAPM.trace('view_queue.acquire_ticket_lock', service: APM_SERVICE_NAME)

          key = "Zendesk-ViewQueue-#{ticket_id}"
          user_id = user.id

          unless client.set(key, user_id, nx: true, ex: TICKET_LOCK)
            user_id = client.get(key).to_i
            if user_id == user.id
              client.expire(key, TICKET_LOCK)
            end
          end

          user_id == user.id
        ensure
          acquire_lock_span.finish
        end

        ##
        # when an agent views the ticket in browser, it is marked as being viewed
        # This calls Radar to confirm
        #
        def viewed_by_other_agent?(ticket)
          radar_span = ZendeskAPM.trace('zendesk.view_queue.verify_agent_collision', service: APM_SERVICE_NAME)
          users_on_ticket = Zendesk::AgentCollision.ticket_viewed_by_user_ids(ticket)

          @radar_total_tickets_verified += 1
          @radar_total_users_collided   += users_on_ticket.size
          Rails.logger.info "#{users_on_ticket.size} users found on ticket #{ticket.id} : #{users_on_ticket}"

          (users_on_ticket - [user.id]).any?
        rescue StandardError => e
          ZendeskExceptions::Logger.record(e, location: ViewQueue, message: "Radar error", reraise: !Rails.env.production?, fingerprint: 'e1daafe612c86ef177253f23f888c4f473f918b1')
          false # radar is down/something bad happens -> let's continue serving tickets
        ensure
          radar_span.finish
        end

        def lock
          lock = "#{@cached_queue_key}_lock"
          raise ViewQueue::Locked unless client.set(lock, nx: true, ex: 30.seconds)

          begin
            yield
          ensure
            client.del lock
          end
        end

        def matching_tickets(ticket_ids)
          skills_matching_span = ZendeskAPM.trace('view_queue.skills_matching', service: APM_SERVICE_NAME)

          ticket_ids & skill_matcher.matching_tickets(ticket_ids)
        ensure
          skills_matching_span.finish
        end

        def skill_matcher
          @skill_matcher ||= Zendesk::Routing::TicketSkillMatcher.new(user)
        end

        def apply_filter?
          if investigation_mode?
            user.account.settings.reload
          end

          user.account.settings.skill_based_filtered_views.include?(view.id) && user.account.has_skill_based_view_filters?
        end

        def investigation_mode?
          @investigation_mode ||= user.account.has_log_skill_based_tickets_in_views?
        end

        ##
        # Confirm if the ticket belongs to skill based view
        # if yes, verifies the ticket has the required skills
        # Calls Deco to match the skills
        #
        def ticket_in_filtered_views?(ticket_id)
          skill_verification_span = ZendeskAPM.trace('view_queue.skill_verification', service: APM_SERVICE_NAME)

          return true unless user.account.has_filtered_views_discrepancy?

          # skip verification if this is not a skill based filtered view
          return true unless apply_filter?

          ticket_matches_skills = skill_matcher.matching_tickets(Array(ticket_id)).present?

          Rails.logger.info(
            "ViewQueue: Verifying ticket belongs to skill filtered view ticket_id: #{ticket_id}: #{ticket_matches_skills ? 'match' : 'no match'}"
          )

          ticket_matches_skills
        ensure
          skill_verification_span.finish
        end

        def refresh_tickets(ticket_ids, options)
          refresh_tickets_span = ZendeskAPM.trace(
            'view_queue.refresh_tickets',
            service: APM_SERVICE_NAME
          )

          uniq_ticket_ids = ticket_ids.uniq

          return [] unless uniq_ticket_ids.present?

          key = options[:key]

          Rails.logger.info(
            "ViewQueue: retrieving new set of tickets along with existing : #{uniq_ticket_ids.size} ids"
          )

          new_ticket_ids = view.find_tickets_with_ids(
            user,
            view_params.merge(caller: PLAY_TICKETS_VERIFICATION, timeout: OCCAM_TICKET_VERIFICATION_TIMEOUT),
            uniq_ticket_ids
          )
          increment_tag('zendesk.view_queue.counts.rule_match_attempts')
          Rails.logger.info(
            "ViewQueue:refresh_tickets: setting new ids for key: #{key}"
          )
          update_store(key, new_ticket_ids, options[:expiry])

          play_span.set_tag(
            'zendesk.view_queue.counts.total_refreshed_tickets',
            new_ticket_ids&.size
          )

          Rails.logger.info "ViewQueue: refreshed Ticket ids: #{new_ticket_ids}"
          new_ticket_ids
        ensure
          refresh_tickets_span.finish
        end

        def view_params
          @options.merge(
            caller:   'play',
            ids_only: true,
            paginate: true,
            per_page: MAX_TICKETS_PER_FETCH
          )
        end

        def register_final_tags_for_pop
          play_span.set_tag('zendesk.view_queue.radar.counts.total_users_collided', @radar_total_users_collided)
          play_span.set_tag('zendesk.view_queue.radar.counts.total_tickets_verified', @radar_total_tickets_verified)
          play_span.set_tag(
            'zendesk.view_queue.radar.counts.users_per_ticket',
            @radar_total_users_collided / @radar_total_tickets_verified.to_f
          ) if @radar_total_tickets_verified > 0
        end

        def set_default_tags
          play_span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
          play_span.set_tag('zendesk.user_id', user.id)
          play_span.set_tag('zendesk.view_id', view_id)
          play_span.set_tag('zendesk.account_id', user.account.id)
          play_span.set_tag('zendesk.account_subdomain', user.account.subdomain)
          play_span.set_tag('zendesk.play_version', 'v2')

          play_span.set_tag('zendesk.view_queue.skipped_queue.counts.tickets', skipped&.size)
          play_span.set_tag('zendesk.view_queue.skipped_queue.counts.unskipped', 0)
          play_span.set_tag('zendesk.view_queue.skipped_queue.checked', false)
          play_span.set_tag('zendesk.view_queue.skipped_queue.served_from_skipped_tickets', false)
          play_span.set_tag('zendesk.view_queue.skipped_queue.refreshed', false)

          play_span.set_tag('zendesk.view_queue.user_queue.checked', false)
          play_span.set_tag('zendesk.view_queue.user_queue.refilled_from_occam', false)

          play_span.set_tag('zendesk.view_queue.counts.skipped', 0)
          play_span.set_tag('zendesk.view_queue.counts.agent_collisions', 0)
          play_span.set_tag('zendesk.view_queue.counts.rule_match_attempts', 0)
          play_span.set_tag('zendesk.view_queue.counts.successfully_acquired_lock', 0)
          play_span.set_tag('zendesk.view_queue.counts.failed_to_acquire_lock', 0)

          play_span.set_tag('zendesk.view_queue.radar.counts.users_per_ticket', 0)
        end

        def view_id
          return PREVIEW if view.id.blank?

          view.id
        end

        def increment_tag(name)
          current = play_span.get_tag(name)
          play_span.set_tag(name, current.to_i + 1)
        end

        # Update cache store with new collision information
        #
        # Used when we need to update a new collision information returned by
        # radar that previously might not be cached in this store
        def update_collisions(ticket_id)
          increment_tag('zendesk.view_queue.counts.update_collisions_cache')

          collisions_cache.upsert(ticket_id)
        end

        def collisions
          @collisions ||= read_collision_from_cache
        end

        def collisions_cache
          @collisions_cache ||= CollisionCache.new(client, collision_key)
        end

        def read_collision_from_cache
          trace = ZendeskAPM.trace(
            'view_queue.collisions_from_cache',
            service: APM_SERVICE_NAME
          )

          cache_items = collisions_cache.read

          play_span.set_tag(
            'zendesk.view_queue.counts.current_collisions_from_cache',
            cache_items.size
          )

          cache_items
        ensure
          trace.finish
        end

        # Update the store without altering the existing TTL
        def update_store(key, values, default_expiry)
          remaining_ttl = (ttl = client.ttl(key)).positive? ? ttl : default_expiry
          client.del(key)

          return unless values.present?

          client.pipelined do
            client.rpush(key, values)
            client.expire(key, remaining_ttl)
          end
        end
      end
    end
  end
end
