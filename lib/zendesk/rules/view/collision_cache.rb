module Zendesk
  module Rules
    module View
      class CollisionCache
        attr_reader :key, :store
        # Why is overall collision cache expiry 1 day?
        #
        # The overall collision cache expiry is mostly to clean-up on old
        # collisions at regular intervals.
        #
        # The actual collisions to skip are evaluated during read
        # time based on each item in that cache key. That number can even be
        # higher if need to be.
        COLLISION_CACHE_EXPIRATION = 1.day.to_i
        SINGLE_COLLISION_EXPIRY    = 20 # in seconds

        def initialize(store, key)
          @store = store
          @key   = key
        end

        def upsert(item)
          score = Time.now.to_i

          store.pipelined do
            store.zadd(key, score, item)

            store.expire(key, COLLISION_CACHE_EXPIRATION)
          end
        end

        # Read collisions from cache store
        #
        # We use zset on Redis to keep recent collisions in order to avoid
        # checking radar for every ticket_id.
        #
        # Note:
        # A ticket's considered a recent collision if the collision happened
        #  within the last 20 seconds
        def read
          results = store.pipelined do
            trim
            store.zrange(key, 0, -1)
          end

          results.last.map(&:to_i)
        end

        private

        def trim
          expiry_time = Time.now.to_i - SINGLE_COLLISION_EXPIRY

          store.zremrangebyscore(key, 0, expiry_time)
        end
      end
    end
  end
end
