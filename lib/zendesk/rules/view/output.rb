module Zendesk
  module Rules
    module View
      class Output
        attr_reader :value

        def initialize(definition, account:, user:)
          @account         = account
          @user            = user
          @title_for_field = definition[:title_for_field]
          @value           = definition[:output_key]
        end

        def self.for_definition(definition, account:, user:)
          new(definition, account: account, user: user)
        end

        def title
          Zendesk::Liquid::DcContext.render(
            title_for_field,
            account,
            user
          )
        end

        private

        attr_reader :account, :user, :title_for_field
      end
    end
  end
end
