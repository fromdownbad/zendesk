module Zendesk
  module Rules
    class NullAppInstallation
      def self.as_json(*)
        nil
      end

      def self.present?
        false
      end
    end
  end
end
