require 'occam_client'

module Zendesk
  module Rules
    class OccamClient < ::Occam::Client
      RW_TIMEOUT = 30

      self.logger = Rails.logger

      attr_accessor :options

      def initialize(settings = {})
        super(default_settings.merge(settings))
      end

      def build_params(user_id, rule, query, context, execution_options)
        context_params = context_to_params(context)

        {
          account: account_to_params(rule.account),
          user_id: user_id,
          query:   query.as_json.merge(execution_options.occam_query_params),
          context: context_params,
          options: execution_options.occam_options
        }.with_indifferent_access
      end

      def default_settings
        defaults = {
          url: ENV['ZENDESK_OCCAM_K8S_URL'],
          timeout: RW_TIMEOUT,
          open_timeout: ENV.fetch('ZENDESK_OCCAM_OPEN_TIMEOUT', 0.05).to_f
        }

        defaults
      end

      def count_many(request_execution_options, views, account, user, params)
        params = build_count_many_params(
          request_execution_options,
          views,
          account,
          user,
          params
        )
        post('/count_many', params)
      end

      def build_count_many_params(
        request_execution_options,
        views,
        account,
        user,
        params
      )
        user ||= account.anonymous_user

        context = Context.new(
          account,
          user,
          rule_type:      :view,
          condition_type: :all,
          component_type: :condition,
          executing:      true,
          timeout:        2
        )
        queries_params = views.map do |view|
          execution_options = ExecutionOptions.new(view, view.account, params)

          query = RuleQueryBuilder.
            query_for(view, user, execution_options, context)

          query.as_json.merge(execution_options.occam_query_params)
        end

        {
          account: account_to_params(account),
          user_id: user.id,
          queries: queries_params,
          context: context_to_params(context),
          options: request_execution_options.occam_options
        }.with_indifferent_access
      end

      private

      def account_to_params(account)
        {
          'id'        => account.id,
          'shard_id'  => account.shard_id,
          'subdomain' => account.subdomain
        }
      end

      def rule_to_params(rule, query)
        {
          'id'   => rule.id,
          'type' => rule.type
        }.merge(query.as_json.stringify_keys)
      end

      def field_to_hash(field)
        h = {
          'id'   => field.id,
          'type' => field.type.to_s
        }

        h['key'] = field.key if field.respond_to?(:key)
        h['tag'] = field.tag if field.respond_to?(:tag)
        h
      end

      def context_to_params(context)
        base = {
          'available_languages' => context.available_languages,
          'use_status_hold?'    => context.use_status_hold?,
          'default_language_id' => context.default_language_id
        }

        base.merge(context.accessed_fields)
      end
    end
  end
end
