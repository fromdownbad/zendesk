module Zendesk
  module Rules
    class Action
      CUSTOM_FIELD_MARKER = 'custom_fields'.freeze

      CUSTOM_DATE_ACTION_TYPES = [
        {
          title: ::I18n.t(
            'txt.admin.public.javascript.new_rule.specific_date'
          ),
          value: 'specific_date'
        },
        {
          title: ::I18n.t(
            'txt.admin.public.javascript.new_rule.days_from_now'
          ),
          value: 'days_from_now'
        }
      ].freeze

      CUSTOM_DATE_FORMATS = {
        'specific_date' => 'date',
        'days_from_now' => 'text'
      }.freeze

      EMPTY_TITLE = '-'.freeze
      NULL_VALUE  = '__NULL__'.freeze

      METADATA = {
        'notification_sms_user'  => %i[phone_numbers],
        'notification_sms_group' => %i[phone_numbers]
      }.freeze

      FORMAT_ACTIONS = {
        'target'      => :target_format,
        'custom_date' => :date_format
      }.freeze

      NULLABLE_ACTIONS = %w[assignee_id group_id].freeze
      TAG_ACTIONS      = %w[current_tags remove_tags set_tags].freeze

      private_constant :CUSTOM_FIELD_MARKER,
        :CUSTOM_DATE_ACTION_TYPES,
        :CUSTOM_DATE_FORMATS,
        :EMPTY_TITLE,
        :FORMAT_ACTIONS,
        :METADATA,
        :NULLABLE_ACTIONS,
        :NULL_VALUE,
        :TAG_ACTIONS

      def self.for_definition(definition, account:)
        new(
          Definition.new(
            definition[:value],
            definition[:title],
            definition[:values][:type],
            definition[:group],
            definition[:values][:list]
          ),
          account: account
        )
      end

      def initialize(definition, account:)
        @account  = account
        @source   = definition.source
        @title    = definition.title
        @type     = action_type(definition.source, definition.type)
        @group    = definition.group
        @list     = action_list(definition.list, definition.type)
        @metadata = METADATA[definition.source]
      end

      def subject
        Api::V2::Tickets::AttributeMappings.ticket_attribute_name(
          source,
          account
        ).to_s
      end

      def values
        @values ||= begin
          list.
            reject(&method(:invalid_item?)).
            map do |item|
              Value.new(
                item_value(item),
                item[:title],
                enabled_item?(item),
                format(item)
              )
            end
        end
      end

      def repeatable?
        false
      end

      def nullable?
        NULLABLE_ACTIONS.include?(source) ||
          subject.include?(CUSTOM_FIELD_MARKER)
      end

      attr_reader :group,
        :metadata,
        :title,
        :type

      protected

      attr_reader :account,
        :list,
        :source

      private

      def action_list(list, type)
        return CUSTOM_DATE_ACTION_TYPES if type == 'custom_date'

        list
      end

      def action_type(source, type)
        TAG_ACTIONS.include?(source) ? 'tags' : type
      end

      def item_value(item)
        return NULL_VALUE if null_item?(item)

        Api::V2::Tickets::AttributeMappings.
          ticket_attribute_value(source, item[:value], account).
          to_s
      end

      def invalid_item?(item)
        null_item?(item) && !nullable?
      end

      def null_item?(item)
        item[:title] == EMPTY_TITLE
      end

      def enabled_item?(item)
        return item[:enabled] if item.key?(:enabled)

        return true unless source == 'priority_id'

        account.field_priority.is_active? &&
          (
            account.has_extended_ticket_priorities? ||
              BasicPriorityType.find(item_value(item)).present?
          )
      end

      def format(item)
        return nil unless FORMAT_ACTIONS.key?(type)

        send(FORMAT_ACTIONS[type], item)
      end

      def date_format(item)
        CUSTOM_DATE_FORMATS[item[:value]]
      end

      def target_format(item)
        (item[:v2] ? UrlTargetV2 : Target).
          public_send(:format, item[:content_type], item[:method])
      end

      Definition = Struct.new(:source, :title, :type, :group, :list)
      Value      = Struct.new(:value, :title, :enabled?, :format)

      private_constant :Definition,
        :Value
    end
  end
end
