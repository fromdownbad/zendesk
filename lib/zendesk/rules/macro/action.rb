module Zendesk
  module Rules
    module Macro
      class Action
        CUSTOM_FIELD_PREFIX = 'ticket_fields'.freeze
        EMPTY_TITLE         = '-'.freeze
        NULL_VALUE          = '__NULL__'.freeze

        NULLABLE_ACTIONS = %w[assignee_id group_id].freeze
        TAG_ACTIONS      = %w[current_tags remove_tags set_tags].freeze

        REPEATABLE_ACTIONS = %w[cc set_tags current_tags].freeze
        REPEATABLE_TYPES   = %w[multiselect_list].freeze

        private_constant :CUSTOM_FIELD_PREFIX,
          :EMPTY_TITLE,
          :NULLABLE_ACTIONS,
          :NULL_VALUE,
          :REPEATABLE_ACTIONS,
          :REPEATABLE_TYPES,
          :TAG_ACTIONS

        def self.for_definition(definition, account:)
          new(
            Definition.new(
              definition[:value],
              definition[:title],
              definition[:values][:type],
              definition[:group],
              definition[:values][:list]
            ),
            account: account
          )
        end

        def initialize(definition, account:)
          @account = account
          @source  = definition.source
          @title   = definition.title
          @type    = action_type(definition.source, definition.type)
          @group   = definition.group
          @list    = definition.list
        end

        def subject
          Api::V2::Tickets::AttributeMappings.ticket_attribute_name(
            source,
            account
          ).to_s
        end

        def values
          @values ||= begin
            list.
              reject(&method(:invalid_item?)).
              map do |item|
                Value.new(item_value(item), item[:title], enabled_item?(item))
              end
          end
        end

        def repeatable?
          REPEATABLE_TYPES.include?(type) || REPEATABLE_ACTIONS.include?(source)
        end

        def nullable?
          NULLABLE_ACTIONS.include?(source) ||
            source.starts_with?(CUSTOM_FIELD_PREFIX)
        end

        def metadata
          nil
        end

        attr_reader :group,
          :title,
          :type

        protected

        attr_reader :account,
          :list,
          :source

        private

        def action_type(source, type)
          TAG_ACTIONS.include?(source) ? 'tags' : type
        end

        def item_value(item)
          return NULL_VALUE if null_item?(item)

          Api::V2::Tickets::AttributeMappings.
            ticket_attribute_value(source, item[:value], account).to_s
        end

        def invalid_item?(item)
          null_item?(item) && !nullable?
        end

        def null_item?(item)
          item[:title] == EMPTY_TITLE
        end

        def enabled_item?(item)
          return item[:enabled] if item.key?(:enabled)

          return true unless source == 'priority_id'

          account.field_priority.is_active? &&
            (
              account.has_extended_ticket_priorities? ||
                BasicPriorityType.find(item_value(item)).present?
            )
        end

        Definition = Struct.new(:source, :title, :type, :group, :list)

        Value = Struct.new(:value, :title, :enabled?) do
          def format
            nil
          end
        end

        private_constant :Definition,
          :Value
      end
    end
  end
end
