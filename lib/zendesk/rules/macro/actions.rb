module Zendesk
  module Rules
    module Macro
      class Actions
        REPEATABLE_ACTIONS = %i[cc set_tags current_tags].freeze
        REPEATABLE_TYPES   = %i[multiselect_list].freeze

        private_constant :REPEATABLE_ACTIONS,
          :REPEATABLE_TYPES

        def initialize(account, user)
          @account = account
          @user    = user
        end

        def definitions
          ZendeskRules::Definitions::Actions.definitions_as_json(context).
            reject { |action| action[:value] == 'PICKACTION' }.
            each(&method(:convert_attributes))
        end

        protected

        attr_reader :account,
          :user

        private

        def context
          Zendesk::Rules::Context.new(
            account,
            user,
            component_type: :action,
            rule_type:      :macro
          )
        end

        def convert_attributes(action)
          action[:values][:list].
            delete_if { |item| invalid_item?(action, item) }.
            each do |item|
              item[:value] = attribute_value(action[:value], item[:value])

              next if item.key?(:enabled)

              item[:enabled] = enabled_item?(action, item)
            end

          action[:repeatable] = repeatable_action?(action)

          action[:value] = attribute_name(action[:value])
          action[:field] = action[:value]
        end

        def attribute_name(attribute)
          Api::V2::Tickets::AttributeMappings.
            ticket_attribute_name(attribute).to_s
        end

        def attribute_value(attribute, value)
          Api::V2::Tickets::AttributeMappings.
            ticket_attribute_value(attribute, value, account).to_s
        end

        def invalid_item?(action, item)
          action[:value] == 'priority_id' && item[:title] == '-'
        end

        def enabled_item?(action, item)
          return true unless action[:value] == 'priority_id'

          account.field_priority.is_active? &&
            (
              account.has_extended_ticket_priorities? ||
                BasicPriorityType.find(item[:value]).present?
            )
        end

        def repeatable_action?(action)
          REPEATABLE_TYPES.include?(action[:values][:type].to_sym) ||
            REPEATABLE_ACTIONS.include?(action[:value].to_sym)
        end
      end
    end
  end
end
