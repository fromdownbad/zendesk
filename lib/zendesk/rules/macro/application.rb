module Zendesk::Rules
  module Macro
    class Application
      def initialize(macro:, ticket: nil, user:, for_chat: false)
        @macro  = macro
        @ticket = ticket
        @user   = user
        @for_chat = for_chat
      end

      def result
        @result ||= begin
          Zendesk::Rules::MacroApplication.new(macro, user, ticket).run
        end
      end

      def upload_token
        return if attachments.none?

        statsd_client.increment(
          'attachment:apply',
          tags: %W[
            macro:#{macro.id}
            attachment-count:#{attachments_count}
          ]
        )

        if account.has_log_macro_attachments?
          Rails.logger.info(
            "Apply macro attachment for macro: #{macro.id} | "\
            "account: #{account.id}, "\
            "user: #{user.id}, "\
            "ticket: #{ticket.try(:id)}, "\
            "attachment-count: #{attachments_count}"
          )
        end

        @upload_token ||= begin
          account.
            upload_tokens.
            create(source: account, target: target).
            tap do |token|
              attachments.each do |attachment|
                token.add_raw_attachment(
                  attachment.current_data,
                  attachment.filename
                ).save!
              end
            end
        end
      end

      protected

      attr_reader :macro,
        :ticket,
        :user,
        :for_chat

      private

      def target
        if ticket.try(:id) && for_chat && account.has_polaris?
          ticket
        else
          user
        end
      end

      def attachments
        @attachments ||= macro.attachments
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'macro')
      end

      def account
        @account ||= user.account
      end

      def attachments_count
        @attachments_count ||= attachments.count
      end
    end
  end
end
