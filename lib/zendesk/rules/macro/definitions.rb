module Zendesk
  module Rules
    module Macro
      class Definitions
        INVALID_ACTION = 'PICKACTION'.freeze

        private_constant :INVALID_ACTION

        def initialize(account, user)
          @account = account
          @user    = user
        end

        def actions
          ZendeskRules::Definitions::Actions.
            definitions_as_json(action_context).
            reject { |definition| definition[:value] == INVALID_ACTION }.
            map do |definition|
              Action.for_definition(definition, account: account)
            end
        end

        protected

        attr_reader :account,
          :user

        private

        def action_context
          Zendesk::Rules::Context.new(
            account,
            user,
            component_type: :action,
            dc_cache:       user.dc_cache,
            rule_type:      :macro
          )
        end
      end
    end
  end
end
