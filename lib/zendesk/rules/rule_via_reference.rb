class Zendesk::Rules::RuleViaReference
  def initialize(rule:, user:)
    @rule = rule
    @user = user
  end

  def rule_id
    rule.try(:id)
  end

  def deleted?
    rule.nil? || rule.deleted?
  end

  def title
    unless rule.present?
      return I18n.t('txt.api.v2.rule_source.hard_deleted_reference')
    end

    if rule.deleted?
      I18n.t(
        'txt.api.v2.rule_source.soft_deleted_reference',
        title: rule.title
      )
    else
      rule.title
    end
  end

  def rule_type
    rule.try(:rule_type)
  end

  def viewable?
    !deleted? && user.can?(:view, rule)
  end

  private

  attr_reader :rule, :user
end
