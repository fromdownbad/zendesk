require 'zendesk_rules/component/context'
require 'ostruct'

module Zendesk::Rules
  class Context < ZendeskRules::Component::Context
    attr_accessor :options
    attr_reader :account

    delegate :has_groups?,
      :has_multibrand?,
      :has_ticket_forms?,
      :has_service_level_agreements?,
      :csat_reason_code_enabled?,
      :extended_ticket_metrics_visible?,
      :business_hours_active?,
      :has_multiple_schedules?,
      :has_ticket_sharing_triggers?,
      :has_rules_can_reference_macros?,
      :has_update_type_validation?,
      :is_collaboration_enabled?,
      :has_tweet_requester_action?,
      :has_organizations?,
      :has_satisfaction_prediction_enabled?,
      :has_automatic_answers_enabled?,
      :has_custom_date_grouping?,
      :has_skill_based_ticket_routing?,
      :has_skill_based_ticket_routing_v2?,
      :has_side_conversation_macros?,
      :has_side_conversation_slack_macros?,
      :has_side_conversation_ticket_macros?,
      :has_ticket_followers_allowed_enabled?,
      :has_email_ccs?,
      :has_follower_and_email_cc_collaborations_enabled?,
      :has_views_any_channel_via_id?,
      :has_social_messaging_agent_workspace?,
      :has_native_messaging?,
      :has_social_messaging?,
      :has_support_webhook_enabled?,
      :has_all_sunco_messaging_channels?,
      :has_social_messaging_allow_wechat?,
      :has_custom_statuses_in_views?,
      :workspaces, to: :account

    delegate :accepted_outbound_sharing_agreements,
      :accessed_fields,
      :active_brands,
      :active_groups,
      :active_shared_macros,
      :active_ticket_forms,
      :agents,
      :assignable_agents,
      :current_organization_ids,
      :default_schedule_id,
      :default_ticket_form_id,
      :find_organization_custom_field,
      :find_ticket_field,
      :find_user_custom_field,
      :integration_service_instance_map,
      :monitored_facebook_page_map,
      :monitored_twitter_handle_map,
      :organization_custom_fields,
      :organization_list,
      :organization_list_size,
      :recipient_emails,
      :registered_integration_service_map,
      :requester_agents,
      :satisfaction_reasons,
      :schedules,
      :sharing_agreements,
      :ticket_custom_fields,
      :active_custom_statuses,
      :user_custom_fields, to: :cache

    def initialize(account, user, options)
      @account = account
      @user    = user
      @options = options
      @cache   = options.fetch(
        :cache,
        ContextCache.new(
          account,
          user,
          executing:  options[:executing],
          validating: options[:validating]
        )
      )
    end

    def account_created_at
      @account.created_at.utc
    end

    def condition_type
      @options[:condition_type]
    end

    def validating
      @options[:validating]
    end

    def executing
      @options[:executing]
    end

    # rubocop:disable Naming/PredicateName
    def has_sms_triggers?
      @account.voice_feature_enabled?(:sms)
    end
    # rubocop:enable Naming/PredicateName

    def unlimited_rule?
      return account.has_unlimited_triggers? if rule_type == :trigger

      true
    end

    def use_status_hold?
      @account.use_status_hold?
    end

    def current_user_group_ids
      (@user && @user.group_ids.first) ? @user.group_ids : nil
    end

    def current_user_skills
      @user ? InstanceValue.skill_ids_for_agent(@user) : nil
    end

    def current_role
      @user.role.downcase
    end

    def ticket_access
      @user.ticket_access
    end

    def find_organization_by_id(id)
      @account.organizations.find_by_id(id)
    end

    def host_name
      @account.host_name(mapped: false)
    end

    def recipients
      @options.fetch(:recipients) { [] }
    end

    def satisfaction_enabled?
      @account.has_customer_satisfaction? &&
        @account.has_customer_satisfaction_enabled?
    end

    def messaging_csat_enabled?
      @account.has_native_messaging_csat?
    end

    def time_zones
      @time_zones ||= begin
        ActiveSupport::TimeZone.all_sorted.map { |tz| [tz.to_s, tz.name] }
      end
    end

    def localized_languages
      return [] unless @user

      @localized_languages ||= begin
         current_locale = @user.translation_locale
         collator = ICU::Collation::Collator.new(current_locale.locale)
         mapped_translations = @account.available_languages.map { |language| [language.localized_name(display_in: current_locale), language.id] }
         mapped_translations.sort { |a, b| collator.compare(a.first, b.first) }
       end
    end

    def default_language_id
      @account.translation_locale.id
    end

    def current_user_id
      @user && @user.id
    end

    def active_targets
      @active_targets ||= begin
        @account.targets.active.map do |t|
          {
            title:           t.title,
            id:              t.id,
            include_message: t.is_message_supported?,
            v2:              t.is_a?(UrlTargetV2),
            method:          t.settings[:method],
            content_type:    t.settings[:content_type]
          }
        end || []
      end
    end

    def available_languages
      @available_languages ||= begin
         @account.available_languages.map do |language|
           [language.name, language.id]
         end
       end || []
    end

    def end_user_id
      @options[:end_user_id]
    end

    def find_user(id)
      u = @account.users.find(id)

      {name: u.name, id: u.id}
    end

    def user_exists?(id)
      @account.users.where(id: id).exists?
    end

    def first_business_hour_before(time)
      @account.time_before_business_hours(Time.at(time), 1)
    end

    def calendar_hours_ago(num)
      @account.calendar_hours_ago(num)
    end

    def calendar_hours_from_now(num)
      @account.calendar_hours_from_now(num)
    end

    def targets_enabled?
      @account.has_targets?
    end

    def schedule_calculations
      @account.schedules.active.by_time.map(
        &Zendesk::BusinessHoursSupport::Calculation.method(:new)
      )
    end

    def has_subject_field? # rubocop:disable Naming/PredicateName
      @account.field_subject.is_active?
    end

    def has_ticket_collaboration_threads? # rubocop:disable Naming/PredicateName
      @account.has_side_conversations_enabled?
    end

    def has_side_conversations_tickets? # rubocop:disable Naming/PredicateName
      @account.has_side_conversations_tickets?
    end

    def edit_rule_mode?
      @options[:edit_rule_mode?] || false
    end

    protected

    attr_reader :cache
  end
end
