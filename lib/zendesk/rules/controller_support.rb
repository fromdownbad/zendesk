module Zendesk::Rules::ControllerSupport
  module InheritViews
    # Use templates in app/views/rules
    def controller_path
      superclass.controller_path
    end
  end

  def self.included(base)
    base.helper_method :sort_alphabetically?
    base.extend(Filters)
  end

  module Filters
    def restrict_access
      before_action :authorize_agent
      before_action :authorize_access_to_rule,   except: [:new, :index, :create, :destroy_inactive, :sort]
      before_action :check_permissions_for_edit, only: [:update, :destroy, :activate]
      before_action :require_delete_method,      only: [:destroy, :destroy_inactive]
    end
  end

  private

  def check_permissions_for_edit
    head :forbidden unless current_user.can?(:edit, rule)
  end

  def owner
    @owner ||= current_account
  end

  def find_rules
    load_rules
    sort_rules
    select_rules

    @active_rules, @inactive_rules = @rules.partition(&:is_active?)
  end

  # Macro specific
  def sort_alphabetically?
    false
  end

  def sort_rules
    if params[:sort] == 'created_at'
      @rules.sort! { |x, y| y.created_at <=> x.created_at }
    elsif params[:sort] == 'updated_at'
      @rules.sort! { |x, y| y.updated_at <=> x.updated_at }
    elsif /execution_count/.match?(params[:sort])
      @rules.sort! do |x, y|
        y.usage.public_send(timeframe).to_i <=>
          x.usage.public_send(timeframe).to_i
      end
    elsif current_account.settings.macro_order == 'alphabetical' &&
          @rule_type == 'macros'
      @rules.sort! { |x, y| x.title.downcase <=> y.title.downcase }
    end
  end

  def select_rules
    if params[:select] == 'group_none'
      # rubocop:disable Style/InverseMethods
      @rules = @rules.select { |r| r.owner_type != 'Group' }
      # rubocop:enable Style/InverseMethods
    elsif params[:select].to_s.include?('group_id_')
      @rules = @rules.select do |r|
        r.owner_type == 'Group' &&
          r.owner_id == params[:select].delete('group_id_').to_i
      end
    end
  end

  def personal?
    params[:select] == 'personal'
  end

  def rule
    @rule ||= begin
      rule = if params[:id] == View::INCOMING
        View.incoming(current_user)
      else
        current_account.send("all_#{rule_type}").find(params[:id])
      end

      rule.current_user = current_user
      rule
    end
  end

  def authorize_access_to_rule
    # simulate api v2 access (agent only) for the incoming view (has no id/is a
    # fake view)
    return if current_user.is_agent? && params[:action] == 'next' && params[:id] == View::INCOMING

    # a regular view
    deny_access unless Rule.find_for_user(current_user, rule, force_all: editing?)
  end

  def definition
    return if params[:sets].blank?

    @definition ||= Definition.build(definition_params)
  end

  def definition_params
    @definition_params ||= DefinitionParams.new(params)
  end

  def editing?
    ['edit', 'update', 'destroy', 'activate'].include?(params[:action])
  end

  def require_delete_method
    render plain: '400 Bad request', status: :bad_request unless request.delete?
  end

  def klass
    @klass ||= rule_type.classify.constantize
  end

  def rule_type
    @rule_type ||= self.class.name.demodulize.tableize.split('_').first
  end

  def timeframe
    case params[:sort]
    when 'execution_count_hourly' then :hourly
    when 'execution_count_daily'  then :daily
    when 'execution_count_weekly' then :weekly
    else
      :hourly
    end
  end

  # Only present for Views
  def output
    nil
  end
end
