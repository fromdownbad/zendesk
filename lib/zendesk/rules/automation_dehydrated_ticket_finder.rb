class Zendesk::Rules::AutomationDehydratedTicketFinder < Zendesk::Rules::AutomationTicketFinder
  def find_tickets
    Ticket.on_slave do
      Zendesk::Rules::DehydratedTicket.
        dehydrate_tickets(super(nice_ids_and_timestamps_only: true))
    end
  end
end
