require 'active_record/comments'
require 'zendesk_rules/custom_rule_field'
require 'zendesk_rules/errors'
require 'zendesk_rules/rule_query/restriction'
require 'zendesk_rules/optimizations/tags_join_optimizer'
require 'zendesk_rules/optimizations/tags_order_optimizer'
require 'zendesk_rules/optimizations/tags_like_optimizer'

module Zendesk
  module Rules
    class RuleExecuter
      class UnprocessableEntity < StandardError; end
      class UnknownOccamError < StandardError; end

      attr_reader :rule, :user, :params, :account

      # miserable hack for tag-join bad path.  please fix me,
      # but we need overall tag counts
      ZendeskRules::TagsJoinOptimizer.blocked [486282]

      # special case one rule for Airbnb which use 100s of tags
      ZendeskRules::TagsLikeOptimizer.allowed [59680187]

      REQUEST_ORIGIN_FOR_COUNT_DURING_FIND = 'count-for-find'.freeze

      def initialize(rule, user, params = {})
        @rule   = rule
        @user   = user
        @params = params
        @account = rule.account
      end

      def query
        @query ||= begin
          raw_query.compile(:sql).tap do |compiled_query|
            optimize_tags!(compiled_query)
          end
        end
      end

      def execution_options
        @execution_options ||=
          Zendesk::Rules::ExecutionOptions.new(@rule, @rule.account, params)
      end

      def self.count_tickets(rule, user, params = {})
        executor = new(rule, user, params)
        executor.count_tickets
      end

      def self.find_tickets(rule, user, params = {})
        executor = new(rule, user, params)
        executor.find_tickets
      end

      def count_tickets
        count_tickets_with_occam_client
      end

      def count_tickets_with_occam_client
        res = nil
        time = Benchmark.realtime { res = occam.count(occam_params) }

        rule.cached_by_hanlon = true if res['cached']

        rule.track_execution_metrics('count_tickets_occam', time, res['count'])

        res['count']
      end

      def context
        @context ||= Zendesk::Rules::Context.new(@rule.account, user || @rule.account.anonymous_user, rule_type: :view, condition_type: :all, component_type: :condition, executing: true)
      end

      def occam_params
        user = @user || @rule.account.anonymous_user

        occam.
          build_params(user.id, @rule, raw_query, context, execution_options)
      end

      def occam
        @occam ||= Zendesk::Rules::OccamClient.new(
          timeout: execution_options.timeout,
          circuit_options: circuit_options
        )
      end

      def circuit_options
        {
          failure_threshold: 3,
          reset_timeout: 10,
          store: circuit_store
        }
      end

      def circuit_store
        if Rails.cache.try(:dalli)
          JohnnyFive::MemcachedStore.new
        else
          JohnnyFive::MemoryStore.instance
        end
      end

      def find_tickets
        find_tickets_with_occam_client
      end

      def expected_sql_from_query
        return '' if query.empty?

        occam.sql(occam_params)
      end

      def expected_sql_count_from_query
        return '' if query.empty?

        occam.sql_count(occam_params)
      end

      def watch
        occam.watch(occam_params)
      end

      def unwatch
        occam.unwatch(occam_params)
      end

      def watchability
        raw_query.cacheability
      end
      OCCAM_422_ERRORS = [
        'AttributeError',
        'InvalidOrdering',
        'InvalidRule',
        'RuleInvalid',
        'UnsupportedCondition',
        'ValidationError'
      ].freeze

      def find_tickets_with_occam_client
        res = nil
        time = Benchmark.realtime do
          res = begin
            if account.has_views_circuit_breaker?
              occam.with_circuit(circuit_key_for('find')) do |client|
                client.find(occam_params)
              end
            else
              occam.find(occam_params)
            end
          end
        end

        rule.cached_by_hanlon = true if res['cached']

        return res if execution_options.occam_raw_output?

        if res.key?('error')
          Rails.logger.error("Occam find error: #{res}")
          if OCCAM_422_ERRORS.include?(res['error'])
            raise UnprocessableEntity, res['message']
          else
            raise UnknownOccamError, res['message']
          end

        else
          ids = res['ids']
        end

        rule.track_execution_metrics('find_tickets_occam', time, ids.length)

        if ids.empty?
          return [] unless execution_options.paginate?

          return simulate_pagination([])
        end

        return ids if execution_options.ids_only?

        hydrate_tickets(ids, caller: REQUEST_ORIGIN_FOR_COUNT_DURING_FIND)
      end

      private

      def raw_query
        @raw_query ||=
          RuleQueryBuilder.query_for(@rule, @user, execution_options, context)
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'rules')
      end

      def reorder_tickets(tickets, ids)
        sort_position = ids.
          each.
          with_index.
          each_with_object({}) do |(id, index), hash|
            hash[id] = index
          end

        tickets.sort_by { |t| sort_position[t.id] }
      end

      def hydrate_tickets(ids, options)
        # hydrate tickets from ids
        scope = rule.
          account.
          tickets.
          where(id: ids).
          order("FIELD(id, #{ids.join(', ')})").
          includes(execution_options.includes)

        tickets = if rule.can_find_tickets_on_archive?
          unsorted = scope.all_with_archived
          reorder_tickets(unsorted, ids)
        else
          scope.all
        end

        if execution_options.paginate?
          tickets = simulate_pagination(tickets, options)
        end

        if tickets.size != ids.size
          hydrated_ticket_ids = tickets.map(&:id)
          Rails.logger.warn("hydrate-tickets went looking for #{ids.inspect} but only got #{hydrated_ticket_ids.inspect}")

        end

        add_virtual_attributes_to_tickets!(tickets)
        tickets
      end

      def query_comment
        [
          "rule:#{@rule.to_param}",
          "user:#{@user.to_param}",
          "shard:#{@rule.account.shard_id}",
          "rule_type:#{@rule.type.downcase}"
        ].join(', ')
      end

      def with_comments_and_deleted
        Ticket.with_deleted do
          ActiveRecord::Comments.comment(query_comment) do
            yield
          end
        end
      end

      def apply_since
        # TODO: push onto the rule
        return unless execution_options.since

        query.with(:updated_at).gt(execution_options.since)
      end

      def tag_cost_hash(compiled_query)
        tags = compiled_query.tag_terms

        return {} unless tags.any?

        account.
          tag_scores.
          where(tag_name: tags).
          each_with_object({}) do |ts, h|
            h[ts.tag_name] = ts.score
          end
      end

      def max_tag_score(compiled_query)
        account.tag_scores.maximum(:score) if compiled_query.tag_terms.any?
      end

      def optimize_tags!(compiled_query)
        # TagsOrderOptimizer:
        #   Reorder tags within a condition, as well as whole conditions
        # based on a heuristic on scores. Very rare tags on positive conditions
        # are brought forward so they can fail quickly. Most common tags on
        # negative conditions are brought forward so they can fail fairly
        # quickly as well.
        #
        # TagsJoinOptimizer:
        #   Find the lowest score tag that we absolutely must match. Then we
        # join taggings table with tickets table based on that tag (we limit
        # the score to < 20000 or so tickets). This means our working set of
        # tickets shrinks drastically.
        #
        # TagsLikeOptimizer:
        #   Find a finite set of substrings from within a positive condition
        # which we must match. It's best if this condition has a large number
        # of tags for which one of them has to match. Since this group of
        # substrings completely cover the actual set of tags, we can insert a
        # condition of the form "(current_tags LIKE '%substring1%' OR ... OR
        # current_tags LIKE '%substringn%')". This condition logically implies
        # the actual condition, and acts like a fast failure gate. So if the
        # actual ticket counts are small, the giant condition is only applied
        # for mostly actual matches. This is currently hardcoded for one rule
        # in Airbnb.
        #
        # See rules gem for details.
        optimizers = [
          ZendeskRules::TagsOrderOptimizer,
          ZendeskRules::TagsJoinOptimizer,
          ZendeskRules::TagsLikeOptimizer
        ].map do |optimizer_class|
          optimizer_class.new(
            compiled_query: compiled_query,
            account_id:     account.id,
            rule_id:        rule.try(:id)
          )
        end

        optimizers.select(&:should_optimize?).each do |optimizer|
          optimizer.optimize_query!(
            score_hash: tag_cost_hash(compiled_query),
            max_score:  max_tag_score(compiled_query)
          )
        end
      end

      def add_virtual_attributes_to_tickets!(tickets)
        return if tickets.empty? || rule.output.nil?

        virtual_attribute_fields = rule.
          output.
          rule_fields.
          select { |f| f.virtual_attribute.present? }

        return if virtual_attribute_fields.empty?

        tickets.each do |ticket|
          virtual_attribute_fields.each do |field|
            attr_name = field.virtual_attribute.to_s
            next if ticket[attr_name]
            value = (field.is_a?(ZendeskRules::CustomRuleField) ? field.value(ticket) : field.humanized_value(ticket))

            ticket.add_virtual_attribute(attr_name, value)
          end
        end
      end

      def simulate_pagination(tickets, options = {})
        cached_count = rule.ticket_count(user, options.merge(refresh: false))

        # Fix for Problem #70828. Please refer to the ticket for more
        # information.
        #
        # TicketsHelper::next_ticket_in_collection calls this twice -
        # once for fetching the current page and once for fetching next page
        # updating the total tickets count cache in each call. If the view has
        # ticket size less than the page size, it incorrectly updates the count
        # to page size.
        #
        # BBO, 6/29/2016
        # Somehow a hanlon result managed to poison a rule count for uber. For
        # now, don't assume that it's correct.
        if tickets.size < execution_options.per_page && !rule.cached_by_hanlon
          @last_page = true
          out_of_range = tickets.empty? && execution_options.page > 1

          statsd_result = 'under_per_page'

          # need to set this value so that it's not undefined
          total_entries = if cached_count.value == ::View::BROKEN_COUNT
            cached_count.value
          else
            execution_options.offset + tickets.size
          end

          unless out_of_range
            # we did not get a full page of tickets, so we can calculate an
            # exact ticket count
            cached_count.value = total_entries
            cached_count.updated_at = Time.now
            cached_count.save
          end

        elsif cached_count.fresh? &&
              cached_count.value &&
              account.has_cached_view_counts_for_pagination?
          # we have a good cached value and there are plenty of pages and
          # we're uber or airbnb.
          statsd_result = 'cached_count'

          total_entries = cached_count
        else
          # we need to do a recount
          statsd_result = 'refresh'

          cached_count.update_now(fallback_after: 2.seconds)
          total_entries = cached_count.value
        end

        statsd_client.
          increment('simulated_pagination', tags: %W[result:#{statsd_result}])

        collection_klass(total_entries).
          create(execution_options.page,
            execution_options.per_page,
            total_entries) do |pager|
          pager.try(:last_page!) if @last_page
          pager.replace(tickets)
        end
      end

      def collection_klass(total_entries)
        if total_entries == ::View::BROKEN_COUNT
          NilCountCollection
        else
          Collection
        end
      end

      def class_to_stat(klass)
        klass.name.underscore.tr('/', '_').squeeze('_')
      end

      def circuit_key_for(operation)
        return unless rule.id

        ['occam_circuit', account.id, rule.id, operation, current_caller].compact.join(':')
      end

      # Returns the orgin string when we have a recognized caller (origin).
      # For everything else it returns nil
      def current_caller
        origin = occam_params.dig(:options, :request_origin, :caller).presence

        case origin
        when Zendesk::Rules::View::ViewQueue::PLAY_TICKETS_VERIFICATION
          origin
        when Zendesk::Rules::View::ViewQueue::PLAY_TICKETS_REFILL
          origin
        end
      end

      # prevent bad clients from requesting endless empty pages
      module EmptyPageThrottling
        class << self
          def with_throttling(rule, user, execution_options)
            if should_throttle?(rule, execution_options) &&
               throttled?(rule, user)
              throttle!(rule, user)
            end

            tickets = yield
            throttle!(rule, user) if should_throttle?(rule, execution_options) && tickets.empty?

            tickets
          end

          private

          def should_throttle?(rule, execution_options)
            !rule.new_record? &&
              execution_options.paginate? &&
              execution_options.page > 1
          end

          def throttled?(rule, user)
            Prop.throttled?(
              :rule_pagination,
              rule_pagination_throttle_key(rule, user)
            )
          end

          def throttle!(rule, user)
            Prop.throttle!(
              :rule_pagination,
              rule_pagination_throttle_key(rule, user)
            )
          end

          def rule_pagination_throttle_key(rule, user)
            # User may be nil. See Rule#find_tickets
            [rule.account_id, rule.id, user.try(:id)].join('-')
          end
        end
      end

      class Collection < WillPaginate::Collection
        def out_of_range?
          Rails.logger.warn("Empty?=#{empty?} get_page=#{current_page} total_pages=#{total_pages}")

          empty? &&
            current_page > 1 &&
            current_page > total_pages &&
            total_pages > 0
        end

        def total_entries=(value)
          if value.is_a?(CachedRuleTicketCount)
            @pretty_total_entries = value.pretty_print
            super(value.value)
          else
            super
          end
        end

        def pretty_total_entries
          @pretty_total_entries || total_entries
        end
      end

      class NilCountCollection < Collection
        ##
        # total_pages is not available for
        # null count results.
        #
        def out_of_range?
          Rails.logger.warn("Empty?=#{empty?} get_page=#{current_page}")

          empty? && current_page > 1
        end

        def last_page!
          @last_page = true
        end

        def total_pages
          @last_page ? current_page : current_page + 1
        end
      end
    end
  end
end
