# Represents a Ticket pending automation processing by its nice_id.
#
# It keeps a copy of the `generated_timestamp` so we can check if the Ticket has
# on the master changed since it was originally queried on the slave. If it has
# changed, we can run the Automation's SQL query on it again to check its
# eligibility.
Zendesk::Rules::DehydratedTicket = Struct.new(:id, :nice_id, :generated_timestamp) do
  OCCAM_CALLER_NAME = 'automation-ticket-match'.freeze

  class << self
    def new_from_ticket(ticket)
      new ticket.id, ticket.nice_id, ticket.generated_timestamp.to_i
    end

    def dehydrate_tickets(tickets)
      tickets.map { |ticket| new_from_ticket(ticket) }
    end

    # True optimistic locking would be ideal, but we'd need to monkey patch
    # ActiveRecord to be able to conditionally enable it.
    def hydrate_tickets_for_automation(dehydrated_tickets, automation)
      by_nice_id = Hash[dehydrated_tickets.map { |t| [t.nice_id, t] }]

      tickets = automation.
        account.
        tickets.
        where(nice_id: by_nice_id.keys).
        select do |ticket|
          dehydrated = by_nice_id[ticket.nice_id]
          dehydrated.current_with?(ticket) ||
            dehydrated.still_matches_automation?(automation)
        end

      Rails.logger.info(
        "Tickets that still match automation conditions after rehydration | automation id: #{automation.id}, # tickets: #{tickets.size}, tickets by id: #{tickets.map(&:id).join(', ')}"
      )

      tickets
    end
  end

  # The resque architecture relying on as_json method of each item in arguments.
  # I.E., when these Struct’s are getting serialized,
  # the as_json is called on each one of them. so it is necessary to make sure
  # all they keys in Struct are included in here
  def as_json(*_args)
    to_a
  end

  def current_with?(ticket)
    generated_timestamp >= ticket.generated_timestamp.to_i
  end

  # This incurs a small cost of a making a http trip to occam.
  def still_matches_automation?(automation)
    options = { caller: OCCAM_CALLER_NAME, timeout: 10, ids_only: true }

    result = automation.find_tickets_with_ids(
      nil,
      options,
      Array(id)
    ).tap do |res|
      Rails.logger.info "Checking if DehydratedTicket#still_matches_automation?(#{automation.id}): #{res}"

      statsd_client.increment(
        'dehydrated_ticket_changed',
        tags: ["still_matches_automation:#{res&.first == id}"]
      )
    end

    result.include?(id)
  end

  def statsd_client
    Zendesk::StatsD.client(namespace: 'automation_executor')
  end
end
