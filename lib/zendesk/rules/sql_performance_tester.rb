# Multiple times: run each query ten times, pick median (duration)
# ```
# Zendesk::Rules::SqlPerformanceTester
#   .new(account, sorting: true, run_multiple_times: true)
#   .run
# ```
#
# ```
# Zendesk::Rules::SqlPerformanceTester.new(account, result_size: 20000).run
# ```
# Recommended running the query killer (script/rule_watch),
# queries taking over 10s will be killed and 10 will be saved as duration.
module Zendesk::Rules
  class SqlPerformanceTester
    MULTIPLE_RUNS_COUNT = 10

    class LogWrapper
      attr_accessor :captured_sql

      def initialize(logger)
        @logger = logger
        @captured_sql = []
      end

      def method_missing(method, *args)
        @logger.send(method, *args)
      end

      def clear_captures
        @captured_sql = []
      end

      # Remember interesting SQL statements logged via 'debug'
      def debug(str)
        super
        if (match = /(SELECT `tickets`\.\* .* LIMIT \d+)/m.match(str))
          @captured_sql << match[1]
        end
      end

      def debug?
        true
      end
    end

    def initialize(account, options = {})
      @account            = account
      @result_size        = options.fetch(:result_size, 30)
      @sorts              = options.fetch(:sorting, true)
      @sort_combos        = options.fetch(:sort_combos, false)
      @constraints        = options.fetch(:constraining, true)
      @constraint_combos  = options.fetch(:constraint_combos, false)
      @pause_for_warmup   = options.fetch(:pause_for_warmup, true)
      @query_method = options[:run_multiple_times] ? method(:run_view_multiple_times) : method(:run_view)

      @log_wrapper = LogWrapper.new(ActiveRecord::Base.logger)

      ActiveRecord::Base.logger = @log_wrapper
    end

    def run
      time_started = Time.now
      puts "Running: #{time_started}"
      @account.on_shard do
        file = Tempfile.new("ticket-view-sql-#{@account.subdomain}.csv")
        CSV.open(file.path, 'wb') do |csv|
          csv << [
            'key',
            'operator',
            'sort',
            'group',
            'duration',
            'count',
            'query'
          ]

          write_csv(csv)
        end
        puts "\n\n#{file.path}"
      end
      time_finished(time_started)
    end

    def time_finished(time_started)
      time = (Time.now - time_started)
      st = 'seconds'
      if time > 60
        time /= 60
        st = 'minutes'
      end
      if time > 60
        time /= 60
        st = 'hours'
      end
      puts "Took #{time} #{st} to finish."
    end

    def known_fields_map
      ZendeskRules::RuleField.instance_variable_get(:@data)
    end

    def known_fields
      known_fields_map.values.uniq
    end

    def get_views # rubocop:disable Naming/AccessorMethodName
      views = []

      # The columns for some fields are ambiguous. If they're included with
      # other fields which cause joins to other tables, the database cannot
      # tell which column was intended.
      # E.g. `locale_id` appears in both the tickets and users tables.
      skip_secondary_order_ids = [:locale_id]
      skip_secondary_order_fields = skip_secondary_order_ids.
        map { |id| known_fields_map[id] }

      known_fields.each do |field|
        # Simple view that just selects column
        simple_view = View.new
        simple_view.output = Output.create(
          type:    'table',
          columns: [field.identifier]
        )
        simple_view.definition = Definition.new
        simple_view.account = @account
        views << simple_view

        # Order by the column, and on other columns if testing combos
        if @sorts
          secondary_orders = ((@sort_combos && !skip_secondary_order_ids.include?(field.identifier)) ? known_fields : []) + [nil]

          secondary_orders -= skip_secondary_order_fields
          secondary_orders.each do |secondary_order|
            view = create_order_view(
              field,
              [field, secondary_order].compact.uniq
            )

            views << view if view
          end
        end

        # Constrain on the column, if supported. And on other columns,
        # if testing combos.
        next unless @constraints

        primary_definition_items = [get_definition_item(field), nil]
        secondary_definition_items = if @constraint_combos
          known_fields.
            map { |f| get_definition_item(f) }
        else
          []
        end
        secondary_definition_items << nil

        primary_definition_items.each do |primary_definition_item|
          secondary_definition_items.each do |secondary_definition_item|
            view = create_constraint_view(
              field,
              [primary_definition_item, secondary_definition_item].compact.uniq
            )

            views << view if view
          end
        end
      end

      views
    end

    def create_order_view(field, order_fields)
      order_view = View.new
      output_info = {
        type:      'table',
        columns:   [field.identifier],
        order:     order_fields[0].identifier,
        order_asc: true
      }

      if order_fields.length > 1
        output_info[:group] = order_fields[1].identifier
        output_info[:group_asc] = true
      end

      order_view.output = Output.create(output_info)
      order_view.definition = Definition.new
      order_view.account = @account
      order_view
    end

    def create_constraint_view(field, constraint_items)
      return if constrain_items.empty?

      View.new.tap do |constrain_view|
        constrain_view.output = Output.create(
          type:    'table',
          columns: [field.identifier]
        )
        constrain_view.definition = Definition.new
        constrain_view.definition.conditions_all.concat constraint_items
        constrain_view.account = @account
      end
    end

    def write_csv(csv)
      get_views.each do |view|
        csv << @query_method.call(view)
      end
    end

    def run_view_multiple_times(view, execution = {})
      run_view(view, execution, warm_cache: true) # warm mysql cache
      sleep 0.5 if @pause_for_warmup
      output    = run_view(view, execution)
      duration  = [output[4]]

      # rubocop:disable Style/BlockDelimiters
      (MULTIPLE_RUNS_COUNT - 1).times.each {
        duration << run_view(view, execution)[4]; puts "duration: #{duration}"
      }
      # rubocop:enable Style/BlockDelimiters

      output[4] = duration.sort[4]
      puts "\n\n=== #{output[0]} ===\n#{output.slice(1, output.size)}\n\n==="
      output
    end

    def run_view(view, execution = {}, _options = {})
      t = Time.now
      begin
        user = @account.agents.first
        @log_wrapper.clear_captures

        Zendesk::Rules::RuleExecuter.
          find_tickets(view, user, paginate: true, per_page: @result_size)

        duration = Time.now - t
        count = Zendesk::Rules::RuleExecuter.count_tickets(view, user)
      rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::LostConnection
        duration = 10 # It means 10+
        count = '-'
      end
      sql = @log_wrapper.captured_sql[0]
      generate_output(view, duration, count, sql, execution)
    end

    def generate_output(view, duration, count, query, _execution = {})
      # BUG: this was here but never used ?
      # if execution.keys.first == "sort"
      #   sort = execution["sort"]["id"]
      # elsif execution.keys.first == "group"
      #   group = execution["group"]["id"]
      # end

      unless view.definition.conditions_all.empty?
        operator = view.definition.conditions_all[0].operator
      end

      [
        view.output.columns[0],
        operator,
        view.output.order,
        view.output.group,
        duration,
        count,
        query
      ]
    end

    def get_definition_item(field)
      case field.identifier
      when :status
        build_definition(:status_id, 'less_than', StatusType.CLOSED)
      when :subject
        build_definition(:description_includes_word, 'includes', 'test')
      when :requester
        build_definition(:requester_id, 'is', 'assignee_id')
      when :assignee
        build_definition(:assignee_id, 'is', 'requester_id')
      when :priority
        build_definition(:priority_id, 'is', 2)
      when :group
        build_definition(:group_id, 'is', 'current_groups')
      when :type
        build_definition(:ticket_type_id, 'is', 2)
      when :organization
        build_definition(:organization_id, 'is', 2)
      when :created
        build_definition(:NEW, 'is', 2)
      when :updated
        build_definition(:updated_at, 'is', 2)
      when :solved
        build_definition(:SOLVED, 'is', 2)
      when :due_date
        build_definition(:due_date, 'is', 2)
      when :assigned
        build_definition(:assigned_at, 'is', 2)
      when :requester_updated_at
        build_definition(:requester_updated_at, 'is', 2)
      when :assignee_updated_at
        build_definition(:assignee_updated_at, 'is', 2)
      when :current_tags
        build_definition(:current_tags, 'includes', 'test')
      when :brand
        build_definition(:brand_id, 'is', 2)
      when :sla_next_breach_at
        build_definition(:until_sla_next_breach_at, 'less_than', '50')
      end
    end

    def build_definition(source, operator, value)
      DefinitionItem.build(source: source, operator: operator, value: value)
    end
  end
end
