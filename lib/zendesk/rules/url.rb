module Zendesk::Rules
  class Url
    RULE_TYPES = Set['views', 'triggers', 'automations']

    # Type detection per action:
    #
    # default
    #   Rule.find(id).type
    #
    # index, destroy_inactive, sort
    #   params[:filter] || 'views'
    #
    # new
    #   params[:id] || params[:filter] || 'automations'
    #
    # create
    #   params[:rule][:type]
    #
    def initialize(env)
      @env     = env
      @request = Rack::Request.new(env)
    end

    def supported_rule_type?
      RULE_TYPES.member?(rule_type)
    end

    def new_path
      if supported_rule_type?
        current_path.gsub('rules', "rules/#{rule_type}")
      else
        current_path
      end
    end

    def rule_type
      type = params[:filter] ||
             rule_action_type ||
             rule_param_type ||
             record_type

      type.to_s.pluralize.downcase
    end

    protected

    def rule_action_type
      'views' if params[:action] == 'search'
    end

    def rule_param_type
      params[:rule] && params[:rule][:type]
    end

    def record_type
      return unless params[:id]

      if current_account
        rule = current_account.all_rules.find_by_id(params[:id])
        rule && rule.type.pluralize
      else
        Rails.logger.info('RuleRouting: account not present, skipping')

        nil
      end
    end

    def params
      @params ||= Zendesk::Application.routes.recognize_path(current_path).merge(@request.params).with_indifferent_access
    end

    def current_path
      @env['PATH_INFO']
    end

    def current_account
      @env['zendesk.account']
    end
  end
end
