module Zendesk::Rules::CmsReferences
  REFERENCE_ACTIONS = %w[
    comment_value
    comment_value_html
    notification_user
    notification_group
    notification_target
    tweet_requester
  ].freeze

  def create_cms_references
    return unless definition && definition_changed?

    initial_references = rule_references

    @new_references = []
    definition.actions.each do |action|
      set_references(action) if REFERENCE_ACTIONS.include?(action.source)
    end

    initial_references.each do |reference|
      reference.destroy if @new_references.exclude?(reference)
    end
  end

  private

  def set_references(action) # rubocop:disable Naming/AccessorMethodName
    return unless [Array, String].include?(action.value.class)

    Array(action.value).each do |value|
      next if value.class != String

      create_references(
        Zendesk::Liquid::Scanner.scan(Liquid::Template.parse(value), 'dc')
      )
    end
  end

  def create_references(identifiers)
    identifiers.each do |identifier|
      text = account.cms_texts.find_by_identifier(identifier)
      return if text.nil?

      new_reference = text.references.find_by_rule_id(id)

      new_reference ||=
        rule_references.create!(item: text, account: account, rule: self)

      @new_references << new_reference
    end
  end
end
