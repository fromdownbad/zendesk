require 'ostruct'

module Zendesk
  module Rules
    class UserViewInitializer
      ALLOWED_TYPES = ['User', 'Group'].freeze

      class InvalidArgument < ArgumentError; end

      attr_reader :params

      def initialize(account, params, key)
        @account = account
        @params = params
        @key = key
        normalize_params
      end

      def user_view
        @user_view ||= find_or_build_view
      end

      private

      def normalize_params
        return unless @params[@key]

        params = @params[@key]
        temp_params = params.extract!(:restriction, :active, :all, :any)

        restriction = temp_params[:restriction]
        if restriction.blank?
          params[:owner] = @account
        else
          params[:owner_id]   = restriction[:id].to_i
          params[:owner_type] = normalize_restriction_type(
            restriction[:type].to_s
          )
        end

        params[:is_active] = temp_params[:active] if temp_params.key?(:active)

        conditions_all = map_conditions(temp_params[:all]) if temp_params[:all]
        conditions_any = map_conditions(temp_params[:any]) if temp_params[:any]

        params[:output] = map_output(
          params.delete(:execution) || HashWithIndifferentAccess.new
        )

        return unless conditions_all || conditions_any

        params[:definition] = map_definition(conditions_all, conditions_any)
      end

      def normalize_restriction_type(type)
        type = type.classify
        msg  = ::I18n.t(
          'txt.api.v2.user_views.restriction.errors.type',
          type: type
        )

        fail InvalidArgument, msg unless ALLOWED_TYPES.include?(type)

        type
      end

      def map_output(output)
        OpenStruct.new.tap do |o|
          o.columns = default_columns
          # TODO: remove `output[:sort][:id] != :id` after migration to remove
          # sort by user id from saved views runs in production.
          if output.key?(:sort) && output[:sort][:id].to_s != 'id'
            o.sort = map_clause(output[:sort])
          end

          o.group   = map_clause(output[:group]) if output.key?(:group)
          o.columns = output[:columns] if output.key?(:columns)
        end
      end

      def map_clause(clause)
        {}.tap do |h|
          h[:id] = clause[:id]
          h[:order] = clause[:order]
        end
      end

      def default_columns
        [:name, :email, :created_at]
      end

      def map_definition(conditions_all, conditions_any)
        OpenStruct.new.tap do |d|
          d.conditions_any = conditions_any || []
          d.conditions_all = conditions_all || []
          d.actions        = []
        end
      end

      def map_conditions(conditions)
        return [] unless conditions

        conditions.map do |condition|
          OpenStruct.new.tap do |c|
            c.source = condition.delete(:field)
            c.operator = condition.delete(:operator)
            c.value = condition.delete(:value)
          end
        end
      end

      def find_or_build_view
        if @params[:id]
          @account.user_views.find(@params[:id].to_i).tap do |view|
            view.attributes = (@params[@key] || {})
          end
        else
          view = (@params[@key] || {}).
            except(:restriction)
          @account.user_views.build(view)
        end
      end
    end
  end
end
