class Zendesk::Rules::SmsNotificationAction
  def initialize(ticket:, user:, rule:, body:, phone_number_id:)
    @ticket          = ticket
    @user            = user
    @rule            = rule
    @phone_number_id = phone_number_id
    @body            = body
  end

  def self.valid_definition_item?(item)
    # Item is an DefinitionItem class object, @value is an array with
    # the following data @value=["current_user", 10007, ""]
    # interpret it as [user, phone_number_id, text_message_body]
    # None of phone number or body should be empty
    %w[notification_sms_user notification_sms_group].include?(item.source) &&
      Array(item.value).delete_if(&:blank?).size == 3
  end

  def apply(recipients)
    recipients = Array(recipients)
    if suppressed = suppressed_recipients(recipients).presence
      recipients -= suppressed

      ticket.audit.set_metadata 'notifications_suppressed_for', suppressed
    end

    if recipients.any?
      ticket.audit.events << build_notification(recipients)
    end
  end

  private

  attr_reader :ticket, :user, :rule, :body, :phone_number_id

  def build_notification(recipients)
    notification = SmsNotification.new(
      recipients: recipients,
      body:       body,
      audit:      ticket.audit
    )
    notification.via_id           = rule.via_reference[:via_id]
    notification.via_reference_id = rule.via_reference[:via_reference_id]
    notification.phone_number_id  = phone_number_id
    notification
  end

  def suppressed_recipients(recipients)
    recipients_with_setting('suspended', [true, 'true'], recipients) |
      trigger_machine_recipients(recipients) |
      system_recipients(recipients)
  end

  def trigger_machine_recipients(recipients)
    return [] unless rule.is_a?(Trigger) && user.machine?

    recipients_with_setting('machine', true, recipients)
  end

  def system_recipients(recipients)
    recipients.select { |id| id == User.system_user_id }
  end

  def recipients_with_setting(name, value, recipients)
    UserSetting.
      where(
        account_id: ticket.account_id,
        name:       name,
        value:      value,
        user_id:    recipients
      ).
      pluck(:user_id)
  end
end
