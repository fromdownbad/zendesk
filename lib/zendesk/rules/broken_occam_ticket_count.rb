module Zendesk
  module Rules
    ##
    # When Count Many Views feature is disabled we need to render a response
    # to avoid having clients(including Lotus) retrying indefinitely.
    # This is a value object with same defaults that indicate a count
    # couldn't be retrieved. It follows the same response conventions adopted
    # by blocked view counts and Lotus already knows how to handle it.
    #
    class BrokenOccamTicketCount < OccamTicketCount
      POLL_WAIT = 10.minutes.to_i

      def initialize(id)
        super(
          id: id,
          count: ::View::BROKEN_COUNT,
          fresh: true,
          updated_at: Time.now.to_s,
          refresh: 'poll',
          poll_wait: POLL_WAIT
        )
      end
    end
  end
end
