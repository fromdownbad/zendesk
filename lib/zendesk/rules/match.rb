module Zendesk::Rules::Match
  WORDS_TOKENIZER = /[^,\s]+/.freeze
  IMMUTABLE_SOURCES = Set.new [
    'agent_stations',
    'comment_includes_word',
    'comment_is_public',
    'current_via_id',
    'group_stations',
    'in_business_hours',
    'recipient',
    'reopens',
    'replies',
    'requester_id',
    'requester_twitter_followers_count',
    'requester_twitter_statuses_count',
    'requester_twitter_verified',
    'role',
    'satisfaction_score',
    'subject_includes_word',
    'ticket_is_public',
    'update_type',
    'via_id',
    'collaboration_thread'
  ]

  ASIAN_SCRIPTS_PATTERN =
    /\p{Han}|\p{Katakana}|\p{Hiragana}|\p{Hangul}/.freeze

  ASIAN_SCRIPTS_PATTERN_WITH_THAI =
    /\p{Han}|\p{Katakana}|\p{Hiragana}|\p{Hangul}|\p{Thai}/.freeze

  NEGATION_OPERATORS = %w[is_not not_includes].freeze

  def self.included(base)
    base.extend(ClassMethods)
  end

  # Test if a ticket matches the rule conditions. Returns true/false.
  def self.match?(rule, ticket, events = nil, run_state = nil)
    condition_match?(rule, ticket, events, run_state) == :match
  end

  # Test if a ticket matches the rule conditons.
  # Returns either of :match, :mismatch, :permanent_mismatch
  def self.condition_match?(rule, ticket, events, run_state)
    unless rule.definition.conditions_all.blank?
      negating_condition = rule.
        definition.
        sorted_conditions_all.
        detect do |condition|
          !instance_matches_condition?(ticket, condition, events, run_state)
        end

      if negating_condition
        mismatch_type = :mismatch

        if IMMUTABLE_SOURCES.include?(negating_condition.sanitized_source)
          mismatch_type = :permanent_mismatch
        end

        trigger_log(rule, ticket, mismatch_type, negating_condition)
        return mismatch_type
      end
    end

    matching_condition = rule.definition.conditions_any.blank? ? :blank : nil
    if matching_condition ||= rule.definition.sorted_conditions_any.detect { |c| instance_matches_condition?(ticket, c, events, run_state) }
      trigger_log(rule, ticket, :match, matching_condition)
      return :match
    end

    trigger_log(rule, ticket, :mismatch)
    :mismatch
  end

  def self.instance_matches_condition?(ticket, condition, events, run_state)
    ticket_matches_condition?(ticket, condition, events, run_state)
  end

  def self.ticket_matches_condition?(ticket, condition, events, run_state)
    if run_state && condition.memoizable?
      run_state.memoize(condition.memoization_key(ticket)) do
        Rule.match_condition?(ticket, condition, run_state, events)
      end
    else
      run_state.increment_checks if run_state
      Rule.match_condition?(ticket, condition, run_state, events)
    end
  end

  def self.trigger_log(rule, ticket, status, condition = nil)
    return unless ticket.account.has_trigger_logging?
    message = "{ account: #{ticket.account_id}, timestamp: #{Time.now.to_json}, trigger: #{rule.id}, ticket_id: #{ticket.id}, nice_id: #{ticket.nice_id}, status: #{status.to_json}"

    message << ", condition: #{condition.to_json}" if condition
    message << '}'

    TRIGGER_LOG.info(message)
  end

  def match?(ticket, events = nil, run_state = nil)
    Zendesk::Rules::Match.match?(self, ticket, events, run_state)
  end

  def condition_match?(ticket, events, run_state)
    Zendesk::Rules::Match.condition_match?(self, ticket, events, run_state)
  end

  module ClassMethods
    def log_dead_code
      line_number = caller[0].sub(/.*:(\d+):in.*/, '\1')

      Rails.logger.info("presumed-dead-code-lives @ match.rb:#{line_number}")
    end

    # Test if a ticket matches a rule condition.
    def match_condition?(ticket, condition, run_state, events = nil)
      source   = condition.sanitized_source
      operator = condition.operator.to_s.downcase

      value =
        case condition.value
        when Integer
          condition.value.to_s
        when Array
          # Preserve strange behavior in 1.8 and 1.9. It's unclear whether we
          # need arrays in condition values at all, though.
          condition.value.join
        when String
          condition.value
        when nil
          ''
        else
          condition.value.to_s
        end

      current_user = ticket.current_user
      events ||= []

      match_result = nil
      match_time = Benchmark.realtime do
        match_result =
          case source
          when 'comment_is_public'
            match_comment_is_public(ticket, value)
          when 'ticket_is_public'
            match_ticket_is_public(ticket, value)
          when 'subject_includes_word'
            match_subject_includes_word(ticket, operator, value)
          when 'comment_includes_word'
            match_comment_includes_word(ticket, operator, value, run_state)
          when 'role'
            match_role(ticket, operator, value, current_user)
          when 'requester_role'
            match_role_id(operator, value, ticket.requester)
          when 'update_type'
            match_update_type(ticket, value)
          when 'recipient'
            match_recipient(ticket, value)
          when 'current_tags'
            match_current_tags(ticket, operator, value)
          when /^ticket_fields_/
            match_ticket_fields(ticket, source, operator, value)
          when /(.+)\.custom_fields\.(.+)/
            match_custom_fields(ticket, $1, $2, operator, value)

          when 'in_business_hours'
            match_in_business_hours(ticket, value)
          when 'on_holiday'
            match_on_holiday(ticket, value)
          when 'within_schedule'
            match_within_schedule(ticket, value, run_state)
          when 'description_includes_word'
            log_dead_code
            match_description_includes_word(ticket, operator, value)
          # Automation conditions end ########################
          when 'exact_created_at'
            match_created_at(ticket, operator, value)
          when 'number_of_incidents'
            return false if ticket.ticket_type_id != TicketType.PROBLEM

            compare_target_and_value(operator, ticket.count_incidents, value)
          when 'received_from'
            match_on_sharing_agreement(ticket, value, direction: :in)
          when 'sent_to'
            match_on_sharing_agreement(ticket, value, direction: :out)
          when 'via_subtype_id'
            match_on_via_subtype(ticket, operator, value)
          when 'collaboration_thread'
            match_collaboration_thread(ticket, value)
          else # Change/Create conditions
            match_change_create_conditions(
              ticket,
              events,
              source,
              operator,
              value,
              current_user
            )
          end
      end

      if run_state&.instance_of?(Zendesk::Rules::Trigger::RunState)
        run_state.increment_match_by_source(source, match_time) if ticket.account.has_instrument_trigger_matching?
      end
      match_result
    end

    def text_includes_words?(text, words, account)
      words = words.to_s.scan(WORDS_TOKENIZER)
      words.any? { |word| text =~ regex_for(text, word, account) }
    end

    private

    def regex_for(text, word, account)
      if word.ascii_only? && text_ascii_only?(text, account)
        # For ascii words we match on normal \W word boundaries
        %r{(^|\W)#{Regexp.escape(word)}($|\W)}
      elsif word&.match?(asian_scripts_pattern_for(account))
        # Matches without word boundaries for languages without spaces
        %r{#{Regexp.escape(word)}}
      else
        # For words with special characters we must use POSIX word boundaries
        %r{(^|[^[:word:]])#{Regexp.escape(word)}($|[^[:word:]])}
      end
    end

    def text_ascii_only?(text, account)
      return true unless account.has_rule_match_non_ascii_characters?

      text.ascii_only?
    end

    def asian_scripts_pattern_for(account)
      # If the arturo is enabled Thai is excluded from partial matching words
      if account.has_pattern_matching_without_spaces_for_thai?
        ASIAN_SCRIPTS_PATTERN
      else
        ASIAN_SCRIPTS_PATTERN_WITH_THAI
      end
    end

    def string_to_boolean(str)
      str == 'true'
    end

    def match_ticket_is_public(ticket, value)
      value == (ticket.is_public? ? 'public' : 'private')
    end

    def match_comment_is_public(ticket, value)
      if comment = current_comment(ticket)
        case value
        when 'not_relevant'
          true
        when 'requester_can_see_comment'
          ticket.requester_is_agent? || comment.is_public?
        else
          comment.is_public == string_to_boolean(value)
        end
      end
    end

    module StatsD
      def self.client
        @@client ||= Zendesk::StatsD::Client.new(namespace: 'science')
      end
    end

    def match_subject_includes_word(ticket, operator, value)
      subject_match?(ticket, operator, value) { ticket.subject.present? }
    end

    def match_comment_includes_word(ticket, operator, value, run_state)
      comment_match?(
        current_comment(ticket),
        operator,
        value,
        run_state,
        account: ticket.account
      ).public_send(
        NEGATION_OPERATORS.include?(operator) ? :& : :|,
        subject_match?(ticket, operator, value) do
          ticket.new_record? && ticket.subject.present?
        end
      )
    end

    def comment_match?(comment, operator, value, run_state, account:)
      if comment.present?
        match_text?(
          matchable_body(comment, run_state),
          operator,
          value,
          account: account
        )
      else
        NEGATION_OPERATORS.include?(operator)
      end
    end

    def subject_match?(ticket, operator, value)
      if yield
        match_text?(ticket.subject, operator, value, account: ticket.account)
      else
        NEGATION_OPERATORS.include?(operator)
      end
    end

    def matchable_body(comment, run_state)
      if run_state&.instance_of?(Zendesk::Rules::Trigger::RunState) && Arturo.feature_enabled_for?(:memoizing_matchable_body, comment.account)
        run_state.memoized_objects[:matchable_body] ||= begin
          process_matchable_body(comment)
        end
      else
        process_matchable_body(comment)
      end
    end

    def process_matchable_body(comment)
      comment.rich? ? CGI.unescapeHTML(comment.plain_body) : comment.body.to_s
    end

    def match_text?(text, operator, condition_value, account:)
      normalized_text  = text.downcase
      normalized_value = condition_value.downcase.strip

      case operator
      when 'is'
        normalized_text.include?(normalized_value)
      when 'is_not'
        !normalized_text.include?(normalized_value)
      when 'includes'
        text_includes_words?(normalized_text, normalized_value, account)
      when 'not_includes'
        !text_includes_words?(normalized_text, normalized_value, account)
      else
        text_includes_words?(normalized_text, normalized_value, account)
      end
    end

    def match_role(ticket, operator, value, current_user)
      return false if current_user.is_system_user? && operator != 'is_not'

      is_agent = current_user.is_agent?
      is_end_user = current_user.is_end_user?

      if Arturo.feature_enabled_for?(:agent_as_end_user, ticket.account) &&
         ticket.agent_as_end_user_for?(current_user)
        is_agent = false
        is_end_user = true
      end

      match = (value == 'agent' && is_agent) ||
              (value == 'end_user' && is_end_user) ||
              (value.to_i == current_user.id)
      match = !match if operator == 'is_not'

      match
    end

    def match_role_id(operator, value, user)
      role_id = value.to_i

      match = role_id == Role::AGENT.id && user.is_agent? ||
              role_id == Role::END_USER.id && user.is_end_user? ||
              role_id == Role::ADMIN.id && user.is_admin?

      operator == 'is_not' ? !match : match
    end

    def match_update_type(ticket, value)
      match = ticket.new_record?
      match = !match if value == 'Change'

      match
    end

    def normalize_recipient(account, email)
      return nil if email.blank?

      email = email.strip.downcase

      return email if email.include?('@')

      zendesk_host = account.host_name(mapped: false)
      "#{email}@#{zendesk_host}"
    end

    # recipient or original_recipient_address include any value or
    # value@subdomain
    #
    # TODO this is inconsistent with rule-query-builder
    # - supports "," separated values
    # - supports matching both recipient or original_recipient_address at
    # once
    def match_recipient(ticket, value)
      values = value.downcase.split(',')

      set = [ticket.recipient, ticket.original_recipient_address].
        map { |v| v.to_s.strip.downcase }

      values.any? do |_v|
        set.include?(normalize_recipient(ticket.account, value))
      end
    end

    def match_current_tags(ticket, operator, value)
      current_tags = " #{ticket.current_tags} "

      match = value.
        downcase.
        split.
        detect { |word| current_tags.include?(" #{word} ") }

      match = !match if operator == 'not_includes'

      match
    end

    def match_ticket_fields(ticket, source, operator, value)
      #  (in the non-checkbox case) this comes in as
      #  "ticket_fields_123456", "is, "654321"
      #  where 654321 is a custom_field_option id that's selected.
      #  then
      #     a = CustomFieldOption.find(654321)
      #     b = [ticket's custom field option]
      #     a.value == b.value
      #
      # and I guess this is comparison is done to support custom field options
      # with duplicate values?
      # we don't allow that in the UI, so just comparing if the ids match
      # seems saner....
      #
      # Some edge cases:
      # 1. If certain field gets deleted, match result should always be false;
      # 2. If one option in a field gets deleted, it returns false.
      field_id = source.gsub('ticket_fields_', '').to_i

      field_entry = ticket.
        ticket_field_entries.
        detect { |t| t.ticket_field_id == field_id }

      field_entry_value = field_entry ? field_entry.value : ''

      ticket_field = (
        field_entry &&
        field_entry.ticket_field
      ) || ticket.account.ticket_fields.detect { |f| f.id == field_id }

      return false unless ticket_field

      if ticket_field.is_a?(FieldDate) || ticket_field.is_a?(FieldMultiselect)
        # operators are defined within the ticket field compare
        ticket_field.compare(field_entry_value, operator, value)
      else
        case operator
        # FieldTagger present/not_present
        when 'present'
          field_entry_value.present?
        when 'not_present'
          field_entry_value.blank?
        # FieldTagger / FieldCheckbox
        when 'is'
          ticket_field_equal?(ticket, ticket_field, field_entry, value)
        when 'is_not'
          !ticket_field_equal?(ticket, ticket_field, field_entry, value)
        else
          false
        end
      end
    end

    def ticket_field_equal?(ticket, ticket_field, field_entry, value)
      field_entry_value = field_entry ? field_entry.value : ''

      if ticket_field.is_a? FieldCheckbox
        Zendesk::DB::Util.truthy?(value.to_s) ==
          Zendesk::DB::Util.truthy?(field_entry_value)
      else
        if value.blank?
          field_entry_value.blank?
        elsif field_entry && (custom_field_option_value = ticket.account.fetch_custom_field_option_value(value))
          field_entry_value == custom_field_option_value
        else
          false
        end

      end
    end

    # rubocop:disable Naming/PredicateName
    def is_date_custom_field?(ticket_field)
      ticket_field.is_a?(FieldDate)
    end
    # rubocop:enable Naming/PredicateName

    def match_custom_fields(ticket, source, source_field, operator, value)
      conditions = {key: source_field, owner: 'User'}
      components = source.split('.', 2)
      if ['requester', 'submitter', 'assignee'].include?(components[0])
        # checking here to enforce existence of ticket.[user] prior to
        # deferred organization check below
        return false unless entity = ticket.send(components.shift)

      end

      if components[0] == 'organization'
        entity = (entity || ticket).organization

        conditions[:owner] = 'Organization'
      end

      # TODO: what if entity is not set yet? return operator == 'is_not'?
      return false unless entity

      field_entry = entity.custom_field_values.value_for_key(source_field)
      field_value = field_entry &&
                    !field_entry.marked_for_destruction? &&
                    field_entry.value

      field = if field_value
        field_entry.field
      else
        ticket.account.custom_fields.where(conditions).first
      end

      # TODO: log that field was not found, and so not compared against?
      match = (field ? field.compare(field_value, operator, value) : false)

      match
    end

    def match_in_business_hours(ticket, value)
      value == 'true' ? ticket.in_business_hours? : !ticket.in_business_hours?
    end

    def match_on_holiday(ticket, value)
      value == 'true' ? ticket.on_holiday? : !ticket.on_holiday?
    end

    def match_within_schedule(ticket, value, run_state)
      return false unless ticket.account.has_multiple_schedules?

      schedule = process_schedule(ticket, value, run_state)

      return false unless schedule.present?

      schedule.in_hours?(Time.now)
    end

    def process_schedule(ticket, value, run_state)
      if Arturo.feature_enabled_for?(:memoizing_schedules_in_triggers, ticket.account) && run_state&.instance_of?(Zendesk::Rules::Trigger::RunState)
        run_state.memoized_objects[:schedules] ||= {}

        run_state.memoized_objects[:schedules][value] ||= ticket.account.schedules.active.find_by_id(value)
      else
        ticket.account.schedules.active.find_by_id(value)
      end
    end

    def match_created_at(ticket, operator, value)
      # For new tickets, created_at is nil so use Time.now instead
      target = ticket['created_at'] || Time.now
      target = target.change(sec: 0, usec: 0)

      other_value = Time.use_zone(ticket.account.time_zone) do
        Time.zone.parse(value)
      end

      other_value = other_value.change(sec: 0, usec: 0)

      compare_target_and_value(operator, target, other_value)
    end

    def match_on_via_subtype(ticket, operator, value)
      value_match = value.match(/^([^:]+):(\d+)/)

      other_via_ids = {
        'TWITTER' => [
          Zendesk::Types::ViaType.TWITTER,
          Zendesk::Types::ViaType.TWITTER_DM,
          Zendesk::Types::ViaType.TWITTER_FAVORITE
        ],
        'FACEBOOK' => [
          Zendesk::Types::ViaType.FACEBOOK_POST,
          Zendesk::Types::ViaType.FACEBOOK_MESSAGE
        ],
        Zendesk::Types::ViaType.ANY_CHANNEL.to_s => [
          Zendesk::Types::ViaType.ANY_CHANNEL
        ]
      }[value_match[1]]

      if other_via_ids
        other_via_reference_id = value_match[2].to_i

        is_a_match = other_via_ids.include?(ticket.via_id) &&
                     (ticket.via_reference_id == other_via_reference_id)
      else
        is_a_match = false
      end

      case operator
      when 'is'
        is_a_match
      when 'is_not'
        !is_a_match
      end
    end

    def match_change_create_conditions(
      ticket,
      events,
      source,
      operator,
      value,
      current_user
    )
      other_value =
        if ['requester_id', 'assignee_id', 'submitter_id', 'group_id'].include?(value)
          ticket[value].to_s # .send(value).to_s
        elsif value == 'current_user'
          current_user ? current_user.id.to_s : '-1'
        else
          value
        end

      match = if ['is', 'is_not', 'less_than', 'greater_than'].include?(operator)
        match_operators(ticket, events, source, operator, value, other_value)
      else
        match_remaining_operators(events, source, operator, other_value)
      end

      match
    end

    def match_operators(ticket, events, source, operator, value, other_value)
      if source == 'current_via_id'
        target = if value.to_i == ViaType.RULE
          if ticket.audit && ticket.audit.via?(:rule)
            ticket.audit.via_id
          else
            events.any? ? events.first.via_id : nil
          end
        else
          ticket.audit ? ticket.audit.via_id : nil
        end
      elsif ['replies', 'reopens', 'agent_stations', 'group_stations'].include?(source)
        if ticket.ticket_metric_set
          ticket.ticket_metric_set.dirty_ticket = ticket
          target = ticket.ticket_metric_set.send("current_#{source}".to_sym)
        else
          target = 0
        end
      elsif source.include?('requester_twitter')
        target = ticket.send(source)
      elsif source.include?('locale_id')
        return true unless ticket.account.has_individual_language_selection?

        target = ticket.requester.locale_id || ticket.account.locale_id
      elsif source == 'satisfaction_score'
        return false unless ticket.account.has_customer_satisfaction_enabled?

        target = ticket.satisfaction_score
      elsif source == 'satisfaction_reason_code'
        return false unless ticket.account.csat_reason_code_enabled?

        target = ticket.satisfaction_reason_code
      elsif source == 'status_id'
        target = StatusType.order.index(ticket.status_id)
        other_value = StatusType.order.index(other_value.to_i).to_s
      elsif source == 'ticket_form_id'
        target = ticket.ticket_form_id ||
                 ticket.account.ticket_forms.default.first.id
      elsif source == 'brand_id'
        target = ticket.brand_id || ticket.account.default_brand_id
      elsif source == 'schedule_id'
        return false unless ticket.account.has_multiple_schedules?

        target = ticket.ticket_schedule.try(:schedule_id)
      elsif source == 'requester_time_zone'
        target = ticket.requester.time_zone
      elsif source == 'ticket_due_date'
        target = ticket.due_date.present?
      elsif source == 'satisfaction_probability'
        return false unless ticket.satisfaction_probability

        # Multiply satisfaction predictions (floats accurate to six digits) by
        # 10^6 to give comparisons accurate to six digits when these floats are
        # converted to integers in compare_target_and_value
        target = ticket.satisfaction_probability.to_f * 10**6
        other_value = other_value.to_f * 10**6
      elsif source == 'via_id'
        target, other_value = via_id_target_and_value(ticket, value)
      else
        target = ticket[source]
      end

      compare_target_and_value(operator, target, other_value)
    end

    def via_id_target_and_value(ticket, value)
      value_match = value.match(/^any_channel:(\d+)/)
      if ticket.via_id == ::Zendesk::Types::ViaType.ANY_CHANNEL && value_match
        target, value = any_channel_target_and_value(ticket, value_match)
      else
        target = ticket.via_id
      end

      [target, value]
    end

    def any_channel_target_and_value(ticket, value_match)
      target = ticket.via_reference_id
      # The ID in the value is the ID of the RIS, but the via_reference_id is
      # the ID of the ISI
      isi_constraint = {
        registered_integration_service_id: value_match[1].to_i,
        account_id:                        ticket.account_id
      }

      isi_ids = ::Channels::AnyChannel::IntegrationServiceInstance.
        where(isi_constraint).pluck(:id)

      value = isi_ids.include?(target) ? target.to_s : nil

      [target, value]
    end

    def compare_target_and_value(operator, target, value)
      case operator
      when 'is'
        target.to_s == value
      when 'is_not'
        target.to_s != value
      when 'less_than'
        target.to_i < value.to_i
      when 'less_than_equal'
        target.to_i <= value.to_i
      when 'greater_than'
        target.to_i > value.to_i
      when 'greater_than_equal'
        target.to_i >= value.to_i
      end
    end

    def match_remaining_operators(events, source, operator, other_value)
      target = events.reverse.detect { |c| c.value_reference == source }

      case operator
      when 'changed'
        target
      when 'not_changed'
        !target
        # changed_to and changed_from conditions
      when 'value'
        target && target.value.to_s == other_value
      when 'value_previous'
        target && target.value_previous.to_s == other_value
        # not_changed_to and not_changed_from conditions
      when 'not_value'
        !target || target.value.to_s != other_value
      when 'not_value_previous'
        !target || target.value_previous.to_s != other_value
      end
    end

    def match_collaboration_thread(ticket, value)
      event_klass = case value
                    when 'created'
                      CollabThreadCreated
                    when 'closed'
                      CollabThreadClosed
                    when 'reopened'
                      CollabThreadReopened
                    when 'reply'
                      CollabThreadReply
                    else
                      return false
      end

      ticket.audit.events.find { |event| event.class == event_klass }.present?
    end

    def match_on_sharing_agreement(ticket, value, direction:)
      agreements = ticket.shared_tickets.map(&:agreement)

      ticket.audit.events.each do |e|
        if e.is_a? TicketSharingEvent
          agreements << e.agreement
        end
      end

      agreements.uniq.any? do |agreement|
        if agreement.nil?
          Rails.logger.warn("Trigger is invoking nonexistent sharing agreement for account #{ticket.account}, ticket #{ticket.id}")

          next
        end
        agreement.id.to_s == value && (
          (agreement.in? && direction == :in) ||
          (agreement.out? && direction == :out)
        )
      end
    end

    def current_comment(ticket)
      if comment = ticket.comment
        comment if comment.body.present? || comment.attachments.any?
      end
    end

    # Checks if a ticket date property is in a defined interval
    def interval(ticket, source, operator, value, field_value_override = nil)
      return false if value.blank?

      field_value = field_value_override || ticket.send(source)

      return false if field_value.blank?

      if operator.include?('_business_hours') && ticket.account.present?
        value = ticket.calendar_hours_ago(value.to_i)

        operator.gsub!('_business_hours', '')
      end

      case operator
      when 'is'
        start_time = (value.to_i + 1).hours.ago
        end_time = value.to_i.hours.ago + 1.second

        field_value < end_time && field_value > start_time
      when 'less_than'
        field_value >= value.to_i.hours.ago
      when 'greater_than'
        field_value < value.to_i.hours.ago
      end
    end
  end
end
