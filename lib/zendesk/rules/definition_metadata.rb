module Zendesk
  module Rules
    class DefinitionMetadata
      SMS_NUMBERS_ENDPOINT = '/api/v2/channels/voice/phone_numbers'.freeze

      COLLECTION_KEYS = {'organization_id' => 'organizations'}.freeze
      ITEM_KEYS       = {'organization_id' => 'organization'}.freeze

      private_constant :COLLECTION_KEYS,
        :ITEM_KEYS,
        :SMS_NUMBERS_ENDPOINT

      def initialize(account, user)
        @account = account
        @user    = user
      end

      def item_key(subject)
        ITEM_KEYS[subject]
      end

      def collection_key(subject)
        COLLECTION_KEYS[subject]
      end

      def phone_numbers(*)
        @phone_numbers ||= begin
          sms_numbers.map do |number|
            PhoneNumber.new(number['id'], build_name_string(number))
          end
        end
      end

      protected

      attr_reader :account,
        :user

      private

      def sms_numbers
        @sms_numbers ||= begin
          voice_client.
            get(SMS_NUMBERS_ENDPOINT, sms_enabled: true, minimal_mode: true).
            body['phone_numbers']
        rescue StandardError => exception
          Rails.logger.error(
            "Error fetching `#{SMS_NUMBERS_ENDPOINT}`: #{exception.message}"
          )

          []
        end
      end

      def voice_client
        @voice_client ||= begin
          Zendesk::Voice::InternalApiClient.
            new(account.subdomain, timeout: 10.seconds)
        end
      end

      def build_name_string(phone_number)
        nickname, number = phone_number.values_at('nickname', 'display_number')

        if nickname.present?
          "#{nickname} #{number}"
        else
          number
        end
      end

      PhoneNumber = Struct.new(:value, :title)

      private_constant :PhoneNumber
    end
  end
end
