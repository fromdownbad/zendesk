module Zendesk
  module Rules
    class ViewsRateLimiter
      LIMITS = {
        count_many: {
          handle: :views_limit_count_many,
          threshold: 6,
          interval: 5.minutes,
          first_throttled: true,
          description: 'Number of allowed count many requests exceeded'
        },
        view_find: {
          handle: :views_limit_find_requests,
          threshold: 5,
          interval: 1.minute,
          first_throttled: true,
          description: 'Number of allowed view requests per minute exceeded'
        },
        view_count: {
          handle: :views_limit_view_count_request,
          threshold: 5,
          interval: 1.minute,
          first_throttled: true,
          description: 'Number of allowed view counts per minute exceeded'
        },
        preview_find: {
          handle: :views_limit_preview_find_requests,
          threshold: 5,
          interval: 1.minute,
          first_throttled: true,
          description: 'Number of allowed preview requests per minute exceeded'
        },
        preview_count: {
          handle: :views_limit_preview_count_requests,
          threshold: 5,
          interval: 1.minute,
          first_throttled: true,
          description: 'Number of allowed preview count requests per minute exceeded'
        },
        view_play: {
          handle: :views_limit_play_requests,
          threshold: 5,
          interval: 1.minute,
          first_throttled: true,
          description: 'Number of allowed view play mode requests per minute exceeded'
        },
        view_tickets: {
          handle: :views_limit_tickets_requests,
          threshold: 5,
          interval: 1.minute,
          first_throttled: true,
          description: 'Number of allowed view tickets requests per minute exceeded'
        }
      }.with_indifferent_access.freeze

      attr_reader :handle, :account, :user, :resource_id, :origin, :first_throttled

      def self.limiter_for(type, account, user, origin, resource_id = nil)
        new(handle_for(type), account, user, origin, resource_id)
      end

      def self.handle_for(type)
        LIMITS.dig(type, :handle)
      end

      def self.definition_for(type)
        LIMITS[type].dup
      end

      def self.configure(*rate_limits)
        rate_limits.each do |type|
          definition = Zendesk::Rules::ViewsRateLimiter.definition_for(type)
          limit = definition.delete(:handle)
          Prop.configure(limit, definition)
        end
      end

      def initialize(handle, account, user, origin, resource_id = nil)
        @handle           = handle
        @account          = account
        @user             = user
        @resource_id      = resource_id
        @first_throttled  = nil
        @origin           = origin
      end

      def to_s
        [
          "#<ViewsRateLimiter handle:#{handle}",
          "account: #{account&.id}",
          "user: #{user&.id}",
          "resource_id: #{resource_id}>"
        ].join(', ')
      end

      ##
      # Without enforcing rate limits, tracks what would have been a breach.
      # @raise Prop::RateLimited if limits are being enforced
      #   by arturo.
      #
      def throttle!
        return if exclude_from_view_limits?

        if raise_rate_limited?
          Prop.throttle!(handle, key)
        elsif (throttle_result = Prop.throttle(handle, key))
          first = throttle_result == :first_throttled
          report(first)
        end
      end

      ##
      # Provides post limiting instrumentation.
      # @param first_throttled [Boolean]
      # Doesn't allow unexpected errors on rate limiting to block the request.
      #
      def report(first_throttled)
        @first_throttled = first_throttled
        add_tracer_tags
        track_metrics
        log_rate_limited
      rescue StandardError => reporting_error
        ZendeskExceptions::Logger.record(
          reporting_error,
          location: self,
          message: to_s,
          fingerprint: '46f6b5c0da5e79432f286151fa23d2b63d73bh01'
        )
      end

      def raise_rate_limited?
        return false if origin.lotus? && !account.has_views_rate_limit_lotus?
        return false if origin.zendesk_mobile_client?

        account.has_views_enforce_rate_limits?
      end

      ##
      # In development mode Lotus headers are not sent over so
      # requests are recognised as API requests.
      # Since we don't want to limit Lotus we're excluding
      # development env here.
      #
      def exclude_from_view_limits?
        Rails.env.development? || !Arturo.feature_enabled_for?(handle, account)
      end

      private

      def track_metrics
        statsd_client.increment(
          'rate_limited',
          tags: [
            "handle:#{handle}",
            "first_throttled:#{first_throttled}",
            "request_source:#{origin.request_source}",
            "status:#{status}"
          ]
        )
      end

      def status
        raise_rate_limited? ? 429 : 200
      end

      def add_tracer_tags
        return unless active_span

        active_span.set_tag('zendesk.rate_limited.handle', handle)
        active_span.set_tag('zendesk.rate_limited.count', request_count)
        active_span.set_tag('zendesk.rate_limited.first_throttled', first_throttled)
      end

      def request_count
        Prop.count(handle, key)
      end

      def key
        @key ||= [account.id, user.id].tap do |array|
          array.push(resource_id) if resource_id
        end
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'views')
      end

      def active_span
        @active_span ||= Datadog.tracer.active_span
      end

      def log_rate_limited
        info = {
          account_id: account.id,
          subdomain: account.subdomain,
          user_id: user.id,
          handle: handle,
          first_throttled: first_throttled,
          request_source: origin.request_source
        }.map { |k, v| "#{k}:#{v}" }.join(', ')

        Rails.logger.warn("[views][rate-limit] API Rate Limited for #{info}")
      end
    end
  end
end
