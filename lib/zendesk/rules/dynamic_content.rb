module Zendesk
  module Rules
    module DynamicContent
      private

      def render_dc_in_custom_field(fields, account, user, dc_cache)
        fields.each do |field|
          field.title = Zendesk::Liquid::DcContext.
            render(field.title, account, user, 'text/plain', nil, dc_cache)

          next unless field.respond_to?('custom_field_options')

          field.custom_field_options.each do |custom_field|
            custom_field.name = Zendesk::Liquid::DcContext.render(
              custom_field.name,
              account,
              user,
              'text/plain',
              nil,
              dc_cache
            )
          end
        end
      end
    end
  end
end
