module Zendesk
  module Rules
    class Initializer
      ALLOWED_TYPES = %w[Account Group User].freeze

      attr_reader :params

      def initialize(account, scope, params, key)
        @account = account
        @scope   = scope
        @params  = params.dup
        @key     = key

        @rule_params = params.fetch(key, {})

        normalize_params if rule_params.any?
      end

      def rule
        @rule ||= begin
          (params.key?(:id) ? found_rule : built_rule).tap do |rule|
            if rule_params[:group_id].present?
              rule.group_id = rule_params[:group_id]
            end

            rule.prepare_for_editing!

            rule.group_owner_ids = owner_ids if group_rule?(rule)
          end
        end
      end

      protected

      attr_reader :account,
        :scope,
        :key,
        :rule_params,
        :restriction

      private

      def normalize_params
        if rule_params.key?(:restriction)
          if @restriction = rule_params.delete(:restriction)
            rule_params[:owner_id]   = owner_id
            rule_params[:owner_type] = owner_type
          else
            rule_params[:owner] = account
          end

        elsif !params[:id]
          rule_params[:owner] = account
        end

        unless rule_params[:active].nil?
          rule_params[:is_active] = rule_params.delete(:active)
        end

        if category_id = rule_params.delete(:category_id)
          rule_params[:rules_category_id] = category_id if account.has_trigger_categories_api_enabled?
        end

        if rule_params[:actions]
          actions = map_definition_items(rule_params.delete(:actions))
        end
        if conditions = rule_params.delete(:conditions)
          rule_params[:all] ||= conditions[:all] || []
          rule_params[:any] ||= conditions[:any] || []
        end

        if rule_params[:all]
          conditions_all = map_definition_items(rule_params.delete(:all))
        end

        if rule_params[:any]
          conditions_any = map_definition_items(rule_params.delete(:any))
        end

        if (output = rule_params.delete(:output)) || !params[:id]
          rule_params[:output] = Output.default(output || {})
        end
        if actions || conditions_all || conditions_any
          rule_params[:definition] = begin
            Definition.build(
              actions:        actions,
              conditions_all: conditions_all,
              conditions_any: conditions_any
            )
          end
        end
      end

      def owner_id
        unless macro? || (view? && account.has_multiple_group_views_writes?)
          return restriction[:id]
        end

        owner_ids.first
      end

      def owner_type
        restriction[:type].classify.tap do |type|
          unless ALLOWED_TYPES.include?(type)
            fail ArgumentError, "Invalid restriction type #{type}"
          end
        end
      end

      def map_definition_items(conditions)
        conditions.each do |condition|
          Api::V2::Rules::ConditionMappings.map_definition_item!(
            account,
            condition
          )
        end
      end

      def owner_ids
        @owner_ids ||=
          restriction[:ids].present? ? restriction[:ids] : [restriction[:id]]
      end

      def group_rule?(rule)
        (macro? || (view? && account.has_multiple_group_views_writes?)) &&
          restriction && rule.owner_type == 'Group'
      end

      def macro?
        key == :macro
      end

      def view?
        key == :view
      end

      def found_rule
        find_rule.tap { |rule| rule.attributes = rule_params.except(:attachments) }
      end

      def find_rule
        params[:rule] || scope.find(params[:id])
      end

      def built_rule
        scope.build(rule_params.except(:attachments))
      end
    end
  end
end
