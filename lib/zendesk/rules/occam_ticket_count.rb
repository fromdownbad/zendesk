module Zendesk
  module Rules
    # Usually channel is only present if refresh is 'pubsub', and poll_wait is
    # only present if refresh is 'poll'
    OccamTicketCount = Struct.new(
      :rule_id,
      :value,
      :fresh,
      :updated_at,
      :refresh,
      :channel,
      :poll_wait
    ) do
      def initialize(
        id:,
        count:,
        fresh:,
        updated_at:,
        refresh:   nil,
        channel:   nil,
        poll_wait: nil
      )
        super(id, count, fresh, updated_at, refresh, channel, poll_wait)
      end

      def pretty_print
        return '...' if value.nil? || value == ::View::BROKEN_COUNT

        if age <= 1.minute || (age <= 5.minute && value < 10)
          value.to_s
        elsif age <= 5.minute || value < 10
          "~#{value}"
        elsif age <= 10.minutes || value < 100
          "~#{(value.to_f / 10).round * 10}"
        else
          "~#{(value.to_f / 100).round * 100}"
        end
      end

      def is_updating? # rubocop:disable Naming/PredicateName
        !fresh
      end

      private

      def age
        @age ||= updated_at ? Time.now - Time.parse(updated_at) : 24.hours
      end
    end
  end
end
