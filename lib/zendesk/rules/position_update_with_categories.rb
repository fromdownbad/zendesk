module Zendesk::Rules
  class PositionUpdateWithCategories
    def self.perform(context, updates)
      Zendesk::RuleSelection::Scope.for_reorder(context, ordering: :unsorted).each do |scope|
        new(scope, updates).perform(context.user.account)
      end
    end

    def initialize(scope, updates)
      @scope   = scope.active
      @updates = validate_updates(updates)
    end

    def perform(account)
      scope.update_position_for_categories(account, new_positions) if updates.any?
    end

    protected

    attr_reader :scope, :updates

    private

    def validate_updates(updates)
      updates_by_category = Hash.new { |hash, key| hash[key] = {} }

      updates.each do |update|
        next unless existing_rules.include?(update[:id])

        existing_rule = existing_rules[update[:id]]

        category_id = existing_rule[:rules_category_id]

        updates_by_category[category_id][update[:id]] = update[:position] || existing_rule[:position]
      end

      updates_by_category.transform_values { |value| value.invert.sort.to_h }
    end

    def new_positions
      @new_positions ||= begin
        updates.map do |category_id, updates_by_category|
          remaining_updates = updates_by_category.dup
          remaining_rules   = existing_rules_by_category[category_id].keys - updates_by_category.values

          [
            category_id,
            possible_positions(category_id).each_with_object([]) do |position, rules|
              next_rule = remaining_updates.delete(position) || remaining_rules.shift || remaining_updates.shift.last

              rules.push(existing_rules_by_category[category_id][next_rule] == position ? nil : next_rule)
            end
          ]
        end.to_h
      end
    end

    def possible_positions(category_id)
      1..existing_rules_by_category[category_id].length
    end

    def existing_rules
      @existing_rules ||= begin
        scope.each_with_object({}) do |rule, rules|
          rules.store(rule.id, {position: rule.position, rules_category_id: rule.rules_category_id, title: rule.title})
        end
      end
    end

    def existing_rules_by_category
      @existing_rules_by_category ||= begin
        scope.ordered.group_by(&:rules_category_id).transform_values do |rules_by_category|
          rules_by_category.each_with_object({}) do |rule, rules|
            rules.store(rule.id, rule.position)
          end
        end
      end
    end
  end
end
