module Zendesk
  module Rules
    class ChangeObject
      CHANGES = %w[= + -].freeze

      attr_reader :change, :content

      def initialize(change, content)
        unless CHANGES.include?(change)
          fail ArgumentError, "`change` must be one of: #{CHANGES.join(', ')}"
        end

        @change  = change
        @content = content
      end

      def ==(other)
        return false unless other.is_a?(self.class)

        change == other.change && content == other.content
      end

      def to_h
        {change: change, content: content}
      end
    end
  end
end
