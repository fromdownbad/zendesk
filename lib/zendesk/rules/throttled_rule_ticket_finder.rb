require 'digest/md5'

module Zendesk
  module Rules
    class ThrottledRuleTicketFinder
      PROP_KEY = :rule_execution_time_limit

      class << self
        def find(rule, user, query, options)
          if disabled?(options, rule)
            statsd_client.increment('fetch', tags: %w[mode:disabled])

            return fetch_tickets(rule, query, options)
          end

          unless throttled?(rule, user, options)
            statsd_client.increment('fetch', tags: ['mode:unthrottled'])

            return throttle(rule, user, options) do
              fetch_tickets(rule, query, options)
            end
          end

          cache_hit = true
          cached_ticket_ids = cache_and_mark_as_cached(rule, user, options) do
            cache_hit = false

            fetch_tickets(rule, query, options, ids_only: true).map(&:id)
          end

          statsd_client.increment(
            'fetch',
            tags: %W[mode:throttled cache_hit:#{cache_hit}]
          )

          tickets_from_ids = fetch_tickets(
            rule,
            query,
            options,
            additional_conditions: {id: cached_ticket_ids},
            allow_force_key:       false
          )

          tickets_from_ids.sort_by { |t| cached_ticket_ids.index(t.id) }
        end

        private

        def disabled?(options, rule)
          # api? = ['v1', 'v2', 'preview'].include?(params[:caller]) &&
          # !params[:lotus]
          (!options.api? || options.group.present? || rule.new_record?)
        end

        def fetch_tickets(
          rule,
          query,
          options,
          allow_force_key:       true,
          ids_only:              false,
          additional_conditions: nil
        )
          scope = rule.account.tickets

          scope = scope.
            where(query.to_sql).
            where(additional_conditions).
            joins(query.joins.join(' ')).
            limit(options.limit).
            order(query.order.join(', ')).
            offset(options.offset)

          scope = if ids_only || options.ids_only?
            scope.select('`tickets`.`id`')
          else
            scope.includes(options.includes)
          end

          if rule.can_find_tickets_on_archive?
            scope.all_with_archived
          elsif allow_force_key && rule.force_key.present? &&
                rule.valid_index_for_force_key?
            table = "tickets FORCE KEY(`#{rule.force_key}`)"
            scope.from(table).all
          else
            scope.all
          end
        end

        def cache_and_mark_as_cached(rule, user, options)
          rule.cached_by_throttled_rule_ticket_finder = true

          expires_in = Prop::Limiter.handles[PROP_KEY][:interval]

          Rails.
            cache.
            fetch(cache_key(rule, user, options), expires_in: expires_in) do
              rule.cached_by_throttled_rule_ticket_finder = false

              yield
            end
        end

        def throttle(rule, user, options, &block)
          result, time = measure(&block)

          Prop.throttle(
            PROP_KEY,
            throttle_key(rule, user, options),
            increment: (time * 1000).to_i
          )

          result
        end

        def measure
          result, time = benchmark { yield }
          statsd_client.histogram('execution_time', time)
          [result, time]
        end

        def benchmark
          result = nil
          time = Benchmark.realtime { result = yield }
          [result, time]
        end

        def throttled?(rule, user, options)
          options.offset.to_i.zero? && # first page
            rule.is_a?(::View) &&
            Prop.throttled?(PROP_KEY, throttle_key(rule, user, options))
        end

        def cache_key(rule, user, options)
          cache_key_options = {
            order: options.order,
            order_asc: options.order_asc?,
            since: options.since,
            limit: options.limit
          }
          "throttled_rule_ticket_finder-#{rule.account.id}-#{rule.id}-#{user.try(:id)}-#{Digest::MD5.hexdigest(cache_key_options.to_query)}"
        end
        alias_method :throttle_key, :cache_key

        def statsd_client
          @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'rules')
        end
      end
    end
  end
end
