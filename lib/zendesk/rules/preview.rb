module Zendesk::Rules
  class Preview
    class Results < Zendesk::Rules::RuleExecuter::Collection
      include Zendesk::Serialization::PreviewResultsSerialization
    end

    class Tickets < Zendesk::Rules::RuleExecuter::Collection
      include Zendesk::Serialization::PreviewTicketsSerialization
    end

    class ResultsOutOfRange < ActiveRecord::RecordNotFound; end

    attr_reader :user, :params, :rule

    OCCAM_REQUEST_TIMEOUT = 30.seconds

    def initialize(user, params, rule)
      @user   = user
      @params = params
      @rule   = rule
    end

    def was_missing_required_condition?
      !!@was_missing_required_condition
    end

    def tickets
      return collection.new(1, 1, 0) if results.nil?

      collection.new(
        results.current_page,
        results.per_page,
        results.total_entries
      ).tap do |tickets|
        tickets.replace(results)
      end
    end

    protected

    def collection
      if rule.new_record?
        Zendesk::Rules::Preview::Results
      else
        Zendesk::Rules::Preview::Tickets
      end
    end

    def results
      return @results if defined?(@results)

      prepare if rule.new_record?
      ensure_required_conditions_are_present

      unless rule.valid?
        @results = nil
        return
      end

      @results = rule.find_tickets(
        user,
        paginate: true,
        order:    params[:order],
        desc:     params[:desc],
        page:     get_page,
        caller:   'classic-preview-api-v1',
        timeout: OCCAM_REQUEST_TIMEOUT
      )

      verify_in_range!

      @results
    end

    def verify_in_range!
      raise(ResultsOutOfRange) if @results.out_of_range?
    end

    def ensure_required_conditions_are_present
      if rule.definition.missing_required_condition?
        rule.definition.add_status_less_than_solved_definition
        @was_missing_required_condition = true
      end
    end

    def prepare
      rule.id         = params[:id] if params[:id]
      rule.definition = definition
      rule.per_page   = API_DEFAULT_PER_PAGE

      # satisfy validity checks
      rule.title      = 'Unsaved rule' if rule.title.blank?
      rule.account  ||= @user.account
      rule.owner    ||= @user
      if rule.is_a?(Automation)
        rule.skip_validate_nullifying_actions!

        if rule.definition.actions.empty?
          rule.definition.actions << DefinitionItem.new('status_id', 'is', '1')
        end
      end

      # Run rule validations to make sure organization_name is mapped to its
      # ID, if present.
      rule.valid?

      rule
    end

    def definition
      Definition.build(definition_params)
    end

    def definition_params
      Zendesk::Rules::ControllerSupport::DefinitionParams.new(params)
    end

    def get_page # rubocop:disable Naming/AccessorMethodName
      params[:page].to_i <= 0 ? 1 : params[:page].to_i
    end
  end
end
