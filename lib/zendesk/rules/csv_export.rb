module Zendesk
  module Rules
    module CsvExport
      def csv_filename
        PermalinkFu.escape("#{title} #{Time.now.in_time_zone.to_s(:csv)}") + '.csv'
      end

      def csv_titles(options = {})
        csv_fields(options).map do |f|
          f.title || Rule::CSV_TITLES[f.identifier]
        end
      end

      def csv_fields(options = {})
        @csv_fields ||= (default_columns(options) + output.remove_irrelevant_columns_for(account)).map { |f| ZendeskRules::RuleField.lookup(f) }.compact.uniq
      end

      def tickets_to_csv(user, tickets)
        return nil if tickets.length > Rule.csv_threshold

        # fix+debug ZD#243083: duplicate tickets in csv
        if tickets.size > tickets.map(&:nice_id).uniq.size
          duplicates = tickets.
            group_by(&:nice_id).
            reject { |_k, v| v.size == 1 }.
            keys
          ZendeskExceptions::Logger.record(Exception.new, location: self, message: "Duplicate tickets in csv fixed #{account.id}: #{duplicates.inspect}", fingerprint: '6c9a171ab173f53f6e3b2df074e6a98925705fcc')

          tickets = tickets.uniq(&:nice_id)
        end

        CSV.generate(col_sep: ',', force_quotes: true) do |csv|
          csv << csv_titles
          build_csv(user, tickets).each do |row|
            csv << row
          end
        end
      end

      def build_csv(user, tickets)
        tickets.map do |ticket|
          csv_fields.map do |field|
            fetch_and_process_value(user, ticket, field)
          end
        end
      end

      def fetch_and_process_value(user, ticket, field)
        value = (field.identifier == :via && ticket.translated_via_type) ||
                field.humanized_value(ticket)

        value = Zendesk::Liquid::TicketContext.
          render(value, ticket, user, user.is_agent?, user.translation_locale)

        value = Zendesk::CsvScrubber.scrub_output(value)

        if value.is_a?(Time)
          value.in_time_zone.to_s(:csv)
        else
          value.to_s.to_str.squish.truncate(10000)

        end
      end

      def default_columns(options = {})
        [:nice_id] | Array(options[:default_columns])
      end
    end
  end
end
