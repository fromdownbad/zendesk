class Zendesk::Rules::AutomationTicketProcessor
  cattr_accessor(:max_automations_per_ticket) { 100 }
  cattr_accessor(:log_slow_per_ticket_apply) { 5.seconds }

  def initialize(automation:)
    @automation = automation
    @queued_audit_ids = []

    @apply_time = 0
    @changed    = 0
  end

  attr_reader :apply_time, :changed, :queued_audit_ids

  def process_tickets(tickets)
    tickets      = tickets.to_a
    tickets_size = tickets.size
    account      = tickets_size > 0 ? tickets.first.account : nil

    @apply_time = Benchmark.realtime do
      # `#pop` allows ruby to garbage collect while automations run, and not
      # blow out its memory footprint
      while ticket = tickets.pop
        next unless process_ticket?(ticket)

        Rails.logger.debug "Applying actions to Ticket ##{ticket.nice_id}"

        ZendeskAPM.trace(
          'automation.apply_actions',
          service: Rule::APM_SERVICE_NAME
        ) do |span|
          span.set_tag('zendesk.ticket.id', ticket.id)
          span.set_tag('zendesk.account_id', ticket.account_id)
          span.set_tag('zendesk.account_subdomain', ticket.account.subdomain)

          @automation.apply_actions(ticket)
        end

        # Save if any notifications/pushes were added or ticket had
        # attributes updated
        @changed += 1 if (ticket.audit.events.any? || ticket.delta_changes?) && ticket.save

      end
    end

    if tickets_size != @changed && !account.nil?
      Rails.logger.warn(
        "Automation execution | automation id: #{@automation.id}, account.has_parallel_automations?: #{account.has_parallel_automations?}, # tickets processed: #{tickets_size}, # tickets changed: #{@changed}"
      )
    end

    if @changed > 0
      apply_time_per_ticket = @apply_time / @changed
      statsd_client.histogram 'apply_time_per_ticket', apply_time_per_ticket
      if apply_time_per_ticket > log_slow_per_ticket_apply
        Rails.logger.warn("SLOW APPLY: Automation #{@automation.id} '#{@automation.title}' for account #{@automation.account.subdomain} took #{apply_time_per_ticket.round(3)} seconds on average to apply actions to ticket")

      end
    end
  end

  private

  def process_ticket?(ticket)
    if potential_automation_loop?(ticket)
      statsd_client.increment 'errors', tags: %w[error:high_audit_count]
      Rails.logger.info("Ticket #{ticket.id} was excluded from automation #{@automation.id} for account #{@automation.account_id} because its was already processed by automations too many times and may be looping")
      create_too_many_automation_audits_event(ticket)

      return false
    end

    if @automation.business_hours_conditions.any? &&
       business_hours_automation_already_applied?(ticket)
      statsd_client.increment 'errors', tags: %w[error:business_hours_already_applied]
      Rails.logger.info("Ticket #{ticket.id} was excluded from automation #{@automation.id} for account #{@automation.account_id} because it was already processed for the business hours condition")

      return false
    end

    if updated_at_condition_causing_loop?(ticket)
      statsd_client.increment 'errors', tags: %w[error:updated_at_causing_loop]
      Rails.logger.info("Ticket #{ticket.id} was excluded from automation #{@automation.id} for account #{@automation.account_id} because the updated_at condition was triggered by this automation")

      return false
    end

    true
  end

  # Too many audits may indicate the ticket is in an endless automation loop
  def potential_automation_loop?(ticket)
    fetch_automation_audit_count(ticket) > max_automations_per_ticket &&
      !@automation.actions_close_ticket?
  end

  def fetch_automation_audit_count(ticket)
    Rails.cache.fetch("#{ticket.account_id}/tickets/#{ticket.id}/automation_audits/count", expires_in: 24.hours) do
      ActiveRecord::Base.with_slave { ticket.audits.where(via_id: ViaType.RULE).count(:all) }
    end
  end

  def create_too_many_automation_audits_event(ticket)
    last_audit = ticket.audits.latest
    last_event = last_audit.events.last
    unless last_event.is_a?(::TranslatableError)
      ticket.will_be_saved_by(User.system)
      ticket.add_translatable_error_event(
        key: 'event.via_rule_too_many_automation_audits'
      )
      ticket.save
    end
  end

  # Don't fire an automation more than once in a non-business-hour period, if
  # the automation has a "(business) is" condition. This should eliminate an
  # automation sending notifications all weekend, as the condition
  # 'is X business hours' can be true throughout the whole non-biz-hour period
  # for edge cases.
  def business_hours_automation_already_applied?(ticket)
    previous_audits = ticket.audits.on_slave.by_automation(@automation)
    previous_audits.any? && ticket.business_minutes_diff(previous_audits.latest.created_at, Time.now.utc) < 60
  end

  # If the automation has an "updated at is X hours" condition, we don't want
  # this automation's changes to be the cause triggering itself again.
  def updated_at_condition_causing_loop?(ticket)
    if definition = @automation.updated_at_conditions.first
      # Was the updated_at updated by another event?
      !Automation.send(
        :interval,
        ticket,
        definition.source,
        definition.operator,
        definition.value.first,
        last_audit_not_for_this_automation(ticket).created_at
      )
    else
      false
    end
  end

  # Fetches the most recent ticket audit that wasn't:
  #  - created by a rule
  #  - created by itself
  def last_audit_not_for_this_automation(ticket)
    ticket.
      audits.
      where('(via_reference_id IS NULL OR via_reference_id != ?) AND via_id != ?', @automation.id, ViaType.RULE).
      last
  end

  def statsd_client
    @statsd_client ||=
      Zendesk::StatsD.client(namespace: 'automation_ticket_processor')
  end
end
