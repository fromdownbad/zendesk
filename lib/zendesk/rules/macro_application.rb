module Zendesk::Rules
  class MacroApplication
    delegate :account, :actions, to: :macro

    attr_reader :macro, :ticket, :user, :output

    def initialize(macro, user, ticket = nil)
      @macro  = macro
      @user   = user
      @ticket = ticket
      @output = {}
    end

    def run
      build_attributes
      clean_output_keys
      apply_changes_to_ticket if ticket
      build_subject
      build_comment
      build_html_comment

      output
    end

    protected

    def build_attributes
      filtered_actions.each do |action|
        value = action.first_value

        case action.source
        when 'comment_mode_is_public'
          comment_mode_is_public(value)
        when 'current_tags', 'set_tags', 'remove_tags'
          build_tags(action, value)
        when 'cc'
          update_followers_or_collaborators(action, value)
        when 'side_conversation'
          update_side_conversation(action)
        when 'side_conversation_slack'
          update_side_conversation_slack(action)
        when 'side_conversation_ticket'
          update_side_conversation_ticket(action)
        when /^ticket_fields_/
          build_ticket_fields(action, value)
        when /^assignee_id/
          assign(action, value)
        when /^brand_id/
          build_brand(action, value)
        else
          build_attribute(action, value)
        end
      end
    end

    def filtered_actions
      actions - [*basic_comment_actions, *subject_actions, html_comment_action]
    end

    def update_side_conversation(action)
      subject, message, recipients, content_type = action.value

      content_type ||= 'text/plain'

      rich_comment = content_type == 'text/html'

      processed_recipients = Zendesk::Liquid::TicketContext.render(recipients, ticket, user, true, locale)
      processed_subject = Zendesk::Liquid::TicketContext.render(subject, ticket, user, true, locale)
      processed_message = Zendesk::Liquid::TicketContext.render(
        message,
        ticket,
        user,
        true,
        locale,
        Mime[:text],
        rich_comment: rich_comment
      )

      output['side_conversation'] ||= {}
      output['side_conversation']['recipients'] = processed_recipients
      output['side_conversation']['subject'] = processed_subject
      output['side_conversation']['message'] = processed_message
      output['side_conversation']['content_type'] = content_type
    end

    def update_side_conversation_slack(action)
      message, channel, content_type = action.value

      content_type ||= 'text/plain'

      rich_comment = content_type == 'text/html'

      processed_channel = Zendesk::Liquid::TicketContext.render(channel, ticket, user, true, locale)
      processed_message = Zendesk::Liquid::TicketContext.render(message, ticket, user, true, locale, Mime[:text], rich_comment: rich_comment)

      output['side_conversation_slack'] ||= {}
      output['side_conversation_slack']['channel'] = processed_channel
      output['side_conversation_slack']['message'] = processed_message
      output['side_conversation_slack']['content_type'] = content_type
    end

    def update_side_conversation_ticket(action)
      subject, message, recipient, content_type = action.value

      content_type ||= 'text/plain'

      rich_comment = content_type == 'text/html'

      processed_message = Zendesk::Liquid::TicketContext.render(message, ticket, user, true, locale, Mime[:text], rich_comment: rich_comment)
      processed_subject = Zendesk::Liquid::TicketContext.render(subject, ticket, user, true, locale)

      output['side_conversation_ticket'] ||= {}
      output['side_conversation_ticket']['recipient'] = recipient
      output['side_conversation_ticket']['subject'] = processed_subject
      output['side_conversation_ticket']['message'] = processed_message
      output['side_conversation_ticket']['content_type'] = content_type
    end

    def update_followers_or_collaborators(action, value)
      return if account.has_follower_and_email_cc_collaborations_enabled? &&
                !account.has_ticket_followers_allowed_enabled?

      user = if value.to_s.casecmp('current_user').zero?
        @user
      else
        account.users.find_by_id(value.to_i)
      end

      # deleted or missing user
      return unless user

      collaborator = {}
      collaborator[:id] = user.id

      if account.has_follower_and_email_cc_collaborations_enabled?
        if account.has_ticket_followers_allowed_enabled?
          output['followers_list'] ||= []
          output_target = output['followers_list']

          collaborator[:following] = (ticket.try(:followers) || []).any? do |c|
            c.id == user.id
          end
        end
      else
        output['collaborator_list'] ||= []
        output_target = output['collaborator_list']

        collaborator[:cced] = (ticket.try(:collaborations) || []).any? do |c|
          c.user_id == user.id
        end
      end

      # duplicate user in the macro
      return if output_target.any? { |c| c[:id] == user.id }

      # Names are included for Lotus error messages.
      collaborator[:name] = action.lookup_name(account.agents, user.id)

      output_target << collaborator
    end

    # ZD#703989 subject rendering and special case handling, with the reasoning
    # that our customers are irrational and expect irrational behavior
    def build_subject
      subject_actions.each do |subject_action|
        value = subject_action.first_value

        output['subject'] = if ticket
                              # also setting ticket subject in case it's
                              # referenced later by a comment
          ticket.subject = Zendesk::Liquid::TicketContext.
            render(value, ticket, user, true, locale)
        else
          value
        end
      end
    end

    def build_comment
      basic_comment_actions.each do |comment_action|
        channel, value = channel_and_value(comment_action)

        result = rendered_comment(value, rich: false)

        output['comment'] ||= {}

        output['comment']['scoped_value'] ||= []
        output['comment']['scoped_value'] << [channel, result]
        if account.try(:has_macro_application_no_liquid?) && result != value
          output['comment']['scoped_value'] << ['channel:raw', value]
        end

        output['comment']['value'] ||= ''
        output['comment']['value'] << result
      end

      output
    end

    def build_html_comment
      return unless html_comment_action.present?

      output['comment'] ||= {}

      output['comment']['html_value'] = begin
        rendered_comment(html_comment_action.first_value, rich: true).auto_link
      end
    end

    def rendered_comment(comment, rich:)
      if ticket.nil? && account.has_dc_context_for_macro_application?
        return Zendesk::Liquid::DcContext.render(
          comment,
          account,
          user,
          Mime[:text],
          nil,
          {},
          rich
        )
      end

      return comment unless ticket.present?

      Zendesk::Liquid::TicketContext.render(
        comment,
        ticket,
        user,
        true,
        locale,
        Mime[:text],
        rendering_options(rich)
      )
    end

    def rendering_options(rich)
      if account.has_macro_render_dc_markdown?
        {render_dc_markdown: rich}
      else
        {rich_comment: rich}
      end
    end

    def channel_and_value(action)
      channel = action.first_value
      value   = action.second_value

      if value.nil?
        value   = channel
        channel = 'channel:all'
      end

      [channel, value]
    end

    def basic_comment_actions
      @basic_comment_actions ||= begin
        actions.select { |action| action.source == 'comment_value' }
      end
    end

    def html_comment_action
      @html_comment_action ||= begin
        actions.find { |action| action.source == 'comment_value_html' }
      end
    end

    def subject_actions
      @subject_actions ||= actions.select { |a| a.source == 'subject' }
    end

    def comment_mode_is_public(value)
      # If the user does not have permissions to make a public comment
      return if value == 'true' && !user.can?(:publicly, Comment)

      # If this is a new ticket, and they don't have FCP setting enabled,
      # then they have no 'Internal Note' comment tab
      return if ticket.try(:new_record?) && value == 'false' && !account.try(:is_first_comment_private_enabled)

      output['comment'] ||= {}
      output['comment']['is_public'] = value
    end

    def build_tags(action, value)
      return unless permit_build_tags?

      current_tags = (output['current_tags'] || ticket.try(:current_tags)).
        to_s.split(' ')

      value = value.split(' ')
      # Normalize tags - returns an array of well formed tags
      value = TagManagement.normalize_tags(value, account)

      if action.source == 'current_tags'
        value = current_tags + value
      elsif action.source == 'remove_tags'
        if ticket.nil? # bulk update
          output['remove_tags'] = value.join(' ')
        end

        value = current_tags - value
      end

      output['current_tags'] = value.join(' ')
    end

    def build_ticket_fields(action, value)
      ticket_field = account.
        ticket_fields.
        find_by_id(action.source.gsub('ticket_fields_', '').to_i)

      return unless ticket_field.try(:is_active?)

      value =
        case ticket_field
        when FieldMultiselect
          output[action.source] ||= []
          result = ticket_field.aggregate_and_add_new_value(value, ticket)
          (output[action.source] << result).flatten.uniq
        when FieldTagger
          account.fetch_custom_field_option_value(value) || ''
        when FieldCheckbox
          Zendesk::DB::Util.truthy?(value)
        end
      output[action.source] = value unless value.nil?
    end

    def assign(action, value)
      if value.to_s.casecmp('current_user').zero?
        output[action.source] = user.id
        new_user = user
      elsif value.to_s.casecmp('requester_id').zero?
        requester_id = ticket.try(:requester_id)
        output[action.source] = requester_id
        new_user = user.account.users.find_by_id(requester_id)
      else
        output[action.source] = value.presence
        new_user = user.account.users.find_by_id(value.to_i)
      end
      output['group_id'] ||= current_group_id(new_user)
    end

    def build_attribute(action, value)
      output[action.source] =
        case value.to_s.downcase
        when 'current_groups'
          current_group_id(user)
        when 'default_ticket_form'
          default_ticket_form_id
        else
          value
        end
    end

    def default_ticket_form_id
      user.account.ticket_forms.default.first.try(:id)
    end

    def build_brand(action, value)
      if account.active_brands.find_by_id(value)
        build_attribute(action, value)
      else
        build_attribute(action, account.default_brand_id)
      end
    end

    def current_group_id(user)
      return account.default_group.try(:id) unless account.has_groups?
      if user.blank?
        if ticket_has_group? &&
           account.settings.change_assignee_to_general_group?
          ticket.group.id
        end
      else
        if ticket_has_group? && user.in_group?(ticket.group)
          ticket.group.id
        else
          default_group = user.
            memberships.
            where(default: true).
            first.
            try(:group_id)

          default_group || user.groups.first.try(:id)
        end
      end
    end

    # Remove things like #all_agents from keys (leaving only proper attribute
    # names) Also remove bogus keys like PICKACTION
    def clean_output_keys
      @output = output.each_with_object({}) do |(key, value), cleaned|
        cleaned[key.gsub(/#.*$/, '')] = value if key != 'PICKACTION'
      end
    end

    # Convert ticket_field_<id> keys to things that can be added to a ticket
    # object.
    def ticket_field_entries
      ticket_fields = output.keys.select { |key| key =~ /^ticket_fields_\d+/ }

      ticket_fields.map do |key|
        {
          ticket_field_id: key[/^ticket_fields_(\d+)/, 1],
          value: output[key]
        }
      end
    end

    # check bulk_ticket update, class access(system users), ticket specific
    # access
    def permit_build_tags?
      ticket.nil? ||
        user.can?(:edit_tags, Ticket) ||
        user.can?(:edit_tags, ticket)
    end

    def apply_changes_to_ticket
      ticket.attributes = ticket.attributes.update(mass_assignable_output)
      # can't mass assign current_tags
      ticket.current_tags = output['current_tags'] if output['current_tags']
      ticket_field_entries.each do |entry_data|
        ticket.ticket_field_entries.build(entry_data)
      end
      ticket.assignee = nil if ticket.assignee_id == -1
    end

    def mass_assignable_output
      output.reject { |k, _v| k =~ /^(?:followers_list|collaborator_list|current_tags|side_conversation|side_conversation_slack|ticket_fields_\d+)$/ }
    end

    def locale
      ticket.requester ? ticket.requester.translation_locale : ticket.account.translation_locale
    end

    def ticket_has_group?
      ticket.present? && ticket.group.present?
    end
  end
end
