module Zendesk::Rules
  module Query
    def self.included(base)
      base.extend(ClassMethods)
      base.extend(Zendesk::Rules::OccamCompare)
    end

    module ClassMethods
      def generic_ticket_list(params)
        output = Output.create(
          group: :status,
          group_asc: true,
          columns: [
            :nice_id,
            :description,
            :requester,
            :created,
            :status,
            :group,
            :assignee
          ]
        )

        [
          output,
          "tickets.*, #{Zendesk::Fom::Field.for(:assignee).join_field_alias}, #{Zendesk::Fom::Field.for(:requester).join_field_alias}, #{Zendesk::Fom::Field.for(:group).join_field_alias}",
          [Zendesk::Fom::Field.for(:assignee).join, Zendesk::Fom::Field.for(:requester).join, Zendesk::Fom::Field.for(:group).join],
          (output.set_order(params) || Ticket::STATUS_SQL_SORT_BY)
        ]
      end
    end

    def is_time_based? # rubocop:disable Naming/PredicateName
      definition.try(:is_time_based?)
    end

    def cache_key_for(user)
      the_cache_key = cache_key
      unless user.nil?
        if definition.try(:user_specific?) || !user.agent_restriction?(:none)
          the_cache_key << '/' + user.cache_key_with_time
        elsif definition.try(:group_specific?)
          group_ids = user.groups.pluck(:id).sort.join('-')
          group_ids = Digest::MD5.hexdigest(group_ids) if group_ids.size > 32
          the_cache_key << "/groups-#{group_ids}"
        end
      end
      the_cache_key
    end

    def reset_per_page
      self.per_page = 15 if per_page != 30
    end

    # allow ad-hoc rule-previews from lotus to hit the archive, nothing else.
    def can_find_tickets_on_archive?
      new_record? && is_a?(::View) && include_archived_tickets && definition && account &&
        (definition.conditions_all && definition.conditions_all.size == 1) &&
        (!definition.conditions_any || definition.conditions_any.empty?) &&
        ['requester_id', 'assignee_id', 'organization_id'].include?(definition.conditions_all.first.source.to_s) &&
        definition.conditions_all.first.operator.to_s == 'is'
    end

    def find_tickets(current_user = nil, params = {})
      # Override output type?
      output.type = params[:output_type] unless params[:output_type].blank?
      includes = [*params[:include]]

      tics = []
      self.cached_by_throttled_rule_ticket_finder = false
      self.cached_by_hanlon = false

      find_tickets_with_tracking(params) do
        tics = Zendesk::Rules::RuleExecuter.
          find_tickets(self, current_user, params).tap do |tickets|
            Comment.map_latest_comment_to_tickets(tickets) if includes.include?(:latest_comment)
            ChatTranscript.map_last_message_to_tickets(tickets) if includes.include?(:latest_chat_message)
          end
      end

      tics
    end

    def find_tickets_with_ids(current_user = nil, params = {}, ticket_ids = [])
      # When ids are passed in, scope the query to run only on those tickets
      definition.conditions_all << DefinitionItem.new(:id, 'is', ticket_ids)
      find_tickets(current_user, params)
    ensure
      definition.conditions_all.pop
    end

    def find_tickets_with_occam(current_user = nil, params = {})
      find_tickets(current_user, params.merge(occam: true))
    end

    def watch(current_user = nil, params = {})
      executer = Zendesk::Rules::RuleExecuter.new(self, current_user, params)
      executer.watch
    end

    def unwatch(current_user = nil, params = {})
      executer = Zendesk::Rules::RuleExecuter.new(self, current_user, params)
      executer.unwatch
    end

    def watchable?(current_user = nil, params = {})
      executer = Zendesk::Rules::RuleExecuter.new(self, current_user, params)
      executer.watchability[:cacheable]
    end

    def find_tickets_with_tracking(params = {})
      res = []
      time = Benchmark.realtime { res = yield }

      auth = if params.key?(:basic_auth)
        (params[:basic_auth] ? 1 : 0)
      else
        ''
      end
      log_rule_execution_timing(
        'FindTickets', res.length, time,
        params[:caller].to_s + (params[:lotus] && '.lotus').to_s,
        basic_auth:    auth,
        cached:        cached_by_throttled_rule_ticket_finder ? 1 : 0,
        hanlon_cached: cached_by_hanlon ? 1 : 0,
        page:          params[:page],
        per_page:      params[:per_page],
        order:         output.try(:order).present? ? "#{output.order} #{output.sort_order}" : '',
        group:         output.try(:group).present? ? "#{output.group} #{output.group_order}" : '',
        db_host:       db_host
      )

      track_execution_metrics('find_tickets', time, res.length)
    end

    def track_execution_metrics(key, time, count)
      tags = statsd_tags(time)
      statsd_client = Zendesk::StatsD::Client.new(namespace: 'rules')
      statsd_client.histogram("#{key}.timing", time * 1000, tags: tags)
      statsd_client.count("#{key}.tickets_found", count, tags: tags)
      statsd_client.increment("#{key}_histogram", tags: tags)
      nil
    end

    # Time buckets to send to datadog as tags
    # * 0-50ms
    # * 50-150ms
    # * 150-500ms
    # * 500ms-5s
    # * +5s
    def time_bucket_tag(time)
      time_in_ms = time * 1000
      [0, 50, 150, 500, 1000, 2000, 5000].each_cons(2) do |start, limit|
        return "time_bucket:#{start}ms-#{limit}ms" if time_in_ms < limit
      end
      'time_bucket:5000ms-'
    end

    def statsd_tags(time)
      tags = ["preview:#{id.nil?}", "class:#{self.class}"]

      # Arturo flag rule_execution_metrics will be enabled only for a few
      # select accounts
      tags << "account:#{account.subdomain}" if account.has_rule_execution_metrics?

      cached = cached_by_throttled_rule_ticket_finder ? 'true' : 'false'
      hanlon = cached_by_hanlon ? 'true' : 'false'
      tags << "cached:#{cached}"
      tags << "hanlon_cached:#{hanlon}"

      tags << time_bucket_tag(time)

      tags
    end

    def db_host
      ActiveRecord::Base.connection_config[:host]
    end

    # Tries to find a ticket in a Rule by nice_id
    # Note: modifies the conditions, so be sure not to save
    def find_ticket(id, current_user = nil, params = {})
      definition.conditions_all << DefinitionItem.new(:nice_id, 'is', id)
      find_tickets(current_user, params.merge(limit: 1)).first
    ensure
      definition.conditions_all.pop
    end

    # ~80ms on uber
    def matches_ticket?(nice_id, current_user = nil, options = {})
      find_ticket(nice_id, current_user, options).present?
    end

    def ticket_count(user, options = {})
      CachedRuleTicketCount.lookup(self, user, options)
    end

    def count_tickets(current_user = nil, params = {})
      count = 0
      self.cached_by_hanlon = false

      time = Benchmark.realtime do
        count = Zendesk::Rules::RuleExecuter.
          count_tickets(self, current_user, params)
      end

      log_rule_execution_timing(
        'CountTickets', count, time, 'count',
        hanlon_cached: cached_by_hanlon ? 1 : 0,
        db_host: db_host
      )
      track_execution_metrics('count_tickets', time, count)

      count
    end

    ##
    # For debug purposes. rule.sql_string should return sql
    # generated by Classic or Occam.
    #
    # @param current_user [User] For views with `current_user`
    #   or `current_groups` conditions.
    # @param params [Hash] params to build execution options
    # @option params [Boolean] :occam if should use occam
    #   to fetch the sql string. Use `occam: false` to use Classic,
    #   otherwise current arturo state will point it to Occam.
    #
    def sql_string(current_user = nil, params = {})
      rule_executer = Zendesk::Rules::RuleExecuter.
        new(self, current_user, params)

      rule_executer.expected_sql_from_query
    end

    def sql_count_string(current_user = nil, params = {})
      rule_executer = Zendesk::Rules::RuleExecuter.
        new(self, current_user, params)

      rule_executer.expected_sql_count_from_query
    end

    def query_cache_key(user, params = {})
      params_exemptions = ['controller', 'action', 'format', 'id', 'rewind']

      # Conditions like SLAs would cause the key to change every time, so get
      # compiled query where time conditions are always the same value (i.e.
      # Time.now)
      compiled = Zendesk::Rules::RuleQueryBuilder.
        compiled_query_with_frozen_time_for(self, user)

      return 'invalid-conditions' if compiled.empty?

      params_exemptions.delete('id')

      key = "#{compiled.to_sql}|#{params.except(*params_exemptions).to_query}"
      Digest::MD5.hexdigest(key)
    end

    def validate_force_key
      return true if force_key.nil?

      return if valid_index_for_force_key?

      errors.add(:base, 'Invalid index added to rule.')
    end

    def valid_index_for_force_key?
      return true if force_key.nil?
      key = force_key.to_s
      key = key.delete_prefix('-')

      ActiveRecord::Base.
        connection.
        indexes('tickets').
        map(&:name).
        detect { |index| index == key }.
        present?
    end
  end
end
