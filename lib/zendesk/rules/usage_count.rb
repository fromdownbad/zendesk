module Zendesk
  module Rules
    class UsageCount
      def initialize(rule)
        @rule = rule
      end

      attr_accessor :hourly, :daily, :weekly, :monthly

      # rubocop:disable Lint/DuplicateMethods
      def hourly
        @hourly ||= execution_count(:hourly)
      end

      def daily
        @daily ||= execution_count(:daily)
      end

      def weekly
        @weekly ||= execution_count(:weekly)
      end

      def monthly
        @monthly ||= execution_count(:monthly)
      end
      # rubocop:enable Lint/DuplicateMethods

      protected

      attr_reader :rule

      private

      def execution_count(timeframe)
        Zendesk::RuleSelection::ExecutionCounter.new(
          account: rule.account,
          type:    rule.rule_type
        ).execution_count(rule, timeframe)
      end
    end
  end
end
