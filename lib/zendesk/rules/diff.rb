require 'simplediff-ruby'

module Zendesk
  module Rules
    class Diff
      attr_reader :source_id, :target_id

      def initialize(source:, target:, differ: SimpleDiff)
        @source = source
        @target = target
        @differ = differ

        @source_id = source.id
        @target_id = target.id
      end

      def title
        @title ||= attribute_diff(&:title)
      end

      def description
        @description ||= attribute_diff(&:description)
      end

      def active?
        @active ||= attribute_diff(&:is_active)
      end

      def conditions_any
        @conditions_any ||= definition_diff(conditions_map, :conditions_any)
      end

      def conditions_all
        @conditions_all ||= definition_diff(conditions_map, :conditions_all)
      end

      def actions
        @actions ||= definition_diff(actions_map, :actions)
      end

      private

      attr_reader :source, :target, :differ

      def diff
        differ.compare(Array.wrap(yield(source)), Array.wrap(yield(target)))
      end

      def definition_diff(aspect_map, aspect)
        diff do |rule|
          rule.definition.public_send(aspect).map do |definition_item|
            definition_item.to_h.merge(cache_id: definition_item.cache_key)
          end
        end.flat_map do |change| # rubocop:disable Style/MultilineBlockChain
          change[:content].map do |value|
            ChangeObject.new(change[:change], aspect_map[value[:cache_id]])
          end
        end
      end

      def attribute_diff(&block)
        diff(&block).map do |change|
          ChangeObject.new(change[:change], change[:content].first)
        end
      end

      def conditions_map
        @conditions_map ||= begin
          (source.definition.conditions_any +
            source.definition.conditions_all +
            target.definition.conditions_any +
            target.definition.conditions_all).
            each_with_object({}) do |condition, aspect_map|
              aspect_map[condition.cache_key] = condition
            end
        end
      end

      def actions_map
        @actions_map ||= begin
          (source.definition.actions +
            target.definition.actions).
            each_with_object({}) do |action, aspect_map|
              aspect_map[action.cache_key] = action
            end
        end
      end
    end
  end
end
