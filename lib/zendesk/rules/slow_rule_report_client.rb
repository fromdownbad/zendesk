module Zendesk
  module Rules
    class SlowRuleReportClient
      attr_reader :account_attr

      def initialize(account)
        @account_attr = account.attributes.slice('id', 'shard_id', 'subdomain')
      end

      def create_report(rule, limits)
        params = {
          account: account_attr,
          rule: rule_attributes(rule),
          limits: limits
        }.stringify_keys
        occam_client.create_report(params).tap do |response|
          create_audit(
            source: rule,
            action: 'create_rule_report',
            source_display_name: rule.title,
            message: "created report for rule #{rule.id}: #{response.to_json}"
          )
        end
      end

      def show_report(rule)
        params = {
          account: account_attr,
          rule: rule_attributes(rule)
        }.stringify_keys
        occam_client.show_report(params)
      end

      def show_many_report(rules)
        params = {
          account: account_attr,
          rules: rules.map { |rule| rule_attributes(rule) }
        }.stringify_keys
        occam_client.show_many_report(params)
      end

      def destroy_report(rule)
        params = {
          account: account_attr,
          rule: rule_attributes(rule)
        }.stringify_keys
        occam_client.destroy_report(params).tap do |response|
          create_audit(
            source: rule,
            action: 'destroy_rule_report',
            source_display_name: rule.title,
            message: "destroyed #{response['deleted_count']}" \
                      "record for rule #{rule.id}"
          )
        end
      end

      def destroy_all
        params = {
          account: account_attr
        }.stringify_keys
        occam_client.destroy_all(params).tap do |response|
          create_audit(
            source_id: -1,
            source_type: 'SlowRuleReport',
            action: 'destroy_all_rule_report',
            source_display_name: 'All Views',
            message: "#{response['deleted_count']} reports destroyed"
          )
        end
      end

      def all_reports
        params = {
          account: account_attr
        }.stringify_keys
        occam_client.all_reports(params)
      end

      private

      def occam_client
        @occam_client ||= Zendesk::Rules::OccamClient.new
      end

      def rule_attributes(rule)
        rule.attributes.slice('id', 'updated_at')
      end

      def create_audit(params = {})
        event_params = {
          account_id: account_attr['id'],
          actor: User.system,
          visible: false
        }.merge(params)
        CIA::Event.create!(event_params)
      end
    end
  end
end
