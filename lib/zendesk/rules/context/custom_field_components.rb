require 'zendesk_rules/component/context'

module Zendesk::Rules
  class Context < ZendeskRules::Component::Context
    module CustomFieldComponents
      include Zendesk::Rules::DynamicContent

      def ticket_custom_fields
        @ticket_custom_fields ||= begin
          list = @account.ticket_fields.custom.active.all
          list = custom_fields_with_dc(list)
          list || []
        end
      end

      def user_custom_fields
        @user_custom_fields ||= begin
          if @account.has_user_and_organization_fields?
            custom_fields = @account.custom_fields.for_user.active.to_a
            translate_system_fields!(custom_fields)
            custom_fields = custom_fields_with_dc(custom_fields)
            custom_fields || []
          else
            []
          end
        end
      end

      def organization_custom_fields
        @organization_custom_fields ||= begin
          if @account.has_user_and_organization_fields?
            list = @account.custom_fields.for_organization.active
            list = custom_fields_with_dc(list)
            list || []
          else
            []
          end
        end
      end

      # note that the following methods do *not* do dynamic content translation.
      # In case you needed that.

      def find_ticket_field(id)
        @cached_ticket_fields ||= {}
        memoize(@cached_ticket_fields, id) do
          @account.ticket_fields.custom.active.find_by_id(id)
        end
      end

      def find_user_custom_field(key)
        @cached_user_fields ||= {}
        memoize(@cached_user_fields, key) do
          @account.has_user_and_organization_fields? &&
            @account.custom_fields.for_user.active.find_by_key(key)
        end
      end

      def find_organization_custom_field(key)
        @cached_organization_fields ||= {}
        memoize(@cached_organization_fields, key) do
          @account.has_user_and_organization_fields? &&
            @account.custom_fields.for_organization.active.find_by_key(key)
        end
      end

      def accessed_fields
        {
          'ticket_custom_fields'       => map_fields(@cached_ticket_fields),
          'user_custom_fields'         => map_fields(@cached_user_fields),
          'organization_custom_fields' => map_fields(@cached_organization_fields)
        }
      end

      private

      def map_fields(hash)
        return [] unless hash.present?

        hash.values.compact.map(&method(:field_to_hash))
      end

      def field_to_hash(field)
        {
          'id'   => field.id,
          'type' => field.type.to_s
        }.tap do |hash|
          hash['key'] = field.key if field.respond_to?(:key)
          hash['tag'] = field.tag if field.respond_to?(:tag)
        end
      end

      def memoize(hash, key)
        return hash[key] if hash.key?(key)

        hash[key] = yield
      end

      def translate_system_fields!(fields)
        fields.map! do |field|
          field.title = ::I18n.t(field.title) if field.is_system?
          field
        end
      end

      def custom_fields_with_dc(list)
        return list if validating || executing

        render_dc_in_custom_field(list, @account, @user, @user.dc_cache)
      end
    end
  end
end
