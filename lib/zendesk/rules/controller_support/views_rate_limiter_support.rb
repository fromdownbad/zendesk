module Zendesk
  module Rules
    module ControllerSupport
      module ViewsRateLimiterSupport
        ##
        # Browsers can only use rate limited cache
        # if all of these headers match.
        # That prevents Lotus from using 429 caches
        # that originated on apps.
        # @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Vary
        #
        VARY_HEADERS_FOR_CACHE = %w[
          HTTP_X_ZENDESK_APP_ID
        ].join(', ').freeze

        ##
        # This 429 cache prevents misbehaving apps from
        # hammering our API after getting rate limited.
        # It also helps distribute the rate limit reset
        # times since Prop uses fixed time buckets. Prop
        # opens the gates for every client at the exact same
        # time. If they're using high frequency retries we'll
        # get flooded with requests. This cache will redistribute
        # the reset time from the browser perspective.
        # Negative effects are:
        # * Browsers see a wrong `retry-after` for the duration of the cache.
        # * The cache can effectively extend the rate limiting period.
        #
        RATE_LIMIT_CACHE_TTL = 10.seconds

        extend ActiveSupport::Concern
        included do
          rescue_from Prop::RateLimited, with: :handle_rate_limited
        end

        private

        def rate_limiter(type, limit_key = nil)
          Zendesk::Rules::ViewsRateLimiter.limiter_for(
            type,
            current_account,
            current_user,
            request_origin_details,
            limit_key
          )
        end

        ##
        # @rate_limiter is being set by each rate limiting before action.
        # This method is called by `rescue_from Prop::RateLimited`
        #
        # If you want to deal with a limit not handled by ViewsRateLimiter
        # you can either accept the default response bellow or implement
        # your own handler with the hook `handle_rate_limited_#{custom_handle}`
        #
        # @example Unhandled Rate limit
        #   # configure the rate limit
        #   Prop.configure :my_limit, threshold: 1, interval: 1
        #
        #   # implement in controller
        #   # (must receive RateLimited exception as argument)
        #
        #   def handle_rate_limited_my_limit(exception)
        #     # deal with it
        #     head :429
        #   end
        #
        def handle_rate_limited(exception)
          if @rate_limiter
            @rate_limiter.report(exception.first_throttled)
            render_default_rate_limited_response(exception)
          else
            custom_handler = "handle_rate_limited_#{exception.handle}"
            if valid_rate_limited_handler?(custom_handler)
              send(custom_handler, exception)
            else
              render_default_rate_limited_response(exception)
            end
          end
        end

        def valid_rate_limited_handler?(custom_handler)
          respond_to?(custom_handler) && method(custom_handler).arity == 1
        end

        def render_default_rate_limited_response(exception)
          response.headers['Retry-After'] = exception.retry_after.to_s

          if current_account.has_views_cache_429?
            response.headers['Vary'] = VARY_HEADERS_FOR_CACHE
            expires_in(RATE_LIMIT_CACHE_TTL)
          end

          render(
            plain: exception.description,
            status: 429
          )
        end
      end
    end
  end
end
