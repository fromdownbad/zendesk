module Zendesk::Rules
  module ControllerSupport
    # Provides a cleaner interface for accessing definition item params.
    # Valid keys are :actions, :conditions_all, or :conditions_any.
    #
    # Returns a collection of items sorted by position, or nil.
    #
    # Example:
    #   params = {
    #     :sets => {
    #       "1" => {
    #         :conditions => {
    #           "0" => {
    #             "operator" => "less_than",
    #             "value"    => {"0"=>"2"},
    #             "source"   => "status_id"
    #           },
    #           "1" => {
    #             "operator" => "is",
    #             "value"    => {"0" => "current_groups"},
    #             "source"   => "group_id"
    #           },
    #           "2" => {
    #             "operator" => "is_not",
    #             "value"    => {"0"=>""},
    #             "source"   => "group_id"
    #           }
    #         }
    #       }
    #     }
    #   }
    #
    #   definition_params = DefinitionParams.new(params)
    #
    #   definition_params[:conditions_all]
    #   => [
    #     {
    #       "operator" => "less_than",
    #       "value"    => {"0"=>"2"},
    #       "source"   => "status_id"
    #     },
    #     {
    #       "operator" => "is",
    #       "value"    => {"0"=>"current_groups"},
    #       "source"   => "group_id"
    #     },
    #     {
    #       "operator" => "is_not",
    #       "value"    => {"0"=>""},
    #       "source"   => "group_id"
    #     }
    #   ]
    #
    class DefinitionParams
      KEYS = {
        actions: [:sets, '1', :actions],
        conditions_all: [:sets, '1', :conditions],
        conditions_any: [:sets, '2', :conditions]
      }.freeze

      def initialize(params)
        @params = params
      end

      def [](type)
        if items = lookup(type)
          # have to do a custom sort block for when our actions/conditions
          # exceed 9 items.
          items.
            to_h.
            sort_by { |position, _| position.to_i }.
            map { |_, item| ActionController::Parameters.new item }
        end
      end

      protected

      def lookup(type)
        return nil unless KEYS.key?(type)

        namespace, position, name = KEYS[type]

        @params.dig(namespace, position, name)
      end
    end
  end
end
