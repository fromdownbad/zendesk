module Zendesk::Rules::ControllerSupport
  module Copying
    private

    def existing_rule
      @existing_rule ||= current_account.all_rules.find(params[:copy_id]) if params[:copy_id]
    end

    def existing_rule?
      existing_rule.present?
    end

    def authorize_access_to_copy_existing_rule
      deny_access unless current_user.can?(:edit, existing_rule)
    end

    def build_copy_from_existing_rule
      @rule       = existing_rule.copy
      @output     = existing_rule.output

      case @rule
      when Macro then @copy_id = existing_rule.id
      when View then @output.type = 'table'
      end

      @definition = existing_rule.definition
    end
  end
end
