module Zendesk::Rules::ControllerSupport
  # Views/Macros
  module Personal
    private

    def owner
      owner_type = params[:rule].delete(:owner_type)
      owner_id   = params[:rule].delete(:owner_id)

      return @owner if @owner

      @owner = if owner_type == 'Account' &&
                  current_user.can?(:manage_shared, klass)
        current_account
      elsif owner_type == 'Group' &&
                     current_user.can?(:manage_group, klass)
        current_account.groups.find_by_id(owner_id)
      else
        current_user
      end
    end

    def rule_collection
      if personal?
        current_user.send("personal_#{rule_type}")
      else
        owner = current_user.can?(:manage_shared, klass) ? current_account : current_user

        owner.send("shared_#{rule_type}")
      end
    end

    def check_permissions_for_destroy_inactive
      head :forbidden unless allow_rule_manipulations?
    end

    def allow_rule_manipulations?
      if personal?
        current_user.can?(:manage_personal, klass)
      else
        current_user.can?(:manage_shared, klass)
      end
    end

    def authorize_manage_access_for_new_rule
      deny_access unless current_user_can_manage_rule?
    end

    def current_user_can_manage_rule?
      current_user.can?(:manage_personal, klass) ||
        current_user.can?(:manage_group, klass)  ||
        current_user.can?(:manage_shared, klass)
    end
  end
end
