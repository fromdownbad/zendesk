module Zendesk::Rules::ControllerSupport
  # Triggers/automations
  module BusinessRuleAuthorization
    protected

    def check_permissions_for_destroy_inactive
      head :forbidden unless can_edit_business_rules?
    end

    def authorize_manage_access_for_new_rule
      deny_access unless can_edit_business_rules?
    end

    def allow_rule_manipulations?
      personal? || can_edit_business_rules?
    end

    def can_edit_business_rules?
      current_user.can?(:edit, rule_type.singularize.capitalize.constantize)
    end
  end
end
