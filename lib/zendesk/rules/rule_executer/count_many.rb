require 'occam_client/client'

module Zendesk
  module Rules
    class RuleExecuter
      class CountMany
        CountManyDisabledError = Class.new(StandardError)
        CUSTOM_COUNT_CAP = 5_000
        TIMEOUT = 2

        attr_reader :views, :account, :user, :include_deleted, :include_suspended, :params

        def initialize(views: [], account:, user:, include_deleted:, include_suspended:, params: {})
          @views   = views
          @account = account
          @user    = user
          @params  = params
          @include_suspended = include_suspended
          @include_deleted   = include_deleted
        end

        def perform
          check_if_disabled!
          res = nil
          time = Benchmark.realtime { res = execute_with_occam }
          process_count_many_response(res, time)
        rescue CountManyDisabledError, JohnnyFive::CircuitTrippedError => e
          track_count_many_error(e.class.name.demodulize.underscore)
          broken_counts
        end

        private

        def check_if_disabled!
          if account.has_views_disable_count_many?
            Rails.logger.error "[views][count_many] CountMany disabled for Account #{account.id}"
            raise CountManyDisabledError
          end
        end

        def process_count_many_response(response, time)
          if response.key?('error')
            Rails.logger.error("Occam count many error: #{response}")
            track_count_many_error(response['error'])
            []
          else
            statsd_client.histogram('rules.count_many_occam.timing', time * 1000)

            counts = response['counts'].map do |json|
              Zendesk::Rules::OccamTicketCount.new(json.symbolize_keys)
            end

            counts + custom_counts
          end
        end

        def execute_with_occam
          if account.has_views_count_many_circuit_breaker?
            occam_client.with_circuit('count_many') do |client|
              client.count_many(execution_options, views, account, user, params)
            end
          else
            occam_client.count_many(execution_options, views, account, user, params)
          end
        end

        def execution_options
          @execution_options ||= Zendesk::Rules::ExecutionOptions.new(nil, account, params)
        end

        def occam_client
          @occam_client ||= Zendesk::Rules::OccamClient.new(
            timeout: TIMEOUT,
            circuit_options: circuit_options
          )
        end

        def circuit_options
          {
            failure_threshold: 3,
            reset_timeout: 30
          }
        end

        ##
        # Builds a list of count objects to be rendered by the Serializer.
        # Suspended and Deleted ticket counts are allowed, even
        # when view counts are disabled.
        #
        def broken_counts
          counts = views.map do |view|
            Zendesk::Rules::BrokenOccamTicketCount.new(view.id)
          end
          counts + custom_counts
        end

        def custom_counts
          [].tap do |counts|
            counts << deleted_custom_count(account)   if include_deleted
            counts << suspended_custom_count(account) if include_suspended
          end
        end

        def custom_count(name, ticket_count)
          attributes = {
            id:         name.to_s,
            count:      ticket_count,
            fresh:      true,
            updated_at: Time.now.to_s,
            refresh:    'poll',
            poll_wait:  60
          }

          Zendesk::Rules::OccamTicketCount.new(attributes)
        end

        def deleted_custom_count(account)
          count = Ticket.with_deleted do
            account.deleted_tickets.capped_count(CUSTOM_COUNT_CAP)
          end

          custom_count(:deleted, count)
        end

        def suspended_custom_count(account)
          count = account.suspended_tickets.capped_count(CUSTOM_COUNT_CAP)
          custom_count(:suspended, count)
        end

        def track_count_many_error(error_name)
          statsd_client.increment(
            'rules.view_api_occam_error',
            tags: ['action:count_many', "error:#{error_name}"]
          )
        end

        def statsd_client
          Rails.application.config.statsd.client
        end
      end
    end
  end
end
