module Zendesk::Rules::Routing
  def update_skill_based_filtered_views
    return unless should_remove_view?

    account.settings.skill_based_filtered_views =
      account.settings.skill_based_filtered_views.reject do |view_id|
        view_id == id
      end

    account.settings.save!
  end

  private

  def should_remove_view?
    return false unless account.settings.skill_based_filtered_views.include?(id)

    !is_active || deleted? || !shared?
  end
end
