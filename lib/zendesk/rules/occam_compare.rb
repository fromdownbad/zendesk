module Zendesk::Rules
  module OccamCompare
    def occam_cached_rules_comparison(debug: false)
      hash = Hash.new do |h1, k1|
        h1[k1] = Hash.new { |h2, k2| h2[k2] = [] }
      end

      all_watched_rules do |account, rule|
        tickets = rule.find_tickets_with_occam(
          nil,
          hanlon_cache:     false,
          occam_raw_output: true
        )

        puts "tickets find payload: #{tickets}" if debug

        cached = rule.find_tickets_with_occam(
          nil,
          hanlon_cache:     true,
          occam_raw_output: true
        )

        puts "cached tickets payload: #{cached}" if debug

        result = {
          rule_id: rule.id,
          status:  compare_fetched_results(tickets, cached)
        }

        unless %i[match missed_cache].include?(result[:status])
          result[:not_cached] = tickets
          result[:cached] = cached
        end
        hash[result[:status]][account.id] << result
      end

      hash
    end

    private

    def all_watched_rules
      ActiveRecord::Base.
        connection.
        select('select * from rule_watches').
        each do |rw|
          account = Account.find(rw['account_id'])
          if rule = account.rules.find_by_id(rw['rule_id'])
            yield account, rule
          else
            Rails.logger.error "Could not find rule #{rw['rule_id']}"
          end
        end
    end

    def compare_fetched_results(tickets, cached)
      if !tickets.key?('ids') || !cached.key?('ids')
        :error
      elsif cached['cached'] == false
        :missed_cache
      elsif tickets['ids'] != cached['ids']
        :mismatch
      else
        :match
      end
    end
  end
end
