module Zendesk::Rules
  class ContextCache
    include Zendesk::Rules::Context::CustomFieldComponents

    def initialize(account, user, executing: false, validating: false)
      @account    = account
      @user       = user
      @executing  = executing
      @validating = validating
    end

    def accepted_outbound_sharing_agreements
      @accepted_outbound_sharing_agreements ||= begin
        account.sharing_agreements.outbound.accepted.pluck(:name, :id)
      end
    end

    def active_brands
      @active_brands ||= account.brands.active.pluck(:name, :id)
    end

    def active_custom_statuses
      @active_custom_statuses ||= begin
        @account.custom_statuses.active.
          pluck(:agent_label, :id).
          map do |agent_label, id|
            [
              resolve_agent_label(agent_label),
              id
            ]
          end
      end
    end

    def active_groups
      @active_groups ||= account.groups.active.pluck(:name, :id)
    end

    def active_shared_macros
      @active_shared_macros ||= account.shared_macros.active.pluck(:title, :id)
    end

    def active_ticket_forms
      @active_ticket_forms ||= account.ticket_forms.active.pluck(:name, :id)
    end

    def agents
      @agents ||= account.agents.pluck(:name, :id)
    end

    def assignable_agents
      @assignable_agents ||= User.assignable(account).pluck(:name, :id)
    end

    def current_organization_ids
      @current_organization_ids ||= begin
        if account.has_multiple_organizations_enabled?
          user.organization_ids
        else
          [user.organization_id]
        end
      end
    end

    def default_schedule_id
      @default_schedule_id ||= account.schedule.try(:id)
    end

    def default_ticket_form_id
      @default_ticket_form_id ||= account.ticket_forms.default.first.try(:id)
    end

    def integration_service_instance_map
      @integration_service_instance_map ||= Hash[
        ::Channels::AnyChannel::RegisteredIntegrationService.
          where(account_id: account.id).
          map do |ris|
            isi_map_format(ris.id, ris.integration_service_instances)
          end
      ]
    end

    def monitored_facebook_page_map
      @monitored_facebook_page_map ||= Hash[
        ::Facebook::Page.
          monitored.
          where(account_id: account.id).
          map { |mfp| [mfp.id, mfp.name] }
      ]
    end

    def monitored_twitter_handle_map
      @monitored_twitter_handle_map ||= Hash[
        MonitoredTwitterHandle.
          visible.
          where(account_id: account.id).
          map { |mth| [mth.id, mth.twitter_screen_name] }
      ]
    end

    def organization_list
      @organization_list ||= begin
        account_orgs = account.fetch_organizations_for_lists

        if organization_restricted_agent?
          account_orgs.select do |_, id|
            current_organization_ids.include? id
          end
        else
          account_orgs
        end
      end
    end

    def organization_list_size
      @organization_list_size ||= organization_list.size
    end

    def recipient_emails
      @recipient_emails ||= account.recipient_emails || []
    end

    def registered_integration_service_map
      @registered_integration_service_map ||= Hash[
        ::Channels::AnyChannel::RegisteredIntegrationService.
          where(account_id: account.id).
          map { |ris| ris_map_format(ris.id, ris.name) }
      ]
    end

    def requester_agents
      @requester_agents ||= User.requesters(account).pluck(:name, :id)
    end

    def satisfaction_reasons
      @satisfaction_reasons ||= account.satisfaction_reasons
    end

    def schedules
      @schedules ||= account.schedules.active.pluck(:name, :id)
    end

    def sharing_agreements
      @sharing_agreements ||= account.sharing_agreements.accepted
    end

    protected

    attr_reader :account, :executing, :user, :validating

    private

    def isi_map_format(ris_id, isis)
      [ris_id, isis.map { |isi| {'id' => isi.id, 'name' => isi.name} }]
    end

    def organization_restricted_agent?
      user && user.agent_restriction?(:organization) && !user.is_admin?
    end

    def ris_map_format(ris_id, ris_name)
      ["any_channel:#{ris_id}", ris_name]
    end

    def resolve_agent_label(agent_label)
      Zendesk::Liquid::DcContext.
        render(agent_label, account, user, 'text/plain', nil, user.dc_cache)
    end
  end
end
