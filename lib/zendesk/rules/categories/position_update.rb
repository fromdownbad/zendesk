module Zendesk::Rules::Categories
  class PositionUpdate
    attr_reader :updates, :rule_type, :account

    def self.update_positions(updates, rule_type, account)
      new(updates, rule_type, account).update_positions
    end

    def initialize(updates, rule_type, account)
      @updates = updates
      @rule_type = rule_type
      @account = account
    end

    def update_positions
      possible_positions = 1..categories.length
      formatted_updates = format_updates
      remaining_categories = categories_existing_order.keys - formatted_updates.values

      position_changes = possible_positions.map do |position|
        next_rule = formatted_updates.delete(position) ||
          remaining_categories.shift ||
          formatted_updates.shift.last

        categories_existing_order[next_rule] == position ? nil : next_rule
      end

      RuleCategory.update_position(account, position_changes).order(:position)
    end

    private

    def categories
      @categories ||= RuleCategory.where(account: account, rule_type: rule_type).order(:position)
    end

    # Formats updates into a syntax that enables re-oredering logic
    #
    # `updates` is an array of categories or objects with keys for `id` & `position`
    #
    # returns a hash with `position` keys and `id` values, sorted by position (low to high),
    #   with ids converted to integers and ids of non-existant catgegories removed
    def format_updates
      updates.each_with_object({}) do |update, obj|
        if categories_existing_order.include?(update['id'].to_i)
          obj.store(update['id'].to_i, update['position'])
        end
      end.invert.sort.to_h
    end

    def categories_existing_order
      @categories_existing_order ||= categories.each_with_object({}) do |category, mapped_categories|
        mapped_categories.store(category.id, category.position)
      end
    end
  end
end
