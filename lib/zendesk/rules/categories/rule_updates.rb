module Zendesk::Rules::Categories
  class RuleUpdates
    attr_reader :updates

    def self.partition(updates)
      new(updates).partition_updates
    end

    def initialize(updates)
      @updates = updates
    end

    def partition_updates
      positionless_updates = []
      position_updates = []

      updates.each do |rule_params|
        params = rule_params.symbolize_keys

        positionless_updates << params.slice(:id, :active, :category_id) if (params.keys & %i[active category_id]).present?
        position_updates << params.slice(:id, :position) if params.key?(:position)
        position_updates << params.slice(:id) if rule_reactivated_without_position?(params)
      end

      [positionless_updates, position_updates]
    end

    private

    def rule_reactivated_without_position?(rule)
      rule.key?(:active) && rule[:active] == true && !rule.key?(:position)
    end
  end
end
