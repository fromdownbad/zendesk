# Enqueues tickets to be processed in batches via Resque
class Zendesk::Rules::AutomationParallelAsyncTicketProcessor < Zendesk::Rules::AutomationTicketProcessor
  BATCH_SIZE = 150

  def initialize(automation:, batch_size: BATCH_SIZE)
    @automation = automation
    @batch_size = batch_size
  end

  def process_tickets(dehydrated_tickets)
    # The best-case scenario of what might happen
    @changed = dehydrated_tickets.size
    @apply_time = 0
    @queued_audit_ids = dehydrated_tickets.
      each_slice(@batch_size).
      map do |dehydrated_ticket_batch|
        audit = AccountAutomationParallelExecutionJob.
          enqueue_and_get_audit(
            @automation.account_id,
            @automation.id,
            dehydrated_ticket_batch
          )

        audit.enqueued_id
      end
  end
end
