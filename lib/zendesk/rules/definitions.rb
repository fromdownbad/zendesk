module Zendesk
  module Rules
    class Definitions
      INVALID_ACTION    = 'PICKACTION'.freeze
      INVALID_CONDITION = 'PICKCONDITION'.freeze

      include Api::V2::Tickets::AttributeMappings

      def initialize(account:, user:, rule_type:)
        @account   = account
        @user      = user
        @rule_type = rule_type
      end

      def actions
        ZendeskRules::Definitions::Actions.
          definitions_as_json(action_context).
          reject { |definition| definition[:value] == INVALID_ACTION }.
          map do |definition|
            Zendesk::Rules::Action.for_definition(
              definition,
              account: account
            )
          end
      end

      def conditions_all
        ZendeskRules::Definitions::Conditions.
          definitions_as_json(condition_context(:all)).
          reject { |definition| definition[:value] == INVALID_CONDITION }.
          map do |definition|
            Zendesk::Rules::Condition.for_definition(
              definition,
              account: account
            )
          end
      end

      def conditions_any
        ZendeskRules::Definitions::Conditions.
          definitions_as_json(condition_context(:any)).
          reject { |definition| definition[:value] == INVALID_CONDITION }.
          map do |definition|
            Zendesk::Rules::Condition.for_definition(
              definition,
              account: account
            )
          end
      end

      def groupables
        output_item_definitions(type: :grouping)
      end

      def sortables
        output_item_definitions(type: :sorting)
      end

      def view_output
        output_item_definitions(type: :selection)
      end

      protected

      attr_reader :account,
        :user,
        :rule_type

      private

      def context_cache
        @context_cache ||= Zendesk::Rules::ContextCache.new(account, user)
      end

      def action_context
        Zendesk::Rules::Context.new(
          account,
          user,
          cache:          context_cache,
          component_type: :action,
          rule_type:      rule_type,
          dc_cache:       user.dc_cache
        )
      end

      def condition_context(condition_type)
        Zendesk::Rules::Context.new(
          account,
          user,
          cache:          context_cache,
          component_type: :condition,
          condition_type: condition_type,
          rule_type:      rule_type,
          dc_cache:       user.dc_cache
        )
      end

      def view_output_context(component_type:)
        Zendesk::Rules::Context.new(
          account,
          user,
          cache:          context_cache,
          rule_type:      :view,
          component_type: component_type
        )
      end

      def output_item_definitions(type:)
        ZendeskRules::Definitions::Conditions.
          definitions_as_json(view_output_context(component_type: type)).map do |definition|
          Zendesk::Rules::View::Output.for_definition(
            definition,
            account: account,
            user: user
          )
        end
      end
    end
  end
end
