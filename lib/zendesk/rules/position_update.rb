module Zendesk::Rules
  class PositionUpdate
    def self.perform(context, updates)
      Zendesk::RuleSelection::Scope.for_reorder(context).each do |scope|
        new(scope, updates).perform
      end
    end

    def initialize(scope, updates)
      @scope   = scope
      @updates = validate_updates(updates)
    end

    def perform
      scope.update_position(new_positions) if updates.any?
    end

    protected

    attr_reader :scope, :updates

    private

    def validate_updates(updates)
      updates.each_with_object({}) do |update, valid_updates|
        if existing_rules.include?(update[:id])
          valid_updates.store(update[:id], update[:position])
        end
      end.invert.sort.to_h
    end

    def new_positions
      @new_positions ||= begin
        remaining_updates = updates.dup
        remaining_rules   = existing_rules.keys - updates.values

        possible_positions.each_with_object([]) do |position, rules|
          next_rule = begin
            remaining_updates.delete(position) ||
              remaining_rules.shift ||
              remaining_updates.shift.last
          end

          rules.push(existing_rules[next_rule] == position ? nil : next_rule)
        end
      end
    end

    def possible_positions
      @possible_positions ||= 1..existing_rules.length
    end

    def existing_rules
      @existing_rules ||= begin
        scope.ordered.each_with_object({}) do |rule, rules|
          rules.store(rule.id, rule.position)
        end
      end
    end
  end
end
