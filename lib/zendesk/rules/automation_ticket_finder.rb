require 'zendesk_rules/errors'
require 'timecop'

class Zendesk::Rules::AutomationTicketFinder
  cattr_accessor(:log_slow_find) { 5.seconds }

  def initialize(automation:, find_time: Time.now, limit: 1000)
    @automation = automation
    @find_time  = find_time
    @limit      = limit
  end

  attr_reader :select_time, :found

  def find_tickets(rule_find_args = {})
    tickets = []
    @select_time = Benchmark.realtime do
      ZendeskAPM.trace('automation.find_tickets', service: Rule::APM_SERVICE_NAME) do
        user = User.system

        # due to business hours - do business hour filtering based on
        # account settings.
        user.account = @automation.account
        Timecop.freeze(@find_time) do
          disable_automation_if_invalid do
            tickets = @automation.find_tickets(
              user,
              {
                limit: @limit,
                caller: 'automation',
                find_time: @find_time,
                timeout: 5.minutes
              }.merge(rule_find_args)
            )
          end
        end
      end
    end

    @found = tickets.size

    if @found >= @limit
      statsd_client.increment 'errors', tags: %w[error:find_tickets_drowning]
      Rails.logger.warn "Automation #{@automation.id} for account #{@automation.account_id} fetched #{tickets.count} tickets and may be drowning"

    end

    if @select_time > log_slow_find
      statsd_client.increment 'errors', tags: %w[error:slow_find]
      Rails.logger.warn("SLOW FIND: Automation #{@automation.id} '#{@automation.title}' for account #{@automation.account.subdomain} took #{@select_time.round(3)} seconds to query")

    end

    tickets
  end

  def disable_automation_if_invalid
    yield
  rescue ZendeskRules::InvalidComponent => e
    statsd_client.increment 'errors', tags: %w[error:rule_invalid_component]
    Rails.logger.warn "Disabling automation #{@automation.id} for account #{@automation.account_id} due to #{e.inspect}"

    @automation.is_active = false
    @automation.save(validate: false)
    raise e
  end

  def statsd_client
    @statsd_client ||=
      Zendesk::StatsD.client namespace: 'automation_ticket_finder'
  end
end
