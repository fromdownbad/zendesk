require 'simplediff-ruby'

module Zendesk
  module Rules
    module Trigger
      class RevisionDiff < Zendesk::Rules::Diff
        def initialize(source:, target:, differ: SimpleDiff)
          super(
            source: RevisionDiffWrapper.new(source),
            target: RevisionDiffWrapper.new(target),
            differ: differ
          )
        end

        class RevisionDiffWrapper
          delegate(*::Trigger::REVISION_ATTRIBUTES, to: :snapshot)

          def initialize(revision)
            @revision = revision
            @snapshot = revision.snapshot
          end

          def id
            revision.nice_id
          end

          private

          attr_reader :revision, :snapshot
        end
        private_constant :RevisionDiffWrapper
      end
    end
  end
end
