module Zendesk
  module Rules
    module Trigger
      class TriggerSnapshotSerialization < ActiveRecord::Coders::YAMLColumn
        def dump(definition)
          definition.is_a?(String) ? definition : super
        end
      end
    end
  end
end
