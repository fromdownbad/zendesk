module Zendesk
  module Rules
    module Trigger
      class Execution
        def self.execute(account:, events:, execution_type:, ticket:)
          ZendeskAPM.trace(
            'trigger.run_loop.execution',
            service: ::Trigger::APM_SERVICE_NAME
          ) do |span|
            span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
            span.set_tag('zendesk.account_id', account.id)
            span.set_tag('zendesk.account_subdomain', account.subdomain)

            span.set_tag('zendesk.ticket.id', ticket.id)
            span.set_tag(
              'zendesk.ticket.update_type',
              ticket.new_record? ? 'Create' : 'Update'
            )

            parent_span_service = span&.parent&.service
            span.set_tag('zendesk.parent_span_service', parent_span_service) if parent_span_service

            triggers = fetch_triggers(account: account, type: execution_type)

            span.set_tag('zendesk.trigger.count', triggers.length)
            span.set_tag('zendesk.trigger.execution_type', execution_type)

            run_state = new(
              account:  account,
              events:   events,
              triggers: triggers,
              ticket:   ticket
            ).execute

            if run_state
              span.set_tag('zendesk.trigger.match_time', run_state.match_time)
              span.set_tag('zendesk.trigger.apply_time', run_state.apply_time)
              span.set_tag('zendesk.trigger.checks', run_state.checks)
              span.set_tag('zendesk.trigger.checks_elided', run_state.checks_elided)
              span.set_tag('zendesk.trigger.applied', run_state.applied.size)
              span.set_tag('zendesk.trigger.discarded', run_state.discarded.size)
              span.set_tag('zendesk.trigger.loop_number', run_state.loop_number)

              if account.has_instrument_trigger_matching?
                run_state.match_time_by_source.each do |source, value|
                  span.set_tag("zendesk.trigger.match_time_by_source.#{source}", value)
                end
                run_state.checks_by_source.each do |source, value|
                  span.set_tag("zendesk.trigger.checks_by_source.#{source}", value)
                end
              end
            end

            run_state
          end
        end

        def self.fetch_triggers(account:, type:)
          case type
          when :ticket_sharing_create
            account.fetch_active_non_update_triggers
          when :satisfaction_prediction
            account.fetch_active_prediction_triggers
          else
            account.fetch_active_triggers
          end
        end
        private_class_method :fetch_triggers

        def initialize(account:, events:, ticket:, triggers:)
          @account  = account
          @events   = events
          @ticket   = ticket
          @triggers = triggers
        end

        def execute
          execute_triggers
        end

        protected

        attr_reader :account,
          :events,
          :ticket,
          :triggers

        private

        def execute_triggers
          # Please read this article for context on this loop
          # https://support.zendesk.com/hc/en-us/articles/203662246-About-triggers-and-how-they-work
          run_state = Zendesk::Rules::Trigger::RunState.new
          idx = 0
          while idx < triggers.length
            run_state.loop_number += 1 if idx == 0
            trigger = triggers[idx]
            idx += 1
            # move on to the next possible trigger if we've run already
            next if run_state.discarded?(trigger) || run_state.applied?(trigger)

            status, changes = trigger.fire(ticket, events, run_state)
            case status
            when :match
              run_state.applied!(trigger)
              # if we applied a change, reset to the beginning. otherwise, keep going
              if changes.any?
                idx = 0
              end
            when :aborted
              # Something invalidated this loop, abort entirely
              run_state.aborted!
              return run_state
            when :permanent_mismatch
              run_state.discard!(trigger)
            end
          end

          run_state.log_results(ticket.id, triggers.size, ticket)
          run_state
        end
      end
    end
  end
end
