# encapsulates the state needed for running all triggers on ticket save
module Zendesk
  module Rules
    module Trigger
      class RunState
        def initialize
          @start_time           = Time.now.to_f
          @discarded            = Set.new
          @applied              = Set.new
          @memos                = {}
          @checks               = 0
          @checks_by_source     = Hash.new(0)
          @checks_elided        = 0
          @routing_action       = false
          @match_time           = 0.0
          @match_time_by_source = Hash.new(0)
          @apply_time           = 0.0
          @loop_number          = 0
          @memoized_objects     = {}
        end

        attr_reader :discarded, :applied, :checks, :checks_by_source, :checks_elided, :match_time_by_source
        attr_accessor :match_time, :apply_time, :loop_number, :memoized_objects

        def discard!(trigger)
          @discarded.add(trigger.id)
        end

        def discarded?(trigger)
          @discarded.include?(trigger.id)
        end

        def applied!(trigger)
          @routing_action = trigger.routing_action
          @applied.add(trigger.id)
        end

        def applied?(trigger)
          @applied.include?(trigger.id)
        end

        def aborted?
          @aborted
        end

        def aborted!
          @aborted = true
        end

        def memoize(key)
          if @memos.key?(key)
            increment_checks_elided
            @memos[key]
          else
            increment_checks
            @memos[key] = yield
          end
        end

        def increment_checks
          @checks += 1
        end

        def increment_checks_elided
          @checks_elided += 1
        end

        def increment_match_by_source(source, time)
          key = if source.include?('ticket_fields_')
            'ticket_fields'
          elsif source =~ /(.+)\.custom_fields\.(.+)/
            if ["requester", "assignee", "submitter", "organization"].include?($1)
              $1 + "_custom_fields"
            else
              "other_custom_fields"
            end
          else
            source
          end

          @match_time_by_source[key] += time
          @checks_by_source[key] += 1
        end

        def log_results(ticket_id, trigger_count, ticket = nil)
          duration = Time.now.to_f - @start_time

          stats_hash = {
            triggers:      trigger_count,
            checks:        @checks,
            checks_elided: @checks_elided,
            applied:       @applied.size,
            discarded:     @discarded.size,
            duration:      duration,
            match_time:    @match_time,
            apply_time:    @apply_time,
            loop_number:   @loop_number
          }
          Rails.logger.info("Finished trigger run for ticket_id #{ticket_id} %s" % stats_hash.to_json)
          Rails.logger.info("Applied rules: #{applied.inspect}")

          update_type = if ticket
            ticket.new_record? ? 'Create' : 'Change'
          else
            'Unknown'
          end

          statsd.increment(
            'trigger_execution',
            tags: [
              "routing_action:#{@routing_action}",
              "update_type:#{update_type}"
            ]
          )
        end

        private

        def statsd
          @statsd ||=
            Zendesk::StatsD::Client.new(namespace: 'trigger_run_state')
        end
      end
    end
  end
end
