module Zendesk
  module Rules
    module SystemRule
      attr_accessor :writable

      def self.included(base)
        base.before_destroy :mark_for_destruction
      end

      def is_system_rule? # rubocop:disable Naming/PredicateName
        feature_identifier.present? && feature_identifier != '0'
      end

      def readonly?
        !writable && !new_record? && is_locked? && !marked_for_destruction?
      end
    end
  end
end
