class Zendesk::Rules::AutomationDehydratedTicketProcessor < Zendesk::Rules::AutomationTicketProcessor
  # Smaller values mean more trips to the database to lookup tickets.
  # Larger values mean a larger race surface between lookup time and `save`
  # time.
  HYDRATION_BATCH_SIZE = 10

  def process_tickets(dehydrated_tickets)
    # Setup the instance variables for capturing stats
    # Because we do multiple batches for the ticket ids we get
    # We need to calculate the full statistics by sum up the total
    total_changed = total_apply_time = 0

    dehydrated_tickets.each_slice(HYDRATION_BATCH_SIZE) do |batch|
      super(
        Zendesk::Rules::DehydratedTicket.
          hydrate_tickets_for_automation(batch, @automation)
      )

      total_apply_time += @apply_time
      total_changed += @changed
    end

    @apply_time = total_apply_time
    @changed = total_changed
  end
end
