module Zendesk
  module Rules
    class Condition
      IGNORE_MAPPING = %w[current_via_id via_id].freeze

      CUSTOM_FIELD_MARKER = 'custom_fields'.freeze

      EMPTY_TITLE = '-'.freeze
      NULL_VALUE  = '__NULL__'.freeze

      NULLABLE_SOURCES = %w[
        assignee_id
        group_id
        organization_id
        priority_id
        ticket_type_id
      ].freeze
      TAG_SOURCES = %w[current_tags].freeze

      TITLE_KEY = 'txt.admin.models.rules.rule_dictionary.%{source}'.freeze

      METADATA = {'autocomplete' => %i[collection_key item_key]}.freeze

      FORMAT_CONDITIONS = {'date' => :date_format}.freeze

      TITLE_MAPPING = {
        'comment_includes_word'            => 'comment_text_label_v2',
        'comment_is_public'                => 'comment_label',
        'requester_twitter_statuses_count' => 'number_of_tweets_label_v2',
        'subject_includes_word'            => 'subject_text_label_v2',
        'update_type'                      => 'ticket_label'
      }.freeze

      private_constant :CUSTOM_FIELD_MARKER,
        :EMPTY_TITLE,
        :FORMAT_CONDITIONS,
        :IGNORE_MAPPING,
        :METADATA,
        :NULLABLE_SOURCES,
        :NULL_VALUE,
        :TAG_SOURCES,
        :TITLE_KEY,
        :TITLE_MAPPING

      def self.for_definition(definition, account:)
        new(
          Definition.new(
            definition[:value],
            definition[:title],
            definition[:values][:type],
            definition[:group],
            definition[:values][:list],
            definition[:operators]
          ),
          account: account
        )
      end

      def initialize(definition, account:)
        @account       = account
        @source        = definition.source
        @title         = condition_title(definition.source, definition.title)
        @type          = condition_type(definition.source, definition.type)
        @group         = definition.group
        @list          = definition.list
        @operator_list = definition.operator_list
        @metadata      = METADATA[definition.type]
      end

      def subject
        return source.to_s if IGNORE_MAPPING.include?(source)

        Api::V2::Tickets::AttributeMappings.ticket_attribute_name(source).to_s
      end

      def values
        @values ||= begin
          list.reject(&method(:invalid_item?)).map do |item|
            Value.new(
              item_value(item),
              item[:title],
              enabled_item?(item)
            )
          end
        end
      end

      def operators
        @operators ||= begin
          operator_list.map do |operator|
            Operator.new(
              operator[:value],
              operator[:title],
              operator.key?(:values) && operator[:values].empty?,
              format_operator(operator)
            )
          end
        end
      end

      def repeatable?
        false
      end

      def nullable?
        NULLABLE_SOURCES.include?(source) ||
          subject.include?(CUSTOM_FIELD_MARKER) && type == 'list'
      end

      attr_reader :group,
        :metadata,
        :output_key,
        :title,
        :type

      protected

      attr_reader :account,
        :list,
        :operator_list,
        :source

      private

      def condition_title(source, title)
        return title unless TITLE_MAPPING.key?(source)

        ::I18n.t(format(TITLE_KEY, source: TITLE_MAPPING[source]))
      end

      def condition_type(source, type)
        TAG_SOURCES.include?(source) ? 'tags' : type
      end

      def item_value(item)
        return NULL_VALUE        if null_item?(item)
        return item[:value].to_s if IGNORE_MAPPING.include?(source)

        Api::V2::Tickets::AttributeMappings.ticket_attribute_value(
          source,
          item[:value],
          account,
          full_fidelity: true
        ).to_s
      end

      def invalid_item?(item)
        null_item?(item) && !nullable?
      end

      def null_item?(item)
        item[:title] == EMPTY_TITLE
      end

      def enabled_item?(item)
        item.key?(:enabled) ? item[:enabled] : true
      end

      def format_operator(operator)
        return nil unless FORMAT_CONDITIONS.key?(type)

        send(FORMAT_CONDITIONS[type], operator)
      end

      def date_format(operator)
        operator.fetch(:field_type, 'date')
      end

      Definition = Struct.new(
        :source,
        :title,
        :type,
        :group,
        :list,
        :operator_list
      )

      Value    = Struct.new(:value, :title, :enabled?)
      Operator = Struct.new(:value, :title, :terminal?, :format)

      private_constant :Definition,
        :Operator,
        :Value
    end
  end
end
