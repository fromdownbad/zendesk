class Zendesk::Rules::AutomationExecutor
  def initialize(automation:, ticket_finder: nil, ticket_processor: nil)
    @automation       = automation
    @ticket_finder    = ticket_finder    || Zendesk::Rules::AutomationTicketFinder.new(automation: automation)
    @ticket_processor = ticket_processor || Zendesk::Rules::AutomationTicketProcessor.new(automation: automation)
  end

  def execute
    unless @automation.account.present?
      ZendeskExceptions::Logger.record(
        StandardError.new,
        location:    self,
        message:     "Automation #{@automation.id} is missing an account",
        fingerprint: '6381bda787320364f23bf2704c86d16f5944d07d'
      )

      return
    end

    ZendeskAPM.trace(
      'automation.execute',
      service: Rule::APM_SERVICE_NAME
    ) do |span|
      span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
      span.set_tag('zendesk.account_id', @automation.account.id)
      span.set_tag('zendesk.account_subdomain', @automation.account.subdomain)
      span.set_tag('zendesk.automation_id', @automation.id)
      span.set_tag('zendesk.automation_processor', @ticket_processor.class.name)

      ZendeskAPM.trace(
        'automation.process_tickets',
        service: Rule::APM_SERVICE_NAME
      ) do
        @process_time = Benchmark.realtime do
          @ticket_processor.process_tickets(@ticket_finder.find_tickets)
        end
        statsd_client.timing('process', (@process_time * 1000).to_i, tags: ["ticket_processor:#{@ticket_processor.class.name}"])
      end

      span.set_tag('zendesk.automation.tickets.found', @ticket_finder.found)
      span.set_tag(
        'zendesk.automation.tickets.changed',
        @ticket_processor.changed
      )

      Automation::Result.new(
        @automation,
        @ticket_finder.found,
        @ticket_processor.changed,
        @ticket_finder.select_time,
        @ticket_processor.apply_time,
        @ticket_processor.queued_audit_ids
      )
    end
  end

  def statsd_client
    Zendesk::StatsD.client(namespace: 'automation_executor')
  end
end
