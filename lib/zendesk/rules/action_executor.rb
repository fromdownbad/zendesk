class Zendesk::Rules::ActionExecutor
  COMMENT_ACTION_SOURCES = %w[comment_value comment_value_html].freeze
  MESSAGING_CSAT_ACTION_CHANNELS = [ViaType.NATIVE_MESSAGING].freeze

  # Apply a trigger or automation's actions to a ticket
  def self.execute(rule, ticket, actions)
    ZendeskAPM.trace('rule.execute') do |span|
      span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)

      current_user = get_current_user(ticket)

      ticket.will_be_saved_by(
        current_user,
        via_id:           ViaType.RULE,
        via_reference_id: rule.id
      )

      span.set_tag('zendesk.account_id', ticket.account_id)
      span.set_tag('zendesk.account_subdomain', ticket.account.subdomain)
      span.set_tag('zendesk.user_id', current_user.id)
      span.set_tag('zendesk.rule.id', rule.id)
      span.set_tag('zendesk.rule.type', rule.rule_type)
      span.set_tag('zendesk.rule.action_count', actions.size)

      if ticket.account.has_rule_usage_stats?
        ticket.rule_ticket_joins.build(ticket: ticket, rule: rule)
      end

      ticket.last_executed_rules ||= []

      ticket.last_executed_rules << {
        user_id:   current_user.id,
        rule_id:   rule.id,
        rule_type: rule.rule_type
      }

      actions.each do |action|
        new(rule, ticket, action).apply
      end

      ticket.enforce_integrity_upon_attribute_change if ticket.delta_changes?
    end
  end

  # `action` here is of type DefinitionItem
  def apply
    Rails.logger.debug("applying-rule-action: #{source}")

    ZendeskAPM.trace(
      'rule.action.apply',
      service: Rule::APM_SERVICE_NAME
    ) do |span|
      span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)

      span.set_tag('zendesk.account_id', ticket.account_id)
      span.set_tag('zendesk.account_subdomain', ticket.account.subdomain)
      span.set_tag('zendesk.rule.id', rule.id)
      span.set_tag('zendesk.rule.type', rule.rule_type)

      source_tag = if source.include?('ticket_fields_')
        "ticket_fields"
      elsif source =~ /(.+)\.custom_fields\.(.+)/
        if ["requester", "assignee", "submitter", "organization"].include?($1)
          $1 + "_custom_fields"
        else
          "other_custom_fields"
        end
      else
        source
      end

      span.set_tag('zendesk.rule.action.source', source_tag)
      span.set_tag('zendesk.rule.action.operator', action.operator)

      if source == 'ticket_type_id'
        # no need to set ticket type if already set (also prevents due date
        # reset on tasks)
        return if value && ticket.ticket_type_id == value.to_i

      end

      if source == 'notification_target'
        apply_target_notification_actions
      elsif source == 'notification_sms_user'
        apply_sms_user_notification_actions
      elsif source == 'notification_sms_group'
        apply_sms_group_notification_actions
      elsif source == 'notification_user'
        apply_notification_user_action
      elsif source == 'notification_group'
        apply_notification_group_action
      elsif source == 'notification_messaging_csat'
        apply_messaging_csat_action
      elsif source == 'deflection'
        apply_deflection_action
      elsif source == 'share_ticket'
        apply_share_ticket_action
      elsif source == 'tweet_requester'
        apply_twitter_action
      elsif source == 'macro_id' || source == 'macro_id_after'
        apply_macro_action
      elsif source == 'comment_value'
        apply_comment_action { |content| ticket.comment.body = content }
      elsif source == 'comment_value_html'
        apply_comment_action do |content|
          ticket.comment.html_body = content.auto_link
        end
      elsif source == 'comment_mode_is_public'
        apply_comment_mode_action
      elsif source == 'cc'
        apply_collaborator_action
      elsif source == 'set_schedule'
        apply_set_schedule_action
      elsif source == 'satisfaction_score'
        apply_satisfaction_score_action
      elsif source.include?('ticket_fields_')
        apply_ticket_field_actions
      elsif source =~ /(.+)\.custom_fields\.(.+)/
        apply_custom_field_actions($1, $2)

      else
        apply_default_action
      end
    end
  end

  def self.get_current_user(ticket)
    if ticket.via?(:sms) && ticket.comment
      ticket.comment.author
    else
      ticket.current_user || User.system
    end
  end

  private

  attr_reader :rule, :ticket, :action, :user, :source, :value

  def initialize(rule, ticket, action)
    @rule   = rule
    @ticket = ticket
    @action = action
    @user   = ticket.current_user
    @source = action.source.split('#').first
    @value  = Array(action.value)[0]
  end

  def apply_share_ticket_action
    agreement = ticket.
      account.
      sharing_agreements.
      outbound.
      accepted.
      find_by_id(value)

    return unless agreement
    return if ticket.shared_tickets.any? { |st| st.agreement == agreement }

    attributes = default_event_attributes.merge(agreement_id: agreement.id)
    new_event = TicketSharingEvent.new(attributes)
    new_event.account_id = ticket&.account&.id
    ticket.audit.events << new_event
  end

  def apply_target_notification_actions
    unless value.blank?
      external = External.new(
        resource: value,
        body:     action.value[1],
        audit:    ticket.audit,
        **rule.via_reference
      )

      ticket.audit.events << external
    end
  end

  def notification_user_recipients
    case value
    when 'all_agents'
      ticket.account.unrestricted_agents.map(&:id)
    when 'current_user'
      user.id if user.is_agent? || user.email.present?
    when 'requester_id', 'requester_and_ccs'
      ticket.requester_id if ticket.requester.try(:email).present?
    when 'assignee_id'
      ticket[value]
    else
      ticket.account.agents.active.find_by_id(value).try(:id)
    end
  end

  def apply_notification_user_action
    #require byebug; byebug
    return if suppress_requester_and_ccs_notification?

    recipients = non_suppressed_recipients(notification_user_recipients)

    return unless recipients.present?

    ticket.audit.events << notification_user_event(recipients)
  end

  def apply_notification_group_action
    group = ticket.
      account.
      groups.
      find_by_id(value == 'group_id' ? ticket.group_id : value)

    return unless group.present?

    recipients = non_suppressed_recipients(group.users.pluck(:id))

    return unless recipients.present?

    ticket.audit.events << notification_event(recipients)
  end

  def non_suppressed_recipients(all_recipients)
    recipients = Array(all_recipients)

    invalid_recipients = suppressed_recipients(recipients)

    return recipients unless invalid_recipients.present?

    if ticket.account.has_email_truncate_notifications_suppressed_for_metadata?
      ticket.audit.notifications_suppressed_for = invalid_recipients
    else
      ticket.audit.set_metadata('notifications_suppressed_for', invalid_recipients)
    end
    recipients - invalid_recipients
  end

  def non_suppressed_email_ccs
    User.where(
      account_id: ticket.account.id,
      id:         non_suppressed_recipients(ticket.email_cc_ids)
    ).to_a
  end

  def notification_event(recipients)
    Notification.new(
      recipients: recipients,
      subject:    action.value[1],
      body:       action.value[2],
      audit:      ticket.audit,
      **rule.via_reference
    )
  end

  def notification_with_ccs_event(recipients)
    NotificationWithCcs.new(
      to_recipient_ids: recipients,
      subject:          action.value[1],
      body:             action.value[2],
      audit:            ticket.audit,
      ccs:              non_suppressed_email_ccs,
      **rule.via_reference
    )
  end

  def notification_with_ccs?
    ticket.account.has_comment_email_ccs_allowed_enabled? &&
      value == 'requester_and_ccs'
  end

  def notification_user_event(recipients)
    if notification_with_ccs?
      notification_with_ccs_event(recipients)
    else
      notification_event(recipients)
    end
  end

  def apply_sms_user_notification_actions
    return if invalid_sms_action_params?

    recipients =
      case value
      when 'current_user'
        user.id if user.is_agent? || user.identities.phone.first
      when 'requester_id'
        ticket[value] if ticket.requester
      when 'assignee_id'
        ticket[value]
      else # designated user. Can only be agent.
        ticket.account.agents.active.find_by_id(value).try(:id)
      end

    sms_notification_action.apply(recipients)
  end

  def apply_messaging_csat_action
    # Apply action only if there are no public Comment events after ChatStartedEvent.
    # ChatStartedEvent is created when ticket is created via chat/messaging.
    # Public comments is created when tickets is updated via email. In some cases, ticket created via chat has public comments due to chat_transcript.
    # The condition is defined such that to prevent this action being fired if the ticket is last updated via email (we want those tickets csat to be sent through email).
    event = ticket.events.reverse_each.find do |e|
      e.is_a?(ChatStartedEvent) || (e.is_a?(Comment) && e.is_public && e.via_id != ViaType.CHAT_TRANSCRIPT)
    end

    return if event.nil? || event.is_a?(Comment)

    return unless MESSAGING_CSAT_ACTION_CHANNELS.include?(event.via_id)

    ticket.audit.events << MessagingCsatEvent.new.tap do |e|
      e.chat_started_id = event.id
      e.audit = ticket.audit
      e.via_id = ViaType.RULE
      e.via_reference_id = rule.id
    end

    response = Zopim::InternalApiClient.new(ticket.account_id).send_messaging_csat(event)

    if response.success?
      self.class.new(
        rule,
        ticket,
        DefinitionItem.new('satisfaction_score', 'is', [SatisfactionType.OFFERED])
      ).apply
    else
      Rails.logger.error("Messaging CSAT Error. Body: #{response.body}.")
    end
  end

  def sms_notification_action
    Zendesk::Rules::SmsNotificationAction.new(
      ticket: ticket,
      user: user,
      rule: rule,
      phone_number_id: action.value[1],
      body: action.value[2]
    )
  end

  def apply_sms_group_notification_actions
    return if invalid_sms_action_params?
    group = ticket.account.groups.find_by_id((value == 'group_id') ? ticket.group_id : value)

    return unless group

    sms_notification_action.apply(group.users.pluck(:id))
  end

  def apply_deflection_action
    ticket.deflect_with_rule rule.id
  end

  def apply_twitter_action?
    ticket.requester.has_twitter_identity? &&
      ticket.comment.try(:channel_source_id) &&
      ticket.new_record?
  end

  # -  Only take the action if:
  # 1. We know how to Tweet at the requester,
  # 2. We're adding a comment,
  # 3. The new comment has a channel source (which we will use to respond)
  # 4. The ticket is new- see https://zendesk.atlassian.net/browse/CT-2363
  def apply_twitter_action
    # Rate limit to avoid runaway loops- see CT-2386
    Prop.throttle!(:twitter_action, ticket.account.id)

    return unless apply_twitter_action?

    ticket.validate_and_create_channel_back_event(
      update_via_id: ViaType.RULE,
      via_id:        ViaType.TWITTER,
      text:          action.value[0]
    )
  end

  def apply_macro_action
    return unless ticket.account.has_rules_can_reference_macros?

    if rule.is_a?(Trigger)
      return if !ticket.audit || ticket.audit.via?(:macro_reference)

      if ticket.macros_to_be_applied_after_save.present?
        ticket.macros_to_be_applied_after_save << value
      else
        ticket.macros_to_be_applied_after_save = Array(value)
      end
    else
      return unless macro = ticket.account.all_macros.active.find_by_id(value)

      comment_actions, other_actions = begin
        macro.definition.actions.partition do |action|
          COMMENT_ACTION_SOURCES.include?(action.source)
        end
      end

      [
        relevant_comment_action(comment_actions),
        *other_actions
      ].compact.each do |action|
        self.class.new(rule, ticket, action).apply
      end
    end

    ticket.audit.events << begin
      MacroReference.new(
        value: value,
        audit: ticket.audit,
        **rule.via_reference
      )
    end
  end

  def apply_comment_action
    return unless ticket.account.has_rules_can_reference_macros?

    unless ticket.comment.present?
      ticket.add_comment(
        is_public:        true,
        author_id:        updater.id,
        via_id:           ViaType.RULE,
        via_reference_id: rule.id
      )

      ticket.set_comment
    end

    yield(
      Zendesk::Liquid::TicketContext.render(
        Array(action.value).last,
        ticket,
        updater,
        false,
        ticket.requester.translation_locale
      )
    )
  end

  def apply_comment_mode_action
    if ticket.account.has_rules_can_reference_macros?
      if ticket.comment
        ticket.comment.is_public = (value == 'true')
      else
        ticket.add_comment(
          body:             nil,
          is_public:        (value == 'true'),
          author_id:        updater.id,
          via_id:           ViaType.RULE,
          via_reference_id: rule.id
        )
        ticket.set_comment
      end
    end
  end

  # This action will be presented as "Add CC" or "Add follower".
  # We decided to keep the existing action in zendesk_rules, so the name kept CC
  # https://github.com/zendesk/zendesk_rules/blob/master/lib/zendesk_rules/definitions/actions/cc.rb
  def apply_collaborator_action
    return unless apply_collaborator_action?

    current_collaborators =
      if ticket.account.has_ticket_followers_allowed_enabled?
        ticket.collaborations.legacy_ccs.select { |cc| cc.user.is_agent? }.
          map(&:user_id) +
          ticket.collaborations.followers.map(&:user_id)
      else
        ticket.collaborations.map(&:user_id)
      end

    add_follower = resolve_value

    return if current_collaborators.member?(add_follower.to_i)

    # Ensure the user we are attempting to add is still a User in the DB
    # and is valid
    collaborator_user = ticket.
      account.
      users.
      where(id: add_follower.to_i).
      first
    return unless collaborator_user.try(:valid?)

    if ticket.account.has_ticket_followers_allowed_enabled?
      ticket.set_followers = [{user_id: add_follower.to_i}]
    else
      ticket.set_collaborators = current_collaborators + Array(add_follower)
    end
  end

  def apply_set_schedule_action
    return unless ticket.account.has_multiple_schedules?
    return unless assignable_schedule?(resolve_value.to_i)

    ticket.audit.events << begin
      ScheduleAssignment.new(
        value_previous:   ticket.ticket_schedule.try(:schedule_id),
        value:            resolve_value,
        **rule.via_reference
      )
    end

    ticket.build_ticket_schedule(account: ticket.account) do |ticket_schedule|
      ticket_schedule.schedule_id = resolve_value
    end
  end

  def apply_satisfaction_score_action
    return unless ticket.satisfaction_score == SatisfactionType.UNOFFERED

    apply_default_action
  end

  def apply_ticket_field_actions
    value_arry = Array.wrap(action.value)

    ticket_field = ticket.
      account.
      ticket_fields.
      find_by_id(source.gsub('ticket_fields_', '').to_i)

    return unless ticket_field

    value = value_arry[0]
    set_value = if ticket_field.is_a?(FieldMultiselect)
      ticket_field.aggregate_and_add_new_value(value, ticket)
    elsif ticket_field.is_a?(FieldTagger)
      ticket.account.fetch_custom_field_option_value(value)
    elsif ticket_field.is_a?(FieldDate)
      type, date_value = value_arry
      if type == 'days_from_now'
        return if date_value.blank?

        date = date_value.
          to_i.
          days.
          from_now.
          in_time_zone(ticket.account.time_zone)

        date.strftime('%Y-%m-%d')
      else
        date_value
      end
    elsif ticket_field.is_a?(FieldCheckbox)
      Zendesk::DB::Util.truthy?(value) ? 1 : 0
    else
      # RAILS5UPGRADE: in rails 5 this returns nil. zendesk_rules requires an update https://github.com/zendesk/zendesk_rules/pull/442
      !!Zendesk::DB::Util.truthy?(value)
    end

    ticket.fields = {ticket_field.id => set_value}
  end

  def apply_custom_field_actions(source, source_field)
    conditions = {key: source_field, owner: 'User'}.tap do |h|
      h[:is_active] = true if ticket.account.has_set_active_only_custom_fields?
    end

    components = source.split('.', 2)
    if ['requester', 'submitter', 'assignee'].include?(components[0])

      # checking here to enforce existence of ticket.[user] prior to deferred
      # organization check below
      return unless entity = ticket.send(components.shift)

      if entity.foreign?
        Rails.logger.info("Skipping action for foreign user: #{entity.id}")
        return
      end
    end

    if components[0] == 'organization'
      entity = (entity || ticket).organization
      conditions[:owner] = 'Organization'
    end

    # TODO: what if entity is not set yet? return operator == 'is_not'?
    return unless entity

    # TODO: log that source_field was not found, and so not set?

    return unless custom_field = ticket.account.custom_fields.where(conditions).first

    old_value = entity.custom_field_values.value_for_key(custom_field.key)
    old_value = old_value.dup if old_value
    new_value = entity.custom_field_values.set_field_value(custom_field, value)

    # NOTE: could do this at higher level: check each user/org object for
    # modified (then save)
    success = entity.save
    unless success
      Rails.logger.warn("Trigger: Entity save failed: rule:#{rule&.id}, rule_type:#{rule&.rule_type} account:#{rule&.account&.id} for #{@source} ticket:#{ticket&.id}")
    end

    event = UserCustomFieldEvent.build_from_values(old_value, new_value)
    ticket.add_external_change(event) if event
  end

  def apply_default_action
    if %w[current_user current_groups].member?(value) && user.is_system_user?
      return
    end

    # TODO: convert "current_tags" to "additional_tags" in all rules
    source.gsub!('current_tags', 'additional_tags')
    apply_locale_action if ['locale_id', 'set_locale_id'].include?(source)

    ticket.send("#{source}=", resolve_value)
    if ticket.delta_changes?
      if error = ticket.has_invalid_attribute_change?
        # Revert property setting if a trigger action results in a non-valid
        # setting
        ticket.send("#{source}=", ticket.send("#{source}_delta_was"))
        Rails.logger.info("Rule ID #{rule.id} validation fail for ticket id #{ticket.id}. #{error.inspect}. Source '#{source}', value '#{value}'. Rolled back.")

        ticket.add_translatable_error_event(
          key:         'activerecord.errors.rule_failed_to_apply',
          error_field: error[0],
          error_type:  error[1],
          rule_id:     rule.id,
          field:       source,
          value:       value
        )
      end

    end
  end

  def suppressed_recipients(recipients)
    suppressed = []

    if rule.is_a?(Trigger) && user.machine?
      # TODO: | recipients_with_setting("machine", true, recipients) can be
      # removed when machine user as deliverable state backfill is run
      suppressed += (machine_recipients(recipients) |
                     recipients_with_setting('machine', true, recipients))
    end

    suppressed += undeliverable_recipients(recipients)
    suppressed += direct_recipients(recipients)
    suppressed += recipients_with_setting(
      'suspended',
      [true, 'true'],
      recipients
    )
    suppressed += agent_as_end_user_recipients(recipients)

    suppressed.uniq
  end

  def machine_recipients(recipients)
    ticket.account.users.where(id: recipients).select(&:machine?).map(&:id)
  end

  def recipients_with_setting(name, value, recipients)
    UserSetting.
      where(
        account_id: ticket.account_id,
        name:       name,
        value:      value,
        user_id:    recipients
      ).
      pluck(:user_id)
  end

  def undeliverable_recipients(recipients)
    # rubocop:disable Style/InverseMethods
    User.
      where(id: recipients, account_id: ticket.account_id).
      select { |user| !user.email_is_deliverable? }.
      map(&:id)
    # rubocop:enable Style/InverseMethods
  end

  def direct_recipients(recipients)
    return [] unless ticket.audit.via?(:mail)
    return [] if notification_with_ccs?

    @latest_email_user_ids ||=
      Zendesk::InboundMail::RecipientsHelper.latest_email_user_ids(ticket)

    recipients.select { |user_id| @latest_email_user_ids.include?(user_id) }
  end

  def agent_as_end_user_recipients(recipients)
    # NOTE: This logic should match ::Access::TicketCommon#view_private_content?
    # I.e. when Arturo is enabled, an agent that opens a ticket in HelpCenter is
    # treated as an end user.

    # If the ticket submitter isn't a recipient, we don't care
    return [] unless recipients.include?(ticket.submitter_id)

    # We're only protecting private comments
    return [] unless ticket.comment && ticket.comment.is_private?

    # Feature is behind an Arturo
    return [] unless Arturo.feature_enabled_for?(:agent_as_end_user,
      ticket.account)

    # Agent-as-end-user only applies to certain tickets
    return [] unless ticket.agent_as_end_user_for?(ticket.submitter)

    # OK, it applies to this Comment- agent should not see it.
    [ticket.submitter_id]
  end

  def resolve_value
    user = ticket.current_user
    if value == 'current_user'
      user.id
    elsif value == 'current_groups'
      user.groups.first&.id if user.is_agent?
    elsif %w[requester_id assignee_id group_id].member?(value)
      ticket[value]
    elsif value == 'default_ticket_form'
      ticket.account.ticket_forms.default.first.id
    else
      value
    end
  end

  def default_event_attributes
    {via_id: ViaType.RULE, via_reference_id: rule.id}
  end

  ## Questionable ???
  def apply_locale_action
    return if ticket.requester.locale_id.to_i == value.to_i
    return unless locale = TranslationLocale.find(value.to_i)

    ticket.requester.translation_locale = locale
    success = ticket.requester.save
    unless success
      Rails.logger.warn("Trigger: Requester save failed: rule:#{rule&.id}, rule_type:#{rule&.rule_type} account:#{rule&.account&.id} for #{@source} ticket:#{ticket&.id}")
    end
    success
  end

  def updater
    ticket.current_user || User.system
  end

  def relevant_comment_action(comment_actions)
    basic_comment_action =
      comment_actions.find { |action| action.source == 'comment_value' }

    if ticket.rich?
      rich_comment_action =
        comment_actions.find { |action| action.source == 'comment_value_html' }

      rich_comment_action || basic_comment_action
    else
      basic_comment_action
    end
  end

  # FIXME: This is awful but necessary. action.value is an array but it can be
  # missing values. Since we don't know which one might be missing, we need to
  # check that the number of params is correct and assume that their order is
  # correct also.
  def invalid_sms_action_params?
    Array(action.value).count != 3
  end

  def assignable_schedule?(schedule_id)
    unless ticket.account.schedules.active.where(id: schedule_id).exists?
      return false
    end

    ticket.ticket_schedule.blank? ||
      ticket.ticket_schedule.schedule_id != schedule_id
  end

  def user_is_persisted_cc_or_requester?
    ticket.requested_by?(user) ||
      ticket.persisted_email_cc_user_ids.include?(user.id)
  end

  def suppress_requester_and_ccs_notification?
    if ticket.account.has_ccs_requester_excluded_public_comments_enabled?
      require byebug; byebug
      notification_with_ccs? && ((requester_or_cc_updating_by_mail? && all_recipients_received_direct_email_legacy?) ||
          ticket.comment.try(:is_private?))
    else
      notification_with_ccs? && (
        requester_or_cc_updating_by_mail? ||
        ticket.comment.try(:is_private?) ||
        all_recipients_received_direct_email_legacy?
      )
    end
  end

  def requester_or_cc_updating_by_mail?
    ticket.persisted? &&
      ticket.audit.via?(:mail) &&
      user_is_persisted_cc_or_requester?
  end

  # This is primarily for the case where a non-participant is updating the
  # ticket by email
  #
  # legacy, rename to `comment_included_and_received_via_email?`
  def all_recipients_received_direct_email_legacy?
    if ticket.account.has_email_comment_placeholder_detected_metric?
      # legacy_start_time = Time.now
      # legacy_total_time = Time.now - legacy_start_time

      # tag that indicates whether the Arturo is enabled and the additional check
      # (which is likely a string/regex operation) is run.
      #require byebug; byebug
      print notification_with_ccs_event.body
      print "here"
      puts "billy"
      #comment_placeholder_detected_tag = "test for placeholder"
      Rails.logger.info("some info")
      #statsd_client.increment('action_executor.comment_placeholder_detected', tags: ["comment_placeholder_detected:#{comment_placeholder_detected_tag}"]  )

      return false if ticket.new_record?
      return false unless ticket.audit.via?(:mail)

      all_recipients = (
        Array(non_suppressed_email_ccs.map(&:email)) << ticket.requester.email
      ).uniq.compact.map(&:downcase)

      (all_recipients - recipients_from_inbound_mail).empty?
    end
  end

  def comment_included_and_received_via_email?
    return false if ticket.new_record?
    return false unless ticket.audit.via?(:mail)
    # return false if the notification body does not contain any placeholders that reveal the current comment's content.

    all_recipients = (
      Array(non_suppressed_email_ccs.map(&:email)) << ticket.requester.email
    ).uniq.compact.map(&:downcase)

    (all_recipients - recipients_from_inbound_mail).empty?
  end

  def recipients_from_inbound_mail
    ticket.recipients_for_latest_mail.to_s.downcase.split(' ')
  end

  def apply_collaborator_action?
    value.present? &&
      (ticket.account.is_collaboration_enabled? || eligible_as_follower?)
  end

  def eligible_as_follower?
    return false unless ticket.account.has_ticket_followers_allowed_enabled?

    value == 'current_user' ? user.is_agent? : true
  end

  def action_executor_statsd_client
    @action_executor_statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'action executor')
  end
end
