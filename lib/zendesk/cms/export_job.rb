class Zendesk::Cms::ExportJob < BaseExportJob
  priority :medium

  throttle can_run_every: 15.minutes

  def self.work(account_id, requester_id)
    account = Account.find(account_id)
    ActiveRecord::Base.on_shard(account.shard_id) do
      user       = account.users.find(requester_id)
      output     = Zendesk::Cms::Exporter.build_export(account)
      subject    = ::I18n.t('txt.email.cms.export_job.email.subject', subdomain: account.subdomain, locale: user.translation_locale)
      filename   = "#{account.subdomain}-dc.zip"
      base_work(account, user, output, subject, filename)
    end
  end

  def self.enqueued_translated
    I18n.t('txt.job.export_job.enqueued')
  end

  def self.throttled_translated
    I18n.t('txt.job.export_job.throttled')
  end

  def self.latest_translated
    I18n.t('txt.job.export_job.latest')
  end
end
