class Zendesk::Cms::Importer
  class ImportError < StandardError
  end

  LOCALE_MATCH = /\A([a-z]{2,3})(?:-([a-z]{4}))?(?:-([a-z]{2}|\d{3}))?((?:-(?:[a-z]{5,8}|\d[a-z0-9]{3}))*)((?:-[a-wyz]{1}(?:-[a-z0-9]{2,8})+)*)(-x(?:-[a-z0-9]{1,8})+)?\z/i.freeze

  attr_accessor :errors, :records, :data, :csv_data, :account, :locale, :language_column_pos

  def initialize(account, csv_data)
    @account = account
    @csv_data = csv_data
    @data = nil
    @locale = nil
    @language_column_pos = nil
    @errors = []
    @records = []
  end

  def process
    build_data
    process_columns_names
    process_rows
    @errors.empty? ? save_records : false
  end

  def errors_full_messages
    messages = []
    @errors.each do |model|
      if model.is_a?(String)
        messages << model.to_s
      elsif model.is_a?(ActiveRecord::Base)
        model.errors.select { |key, _value| key != 'events' }.uniq.each do |key, value|
          messages << "#{key.humanize} #{value}"
        end
      end
    end
    messages
  end

  def language(user_locale = nil)
    @locale.try(:localized_name, display_in: user_locale || I18n.translation_locale)
  end

  private

  def build_data
    # ZD:492985 coerce UTF-8 and remove BOM
    csv_data.to_utf8!
    csv_data.sub!("\xEF\xBB\xBF".force_encoding(Encoding::UTF_8), '') # strip BOM if present
    @data = CSV.parse(csv_data, col_sep: ',', encoding: Encoding::UTF_8)
  end

  def process_columns_names
    if data.first[0] != "Title"
      raise ImportError, ::I18n.t('txt.admin.lib.zendesk.cms.importer.expected_column_missing_title', locale: @account.translation_locale)
    else
      locale = pos = nil
      data.first.each_with_index do |name, index|
        raise ImportError, ::I18n.t('txt.admin.lib.zendesk.cms.importer.empty_column_name', locale: @account.translation_locale) if name.blank?

        if match = name.split(' ').first.match(LOCALE_MATCH)
          locale = match[0]
          pos = index
        end
      end

      raise ImportError, ::I18n.t('txt.admin.lib.zendesk.cms.importer.expected_column_missing_language_text', locale: @account.translation_locale) if pos.nil?
      raise ImportError, ::I18n.t('txt.admin.lib.zendesk.cms.importer.language_not_active_for_account', locale: @account.translation_locale) unless (locale = account.available_languages.find { |tl| locale.casecmp(tl.locale.downcase).zero? })

      @locale = locale
      @language_column_pos = pos
    end
  end

  def process_rows
    data[1..-1].each do |row|
      title = get_title(row)
      text = get_text(row)
      next if text.blank? # Ignore if no value
      # process row
      if (cms_text = find_cms_text_by_name(title))
        if (cms_variant = find_cms_variant(cms_text))
          cms_variant.value = text
        else
          cms_variant = cms_text.variants.new(account_id: @account.id, is_fallback: false, value: text, translation_locale_id: @locale.id, active: true)
        end
        cms_variant.valid? ? (@records << cms_variant) : (@errors << cms_variant)
      else
        @errors << ::I18n.t('txt.admin.lib.zendesk.cms.importer.title_not_found', title: title, locale: @account.translation_locale)
      end
    end
  end

  def save_records
    @records.each do |variant|
      if variant.save && variant.text.touch_without_callbacks
        DC::Synchronizer.synchronize_snippet(variant.text)
      end
    end
  end

  def get_title(row)
    row[0]
  end

  def get_text(row)
    row[@language_column_pos]
  end

  def find_cms_text_by_name(title)
    @account.cms_texts.active.find_by_name(title)
  end

  def find_cms_variant(cms_text)
    cms_text.variants.find_by_translation_locale_id(@locale.id)
  end
end
