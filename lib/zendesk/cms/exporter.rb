require 'zip/zip'
require 'zip/zipfilesystem'

module Zendesk::Cms::Exporter
  include Zendesk::CsvScrubber

  def self.build_export(account)
    Zip::File.open("#{account.subdomain}.zip", Zip::File::CREATE) do |zos|
      account.available_languages.each do |language|
        zos.get_output_stream("#{language.locale}.csv") do |file|
          build_for_language(account, file, language)
        end
      end
    end
    "#{account.subdomain}.zip"
  end

  def self.build_for_language(account, file, language)
    file.puts CSV.generate_line(["Title", "Default language", "Default text", "#{language.locale} text", "Variant status", "Placeholder"], force_quotes: true)
    account.cms_texts.each do |text|
      scrubbed_values = export_values(text, language.id).map { |v| Zendesk::CsvScrubber.scrub_output(v.to_s) }
      file.puts CSV.generate_line(scrubbed_values, force_quotes: true)
    end
  end

  private_class_method def self.export_values(text, locale_id)
    if variant = text.variants.find_by_translation_locale_id(locale_id)
      [text.name, text.fallback.name, text.fallback.value, variant.value, variant.status, text.fully_qualified_identifier]
    else
      [text.name, text.fallback.name, text.fallback.value, "", "", text.fully_qualified_identifier]
    end
  end
end
