class Zendesk::Cms::ImportJob
  extend ZendeskJob::Resque::BaseJob
  extend Zendesk::ExternalErrorTranslations

  priority :medium

  class << self
    def work(account_id, requester_id, token)
      account = Account.find(account_id)
      ActiveRecord::Base.on_shard(account.shard_id) do
        user = account.users.find(requester_id)
        Time.use_zone(user.time_zone) { process(account, user, token) }
      end
    end

    def args_to_log(account_id, requester_id, token)
      { account_id: account_id, requester_id: requester_id, token: token }
    end

    private

    def process(account, user, token)
      token       = get_token(user, token)
      time        = token.created_at.to_format(time_format: "%F #{user.time_format_string}")
      identifier  = "account_id:#{account.id}-requester_id:#{user.id}"
      subject     = I18n.t('txt.lib.zendesk.cms.import_job.your_dynamic_content_import_for', account_subdomain: account.subdomain, locale: user.translation_locale)

      begin
        csv_data = get_csv_data(token)
        importer = Zendesk::Cms::Importer.new(account, csv_data)
        if importer.process
          JobsMailer.deliver_job_complete(user, identifier, subject, message(user, importer.language(user.translation_locale), time, I18n.t('txt.lib.zendesk.cms.import_job.upload_was_successfully_completed', locale: user.translation_locale)))
        else
          JobsMailer.deliver_job_complete(user, identifier, subject, message(user, importer.language(user.translation_locale), time, "#{I18n.t('txt.lib.zendesk.cms.import_job.import_unsuccessful', locale: user.translation_locale)}\n\n #{I18n.t('txt.lib.zendesk.cms.import_job.the_error_was', locale: user.translation_locale)}\n\n #{importer.errors_full_messages.join("\n")}"))
        end
      rescue CSV::MalformedCSVError => e
        translated_error = translate_error(e.message, user.translation_locale)
        JobsMailer.deliver_job_complete(user, identifier, subject, message(user, importer.language(user.translation_locale), time, I18n.t('txt.email.import_job.bad_format.message', error_message: translated_error, locale: user.translation_locale)))
      rescue Zendesk::Cms::Importer::ImportError => e
        JobsMailer.deliver_job_complete(user, identifier, subject, message(user, importer.language(user.translation_locale), time, I18n.t('txt.email.import_job.bad_format.message', error_message: e.message, locale: user.translation_locale)))
      rescue StandardError => e
        JobsMailer.deliver_job_complete(user, identifier, subject, message(user, importer.try(:language, user.translation_locale), time, "#{I18n.t('txt.lib.zendesk.cms.import_job.please_try_again', locale: user.translation_locale)}\n\n#{I18n.t('txt.lib.zendesk.cms.import_job.we_have_been_notified_about_this', locale: user.translation_locale)}\n\n #{e.message}"))
        raise e
      end
    end

    def get_token(user, token)
      user.account.upload_tokens.find_by_value(token)
    end

    def get_csv_data(token)
      attachment = token.attachments.first
      attachment.current_data
    end

    def message(user, language, time, msg)
      content = "#{I18n.t('txt.lib.zendesk.cms.import_job.upload_time', time: time, locale: user.translation_locale)}\n"
      content << "#{I18n.t('txt.lib.zendesk.cms.import_job.language', language: language, locale: user.translation_locale)}\n" if language
      content << msg
      content
    end
  end
end
