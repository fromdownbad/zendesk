# frozen_string_literal: true

# Implements Arturo Rollouts based on a percentage of requests
# as opposed to the usual account based Arturo approach.
#
# DON'T use this if you need all users from an account
# to have the same experience with respect to your changes. Even the same
# user from an account would experience differences between requests
# if you're implementing an Arturo Slider for a Customer Facing feature.
#
module Zendesk
  class ArturoSlider
    attr_reader :name
    ##
    # @param name [String, Symbol] arturo name
    # @param valid_range [Range] minimum and maximumn allowed values
    #   as protection from human errors
    # @param disabled_if [#call] A proc that receives the slider and
    #   decide if it should be disabled based on a specific condition
    #
    # @example Running queries on primary database
    #   slider = Zendesk::ArturoSlider.new(
    #      :run_on_primary_slider,
    #      valid_range: (0, 10), # limits sampling to 10%
    #      disable_if: ->(slider) { # exclude some whale from eligibility
    #        slider.arturo.external_beta_subdomains.include?('-some_whale')
    #      }
    #   )
    #   run_query if slider.eligible?
    #
    def initialize(name, valid_range: nil, disable_if: nil)
      @name         = name
      @valid_range  = valid_range || Range.new(0, 100)
      @disable_if   = disable_if
      Rails.logger.debug("[slider] initializing #{@name} with valid_range: #{@valid_range}")
    end

    ##
    # The slider can work as a kill switch.
    # Just set it to off or zero rollout percentage.
    # Note: You won't be able to switch it off sliding to zero
    #   if the minimum valid value is over zero.
    #   You'd have to turn the arturo off in this case.
    #
    # @return [Boolean] if the slider is enabled
    #
    def enabled?
      return unless arturo
      return if @disable_if&.call(self)

      rollout?
    end

    ##
    # @return [Boolean] if the event is eligible
    #
    def was_drawn?
      return false unless enabled?

      random = Kernel.rand * 100
      Rails.logger.debug("random: #{random}, value: #{value}")
      random <= value
    end

    ##
    # Returns a rollout percentage with protection from human error
    # It guarantees the value will be within what is given for :valid_range
    #
    # @return [Integer] the rollout percentage
    #
    def value
      @value ||= [minimum, maximum].max
    end

    def arturo
      @arturo ||= ::Arturo::Feature.find_feature(name)
    end

    private

    def minimum
      [@valid_range.min, 0].max
    end

    def maximum
      [@valid_range.max, rollout_percentage].min
    end

    def rollout?
      arturo&.rollout? && value > 0
    end

    def rollout_percentage
      return 0 unless arturo

      per_pod_rollout_percentage.presence || arturo.deployment_percentage
    end

    ##
    # per_pod_threshold is a private method
    # TODO: expose it in zendesk_arturo gem
    # @return [Integer] the percentage rollout for the current pod
    #
    def per_pod_rollout_percentage
      arturo.send(:per_pod_threshold, ENV['ZENDESK_POD_ID'].to_i)
    end
  end
end
