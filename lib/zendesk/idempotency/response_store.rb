module Zendesk
  module Idempotency
    class ResponseStore
      ITEM_SCHEMA = {
          type: 'array',
          items: [
            { type: 'string' },
            { type: 'integer', minimum: 200, maximum: 302 },
            { type: 'object' },
            { type: 'string' }
          ],
          minItems: 4,
          maxItems: 4
      }.freeze

      def initialize(backend:, logger:, expiry: 15.minutes)
        @backend = backend
        @expiry = expiry
        @logger = logger
      end

      def get(key)
        client.get(key).try { |value| parse_cached_response(key, value) }
      rescue StandardError => error
        log_error(error, 'Store key could not be read', key: key)
        nil
      end

      def touch(key)
        client.expire(key, @expiry)
      end

      def set(key, response, signature:)
        status, headers, body = response.to_a
        client.set(key, [signature, status, headers, body.body].to_json, ex: @expiry)
        nil
      rescue StandardError => error
        log_error(error, 'Store key could not be set', key: key)
        nil
      end

      private

      delegate :client, to: :@backend

      def parse_cached_response(key, value)
        ActiveSupport::JSON.decode(value).tap { |json| JSON::Validator.validate!(ITEM_SCHEMA, json) }
      rescue JSON::ParserError, JSON::Schema::ValidationError => error
        log_error(error, 'Store key contained an invalid response value', key: key, value: value)
        nil
      end

      def log_error(error, message, **metadata)
        @logger.error(error, message, location: self, metadata: metadata)
      end
    end
  end
end
