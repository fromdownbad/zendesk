module Zendesk
  module Idempotency
    class ErrorLog
      def self.error(error, message, **metadata)
        data = metadata.map { |pair| pair.join(': ') }.join(', ')
        Rails.logger.info("#{message} (#{error.message}) - #{data}")
      end
    end
  end
end
