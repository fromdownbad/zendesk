module Zendesk
  module Idempotency
    class ParameterMismatchError < StandardError; end

    module ErrorHandling
      extend ActiveSupport::Concern

      included do
        rescue_from ParameterMismatchError do
          render status: 400, json: {
            error: 'IdempotentRequestError',
            description: ::I18n.t('txt.error_message.controllers.idempotency.request_mismatch')
          }
        end
      end
    end
  end
end
