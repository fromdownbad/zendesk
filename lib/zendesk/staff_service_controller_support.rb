module Zendesk
  module StaffServiceControllerSupport
    def self.included(base)
      base.allow_subsystem_user :metropolis
      base.before_action :require_requester_id
    end

    private

    def requester
      @requester ||= current_account.users.find(params[:requester_id])
    end

    def require_requester_id
      return if params[:requester_id].present?
      render status: :bad_request,
             json: {
               error: 'MissingRequesterId',
               description: "'requester_id' is a required field"
             }
    end
  end
end
