require 'zendesk/internal_api/client'

module Zendesk::Sms
  class InternalApiClient
    EXTENDED_REQUEST_TIMEOUT = 10.seconds.freeze
    RESCUABLE_ERRORS = [Faraday::TimeoutError, Kragle::ClientError, Kragle::BadGateway].freeze

    attr_reader :connection, :account

    def initialize(account)
      @account = account
      @connection = Kragle.new('sms', shared_cache: false)
      @connection.url_prefix = account.url(ssl: true, mapped: false)
    end

    def send_notification(trigger_id, event, params = {})
      connection.post do |req|
        req.url "/api/v2/channels/sms/internal/triggers/#{trigger_id}/messages"
        req.options.timeout = EXTENDED_REQUEST_TIMEOUT if account.has_sms_extended_notification_timeout?
        req.body = {
          message: {
            phone_number_id: params[:phone_number_id],
            user_id: params[:user_id],
            body: params[:body],
            trigger_id: trigger_id,
            ticket_id: event.ticket_id,
            event_id: event.id
          }
        }
      end
    rescue *RESCUABLE_ERRORS => ex
      Rails.logger.warn("Sms::InternalApiClient send_notification failure - ticket_id: #{event.ticket_id}, event: #{event}, params: #{params}, error: #{ex.message}")
      statsd_client.increment('error', tags: ["error:#{ex.class}"])
      nil
    rescue StandardError => ex
      Rails.logger.warn("Sms::InternalApiClient send_notification unrescuable error - #{ex.message}")
      statsd_client.increment('unrescuable_sms_error', tags: ["error:#{ex.class}", 'method:send_notification'])
      raise ex
    end

    def send_request_message(ticket_id, agent_id, location, outgoing_text)
      connection.post do |req|
        req.url "/api/v2/channels/#{location}/internal/tickets/#{ticket_id}/reply"
        req.headers['X-SMS-Path'] = "/api/v2/channels/sms/internal/tickets/#{ticket_id}/reply"
        req.body = {
          message: {
            agent_id: agent_id,
            body: outgoing_text
          }
        }
      end
    rescue *RESCUABLE_ERRORS => ex
      Rails.logger.warn("Sms::InternalApiClient request failed to send request message for #{ticket_id}, error: #{ex.message}")
      statsd_client.increment("error", tags: ["error:#{ex.class}"])
      nil
    rescue StandardError => ex
      Rails.logger.warn("Sms::InternalApiClient send_notification unrescuable error - #{ex.message}")
      statsd_client.increment('unrescuable_sms_error', tags: ["error:#{ex.class}", 'method:send_request_message'])
      raise ex
    end

    private

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["internal_api_client"], tags: ["internal_api_client:sms"])
    end
  end
end
