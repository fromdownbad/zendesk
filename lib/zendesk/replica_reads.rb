module Zendesk
  module ReplicaReads
    def self.included(controller)
      controller.around_action :reader_for_api_call
      controller.extend(ClassMethods)
    end

    protected

    def reader_for_api_call(&block)
      if !current_account || current_account.created_at < 10.minutes.ago
        # TODO: Remove this nonsense, Occam always executes on Reader
        force_enabled = true if current_account.try(:id) == 34989 && self.class == Api::V2::Rules::ViewsController && action_name == "execute" # view executions on writer for uber are too slow
        on_reader_or_writer(read_request?, force_enabled, &block)
      else
        yield
      end
    end

    def read_request?
      ["OPTIONS", "HEAD", "GET"].include?(request.method) || self.class.treat_as_read_actions.include?(action_name.to_sym)
    end

    def on_reader_or_writer(is_read_request, force_enabled, &block)
      if !replica_reads_disabled? && is_read_request
        message = force_enabled ? "force enabled" : "aurora"
        Rails.logger.info("Zendesk::ReaderState: reader -- #{message}")
        ActiveRecord::Base.with_on_slave_by_default_override(&block)
      else
        message = is_read_request ? "reader disabled" : "not a read request"
        Rails.logger.info("Zendesk::ReaderState: writer -- #{message}")
        yield
      end
    end

    def replica_reads_disabled?
      Rails.env.development? || Rails.env.test?
    end

    module ClassMethods
      def treat_as_read_actions
        @treat_as_read_actions ||= Set.new
      end

      def treat_as_read_request(*actions)
        treat_as_read_actions.merge(actions)
      end
    end
  end
end
