module Zendesk
  class UserPortalState
    def initialize(account)
      @account = account
    end

    def send_verify_email?(user)
      @account.help_center_enabled? || @account.web_portal_restricted? ||
        user.try(:is_agent?)
    end

    def send_suspend_verify_email?(user)
      send_verify_email?(user)
    end

    def send_identity_verification_email?(identity)
      send_verify_email?(identity.user)
    end

    def send_initial_identity_verification_email?(identity)
      send_verify_email?(identity.user) && identity.user.send_verify_email && !identity.is_verified?
    end

    def show_auth_cancel_link?
      has_web_portal_or_help_center?
    end

    alias :show_anonymous_modify_csat_link? :show_auth_cancel_link?
    alias :can_users_signup? :show_auth_cancel_link?

    def show_help_center_logo?
      !@account.help_center_disabled?
    end
    alias :show_help_center_favicon? :show_help_center_logo?

    def dropbox_uses_help_center_search?
      !@account.help_center_disabled?
    end

    def shows_search_and_topic_suggestion_in_dropbox?
      !has_web_portal_and_help_center_disabled?
    end

    private

    def has_web_portal_and_help_center_disabled? # rubocop:disable Naming/PredicateName
      @account.web_portal_and_help_center_disabled?
    end

    def has_web_portal_or_help_center? # rubocop:disable Naming/PredicateName
      !@account.web_portal_disabled? || @account.help_center_enabled?
    end

    def has_web_portal_disabled? # rubocop:disable Naming/PredicateName
      @account.web_portal_disabled?
    end

    def has_web_portal_restricted? # rubocop:disable Naming/PredicateName
      @account.web_portal_restricted?
    end
  end
end
