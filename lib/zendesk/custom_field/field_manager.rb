module Zendesk::CustomField
  class UnknownCustomFieldTypeError < ArgumentError
  end

  class FieldManager
    include Zendesk::DynamicContentFieldsHelper
    class_attribute :raw_parameters

    def self.type(name)
      return default_type if name.blank?

      if type_map.values.member?(name)
        name.constantize
      else
        raise UnknownCustomFieldTypeError, "Unknown custom field type: #{name}"
      end
    end

    def self.apply(options)
      # Changes from short hand to class name
      if type_map[options[:type]]
        options[:type] = type_map[options[:type]]
      end

      boolean_attr_list.each do |bool|
        if options.key?(bool)
          options["is_#{bool}"] = options.delete(bool)
        end
      end

      options
    end

    # Create a new ticket field manager.
    #
    # account - the Account on which to manage ticket fields.
    #
    def initialize(account)
      @account = account
    end

    # Build a new ticket field.
    #
    # options - the Hash set of options with which to build the TicketField.
    #
    # If a `:type` is specified in `options`, it will be used to determine which
    # TicketField subclass to build. Default is FieldText.
    #
    # Returns the new TicketField.
    def build(options = {})
      options = map(options)
      type = self.class.type(options.delete(:type))
      custom_field_options = options.delete(:custom_field_options) || []

      type.new(options) do |f|
        f.account = @account

        # silently eat custom_field_options if we're not creating a dropdown
        if f.respond_to?(:custom_field_options)
          custom_field_options.each do |cfo|
            f.custom_field_options.build(cfo)
          end
        end
      end
    end

    def update(ticket_field, options = {})
      options ||= {}
      options.delete(:type) # to mirror build
      options = map(options)

      if options[:custom_field_options]
        # if this field can not have custom_field_options return 4XX
        if ticket_field.respond_to?(:custom_field_options)
          update_custom_field_options(ticket_field, options[:custom_field_options])
        else
          ticket_field.errors.add(:base, Api.error('txt.admin.models.ticket_fields.field_tagger.invalid_custom_field_options_param', field_name: ticket_field.title, error: 'Invalid'))
        end
      end

      ticket_field.attributes = options.except(:custom_field_options)
      ticket_field
    end

    def update_custom_field_options(ticket_field, custom_field_options)
      if custom_field_options.empty?
        ticket_field.add_persistent_error(Api.error('txt.admin.models.ticket_fields.field_tagger.must_contain_least_one_option', error: 'EmptyValue'))
      else
        update_custom_field_options_by_value(ticket_field, custom_field_options)
      end
    end

    # This will update the options for a given ticket field based on the value.
    #
    # First check if the option has been deleted before, and if so undelete it.
    # Then we update the existing option if it is present, or create a new one
    # if necessary.  Finally remove it from the options map so at the end we
    # know which options need to be removed.
    #
    # Params:
    #  - ticket_field: The TicketField to update
    #  - custom_field_options: An array of name/value pairs that will be turned
    #    into ticket field options
    #
    def update_custom_field_options_by_value(ticket_field, custom_field_options)
      return unless all_options_have_normalized_value?(ticket_field, custom_field_options)

      ZendeskAPM.trace(
        'ticket_field_manager.update_custom_field_options_by_value',
        service: TicketField::APM_SERVICE_NAME
      ) do |span|
        span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)

        all_field_options = CustomFieldOption.with_deleted { ticket_field.custom_field_options.reload }
        old_options_map, deleted_map, index_map = build_maps_for_update(all_field_options)

        span.set_tag("zendesk.account_id",              ticket_field.account.id)
        span.set_tag("zendesk.account_subdomain",       ticket_field.account.subdomain)
        span.set_tag("ticket_fields.all_field_options_size", all_field_options.size)
        span.set_tag("ticket_fields.deleted_map_size", deleted_map.size)

        # take one pass through the new options, building new fields as needed and getting the new positions right.
        custom_field_options.each_with_index do |field_option, i|
          resulting_option, index = create_or_update_option(field_option,
            deleted_map, old_options_map, all_field_options, index_map)

          if index != i
            # swap resulting_option into position
            tmp = all_field_options[i]
            all_field_options.target[i] = resulting_option
            all_field_options.target[index] = tmp
            index_map[tmp] = index
          end

          index_map[resulting_option] = i
        end

        # any old choices that weren't referenced will be deleted
        old_options_map.each_value(&:mark_as_deleted)
      end
    end

    def map(options)
      return {} unless HashParam.ish?(options)
      map_dc_parameters(raw_parameters, options, true)
      self.class.apply(options)
    end

    private

    # When we update ticket field options, we have three possible scenarios:
    # 1. The option was previously here, now deleted
    # 2. The option is current, lets update it
    # 3. We need to create the option
    #
    # This will build three maps that we can reference in these various cases.
    # In the deleted and old maps, the key will be the downcased version of the field value.
    # As of Ruby 2.4, this will correctly downcase Cyrillic characters so we correctly
    # update existing values instead of inserting and deleting one that will result in a
    # key constraint violation.
    def build_maps_for_update(field_options)
      old_options_map = {}
      deleted_map = {}
      index_map = {}

      field_options.each_with_index do |option, index|
        index_map[option] = index # create map of option -> index for easy swapping

        if option.deleted? # create multimap of deleted items, indexed by value, for quick undeletes
          deleted_map[option.value.downcase] ||= []
          deleted_map[option.value.downcase] << option
        else
          old_options_map[option.value.downcase] = option # map to quickly get from existing option's value to the option object
        end
      end

      [old_options_map, deleted_map, index_map]
    end

    # Make sure that all of the passed in options have a valid value, and
    # normalize that value to make sure any comparisons behave as expected.
    def all_options_have_normalized_value?(ticket_field, field_options)
      field_options.each do |field_option|
        if field_option[:value].blank?
          ticket_field.add_persistent_error(Api.error('txt.admin.models.ticket_fields.field_tagger.must_have_tag', error: 'BlankValue'))
          return false
        end
        field_option[:value] = CustomFieldOption.normalize_value(field_option[:value], @account)
      end
      true
    end

    # Determine which case our option is in and perform the correct action.
    # - Undelete it if necessary
    # - Update the existing option
    # - Remove the option from the map now that its been processed
    #
    # OR
    #
    # - create our new option
    #
    # This will return the option and its position
    def create_or_update_option(field_option, deleted_options, old_options, all_options, indexes)
      field_option_attrs = field_option.except(:id)
      tag_value = field_option[:value].downcase

      # see if choice is in deleted list if so use and undelete
      if deleted_option = deleted_options[tag_value].try(:shift)
        # undelete
        deleted_option.attributes = field_option_attrs
        deleted_option.mark_as_undeleted
        old_options.delete(deleted_option.value)
        [deleted_option, indexes[deleted_option]]
      elsif existing_option = old_options.delete(tag_value)
        # update an existing option
        existing_option.attributes = field_option_attrs
        old_options.delete(existing_option.value)
        [existing_option, indexes[existing_option]]
      else
        # create new option
        [all_options.build(field_option_attrs), all_options.length - 1]
      end
    end
  end
end
