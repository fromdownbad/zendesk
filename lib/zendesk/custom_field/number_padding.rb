module Zendesk
  module CustomField
    module NumberPadding
      NEG = "-".freeze
      POS = "0".freeze
      LEFT_SPAN = 12
      RIGHT_SPAN = 4

      # NOTE: updated Regex
      def self.pad(val, left_span = LEFT_SPAN, right_span = RIGHT_SPAN)
        if val < 0
          ret = NEG
          val = -val
        else
          ret = POS
        end

        number = if val.is_a?(Integer)
          "%0#{left_span}d" % val
        else
          "%0#{left_span + 1 + right_span}.#{right_span}f" % val
        end

        ret + number
      end
    end
  end
end
