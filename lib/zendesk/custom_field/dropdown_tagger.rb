module Zendesk
  module CustomField
    module DropdownTagger
      def self.included(base)
        base.before_save :assign_custom_field_options_positions # Must happen before touch_updated_at_on_options_change or we'll return stale data
        base.before_save :touch_updated_at_on_options_change
        base.before_save :bulk_allocate_custom_field_option_ids
        base.around_save :filter_key_constraint_violation
        base.validate :validate_persistent_errors
      end

      def assignable_value?(some_value)
        !!custom_field_options.detect { |o| o.value == some_value }
      end

      def self.validate_unique(errors, field, tags)
        # This code is basicly doing `tags.compact.select{ |e| tags.count(e) > 1 }.uniq` but MUCH FASTER
        duplicates = tags.compact.group_by { |e| e }.select { |_k, v| v.size > 1 }.keys

        if duplicates.any?
          error_translated = if duplicates.size == 1
            Api.error('txt.admin.models.ticket_fields.field_tagger.unique_tag_singular', error: 'DuplicateValue', tags: "<strong>#{CGI.escapeHTML(duplicates.join(', '))}</strong>")
          else
            Api.error('txt.admin.models.ticket_fields.field_tagger.unique_tag_plural', error: 'DuplicateValue', tags: "<strong>#{CGI.escapeHTML(duplicates.join(', '))}</strong>")
          end
          errors.add(:field_options, error_translated.html_safe)
        end

        if tags.any?(&:blank?)
          errors.add(:field_options, Api.error('txt.admin.models.ticket_fields.field_tagger.must_have_tag', error: 'BlankValue'))
        end

        if field.is_active? || field.new_record?
          Zendesk::CustomField::DropdownTagger.validate_to_activate(errors, field, tags)
        end
      end

      def self.validate_to_activate(errors, field, tags)
        all_tags = field.class.get_all_tags(field)
        tags_already_used = (all_tags & tags).to_a
        max_tags_to_show = 10
        if tags_already_used.any?
          message_translated = if tags_already_used.size == 1
            Api.error('txt.admin.models.ticket_fields.tagger_fields.tag_already_used_singular', error: 'ReservedValue', tags: "<strong>#{tags_already_used.join(', ')}</strong>")
          elsif tags_already_used.size > max_tags_to_show
            # Avoiding cookie overflow when there's a very high number of duplicates.
            #
            # In this code if there are 13 tags_already_used we only return the first 10 (max_tags_to_show = 10)
            #   likewise the other_tags_count == 3 to match the number of tags we don't show the user
            example_tags = tags_already_used[0..max_tags_to_show - 1]
            other_tags_count = tags_already_used.size - example_tags.size
            Api.error('txt.admin.models.ticket_fields.tagger_fields.many_duplicate_tags', error: 'ReservedValue', example_tags: "<strong>#{example_tags.join(', ')}</strong>", other_tags_count: other_tags_count)
          else
            Api.error('txt.admin.models.ticket_fields.tagger_fields.tag_already_used_plural', error: 'ReservedValue', tags: "<strong>#{tags_already_used.join(', ')}</strong>")
          end
          errors.add(:base, message_translated.html_safe)
        end
      end

      def add_persistent_error(message)
        @persistent_errors ||= []
        @persistent_errors << message
      end

      protected

      def validate_persistent_errors
        # include any errors caught by update_custom_field_options
        if @persistent_errors
          @persistent_errors.each { |error| errors.add(:custom_field_options, error) }
          @persistent_errors.clear
        end
      end

      def validate_custom_field_options
        # needed because we let the rails autosave option on the custom_field_options association handle
        # the soft_deletion and this validation happens before the custom_field_option save happens
        custom_field_options = self.custom_field_options.reject(&:deleted?)

        # NOTE: just for TicketField?
        if errors[:"custom_field_options.value"].present?
          value_errors = errors.delete(:"custom_field_options.value")
          value_errors.each { |e| errors.add(:field_options, e) }
          # We already have an error and there's no need to continue
          # Also, other validations below can fail with a potential nil 'value'
          return
        end

        # extraneous error key, choice values will be validated individually later
        errors.delete(:"dropdown_choices.value")

        ZendeskAPM.trace(
          'dropdown_tagger.validate_customer_field_options',
          service: TicketField::APM_SERVICE_NAME
        ) do |span|
          span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
          span.set_tag("zendesk.account_id",              account.id)
          span.set_tag("zendesk.account_subdomain",       account.subdomain)
          span.set_tag("dropdown_tagger.custom_field_option_class", custom_field_option_class)

          if custom_field_options && custom_field_options.any? { |o| o.name.to_s.strip == '' }
            span.set_tag("dropdown_tagger.blank_value_error", true)
            errors.add(:field_options, Api.error('txt.admin.models.ticket_fields.field_tagger.must_have_title', error: 'BlankValue'))
          end

          if custom_field_options.detect { |o| o.value =~ SPECIAL_CHARS }
            span.set_tag("dropdown_tagger.special_chars_error", true)
            if account && !account.has_special_chars_in_custom_field_options?
              errors.add(:field_options, Api.error('txt.admin.models.ticket_fields.field_tagger.cannot_contain_special_characters', error: 'ValueNotAccepted'))
            end
          elsif custom_field_options.detect { |o| o.value =~ /[^[:graph:]]/ }
            span.set_tag("dropdown_tagger.special_characters_error", true)
            # cannot_contain_non_printable_characters
            errors.add(:field_options, Api.error('txt.admin.models.ticket_fields.field_tagger.cannot_contain_special_characters', error: 'ValueNotAccepted'))
          end

          # normalize_tags will turn a string into an array, but here we want to leave it a string
          tags = custom_field_options.map { |o| TagManagement.downcase_tag_string(o.value, account) }
          Zendesk::CustomField::DropdownTagger.validate_unique(errors, self, tags)
        end
      end

      def validate_custom_field_options_are_not_empty
        if custom_field_options.empty?
          errors.add(:field_options, Api.error('txt.admin.models.ticket_fields.field_tagger.must_contain_least_one_option', error: 'EmptyValue'))
        end
      end

      # Need to reload custom field options
      # since setting the deleted_at (mark_as_deleted)
      # and then calling save will not remove
      # that option from the association collection
      def remove_deleted
        custom_field_options.target.reject!(&:deleted?)
      end

      private

      def assign_custom_field_options_positions
        # don't included soft deleted options since reassigning their
        # positions will create unnecessary database load
        options_to_use = custom_field_options.reject(&:deleted?)

        sorted_collection = begin
          if sort_alphabetically
            options_to_use.sort_by { |opt| TagManagement.downcase_tag_string(opt.name, account) }
          else
            options_to_use
          end
        end

        sorted_collection.each_with_index do |option, index|
          option.position = index
        end
      end

      def bulk_allocate_custom_field_option_ids
        custom_field_option_class.reserve_global_uids(custom_field_options.to_a.count(&:new_record?))
      end

      def touch_updated_at_on_options_change
        self.updated_at = Time.current if custom_field_options.any? { |option| option.new_record? || option.changed? }
      end

      def filter_key_constraint_violation
        yield
      rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation => e
        error_translated = if e.duplicate_value.to_s =~ /\A\d+-(.*)\z/
          Api.error('txt.admin.models.ticket_fields.field_tagger.unique_tag_singular', error: 'DuplicateValue', tags: "<strong>#{$1}</strong>")
        else
          Api.error('txt.admin.models.ticket_fields.field_tagger.unique_tag', error: 'DuplicateValue')
        end
        errors.add(:field_options, error_translated)
        raise ActiveRecord::RecordInvalid, self
      end
    end
  end
end
