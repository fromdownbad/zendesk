module Zendesk::CustomField
  class CustomFieldManager < FieldManager
    self.raw_parameters = ["description", "title"].freeze

    def self.type_map
      {
        "checkbox" => "CustomField::Checkbox",
        "date"     => "CustomField::Date",
        "decimal"  => "CustomField::Decimal",
        "integer"  => "CustomField::Integer",
        "regexp"   => "CustomField::Regexp",
        "text"     => "CustomField::Text",
        "textarea" => "CustomField::Textarea",
        "tagger"   => "CustomField::Dropdown",
        "dropdown" => "CustomField::Dropdown"
      }
    end

    def self.boolean_attr_list
      @boolean_attr_list ||= [:active, :required]
    end

    def self.default_type
      CustomField::Text
    end

    def initialize(account, current_user, owner)
      super(account)
      @owner = owner
      @current_user = current_user
    end

    def build(options = {})
      options.delete(:is_system) if non_system_user?

      record = super(options)
      record.owner = @owner
      record
    end

    def update(field, options = {})
      options.slice!(:active, :position) if non_system_user? && field.is_system?
      super(field, options)
    end

    def reorder(custom_field_ids, type)
      ActiveRecord::Base.transaction do
        @account.custom_fields.active.send("for_#{type}").each do |field|
          custom_field_id = custom_field_ids & [field.id, field.id.to_s]
          if custom_field_id.present?
            field.update_attribute(:position, custom_field_ids.index(custom_field_id[0]))
          else
            field.update_attribute(:position, custom_field_ids.size + field.position)
          end
        end
      end
    end

    def update_custom_field_options(custom_field, dropdown_choices)
      cf_dropdown_choices = CustomField::DropdownChoice.with_deleted { custom_field.dropdown_choices.reload }

      # map to quickly get from existing choice's ID to the choice object
      old_choices_map = cf_dropdown_choices.each_with_object({}) { |c, h| h[c.id] = c }

      # create map of choice -> index for easy swapping
      index_map = {}
      cf_dropdown_choices.each_with_index { |choice, i| index_map[choice] = i }

      # create multimap of deleted items, indexed by value, for quick undeletes
      deleted_map = cf_dropdown_choices.select(&:deleted?).group_by(&:value)

      # take one pass through the new options, building new fields as needed and getting the new positions right.
      dropdown_choices.each_with_index do |dc, i|
        unless dc.key?(:id)
          custom_field.add_persistent_error(Api.error('activerecord.errors.models.custom_field.dropdown.attributes.custom_field_options.id_not_specified', error: 'BlankValue'))
          return
        end

        if dc[:id].blank?
          # either restore deleted choice, or create a new one
          deleted_list = deleted_map[dc[:value]]
          choice = deleted_list.shift if deleted_list
          if choice
            choice.attributes = dc.except(:id)
            choice.mark_as_undeleted
            old_index = index_map[choice]
            old_choices_map.delete(choice.id)
          else
            choice = cf_dropdown_choices.build(dc.except(:id))
            old_index = cf_dropdown_choices.length - 1
          end
        else
          # update an existing choice.
          id = dc[:id].to_i
          choice = old_choices_map[id]
          unless choice
            custom_field.add_persistent_error(:invalid)
            return
          end
          old_choices_map.delete(id)
          choice.attributes = dc.except(:id)
          old_index = index_map[choice]
        end

        if old_index != i
          # swap choice into position
          tmp = cf_dropdown_choices[i]
          cf_dropdown_choices.target[i] = choice
          cf_dropdown_choices.target[old_index] = tmp
          index_map[tmp] = old_index
        end

        index_map[choice] = i
      end

      # any old choices that weren't referenced will be deleted
      old_choices_map.each_value(&:mark_as_deleted)
    end

    private

    def non_system_user?
      !@current_user.is_system_user?
    end
  end
end
