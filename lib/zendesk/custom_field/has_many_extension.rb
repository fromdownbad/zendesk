module Zendesk::CustomField
  module HasManyExtension
    def as_json(options = {})
      destroyed_values_by_cf_field_id = {}

      values_by_cf_field_id = each_with_object({}) do |e, a|
        a[e.cf_field_id] = e
        destroyed_values_by_cf_field_id[e.cf_field_id] = true if e.destroyed?
      end

      # avoid N+1
      custom_fields = options[:custom_fields] || options[:account].custom_fields_for_owner(proxy_owner.class.name)

      custom_fields.each_with_object({}) do |cf_field, accum|
        accum[cf_field.key] = if (cf_value = values_by_cf_field_id[cf_field.id]) && !destroyed_values_by_cf_field_id[cf_field.id]
          if options[:title] && cf_field.type == "Dropdown"
            cf_field.liquid_title(cf_value)
          else
            cf_value.as_json["value"]
          end
        else
          cf_field.value_as_json(nil)
        end
      end
    end

    def to_liquid
      key_to_field_map.each_with_object({}) do |array, accum|
        key, field = *array

        field_value = value_for_field(field)

        if field.is_system?
          sys_field_name = key.to_s.split("::")[1]
          if accum.key? :system
            next unless accum[:system].is_a?(Hash)
            accum[:system][sys_field_name] = field.to_liquid(field_value)
          else
            accum[:system] = { sys_field_name => field.to_liquid(field_value) }
          end
        else
          accum[key] = field.to_liquid(field_value)
        end
      end
    end

    def update_from_hash(h)
      return if h.nil?

      h = h.symbolize_keys
      h.each do |k, v|
        next unless field = key_to_field_map[k]
        set_field_value(field, v)
      end
    end

    def value_for_key(key)
      detect { |fv| fv.field.key == key }
    end

    # returns field value
    def set_field_value(field, value)
      return unless _account.has_user_and_organization_fields?

      # TODO: update this code if "default value job" is developed
      fv = value_for_field(field)
      proxy_owner.remove_tags = get_tag(fv)
      if field.is_blank?(value)
        if fv
          touch_owner
          fv.mark_for_destruction # will delete upon save
        end
        add_field_changes_to_map(field, fv&.value, value)
        nil
      else
        fv ||= build(field: field, owner: proxy_owner)
        new_value = field.cast_value_for_db(value)
        if fv.value != new_value
          touch_owner
          add_field_changes_to_map(field, fv&.value, new_value)
          fv.value = value
        end
        proxy_owner.additional_tags = get_tag(fv)
        fv
      end
    end

    def field_changes
      @field_changes ||= []
    end

    private

    def touch_owner
      proxy_owner.updated_at = Time.now
    end

    def add_field_changes_to_map(field, old_value, new_value)
      field_changes.push([field, old_value, new_value])
    end

    def key_to_field_map(custom_fields = nil)
      return {} unless _account
      custom_fields ||= _account.custom_fields_for_owner(proxy_owner.class.name)
      @key_to_field_map ||= custom_fields.each_with_object({}) do |field, accum|
        accum[field.key.to_sym] = field
      end
    end

    def value_for_field(field)
      detect { |fv| !fv.destroyed? && fv.cf_field_id == field.id }
    end

    def get_tag(fv)
      fv && fv.field.get_tag(fv)
    end

    def _account
      proxy_owner.account
    end

    def proxy_owner
      proxy_association.owner
    end
  end
end
