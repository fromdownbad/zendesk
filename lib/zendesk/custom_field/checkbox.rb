module Zendesk
  module CustomField
    module Checkbox
      TRUTHY_VALUES = ["true", "1", 1, true, "TRUE", "t", "on", "ON", "yes", "Yes"].freeze
      FALSY_VALUES  = ["false", "0", 0, false, "FALSE", "f", "off", "OFF", "no", "No"].freeze

      def normalize_value(val)
        TRUTHY_VALUES.include?(val) ? "1" : "0"
      end

      def assignable_value?(some_value)
        some_value == '0' ||
        some_value == '1'
      end

      def is_blank?(ticket) # rubocop:disable Naming/PredicateName
        value(ticket) == '0'
      end
    end
  end
end
