module Zendesk
  module CustomField
    module Regexp
      protected

      def validate_regex
        if regexp_for_validation.blank?
          errors.add(:base, ::I18n.t('txt.admin.modesl.ticket_fields.field_regexp.regular_expression_missing'))
          return
        end

        begin
          ::Regexp.compile(regexp_for_validation)
          # this is necessary for as long as our mail workers are running 1.8, as it doesn't support the (?<named_groups>.*) extension
          raise RegexpError, ::I18n.t('txt.admin.models.ticket_fields.field_regexp.undefined_sequence', regexp: regexp_for_validation) if regexp_for_validation =~ /\(\?<.*>.*\)/
        rescue RegexpError => e
          errors.add(:base, ::I18n.t('txt.admin.modesl.ticket_fields.field_regexp.invalid_regular_expression') + e.message)
        end
      end
    end
  end
end
