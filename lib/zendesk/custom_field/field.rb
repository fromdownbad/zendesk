# code common to ticket_field.rb and custom_field/field.rb

module Zendesk
  module CustomField
    module Field
      INTEGER_REGEXP = '\A[-+]?\d+\z'.freeze
      # zd632915: allow the "-" symbol for negative numbers
      DECIMAL_REGEXP = '\A[-+]?[0-9]*[.,]?[0-9]+\z'.freeze
      DATE_REGEXP    = '\A([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])\z'.freeze # yyyy-mm-dd

      def self.included(base)
        base.belongs_to     :account
        base.scope :active, -> { base.where(is_active: true) }
        base.scope :inactive, -> { base.where(is_active: false) }

        base.scope :visible_in_portal, -> { base.where(is_visible_in_portal: true) }
        base.scope :editable_in_portal, -> { base.where(is_editable_in_portal: true) }

        base.before_validation :set_default_position, on: :create
      end

      # Toggle whether the ticket field is active.
      #
      # Returns nothing.
      def toggle!
        # toggle without saving to include in the set of changes checked by #can_modify_collection?
        toggle(:is_active)
        if can_modify_collection?
          update_attribute(:is_active, is_active)
        else
          # revert unsaved toggle
          toggle(:is_active)
        end
      end

      def assignable_value?(_some_value)
        true # overwritten by checkbox and tagger
      end

      def set_default_position
        self.position ||= 9999
      end

      # override for Checkbox
      def two_state?
        false
      end

      private

      def set_field_defaults
        self.title_in_portal ||= title
        self.description ||= title

        self.is_visible_in_portal ||= false
        self.is_editable_in_portal ||= false
        self.is_active = true if is_active.nil?

        unless is_visible_in_portal?
          self.title_in_portal = title
          self.is_editable_in_portal = false
        end

        # ideally this would be done in the models themselves, but sometimes they can be created with the wrong
        # class (think account.custom_fields.build()...), I think that's why this code is here instead.
        case type.to_s
        when 'FieldAssignee'
          self.is_required = true
        when 'FieldGroup', 'FieldDescription'
          self.is_active = true
        when 'FieldStatus'
          self.is_active = true
          self.use_status_hold ||= false
        when 'FieldPriority'
          self.sub_type_id ||= PrioritySet.FULL
        when 'FieldDecimal'
          self.regexp_for_validation = DECIMAL_REGEXP
        when 'FieldInteger'
          self.regexp_for_validation = INTEGER_REGEXP
        when 'FieldDate', 'Date'
          self.regexp_for_validation = DATE_REGEXP
        end

        true
      end
    end
  end
end
