module Zendesk
  module CustomField
    module FieldHelper
      def render_dc_or_i18n(field, attribute)
        if field.is_system?
          ::I18n.t(attribute)
        else
          render_dynamic_content(attribute)
        end
      end
    end
  end
end
