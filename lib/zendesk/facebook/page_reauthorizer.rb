module Zendesk
  module Facebook
    class PageReauthorizer
      attr_reader :account

      def initialize(account)
        @account = account
      end

      def reauthorize_pages
        pages = ::Facebook::Page.where(account_id: account.id, state: ::Facebook::Page::State::UNAUTHORIZED)
        pages.each do |page|
          puts "Activating facebook page #{page}"
          page.update_column(:state, ::Facebook::Page::State::ACTIVE)
        end
      end

      def self.run
        ActiveRecord::Base.on_all_shards do
          Account.active.pod_local.shard_unlocked.each do |account|
            new(account).reauthorize_pages
          end
        end
      end
    end
  end
end
