module Zendesk
  module Sunshine
    class AccountConfigClient
      def initialize(account:)
        @account = account
      end

      def delete_account(reason)
        response = api_client.delete("/api/sunshine/account_config/private/account/#{@account.id}?reason=#{reason}")
        response.body.dig("data", "status")
      end

      private

      def api_client
        @api_client ||= KragleConnection.build_for_sunshine_account_config(@account)
      end

      attr_accessor :account
    end
  end
end
