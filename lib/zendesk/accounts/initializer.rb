module Zendesk
  module Accounts
    class Initializer
      attr_accessor :subdomain, :language

      def initialize(params, request_ip, region)
        @params                = params || {}
        @request_ip            = request_ip
        @region                = region

        @account_params        = @params[:account] || {}
        @owner_params          = @params[:owner]
        @trial_extras          = @params[:trial_extras] || {}
        @address               = @params[:address] || {}
        @partner               = @params[:partner]
        @creation_channel      = @params[:creation_channel]

        @source                = @account_params[:source]
        @preferred_shard       = @account_params[:_zd_choose_shard_udupi]
        @account_name          = @account_params[:name]
        @help_desk_size        = @account_params[:help_desk_size]
        @remote_login_url      = @account_params[:remote_login_url]

        @currency              = @account_params.delete(:currency)
        @utc_offset            = @account_params.delete(:utc_offset)
        @suite_trial           = @account_params.delete(:suite_trial)
        @subdomain             = @account_params.delete(:subdomain)
        @remote_authentication = @account_params.delete(:remote_authentication)
        @language              = @account_params.delete(:language)
        @google_apps_domain    = @account_params.delete(:google_apps_domain)
      end

      def account
        precreation_supported? ? find_or_precreate_account : new_account
      end

      def precreate_account(convert: true)
        status = if convert
          PreAccountCreation::ProvisionStatus::ONGOING
        else
          PreAccountCreation::ProvisionStatus::INIT
        end

        ActiveRecord::Base.on_first_shard do # Account initialization get issues with schema loading without a shard
          new_account.build_pre_account_creation(
            locale_id:     new_account.locale_id,
            account_class: account_class.to_s,
            region:        @region || 'us',
            source:        new_account.source.presence || 'classic',
            status:        status,
            pod_id:        Zendesk::Configuration.fetch(:pod_id)
          )
          new_account
        end
      end

      private

      def find_or_precreate_account
        (find_precreated_account || precreate_account).tap do |acc|
          acc.is_fast_account_creation = !acc.new_record?
        end
      end

      def new_account
        ActiveRecord::Base.on_first_shard do # Account initialization get issues with schema loading without a shard
          @new_account ||= account_class.new(@account_params).tap do |account|
            # Only new account needs these
            account.choose_shard!(@region, @preferred_shard)

            if create_owner_as_unverified?(account)
              # Overriding whatever variable marketing passes in
              # to avoid having to coordinate deploys
              @owner_params[:is_verified] = false
            end

            account.set_owner(@owner_params)

            # Common updates for pre-created and new accounts
            update_account_information(account)
          end
        end
      end

      def account_class
        case @source
        when Zendesk::Accounts::Source::MAGENTO
          ::Accounts::Magento
        when Zendesk::Accounts::Source::GOOGLE_APP_MARKET
          ::Accounts::Classic
        else
          ::Accounts::Classic
        end
      end

      def locale
        ::DefaultLocaleChooser.choose(@language) || ENGLISH_BY_ZENDESK
      end

      def precreation_supported?
        PreAccountCreation.precreation_supported?(account_class.to_s)
      end

      def find_precreated_account
        ActiveRecord::Base.on_first_shard do # Account initialization get issues with schema loading without a shard
          @pre_created_account ||= begin
            account = PreAccountCreation.find_precreated_account(
              locale_id:     locale.id,
              account_class: account_class.to_s,
              region:        @region || 'us'
            ).try(:account)

            @trial_extras['is_fast_account_creation'] = account.present?
            update_precreated_account(account) if account
          end
        end
      end

      def create_owner_as_unverified?(account)
        Arturo.feature_enabled_for?(:create_owner_as_unverified, account) && !verified_partner?
      end

      def verified_partner?
        @creation_channel.to_s =~ /shopify|magento|inbox/i
      end

      def update_precreated_account(account)
        account.on_shard do
          owner_params = @owner_params.with_indifferent_access
          if owner_params[:email]
            account.owner.identities.destroy_all
          end

          if create_owner_as_unverified?(account)
            # Overriding whatever variable marketing passes in
            # to avoid having to coordinate deploys
            owner_params[:is_verified] = false
          end

          password = owner_params.delete :password
          if password.present?
            account.owner.password = password
          end
          account.owner.assign_attributes(owner_params)
          account.owner.save!

          account.name           = @account_name if @account_name
          account.help_desk_size = @help_desk_size if @help_desk_size
          account.source         = @source if @source
          account.is_serviceable = true

          if @currency
            account.subscription.currency = @currency
          end

          if @trial_extras[:created_via_google].present?
            account.enable_google_login!
          end

          if @trial_extras[:created_via_office_365].present?
            account.enable_office_365_login!
          end

          update_account_information(account)

          # NOTE: need to update the subscription *after* `update_account_information` because we need
          # `add_trial_extras` to run so that accounts participating in a trial length experiment (like 14-day trial)
          # can utilize trial_extras
          account.subscription.set_trial_expiry
          account.subscription.save!(validate: false) # skip credit card check

          account
        end
      end

      def update_account_information(account)
        account.subdomain          = @subdomain
        account.locale_id          = locale.id
        account.translation_locale = locale
        ::I18n.locale              = locale
        account.remote_login_url   = @remote_login_url if @remote_login_url
        account.add_trial_extras(@trial_extras)

        account.set_remote_authentication_settings(@remote_authentication)
        account.set_address(@address)
        account.set_partner_settings(@partner)
        account.set_created_from_ip_address(@request_ip)
        account.set_currency(@currency)
        account.set_time_zone(@utc_offset)
        account.set_creation_channel(@creation_channel)
        has_google_apps_admin = @trial_extras[:google_apps_admin].to_i > 0
        account.set_google_apps_creds(@google_apps_domain, has_google_apps_admin)
        account.settings.ssl_required = true

        # In the future we'll deprecate these fields in trial_extras.
        unless @trial_extras[:xsell_source].blank?
          account.settings.xsell_source = @trial_extras[:xsell_source]
        end
        unless @trial_extras[:product_sign_up].blank?
          account.settings.product_sign_up = @trial_extras[:product_sign_up]
        end

        account.settings.suite_trial = @suite_trial if @suite_trial.present?

        account
      end
    end
  end
end
