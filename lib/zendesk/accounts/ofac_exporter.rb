#
# NOTE:
#
# This script needs to run on all pods; if possible, use a readonly instance (eg: admin04)
# and take care to set the memory limit before spinning up the console (eg: ulimit -m 524288)
#
# Currently, the script takes about 3 hours to complete on pod1 and much less time on pod2.
#
# - 2013/01/01
#
module Zendesk
  module Accounts
    class OfacExporter
      def to_csv
        CSV.generate(force_quotes: true, row_sep: "\r\n") do |csv|
          emit_rows(csv)
        end
      end

      def save(filename = nil)
        filename ||= build_filename

        CSV.open(filename, "w") do |csv|
          emit_rows(csv)
        end
      end

      private

      def build_filename
        Time.now.strftime("ofac_%Y_%m_%d.csv")
      end

      def emit_rows(csv)
        Account.where(sandbox_master_id: nil).where('id > 0').find_each(batch_size: 5000) do |account|
          next unless account.pod_local?

          account.on_shard do
            address = account.address
            owner = account.owner
            subscription = account.subscription

            fields = []
            fields << nil                               # FIRSTNAME
            fields << nil                               # MIDDLE
            fields << nil                               # LASTNAME
            fields << nil                               # OTHER
            fields << owner.try(:name)                  # UNPARSED
            fields << nil                               # ALL ENTITIES
            fields << address.try(:city)                # CITY
            fields << address.try(:state)               # STATE
            fields << address.try(:zip)                 # POSTAL
            fields << nil                               # COUNTRY_ISO3
            fields << address.try(:country).try(:name)  # COUNTRY
            fields << nil                               # DOB
            fields << account.id                        # UNIQUE_ID
            fields << subscription.try(:account_status) # CLIENT DEFINED 1
            fields << nil                               # CLIENT DEFINED 2
            fields << nil                               # CLIENT DEFINED 3

            csv << fields.map { |f| f }
          end
        end

        nil
      end
    end
  end
end
