class Zendesk::Accounts::SettingsControllerSupport
  MANAGE_TICKET_SKILLS_PERMISSION = {
    EDITABLE_BY_ADMINS: 0,
    VISIBLE_TO_AGENTS: 1,
    HIDDEN_FOR_ALL: 2,
    EDITABLE_BY_ALL: 3
  }.freeze

  EMAIL_CC_SETTINGS = [
    'agent_email_ccs_become_followers',
    'auto_updated_ccs_followers_rules',
    'comment_email_ccs_allowed',
    'follower_and_email_cc_collaborations',
    'light_agent_email_ccs_allowed',
    'ticket_followers_allowed'
  ].freeze

  def self.normalize_and_update_settings(setting_arguments, current_account)
    acct_atts = {}
    acct_settings = {}

    # This maps from an input key to a description of what should be
    # done when the input is found.
    # The input parameters are a two level tree, like
    # `{ 'active_features' => { 'user_tagging' => true }}`.
    # The value in this map is either a hash into which the value
    # should be placed (either `acct_atts` or `acct_settings`), or a
    # tuple of hash destination and key that should be used in that
    # destination.  E.g.
    # 'active_features' => {
    #   'user_tagging' =>             [acct_settings, 'has_user_tags']
    # }
    # means that an input like `{ 'active_features' => { 'user_tagging' => true }}`
    # should store the value `true` into the `has_user_tags account`
    # setting.
    args_transformation_map = {
      'active_features' => {
        'user_tagging' =>                       [acct_settings, 'has_user_tags'],
        'ticket_tagging' =>                     acct_settings,
        'customer_satisfaction' =>              acct_settings,
        'business_hours' =>                     acct_settings,
        'automatic_answers' =>                  acct_settings,
        'allow_ccs' =>                          [acct_settings, 'collaboration_enabled'] # Same as tickets setting?
      },
      'api' => {
        'accepted_api_agreement' =>             acct_settings,
        'api_password_access' =>                acct_settings,
        'api_token_access' =>                   acct_settings
      },
      # NOTE: Branding is handled as a special case below
      'brands' => {
        'default_brand_id' =>                   acct_settings,
        'require_brand_on_new_tickets' =>       acct_settings
      },
      'google_apps' => {
        'has_google_apps_admin' =>              acct_settings
      },
      'groups' => {
        'check_group_name_uniqueness' =>        acct_settings
      },
      'ticket_form' => {
        'ticket_forms_instructions' =>          acct_atts
      },
      'tickets' => {
        'accepted_new_collaboration_tos' =>       acct_settings,
        'agent_can_change_requester' =>           acct_settings,
        'agent_email_ccs_become_followers' =>     acct_settings,
        'agent_ticket_deletion' =>                [acct_settings, 'ticket_delete_for_agents'],
        'auto_updated_ccs_followers_rules' =>     acct_settings,
        'comment_email_ccs_allowed' =>            acct_settings,
        'comments_public_by_default' =>           [acct_atts, 'is_comment_public_by_default'],
        'edit_ticket_skills_permission' =>        acct_settings,
        'email_attachments' =>                    acct_settings,
        'emoji_autocompletion' =>                 acct_settings,
        'follower_and_email_cc_collaborations' => acct_settings,
        'is_first_comment_private_enabled' =>     acct_atts,
        'light_agent_email_ccs_allowed' =>        acct_settings,
        'list_empty_views' =>                     [acct_settings, 'ticket_show_empty_views'],
        'list_newest_comments_first' =>           [acct_settings, 'events_reverse_order'],
        'markdown_ticket_comments' =>             acct_settings,
        'private_attachments' =>                  acct_settings,
        'tagging' =>                              [acct_settings, 'ticket_tagging'],
        'ticket_followers_allowed' =>             acct_settings
      },
      'agents' => {
        'agent_workspace' =>                    [acct_settings, 'polaris'],
      },
      'rule' => {
        'macro_most_used' =>                    acct_settings,
        'macro_order' =>                        acct_settings,
        'skill_based_filtered_views' =>         acct_settings
      },
      'user' => {
        'tagging' =>                            [acct_settings, 'has_user_tags'],
        'agent_created_welcome_emails' =>       [acct_atts, 'is_welcome_email_when_agent_register_enabled'],
        'end_user_phone_number_validation' =>   acct_settings,
        'have_gravatars_enabled' =>             acct_settings
      },
      'onboarding' => {
        'onboarding_segments' =>                acct_settings
      },
      'cross_sell' => {
        'show_chat_tooltip' =>                  acct_settings
      },
      'localization' => {
        'locale_ids' =>                         [acct_atts, 'allowed_translation_locale_ids']
      },
      'email' => {
        'rich_content_in_emails' => acct_settings,
        'email_template_selection' => acct_settings,
        'send_gmail_messages_via_gmail' => acct_settings,
        'modern_email_template' => acct_settings,
        'accept_wildcard_emails' => acct_settings,
        'personalized_replies' => acct_settings,
        'gmail_actions' => acct_settings,
        'email_template_photos' => acct_settings,
        'simplified_email_threading' => acct_settings,
        'email_sender_authentication' => acct_settings,
        'custom_dkim_domain' => acct_settings,
        'html_mail_template' => acct_atts,
        'text_mail_template' => acct_atts,
        'mail_delimiter' => acct_atts,
        'no_mail_delimiter' => acct_settings
      }
    }

    args_transformation_map.each_pair do |mapping_section_key, mapping_section_info|
      next unless setting_arguments.key?(mapping_section_key)
      setting_arguments_section = setting_arguments[mapping_section_key]
      mapping_section_info.each_pair do |mapping_item_key, mapping_item_value|
        if setting_arguments_section.key?(mapping_item_key)
          if mapping_item_value.is_a?(Array)
            mapping_item_value[0][mapping_item_value[1]] = setting_arguments_section[mapping_item_key]
          else
            mapping_item_value[mapping_item_key] = setting_arguments_section[mapping_item_key]
          end
        end
      end
    end

    if current_account.has_email_ccs?
      acct_settings.delete('auto_updated_ccs_followers_rules') unless acct_settings['auto_updated_ccs_followers_rules']

      # The `agent_email_ccs_become_followers` and `light_agent_email_ccs_allowed` settings are
      # dependent on the `comment_email_ccs_allowed` setting being enabled. Therefore, if the
      # `comment_email_ccs_allowed` setting is explicitly disabled, the
      # `agent_email_ccs_become_followers` and `light_agent_email_ccs_allowed` settings should
      # also be disabled.
      if acct_settings['comment_email_ccs_allowed'] == false
        acct_settings['agent_email_ccs_become_followers'] = false
        acct_settings['light_agent_email_ccs_allowed'] = false
      end

      # Updates to the `light_agent_email_ccs_allowed` account setting is only allowed if the
      # account has the `email_ccs_light_agents_v2` arturo enabled
      acct_settings.delete('light_agent_email_ccs_allowed') unless current_account.has_email_ccs_light_agents_v2?
    else
      acct_settings.except!(
        'accepted_new_collaboration_tos',
        'agent_can_change_requester',
        'agent_email_ccs_become_followers',
        'auto_updated_ccs_followers_rules',
        'comment_email_ccs_allowed',
        'follower_and_email_cc_collaborations',
        'light_agent_email_ccs_allowed',
        'ticket_followers_allowed'
      )
    end

    # Some special cases
    current_account.branding = Branding.create(setting_arguments['branding']) if setting_arguments.key?('branding')

    if setting_arguments.key?('active_features') && setting_arguments['active_features'].key?('customer_satisfaction')
      current_account.changed_customer_satisfaction = true
    end

    if acct_settings.key?('onboarding_segments') && acct_settings['onboarding_segments'].is_a?(String)
      acct_settings['onboarding_segments'] = sanitize_onboarding_segments(acct_settings['onboarding_segments'])
    end

    if acct_settings.key?('skill_based_filtered_views')
      # Only one view can be a skill filtered view at this time. To support
      # multiple views in the future without introducing a breaking API
      # change, the `skill_based_filtered_views` param is converted to
      # an array before it gets persisted to account settings.
      normalized_val = Array(acct_settings['skill_based_filtered_views'])
      if normalized_val.length <= 1
        acct_settings['skill_based_filtered_views'] = normalized_val
      else
        acct_settings.delete('skill_based_filtered_views')
      end
    end

    if acct_settings.key?('edit_ticket_skills_permission')
      permission = acct_settings['edit_ticket_skills_permission']
      acct_settings['edit_ticket_skills_permission'] = if MANAGE_TICKET_SKILLS_PERMISSION.values.include?(permission)
        permission
      else
        MANAGE_TICKET_SKILLS_PERMISSION[:EDITABLE_BY_ADMINS]
      end
    end

    if acct_settings.key?('macro_order') && !%w[alphabetical position].include?(acct_settings['macro_order'])
      acct_settings.delete('macro_order')
    end

    if setting_arguments.key?('automatic_answers')
      update_automatic_answers_settings setting_arguments['automatic_answers'], current_account
    end

    # Polaris - don't toggle agent_workspace to true if groups and departments have not been migrated
    if acct_settings.key?('polaris') && !current_account.settings.check_group_name_uniqueness
      acct_settings['polaris'] = false
    end

    if current_account.has_email_settings_api?
      unless current_account.allow_email_template_customization?
        acct_atts.except!(
          'html_mail_template',
          'text_mail_template',
          'mail_delimiter'
        )
      end
    else
      acct_settings.except!(
        'rich_content_in_emails',
        'email_template_selection',
        'send_gmail_messages_via_gmail',
        'modern_email_template',
        'accept_wildcard_emails',
        'personalized_replies',
        'gmail_actions',
        'email_template_photos',
        'email_sender_authentication',
        'custom_dkim_domain',
        'no_mail_delimiter'
      )
      acct_atts.except!(
        'html_mail_template',
        'text_mail_template',
        'mail_delimiter'
      )
    end

    acct_settings.except!("simplified_email_threading") unless current_account.has_email_simplified_threading_onboarding? || current_account.has_email_settings_api?

    # Store all the info
    current_account.settings.set(acct_settings)

    if RAILS4
      current_account.assign_attributes(acct_atts, without_protection: true)
    else
      current_account.assign_attributes(acct_atts)
    end

    # See https://github.com/zendesk/zendesk/pull/23144 for additional context
    if (acct_settings.keys & EMAIL_CC_SETTINGS).present?
      current_account.settings.each { |s| s.account = current_account }
    end

    current_account.save!
  end

  def self.update_automatic_answers_settings(auto_answers_settings, current_account)
    threshold = Zendesk::Types::PredictionThresholdType.find(auto_answers_settings[:threshold])
    if threshold
      current_account.settings.automatic_answers_threshold = threshold
    end
  end

  def self.sanitize_onboarding_segments(segments)
    valid_segments = Users::OnboardingSupport::SEGMENTS

    (segments.split(',').map(&:strip).uniq & valid_segments).join(',')
  end
end
