module Zendesk
  module Accounts
    module Multiproduct
      # This is sorted in order of priority for redirect logic as specified by https://zendesk.atlassian.net/browse/VOLT-379
      PRODUCT_TYPE_HOMES = {
        support: 'agent',
        chat: 'chat',
        explore: 'explore',
        connect: 'connect',
        outbound: 'outbound/start',
        guide: 'guide',
        message: 'message',
        voice: 'talk',
        sell: 'sell/start',
        acme_app: 'acme_app',
        central_ui: 'central_ui'
      }.freeze

      protected

      def current_account_products
        @products ||= multiproduct? ? account_service_client.products : []
      end

      def support_product
        @support_product ||= account_service_client.product(Zendesk::Accounts::Client::SUPPORT_PRODUCT, use_cache: true)
      end

      # Returns the first active product according to the priority defined in PRODUCT_TYPE_HOMES
      def first_active_product
        return @first_active_product if defined?(@first_active_product) # handle caching nils
        product_hash = active_products.each_with_object({}) { |p, h| h[p.name] = p }
        product_name = (PRODUCT_TYPE_HOMES.keys & product_hash.keys).first
        @first_active_product = product_hash[product_name]
      end

      def active_products
        current_account_products.select(&:active?)
      end

      def multiproduct?
        current_account.try(:multiproduct?)
      end

      private

      def account_service_client
        @account_service_client ||=
          Zendesk::Accounts::Client.new(
            current_account,
            retry_options: {
              max:        3,
              interval:   1,
              exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS
            }
          )
      end
    end
  end
end
