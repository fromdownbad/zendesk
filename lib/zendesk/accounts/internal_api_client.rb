require 'zendesk/internal_api/client'

module Zendesk::Accounts
  class InternalApiClient < Zendesk::InternalApi::Client
    def create_hub_spoke_ticket(params = {})
      connection.post("/api/v2/tickets.json", ticket: params)
    end
  end
end
