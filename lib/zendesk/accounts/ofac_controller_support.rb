module Zendesk::Accounts
  module OfacControllerSupport
    def self.included(base)
      base.before_action :check_owner_email_domain!, only: :create, if: :owner_email
    end

    protected

    def check_owner_email_domain!
      if in_restricted_domain?(owner_email.split('.').last)
        respond_to do |format|
          format.json do
            Rails.logger.warn("Account creation failed: Email domain identified as coming from restricted country.")
            render(json: {
                success: false,
                message: "You appear to be in a Prohibited Jurisdiction under our <a href='https://www.zendesk.com/company/customers-partners/#master-subscription-agreement'>Zendesk Master Subscription Agreement</a>. If this is an error, please email legal@zendesk.com.",
              },
                   callback: params[:callback])
          end
        end
      end
    end

    private

    def in_restricted_domain?(domain)
      %w[ir cu sy kp].include?(domain.try(:downcase))
    end

    def owner_email
      if params[:owner]
        params[:owner][:email].presence
      end
    end
  end
end
