#
# This is a module that implements a DSL that allows the user to change
# an accounts billing handler from Zuora to Credit Card. Additional
# changes to specific fields of the account's subscription record may be
# specified.
#
# USAGE:
#
#   extend Zendesk::Accounts::Dsl::ZuoraToCreditCard
#
#   account_id        45992        # REQUIRED
#   subdomain        "thecity"     # REQUIRED
#   period_begin_at  "2013-01-05"  # REQUIRED
#   period_end_at    "2013-02-04"  # OPTIONAL; visual verification only
#   number_of_agents 4             # OPTIONAL; must be > 0
#   billing_cycle    "Monthly"     # OPTIONAL; one-of: Monthly, Quarterly, Annually, BiAnnually
#   plan             "Regular"     # OPTIONAL; one-of: Solo, Regular, Plus, Enterprise
#
#   review                         # print the list of changes about to be applied
#
#   apply!                         # will apply the changes
#   report                         # generate report of the last successful changes applied
#
# As a safety measure, calling apply! more than once will raise an error and requires
# that the values for the required attributes to be set again.
#
module Zendesk
  module Accounts
    module Dsl
      module ZuoraToCreditCard
        class Error < StandardError; end

        ATTRIBUTES = [:account_id, :subdomain, :period_begin_at, :period_end_at, :number_of_agents, :plan, :billing_cycle].freeze

        # Ruby 1.8 friendly - otherwise we should just use splat
        (ATTRIBUTES + [:dump]).each do |attribute|
          delegate attribute, to: :processor
        end

        def apply!
          processor.apply!
          @report = processor.summary_of_changes
          reset
        end

        def reset
          @processor = nil
        end

        def review
          puts "\n#{dump.join("\n")}\n\n"
        end

        def report
          puts (@report || "").gsub(/  /, "")
        end

        protected

        def processor
          @processor ||= Processor.new
        end

        class Processor
          Zendesk::Accounts::Dsl::ZuoraToCreditCard::ATTRIBUTES.each do |name|
            define_method name do |value|
              instance_variable_set :"@#{name}", value
            end
          end

          def dump
            Zendesk::Accounts::Dsl::ZuoraToCreditCard::ATTRIBUTES.map do |name|
              value = instance_variable_get :"@#{name}"
              "#{name}: #{value.inspect}"
            end
          end

          def apply!
            account.on_shard do
              Account.transaction do
                create_pending_payment!
                update_subscription!
                destroy_zuora_subscription!
              end
            end
          end

          def summary_of_changes
            account.reload
            <<-SUMMARY
              The account for **#{account.name.strip}** (#**#{account.id}**, subdomain: **#{account.subdomain.strip}**) has been updated.

              The payment method type is now set to **#{PaymentMethodType[subscription.payment_method_type].to_s.upcase}**
              It is being billed **#{BillingCycleType[subscription.billing_cycle_type].to_s.upcase}** for **#{subscription.max_agents}** agents

              A pending payment was created with the following order-line:

              ````
              #{subscription.payments.pending.first.order_line}
              ````

              The account's entry from the pending approval list for [Zuora Subscriptions](https://monitor.zendesk.com/zuora_subscriptions) has been removed.

              See: https://monitor.zendesk.com/accounts/#{account.id}
            SUMMARY
          end

          private

          def account
            raise Error, "account_id undefined" if @account_id.nil?
            raise Error, "subdomain undefined"  if @subdomain.nil?
            @account ||= Account.find_by_id_and_subdomain(@account_id, @subdomain)
          end

          def subscription
            account.subscription
          end

          def update_subscription!
            options = { payment_method_type: PaymentMethodType.CREDIT_CARD }
            options[:max_agents]         = @number_of_agents  unless @number_of_agents.nil?
            options[:billing_cycle_type] = billing_cycle_type unless @billing_cycle.nil?
            options[:plan_type]          = plan_type          unless @plan.nil?

            subscription.managed_update = true
            subscription.update_attributes!(options)
            subscription.managed_update = false
          end

          def destroy_zuora_subscription!
            zs = ZendeskBillingCore::Zuora::Subscription.find_by_account_id(@account_id)
            zs.destroy unless zs.nil?
          end

          # Monthly, Quarterly, Annually, BiAnnually
          def billing_cycle_type
            BillingCycleType.fuzzy_find(@billing_cycle)
          end

          # Solo, Regular, Plus, Enterprise
          def plan_type
            SubscriptionPlanType.list.map do |_, id|
              @plan.casecmp(SubscriptionPlanType[id].score.downcase).zero? ? id : nil
            end.compact.first
          end
        end
      end
    end
  end
end
