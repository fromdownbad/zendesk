# property_sets writes settings by doing object.settings.set({key}, {value}) which invokes Kasket's cache loading before calling set.
# If the cache was fetched without the persisted record to update, it will attempt to create a new one.
# Because we don't have a persistent memcache proxy, any updates that happen while a memcached server is marked as dead
# will cause attempted updates that happen when it is marked as alive to fail the uniquness validation on name.
module Zendesk::Accounts::SettingsCacheSupport
  private

  def disable_account_settings_kasket
    AccountSetting.without_kasket do
      current_account.settings.reload

      yield
    end
  end
end
