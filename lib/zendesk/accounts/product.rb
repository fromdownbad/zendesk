# Wrap product response payload from Account Service as object
module Zendesk
  module Accounts
    class Product
      attr_reader :id, :account_id, :name, :trial_expires_at, :plan_settings, :skus, :state_updated_at, :updated_at, :created_at
      attr_accessor :state

      NOT_STARTED = :not_started
      TRIAL = :trial
      SUBSCRIBED = :subscribed
      EXPIRED = :expired
      INACTIVE = :inactive
      FREE = :free
      CANCELLED = :cancelled
      DELETED = :deleted

      STATUSES = [
        NOT_STARTED,
        TRIAL,
        SUBSCRIBED,
        EXPIRED,
        INACTIVE,
        FREE,
        CANCELLED,
        DELETED
      ].freeze

      def initialize(product_params)
        product_params = HashWithIndifferentAccess.new(product_params)
        @id = product_params['id']
        @account_id = product_params['account_id']
        @state = product_params['state'].to_sym
        @name = product_params['name'].to_sym
        @active = product_params['active']
        @plan_settings = product_params['plan_settings']
        @skus = product_params['skus'] || []
        @trial_expires_at = product_params['trial_expires_at'].try(:to_datetime)
        @state_updated_at = product_params['state_updated_at'].try(:to_datetime)
        @updated_at = product_params['updated_at'].try(:to_datetime)
        @created_at = product_params['created_at'].try(:to_datetime)
      end

      # indicate if the product is part of SPP Suite
      def zendesk_suite_sku?
        skus.include?('zendesk_suite')
      end

      def deleted?
        state == DELETED
      end

      def cancelled?
        state == CANCELLED
      end

      def not_started?
        state == NOT_STARTED
      end

      def trial?
        state == TRIAL
      end

      def subscribed?
        state == SUBSCRIBED
      end

      def expired?
        state == EXPIRED
      end

      def inactive?
        state == INACTIVE
      end

      def active?
        @active
      end

      def free?
        state == FREE
      end

      def started?
        !not_started?
      end
    end
  end
end
