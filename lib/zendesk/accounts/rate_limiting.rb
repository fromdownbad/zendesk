module Zendesk
  module Accounts
    module RateLimiting
      protected

      # Determines if an identifier (which represents an API endpoint) should be throttled
      #
      # @return [Boolean] Returns false if the throttle should not be enforced.  Otherwise Raise an error.
      def throttle_endpoint!(controller_identifier)
        @controller_identifier = controller_identifier
        throttle_enabled_for_account_or_account_is_recent =
          throttle_enabled_for_account ||
          enforce_throttle_for_new_accounts?

        # If there's no arturo defined for this endpoint OR
        # There is an arturo and it's enabled OR
        # The arturo is OFF and it's a new account(created after the grandfather date)

        # There are 2 arturos possible
        # arturo & arturo_master_switch
        # Arturo is an ON/OFF for grandfathered accounts
        # arturo_master_switch is an ON/OFF for all accounts
        # arturo_master_switch => optional arturo used to turn on limiting the API
        #           if you have the `arturo` flag on and `arturo_master_switch` off
        #              => ALL account are OFF
        #           if you have the `arturo` flag OFF and `arturo_master_switch` OFF
        #              => ALL account are OFF
        #           if you have the `arturo` flag OFF and `arturo_master_switch` ON
        #              => grandfathered accounts are OFF
        #              => new (non-grandfathered) accounts are ON
        #           if you have the `arturo` flag ON and `arturo_master_switch` ON
        #              => ALL account are ON
        #           if either `arturo` or `arturo_master_switch` is absent
        #              => the default is ON

        enforce_throttle = throttle_enabled_by_master_switch && throttle_enabled_for_account_or_account_is_recent
        if enforce_throttle
          throttled = Prop.throttled?(throttle_identifier, current_account.id,
            threshold: throttle_threshold,
            interval: throttle_interval)

          report_throttled_metrics(false, throttle_identifier) if throttled
          # Raise a ruckus up in here
          # throttle! will raise an exception if over the set limit
          current_count = Prop.throttle!(throttle_identifier, current_account.id,
            threshold: throttle_threshold,
            interval: throttle_interval,
            description: ::I18n.t(action_throttling_limits[:description]))

          warn_throttle_monitor!(false, throttle_identifier) if warn_throttle_monitor?(current_count)
        else
          # We're being nice and just logging infractions
          throttled = Prop.throttle(throttle_identifier, current_account.id,
            threshold: throttle_threshold,
            interval: throttle_interval)

          if throttled
            report_throttled_metrics(true, throttle_identifier)
          elsif warn_throttle_monitor?
            warn_throttle_monitor!(true, throttle_identifier)
          end
          false
        end
      end

      def throttle_identifier
        if paginated?
          if deep_paginated?
            "#{controller_throttling_limits[:identifier]}_deep_pagination".to_sym
          else # Very Deep Paginated results get different limits
            "#{controller_throttling_limits[:identifier]}_pagination".to_sym
          end
        else
          controller_throttling_limits[:identifier]
        end
      end

      def throttle_account_setting_method
        return :account_setting_deep_threshold if deep_paginated?
        return :account_setting_shallow_threshold if paginated?
        :account_setting_threshold
      end

      def throttle_threshold
        return threshold_from_settings if threshold_from_settings
        return throttle_base_value unless current_account.has_use_scaling_strategies_for_rate_limiting?

        return @throttle_threshold if @throttle_threshold
        strategy = throttle_base_strategy
        definition = throttle_base_definition
        @throttle_threshold = if strategy && definition
          strategy.calculate(self, definition, current_account)
        elsif definition
          definition[:threshold]
        else
          throttle_base_value
        end
      end

      def threshold_from_settings
        return nil unless controller_throttling_limits[throttle_account_setting_method]
        @threshold_from_settings ||= current_account.settings.send(controller_throttling_limits[throttle_account_setting_method])
      end

      def throttle_base_definition
        if paginated?
          if deep_paginated?
            action_throttling_limits[definitions].max_by { |option| option[:page] }
          else
            action_throttling_limits[definitions].min_by { |option| option[:page] }
          end
        else
          custom_setting_limit = current_account.settings.send(controller_throttling_limits[:account_setting_threshold])
          return nil if custom_setting_limit

          action_throttling_limits[definitions].first
        end
      end

      def throttle_base_strategy
        if paginated?
          if deep_paginated?
            action_throttling_limits[definitions].max_by { |option| option[:page] }[:scaling_strategy]
          else
            action_throttling_limits[definitions].min_by { |option| option[:page] }[:scaling_strategy]
          end
        else
          custom_setting_limit = current_account.settings.send(controller_throttling_limits[:account_setting_threshold])
          return nil if custom_setting_limit

          action_throttling_limits[definitions][0][:scaling_strategy]
        end
      end

      def throttle_base_value
        if paginated?
          if deep_paginated?
            deep_pagination_limits
          else
            shallow_pagination_limits
          end
        else
          current_account.settings.send(controller_throttling_limits[:account_setting_threshold]) || action_throttling_limits[definitions][0][:threshold]
        end
      end

      def throttle_interval
        controller_throttling_limits[:interval]
      end

      def throttle_count
        @throttle_count ||= Prop.count(throttle_identifier, current_account.id, interval: throttle_interval)
      end

      # @returns the limits for the specific API action
      def action_throttling_limits
        @action_throttling_limits ||= controller_throttling_limits[:limits][throttle_limit_type]
        @action_throttling_limits ||= controller_throttling_limits[:limits].fetch(:default, {})
      end

      def deep_pagination_limits
        if controller_throttling_limits[:account_setting_deep_threshold]
          @deep_pagination_limits ||= current_account.settings.send(controller_throttling_limits[:account_setting_deep_threshold])
        end
        @deep_pagination_limits ||= action_throttling_limits[definitions].max_by { |option| option[:page] }[:threshold]
      end

      def shallow_pagination_limits
        if controller_throttling_limits[:account_setting_shallow_threshold]
          @shallow_pagination_limits ||= current_account.settings.send(controller_throttling_limits[:account_setting_shallow_threshold])
        end
        @shallow_pagination_limits ||= action_throttling_limits[definitions].min_by { |option| option[:page] }[:threshold]
      end

      def enforce_throttle_for_new_accounts?
        !!controller_throttling_limits[:grandfathered_date] && current_account.created_at > Time.parse(controller_throttling_limits[:grandfathered_date])
      end

      def legacy_account?
        !!controller_throttling_limits[:grandfathered_date] && current_account.created_at <= Time.parse(controller_throttling_limits[:grandfathered_date])
      end

      # There are times all actions in a controller have one rate limit
      # ie => A account can ping the index & show actions a combined 100 times in a minute.
      # In this case the controller would not define each endpoint's limit, just one set of limits
      # for the whole controller
      def controller_throttling_limits
        @controller_throttling_limits ||= ::ProductLimits::Controllers::DEFAULT_LIMITS.fetch(@controller_identifier, {}).fetch(action_name.to_sym, {})
      end

      def throttle_limit_type
        return :paginated if paginated? # it is assumed that this is the worst case hence the most limited.
        return :sandbox if current_account.is_sandbox?
        return :trial if current_account.is_trial?
        return :grandfathered_default if legacy_account?
        :default
      end

      # This arturo is used if we want to turn on older accounts
      # that have not been limited before.  Hence it is used to turn ON
      # grandfathered accounts.
      def throttle_arturo
        @throttle_arturo ||= controller_throttling_limits[:arturo]
      end

      # This arturo is used as a master OFF switch
      # after turning this switch ON,
      # logic will still obey the throttle_arturo arturo & grnadfathered logic.
      def throttle_arturo_master
        @throttle_arturo_master ||= controller_throttling_limits[:arturo_master_switch]
      end

      # Default the arturo to ON if the arturo does not exist
      # Otherwise return the value (true/false) of the arturo
      def throttle_enabled_for_account
        return true if throttle_arturo.nil?
        # using feature_enabled_for just in case the arturo is misspelled
        Arturo.feature_enabled_for?("throttle_endpoint_#{throttle_arturo}", current_account)
      end

      # Default switch to ON if the arturo for the master does not exist
      # Otherwise return the value (true/false) of the master arturo
      def throttle_enabled_by_master_switch
        return true if throttle_arturo_master.nil?
        # using feature_enabled_for just in case the arturo is misspelled
        Arturo.feature_enabled_for?("throttle_endpoint_#{throttle_arturo_master}", current_account)
      end

      def definitions
        current_account.subscription.has_high_volume_api? ? :high_volume_definitions : :definitions
      end

      # for paginated results with a "small" page number we don't use the paginated rate limits
      def paginated?
        # Skip CBP V2 requests that use page[:size]=2 and page[:after]='abc'
        return false if params[:page].respond_to?(:key?)
        if params[:page] && controller_throttling_limits[:limits][:paginated]
          params[:page].to_s.to_i > controller_throttling_limits[:limits][:paginated][definitions].min_by { |option| option[:page] }[:page]
        else
          false
        end
      end

      def deep_paginated?
        return false unless paginated?
        params[:page].to_s.to_i > controller_throttling_limits[:limits][:paginated][definitions].max_by { |option| option[:page] }[:page]
      end

      def rounded_page
        # round to the highest 25
        params[:page].to_s.to_i > 1 ? (params[:page].to_s.to_i / 25.0).ceil * 25 : -1
      end

      def warn_throttle_monitor?(count = nil)
        count ||= Prop.count(throttle_identifier, current_account.id,
          threshold: throttle_threshold,
          interval: throttle_interval)
        count > (throttle_threshold / 4.0).ceil
      end

      def warn_throttle_monitor!(log_only, identifier)
        return if current_account.has_suppress_sending_datadog_threshold_data?

        statsd_client.increment("warning_throttled.#{identifier}", tags: throttle_tags(log_only))
      end

      def report_throttled_metrics(log_only, identifier)
        # if we don't have a datadog arturo for this throttle or the datadog suppression arturo is off
        return if current_account.has_suppress_sending_datadog_threshold_data?

        statsd_client.increment("throttled.#{identifier}", tags: throttle_tags(log_only))
      end

      # this ensure we return a certain number of signifant_digits
      #
      # if throttle_count is 455555 => 460000
      # if throttle_count is 333 => 330
      # if throttle_count is 4 => 4
      def significant_count(count, signifant_digits = 2)
        number_of_digits = Math.log10(count).to_i + 1
        count.round(-(number_of_digits - signifant_digits)).to_i
      rescue FloatDomainError # happens when prop returns ∞
        count
      end

      def throttle_tags(log_only)
        [
          "log_only:#{log_only}",
          "account:#{current_account.subdomain}",
          "trial:#{current_account.is_trial?}",
          "sandbox:#{current_account.is_sandbox?}",
          "grandfathered:#{legacy_account?}",
          "page:#{rounded_page.to_i}",
          "request_count:#{significant_count(throttle_count)}",
          "limit:#{throttle_threshold}",
          "plan_type:#{SubscriptionPlanType[current_account.subscription.plan_type]}",
          "strategy:#{threshold_from_settings ? 'settings' : throttle_base_strategy || 'Stationary'}",
          "high_volume_account:#{current_account.subscription.has_high_volume_api?}"
        ]
      end
    end
  end
end
