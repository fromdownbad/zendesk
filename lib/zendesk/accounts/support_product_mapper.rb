module Zendesk
  module Accounts
    module SupportProductMapper
      def self.derive_product(account)
        product = Zendesk::SupportAccounts::Product.retrieve(account)
        return if product.nil?
        product.to_h
      end
    end
  end
end
