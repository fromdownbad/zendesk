module Zendesk::Accounts::Security
  class RoleSettingsUpdate < SettingsUpdate
    attr_reader :remote_auths

    def initialize(user, params, remote_auths)
      super(user, params)

      @remote_auths = remote_auths

      save_model(role_settings)
    end

    def perform
      verify_only_one_login_service_selected

      update_remote_auth_settings if enabled_login_service?(:remote)

      role_settings.attributes = selected_settings

      unless role_settings.login_allowed_for_any_role?(:remote)
        deactivate_all_remote_auths
      end
    end

    private

    def update_remote_auth_settings
      return false unless any_remote_auth_methods_selected?

      update_remote_auth_priority

      selected_remote_auth_settings.values.each do |settings|
        remote_auth = fetch_remote_auth(settings.delete(:id).to_i)

        next unless remote_auth

        if settings[:is_active] == true
          remote_auth.update_attributes(settings)
        else
          remote_auth.is_active = false
        end

        save_model(remote_auth)
      end
    end

    def deactivate_all_remote_auths
      remote_auths.each do |ra|
        ra.is_active = false

        save_model(ra)
      end
    end

    def update_remote_auth_priority
      sorted = remote_auths.sort_by do |auth|
        auth.id == selected_primary_remote_auth_id ? 0 : auth.auth_mode
      end
      sorted.each_with_index do |auth, i|
        auth.update_attribute(:priority, i)
      end
    end

    def selected_remote_auths
      selected_remote_auth_settings.values.select do |settings|
        settings[:is_active] == true
      end
    end

    def any_remote_auth_methods_selected?
      selected_remote_auths.count > 0
    end

    def verify_only_one_login_service_selected
      if enabled_settings.reject { |setting| setting =~ /zendesk_login$/ }.count > 1
        @errors << I18n.t('txt.admin.views.settings.security_policy.update.more_than_one_auth_type_selected')
      end
    end

    def fetch_remote_auth(settings_id)
      remote_auths.detect { |ra| ra.id == settings_id }
    end

    def selected_remote_auth_settings
      params[:account][:remote_authentications]
    end

    def selected_primary_remote_auth_id
      if selected_remote_auths.count == 1
        selected_remote_auths.first[:id]
      else
        params[:account][:primary_sso]
      end.to_i
    end

    def enabled_settings
      selected_settings.reject do |_, selected|
        selected == false
      end.keys.map(&:to_sym)
    end
  end
end
