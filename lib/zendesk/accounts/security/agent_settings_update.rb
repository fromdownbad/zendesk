require 'set'

module Zendesk::Accounts::Security
  class AgentSettingsUpdate < RoleSettingsUpdate
    LOGIN_SERVICE_KEYS = Set[:agent_zendesk_login, :agent_google_login, :agent_office_365_login, :agent_remote_login].freeze

    def perform
      super

      if enabled_login_service?(:zendesk)
        role_settings.agent_security_policy_id = selected_policy_id

        if custom_policy_selected?
          account.settings.session_timeout = selected_custom_policy_settings[:session_timeout] if selected_custom_policy_settings[:session_timeout]

          custom_policy.update_attributes(selected_custom_policy_settings.reject { |k| k == :session_timeout })
          save_model(custom_policy)
        end
      end

      if enabled_login_service?(:google, :office_365, :remote)
        update_enforce_sso_settings
      end

      account.settings.session_timeout = account.settings.default(:session_timeout) if non_custom_policy_selected? && !account.multiproduct?

      save
    end

    private

    def selected_settings
      params[:account][:role_settings].slice(*LOGIN_SERVICE_KEYS)
    end

    def selected_policy_id
      params[:account][:role_settings][:agent_security_policy_id].to_i
    end

    def update_enforce_sso_settings
      role_settings.agent_password_allowed = params[:account][:role_settings][:agent_password_allowed] if params[:account][:role_settings].key?(:agent_password_allowed)
      role_settings.agent_remote_bypass = params[:account][:role_settings][:agent_remote_bypass] if params[:account][:role_settings][:agent_remote_bypass]
    end

    def enabled_login_service?(*services)
      services.any? do |service|
        enabled_settings.include?("agent_#{service}_login".to_sym)
      end
    end

    def selected_custom_policy_settings
      params[:custom_security_policy]
    end

    def non_custom_policy_selected?
      selected_policy_id != 0 && !custom_policy_selected?
    end

    def custom_policy_selected?
      selected_policy_id == Zendesk::SecurityPolicy::Custom.id
    end

    def current_policy
      account.agent_security_policy
    end

    def custom_policy
      @custom_policy ||= begin
        account.custom_security_policy || account.build_custom_security_policy
      end
    end
  end
end
