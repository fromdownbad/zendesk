module Zendesk::Accounts::Security
  class AccountAssumption
    attr_reader :account

    def initialize(account)
      @account = account
    end

    def assumption_duration_for_select
      duration = {
        select_default_heading                                                                    => select_default_value,
        'txt.admin.views.settings.security_policy.show.global.account_assumption.duration.day'    => :day,
        'txt.admin.views.settings.security_policy.show.global.account_assumption.duration.week'   => :week,
        'txt.admin.views.settings.security_policy.show.global.account_assumption.duration.month'  => :month,
        'txt.admin.views.settings.security_policy.show.global.account_assumption.duration.year'   => :year,
        'txt.admin.views.settings.security_policy.show.global.account_assumption.duration.always' => :always
      }
      duration.map { |k, v| [I18n.t(k), v] }
    end

    private

    def select_default_heading
      if account.assumable?
        'txt.admin.views.settings.security_policy.show.global.account_assumption.duration.change'
      else
        'txt.admin.views.settings.security_policy.show.global.account_assumption.duration.select'
      end
    end

    def select_default_value
      account.assumable? ? :already_set : :not_selected
    end
  end
end
