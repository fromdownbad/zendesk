require 'set'

module Zendesk::Accounts::Security
  class GlobalSettingsUpdate < SettingsUpdate
    AUTH_SETTINGS = Set[
      :ip_restriction_enabled, :enable_agent_ip_restrictions,
      :enable_ip_mobile_access, :credit_card_redaction,
      :agreed_to_audioeye_tos, :two_factor_enforce, :mobile_app_access
    ].freeze

    def perform
      update_email_agent_when_sensitive_fields_changed
      update_admins_can_set_user_passwords_setting if user.is_account_owner?
      update_ip_ranges if !!selected_ip_ranges
      update_assumption_expiration_setting

      settings.set(selected_settings)
      settings.set(notifications_recipient_settings)

      save
    end

    private

    def selected_notifications_recipient
      params.dig(:account, :notifications_recipient)
    end

    def update_admins_can_set_user_passwords_setting
      account.admins_can_set_user_passwords = selected_admins_can_set_user_passwords_setting
    end

    def update_email_agent_when_sensitive_fields_changed
      account.email_agent_when_sensitive_fields_changed = selected_email_agent_when_sensitive_fields_changed
    end

    def update_assumption_expiration_setting
      return unless params.dig(:account, :settings)
      update_expiration(params.dig(:account, :settings, :assumption_duration))
    end

    def selected_admins_can_set_user_passwords_setting
      params.dig(:account, :admins_can_set_user_passwords) == true
    end

    def selected_email_agent_when_sensitive_fields_changed
      params.dig(:account, :email_agent_when_sensitive_fields_changed) == true
    end

    def selected_ip_ranges
      params.dig(:account, :ip_ranges)
    end

    def selected_settings
      (
        params.dig(:account, :settings) ||
        HashWithIndifferentAccess.new
      ).slice(*AUTH_SETTINGS)
    end
  end
end
