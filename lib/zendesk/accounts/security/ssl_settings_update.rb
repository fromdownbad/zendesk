module Zendesk::Accounts::Security
  class SslSettingsUpdate < SettingsUpdate
    def perform
      update_ssl_setting
      update_hsts_setting
      update_automatic_certificate_provisioning_setting
      update_hosted_ssl if hosted_ssl_settings?

      save

      if success? && certificates_activated.any?
        certificates_activated.each(&:notify_customer_of_activation!)
      end
    end

    private

    def certificates_activated
      @certificates_activated ||= []
    end

    def update_hosted_ssl
      return unless uploaded_ssl_certificate || uploaded_ssl_key

      if active_cert = account.certificates.active.last
        revoked_cert = active_cert.dup # keep a copy of the old cert in case we need to rollback
        set_sni!(active_cert)
        temp_cert_required = (revoked_cert.sni_enabled? && !active_cert.sni_enabled?) || revoked_cert.autoprovisioned?

        if !temp_cert_required && active_cert.set_uploaded_data(uploaded_ssl_certificate, uploaded_ssl_key, uploaded_ssl_passphrase).nil?
          if active_cert.sni_enabled? && !revoked_cert.sni_enabled?
            active_cert.detach_certificate_ips!
            active_cert.build_sni_pod_record
          end
          revoked_cert.revoke
          save_model(revoked_cert)

          save_model(active_cert)
          certificates_activated << active_cert
        else
          update_temp_cert
        end
      else
        update_temp_cert
      end
    end

    def update_temp_cert
      temp_certs = account.certificates.temporary.reverse
      temp_certs = [account.certificates.new] if temp_certs.empty?

      temp_cert = nil
      error_code = nil
      temp_certs.each do |cert|
        temp_cert = cert
        error_code = temp_cert.set_uploaded_data(uploaded_ssl_certificate, uploaded_ssl_key, uploaded_ssl_passphrase)
        # Needing Intermediate is not fatal, and will be the same across any temporary certificates
        break if error_code.nil? || error_code == [Certificate::ErrorCode::NEED_INTERMEDIATE]
      end

      if error_code
        message =
          case error_code.first
          when Certificate::ErrorCode::NEED_KEY
            I18n.t('txt.admin.controllers.settings.security_controller.certificate_must_be_paired')
          when Certificate::ErrorCode::NEED_CRT
            I18n.t('txt.admin.controllers.settings.security_controller.private_key_must_be_uploaded')
          when Certificate::ErrorCode::BAD_PASSPHRASE
            I18n.t('txt.admin.controllers.settings.security_controller.error_bad_passphrase')
          when Certificate::ErrorCode::BAD_KEY
            I18n.t('txt.admin.controllers.settings.security_controller.error_bad_key')
          when Certificate::ErrorCode::BAD_CRT
            I18n.t('txt.admin.controllers.settings.security_controller.error_bad_crt')
          when Certificate::ErrorCode::MISMATCH
            I18n.t('txt.admin.controllers.settings.security_controller.error_mismatch')
          when Certificate::ErrorCode::BAD_CHAIN
            I18n.t('txt.admin.controllers.settings.security_controller.error_bad_chain')
          when Certificate::ErrorCode::SHA1_SIGNATURE
            I18n.t('txt.admin.controllers.settings.security_controller.sha1_signature',
              url: I18n.t('txt.admin.controllers.settings.security_controller.sha1_signature_link'))
          when Certificate::ErrorCode::NEED_INTERMEDIATE
            # Don't set an error. We still want to create a 'pending'
            # Certificate record, which will be approved via monitor.
            nil
          end
        @errors << message if message
      end

      if @errors.empty?
        set_sni!(temp_cert)
        temp_cert.upload_certificate

        if temp_cert.sni_enabled? && temp_cert.state == 'active'
          if old_cert = account.certificates.active.last
            old_cert.revoke
            save_model(old_cert)
          end

          certificates_activated << temp_cert
        end

        save_model(temp_cert)
      end
    end

    def update_ssl_setting
      account.is_ssl_enabled = account.ssl_forced? || selected_ssl_setting
    end

    def update_hsts_setting
      account.set_long_hsts_header_on_host_mapping = params[:account].try(:[], :set_long_hsts_header_on_host_mapping) == true
    end

    def update_automatic_certificate_provisioning_setting
      if params[:account] && params[:account].key?(:automatic_certificate_provisioning)
        account.automatic_certificate_provisioning = params[:account][:automatic_certificate_provisioning] == true
      end
    end

    def uploaded_ssl_certificate
      @uploaded_ssl_certificate ||= params[:hosted_ssl][:uploaded_certificate_data].try(:read)
    end

    def uploaded_ssl_key
      @uploaded_ssl_key ||= params[:hosted_ssl][:uploaded_key_data].try(:read)
    end

    def uploaded_ssl_passphrase
      params[:hosted_ssl][:key_data_passphrase]
    end

    def selected_ssl_setting
      params[:account][:is_ssl_enabled] == true
    end

    def hosted_ssl_settings?
      !!params[:hosted_ssl]
    end

    alias_method :ssl_selected?, :selected_ssl_setting

    def set_sni!(certificate) # rubocop:disable Naming/AccessorMethodName
      certificate.sni_enabled = if account.has_hosted_ip_ssl?
        params[:hosted_ssl][:ssl_type] == 'sni'
      else
        certificate.sni_enabled = true
      end
    end
  end
end
