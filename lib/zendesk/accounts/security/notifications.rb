module Zendesk::Accounts::Security
  class Notifications
    DEFAULT_RECIPIENT     = 'admins'.freeze
    RECIPIENTS_WITHOUT_ID = ['admins', 'owner'].freeze
    RECIPIENTS_WITH_ID    = ['group'].freeze

    attr_reader :account

    def initialize(account)
      @account = account
    end

    def recipient
      if recipient_with_id?
        recipient_exists?(stored_recipient_id) ? stored_recipient_id : DEFAULT_RECIPIENT
      else
        stored_recipient
      end
    end

    def recipient_emails
      recipients.map(&:email)
    end

    def possible_recipients_for_select
      possible_recipients_without_id + possible_recipients_with_id
    end

    def recipient_settings(selected_recipient)
      if RECIPIENTS_WITHOUT_ID.include?(selected_recipient)
        default_settings.merge(
          security_notifications_recipient: selected_recipient
        )
      elsif recipient_exists?(selected_recipient)
        default_settings.merge(
          security_notifications_recipient: 'group',
          security_notifications_recipient_id: selected_recipient
        )
      else
        default_settings
      end
    end

    def recipients
      case recipient
      when 'admins'
        account.admins.verified
      when 'owner'
        [account.owner]
      when Integer
        account.groups.find(stored_recipient_id).users
      else
        [account.owner]
      end
    end

    private

    def stored_recipient
      account.settings.security_notifications_recipient
    end

    def stored_recipient_id
      account.settings.security_notifications_recipient_id
    end

    def recipient_with_id?
      RECIPIENTS_WITH_ID.include?(stored_recipient)
    end

    def possible_recipients_without_id
      RECIPIENTS_WITHOUT_ID.map do |recipient|
        [translated_recipient_name(recipient), recipient]
      end
    end

    def possible_recipients_with_id
      account.groups.map { |group| [group.name, group.id.to_s] }
    end

    def recipient_exists?(recipient_id)
      account.groups.exists?(recipient_id)
    end

    def default_settings
      {
        security_notifications_recipient: DEFAULT_RECIPIENT,
        security_notifications_recipient_id: nil
      }
    end

    def translated_recipient_name(token)
      I18n.t("txt.admin.views.settings.security_policy.show.global.security_notifications.recipient.#{token}")
    end
  end
end
