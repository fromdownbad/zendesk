require 'set'

module Zendesk::Accounts::Security
  class SettingsUpdate
    include Zendesk::Accounts::Security::SettingsHelper
    def self.create(user, params, remote_auths)
      case params[:tab]
      when 'agents'
        Zendesk::Accounts::Security::AgentSettingsUpdate.new(
          user, params, remote_auths
        )
      when 'end_users'
        Zendesk::Accounts::Security::EndUserSettingsUpdate.new(
          user, params, remote_auths
        )
      when 'global'
        Zendesk::Accounts::Security::GlobalSettingsUpdate.new(
          user, params
        )
      when 'support_product'
        Zendesk::Accounts::Security::SupportProductSettingsUpdate.new(
          user, params
        )
      when 'ssl'
        Zendesk::Accounts::Security::SslSettingsUpdate.new(
          user, params
        )
      end
    end

    attr_reader :user, :account, :settings, :params
    attr_accessor :errors

    def initialize(user, params)
      @user           = user
      @account        = user.account
      @settings       = @account.settings
      @params         = params
      @models_to_save = Set[@account]
      @errors         = []
      @performed      = false
    end

    def perform
      raise 'This method must be implemented in subclasses.'
    end

    def success?
      raise NotYetPerformed unless @performed

      errors.empty?
    end

    private

    def save_model(model)
      @models_to_save << model
    end

    def save
      if errors.empty?
        begin
          on_all_database_connections { @models_to_save.all?(&:save!) }
        rescue ActiveRecord::RecordInvalid
          @models_to_save.each { |m| @errors << m }
        end
      end

      @performed = true
    end

    def on_all_database_connections
      ::Account.transaction do
        ::RemoteAuthentication.transaction do
          yield
        end
      end
    end

    class NotYetPerformed < StandardError; end
  end
end
