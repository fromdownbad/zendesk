module Zendesk::Accounts::Security
  class ApiSettingsUpdate < SettingsUpdate
    IP_SETTINGS = Set[:ip_restriction_enabled, :enable_agent_ip_restrictions].freeze
    AGENT_LOGIN_KEYS = Set[:zendesk_login, :google_login, :office_365_login, :remote_login].freeze
    AGENT_OTHER_KEYS = Set[:remote_bypass, :security_policy_id, :enforce_sso].freeze
    END_USER_KEYS = Set[:zendesk_login, :facebook_login, :google_login, :office_365_login, :remote_login, :twitter_login, :security_policy_id, :enforce_sso].freeze
    PASSWORD_SETTINGS = Set[
      :password_history_length, :password_length, :password_complexity, :password_in_mixed_case,
      :failed_attempts_allowed, :max_sequence, :disallow_local_part_from_email, :password_duration
    ].freeze

    attr_reader :remote_auths

    def initialize(user, params)
      super(user, params)
      transform_shared_secret_to_next_token if shared_secret_present?
      @remote_auths = [saml, jwt]
    end

    def perform
      account.admins_can_set_user_passwords = params[:admins_can_set_user_passwords] if params.include?(:admins_can_set_user_passwords)
      account.email_agent_when_sensitive_fields_changed = params[:email_agent_when_sensitive_fields_changed] if params.include?(:email_agent_when_sensitive_fields_changed)
      update_ip_ranges if !!selected_ip_ranges
      update_expiration(params[:assumption_duration])

      settings.set(selected_settings)

      check_errors_in_custom_agent_password!

      update_role_settings if params[:authentication]

      account.settings.session_timeout = params[:session_timeout] if params[:session_timeout]

      update_remote_auth_settings if params[:remote_authentications]

      add_model_errors

      save unless errors.any?

      if account.has_update_account_associations_for_security_settings_api?
        update_account_associations unless errors.any?
      end
    end

    private

    def update_account_associations
      account.settings = settings
      @models_to_save.each do |model|
        if model.class.name == "RoleSettings"
          account.role_settings = model
        elsif model.class.name == "CustomSecurityPolicy"
          account.custom_security_policy = model
        end
      end
    end

    def add_model_errors
      @models_to_save.each do |model|
        next if model.valid?
        auth_mode = model.try(:auth_mode)

        added_error = if auth_mode == RemoteAuthentication::SAML
          model.errors.full_messages << :saml
        elsif auth_mode == RemoteAuthentication::JWT
          model.errors.full_messages << :jwt
        else
          model.errors.full_messages
        end

        errors << added_error
      end
    end

    def remote_authentications
      @remote_authentications ||= account.remote_authentications
    end

    def saml
      @saml ||=
        remote_authentications.saml.first || remote_authentications.saml.build(remote_auth_mode_params(:saml))
    end

    def jwt
      @jwt ||=
        remote_authentications.jwt.first || remote_authentications.jwt.build(remote_auth_mode_params(:jwt))
    end

    def remote_auth_mode_params(mode)
      params[:remote_authentications][mode] if params[:remote_authentications]
    end

    def selected_ip_ranges
      params[:ip][:ip_ranges] if params[:ip]
    end

    def selected_settings
      selected_ip_settings.merge(selected_auth_settings)
    end

    def selected_ip_settings
      params[:ip] ? params[:ip].slice(*IP_SETTINGS) : {}
    end

    def selected_auth_settings
      params[:authentication] ? params[:authentication].slice(:two_factor_enforce) : {}
    end

    def custom_security_policy_selected?
      params[:authentication] && params[:authentication][:agent] && params[:authentication][:agent][:security_policy_id].to_i == Zendesk::SecurityPolicy::Custom.id
    end

    def security_policy_id_sent?
      params[:authentication][:agent] && params[:authentication][:agent][:security_policy_id]
    end

    def populate_custom_security_policy
      check_errors_in_custom_security_policy_id!
      if (custom_security_policy_selected? || agent_custom_security_policy_id_set?) && agent_custom_password_settings_params?
        update_custom_password_settings
      elsif non_custom_security_policy_selected?
        new_custom_policy = CustomSecurityPolicy.update_or_build_custom_policy_from_non_custom_policy(agent_security_policy_id, account, custom_security_policy)

        save_model(new_custom_policy)
      end
    end

    def non_custom_security_policy_selected?
      non_custom_security_policy_ids.include? agent_security_policy_id
    end

    def non_custom_security_policy_ids
      Zendesk::SecurityPolicy::NON_CUSTOM_SECURITY_POLICY_IDS
    end

    def agent_security_policy_id
      @agent_security_policy_id ||=
        params[:authentication][:agent] && params[:authentication][:agent][:security_policy_id].to_i
    end

    def update_role_settings
      populate_custom_security_policy if security_policy_id_sent? || agent_custom_security_policy_id_set?
      manage_agent_login_settings if params[:authentication][:agent]

      [[:agent, AGENT_OTHER_KEYS], [:end_user, END_USER_KEYS]].each do |role, keys|
        next unless params[:authentication][role]

        updated_params = params[:authentication][role].slice(*keys)
        updated_params = updated_params.to_h if RAILS5
        updated_params = updated_params.each_with_object({}) do |(attribute, value), accepted_params|
          attribute, value = transform_enforce_sso(value) if
            attribute.to_s == 'enforce_sso'

          accepted_params["#{role}_#{attribute}"] = value
        end

        role_settings.assign_attributes(updated_params)
      end

      save_model(role_settings)
    end

    def manage_agent_login_settings
      agent_logins = mix_params_with_current_agent_login_settings
      enabled_agent_logins_count = agent_logins.count { |_parameter, value| value == true }
      check_agent_login_errors!(enabled_agent_logins_count)
      agent_logins["agent_zendesk_login"] = true if enabled_agent_logins_count == 0

      role_settings.assign_attributes(agent_logins)
    end

    def mix_params_with_current_agent_login_settings
      current_logins = role_settings.attributes.slice(*AGENT_LOGIN_KEYS.map { |key| "agent_#{key}" })
      passed_parameters = sliced_agent_login_parameters
      passed_parameters = passed_parameters.to_h if RAILS5
      passed_parameters = passed_parameters.each_with_object({}) do |(name, value), h|
        h["agent_#{name}"] = value
      end

      current_logins.merge(passed_parameters)
    end

    def sliced_agent_login_parameters
      params[:authentication][:agent].slice(*AGENT_LOGIN_KEYS)
    end

    def transform_enforce_sso(value)
      ['password_allowed', !value]
    end

    def agent_custom_password_settings_params? # only when agent password sent
      !!(params[:authentication] && params[:authentication][:agent] && params[:authentication][:agent][:password].present?)
    end

    def update_custom_password_settings
      custom_security_policy.assign_attributes(selected_password_settings)
      save_model(custom_security_policy)
    end

    def selected_password_settings
      params[:authentication][:agent][:password].slice(*PASSWORD_SETTINGS)
    end

    def custom_security_policy
      @custom_security_policy ||= begin
        account.custom_security_policy || CustomSecurityPolicy.build_from_current_policy(account)
      end
    end

    def shared_secret_present?
      params.dig(:remote_authentications, :jwt, :shared_secret)
    end

    def transform_shared_secret_to_next_token
      params[:remote_authentications][:jwt][:next_token] = params[:remote_authentications][:jwt].delete(:shared_secret)
    end

    def update_remote_auth_settings
      %i[saml jwt].each do |remote_auth_type|
        next unless params[:remote_authentications][remote_auth_type]

        remote_auth = remote_auths.find { |i| i.auth_mode == RemoteAuthentication.const_get(remote_auth_type.upcase) }

        new_settings = params[:remote_authentications][remote_auth_type]

        remote_auth.assign_attributes(new_settings)

        save_model(remote_auth)
      end
      set_enabled_remote_auth_as_priority

      check_errors_in_disabled_remote_auths!
      check_errors_in_remote_authentications_priorities!
    end

    def set_enabled_remote_auth_as_priority
      if saml.is_active? && !jwt.is_active?
        saml.priority = 0
        jwt.priority = 1
      elsif !saml.is_active? && jwt.is_active?
        saml.priority = 1
        jwt.priority = 0
      end

      save_model(saml)
      save_model(jwt)
    end

    def agent_custom_security_policy_id_set?
      !!(role_settings.agent_security_policy_id == Zendesk::SecurityPolicy::Custom.id)
    end

    def all_remote_auths_disabled?
      remote_auths.map(&:is_active).all? { |is_active| is_active == false }
    end

    def check_errors_in_disabled_remote_auths!
      errors << 'Cannot disable all remote auths with agent/end-user remote login enabled' if all_remote_auths_disabled? && (role_settings.agent_remote_login || role_settings.end_user_remote_login)
    end

    def check_errors_in_custom_agent_password!
      errors << 'Cannot customize password without selecting agent custom policy.' if agent_custom_password_settings_params? && (!allow_custom_password_update? || non_custom_security_policy_selected?)
    end

    def allow_custom_password_update?
      custom_security_policy_selected? || agent_custom_security_policy_id_set?
    end

    def check_errors_in_custom_security_policy_id!
      errors << 'Cannot update to custom without any password customizations.' if custom_security_policy_selected? && !agent_custom_password_settings_params?
    end

    def check_errors_in_remote_authentications_priorities!
      errors << 'JWT and SAML remote authentications can\'t have the same priority' if saml.is_active && jwt.is_active && jwt.priority == saml.priority
    end

    def check_agent_login_errors!(enabled_agent_logins_count)
      errors << I18n.t('txt.admin.views.settings.security_policy.update.more_than_one_auth_type_selected') if enabled_agent_logins_count > 1
    end
  end
end
