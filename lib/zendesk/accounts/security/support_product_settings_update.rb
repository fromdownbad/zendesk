module Zendesk::Accounts::Security
  class SupportProductSettingsUpdate < SettingsUpdate
    SUPPORT_PRODUCT_SETTINGS = [
      :mobile_app_access, :agreed_to_audioeye_tos, :credit_card_redaction
    ].freeze

    def perform
      settings.set(selected_settings)

      save
    end

    private

    def selected_settings
      (
        params[:account][:settings] ||
        HashWithIndifferentAccess.new
      ).slice(*SUPPORT_PRODUCT_SETTINGS)
    end
  end
end
