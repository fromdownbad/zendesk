require 'set'

module Zendesk::Accounts::Security
  class EndUserSettingsUpdate < RoleSettingsUpdate
    CORE_LOGIN_SERVICE_KEYS = Set[:end_user_zendesk_login, :end_user_remote_login].freeze
    SOCIAL_LOGIN_SERVICE_KEYS = Set[:end_user_google_login, :end_user_office_365_login, :end_user_twitter_login, :end_user_facebook_login].freeze

    def perform
      super

      if enabled_login_service?(:zendesk)
        role_settings.end_user_security_policy_id = selected_policy_id
        role_settings.attributes = selected_social_login_settings
      end

      if enabled_login_service?(:remote)
        role_settings.attributes = disabled_social_login_settings
        update_enforce_sso_settings
      end

      save
    end

    private

    def selected_settings
      params[:account][:role_settings].slice(*CORE_LOGIN_SERVICE_KEYS)
    end

    def selected_social_login_settings
      params[:account][:role_settings].slice(*SOCIAL_LOGIN_SERVICE_KEYS)
    end

    def selected_policy_id
      params[:account][:role_settings][:end_user_security_policy_id].to_i
    end

    def update_enforce_sso_settings
      role_settings.end_user_password_allowed = params[:account][:role_settings][:end_user_password_allowed] if params[:account][:role_settings].key?(:end_user_password_allowed)
    end

    def disabled_social_login_settings
      Hash[SOCIAL_LOGIN_SERVICE_KEYS.map { |s| [s, false] }]
    end

    def enabled_login_service?(*services)
      services.any? do |service|
        enabled_settings.include?("end_user_#{service}_login".to_sym)
      end
    end
  end
end
