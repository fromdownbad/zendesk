module Zendesk::Accounts::Security
  module SettingsHelper
    ASSUMPTION_VALUES = ['day', 'week', 'month', 'year', 'always', 'off'].freeze

    private

    def update_ip_ranges
      validate_ip_ranges
      account.ip_ranges = selected_ip_ranges
    end

    def validate_ip_ranges
      selected_ip_ranges.split.each do |range|
        unless Zendesk::Net::IPTools.is_valid_ip_range?(range)
          @errors << I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: range)
        end
      end
    end

    def notifications_recipient_settings
      notifications.recipient_settings(selected_notifications_recipient)
    end

    def notifications
      @notifications ||= Notifications.new(account)
    end

    def update_expiration(expiration_param)
      return unless ASSUMPTION_VALUES.include?(expiration_param)
      settings.set(assumption_duration: expiration_param)
      settings.set(assumption_expiration: expiration_date(expiration_param.to_sym))
    end

    def expiration_date(date_param)
      case date_param
      when :off
        nil
      when :day
        1.day.from_now
      when :week
        1.week.from_now
      when :month
        1.month.from_now
      when :year
        1.year.from_now
      when :always
        # set to > 100 years from now Jan 1, 2200
        Account::AssumptionSupport::ALWAYS_ASSUMABLE
      end
    end

    def role_settings
      @role_settings ||= RoleSettings.where(account_id: account.id).first_or_create
    end
  end
end
