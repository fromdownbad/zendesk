module Zendesk
  module Accounts
    class LightAgentDowngrader
      private_class_method :new

      def self.perform(account:)
        new(account: account).downgrade
      end

      def initialize(account:)
        @account = account
      end

      def downgrade
        return unless account.spp? && max_light_agents && number_to_downgrade > 0
        downgradable_agents = account.light_agents.sort_by(&:created_at).last(number_to_downgrade)
        downgradable_agents.each { |agent| Zendesk::Users::AgentDowngrader.perform(agent: agent, products: [:support]) }
      end

      private

      attr_reader :account

      def light_agent_addon
        @light_agent_addon ||= account.products(use_cache: false)&.find { |p| p.name == Zendesk::Accounts::Client::LIGHT_AGENT_ADDON.to_sym }
      end

      def max_light_agents
        @max_light_agents ||= light_agent_addon&.plan_settings.present? && light_agent_addon.plan_settings['max_light_agents']
      end

      def number_to_downgrade
        @number_to_downgrade ||= account.light_agents.count - max_light_agents
      end
    end
  end
end
