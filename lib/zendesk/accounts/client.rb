# This is the client for the 'Account Service', a.k.a, 'Pravda'
module Zendesk
  module Accounts
    class Client
      include Zendesk::CoreServices::ErrorHandling

      RETRYABLE_ERRORS = [
        Faraday::ConnectionFailed,
        Faraday::TimeoutError,
        Kragle::ServerError
      ].freeze

      SERVICE_NAME = 'account-service'.freeze

      ALL_PRODUCT = 'all'.freeze
      CHAT_PRODUCT = 'chat'.freeze
      ANSWER_BOT_PRODUCT = 'answer_bot'.freeze
      EXPLORE_PRODUCT = 'explore'.freeze
      ACME_APP_PRODUCT = 'acme_app'.freeze
      GATHER_PRODUCT = 'gather'.freeze
      GUIDE_PRODUCT = 'guide'.freeze
      SUPPORT_PRODUCT = 'support'.freeze
      TALK_PRODUCT = 'talk'.freeze
      TALK_PARTNER_PRODUCT = 'talk_partner'.freeze
      SELL_PRODUCT = 'sell'.freeze
      SOCIAL_MESSAGING_ADDON = 'social_messaging'.freeze
      LIGHT_AGENT_ADDON = 'light_agents'.freeze
      UNLIMITED_MULTIBRAND = 'unlimited_multibrand'.freeze
      DEFAULT_SUBDOMAIN = 'accounts'.freeze
      CACHE_PREFIX = 'account_service_client'.freeze
      ABUSIVE_ATTRIBUTE_NAME = 'is_abusive'.freeze
      PRODUCT_CACHE_EXPIRY = ENV.fetch('PRODUCT_CACHE_EXPIRY_SECONDS', 5.minutes).to_i
      ACCOUNT_CLIENT_ACCOUNT_CACHE_EXPIRY_SECONDS = ENV.fetch('ACCOUNT_CLIENT_ACCOUNT_CACHE_EXPIRY_SECONDS', 5.minutes).to_i

      attr_reader :account, :account_subdomain, :circuit_options, :timeout, :retry_options

      def initialize(account, account_subdomain = nil, **options)
        if account.is_a?(::Account)
          @account = account
        else
          statsd_client.increment('account_id_initialization')
          @account = ::Account.find account
        end

        @account_subdomain = account_subdomain.try(:downcase)
        @account_subdomain ||= @account.subdomain.downcase
        @circuit_options = options.fetch(:circuit_options, failure_threshold: 10, reset_timeout: 5)
        @timeout = options.fetch(:timeout, 5)
        @retry_options = options.fetch(:retry_options, {})
      end

      def expire_product_cache(product_name)
        Rails.cache.delete(product_cache_key(product_name))
        Rails.cache.delete(product_cache_key(ALL_PRODUCT))
      end

      def expire_account_client_account_cache
        Rails.cache.delete(account_client_account_cache_key)
      end

      def zuora_billing_id(use_cache: true)
        account_from_account_service(use_cache).try(:[], 'billing_id')
      end

      def owner_id(use_cache: true)
        account_from_account_service(use_cache).try(:[], 'owner_id')
      end

      def abusive(use_cache: true)
        account_from_account_service(use_cache).try(:[], ABUSIVE_ATTRIBUTE_NAME) == true
      end

      def account_from_account_service(use_cache)
        cache_hit = true

        Rails.cache.fetch(account_client_account_cache_key, force: !use_cache, expires_in: ACCOUNT_CLIENT_ACCOUNT_CACHE_EXPIRY_SECONDS) do
          begin
            cache_hit = false
            response = pravda_connection.get(account_url)
            response.body['account']
          rescue Kragle::ResourceNotFound
            nil
          end
        end
      rescue Kragle::ResponseError, Faraday::Error => e
        message = "Failed to GET account: #{account.idsub} in account service: #{e.message}"
        Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
        nil
      ensure
        if use_cache
          statsd_client.increment("accounts.cache", tags: ["cache_hit:#{cache_hit}"])
        end
      end

      def update_account(attributes)
        response = pravda_connection.patch(account_url, account: attributes)
        response.body['account']
      rescue Kragle::ResourceNotFound => error
        message = "Account service account not found: #{error.message}"
        Rails.logger.error(message + "\n" + error.backtrace.join("\n"))
      end

      def products(include_deleted_account: false, use_cache: false)
        cache_hit = true

        Rails.cache.fetch(product_cache_key(ALL_PRODUCT), force: !use_cache, expires_in: PRODUCT_CACHE_EXPIRY) do
          cache_hit = false

          url = products_url + (include_deleted_account ? '?include_deleted_account=true' : '')
          res = pravda_connection.get(url)
          res.body['products'].map { |p| Zendesk::Accounts::Product.new(p) }
        end
      rescue Kragle::TooManyRequests
        statsd_client.increment('too_many_requests')
        Rails.logger.error('Too many requests to account service. Try again later')
        []
      ensure
        if use_cache
          statsd_client.increment('products.cache', tags: ["cache_hit:#{cache_hit}"])
        end
      end

      def product(product_name, use_cache: false)
        cache_hit = true

        if use_cache
          cached_product_from_products(product_name)
        else
          Rails.cache.fetch(product_cache_key(product_name), force: !use_cache, expires_in: PRODUCT_CACHE_EXPIRY) do
            cache_hit = false

            begin
              product!(product_name)
            rescue Kragle::ResourceNotFound
              nil
            end
          end
        end
      rescue Kragle::ResponseError, Faraday::Error => e
        message = "Failed to GET product in account service: #{e.message}"
        Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
        nil
      ensure
        if use_cache
          statsd_client.increment('product.cache', tags: ["product_name:#{product_name}", "cache_hit:#{cache_hit}"])
        end
      end

      def product!(product_name)
        response = pravda_connection.get("#{products_url}/#{product_name}")
        product  = response.body['product']

        Zendesk::Accounts::Product.new(product)
      end

      def sku!(sku_name)
        response = pravda_connection.get("#{skus_url}/#{sku_name}")

        response.body['sku']
      rescue Kragle::ResourceNotFound
        nil
      end

      def update_or_create_product(product_name, product_params, include_deleted_account: false, context: nil)
        tags = ["context:#{context}", "product_name:#{product_name}"]

        begin
          call_update_product(product_name, product_params, include_deleted_account: include_deleted_account)
        rescue Kragle::ResourceNotFound
          begin
            call_create_product(product_name, product_params, include_deleted_account: include_deleted_account)
          rescue Kragle::ResponseError, Faraday::Error => e
            statsd_client.increment('update_or_create_product', tags: tags + ["result:error", "error:#{e.class}", "fallback:1"])
            message = "Failed to create (after fallback from update) '#{product_name}' product record for account_id '#{account.id}' with #{product_params}: #{e.message}"
            Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
          else
            statsd_client.increment('update_or_create_product', tags: tags + ["result:ok", "fallback:1"])
          end
        rescue Kragle::ResponseError, Faraday::Error => e
          statsd_client.increment('update_or_create_product', tags: tags + ["result:error", "error:#{e.class}", "fallback:0"])
          message = "Failed to update (did not fallback to create) '#{product_name}' product record for account_id '#{account.id}' with #{product_params}: #{e.message}"
          Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
        else
          statsd_client.increment('update_or_create_product', tags: tags + ["result:ok", "fallback:0"])
        end
      end

      def update_or_create_product!(product_name, product_params, include_deleted_account: false, context: nil)
        with_instrumentation("update_or_create_product_bang", ["context:#{context}", "product_name:#{product_name}"]) do
          begin
            call_update_product(product_name, product_params, include_deleted_account: include_deleted_account)
          rescue Kragle::ResourceNotFound
            call_create_product(product_name, product_params, include_deleted_account: include_deleted_account)
          end
        end
      end

      def create_product(product_name, product_params, include_deleted_account: false, context: nil)
        with_instrumentation("create_product", ["context:#{context}", "product_name:#{product_name}"]) do
          call_create_product(product_name, product_params, include_deleted_account: include_deleted_account)
        end
      rescue Kragle::ResponseError, Faraday::Error => e
        message = "Failed to create '#{product_name}' product record for account_id '#{account.id}' with #{product_params}: #{e.message}"
        Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
      end

      def create_product!(product_name, product_params, include_deleted_account: false, context: nil)
        with_instrumentation("create_product_bang", ["context:#{context}", "product_name:#{product_name}"]) do
          call_create_product(product_name, product_params, include_deleted_account: include_deleted_account)
        end
      end

      def update_product(product_name, product_params, include_deleted_account: false, if_unmodified_since: nil, context: nil)
        with_instrumentation("update_product", ["context:#{context}", "product_name:#{product_name}"]) do
          call_update_product(product_name, product_params, include_deleted_account: include_deleted_account, if_unmodified_since: if_unmodified_since)
        end
      rescue Kragle::ResourceNotFound, Kragle::ResponseError, Faraday::Error => e
        message = "Failed to update '#{product_name}' product record for account_id '#{account.id}' with #{product_params}: #{e.message}"
        Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
      end

      def update_product!(product_name, product_params, include_deleted_account: false, if_unmodified_since: nil, context: nil)
        with_instrumentation("update_product_bang", ["context:#{context}", "product_name:#{product_name}"]) do
          call_update_product(product_name, product_params, include_deleted_account: include_deleted_account, if_unmodified_since: if_unmodified_since)
        end
      end

      def update_or_create_sku(sku_name, sku_params, include_deleted_account: false, context: nil)
        tags = ["context:#{context}", "sku_name:#{sku_name}"]

        begin
          call_update_sku(sku_name, sku_params, include_deleted_account: include_deleted_account)
        rescue Kragle::ResourceNotFound
          begin
            call_create_sku(sku_name, sku_params, include_deleted_account: include_deleted_account)
          rescue Kragle::ResponseError, Faraday::Error => e
            statsd_client.increment('update_or_create_sku', tags: tags + ["result:error", "error:#{e.class}", "fallback:1"])
            message = "Failed to create (after fallback from update) '#{sku_name}' sku record for account_id '#{account.id}' with #{sku_params}: #{e.message}"
            Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
          else
            statsd_client.increment('update_or_create_sku', tags: tags + ["result:ok", "fallback:1"])
          end
        rescue Kragle::ResponseError, Faraday::Error => e
          statsd_client.increment('update_or_create_sku', tags: tags + ["result:error", "error:#{e.class}", "fallback:0"])
          message = "Failed to update (did not fallback to create) '#{sku_name}' sku record for account_id '#{account.id}' with #{sku_params}: #{e.message}"
          Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
        else
          statsd_client.increment('update_or_create_sku', tags: tags + ["result:ok", "fallback:0"])
        end
      end

      def update_or_create_sku!(sku_name, sku_params, include_deleted_account: false, context: nil)
        with_instrumentation("update_or_create_sku_bang", ["context:#{context}", "sku_name:#{sku_name}"]) do
          begin
            call_update_sku(sku_name, sku_params, include_deleted_account: include_deleted_account)
          rescue Kragle::ResourceNotFound
            call_create_sku(sku_name, sku_params, include_deleted_account: include_deleted_account)
          end
        end
      end

      def create_sku(sku_name, sku_params, include_deleted_account: false, context: nil)
        with_instrumentation("create_sku", ["context:#{context}", "sku_name:#{sku_name}"]) do
          call_create_sku(sku_name, sku_params, include_deleted_account: include_deleted_account)
        end
      rescue Kragle::ResponseError, Faraday::Error => e
        message = "Failed to create '#{sku_name}' sku record for account_id '#{account.id}' with #{sku_params}: #{e.message}"
        Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
      end

      def create_sku!(sku_name, sku_params, include_deleted_account: false, context: nil)
        with_instrumentation("create_sku_bang", ["context:#{context}", "sku_name:#{sku_name}"]) do
          call_create_sku(sku_name, sku_params, include_deleted_account: include_deleted_account)
        end
      end

      def update_sku(sku_name, sku_params, include_deleted_account: false, if_unmodified_since: nil, context: nil)
        with_instrumentation("update_sku", ["context:#{context}", "sku_name:#{sku_name}"]) do
          call_update_sku(sku_name, sku_params, include_deleted_account: include_deleted_account, if_unmodified_since: if_unmodified_since)
        end
      rescue Kragle::ResourceNotFound, Kragle::ResponseError, Faraday::Error => e
        message = "Failed to update '#{sku_name}' sku record for account_id '#{account.id}' with #{sku_params}: #{e.message}"
        Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
      end

      def update_sku!(sku_name, sku_params, include_deleted_account: false, if_unmodified_since: nil, context: nil)
        with_instrumentation("update_sku_bang", ["context:#{context}", "sku_name:#{sku_name}"]) do
          call_update_sku(sku_name, sku_params, include_deleted_account: include_deleted_account, if_unmodified_since: if_unmodified_since)
        end
      end

      def chat_product
        products.find { |p| p.name == CHAT_PRODUCT.to_sym }
      end

      def cached_product_from_products(product_name)
        products(use_cache: true)&.find { |p| p.name == product_name.to_sym }
      end

      def chat_products(limit, starting_after)
        res = pravda_connection.get(global_products_url(limit, starting_after))
        res.body['products'].map { |p| Zendesk::Accounts::Product.new(p) }
      rescue Kragle::ResponseError, Faraday::Error => e
        message = "Failed to get chat products: #{e.message}"
        Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
        []
      end

      def url_prefix
        ENV.fetch("ACCOUNT_SERVICE_URL") # Fail noisily
      end

      def account_service_host
        "#{account_subdomain}.#{Zendesk::Configuration['host']}"
      end

      def products_url
        "#{account_url}/products"
      end

      def skus_url
        "/api/services/sku/#{account.id}/skus"
      end

      def max_light_agents
        pravda_connection.get("/api/services/accounts/#{account.id}/products/light_agents").
          body.deep_symbolize_keys.dig(:product, :plan_settings, :max_light_agents).to_i
      rescue Kragle::ResponseError, Faraday::ClientError => e
        Rails.logger.error("Failed to get max light agents for account #{account.idsub} from account services: #{e.message}\n#{e.backtrace.join('\n ')}")
        raise
      end

      private

      def pravda_connection
        @pravda_connection ||= begin
          Kragle.new(SERVICE_NAME, circuit_options: circuit_options, retry_options: retry_options).tap do |connection|
            connection.options.timeout = timeout
            connection.headers['Host'] = account_service_host
            connection.url_prefix = url_prefix
          end
        end
      end

      def call_create_product(product_name, product_params, include_deleted_account: false)
        pravda_connection.post("#{products_url}?include_deleted_account=#{include_deleted_account}", product_params.symbolize_keys.deep_merge(product: { name: product_name }))
      end

      def call_update_product(product_name, product_params, include_deleted_account: false, if_unmodified_since: nil)
        pravda_connection.patch do |request|
          request.url("#{products_url}/#{product_name}?include_deleted_account=#{include_deleted_account}")
          request.headers['If-Unmodified-Since'] = if_unmodified_since.httpdate if if_unmodified_since
          request.body = product_params
        end
        expire_product_cache(product_name)
      end

      def call_create_sku(sku_name, sku_params, include_deleted_account: false)
        pravda_connection.post("#{skus_url}?include_deleted_account=#{include_deleted_account}", sku_params.symbolize_keys.deep_merge(sku: { name: sku_name }))
      end

      def call_update_sku(sku_name, sku_params, include_deleted_account: false, if_unmodified_since: nil)
        pravda_connection.patch do |request|
          request.url("#{skus_url}/#{sku_name}?include_deleted_account=#{include_deleted_account}")
          request.headers['If-Unmodified-Since'] = if_unmodified_since.httpdate if if_unmodified_since
          request.body = sku_params
        end
      end

      def account_url
        "/api/services/accounts/#{account.id}"
      end

      def global_products_url(limit, starting_after)
        "/api/services/products?name=chat&limit=#{limit}&starting_after=#{starting_after}"
      end

      def account_client_account_cache_key
        "#{CACHE_PREFIX}/#{account.id}/account"
      end

      def product_cache_key(product_name)
        "#{CACHE_PREFIX}/#{account.id}/products/#{product_name}"
      end
    end
  end
end
