module Zendesk
  module Accounts
    module ProductPayload
      class << self
        def for_support(account, suite: nil)
          suite ||= account.settings.suite_trial? && !account.spp?
          plan_settings = {
            plan_type: account.subscription.plan_type,
            max_agents: account.subscription.max_agents,
            light_agents: account.subscription.has_light_agents?,
            insights: Arturo.feature_enabled_for?(:new_accounts_use_explore, account) ? :explore : :good_data,
            suite: suite,
            agent_workspace: account.settings.check_group_name_uniqueness?
          }
          trial_product(account, plan_settings)
        end

        def for_social_messaging(account, suite: false)
          plan_settings = { whatsapp_numbers_purchased: 0, suite: suite }
          trial_product(account, plan_settings)
        end

        private

        def trial_product(account, plan_settings)
          settings = {
            product: {
              state: Zendesk::Accounts::Product::TRIAL,
              plan_settings: plan_settings
            }
          }
          settings[:product][:trial_expires_at] = trial_expiry_timestamp(account) unless account.spp?
          settings
        end

        def trial_expiry_timestamp(account)
          stamp = account.subscription.trial_expires_on || account.subscription.default_trial_expiry

          stamp.to_datetime.iso8601
        end
      end
    end
  end
end
