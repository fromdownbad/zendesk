module Zendesk
  module Accounts
    class CreationParamsValidator
      include ActiveModel::Validations

      validate :valid_owner_email
      validate :valid_owner_name_cjk_char_count
      validates_presence_of :owner_name
      validates_presence_of :address_phone

      def initialize(params)
        @params = params
      end

      private

      attr_accessor :params

      def valid_owner_email
        unless Zendesk::Mail::Address.valid_address?(owner_email)
          errors.add(:owner_email, ::I18n.t('activerecord.errors.models.user_identity.attributes.value.invalid', value: CGI.escapeHTML(owner_email)))
        end
      end

      MAXIMUM_CJK_CHARS = 8
      CJK_CHAR_REGEX = /(?=\p{Han}|\p{Katakana}|\p{Hiragana}|\p{Hangul})/.freeze
      WEBSITE_NAME_REGEX = /[^ ]+(.com|.co.uk)/.freeze

      def valid_owner_name_cjk_char_count
        if Arturo.feature_enabled_for_pod?(:ocp_limit_owner_name_cjk_characters_on_account_creation, Zendesk::Configuration.fetch(:pod_id))
          if too_many_cjk_chars(owner_name) || contains_cjk_chars_and_website(owner_name)
            errors.add(:owner_name, ::I18n.t('activerecord.errors.models.user_identity.attributes.value.invalid', value: CGI.escapeHTML(owner_name)))
            report_cjk_spam_to_datadog
          end
        else
          true
        end
      end

      def contains_cjk_chars_and_website(str)
        (cjk_char_count(str) > 0) && contains_website_name(str)
      end

      def contains_website_name(str)
        return false unless str
        str.scan(WEBSITE_NAME_REGEX).count > 0
      end

      def too_many_cjk_chars(str)
        cjk_char_count(str) > MAXIMUM_CJK_CHARS
      end

      def cjk_char_count(str)
        return 0 unless str
        str.scan(CJK_CHAR_REGEX).count
      end

      def report_cjk_spam_to_datadog
        statsd_client.increment('new_account_cjk_spam')
      end

      def statsd_client
        @statsd_client ||= ::Zendesk::StatsD::Client.new(namespace: %w[account_creation cjk])
      end

      def owner_params
        params[:owner]
      end

      def owner_name
        owner_params[:name] if owner_params
      end

      def owner_email
        @owner_email ||= owner_params[:email].downcase if owner_params
      end

      def address_params
        params[:address]
      end

      def address_phone
        address_params[:phone] if address_params
      end
    end
  end
end
