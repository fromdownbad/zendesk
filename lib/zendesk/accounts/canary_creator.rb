require 'securerandom'

module Zendesk
  module Accounts
    class CanaryCreator
      NUM_BRANDS = 5
      NUM_ORGANIZATIONS = 10
      NUM_GROUPS = 15
      NUM_SHARING_AGREEMENTS = 5
      NUM_TICKET_FORMS = 25
      NUM_AGENTS = 30
      NUM_END_USERS = 50
      NUM_MAX_AGENTS = NUM_AGENTS + 2
      PLAN_TYPE = SubscriptionPlanType.ExtraLarge

      class << self
        def create_for_shard!(shard_id, subdomain = nil)
          subdomain ||= "new-canary-shard-#{shard_id}"
          create!(shard_id, subdomain, "Z3N Canary Account for shard #{shard_id}")
        end

        def create_for_cluster!(cluster_id, shard_id, subdomain = nil)
          subdomain ||= "new-canary-cluster-#{cluster_id.tr('.', '-')}"
          create!(shard_id, subdomain, "Z3N Canary Account for cluster #{cluster_id}")
        end

        def create!(shard_id, subdomain, name)
          Account.on_master do
            ActiveRecord::Base.on_shard(shard_id) do
              raise "canary account already exists for shard #{shard_id}" if Account.find_by_subdomain(subdomain)

              CIA.audit actor: User.system do
                account = ::Accounts::Classic.new do |a|
                  a.shard_id       = shard_id
                  a.subdomain      = subdomain
                  a.name           = name
                  a.time_zone      = "Pacific Time (US & Canada)"
                  a.help_desk_size = "Small team"

                  a.set_address(country_code: "US", phone: '123456')

                  a.set_owner(
                    name: 'Infrastructure Team',
                    email: "#{Rails.env}-#{subdomain}@status-monitor.aws1.zende.sk",
                    password: SecureRandom.hex + 'Aa1-',
                    skip_verification: true
                  )
                end
                account.save!

                account.subscription.update_attributes!(
                  managed_update: true,
                  manual_discount: 100,
                  manual_discount_expires_on: 10.years.from_now.to_date,
                  is_trial: false,
                  plan_type: PLAN_TYPE
                )

                account.subscription.update_max_agents!(NUM_MAX_AGENTS)

                # We reset the password because we can't specify a non-conforming password
                # at account creation time.
                account.role_settings.update_attributes!(
                  agent_security_policy_id: Zendesk::SecurityPolicy::Low.id
                )

                account.owner.password = "coulditpossiblywork"

                account.settings.admins_can_set_user_passwords = true

                # Set up CSV export tests
                account.csv_export_records.create!(user_id: -1)
                account.settings.export_accessible_types += ['report_feed']

                # Disable HC for web portal
                account.settings.web_portal_state = :disabled
                account.settings.help_center_state = :enabled

                account.settings.prefer_lotus = true

                account.save!
                account.owner.save!

                account.update_attribute(:is_serviceable, true)

                feature = Arturo::Feature.find_by_symbol(:allow_login_on_get)
                if feature
                  feature.external_beta_subdomains += [account.subdomain]
                  feature.save!
                end

                seed(account)
                update_product_record(account)

                account
              end
            end
          end
        end

        def seed(account)
          # Add minimum ticket set for canary accounts
          ############################################

          # groups
          groups = []
          NUM_GROUPS.times do |i|
            groups[i] = account.groups.new(name: "group#{i}")
            groups[i].save!
          end

          # organizations
          organizations = []
          NUM_ORGANIZATIONS.times do |i|
            organizations[i] = account.organizations.new(name: "organization#{i}")
            organizations[i].save!
          end

          # agents
          agents = []
          NUM_AGENTS.times do |i|
            agents[i] = account.users.new(
              name: "Agent #{i}",
              email: "agent#{i}@#{account.subdomain}.example.com",
              group: groups[i % NUM_GROUPS]
            )
            agents[i].roles = 4
            agents[i].save!
          end

          # end users
          end_users = []
          NUM_END_USERS.times do |i|
            end_users[i] = account.users.new(
              name: "End User #{i}",
              email: "end_user#{i}@#{account.subdomain}.example.com",
              organization: organizations[i % NUM_ORGANIZATIONS]
            )
            end_users[i].save!
          end

          # sharing agreements
          sharing_agreements = []
          NUM_SHARING_AGREEMENTS.times do |i|
            sharing_agreements[i] = account.sharing_agreements.new(remote_url: "http://#{account.subdomain}#{i}.zendesk.com", status: :accepted)
            sharing_agreements[i].save!
          end

          # ticket forms
          ticket_forms = []
          NUM_TICKET_FORMS.times do |i|
            ticket_forms[i] = account.ticket_forms.new(name: "Ticket Form #{i}", end_user_visible: true, display_name: "Ticket Form #{i}", position: i, active: true, default: false)
            ticket_forms[i].save!
          end

          # brands
          brands = []
          NUM_BRANDS.times do |i|
            route = account.routes.new(subdomain: "#{account.subdomain}brand#{i}")
            route.save

            brands[i] = account.brands.new(name: "brand#{i}")
            brands[i].route = route
            brands[i].save
          end
        end

        private

        def update_product_record(account)
          product_params = {
            product: {
              state: 'free',
              plan_settings: {
                plan_type: account.subscription.plan_type,
                max_agents: account.subscription.max_agents,
                light_agents: account.subscription.has_light_agents?
              }
            }
          }

          Zendesk::Accounts::Client.new(
            account,
            Zendesk::Accounts::Client::DEFAULT_SUBDOMAIN,
            retry_options: {
              max: 3,
              interval: 2,
              exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS.dup << Kragle::ResourceNotFound,
              methods: [:patch]
            },
            timeout: 10
          ).update_product('support', product_params, context: "canary_creator")
        end
      end
    end
  end
end
