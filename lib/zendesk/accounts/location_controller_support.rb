module Zendesk::Accounts
  module LocationControllerSupport
    TRIAL_EXTRAS              = 'trial_extras'.freeze
    INFERRED_IP_ADDRESS       = 'Inferred_IP_Address'.freeze
    INFERRED_CCODE            = 'Inferred_CCode'.freeze
    RESTRICTED_COUNTRY_CODES = [
      'CU', # Cuba
      'IR', # Iran
      'SY', # Syria
      'KP', # North Korea
      'PIRATE' # Means the pirate option will only display in i18n pages
    ].freeze

    def self.included(base)
      base.before_action :check_restricted_jurisdiction!, only: :create, if: :geo_ip_info
      base.before_action :set_address,               only: :create, if: :geo_ip_info
      base.before_action :set_region,                only: [:create, :create_new_account]
      base.before_action :set_time_zone,             only: :create, if: :geo_ip_info
      base.before_action :set_params_currency,       only: [:create, :create_new_account], if: :geo_ip_info
    end

    private

    attr_reader :region

    def check_restricted_jurisdiction!
      if in_restricted_jurisdiction?
        respond_to do |format|
          format.json do
            Rails.logger.warn(
              "Account creation failed: IP address identified as " \
              "coming from restricted jurisdiction. Country code: #{geo_ip_info[:country_code]}, " \
              "Lat: #{geo_ip_info[:latitude]}, Lng: #{geo_ip_info[:longitude]}, City: #{geo_ip_info[:city]}"
            )
            render(
              json: {
                success: false,
                message: "You appear to be in a Prohibited Jurisdiction under our <a href='https://www.zendesk.com/company/customers-partners/#master-subscription-agreement'>Master Subscription Agreement</a>. If this is an error, please email legal@zendesk.com.",
              },
              callback: params[:callback],
              status: :forbidden
            )
          end
        end
      end
    end

    def set_address
      if country_code = geo_ip_info[:country_code]
        params[:address] ||= {}
        params[:address][:country_code] = country_code
      end
    end

    ## There is no default value in this function because it is used in too
    #  many places. the default will be set in the places calling this function
    def set_region
      country_code = request.params.dig(TRIAL_EXTRAS, INFERRED_CCODE)
      @region = Zendesk::Accounts::CreationRegion.lookup(country_code)
    end

    def set_time_zone
      if params[:account]
        params[:account][:utc_offset] ||= geo_ip_info[:timezone]
      end
    end

    def geo_lookup_currency
      country = Country.find_by_code(geo_ip_info[:country_code])
      country.currency unless country.nil?
    end

    def set_params_currency
      if params[:account]
        params[:account][:currency] ||= geo_lookup_currency || 'USD'
      end
    end

    def in_ofac_regulation?
      in_restricted_zone? || in_restricted_city?
    end

    def in_restricted_jurisdiction?
      in_restricted_country? || in_ofac_regulation?
    end

    def in_restricted_country?
      RESTRICTED_COUNTRY_CODES.include?(geo_ip_info[:country_code])
    end

    def in_restricted_zone?
      lat = geo_ip_info[:latitude]
      long = geo_ip_info[:longitude]

      Country::RESTRICTED_ZONES.any? do |restricted_zone|
        restricted_zone[:latitude_range].cover?(lat) &&
        restricted_zone[:longitude_range].cover?(long)
      end
    end

    def in_restricted_city?
      city = geo_ip_info[:city].to_s

      Country::RESTRICTED_CITIES.any? do |restricted_city|
        restricted_city.casecmp(city) == 0
      end
    end

    def original_remote_ip
      @original_remote_ip ||= Zendesk::Net::IPTools.untrusted_ip(request.env)
    end

    def inferred_ip
      request.params.dig(TRIAL_EXTRAS, INFERRED_IP_ADDRESS)
    end

    def geo_ip_info
      ip = inferred_ip || original_remote_ip

      @geo_ip_info ||= begin
        if location = Zendesk::GeoLocation.locate(ip)
          location.slice(:country_code, :region, :timezone, :latitude, :longitude, :city)
        else
          Rails.logger.warn("GeoIP database did not return information for #{original_remote_ip}")
          nil
        end
      rescue StandardError => e
        Rails.logger.warn("Unable to extract geoip information for #{original_remote_ip}")
        ZendeskExceptions::Logger.record(e, location: self, message: "GeoIP information extraction failure for #{original_remote_ip}: #{e.message}", fingerprint: 'dd1e457934a87564939285e444bca9cdf6d0dcb5')
      end
    end
  end
end
