module Zendesk
  module Accounts
    class CustomRolesResolver
      def initialize(current_account)
        @current_account = current_account
      end

      def enabled_permission_sets?
        current_account.has_permission_sets? ||
          current_account.has_light_agents? ||
          current_account.has_chat_permission_set? ||
          contributor_permission_set.exists? ||
          billing_admin_permission_set.exists?
      end

      def assignable_permission_sets
        permission_sets = []
        permission_sets = non_system_custom_roles.to_a if current_account.has_permission_sets?
        permission_sets << current_account.chat_permission_set if current_account.has_chat_permission_set?
        permission_sets << current_account.light_agent_permission_set if light_agent_permission_set?
        permission_sets << contributor_permission_set.first if contributor_permission_set.exists?
        permission_sets << billing_admin_permission_set.first if billing_admin_permission_set.exists?
        permission_sets
      end

      private

      attr_reader :current_account

      def billing_admin_permission_set
        @billing_admin_permission_set ||= current_account.permission_sets.where(role_type: ::PermissionSet::Type::BILLING_ADMIN)
      end

      def contributor_permission_set
        @contributor_permission_set ||= current_account.permission_sets.where(role_type: ::PermissionSet::Type::CONTRIBUTOR)
      end

      def non_system_custom_roles
        @non_system_custom_roles ||= current_account.permission_sets.where(role_type: ::PermissionSet::Type::CUSTOM)
      end

      def light_agent_permission_set?
        current_account.has_light_agents? && current_account.light_agent_permission_set
      end
    end
  end
end
