module Zendesk
  module Accounts
    class ProductSyncValidator
      attr_reader :account, :subscription, :instrument_results
      TIME_FUZZY_FACTOR = 1.minute

      def self.validate(account, instrument_results: false, light_agents: nil)
        return if account.id < 1 # skip system account
        return if account.is_sandbox? # skip sandbox accounts
        return if account.subscription.blank? # skip multiproduct accounts without Support product
        return unless account.is_active? # skip inactive accounts

        Rails.logger.info("Performing account service product sync validation for account #{account.idsub}")
        new(account, instrument_results, light_agents: light_agents).compare_product_expected_vs_actual
      end

      def initialize(account, instrument_results, light_agents: nil)
        @account = account
        @subscription = account.subscription
        @instrument_results = instrument_results
        @validate_light_agents = light_agents.nil? ? Arturo::Feature.find_feature(:voltron_product_sync_validate_light_agents).try(:phase) == 'on' : light_agents
      end

      def compare_product_expected_vs_actual
        current_datetime = Time.now.to_datetime.iso8601

        result = if account.pre_created_account? # validate no support product record exists
          support_product.nil? ? :pass : :fail
        elsif support_product.nil?
          :no_product_record
        elsif comparison_results.values.all? { |value| value == :pass }
          :pass
        else
          :fail
        end

        comparison_data = {
          timestamp: current_datetime,
          result: result,
          comparison_results: comparison_results,
          expected_product_attributes: expected_product_attributes,
          actual_product_attributes: actual_product_attributes,
          account: account,
          support_product: support_product
        }

        if instrument_results
          send_statsd_metric(comparison_data)
          if result != :pass
            Rails.logger.warn("Support product record mismatch for account #{account.idsub}. comparison_data: #{comparison_data}")
            send_statsd_event(comparison_data)
          end
        end

        comparison_data
      rescue Kragle::ResponseError, Faraday::Error => e
        message = "Failed to retrieve products list from account service for account #{account.idsub}: #{e.message}"
        Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
        statsd.increment('comparison.aborted', tags: [])
        {
          timestamp: current_datetime,
          result: :aborted,
          comparison_results: {},
          expected_product_attributes: expected_product_attributes,
          actual_product_attributes: {},
          account: account,
          support_product: nil
        }
      end

      def products
        @products ||= account_client.products(include_deleted_account: true)
      end

      def account_client
        @account_client ||= Zendesk::Accounts::Client.new(
          account,
          Zendesk::Accounts::Client::DEFAULT_SUBDOMAIN,
          retry_options: {
            max: 3,
            interval: 2,
            exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS
          }
        )
      end

      def support_product
        @support_product ||= products.find { |product| product.name == :support }
      end

      def expected_product_attributes
        @attributes ||= begin
          product = Zendesk::Accounts::SupportProductMapper.derive_product(account)

          attributes = {
            state: product.try(:[], :state),
            trial_expires_at: product.try(:[], :trial_expires_at),
            plan_type: product.try(:[], :plan_settings).try(:[], :plan_type),
            max_agents: product.try(:[], :plan_settings).try(:[], :max_agents),
            light_agents: product.try(:[], :plan_settings).try(:[], :light_agents)
          }

          attributes
        end
      end

      def actual_product_attributes
        return {} if support_product.nil?

        {
          state: support_product.state,
          trial_expires_at: support_product.trial_expires_at.try(:iso8601),
          plan_type: support_product.plan_settings['plan_type'],
          max_agents: support_product.plan_settings['max_agents'],
          light_agents: support_product.plan_settings['light_agents'],
          state_updated_at: support_product.state_updated_at.try(:iso8601)
        }
      end

      def comparison_results
        @comparison_results ||= if support_product.nil?
          {}
        else
          {}.tap do |results|
            expected_product_attributes.each do |key, expected_value|
              results[key] =
                if key == :trial_expires_at
                  # If the account is subscribed (i.e. zuora_managed?) we don't fail if
                  # trial_expires_on does not match
                  validate_trial_expires_on(actual_product_attributes[key], expected_value)
                elsif key == :light_agents && !@validate_light_agents
                  :pass
                elsif actual_product_attributes[key] == expected_value
                  :pass
                else
                  :fail
                end
            end
          end
        end
      end

      def send_statsd_metric(comparison_data)
        statsd.increment("comparison.#{comparison_data[:result]}", tags: statsd_tags(comparison_data))
      end

      def send_statsd_event(comparison_data)
        statsd.event(
          'Support product sync validation',
          statsd_event_message(comparison_data),
          alert_type: 'info',
          tags: statsd_tags(comparison_data),
          aggregation_key: 'support_product_sync_validation'
        )
      end

      def field_comparison_markdown
        field_comparison_body = [:state, :state_updated_at, :trial_expires_at, :plan_type, :max_agents, :light_agents].map do |product_attr|
          result = comparison_results[product_attr]
          [
            "- *#{product_attr}*",
            result == :fail ? "**fail**" : result,
            "Expected: #{expected_product_attributes[product_attr]}",
            "Actual: #{actual_product_attributes[product_attr]}"
          ]
        end

        field_comparison_body.map { |row| row.join(" | ") }.join("\n")
      end

      def misc_data_markdown
        [
          ["- *pod_id*", account.pod_id],
          ["- *shard_id*", account.shard_id],
          ["- *support_product_created_at*", support_product.try(:created_at)],
          ["- *support_product_updated_at*", support_product.try(:updated_at)],
          ["- *is_serviceable*", account.is_serviceable?],
          ["- *is_active*", account.is_active?],
          ["- *churned_on*", account.subscription.churned_on.try(:iso8601)],
          ["- *deleted_at*", account.deleted_at.try(:iso8601)],
          ["- *is_trial*", account.subscription.is_trial?],
          ["- *trial_expired*", account.subscription.trial_expired?],
          ["- *zuora_managed*", account.subscription.zuora_managed?],
          ["- *billing_id*", account.billing_id || account.zuora_subscription.try(:zuora_account_id)],
          ["- *multiproduct billing participant*", account.billing_multi_product_participant?],
          ["- *sales_model*", account.zuora_subscription.try(:sales_model)],
          ["- *account_type*", account.subscription.account_type],
          ["- *account_status*", account.subscription.account_status],
          ["- *payment_method_type*", PaymentMethodType[account.subscription.payment_method_type].name],
          ["- *manual_discount*", account.subscription.manual_discount],
          ["- *multiproduct*", account.multiproduct?],
          ["- *pre_account_creation_bound_at*", account.pre_account_creation.try(:bound_at).try(:iso8601)],
          ["- *pre_account_creation_status*", account.pre_account_creation.try(:status)],
          ["- [monitor support page](#{account_monitor_url})"]
        ].map { |row| row.join(": ") }.join("\n")
      end

      def monitor_host
        case Rails.env
        when "production"
          "monitor.zende.sk"
        when "staging"
          account.pod_id > 101 ? "monitor.zendesk-staging.com" : "monitor.zd-staging.com"
        else
          "monitor.zd-dev.com"
        end
      end

      def account_monitor_url
        "https://#{monitor_host}/accounts/#{account.id}/support"
      end

      def statsd_event_message(comparison_data)
        heading = "## #{comparison_data[:result].to_s.humanize} - Account #{account.idsub}"
        timestamp = "Validated at #{comparison_data[:timestamp]}"

        ["%%%", heading, timestamp, field_comparison_markdown, misc_data_markdown, "%%%"].join("\n\n")
      end

      def statsd_tags(comparison_data)
        [
          "shard:#{account.shard_id}",
          "validation_result:#{comparison_data[:result]}",
          "expected_state:#{comparison_data[:expected_product_attributes][:state]}",
          "actual_state:#{comparison_data[:actual_product_attributes][:state]}",
          "billing:#{account.subscription.zuora_managed? || account.billing_id.present?}",
        ] + comparison_data[:comparison_results].map { |k, v| "#{k}:#{v}" }
      end

      def statsd
        @statsd ||= Zendesk::StatsD::Client.new(namespace: 'account_product_sync')
      end

      def validate_trial_expires_on(actual, expected)
        return :pass if actual == expected
        subscription.zuora_managed? ? :pass : :fail
      end
    end
  end
end
