require 'csv'

#
# This is a utility class used to generate a CSV file listing account
# records that have been invoiced.
#
# USAGE:
#
#   Zendesk::Accounts::InvoicedCustomerReport.generate                  # will generate a file named invoiced-customer-report-2013-02-21.csv
#   Zendesk::Accounts::InvoicedCustomerReport.generate('invoiced.csv')  # will generate a file named invoiced.csv
#
# Take care to run this behind a screen as it could take some time (~1 hour)
#
#
module Zendesk
  module Accounts
    class InvoicedCustomerReport
      def self.generate(path = nil)
        new.save(path)
      end

      def to_csv
        CSV.generate(force_quotes: true, row_sep: "\r\n") do |csv|
          emit_header_for csv
          emit_rows_for csv
        end
      end

      def save(path)
        path ||= build_filename

        CSV.open(path, "w") do |csv|
          emit_header_for csv
          emit_rows_for csv
        end
      end

      private

      def build_filename
        Date.today.strftime("invoiced-customer-report-%Y-%m-%d.csv")
      end

      def emit_header_for(csv)
        csv << [
          'Zendesk Account ID',
          'Billing Cycle',
          'Plan',
          'Latest Paid Date',
          'Latest Invoiced Date',
          'Max Agents',
          'Subdomain',
          'Hub Account ID'
        ]
      end

      def emit_rows_for(csv)
        Account.where('id > 0').includes(:subscription, :payments).find_each(batch_size: 5000) do |account|
          subscription = account.subscription
          next unless subscription.try(:invoicing?)

          fields = []
          fields << account.id
          fields << BillingCycleType[subscription.billing_cycle_type]
          fields << SubscriptionPlanType[subscription.plan_type]
          fields << subscription.payments.paid.standard.last.try(:period_end_at)
          fields << subscription.payments.invoiced.standard.last.try(:period_end_at)
          fields << subscription.max_agents
          fields << account.subdomain
          fields << subscription.hub_account_id

          csv << fields.map { |f| f }
        end

        nil
      end
    end
  end
end
