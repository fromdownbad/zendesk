module Zendesk::Entries
  class Suspension
    attr_reader :error, :suspended_entries, :whitelisted_entries,
      :suspended_posts, :whitelisted_posts

    def initialize(account, options = {})
      @account = account

      @suspended_entries = Array.wrap(options.fetch(:suspended_entries, [])).map(&:to_i)
      @suspended_posts = Array.wrap(options.fetch(:suspended_posts, [])).map(&:to_i)

      @whitelisted_entries = Array.wrap(options.fetch(:whitelisted_entries, [])).map(&:to_i)
      @whitelisted_posts = Array.wrap(options.fetch(:whitelisted_posts, [])).map(&:to_i)

      @error = nil
    end

    def run
      # Just a short-circuit, would be caught below too
      if (suspended_entries & whitelisted_entries).any? || (suspended_posts & whitelisted_posts).any?
        @error = I18n.t("txt.moderation.suspension.duplicate_user")
        return false
      end

      entries = Entry.with_deleted do
        Entry.
          includes(:submitter).
          where(
            account_id: @account.id,
            id: suspended_entries + whitelisted_entries
          ).
          to_a
      end

      posts = Post.with_deleted do
        Post.
          includes(:user).
          where(
            account_id: @account.id,
            id: suspended_posts + whitelisted_posts
          ).
          to_a
      end

      users_to_suspend = []
      users_to_whitelist = []

      entries.each do |entry|
        if suspended_entries.include?(entry.id)
          users_to_suspend << entry.submitter
        end

        if whitelisted_entries.include?(entry.id)
          users_to_whitelist << entry.submitter
        end
      end

      posts.each do |post|
        if suspended_posts.include?(post.id)
          users_to_suspend << post.user
        end

        if whitelisted_posts.include?(post.id)
          users_to_whitelist << post.user
        end
      end

      users_to_suspend.uniq!
      users_to_whitelist.uniq!

      if (users_to_suspend & users_to_whitelist).any?
        @error = I18n.t("txt.moderation.suspension.duplicate_user")
        return false
      end

      users_to_suspend.each do |user|
        user.suspended = true
        user.save!

        # Backtrack and suspend all other entries
        Entry.with_deleted do
          Entry.
            where(
              "account_id = ? AND submitter_id = ? AND (deleted_at IS NULL OR flag_type_id = ?)", @account.id, user.id, EntryFlagType::SUSPENDED.id
            ).
            find_each(&:soft_delete!)

          Post.with_deleted do
            Post.
              where(
                "account_id = ? AND (user_id = ? OR entry_id IN (?)) AND (deleted_at IS NULL OR flag_type_id = ?)", @account.id, user.id, entries.map(&:id), PostFlagType::SUSPENDED.id
              ).
              find_each(&:soft_delete!)
          end
        end
      end

      users_to_whitelist.each do |user|
        user.whitelisted_from_moderation = true
        user.save!

        # Can't call user.entries, deleted are not returned
        # Find all suspended entries, unsuspend
        Entry.with_deleted do
          Entry.
            where(
              account_id: @account.id,
              submitter_id: user.id,
              flag_type_id: EntryFlagType::SUSPENDED.id
            ).
            find_each(&:unsuspend!)

          Post.with_deleted do
            Post.
              where(
                account_id: @account.id,
                user_id: user.id,
                flag_type_id: PostFlagType::SUSPENDED.id
              ).
              find_each(&:unsuspend!)
          end
        end
      end

      true
    rescue ActiveRecord::RecordInvalid => e
      Rails.logger.append_attributes(moderation: { backtrace: e.backtrace })
      Rails.logger.info("[MODERATION] Unknown suspension error: \"#{e.message}\"")

      @error = I18n.t("txt.moderation.suspension.unknown_error")
      false
    end

    def success?
      error.nil?
    end
  end
end
