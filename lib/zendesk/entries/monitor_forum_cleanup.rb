module Zendesk::Entries
  class MonitorForumCleanup
    attr_accessor :whitelisted, :suspended, :entries_removed, :errors

    def initialize(params)
      @current_account = params["account"]
      @params = params
      self.whitelisted = 0
      self.suspended = 0
      self.entries_removed = 0
      self.errors = {}
    end

    def run
      if !target_forum
        self.errors = { message: "The forum you requested does not exist.", status: :not_found }
      elsif entries.nil?
        self.errors = { message: "There were no entries found in the forum you specified for the date range given.", status: :bad_request}
      else
        moderate_entries
      end
    end

    private

    def moderate_entries
      Rails.logger.warn("ModerationForumsInternal_Request: #{Time.now.utc} | AccountID: #{target_forum.account.id} | Entries to remove: #{entries.size}")
      entries.each do |entry|
        if entry.submitter.is_agent?
          Rails.logger.warn("Skipping #{entry.id} because it was authored by an agent/admin #{entry.submitter.id}")
          self.whitelisted += 1
          next
        elsif entry.soft_delete
          self.entries_removed += 1
        else
          errors[:message] ||= "Error: "
          errors[:message] += "Failed to soft-delete #{entry.id}. "
          Rails.logger.error("ModerationForumsInternal_Request: Entry #{entry.id} could not be soft_deleted. #{Time.now.utc}")
        end
        suspend_user(entry.submitter) if @params["suspend_users"]
      end
    end

    def suspend_user(user)
      return if user.suspended?
      if user.suspend
        self.suspended += 1
        Rails.logger.warn("ModerationForumsInternal_Request: User #{user.id}, is #{user.is_agent? ? "an" : "not an"} agent and  was #{user.suspended? ? "suspended" : "whitelisted"}.")
      else
        Rails.logger.error("ModerationForumsInternal_Request: User #{user.id} was not suspended and errors occurred. #{user.errors.inspect}. #{Time.now.utc}.")
        errors[:message] += "User #{user.id} could not be suspended: #{user.errors.inspect}"
      end
    end

    def target_forum
      @target_forum ||= @current_account.forums.where(id: @params["moderate_forum"]).first
    end

    def start_date
      @start_date ||= Time.at(@params["start_date"].to_i)
    end

    def end_date
      @end_date ||= Time.at(@params["end_date"].to_i)
    end

    def entries
      @entries ||= target_forum.entries.where(["created_at >= ? and created_at <= ? and deleted_at is null", start_date.utc, end_date.utc])
    end
  end
end
