module Zendesk::HtmlEmail
  class Button
    class << self
      # Renders two buttons: A normal <a> and a fallback <v> graphic link for microsoft outlook clients
      def render(href: '', stroke: '#cccccc', fill: '#78a300', color: '#555555', text: '', width: '150px', style: '')
        <<-HTML
          <!--[if mso]>
          <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#{href}" style="font-family:'Lucida Grande',Verdana,Arial,sans-serif;height:40px;v-text-anchor:middle;width:#{width};#{style}" arcsize="4%" strokecolor="#{stroke}" fillcolor="#{fill}">
            <w:anchorlock/>
            <center style="color:#{color};font-family:sans-serif;font-size:12px;">#{text}</center>
          </v:roundrect>
          <![endif]-->
          <a href="#{href}" style="background-color:#{fill};border:1px solid #{stroke};border-radius:4px;color:#{color};display:inline-block;font-family:sans-serif;font-size:12px;line-height:40px;text-align:center;text-decoration:none;width:#{width};-webkit-text-size-adjust:none;mso-hide:all;#{style}">
            <span style="color:#{color};text-decoration:none;">#{text}</span>
          </a>
        HTML
      end
    end
  end
end
