module Zendesk::HtmlEmail
  class RelativeToAbsoluteUrlConverter
    class << self
      TAGS = {
        'a' => 'href',
        'img' => 'src'
      }.freeze

      def convert(content, host)
        dom = Nokogiri::HTML::DocumentFragment.parse(content)
        dom.search(TAGS.keys.join(',')).each do |node|
          object = TAGS[node.name]
          src = node[object]
          next unless src
          uri = URI.parse(src)
          next unless uri.relative?
          uri.scheme = 'https'
          uri.host = host
          node[object] = uri.to_s
        end
        dom.to_html(save_with: 0)
      rescue => e
        Rails.logger.warn("Unable to parse html when converting relative links - #{e.exception.inspect}")
        content
      end
    end
  end
end
