require 'zendesk/extensions/ar_skipped_callback_metrics/base_methods'

module ActiveRecordStats
  class StatsSubscriber < ActiveSupport::Subscriber
    TAG = 'ar_skip_callback'.freeze
    INSTRUMENTED_METHODS = \
      ArSkippedCallbackMetrics::InstanceMethods::METHODS |
      ArSkippedCallbackMetrics::ClassMethods::METHODS

    AR_INTERNAL_CALL_REGEX = /activerecord.+(#{INSTRUMENTED_METHODS.join('|')})/.freeze

    def skipped_callback(event)
      payload = event.payload
      report(payload[:klass], payload[:method], payload[:caller])
    rescue StandardError
      # Instrumentation should never fail the original call. Continue and log
      Rails.logger.warn("[#{TAG}] ActiveRecordStats instrumentation failed")
    end

    private

    def report(klass, method_name, call_stack)
      return if internal_activerecord_call(call_stack)

      send_statsd_metric(klass, method_name)
      log("#{klass}##{method_name}", call_stack)
      append_tag("#{klass}##{method_name}")
    end

    # We only care about calls the app makes directly
    # Skip anything coming from inside activerecord
    def internal_activerecord_call(call_stack)
      call_stack.any? { |line| line.match(AR_INTERNAL_CALL_REGEX) }
    end

    def send_statsd_metric(klass, method_name)
      statsd.increment(
        TAG,
        tags: %W[class:#{klass} method:#{method_name}]
      )
    end

    def log(message, call_stack)
      Rails.logger.info <<~TEXT
        [#{TAG}] ActiveRecord callbacks skipped: #{message}
        #{find_call(call_stack)}
      TEXT
    end

    def append_tag(value)
      return unless span

      tag = "zendesk.#{TAG}"
      tag_contents = span.get_tag(tag)
      span.set_tag(tag, ([tag_contents] << value).compact.join(", "))
    end

    # Find the line that made the call to activerecord. This is the first place in the stack
    # from inside the app, aside from the module used to instrument the method
    def find_call(call_stack)
      call_stack.detect { |line| line.match(/\/app/) && !line.match(/ar_skipped/) }
    end

    def statsd
      Zendesk::StatsD.client(namespace: 'classic')
    end

    def span
      Datadog.tracer.active_span
    end
  end
end
