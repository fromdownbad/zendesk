require 'zendesk/stats/active_record_stats/stats_subscriber'

module ActiveRecordStats
  class << self
    attr_reader :subscriber

    def subscribe
      @subscriber ||= ActiveSupport::Notifications.subscribe(
        'skipped_callback.ar_metrics',
        ActiveRecordStats::StatsSubscriber.new
      )
    end

    def unsubscribe
      ActiveSupport::Notifications.unsubscribe(@subscriber)
      @subscriber = nil
    end
  end
end
