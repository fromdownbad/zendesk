require "zendesk_statsd"

module Zendesk
  module StatRollup
    class RollupJob < Zendesk::Jobs::Resque
      not_sharded

      self.queue = :stats

      def statsd_job_name
        @statsd_job_name ||= zendesk_params[:args][1]
      end

      def work
        do_work(zendesk_params[:args][0], zendesk_params[:args][1], zendesk_params[:args][2])
      end

      def do_work(fact_class_name, job_name, shard_id)
        running_time = Benchmark.realtime do
          Zendesk::Stats.logger = logger
          Zendesk::Stats.logger.level = Logger::INFO
          job = Zendesk::Stats::MapReduce::JobRegister.lookup_job(fact_class_name, job_name)
          fact_class = job.fact_klass
          raise "invalid job name or fact name" if job.nil? || fact_class.nil?
          ActiveRecord::Base.on_shard(shard_id) do
            account_ids = Account.shard_local.active.shard_unlocked.map(&:id)
            fact_class.map_reduce_job(job, shard_id, account_ids)
          end
        end

        log_stats(running_time, fact_class_name, job_name, shard_id)
      end

      def log_stats(running_time, fact_class_name, job_name, shard_id)
        statsd = Zendesk::StatsD::Client.new(namespace: ["jobs", "RollupJob"])
        statsd.histogram("execution_time", running_time, tags: ["mrjob:#{fact_class_name}-#{job_name}", "shard:#{shard_id}"])
      rescue
        resque_error("Could not log stats to datadog")
      end

      def args_to_log(fact_class_name, job_name, shard_id)
        { klass: fact_class_name, job: job_name, shard_id: shard_id }
      end
    end
  end
end
