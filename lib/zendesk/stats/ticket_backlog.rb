module Zendesk
  module Stats
    class TicketBacklog
      class << self
        def backlog_status
          [StatusType.NEW, StatusType.OPEN, StatusType.PENDING, StatusType.HOLD]
        end

        # API to do everything
        def rollup(account_id, duration, time_stamp)
          rollup_by_account(account_id, duration, time_stamp)
        end

        # Save ticket backlog by account
        def rollup_by_account(account_id, duration, time_stamp)
          ActiveRecord::Base.on_slave do
            ticket_count(account_id, :account_id).each do |a_id, count|
              ActiveRecord::Base.on_master do
                ::StatRollup::Ticket.create!(
                  account_id: a_id, duration: duration, ts: time_stamp,
                  agent_id: 0, organization_id: 0, group_id: 0,
                  type: "backlog", value: count, num: nil
                )
              end
            end
          end
        end

        # Save ticket backlog by assignee and account
        def rollup_by_assignee(account_id, duration, time_stamp)
          ActiveRecord::Base.on_slave do
            ticket_count(account_id, [:account_id, :assignee_id]).each do |pair, count|
              account_id, assignee_id = pair
              next if assignee_id.to_i == 0

              ActiveRecord::Base.on_master do
                ::StatRollup::Ticket.create!(
                  account_id: account_id, duration: duration, ts: time_stamp,
                  agent_id: assignee_id, organization_id: 0, group_id: 0,
                  type: "backlog", value: count, num: nil
                )
              end
            end
          end
        end

        # Save ticket backlog by organization and account
        def rollup_by_organization(account_id, duration, time_stamp)
          ActiveRecord::Base.on_slave do
            ticket_count(account_id, [:account_id, :organization_id]).each do |pair, count|
              account_id, organization_id = pair
              next if organization_id.to_i == 0

              ActiveRecord::Base.on_master do
                ::StatRollup::Ticket.create!(
                  account_id: account_id, duration: duration, ts: time_stamp,
                  agent_id: 0, organization_id: organization_id, group_id: 0,
                  type: "backlog", value: count, num: nil
                )
              end
            end
          end
        end

        # Save ticket backlog by priority and account
        def rollup_by_priority(account_id, duration, time_stamp)
          ActiveRecord::Base.on_slave do
            ticket_count(account_id, [:account_id, :priority_id]).each do |pair, count|
              account_id, priority_id = pair
              next if priority_id.nil?
              type = "backlog_by_priority_#{priority_id}"

              ActiveRecord::Base.on_master do
                ::StatRollup::Ticket.create!(
                  account_id: account_id, duration: duration, ts: time_stamp,
                  agent_id: 0, organization_id: 0, group_id: 0,
                  type: type, value: count, num: nil
                )
              end
            end
          end
        end

        private

        def ticket_count(account_id, group)
          Ticket.where(status_id: backlog_status, account_id: account_id).group(group).count(:all)
        end
      end
    end
  end
end
