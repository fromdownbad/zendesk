class StatRollup::HelpCenterStat
  belongs_to :account

  scope :by_account, -> { where(child_id: 0, parent_id: 0, grandparent_id: 0, help_center_id: 0).group(:account_id) }
end

class StatRollup::HCSearchAccountStat
  belongs_to :account
end

class StatRollup::HCSearchStringStat
  belongs_to :account

  scope :default, -> { group(:string).order('searches desc').where(origin: 'all') }
end
