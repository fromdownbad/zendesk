class StatRollup::Ticket
  belongs_to :account
  belongs_to :user, foreign_key: :agent_id
  belongs_to :group

  scope :by_agent,   ->(*_args) { where("agent_id > 0 and group_id = 0 and organization_id = 0").group(:agent_id) }
  scope :by_account, ->(*_args) { where("group_id = 0 and organization_id = 0 and agent_id = 0") }
  scope :by_group,   ->(*_args) { where("group_id > 0 and organization_id = 0 and agent_id = 0").group(:group_id) }

  scope :by_members, ->(group) {
    uids = group.memberships.map { |m| m.user_id.to_i }
    where(group_id: 0, organization_id: 0, agent_id: uids).group(:agent_id)
  }

  class << self
    def lpf_csr(options)
      # where do we set the low-pass filter (in relation to the most-reviewed agent)
      fraction_thresh = options[:threshold] ? options[:threshold].to_f : 0.3

      time_options = TimeOptions.new(self, options)
      conditions = options[:conditions] || {}
      conditions[:type] = 'csr'

      grouping = extract_query_option(:group, true, options)

      row = select('sum(num) as num').where(conditions).where(time_options.to_mysql_conditions).group(grouping).order('num desc').first
      return {data: [], csr_threshold: 0} unless row

      most_rated_count = row.num.to_i

      threshold = (most_rated_count.to_f * fraction_thresh).to_i
      res = { data: top_n(options.merge(having: "sum(num) > #{threshold}", order: "value desc")).to_a }
      res.merge(csr_threshold: threshold)
    end
  end
end
