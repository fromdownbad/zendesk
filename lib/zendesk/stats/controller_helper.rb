module Zendesk
  module Stats
    module ControllerHelper
      private

      def store_last_search_in_session(query, origin)
        session[:last_search] = {query: query, time: Time.now.to_i, clicked: false, origin: origin}
      end

      def get_last_search_from_session(time_limit = 1.minute)
        if session[:last_search] && session[:last_search][:time] > time_limit.ago.to_i
          session[:last_search]
        end
      end

      def store_search_click(options)
        options[:type] = 'click'
        last_search = session[:last_search]

        return unless last_search

        # unique the click for the search
        return if last_search[:clicked]

        return if last_search[:time] < 1.minute.ago.to_i

        session[:last_search][:clicked] = true

        options[:user_id] ||= current_user.id
        options[:string] = last_search[:query]
        options[:origin] = last_search[:origin]

        store_search_string(options)
      end

      def store_search_stat(query, options)
        return if params[:page] && params[:page].to_s.to_i != 1
        options[:type] ||= 'search'
        # to sym is there because format.to_sym is :text/html
        options[:format] ||= request.format.try(:to_sym).to_s
        options[:string] = query
        options[:user_id] ||= current_user.id
        options[:origin] = 'agent' if current_user.is_agent?
        options[:origin] = 'api' if options[:origin] != 'dropbox' && params[:format] && params[:format] != 'html'

        if options[:results] == 0 && flash[:errors]
          options.delete(:results)
        end

        # prevent people from jamming the reload button
        if session[:last_search] && session[:last_search][:query] == query &&
            options[:type] == 'search' && (Time.now.to_i - session[:last_search][:time] < 60)
          session[:last_search][:time] = Time.now.to_i
          return
        end

        if options[:type] == 'search' && !['agent', 'api'].include?(options[:origin])
          store_last_search_in_session(query, options[:origin])
        end

        store_search_string(options)
      end

      def store_search_ticket_stat(hash)
        if params[:ticket_from_search].present?
          hash[:type] = 'ticket'
          store_search_stat(params[:ticket_from_search], hash)
        end
      end
    end
  end
end
