module Zendesk
  module Stats
    module ConsulToStatsYmlAdapter
      class MissingMongoConfiguration < StandardError; end

      MONGO_SERVICE_NAME = 'mongodb'.freeze
      MONGO_SERVICE_TAGS = [].freeze
      REPLICASET_TAG_PATTERN = /replicaset-(\d+-\d+)/.freeze
      SHARD_TAG_PATTERN = /shard-(\d+)/.freeze
      JSON_OUTPUT_PATH = '/var/spool/flume'.freeze

      class << self
        def config
          @config ||= {
            'mongo' => {
              'replicasets' => replicasets,
              'shards' => shards
            },
            'redis' => {
              'host' => ENV.fetch('ZENDESK_STATS_REDIS_HOST')
            },
            'json_output' => {
              'path' => JSON_OUTPUT_PATH
            }
          }
        end

        private

        def replicasets
          @replicasets ||= begin
            replicasets_default = Hash.new { |hash, key| hash[key] = { 'hosts' => [] } }

            mongo_servers.each_with_object(replicasets_default) do |server, config|
              config[replicaset_id(server)]['hosts'] << server.values_at(:host, :port)
            end
          end
        end

        def shards
          @shards ||= mongo_servers.each_with_object({}) do |server, config|
            server_shards(server).each do |shard|
              config[shard] = {
                'replicaset' => replicaset_id(server),
                'stats_db' => stats_db(server, shard)
              }
            end
          end
        end

        def mongo_servers
          @mongo_servers ||= Zendesk::Configuration.servers(
            MONGO_SERVICE_NAME,
            MONGO_SERVICE_TAGS.dup,
            include_dead: true,
            global: false,
            pod_local: true,
            full: true
          )
        end

        def replicaset_id(server)
          server.
            fetch(:tags).
            find(missing_replicaset_tag(server)) { |tag| tag.match?(REPLICASET_TAG_PATTERN) }.
            match(REPLICASET_TAG_PATTERN)[1]
        end

        def server_shards(server)
          server.
            fetch(:tags).
            find_all { |tag| tag.match?(SHARD_TAG_PATTERN) }.
            map { |shard_tag| shard_tag[SHARD_TAG_PATTERN, 1].to_i }
        end

        def stats_db(server, shard)
          db_tag_pattern = stats_db_tag_pattern(shard)

          server.
            fetch(:tags).
            find(missing_db_tag(shard, db_tag_pattern)) { |tag| tag.match?(db_tag_pattern) }.
            match(db_tag_pattern)[1]
        end

        def stats_db_tag_pattern(shard)
          /statsdb-([a-z_-]+#{shard})/
        end

        def missing_replicaset_tag(server)
          -> {
            raise(
              MissingMongoConfiguration,
              "Unable to find mongo replicaset for server: #{server[:host]}. Looking for tag: #{REPLICASET_TAG_PATTERN}"
            )
          }
        end

        def missing_db_tag(shard, regex)
          -> {
            raise(
              MissingMongoConfiguration,
              "Unable to find stats database for shard: #{shard} ... looking for tag: #{regex}"
            )
          }
        end
      end
    end
  end
end
