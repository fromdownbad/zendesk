module Zendesk
  module Incidents
    class Finder
      attr_reader :account, :current_user, :params

      def initialize(account, current_user, params)
        @account = account
        @current_user = current_user
        @params = params
      end

      def incidents
        account.tickets.find_by_nice_id!(problem_nice_id).incidents.for_user(current_user)
      end

      private

      def problem_nice_id
        params[:problem_nice_id]
      end
    end
  end
end
