module Zendesk
  class AuthenticatedSession
    PASSWORD_CHECK_INTERVAL = 1.hour
    DEFAULT_SESSION_DURATION = 8.hours
    TWO_WEEK_DURATION = 60 * 24 * 14

    class SessionNamespace
      attr_reader :session, :namespace
      delegate :invalidate_id!, :destroy, :clear, to: :session

      def initialize(session, namespace)
        @session   = session
        @namespace = namespace
      end

      def [](key)
        session[namespaced(key)]
      end

      def []=(key, value)
        session[namespaced(key)] = value
      end

      def delete(key)
        session.delete(namespaced(key))
      end

      protected

      def namespaced(key)
        "#{namespace}_#{key}"
      end
    end

    def self.create(params = {})
      auth = Zendesk::AuthenticatedSession.new(params.fetch(:session))
      auth.current_account = params.fetch(:account)
      auth.current_user    = params.fetch(:user)
      auth.via             = params[:via]
      auth.created_at      = Time.now
      invalidate_id        = params.fetch(:invalidate_id, true)

      if params[:store]
        if invalidate_id?(auth.current_account, auth.is_assuming_user?, invalidate_id)
          auth.invalidate_id!
        end

        auth.duration = if params[:remember_me_requested] && !Arturo.feature_enabled_for?(:remove_stay_signed_in, auth.current_account)
          TWO_WEEK_DURATION * 60 # constant is in minutes and we need seconds
        else
          if auth.current_user.is_agent?
            auth.current_account.settings.session_timeout.try(:minutes).to_i
          else
            DEFAULT_SESSION_DURATION.to_i
          end
        end

        auth.touch
        auth.store!
      end

      auth
    end

    def self.invalidate_id?(_account, is_assuming_user, invalidate_id)
      !is_assuming_user && invalidate_id
    end

    attr_accessor :current_user
    attr_reader :current_account

    def initialize(session)
      @session = SessionNamespace.new(session, :auth)
    end

    def current_account=(account)
      @current_account = account
      session.session[:account] = account.id if account
    end

    def store!
      session[:stored] = true
    end

    def stored?
      session[:stored] == true
    end

    def keepalive
      # For users that have checked Remember Me, we have to do this
      # in order to update the last_login attribute on the user and account
      current_user.update_last_login if current_user && current_user.last_login.to_i <= 1.day.ago.to_i && !is_assuming_user_from_monitor? && !is_assuming_user? && session[:via] != 'master_token'

      touch if updated_at < period_for_touching.minutes.ago.to_i
    end

    def touch
      session[:updated_at] = Time.now.to_i
    end

    def via=(strategy_name)
      session[:via] = strategy_name
    end

    def via
      session[:via]
    end

    def created_at
      session[:authenticated_at]
    end

    def created_at=(time)
      session[:authenticated_at] = time.to_i
    end

    def lotus_first_boot_at=(time)
      session[:lotus_first_boot_at] = time.to_i
    end

    def lotus_first_boot_at
      session[:lotus_first_boot_at]
    end

    def duration=(duration)
      session[:duration] = duration.to_i
    end

    def destroy
      session.destroy
    end

    def invalidate_id!
      session.invalidate_id!
    end

    def stale?
      timeout < Time.now
    end

    def updated_at
      session[:updated_at]
    end

    def timeout
      Time.at(updated_at + duration)
    end

    def password_expired?
      return false if session[:via] == 'master_token'

      if session[:via] == 'password'
        update_password_expiration
        session[:password_expired] == true
      else
        false
      end
    end

    def is_assuming_user? # rubocop:disable Naming/PredicateName
      session[:original_user_id].present?
    end

    def is_assuming_user_from_monitor? # rubocop:disable Naming/PredicateName
      session[:assuming_monitor_user_id].present?
    end

    def assuming_monitor_user_id
      session[:assuming_monitor_user_id]
    end

    def original_user
      if is_assuming_user?
        @original_user ||= current_account.users.find_by_id(session[:original_user_id])
      end
    end

    def original_user=(user)
      if user
        session[:original_user_id] = user.id
      else
        session.delete(:original_user_id)
      end
      @original_user = user
    end

    def password_changed_for_user!(user, shared_session_record_id: nil)
      invalidate_other_sessions!(user, shared_session_record_id: shared_session_record_id)
      session.delete(:password_expired) if current_user == user
    end

    def invalidate_other_sessions!(user, shared_session_record_id: nil)
      if shared_session_record_id
        roll_session_id!(current_user.shared_sessions.find_by(id: shared_session_record_id))
        current_user.shared_sessions.where("shared_sessions.id != ?", shared_session_record_id).delete_all
      elsif !original_user && current_user == user && session && session.session[:id]
        roll_session_id!(current_user.shared_sessions.find_by(session_id: session.session.id))
        current_user.shared_sessions.where("shared_sessions.session_id != ?", session.session.id).delete_all
      else
        user.shared_sessions.delete_all
      end
    end

    protected

    attr_reader :session

    def update_password_expiration
      last_check = session[:password_expired_checked] || 0

      if last_check < PASSWORD_CHECK_INTERVAL.ago.to_i
        session[:password_expired_checked] = Time.now.to_i
        if current_user.password_expired?
          session[:password_expired] = true
        end
      end
    end

    # set by warden on login
    def duration
      session[:duration]
    end

    def period_for_touching
      duration <= 15.minutes ? 1 : 5
    end

    def roll_session_id!(session)
      return unless session && current_account.has_regenerate_session_id_after_self_password_change?

      session.roll_session_id!
    end
  end
end
