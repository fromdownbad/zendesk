module Zendesk
  module ScopeExtensions
    def self.included(klass)
      # Below, thanks to fake_arel, modified + hacked for Rails 3
      # https://github.com/gammons/fake_arel/blob/master/lib/fake_arel/rails_3_finders.rb
      # http://stackoverflow.com/questions/3548548/rails3-combine-scope-with-or
      klass.class_eval do
        def self.and_disjunction_of(*scopes)
          if RAILS4
            raise "this does nothing" if scopes.empty?
            if scopes.size == 1
              warn("Calling #and_disjunction_of with only 1 scope from #{caller.first}") # TODO: use Deprecation.warn
              return scopes.first
            end

            where = scopes.map(&:condition_sql)
            combined_scope = self
            combined_scope = combined_scope.where where.join(" OR ") unless where.empty?
            combined_scope
          else
            ActiveSupport::Deprecation.warn("Class-level `and_disjunction_of` (aka `or`) is fundamentally incompatible with rails 5. Please replace this with instance-level `or`.")
            if scopes.empty?
              self
            else
              scopes.reduce { |a, b| a.or b }
            end
          end
        end
      end
    end
  end
end
