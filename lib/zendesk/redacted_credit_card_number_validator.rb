class Zendesk::RedactedCreditCardNumberValidator
  MIN_LENGTH = 13
  MAX_LENGTH = 19
  REDACTED_REGXP = /\AX+\d{4}\Z/ # http://rubular.com/r/ezYr7eVFVU
  INVALID_REGEXP = /^((\d)|-)+$/ # http://rubular.com/r/EXeYf6lM82

  def self.too_short?(number)
    number.to_s.length < MIN_LENGTH
  end

  def self.too_long?(number)
    number.to_s.length > MAX_LENGTH
  end

  def self.redacted?(number)
    !!(number =~ REDACTED_REGXP)
  end

  def self.invalid_format?(number)
    !(number =~ INVALID_REGEXP)
  end

  def self.valid?(number)
    !too_short?(number) && !too_long?(number) && redacted?(number)
  end
end
