module Zendesk::HelpCenter
  class InternalApiClient
    NUM_ARTICLES_TO_DISPLAY = 3
    PAGE_SIZE = 75
    ARTICLES_QUERY_ENDPOINT = "/hc/api/v2/articles.json?ids=".freeze
    ARTICLES_SEARCH_ENDPOINT = "/hc/api/v2/articles/search.json".freeze
    ANSWER_BOT_SETTING_ENDPOINT = "/hc/api/internal/answer_bot/settings.json".freeze
    ANSWER_BOT_FORM_SETTING_ENDPOINT = '/hc/api/internal/answer_bot/brand_settings/%{brand_id}/ticket_forms/%{form_id}.json'.freeze
    RESCUABLE_ERRORS = [
      Kragle::BadGateway,
      Kragle::InvalidContentType,
      Faraday::ConnectionFailed,
      Faraday::Error::TimeoutError,
      Faraday::TimeoutError,
      Kragle::InternalServerError,
      Kragle::ResourceNotFound,
      Kragle::Forbidden,
      KeyError
    ].freeze

    def initialize(account:, requester: nil, brand: nil)
      @account = account
      @requester = requester
      @brand = brand
    end

    def search_articles(query, options)
      # Search in all the locales, and in all the brands
      url = "#{ARTICLES_SEARCH_ENDPOINT}?query=#{CGI.escape(query)}&locale=*&multibrand=true&exclude=body&"

      url << options.join("&") # add all query params

      resp = connection_as_user.get(url)
      resp.body
    rescue Kragle::ResourceNotFound # no help center for account
      {}
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: "Could not search HC articles for query: #{query}", fingerprint: '26e389c4648e4da89aa3132a3cefdd76')
      {}
    end

    def fetch_article(article_id, locale)
      locale = default_locale if locale.nil?
      fetch_articles_for_ids_and_locales([{'article_id' => article_id, 'locale' => locale}]).first
    end

    def fetch_articles_for_ids_and_locales(article_ids_and_locales)
      return [] if article_ids_and_locales.blank?

      articles_group_by_locale = article_ids_and_locales.group_by { |a| a['locale'] }

      fetched_articles = []
      articles_group_by_locale.each do |locale, article_objects|
        locale = default_locale if locale.nil?
        fetched_articles << get_articles(article_objects.map { |a| a['article_id'] }, locale)
      end

      article_ids = article_ids_and_locales.map { |a| a['article_id'] }
      article_data(filter_articles(fetched_articles.flatten, article_ids))
    rescue Kragle::ClientError => e
      log_client_error(e.class)
      []
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: "Could not fetch articles with ids and locales #{article_ids_and_locales}", fingerprint: '432b394ba5950d5f4c343df99256da1edd08702e')
      []
    end

    def fetch_article_ids_for_labels(labels)
      articles = []
      search_url = "#{ARTICLES_SEARCH_ENDPOINT}?label_names=#{labels.join(',')}&per_page=#{PAGE_SIZE}"
      response = connection_as_ticket_requester.get(search_url).body
      articles.push(*article_ids(response.fetch('results')))

      until response.fetch('next_page').blank?
        next_page = response.fetch('page') + 1
        response = connection_as_ticket_requester.get(search_url + "&page=#{next_page}").body
        articles.push(*article_ids(response.fetch('results')))
      end

      articles
    rescue Kragle::ClientError => e
      log_client_error(e.class)
      []
    end

    def answer_bot_setting_enabled?
      response = connection.get(ANSWER_BOT_SETTING_ENDPOINT)

      response.body.fetch("enabled")
    rescue *RESCUABLE_ERRORS => e
      log_client_error(e.class)
      false
    end

    def answer_bot_form_setting_enabled?(brand_id:, form_id:)
      response = connection.get(
        answer_bot_form_setting_endpoint(brand_id, form_id)
      )

      response.body.fetch('enabled')
    rescue *RESCUABLE_ERRORS => e
      log_client_error(e.class)
      false
    end

    private

    def get_articles(article_ids, locale)
      connection_as_ticket_requester.get("#{ARTICLES_QUERY_ENDPOINT}#{article_ids.join(',')}&locale=#{locale}").body["articles"]
    end

    def default_locale
      @locale ||= begin
                    connection_as_ticket_requester.get('/hc/api/v2/locales.json').body.fetch('default_locale')
                  rescue => e
                    log_locale_fetch_error(e.class)
                    "en-us" # send a default locale
                  end
    end

    def filter_articles(articles, article_ids)
      filtered_articles = []
      article_ids.each do |article_id|
        filtered_articles << articles.find { |article| article_id.to_i == article.fetch("id") }
        break if filtered_articles.compact.size >= NUM_ARTICLES_TO_DISPLAY
      end
      filtered_articles.compact
    end

    def article_ids(articles)
      articles.map { |article| article.fetch('id') }
    end

    def article_data(articles)
      articles.map do |article|
        {
          id: article.fetch('id'),
          title: article.fetch("title"),
          url: article.fetch("url"),
          html_url: article.fetch("html_url"),
          body: article.fetch("body")
        }
      end
    end

    def connection_as_ticket_requester
      @connection_as_ticket_requester ||=
        begin
          connection = Kragle.new('hc', shared_cache: false, signing: true, retry_options: { max: 3 }) do |faraday|
            faraday.headers["X-Zendesk-Internal-User"] = @requester.id.to_s
            faraday.use(FaradayMiddleware::FollowRedirects)
          end
          connection.url_prefix = @brand.url(mapped: false, ssl: true)
          connection
        end
    end

    def connection_as_user
      @connection_as_user ||=
        begin
          connection = KragleConnection.build_for_help_center(@account)
          connection.headers["X-Zendesk-Internal-User"] = @requester.id.to_s
          connection.use(FaradayMiddleware::FollowRedirects)
          connection
        end
    end

    def connection
      @connection ||=
        KragleConnection.build_for_account(@account, 'help_center')
    end

    def answer_bot_form_setting_endpoint(brand_id, form_id)
      ANSWER_BOT_FORM_SETTING_ENDPOINT % { brand_id: brand_id, form_id: form_id }
    end

    def clean_class_name(class_name)
      class_name.to_s.split("::").last.downcase
    end

    def log_client_error(error_class)
      Rails.logger.warn("ticket_deflection.help_center.client_error -- #{error_class} -- account: #{@account.subdomain}")
      statsd_client.increment("client_error", tags: ["error:#{clean_class_name(error_class)}", "subdomain:#{@account.subdomain}"])
    end

    def log_locale_fetch_error(error_class)
      Rails.logger.warn("ticket_deflection.help_center.locale_fetch_error -- #{error_class} -- account: #{@account.subdomain}")
      statsd_client.increment("locale_fetch_error", tags: ["error:#{clean_class_name(error_class)}", "subdomain:#{@account.subdomain}"])
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['ticket_deflection', 'help_center'])
    end
  end
end
