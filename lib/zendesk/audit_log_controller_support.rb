module Zendesk
  module AuditLogControllerSupport
    class InvalidCreatedAtFilterError < StandardError; end

    RULE_SUBTYPES = ['View', 'Trigger', 'Macro'].freeze

    private

    include ZendeskApi::Controller::Pagination

    def paginated_cia_events
      params[:sort_order] ||= "desc"

      if params[:filter]
        rule_subtype = rule_subtype(params[:filter])
      end

      # As the cia_event don't know the type of rule we have, we need in some cases
      # a join with rules to find the correct events.
      events = if rule_subtype
        params[:filter][:source_type] = 'Rule'
        base_scope.
          joins("INNER JOIN rules ON rules.id = cia_events.source_id").
          where("type = ?", rule_subtype)
      else
        base_scope
      end

      CIA::Event.assign_exact_source_type(events)
      events
    end

    def cia_events_scope
      CIA::Event.for_account(current_account).visible
    end

    def base_scope
      cia_events_scope.
        where(Zendesk::AuditLogControllerSupport.conditions(params[:filter])).
        includes(:attribute_changes, :actor).
        order(sorting).
        paginate(pagination)
    end

    def rule_subtype(options)
      return unless source_type = options[:source_type]
      source_type = source_type.classify
      source_type if RULE_SUBTYPES.include?(source_type)
    end

    public_class_method def self.conditions(options)
      conditions = options.to_h.with_indifferent_access
      parse_created_at(conditions)
      conditions.slice!(:source_type, :source_id, :actor_type, :actor_id, :ip_address, :created_at)
      conditions[:source_type] = conditions[:source_type].classify if conditions[:source_type]
      conditions
    end

    private_class_method def self.parse_created_at(conditions)
      if created_at = conditions[:created_at]
        if created_at.is_a?(Array) && created_at.length == 2
          conditions[:created_at] = Time.parse(created_at[0])..Time.parse(created_at[1])
        else
          raise Zendesk::AuditLogControllerSupport::InvalidCreatedAtFilterError
        end
      end
    rescue ArgumentError
      raise Zendesk::AuditLogControllerSupport::InvalidCreatedAtFilterError
    end
  end
end
