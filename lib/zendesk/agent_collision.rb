require 'zendesk/radar_factory'

module Zendesk
  class AgentCollision
    class << self
      def ticket_viewed_by_user_ids(ticket)
        radar_client(ticket.account).presence("ticket/#{ticket.nice_id}").get.keys.map(&:to_i)
      end

      private

      def radar_client(account)
        @radar_clients ||= {}
        @radar_clients[account.subdomain] ||= ::RadarFactory.create_radar_client(account)
      end
    end
  end
end
