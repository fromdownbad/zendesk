# frozen_string_literal: true

module Zendesk
  module EntityPublication
    class DatabaseBackoffError < StandardError
      attr_reader :db_cpu_util

      def initialize(db_cpu_util)
        @db_cpu_util = db_cpu_util
      end
    end
  end
end
