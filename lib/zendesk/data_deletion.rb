module Zendesk
  module DataDeletion
    class PermanentError < StandardError; end
    class InvalidAudit < PermanentError; end
  end
end
