module Zendesk
  module Tickets
    module SetCollaboratorsV2
      include CollaborationSupport

      def set_followers=(followers_params) # rubocop:disable Naming/AccessorMethodName
        raise ArgumentError, 'followers data must be an array' unless followers_params.is_a?(Array)

        if force_collaboration || account.has_follower_and_email_cc_collaborations_enabled?
          return if !account.has_ticket_followers_allowed_enabled? && !force_collaboration

          followers_data = complete_collaborators_data(followers_params, CollaboratorType.FOLLOWER)
          mark_collaborations_for_deletion(
            collaborations_with_removal_request(follower_collaborations_by_user_id, followers_data)
          )
          mark_collaborations_for_deletion(
            collaborations_with_inactive_users(follower_users_by_id, follower_collaborations_by_user_id, followers_data)
          )

          build_missing_follower_collaborations(followers_data)
          update_current_collaborators # defined in ticket/collaborating.rb
        else
          update_collaborators(collaborators: transform_collaborators_params_to_legacy_format(followers_params))
        end
      end

      def set_email_ccs=(email_ccs_params) # rubocop:disable Naming/AccessorMethodName
        raise ArgumentError, 'email_ccs data must be an array' unless email_ccs_params.is_a?(Array)

        if force_collaboration || account.has_follower_and_email_cc_collaborations_enabled?
          return if !account.has_comment_email_ccs_allowed_enabled? && !force_collaboration

          email_ccs_data = complete_collaborators_data(email_ccs_params, CollaboratorType.EMAIL_CC)

          mark_collaborations_for_deletion(
            collaborations_with_removal_request(
              email_cc_collaborations_by_user_id,
              email_ccs_data
            ) +
            collaborations_with_inactive_users(
              email_cc_users_by_id,
              email_cc_collaborations_by_user_id,
              email_ccs_data
            )
          )

          build_missing_email_cc_collaborations(email_ccs_data)
          update_current_collaborators # defined in ticket/collaborating.rb
        else
          update_collaborators(collaborators: transform_collaborators_params_to_legacy_format(email_ccs_params))
        end
      end

      protected

      # Common
      def complete_collaborators_data(collaborators_params, collaborator_type)
        email_addresses_with_data_missing = collaborators_params.map do |collaborator|
          collaborator[:email]&.downcase unless collaborator[:user_id]
        end.compact

        email_identities = account.user_email_identities.where(value: email_addresses_with_data_missing).
          select('value,user_id')
        user_ids_by_email = email_identities.each_with_object({}) do |email_identity, user_ids_by_email_hash|
          user_ids_by_email_hash[email_identity.value&.downcase] = email_identity.user_id
        end

        collaborators_params.each do |collaborator|
          collaborator[:user_id] ||= user_ids_by_email[collaborator[:email]]
        end

        user_ids = collaborators_params.each_with_object([]) do |collaborator_data, user_ids_collector|
          user_ids_collector << collaborator_data[:user_id] if collaborator_data[:user_id]
        end

        if collaborator_type == CollaboratorType.FOLLOWER
          populate_followers_data(user_ids)
        else
          populate_ccs_data(user_ids)
        end

        collaborators_params
      end

      def mark_collaborations_for_deletion(collaborations_for_removal)
        @collaborations_for_removal ||= []
        @collaborations_for_removal += collaborations_for_removal
      end

      def collaborations_with_removal_request(collaborations_by_user_id, collaborators_data)
        collaborators_data.map do |collaborator_data|
          collaborations_by_user_id[collaborator_data[:user_id]] if collaborator_data[:action] == :delete
        end.compact
      end

      def collaborations_with_inactive_users(collaboration_users_by_id, existing_collaborations, collaborators_data)
        all_user_ids = collaborators_data.map { |collaborator_data| collaborator_data[:user_id] }.compact
        ids_for_inactive_users = (all_user_ids - collaboration_users_by_id.keys)
        ids_for_inactive_users.map { |user_id| existing_collaborations[user_id] }.compact
      end

      def transform_collaborators_params_to_legacy_format(collaborators_data)
        collaborators_data.map do |collaborator_data|
          collaborator_data[:user_id] || collaborator_data[:email]
        end.join(',')
      end

      # Followers-specific
      def follower_users_by_id
        @follower_users_by_id ||= {}
      end

      def follower_collaborations_by_user_id
        @follower_collaborations_by_user_id ||= begin
          collaborations.select do |collaboration|
            collaboration.follower?(verify_agent: false)
          end.index_by(&:user_id)
        end
      end

      def build_missing_follower_collaborations(followers_data)
        user_ids_for_persistable_followers_data = followers_data.map do |follower_data|
          follower_data[:user_id] if follower_data[:action] != :delete
        end.compact

        user_ids_for_persistable_followers = user_ids_for_persistable_followers_data - user_ids_ineligible_for_follower_collaboration

        user_ids_without_follower_collaborations = user_ids_for_persistable_followers - follower_collaborations_by_user_id.keys
        users_for_new_collaborations = user_ids_without_follower_collaborations.each_with_object([]) do |user_id, users_collector|
          next unless follower_users_by_id.key?(user_id)

          user = follower_users_by_id[user_id]

          if email = user.email
            next unless permitted_collaborator_email?(email)
          end

          users_collector << user
        end
        users_for_new_collaborations.each do |user|
          next unless user.is_agent?
          collaborations.build(account: account, ticket: self, user: user, collaborator_type: CollaboratorType.FOLLOWER)
        end
      end

      # Email CCs-specific
      def email_cc_users_by_id
        @email_cc_users_by_id ||= {}
      end

      def email_cc_collaborations_by_user_id
        @email_cc_collaborations_by_user_id ||= begin
          collaborations.select do |collaboration|
            collaboration.email_cc?(verify_user: false)
          end.index_by(&:user_id)
        end
      end

      def build_missing_email_cc_collaborations(email_ccs_data)
        user_ids_for_persistable_email_ccs_data = email_ccs_data.map do |email_cc_data|
          email_cc_data[:user_id] if email_cc_data[:action] == :put
        end.compact

        user_ids_for_persistable_email_ccs = user_ids_for_persistable_email_ccs_data - user_ids_ineligible_for_cc_collaboration

        user_ids_without_email_cc_collaborations = user_ids_for_persistable_email_ccs - email_cc_collaborations_by_user_id.keys
        build_existing_user_email_cc_collaborations(user_ids_without_email_cc_collaborations)
        new_users_email_ccs_data = email_ccs_data.select { |email_cc_data| email_cc_data[:user_id].nil? }
        build_new_email_cc_users_and_collaborations(users_data: new_users_email_ccs_data)
      end

      def build_existing_user_email_cc_collaborations(user_ids)
        users = user_ids.each_with_object([]) do |user_id, users_collector|
          next unless email_cc_users_by_id.key?(user_id)

          user = email_cc_users_by_id[user_id]

          if email = user.email
            next unless permitted_collaborator_email?(email)
          end

          users_collector << user
        end

        users.each do |user|
          collaborations.build(ticket: self, user: user, collaborator_type: CollaboratorType.EMAIL_CC)
        end
      end

      def build_new_email_cc_users_and_collaborations(users_data:)
        if new_users_allowed?
          users_data.each do |new_user_data|
            next if new_user_data[:email].blank? || new_user_data[:action] != :put
            next unless permitted_collaborator_email?(new_user_data[:email])

            # Ensure there is a name
            if new_user_data[:name].blank?
              new_user_name = Zendesk::Mail::Address.parse(new_user_data[:email]).try(:name)
              new_user_data[:name] = new_user_name if new_user_name

              # TODO: Logging for EM-3853. Remove when bug is fixed.
              if new_user_data[:name].nil?
                statsd_client.increment("collaborators.new_user_data_name_nil")
                Rails.logger.info("new_user_data[:name] nil even after attempting to extract it from email. account_id: #{account.id} new_user_data: #{new_user_data.inspect}")
                new_user_data[:name] = "Unknown"
              end
            end

            # Ensure name is longer than minimum length
            if new_user_data[:name].size < 2
              new_user_data[:name] = "Unknown #{new_user_data[:name]}".strip
            end

            user = find_or_build_new_user(new_user_data)

            next unless user.valid?

            collaborations.build(ticket: self, user: user, collaborator_type: CollaboratorType.EMAIL_CC)
          end
        else
          disallowed_users = users_data.map { |user| user[:email] }.join(", ")
          if disallowed_users.present?
            add_translatable_error_event(key: "ticket_fields.current_collaborators.new_users_not_allowed", removed_ccs: disallowed_users)
          end
        end
      end

      private

      # private helpers to process email ccs data

      def email_ccs_collaborations(collaborations)
        collaborations.select do |collaboration|
          collaboration.collaborator_type == CollaboratorType.EMAIL_CC || (
            collaboration.collaborator_type == CollaboratorType.LEGACY_CC &&
            collaboration.user.is_end_user?
          )
        end
      end

      def group_ccd_users_based_on_comment_access(users)
        # When removing the email_ccs_light_agents_v2 Arturo after being GA'd, this entire method can be removed -
        # it was found to be seemingly unused code while fixing issues for the CCs/Followers light agents v2 functionality
        users.partition { |user| user_can_comment_publicly_or_light_agent_ccs_allowed?(user) }
      end

      def user_ids_ineligible_for_cc_collaboration
        (
          cc_user_ids_to_mark_for_removal +
            cc_users_with_only_private_comment_access.map(&:id)
        ).uniq
      end

      def cc_user_ids_to_mark_for_removal
        @collaborations_for_removal.each_with_object([]) do |collaboration, result|
          result << collaboration.user_id if collaboration.email_cc?(verify_user: false)
        end
      end

      def populate_ccs_data(user_ids)
        ccd_users = account.users.where(id: user_ids)

        @email_cc_users_by_id = ccd_users.index_by(&:id)
      end

      def cc_users_with_only_private_comment_access
        return [] if account.has_email_ccs_light_agents_v2? && account.has_light_agent_email_ccs_allowed_enabled?

        email_cc_users_by_id.values.reject do |user|
          user.can?(:publicly, Comment)
        end
      end

      # private helpers to process followers data

      # Include end-user IDs so that we do not create collaborations for them
      def user_ids_ineligible_for_follower_collaboration
        (
          follower_user_ids_to_mark_for_removal +
          end_user_followers.map(&:id)
        ).uniq
      end

      def populate_followers_data(user_ids)
        followers = account.users.where(id: user_ids)

        @follower_users_by_id = followers.index_by(&:id)
      end

      def follower_user_ids_to_mark_for_removal
        @collaborations_for_removal.each_with_object([]) do |collaboration, result|
          result << collaboration.user_id if collaboration.follower?(verify_agent: false)
        end
      end

      def end_user_followers
        follower_users_by_id.values.select(&:is_end_user?)
      end
    end
  end
end
