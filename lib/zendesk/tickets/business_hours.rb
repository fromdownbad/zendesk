module Zendesk
  module Tickets
    module BusinessHours
      def self.included(base)
        base.has_one :ticket_schedule,
          dependent: :destroy,
          inverse_of: :ticket
        base.has_one :schedule, through: :ticket_schedule
      end

      def schedule
        ticket_schedule.try(:schedule) || account.schedule
      end

      def calendar_minutes_diff(time_1, time_2)
        [0, ((time_2 - time_1) / 1.minute).round].max
      end

      def business_minutes_diff(time_1, time_2)
        if account.business_hours_active?
          business_hours_calculation.business_minutes_diff(time_1, time_2)
        else
          calendar_minutes_diff(time_1, time_2)
        end
      end

      def calendar_hours_ago(business_hours)
        if account.business_hours_active?
          business_hours_calculation.calendar_hours_ago(business_hours)
        else
          business_hours
        end
      end

      def in_business_hours?
        if account.business_hours_active?
          business_hours_calculation.in_business_hours?(Time.now)
        else
          true
        end
      end

      def on_holiday?
        if account.business_hours_active?
          business_hours_calculation.on_holiday?(Time.now)
        else
          false
        end
      end

      def business_hours_calculation
        BusinessHoursSupport::Calculation.new(schedule)
      end
    end
  end
end
