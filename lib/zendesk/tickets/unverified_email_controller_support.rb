module Zendesk
  module Tickets
    module UnverifiedEmailControllerSupport
      # This module contains the logic needed to block end users in the following situations:
      # * When unverified_ticket_creations is off: end users
      # with unverified emails created after September 17, 2017.
      # * When unverified_ticket_creations is on: end users with
      # tickets created using unverified email addresses.
      #
      # This is to control a security vulnerability known as "Ticket Trick".
      # This vulnerability would allow hackers to sign up to a company's
      # help center and use the unverified email provided to access that
      # company's Yammer or Slack instances.
      #
      # We use this code in different endpoints to stop showing requests/comments
      # to end users in the situation described above:
      #
      # * /api/v2/requests
      # * /api/v2/requests/{id}/comments
      # * /api/v1/requests
      #
      # This code also logs everything. Here is a visualization chart of number
      # of request from verified users compared to unverified users per time:
      # https://zendesk.datadoghq.com/dash/361202?live=true&page=0&is_auto=false&from_ts=1505871469859&to_ts=1505885869859&tile_size=m
      #
      # References
      #
      # * Announcement: https://support.zendesk.com/hc/en-us/articles/115012718048
      # * Ticket Trick blog post: https://thenextweb.com/security/2017/09/21/ticket-trick-see-hackers-gain-unauthorized-access-slack-teams-exploiting-issue-trackers/
      #

      protected

      def ensure_end_user_email_is_verified
        if current_account.has_unverified_ticket_creations?
          ensure_end_user_has_no_unverified_tickets
        else
          ensure_end_user_has_no_unverified_emails
        end
      end

      def ensure_end_user_has_no_unverified_tickets
        if current_user.is_end_user? && current_user.unverified_ticket_creations.any?
          Rails.logger.info("End User created tickets using unverified emails - ACCOUNT #{current_account.id} - UNVERIFIED TICKET CREATIONS IN QUESTION #{current_user.unverified_ticket_creations.inspect}")
          statsd_client.increment('unverified.ticket')
          render_unverified_email_address
        else
          statsd_client.increment('verified.ticket')
        end
      end

      def ensure_end_user_has_no_unverified_emails
        if current_user.is_end_user? && current_user.identities.unverified_emails.any?
          Rails.logger.info("Unverified End User Email would be restricted ACCOUNT #{current_account.id} IDENTITIES IN QUESTION #{current_user.identities.unverified_emails.inspect}")
          statsd_client.increment('unverified.email.blocked')
          render_unverified_email_address
        else
          statsd_client.increment('verified.email')
        end
      end

      def render_unverified_email_address
        # help center will rely on the message to determine if the error is related to
        # unverified email address please do NOT extract the error message.
        render_failure(
          status: :forbidden,
          title: 'Forbidden',
          message: 'Unverified email address'
        )
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [controller_name])
      end
    end
  end
end
