# This module contains the logic around problem/incident management
module Zendesk::Tickets::ProblemIncident
  # Threshold for when to push solving linked incidents to the background
  INLINE_INCIDENT_PROCESSING_LIMIT = ENV.fetch("INLINE_INCIDENT_PROCESSING_LIMIT", 2).to_i

  def self.included(base)
    base.validate :incident_must_link_to_problem

    # Tickets of type Problem can have linked incidents
    base.after_update :save_intermediate_comment_value
    base.after_commit :solve_or_enqueue_linked_incidents, on: :update
  end

  attr_accessor :linked_incidents_summary

  def incident_must_link_to_problem
    return unless incident? && problem
    return if problem.problem? || skip_incident_problem_check?

    Rails.logger.info("Updating ticket #{id} on account #{account_id} with an incorrectly linked problem")

    errors.add(:base, I18n.t("activerecord.errors.models.ticket.attributes.problem_id.invalid"))
  end

  def will_process_incidents_in_background?
    !current_user.is_system_user? && incident_count_mandates_background_processing?
  end

  def incident_count_mandates_background_processing?
    incident_working_count > INLINE_INCIDENT_PROCESSING_LIMIT
  end

  def incident_working_count
    @incident_count ||= incidents.working.count(:all)
  end

  def will_solve_linked_tickets?
    return false unless previous_changes[:status_id] && status?(:solved)
    return false unless ticket_type?(:problem)
    return false unless current_user
    return false unless current_user.is_agent? || current_user.foreign_agent?

    true
  end

  def save_intermediate_comment_value
    if comment.present?
      @problem_incident_options = {
        value: comment.intermediate_value,
        is_public: comment.is_public?,
        format: comment.format
      }
      @problem_incident_options[:attachment_ids] = comment.attachments.reject { |a| !a.parent_id.nil? }.map(&:id) if comment.attachments.any?
    else
      @problem_incident_options = { }
    end
  end

  def solve_or_enqueue_linked_incidents
    return unless will_solve_linked_tickets?

    if will_process_incidents_in_background?
      Rails.logger.info("Enqueueing SolveIncidentsJob for ticket #{id} on account #{account_id}")
      SolveIncidentsJob.enqueue(account_id, current_user.id, @problem_incident_options.merge(problem: id))
    else
      Rails.logger.info("Executing SolveIncidentsJob for ticket #{id} on account #{account_id}")
      ZendeskAPM.trace(
        'solve_incidents_job.execution',
        service: 'classic-ticket-solve-incidents'
      ) do |span|
        span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
        span.set_tag('zendesk.account_id', account_id)
        span.set_tag('zendesk.ticket.incident_count', incident_working_count)
        self.linked_incidents_summary = SolveIncidentsJob.work(account_id, current_user.id, @problem_incident_options.merge(problem: self))
      end
    end
  end

  def problem?
    ticket_type_id == TicketType.PROBLEM
  end

  def incident?
    ticket_type_id == TicketType.INCIDENT
  end

  private

  def skip_incident_problem_check?
    current_via_id == ViaType.MAIL || current_via_id == ViaType.RULE || !current_user.can?(:edit_properties, self) || current_user.is_end_user?
  end
end
