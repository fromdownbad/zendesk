module Zendesk
  module Tickets
    module CommentUpdate
      def can_bulk_update_comments(ticket)
        # ZD678349 - light agents should be able to bulk update tickets with a private comment
        current_user.can?(:only_create_ticket_comments, ticket) &&
        ticket.changes.empty? && ticket.comment &&
        ticket.comment.value_changed?
      end
    end
  end
end
