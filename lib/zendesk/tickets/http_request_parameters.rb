module Zendesk
  module Tickets
    class HttpRequestParameters
      attr_reader :user_agent, :remote_ip, :xml_or_json_format, :token_user

      def initialize(request)
        if HashParam.ish?(request)
          sanitize_request_from_hash(request)
        else
          sanitize_request(request)
        end
      end

      def internal_client?
        @request_from_internal_client
      end

      def access_token_has_sdk_scope?
        @access_token_has_sdk_scope
      end

      def has_x_on_behalf_of_header? # rubocop:disable Naming/PredicateName
        @x_on_behalf_of_header.present?
      end

      private

      def sanitize_request(request)
        # request.try(:user_agent) will work on Rails 4+
        @user_agent = request.user_agent if request.respond_to?(:user_agent)
        @remote_ip  = request.remote_ip if request.respond_to?(:remote_ip)

        @xml_or_json_format = check_format_type(request.format) if request.respond_to?(:format)

        if request.respond_to?(:env)
          token = request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN]

          @token_user = token.try(:user)
          # mobile sdk request needs this to check token scopess
          @access_token_has_sdk_scope = check_token_scope(token)
        end

        @request_from_internal_client = request.respond_to?(:internal_client?) && request.internal_client?
        @x_on_behalf_of_header = request.headers['X-On-Behalf-Of'] if request.respond_to?(:headers)
      end

      def sanitize_request_from_hash(request)
        # request_params will be converted to a hash when it is passed into Redis Job.
        # This is used to recreate a HttpRequestParameters object for ticket/initializer.
        @user_agent = request[:user_agent]
        @remote_ip  = request[:remote_ip]
        @token_user = request[:token_user]
        @xml_or_json_format = request[:xml_or_json_format]
        @x_on_behalf_of_header = request[:x_on_behalf_of_header]
        @access_token_has_sdk_scope = request[:access_token_has_sdk_scope]
        @request_from_internal_client = request[:request_from_internal_client]
      end

      def check_format_type(format)
        [Mime[:xml], Mime[:json]].member?(format)
      end

      def check_token_scope(token)
        !!token && token.scopes.include?('sdk')
      end
    end
  end
end
