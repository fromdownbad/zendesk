module Zendesk::Tickets::SoftDeletion
  def self.included(base)
    base.class_eval do
      has_soft_deletion

      # Match the default_scope implementation in soft_deletion 1.0
      self.soft_delete_default_scope = name
      default_scope do
        if Thread.current[:"soft_deletion_with_deleted_#{soft_delete_default_scope}"]
          where(nil)
        else
          where("tickets.status_id NOT IN (?)", Zendesk::Types::StatusType::INACCESSIBLE_LIST)
        end
      end

      include InstanceMethods

      after_soft_delete :remove_from_recent_tickets
      after_soft_delete :log_deletion
      after_soft_delete :clear_bookmarks
    end
  end

  module InstanceMethods
    def soft_delete!
      # run soft deletion without validation
      soft_delete(validate: false) || raise(ActiveRecord::RecordInvalid, self)
    end

    def soft_delete(*args)
      @audit.disable_triggers = true
      if archived?
        ticket_archive_stub.soft_delete(@audit, *args)
        remove_from_recent_tickets
        clear_bookmarks
        log_deletion
      else
        # module's copy of #soft_delete
        super
      end
    end

    def self.mark_as_soft_deleted_sql
      "status_id = #{StatusType.DELETED}"
    end

    def mark_as_deleted
      self.status_id = StatusType.DELETED
    end

    def remove_from_recent_tickets
      if current_user.present?
        recent_tickets_manager = Zendesk::Tickets::RecentTicketManager.new(current_user)
        recent_tickets_manager.remove_ticket(self)
      end
    end

    def clear_bookmarks
      bookmarks.clear if bookmarks.exists?
    end

    def log_deletion
      logger.warn("#{Time.now}: #{account.subdomain}/#{account.id} ticket ##{nice_id}/#{id}")
    end

    def soft_undelete!(user = User.system)
      return if scrubbed?
      return if status_id != StatusType.DELETED
      will_be_saved_by(user)
      audit.disable_triggers = true
      if archived?
        ticket_archive_stub.soft_undelete!(audit)
      else
        @force_reopen = true
        self.status_id = previous_status
        save!(validate: false)
      end
    ensure
      @force_reopen = false
    end

    def soft_deletion_event
      scope = events.use_index(:index_events_on_ticket_id_and_type)
      scope.where(value_reference: "status_id", value: StatusType.DELETED).last
    end

    def previous_status
      soft_deletion_event.value_previous.to_i
    end
  end
end
