# This module contains the logic to create a test ticket and generate automated responses in that ticket
# The test ticket is used in the getting started guide
module Zendesk::Tickets::TestTicket
  TEST_REQUESTER = { email: "customer@example.com" }.freeze

  def self.included(base)
    base.extend(ClassMethods)
    base.after_commit :add_automatic_reply, on: :update, if: :automatic_reply?
    base.validate :validate_no_ticket_sharing
  end

  # Sharing sample tickets can cause circular looping.
  def validate_no_ticket_sharing
    return unless via?(:sample_ticket) || via?(:sample_interactive_ticket)

    if audit && audit.events.detect { |e| e.is_a? TicketSharingEvent }
      errors.add(:base, "Sample tickets can't be shared")
    end
  end

  module ClassMethods
    def generate_sample_ticket(name,
      account,
      user,
      requester = nil,
      tag       = 'txt.admin.models.account.creation.first_ticket_mailer.sample_zendesk_tag',
      _bindings = {})
      ticket_template = ticket_template_with_context(user, name)
      return unless ticket_template

      requester ||= sample_customer(account)
      ticket = account.tickets.build do |t|
        t.requester      = requester
        t.assignee       = user
        t.subject        = ticket_template[:subject]
        t.description    = ticket_template[:description]
        t.via_id         = ticket_template[:via_type]
        t.status_id      = ticket_template[:status_id]
        t.ticket_type_id = ticket_template[:ticket_type_id]
        t.priority_id    = ticket_template[:priority_id]
      end

      ticket.set_tags = I18n.t(tag)
      ticket.will_be_saved_by(user)
      Rails.logger.info("InvalidSampleTicket created for account #{account.id}") unless ticket.valid?
      ticket.generated_timestamp = Time.now
      instrument_sample_ticket_creation(account, name) if ticket.save!(validate: false)
      ticket
    end

    def sample_customer(account)
      image_path = I18n.t('txt.zero_states.sample_ticket.sample_customer_avatar')
      customer_name = I18n.t('txt.zero_states.sample_ticket.sample_customer')

      if Arturo.feature_enabled_for?(:support_suite_trial_refresh_phase2, account)
        image_path = I18n.t('txt.zero_states.sample_ticket.fake_customer_avatar')
        customer_name = I18n.t('txt.zero_states.sample_ticket.fake_customer')
      end

      image_url = "#{account.url(mapped: false)}/#{image_path}"
      requester_info = TEST_REQUESTER.merge name: customer_name, remote_photo_url: image_url
      account.find_user_by_email(TEST_REQUESTER[:email]) || account.users.new(requester_info)
    end

    def create_test_ticket!(account, user)
      generate_sample_ticket('test_ticket', account, user)
    end

    def with_triggers_disabled(account, title_keys)
      return unless block_given?

      modified_triggers = account.disable_triggers(title_keys)

      begin
        sample_ticket = yield
      ensure
        enable_triggers(modified_triggers)
      end

      sample_ticket
    end

    def generate_sample_ticket_without_notifications(account, user, template_name = nil)
      notification_trigger_keys = [
        "txt.default.triggers.notify_requester_received.title",
        "txt.default.triggers.notify_all_received.title",
        "txt.default.triggers.notify_requester_and_ccs_of_received_request.title",
        "txt.default.triggers.notify_requester_proactive_ticket.title"
      ]

      if template_name.blank? || template_name == "meet_the_ticket"
        template_name = account.translation_locale.english? ? 'meet_the_ticket_en' : 'meet_the_ticket'
      end

      with_triggers_disabled(account, notification_trigger_keys) do
        generate_sample_ticket(template_name, account, user).tap do |t|
          t.ticket_type_id = TicketType.INCIDENT
          t.priority_id    = PriorityType.NORMAL
          t.will_be_saved_by(user)
          t.save!(validate: false)
        end
      end
    end

    def instrument_sample_ticket_creation(account, template_name)
      return if template_name != "suite_trial_sample_ticket"

      statsd_client = Zendesk::StatsD::Client.new(namespace: ['suite_trial'])

      tags = { 'account.subdomain': account.subdomain }
      tags = tags.to_a.map! { |tag| tag[0].to_s + ":" + tag[1].to_s }

      statsd_client.increment("sample_ticket_created", tags: tags)
    end

    def enable_triggers(triggers)
      triggers.each do |trigger|
        trigger.update_attribute(:is_active, true)
      end
    end

    def generate_email_forward_pending_ticket(account, user)
      notification_trigger_keys = [
        "txt.default.triggers.notify_all_received.title"
      ]

      with_triggers_disabled(account, notification_trigger_keys) do
        ticket_template = 'email_forward_pending_ticket'
        generate_sample_ticket(ticket_template, account, user, nil).tap do |t|
          t.ticket_type_id = TicketType.INCIDENT
          t.priority_id = PriorityType.NORMAL
          t.will_be_saved_by(user)
          t.save!
        end
      end
    end

    def ticket_template_with_context(user, name)
      case name
      when 'first_sample_ticket'
        { subject: I18n.t('txt.zero_states.sample_ticket.first.subject'),
          via_type: ViaType.SAMPLE_TICKET,
          description: <<~DESC
            #{I18n.t('txt.email.boost_mailer.boost_expiry.email_body_1', name: user.name)}

            #{I18n.t('txt.zero_states.sample_ticket.first.body_1', support_email: user.account.subdomain)}

            #{I18n.t('txt.zero_states.sample_ticket.first.body_2')}
            #{I18n.t('txt.zero_states.sample_ticket.learn_how_ticket_works')}
          DESC
        }
      when 'second_sample_ticket'
        { subject: I18n.t('txt.zero_states.sample_ticket.second.subject'),
          via_type: ViaType.SAMPLE_TICKET,
          description: <<~DESC
            #{I18n.t('txt.email.boost_mailer.boost_expiry.email_body_1', name: user.name)}

            #{I18n.t('txt.zero_states.sample_ticket.second.body_1')}

            #{I18n.t('txt.zero_states.sample_ticket.second.body_2')}
            #{I18n.t('txt.zero_states.sample_ticket.learn_add_agents')}
          DESC
        }
      when 'third_sample_ticket'
        { subject: I18n.t('txt.zero_states.sample_ticket.third.subject'),
          via_type: ViaType.SAMPLE_TICKET,
          description: <<~DESC
            #{I18n.t('txt.email.boost_mailer.boost_expiry.email_body_1', name: user.name)}

            #{I18n.t('txt.zero_states.sample_ticket.third.body_1')}

            #{I18n.t('txt.zero_states.sample_ticket.third.body_2')}
            #{I18n.t('txt.zero_states.sample_ticket.learn_how_macro_works')}

            #{I18n.t('txt.zero_states.sample_ticket.third.body_3')}
            #{I18n.t('txt.zero_states.sample_ticket.learn_how_views_works')}

            #{I18n.t('txt.zero_states.sample_ticket.third.body_4')}

            #{I18n.t('txt.zero_states.sample_ticket.third.body_5')}
          DESC
         }
      when 'test_ticket'
        { subject: I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.subject',
          user_name: user.name),
          via_type: ViaType.SAMPLE_INTERACTIVE_TICKET,
          description: <<~DESC
            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_1')}

            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_2')}

            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_3')}

            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_4')}
            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_5')}

            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_7')}
          DESC
         }
      when 'test_ticket_first_comment'
        { description: <<~DESC
          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_first_reply.body_1')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_first_reply.body_2')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_first_reply.body_3')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_first_reply.body_4')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_first_reply.body_5')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_first_reply.body_6')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_first_reply.body_8_lotus')}
          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_first_reply.body_7_lotus')}
        DESC
        }
      when 'test_ticket_second_comment'
        { description: <<~DESC
          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_second_reply.body_1')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_second_reply.body_2')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_second_reply.body_3')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_second_reply.body_4', language_link: I18n.t('txt.link_to_lesson_4_zendesk'))}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_second_reply.body_5')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_second_reply.body_6')}

          #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.add_second_reply.body_7')}
        DESC
        }
      when 'closed_sample_ticket'
        { subject: I18n.t('txt.zero_states.sample_ticket.closed.subject'),
          via_type: ViaType.SAMPLE_TICKET,
          status_id: StatusType.CLOSED,
          description: <<~DESC
            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_1')}

            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_2')}

            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_3')}

            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_4')}

            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_5')}

            #{I18n.t('txt.email.lib.zendesk.tickets.test_ticket.create_test_ticket.body_7')}
          DESC
          }
      when 'meet_the_ticket_en'
        { subject: I18n.t('txt.zero_states.sample_ticket.meet_the_ticket.subject'),
          via_type: ViaType.SAMPLE_TICKET,
          description: <<~DESC
            #{I18n.t('txt.zero_states.sample_ticket.greeting_2', first_name: user.first_name, last_name: user.last_name)}

            #{I18n.t('txt.zero_states.sample_ticket.first.body_1a', support_email: user.account.subdomain)}

            #{I18n.t('txt.zero_states.sample_ticket.first.body_2')}
            #{I18n.t('txt.zero_states.sample_ticket.learn_how_ticket_works')}
          DESC
        }
      when 'meet_the_ticket'
        { subject: I18n.t('txt.zero_states.sample_ticket.meet_the_ticket.subject'),
          via_type: ViaType.SAMPLE_TICKET,
          ticket_type_id: TicketType.INCIDENT,
          priority_id: PriorityType.NORMAL,
          description: <<~DESC
            #{I18n.t('txt.zero_states.sample_ticket.greeting_2', first_name: user.first_name, last_name: user.last_name)}

            #{I18n.t('txt.zero_states.sample_ticket.first.body_1a', support_email: user.account.subdomain)}
          DESC
          }
      when 'email_forward_pending_ticket'
        { subject: I18n.t('txt.zero_states.email_forward_pending_ticket.subject'),
          via_type: ViaType.SAMPLE_TICKET,
          description: <<~DESC
            #{I18n.t('txt.zero_states.email_forward_pending_ticket.body_pending')}
          DESC
          }
      when 'email_forward_complete_comment'
        { description: <<~DESC
          #{I18n.t('txt.zero_states.email_forward_pending_ticket.comment_complete')}
        DESC
        }
      when "suite_trial_sample_ticket" # This block needs to be I18n translated
        { subject: I18n.t('txt.zero_states.sample_ticket.suite_trial_sample_ticket.subject'),
          via_type: ViaType.SAMPLE_TICKET,
          description: <<~DESC
            #{I18n.t('txt.support_suite_trial_refresh.sample_ticket.greeting')}
            #{I18n.t('txt.support_suite_trial_refresh.sample_ticket.body_1')}
            #{I18n.t('txt.support_suite_trial_refresh.sample_ticket.body_2')}
            #{I18n.t('txt.support_suite_trial_refresh.sample_ticket.customer_sign')}
          DESC
        }
      end
    end
  end

  def test_ticket?
    requester.try(:email) == TEST_REQUESTER[:email]
  end

  def add_reply_with_template(template_name:)
    ticket_template = self.class.ticket_template_with_context(assignee, template_name)
    add_reply(ticket_template)
  end

  protected

  def add_automatic_reply
    # Automatic replies on shared tickets can generate a loop.
    return if shared_tickets.present?

    case comments.size
    when 2
      ticket_template = self.class.ticket_template_with_context(assignee, 'test_ticket_first_comment')
      add_reply(ticket_template)
    when 4
      ticket_template = self.class.ticket_template_with_context(assignee, 'test_ticket_second_comment')
      add_reply(ticket_template)
    end
  end

  def add_reply(ticket_template)
    locale = assignee.try(:translation_locale) || account.translation_locale
    I18n.with_locale(locale) do
      add_comment(body: ticket_template[:description], is_public: true)
      will_be_saved_by(requester, via_id: via_id)
      save
    end
  end

  def automatic_reply?
    test_ticket? && via_id == ViaType.SAMPLE_INTERACTIVE_TICKET
  end
end
