module Zendesk::Tickets
  # Build URLs for tickets.
  #
  # Depending on who needs to access a ticket, the URL may vary. Agents generally use
  # `/tickets/<id>` while end users go through `/requests/<id>`. End users may also
  # be allowed access without authenticating if a token parameter is passed.
  #
  # This class provides a method for building these URLs.
  #
  class UrlBuilder
    attr_reader :account, :user

    # Create a new Tickets::UrlBuilder for the specified account and user.
    #
    # If a user is not provided, it is assumed to be the anonymous user.
    #
    # Options
    #
    # :account - the Account that holds the tickets.
    # :user    - the User that is to use the URL (optional).
    #
    def initialize(options = {})
      @account   = options.fetch(:account)
      @user      = options[:user]
      @for_agent = options[:for_agent]
    end

    # Build a URL pointing to a ticket.
    #
    # ticket - the Ticket to which the URL should point.
    #
    # Examples
    #
    #   # If the user is an agent:
    #   url_for(ticket)
    #   #=> "support.acme.com/tickets/42"
    #
    #   # If the user is an end user:
    #   url_for(ticket)
    #   #=> "support.acme.com/requests/42"
    #
    #   # This is the default:
    #   url_for(ticket)
    #   #=> "support.acme.com/requests/42"
    #
    #   # If no ticket is specified
    #   path_for(nil)
    #   #=> "requests" #used for autolinking base url
    #
    # Returns the String URL pointing to the ticket.
    def url_for(ticket, options = {})
      return unless path = path_for(ticket, options)
      "#{base_url(ticket.brand)}/#{path}"
    end

    # without leading slash
    def path_for(ticket, options = {})
      return unless path = page

      if ticket
        params = options[:params] || {}
        path << "/#{ticket.nice_id}"
        path << "?#{params.to_param}" unless params.empty?
      end

      path
    end

    def base_url(brand)
      return account.url(url_params) if agent? || brand.nil?
      brand.url(url_params)
    end

    private

    def url_params
      if agent?
        {mapped: false, ssl: true}
      else
        {}
      end
    end

    def agent?
      @for_agent ||= user && user.is_agent?
    end

    def help_center?
      !agent? && account.help_center_enabled?
    end

    def page
      if agent?
        "agent/tickets"
      elsif help_center?
        "hc/requests"
      end
    end
  end
end
