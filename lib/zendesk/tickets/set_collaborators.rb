module Zendesk
  module Tickets
    module SetCollaborators
      include CollaborationSupport

      def set_collaborators=(string_or_array_of_ids_or_emails) # rubocop:disable Naming/AccessorMethodName
        update_collaborators(collaborators: string_or_array_of_ids_or_emails)
      end

      def additional_collaborators=(string_or_array_of_ids_or_emails)
        update_collaborators(collaborators: string_or_array_of_ids_or_emails, update_type: 'add')
      end

      def set_collaborators_with_type(string_or_array_of_ids_or_emails, collaborator_type)
        update_collaborators(collaborators: string_or_array_of_ids_or_emails, collaborator_type: collaborator_type)
      end

      def additional_collaborators_with_type(string_or_array_of_ids_or_emails, collaborator_type)
        update_collaborators(collaborators: string_or_array_of_ids_or_emails, update_type: 'add', collaborator_type: collaborator_type)
      end

      def set_collaborators_with_email_cc_type=(string_or_array_of_ids_or_emails) # rubocop:disable Naming/AccessorMethodName
        update_collaborators(collaborators: string_or_array_of_ids_or_emails, collaborator_type: CollaboratorType.EMAIL_CC)
      end

      def additional_collaborators_with_email_cc_type=(string_or_array_of_ids_or_emails)
        update_collaborators(collaborators: string_or_array_of_ids_or_emails, update_type: 'add', collaborator_type: CollaboratorType.EMAIL_CC)
      end

      # This updates collaborations without checking for the role of the collaborator
      def set_collaborators_with_follower_type=(string_or_array_of_ids_or_emails) # rubocop:disable Naming/AccessorMethodName
        update_collaborators(collaborators: string_or_array_of_ids_or_emails, collaborator_type: CollaboratorType.FOLLOWER)
      end

      # This updates collaborations without checking for the role of the collaborator
      def additional_collaborators_with_follower_type=(string_or_array_of_ids_or_emails)
        update_collaborators(collaborators: string_or_array_of_ids_or_emails, update_type: 'add', collaborator_type: CollaboratorType.FOLLOWER)
      end

      protected

      def update_collaborators(collaborators:, update_type: 'replace', collaborator_type: nil)
        return unless account.present?
        return unless collaboration_enabled_or_forced?

        replacing_existing = update_type == 'replace'

        if account.has_follower_and_email_cc_collaborations_enabled? && collaborator_type
          collaborations_by_type = collaborations.where(collaborator_type: collaborator_type)
          inactive_collaborations = collaborations_by_type.reject(&:user)
        else
          collaborations_by_type = collaborations
          inactive_collaborations = collaborations.to_a.delete_if(&:user)
        end

        # [ZD#1715319] Make collaborations more robust if a user is deleted incorrectly
        active_collaborations = collaborations_by_type - inactive_collaborations
        deleted_collaborations = (@collaborations_for_removal.map(&:user_id) if @collaborations_for_removal) || []

        collaboration_user_ids = active_collaborations.map(&:user_id)
        end_user_count = active_collaborations.reject { |c| c.user.is_agent? }.count
        existing_users, new_users = filtered_collaborators(list: collaborators, update_type: update_type, collaboration_user_ids: collaboration_user_ids)

        existing_user_ids = existing_users.map(&:id)
        collaborations_left = account.settings.collaborators_maximum - end_user_count

        if replacing_existing
          collaboration_user_ids_to_delete = collaboration_user_ids - existing_user_ids + inactive_collaborations.map(&:user_id) + deleted_collaborations
          @collaborations_for_removal = existing_collaborations(collaboration_user_ids_to_delete, collaborator_type)
          collaborations_left += collaboration_user_ids_to_delete.count
        end

        collaboration_user_ids_to_add = existing_user_ids - collaboration_user_ids

        new_collaborators = []
        all_collaborators = []
        skipped = []

        existing_users.each do |user|
          if account.has_follower_and_email_cc_collaborations_enabled? || collaborations_left > 0 || user.is_agent?
            all_collaborators << user
            next unless collaboration_user_ids_to_add.include?(user.id)

            new_collaborators << user

            if !account.has_follower_and_email_cc_collaborations_enabled? && !user.is_agent?
              collaborations_left -= 1
            end
          elsif collaboration_user_ids.include?(user.id)
            all_collaborators << user
          else
            skipped << user.id
          end
        end

        if new_users_allowed?
          new_users.each do |new_user_data|
            if account.has_follower_and_email_cc_collaborations_enabled? || collaborations_left > 0
              user = find_or_build_new_user(new_user_data)
              next unless user.valid?

              all_collaborators << user
              new_collaborators << user

              unless account.has_follower_and_email_cc_collaborations_enabled?
                collaborations_left -= 1
              end
            else
              skipped << new_user_data[:email]
            end
          end
        else
          disallowed_new_users = new_users.map { |new_user_data| new_user_data[:email] }.join(", ")
          add_translatable_error_event(key: 'ticket_fields.current_collaborators.new_users_not_allowed', removed_ccs: disallowed_new_users) unless disallowed_new_users.empty?
          Rails.logger.warn("is_open? is false for #{account.subdomain}, skipping #{disallowed_new_users}")
        end

        if skipped.any?
          Rails.logger.warn("More than #{account.settings.collaborators_maximum} collaborators requested on ticket #{account.subdomain}/#{id}")

          total_attempted_ccs = skipped.count + all_collaborators.count

          if total_attempted_ccs > collaborators_limit
            Rails.logger.warn("Attempting to exceed #{collaborators_limit} collaborators. #{total_attempted_ccs} collaborators requested on ticket #{account.subdomain}/#{id}")
            statsd_client.increment('collaborators.exceeding_ccs_limit')
          end

          Rails.logger.warn("Skipped: #{skipped}")
        end

        new_collaborators.each do |user|
          if collaborator_type
            collaborations.build(ticket: self, user: user, collaborator_type: collaborator_type)
          else
            collaborations.build(ticket: self, user: user)
          end
        end

        if account.has_email_fix_current_collaborators_set_collaborators? && account.has_follower_and_email_cc_collaborations_enabled?
          update_current_collaborators
        else
          # This is used in RadarNotification to notify inbox about updates to collaborators
          # clear it out so that it'll be repopulated after any new users have been created
          @current_collaborator_ids = nil

          # keep legacy collaborators with just names, but use name and email for all new ones
          self.current_collaborators = if current_collaborators.blank? || current_collaborators =~ /<.*@.*>/
            all_collaborators.map { |u| "#{u.name} <#{u.email}>" }
          else
            all_collaborators.map(&:name)
          end.sort.join(", ").presence
        end
      end

      def filtered_collaborators(list:, update_type:, collaboration_user_ids: nil)
        new_users = []
        user_ids = []
        user_data = []
        user_emails = []

        list = list.to_s.split(",") unless list.is_a?(Array)

        list.compact!
        list.uniq!
        list.each do |item|
          if data = parse_requester_data(item)
            user_data << data
            user_emails << data[:email]
          elsif /^\d+$/.match?(item.to_s.strip)
            user_ids << item.to_i
          end
        end

        user_ids_by_email_address = account.user_email_identities.where(value: user_emails).pluck('lower(value)', 'user_id').to_h

        user_data.each do |data|
          if user_id = user_ids_by_email_address[data[:email]]
            user_ids << user_id
          else
            new_users << data if permitted_collaborator_email?(data[:email])
          end
        end

        user_ids.concat(collaboration_user_ids) if update_type == 'add'

        existing_users = account.users.includes(:identities).where(id: user_ids).select { |user| permitted_collaborator_email?(user.email.to_s) }

        [existing_users.uniq, new_users.uniq]
      end

      def collaboration_enabled_or_forced?
        return true if force_collaboration

        legacy_or_modern_collaboration_enabled?
      end

      # Since requester_email was taken (and not reliable for what we want)
      def collaboration_requester_email
        @collaboration_requester_email ||= requester.try(:email).to_s.downcase
      end

      def find_or_build_new_user(new_user_data)
        email = new_user_data[:email].to_s.downcase

        if !email.blank? && (email == collaboration_requester_email || requester.try(:emails_include?, email))
          requester
        else
          build_new_user(new_user_data)
        end
      end

      def existing_collaborations(collaboration_user_ids, collaborator_type = nil)
        collaborations_for_user_and_type = if collaborator_type
          collaborations.where(user_id: collaboration_user_ids, collaborator_type: collaborator_type)
        else
          collaborations.where(user_id: collaboration_user_ids)
        end

        unless account.has_follower_and_email_cc_collaborations_enabled? && collaborator_type
          return collaborations_for_user_and_type
        end

        collaborations_for_user_and_type.select do |collaboration|
          case collaborator_type
          when CollaboratorType.FOLLOWER
            collaboration.follower?
          when CollaboratorType.EMAIL_CC
            collaboration.email_cc?
          else
            collaboration.legacy_cc?
          end
        end
      end

      def collaborators_limit
        # As a method so is easily stubbed for specs, and avoid n+ test records.
        Ticket::MAX_COLLABORATORS_V2
      end
    end
  end
end
