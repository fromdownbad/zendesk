module Zendesk
  module Tickets
    # Provides functions to get ticket field options for an account
    module TicketFieldOptions
      def get_priorities_for(account, ticket = nil, include_options = [])
        priority_exclude = account.has_extended_ticket_priorities? ? [] : [PriorityType.LOW, PriorityType.URGENT]
        if !account.has_extended_ticket_priorities? && !ticket.nil? && (ticket.priority_id == 1 || ticket.priority_id == 4)
          priority_exclude << ticket.priority_id
        end
        priority_exclude << 0 if ticket.nil? || (ticket.priority_id.to_i > 0 && ticket.ticket_type_id != 0)
        include_options + PriorityType.list(priority_exclude)
      end

      def get_priorities_for_v2(account, ticket = nil, include_options = [])
        priorities = get_priorities_for(account, ticket, include_options)
        map_attributes_for_v2(priorities, Api::V2::Tickets::AttributeMappings::PRIORITY_MAP)
      end

      def get_status_for(account, ticket = nil, include_options = [])
        include_status_hold = account.use_status_hold? ||
          (!ticket.nil? && ticket.status_id.present? && ticket.status?(:hold))

        status_exclude = [StatusType.CLOSED, StatusType.DELETED]
        status_exclude << StatusType.NEW if ticket.nil? || (ticket.status_id && !ticket.status?(:new))
        status_exclude << StatusType.HOLD unless include_status_hold
        include_options + StatusType.list(status_exclude)
      end

      def get_status_for_v2(account, ticket = nil, include_options = [])
        options = get_status_for(account, ticket, include_options)
        map_attributes_for_v2(options, Api::V2::Tickets::AttributeMappings::STATUS_MAP)
      end

      def get_ticket_types_for(_account, ticket = nil, include_options = [])
        include_options + TicketType.list((!ticket.nil? && ticket.ticket_type_id == 0) ? [] : [0])
      end

      def get_ticket_types_for_v2(account, ticket = nil, include_options = [])
        types = get_ticket_types_for(account, ticket, include_options)
        map_attributes_for_v2(types, Api::V2::Tickets::AttributeMappings::TYPE_MAP)
      end

      def map_attributes_for_v2(input, attribute_mappings)
        input.map do |(content, value)|
          [content, attribute_mappings[value.to_s]]
        end
      end
    end
  end
end
