module Zendesk
  module Tickets
    # Used in tickets controller to find tickets.
    # Takes account, current_user and params hash as arguments.
    #
    #   Zendesk::Tickets::Finder.new(
    #     current_account,
    #     current_user,
    #     params
    #   ).tickets
    class Finder
      attr_reader :account, :current_user, :params

      def initialize(account, current_user, params)
        @account = account
        @current_user = current_user
        @params = params
      end

      def tickets
        scope = if organization
          organization.tickets
        else
          account.tickets
        end

        scope = scope.where(external_id: external_id) if external_id.present?

        if current_user.is_admin? || current_user.is_system_user?
          scope
        else
          scope.for_user(current_user)
        end
      end

      private

      def external_id
        params[:external_id]
      end

      def organization_id
        params[:organization_id]
      end

      def organization
        @organization ||= organization_id && account.organizations.find(organization_id)
      end
    end
  end
end
