module Zendesk
  module Tickets
    # A module for setting properties on a ticket in a controlled fashion. The properties may
    # need to get resolved to instances before getting set and may contain invalid data and may
    # put a ticket in an invalid state, in which case they should be rolled back. Hence this solution
    # rather than using #attributes=
    #
    # Properties will map :requester_id and :requester_email to :requester instances and like wise for
    # assignee and group.
    module Properties
      # Will set the properties passed on the ticket. If the ticket becomes invalid by doing so,
      # the ticket state is rolled back to how it was before the properties got set.
      def properties=(props)
        properties_not_applied = []

        if requester_can_not_be_changed?
          if props.key?(:requester_id)
            props.delete(:requester_id)
            properties_not_applied << :requester_id
          end

          if props.key?(:requester_email)
            props.delete(:requester_email)
            properties_not_applied << :requester_email
          end
        end

        props, properties_not_applied = Zendesk::Tickets::Properties.normalize(account, props, properties_not_applied)
        before = {}
        # Pull out the requester name because we don't want to use Ticket.requester_name= since there's magic attached
        requester_name = props.delete(:requester_name)

        props.keys.each do |key|
          before[key] = send(key)
          send("#{key}=", props[key])
        end

        # Special case requester_name for the reason above
        if requester_name.present?
          # If we were being really proper we'd set the before property. But we don't care enough
          # before[:requester_name] = self.requester.name
          requester.name = requester_name
          requester.save!
        end

        # We only check for validation if the ticket has a current user set. This is to allow people
        # using this method while building a ticket that may not be in its final state
        if current_user.present? && !valid?
          Rails.logger.warn("Ticket invalid after property update, reverting. Errors #{errors.inspect} self #{inspect}")
          # ... once again, we should handle requester_name differently but, in reality, we don't care
          # if before.key?(:requester_name)
          #   self.requester.name = before.delete(:requester_name)
          #   self.requester.save!
          # end
          before.keys.each do |key|
            send("#{key}=", before[key])
          end

          return [props, false]
        end

        [props, properties_not_applied.empty?]
      end

      # When you call the above method like `ticket.properties = {...}` you don't get access to the return value.
      # Using this alias you do.
      alias_method :apply_properties, :properties=

      # Turns value references into values
      def self.normalize(account, props, properties_not_applied = [])
        result = {}

        result[:set_tags]       = props[:set_tags]       if props.key?(:set_tags)
        result[:requester_name] = props[:requester_name] if props.key?(:requester_name)
        result[:ticket_type_id] = props[:ticket_type_id] if TicketType.fields.member?(props[:ticket_type_id])
        result[:priority_id]    = props[:priority_id]    if PriorityType.fields.member?(props[:priority_id])
        result[:status_id]      = props[:status_id]      if StatusType.fields.member?(props[:status_id])
        result[:locale_id]      = props[:locale_id]      if props.key?(:locale_id)

        if props[:requester_id]
          result[:requester] = account.users.find_by_id(props[:requester_id])
          properties_not_applied << :requester_id unless result[:requester]
        elsif props[:requester_email]

          unless user = account.find_user_by_email(props[:requester_email])
            # Extract the name part from the email address.
            # E.g. "Bill Gates <bill@gatesfoundation.org>" becomes "Bill Gates"
            name = Zendesk::Mail::Address.parse(props[:requester_email]).try(:name)
            name = "Unknown" if name.blank?

            # Create a new user
            user = account.users.new(email: props[:requester_email], name: name)
            user.send_verify_email = account.is_welcome_email_when_agent_register_enabled?
            user.save!
          end

          result[:requester] = user

          properties_not_applied << :requester_email unless result[:requester]
        elsif props[:requester_phone]
          props[:requester_phone] = ::Voice::Core::NumberSupport::E164Number.internal(props[:requester_phone])
          if phone_identity = account.user_identities.phone.find_by_value(props[:requester_phone])
            result[:requester] = phone_identity.user
          else
            name = props[:requester_name]
            name = props[:requester_phone] if name.nil?
            user = account.users.create(name: name)
            UserPhoneNumberIdentity.create(user: user, value: props[:requester_phone])
            result[:requester] = user
          end
          properties_not_applied << :requester_phone unless result[:requester]
        end

        if props[:assignee_id]
          result[:assignee] = account.agents.find_by_id(props[:assignee_id])
          properties_not_applied << :assignee_id unless result[:assignee]
        elsif props[:assignee_email]
          result[:assignee] = account.find_user_by_email(props[:assignee_email], scope: account.agents)
          properties_not_applied << :assignee_email unless result[:assignee]
        elsif props[:assignee_name]
          result[:assignee] = account.agents.where("name LIKE ?", "%#{props[:assignee_name]}%").first
          properties_not_applied << :assignee_name unless result[:assignee]
        end

        if props[:group_id]
          result[:group] = account.groups.find_by_id(props[:group_id])
          properties_not_applied << :group_id unless result[:group]
        elsif props[:group_name]
          result[:group] = account.groups.find_by_name(props[:group_name])
          properties_not_applied << :group_name unless result[:group]
        end

        # Currently there's no need to allow people to unset attributes
        result.keys.each do |key|
          result.delete(key) if result[key].nil?
        end

        [result, properties_not_applied]
      end

      private

      def requester_can_not_be_changed?
        if account.has_follower_and_email_cc_collaborations_enabled?
          !account.has_agent_can_change_requester_enabled? # Setting under Admin > Tickets
        else
          !new_record? && !account.is_collaboration_enabled?
        end
      end
    end
  end
end
