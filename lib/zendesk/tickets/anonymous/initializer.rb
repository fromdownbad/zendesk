module Zendesk
  module Tickets
    module Anonymous
      class Initializer < Tickets::Initializer
        APM_SERVICE_NAME = 'anonymous-requests'.freeze

        def anonymous_ticket
          @anonymous_ticket ||= begin
            ticket
            apply_anonymous_changes
          end
        end

        def identity
          data = @params[:ticket][:requester_data]
          data = data[:email] if HashParam.ish?(data)
          @identity ||= find_email_identity(data.to_s.strip)
        end

        ## TODO Is this the right way to ask if a user can log in?
        def can_login_to_verify?
          identity && identity.try(:is_verified?) && !identity.user.crypted_password.blank?
        end

        def apply_anonymous_changes
          @ticket.current_user = @ticket.submitter = @ticket.requester

          if @ticket.requester.suspended?
            @ticket.valid?
            # Ignore the error that the user is suspended but still surface other errors
            @ticket.errors.delete(:requester)
            if @ticket.errors.blank?
              return build_suspended_ticket(
                @account,
                @ticket,
                cause: SuspensionType.SPAM
              )
            else
              raise TicketInvalid, @ticket
            end
          end

          if !identity_is_verified? && requester_needs_verification?
            @ticket.fix_tags # Call this to populate ticket field entries for field taggers

            if @ticket.valid?
              build_suspended_ticket(
                @account,
                @ticket,
                cause: SuspensionType.SIGNUP_REQUIRED
              )
            else
              raise TicketInvalid, @ticket
            end
          else

            @ticket.add_flags!([EventFlagType.REGISTERED_USER_LOGGED_OUT])
            @ticket
          end
        end

        private

        # Security fix to block tickets created using unverified identities
        # on accounts where signup/verification is required, but only identities
        # that have tickets previously created.
        def identity_is_verified?
          return !!identity unless needs_to_sign_up?

          identity && (identity.try(:is_verified?) || requester_has_tickets?)
        end

        def find_email_identity(email)
          return unless EMAIL_PATTERN.match(email)
          @account.user_identities.find_by(type: UserEmailIdentity.name, value: email)
        end

        def needs_to_sign_up?
          @account.is_signup_required?
        end

        def needs_to_sign_in?
          @user.is_anonymous_user?
        end

        def requester_has_tickets?
          @requester_has_tickets ||= @ticket.requester.tickets.exists?
        end

        def requester_needs_verification?
          if requester_has_verified_identities?
            needs_to_sign_in?
          else
            needs_to_sign_up?
          end
        end

        def requester_has_verified_identities?
          @requester_has_verified_identities ||= @ticket.requester.identities.email.verified.any?
        end

        def build_suspended_ticket(account, ticket, options = {})
          cause = options.fetch(:cause, SuspensionType.SIGNUP_REQUIRED)

          suspended_ticket = ticket.suspend(cause)
          suspended_ticket.properties ||= {}
          suspended_ticket.properties[:locale_id] = ticket.requester.locale_id
          suspended_ticket.properties[:ticket_form_id] = ticket.ticket_form_id

          # Record the custom field entries on the suspended ticket
          ticket.ticket_field_entries.each do |ticket_field_entry|
            suspended_ticket.properties[:fields] ||= {}
            suspended_ticket.properties[:fields][ticket_field_entry.ticket_field_id] = ticket_field_entry.value
          end

          # Record the editable ticket system properties on the suspended ticket - type and priority
          account.ticket_fields.properties.active.visible_in_portal.editable_in_portal.each do |field|
            next unless field.value(ticket).present?
            suspended_ticket.properties[:priority_id] = ticket.priority_id if field.is_a?(FieldPriority)
            suspended_ticket.properties[:ticket_type_id] = ticket.ticket_type_id if field.is_a?(FieldTicketType)
          end

          if @system_metadata && @system_metadata[:client].present? && @system_metadata[:ip_address].present?
            add_client_info_from_hash(suspended_ticket,
              user_agent: @system_metadata[:client],
              remote_ip: @system_metadata[:ip_address])
          end

          suspended_ticket
        end
      end
    end
  end
end
