module Zendesk
  module Tickets
    module Anonymous
      class TicketInvalid < ActiveRecord::RecordInvalid; end
    end
  end
end
