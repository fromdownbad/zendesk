module Zendesk
  module Tickets
    module Anonymous
      module ControllerSupport
        include Zendesk::Auth::InternalRequest

        def self.included(base)
          base.before_action :validate_rate_limit, only: :create, if: :anonymous_request?, unless: :internal_request?
        end

        protected

        def anonymous_ticket_initializer
          @anonymous_ticket_initializer ||= Zendesk::Tickets::Anonymous::Initializer.new(current_account,
            current_user, params, request_params: request_params, include: ticket_finder_includes)
        end

        def initialized_anonymous_ticket
          @initialized_anonymous_ticket ||= begin
            ticket = anonymous_ticket_initializer.anonymous_ticket

            case ticket
            when Ticket
              ticket if ensure_access_to_ticket(ticket)
            when SuspendedTicket
              ticket
            end
          end
        end

        def anonymous_request?
          current_user.is_anonymous_user?
        end

        def validate_rate_limit
          if anonymous_requests_throttled?
            Rails.logger.info("Anonymous request throttled for #{self.class} account: #{current_account.subdomain} ip: #{request.remote_ip}")
            return unless current_account.has_anonymous_requests_throttled? || current_account.subscription.is_trial? || from_web_widget?
          end
          anonymous_requests_throttled!
        end

        def anonymous_requests_throttled?
          Prop.throttled?(anonymous_rate_limit_key, [current_account.id, request.remote_ip], threshold: current_account.settings.anonymous_request_threshold)
        end

        def anonymous_requests_throttled!
          Prop.throttle!(anonymous_rate_limit_key, [current_account.id, request.remote_ip], threshold: current_account.settings.anonymous_request_threshold)
        end

        def anonymous_rate_limit_key
          if from_web_widget?
            :anonymous_web_widget_ticket_requests
          else
            :anonymous_requests
          end
        end

        def from_web_widget?
          params[:request].try(:[], :via_id) == ViaType.WEB_WIDGET
        end
      end
    end
  end
end
