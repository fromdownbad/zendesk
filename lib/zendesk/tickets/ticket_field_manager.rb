module Zendesk::Tickets
  # Manages ticket fields on an account.
  #
  class TicketFieldManager < Zendesk::CustomField::FieldManager
    self.raw_parameters = ["description", "title", "title_in_portal"].freeze

    def self.type_map
      {
        "checkbox" => FieldCheckbox.name,
        "partialcreditcard" => FieldPartialCreditCard.name,
        "partial_credit_card" => FieldPartialCreditCard.name, # kept for backwards compatibility [BOX-609]
        "date"     => FieldDate.name,
        "decimal"  => FieldDecimal.name,
        "integer"  => FieldInteger.name,
        "regexp"   => FieldRegexp.name,
        "text"     => FieldText.name,
        "textarea" => FieldTextarea.name,
        "tagger"   => FieldTagger.name,
        "multiselect" => FieldMultiselect.name
      }
    end

    def self.boolean_attr_list
      @boolean_attr_list ||= [:active, :required, :collapsed_for_agents, :visible_in_portal, :editable_in_portal, :required_in_portal]
    end

    def self.default_type
      FieldText
    end
  end
end
