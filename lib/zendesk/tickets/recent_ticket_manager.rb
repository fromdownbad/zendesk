require 'zendesk/tickets/recent_ticket_store'

module Zendesk::Tickets
  # Manage recently viewed tickets for a user.
  #
  # A list of recently viewed tickets is stored for each user. By default the Rails cache is
  # used for storage, which means that persistence is not guaranteed.
  #
  class RecentTicketManager
    # Create a new recent ticket manager.
    #
    # user - the User whose recent ticket list should be managed.
    #
    # Options
    #
    # :store - an object that can persist the recent ticket list between sessions. Must
    #          respond to #read and #write (optional).
    #
    def initialize(user, options = {})
      raise ArgumentError if user.nil?

      @user = user
      @store = options[:store] || RecentTicketStore.new(user)
    end

    # Get the recent ticket list.
    #
    # Examples
    #
    #   manager.recent_tickets
    #   # => [#<Ticket id: 1, ...>, #<Ticket id: 2, ...>]
    #
    # Returns an Array of full ticket objects.
    def recent_tickets
      ticket_ids = store.read

      Rails.logger.debug("RecentTicketManager recent_tickets returned: #{ticket_ids}")

      @user.account.tickets.where(id: ticket_ids).to_a.sort_by do |ticket|
        ticket_ids.index(ticket.id)
      end
    end

    # Add a ticket to the recent ticket list.
    #
    # ticket - the Ticket that should be added to the list.
    #
    # Returns nothing.
    def add_ticket(ticket)
      list = recent_tickets_except(ticket)
      list.unshift(ticket.id)
      list = list[0...5]

      Rails.logger.debug("RecentTicketManager add_ticket writing: #{list}")

      store.write(list)

      nil
    end

    # Remove a ticket from the recent ticket list.
    #
    # ticket - the Ticket to remove, or the Integer id of the ticket to remove.
    #
    # Returns nothing.
    def remove_ticket(ticket)
      list = recent_tickets_except(ticket)

      Rails.logger.debug("RecentTicketManager remove_ticket writing: #{list}")

      store.write(list)

      nil
    end

    def cache_key
      [@user, store.read.first]
    end

    private

    attr_reader :store

    def recent_tickets_except(ticket)
      store.read.reject { |r| r == ticket.id }
    end
  end
end
