module Zendesk
  module Tickets
    module Comments
      class Initializer < Tickets::Initializer
        def initialize(user, params)
          @user = user
          @params = params
        end

        def normalize
          return nil unless @params.respond_to?(:key?)

          if @params.key?(:value)
            @params[:body] ||= @params.delete(:value)
          end

          normalize_boolean(@params, :add_short_url)

          if @params.key?(:public)
            @params[:is_public] = @params.delete(:public)
          end

          normalize_boolean(@params, :is_public)

          raise Event::ValueTooLarge if block_large_body?(@user, @params[:body])

          @params
        end

        alias :comment :normalize

        private

        def block_large_body?(user, body)
          return false unless user&.account&.has_block_large_comment_body_on_initialize?
          return false unless body

          body.to_s.bytesize > Event::MAX_VALUE_BYTESIZE
        end
      end
    end
  end
end
