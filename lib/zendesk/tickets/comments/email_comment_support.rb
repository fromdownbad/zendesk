module Zendesk::Tickets::Comments::EmailCommentSupport
  REQUEST_TIMEOUT = 30.seconds.freeze
  SUPPORTED_VIA_TYPES = [ViaType.CLOSED_TICKET, ViaType.MAIL].freeze

  protected

  def supported_via_type?
    SUPPORTED_VIA_TYPES.include?(comment&.via_id)
  end

  def should_report_mail_rendering_issue?
    JSON.parse(comment.audit.value_previous).dig('custom', 'z1_request_url').nil?
  rescue JSON::ParserError
    true
  end

  def create_dev_comment_on_ticket(ticket_id)
    internal_api_client.put(
      "api/v2/tickets/#{ticket_id}",
      ticket: { comment: { body: dev_comment_body, public: false } }
    )
  end

  def report_mail_rendering_issue
    internal_api_client.post(
      'api/v2/requests',
      request: {
        subject: reply_parser_report_message,
        comment: { body: issue_description },
        requester: current_user
      }
    )
  end

  def dev_comment_body
    body = []

    body << "Subdomain: `#{current_account.subdomain}`"
    body << "JSON email identifier: `#{json_email_identifier}`" if json_email_identifier.present?
    body << "Raw email identifier: `#{raw_email_identifier}`" if raw_email_identifier.present?
    body << "Message-Id: `#{message_id}`" if message_id.present?
    body << "Monitor: #{monitor_url}" if monitor_url.present?
    body << "Ticket: #{ticket_url}"
    body << "Z1 query: #{z1_query_url}"
    body << datadog_links
    body << console_classic_commands

    body.compact.join("\n\n")
  end

  def ticket_url
    Addressable::URI.new(
      scheme: 'https',
      host: "#{current_account.subdomain}.#{ENV['ZENDESK_HOST']}",
      path: "/agent/tickets/#{params[:ticket_id]}"
    ).to_s
  end

  def console_classic_commands
    if json_email_identifier.present? || raw_email_identifier.present?
      ["Console Classic:\n\n```\nssh dba#{current_account.pod_id}\n"].tap do |arr|
        arr << "console_classic\n\n"
        arr << "s3_email_download_url(\"#{json_email_identifier}\")\n" if json_email_identifier.present?
        arr << "s3_email_download_url(\"#{raw_email_identifier}\")\n" if raw_email_identifier.present?
        arr << "\naccount = #{current_account.id}.to_account\n"
        arr << "account.shard\n"
        arr << "comment = account.tickets.find_by_nice_id(#{params[:ticket_id]}).events.find(#{params[:id]})\n"
        arr << "raw_email_json = Yajl::Parser.parse(comment.audit.raw_email.json, check_utf8: false).with_indifferent_access\n\n"
        arr << "html_reply = raw_email_json[\"processing_state\"][\"content\"][\"html_reply\"]\n"
        arr << "html_quoted = raw_email_json[\"processing_state\"][\"content\"][\"html_quoted\"]\n"
        arr << "html_full = raw_email_json[\"processing_state\"][\"content\"][\"html_full\"]\n"
        arr << "plain_reply = raw_email_json[\"processing_state\"][\"content\"][\"plain_reply\"]\n"
        arr << "plain_quoted = raw_email_json[\"processing_state\"][\"content\"][\"plain_quoted\"]\n"
        arr << "plain_full = raw_email_json[\"processing_state\"][\"content\"][\"plain_full\"]\n"
        arr << "```"
      end.join
    end
  end

  def datadog_links
    start_ms, end_ms = datadog_time_range

    [
      "Datadog MTC: #{datadog_url(datadog_query_values(start_ms, end_ms, 'mail_ticket_creator'))}",
      "Datadog MPQ: #{datadog_url(datadog_query_values(start_ms, end_ms, 'mail_conveyor'))}",
      "Datadog Chronicles: #{datadog_url(datadog_query_values(start_ms, end_ms, 'incoming_email_json'))}"
    ]
  end

  def datadog_time_range(interval = 4.weeks)
    now = DateTime.now
    [(now - interval).strftime('%Q'), now.strftime('%Q')]
  end

  def datadog_url(query_values)
    Addressable::URI.new(
      scheme: 'https',
      host: 'zendesk.datadoghq.com',
      path: '/logs',
      query_values: query_values
    ).to_s
  end

  def datadog_query_values(start_ms, end_ms, service)
    {
      cols: 'core_host,core_service,event',
      from_ts: start_ms,
      index: Rails.env.production? ? 'support-mail-processing' : 'staging',
      live: true,
      messageDisplay: 'inline',
      query: "@message_id:#{message_id.gsub(/[<>]/, '')} +service:#{service}",
      stream_sort: 'asc',
      to_ts: end_ms,
    }
  end

  def z1_query_url
    Addressable::URI.new(
      scheme: 'https',
      host: "support.#{Zendesk::Configuration['host']}",
      path: '/agent/search/1',
      query_values: {
        q: "\"Reply Parser Issue - account_id:#{current_account.id}\" tags:no_delimiter_rendering_issues order_by:created sort:desc"
      }
    ).to_s
  end

  def monitor_url
    Addressable::URI.new(
      scheme: 'https',
      host: Rails.env.production? ? 'monitor.zende.sk' : 'monitor.zendesk-staging.com',
      path: "/accounts/#{current_account.id}/overview"
    ).to_s
  end

  def update_audit_metadata!(metadata)
    comment.audit.set_custom_metadata!(metadata)
  end

  def prepare_full_body_from_raw_email
    json = raw_email&.full_message_body_json

    raise ActiveRecord::RecordNotFound if json.nil?

    processed_content = Yajl::Parser.parse(json, check_utf8: true).with_indifferent_access

    css = nil
    if current_account.settings.rich_content_in_emails? && comment.rich?
      html = processed_content[:processing_state][:content][:html_quoted]
      doc = Nokogiri::HTML5.parse(html, max_tree_depth: Zendesk::InboundMail::HTMLDocument::MAX_NOKOGIRI_TREE_DEPTH)
      css = doc.css('style').inner_html

      comment.temp_html_body = doc.css('body').children.to_html
    else
      comment.temp_plain_body = processed_content[:processing_state][:content][:plain_quoted]
    end
    comment.html_body(css: css)
  end

  def raw_email
    @raw_email ||= comment&.audit&.raw_email
  end

  def internal_api_client
    Kragle.new('classic') do |conn|
      conn.url_prefix = "https://support.#{Zendesk::Configuration['host']}"
      conn.headers['Content-Type'] = 'application/json'
      conn.options.timeout = REQUEST_TIMEOUT
    end
  end

  def reply_parser_report_message
    "Reply Parser Issue - account_id: #{current_account.id}, ticket_id: #{params[:ticket_id]}, comment_id: #{params[:id]}"
  end

  def issue_description
    description = ['There is an reply parser issue with the following ticket comment:']
    description << "account_id: #{current_account.id}"
    description << "ticket_id: #{params[:ticket_id]}"
    description << "comment_id: #{params[:id]}"
    description << params[:description] if params[:description].present?
    description.join("\n\n")
  end

  def message_id
    params[:message_id]
  end

  def json_email_identifier
    params[:json_email_identifier]
  end

  def raw_email_identifier
    params[:raw_email_identifier]
  end
end
