module Zendesk
  module Tickets
    module Channels
      # To be used only when a new comment is being created
      def create_channel_back_event
        return unless comment.should_create_channel_back_event?
        validate_and_create_channel_back_event
      end

      # Creates an audit event that sends the message out to the external channel.
      # @options:
      #   text: The message to be sent out. Will default to the comment body if a comment is present.
      #   via_id: The via_id of the channel to which this message will be sent
      #   update_via_id: The via_id by which this update was made eg. rule, web form.
      def validate_and_create_channel_back_event(options = {})
        options[:text] ||= comment.try(:body)
        author = comment.try(:author) || User.system

        outgoing_text = Zendesk::Liquid::TicketContext.render(
          options[:text], self, author, false, requester.translation_locale
        )

        # This should eventually be an API call to the channels service
        data = reply_data(outgoing_text, options[:via_id] || via_id)

        if data[:error]
          errors.add(:base, data[:error])
          return false
        end

        if comment && options[:update_via_id] != ViaType.RULE
          comment.set_liquidized_body(data[:metadata][:text])
        end

        append_channel_back_event(data, options)
      end

      # TODO: Channels. Remove method, only confuses the extraction below
      def reply_parameters_validator(outgoing_text, channel_via_id)
        ::Channels::Converter::ReplyParametersValidator.new(
          validator_params(outgoing_text, channel_via_id)
        )
      end

      def deactivate_event_decoration
        first_comment.event_decoration.deactivate
      end

      def twitter_source
        if ViaType.TWITTER_DM == via_id
          # For Direct Messages, we want to always use the default source
          channel_source_id = ticket_source_id
        else
          channel_source_id = comment.channel_source_id
          channel_source_id ||= ticket_source_id
        end
        MonitoredTwitterHandle.where(account_id: account.id).active.find_by_id(channel_source_id)
      end

      def twitter_ticket_source(action = '')
        via_twitter? && ticket_source(action)
      end

      def ticket_source(action = '')
        @ticket_source ||= begin
          handles = action == 'follow!' ? enabled_twitter_handles : reply_enabled_twitter_handles
          handles.find { |h| h.id == ticket_source_id } || handles.first
        end
      end

      def enabled_twitter_handles
        @enabled_twitter_handles ||= MonitoredTwitterHandle.where(account_id: account.id).active
      end

      def reply_enabled_twitter_handles
        @reply_enabled_twitter_handles ||= MonitoredTwitterHandle.where(account_id: account.id).active.select(&:can_reply?)
      end

      def ticket_source_id
        latest_decoration = event_decorations.order('created_at desc').first
        latest_decoration && latest_decoration.data.source.zendesk_id
      end

      def handle_reply_options
        if comment.reply_option
          # TODO: Remember to do correct thing to invoke or cancel email trigger
          # NOTE: This is a temporary proof-of-concept implementation.  It's obviously incomplete.  It's intended
          # to allow clients to play around with reply_option as described at
          # https://docs.google.com/document/d/1aUOcPAlcV2pj72oGcz6wUCeKGyKyV1dC9fHbXhQIFgA/edit?pli=1
          raise StandardError, "reply_option found but not currently supported: #{comment.reply_option.to_json}"
        else
          # Fall back to old behavior
          create_channel_back_event
          create_sms_response if should_create_sms_response?
        end
      end

      private

      def append_channel_back_event(data, options)
        options[:via_id] ||= via_id

        event_params = {
          ticket: self,
          source_id: data[:source_id],
          via_id: options[:update_via_id],
          value: data[:metadata]
        }

        # If the update was via a rule action (e.g. tweet requester), then the ticket needs to spawn a
        # new thread on twitter since there is no existing tweet to reply to.
        event_params[:new_thread] = true if options[:update_via_id] == ViaType.RULE

        event_params[:append_ticket_url] = comment.try(:add_short_url) if via_twitter?

        @audit.events << ChannelBackEvent.new(event_params)
      end

      def reply_data(outgoing_text, channel_via_id)
        reply_parameters_validator(outgoing_text, channel_via_id).validate
      end

      def validator_params(outgoing_text, channel_via_id)
        {
          account_id: account.id,
          ticket_id: id,
          via_id: channel_via_id,
          source_id: comment.try(:channel_source_id),
          requester_id: requester.id,
          text: outgoing_text
        }
      end
    end
  end
end
