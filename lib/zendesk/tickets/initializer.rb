module Zendesk
  module Tickets
    class Initializer
      include Zendesk::Tickets::ClientInfo
      extend Zendesk::Tickets::RequesterDataParser

      attr_reader :account

      VOICE_VIA_TYPES = [ViaType.VOICEMAIL, ViaType.API_VOICEMAIL, ViaType.API_PHONE_CALL_INBOUND,
                         ViaType.API_PHONE_CALL_OUTBOUND, ViaType.PHONE_CALL_INBOUND, ViaType.PHONE_CALL_OUTBOUND].freeze

      OVERRIDABLE_VIA_TYPES = [ViaType.CHAT, ViaType.WEB_SERVICE, ViaType.LOTUS,
                               ViaType.GET_SATISFACTION, ViaType.CLOSED_TICKET, ViaType.MOBILE_SDK, ViaType.SATISFACTION_PREDICTION,
                               ViaType.WEB_WIDGET, ViaType.ANSWER_BOT].freeze

      ACCEPTED_STATUS_TYPES = [StatusType.NEW, StatusType.OPEN, StatusType.PENDING, StatusType.HOLD, StatusType.SOLVED, StatusType.CLOSED].freeze

      APM_SERVICE_NAME = 'tickets-initializer'.freeze

      def initialize(account, user, params, options = {})
        @account = account
        @user    = user
        @options = options
        @request_params, @params = normalize_request_and_params(options.delete(:request_params), params, user)
        @via_id = @params[:ticket].delete(:via_id)
        @original_via_id = @params[:ticket].delete(:original_via_id)
        @metadata = @params[:ticket].delete(:metadata)
        @system_metadata = @params[:ticket].delete(:system_metadata)
        updated_stamp = @params[:ticket].delete(:updated_stamp)
        @safe_update_timestamp = updated_stamp if @params[:ticket].delete(:safe_update)
      end

      def ticket
        return @ticket if @ticket.present?

        @ticket = find_ticket

        filter_ticket_params(@ticket)

        apply_changes

        @ticket
      end

      def solved_ticket?
        did_solve = @previous_status_id != ticket.status_id
        did_solve &&= ![StatusType.SOLVED, StatusType.CLOSED].include?(@previous_status_id.to_i)
        did_solve && [StatusType.SOLVED, StatusType.CLOSED].include?(ticket.status_id)
      end

      def self.set_rating(ticket, rating_params)
        score = rating_params[:score].to_s.downcase
        ticket.satisfaction_score = Api::V2::Tickets::AttributeMappings::PARAMS_SATISFACTION_MAP[score]
        ticket.satisfaction_comment = rating_params.fetch(:comment, "")
        reason_code = rating_params.fetch(:reason_code, nil)
        ticket.apply_satisfaction_reason_code(reason_code) if reason_code
      end

      def find_ticket
        if @params[:ticket_object].is_a?(Ticket)
          ticket = @params[:ticket_object]
        elsif @params[:id].present?
          ticket = find_ticket_by_id(@params[:id], @options)
        elsif @params[:suspended].present?
          suspended_ticket = account.suspended_tickets.find(@params[:suspended])
          ticket = suspended_ticket.to_ticket
          ticket.suspended_ticket = suspended_ticket
        else
          ticket = account.tickets.build
          ticket.via_id = @via_id
          ticket.original_via_id = @original_via_id
        end

        if ticket.new_record?
          if @params[:ticket][:entry_id].present?
            entry = account.entries.find(@params[:ticket][:entry_id])
            @params[:ticket][:subject] = entry.title if @account.field_subject.is_active?
          end
        else
          # Silently ignore updates to read-only attributes
          @params[:ticket].delete(:created_at)
          @params[:ticket].delete(:description)
          @params[:ticket].delete(:submitter_id)
        end

        ticket.current_user = @user

        ticket
      end

      def find_ticket_by_id(id, options)
        account.tickets.find_by_nice_id!(id, options)
      end

      protected

      def normalize_boolean(params, name)
        return if params[name].nil?
        params[name] = to_boolean(params[name])
      end

      private

      def on_behalf_of_user?
        @request_params && @user.id && @request_params.has_x_on_behalf_of_header?
      end

      def end_user_can_add_collaborators?
        !@account.is_collaborators_addable_only_by_agents?
      end

      # There's some complex logic alters the list of allowed ticket keys.
      # Keep this updated if there is any new key added into the RequestsController whitelist.
      def filter_ticket_params(ticket)
        return unless @user.is_end_user?

        allowed_ticket_keys = [
          :description, :subject, :fields, :ticket_field_entries, :via_followup_source_id,
          :solved, :force_status_change, :ticket_form_id, :organization_id, :brand_id,
          :tags, :original_recipient_address, :attribute_value_ids
        ]

        if @user.is_anonymous_user?
          allowed_ticket_keys += [:requester_data]
        else
          @params.delete(:email)
        end

        allowed_ticket_keys << :created_at  if on_behalf_of_user?
        allowed_ticket_keys << :priority_id if editable_field?(@account.field_priority)
        allowed_ticket_keys << :via if @account.has_agent_as_end_user_allow_anonymous_via?

        if @account.has_follower_and_email_cc_collaborations_enabled?
          if end_user_can_add_collaborators?
            allowed_ticket_keys << :set_collaborators << :additional_collaborators
            if @account.has_comment_email_ccs_allowed_enabled?
              allowed_ticket_keys << :set_email_ccs << :additional_collaborators_with_email_cc_type << :set_collaborators_with_email_cc_type
            end
          end
        else
          if end_user_can_add_collaborators?
            allowed_ticket_keys << :set_collaborators << :additional_collaborators << :set_followers << :set_email_ccs
          end
        end

        if editable_field?(@account.field_ticket_type)
          allowed_ticket_keys += [:ticket_type_id, :due_date, :'due_date(1i)', :'due_date(2i)', :'due_date(3i)']
        end

        # Removes all but allowed ticket keys
        @params[:ticket].slice!(*allowed_ticket_keys)
        @params[:comment].slice!(:uploads, :value, :body, :html_body, :is_public) if @params[:comment]

        if ticket.assignee.present? && (@params[:ticket][:force_status_change] || @params[:ticket][:solved])
          @params[:ticket][:status_id] = Zendesk::Types::StatusType.SOLVED
          @params[:ticket][:force_status_change] = true
        end
      end

      def apply_changes
        @previous_status_id ||= @ticket.status_id || 0
        apply_changes_from_params

        @ticket.audit.try(:apply_metadata, :custom, @metadata)
        @ticket.audit.try(:apply_metadata, :system, @system_metadata) if internal_client?(@request_params)

        if @params[:ticket].key?(:sharing_agreements)
          @sharing_agreements ||= []
          defunct_agreements = @ticket.shared_tickets.map(&:agreement_id)

          @sharing_agreements.each do |agreement|
            next unless agreement[:id].present?
            id = agreement[:id].to_i

            @ticket.add_sharing_event(id, agreement[:custom_fields])
            defunct_agreements.delete(id)
          end

          if defunct_agreements.any?
            shared_tickets = @ticket.shared_tickets.where(agreement_id: defunct_agreements).to_a
            @ticket.unshare(shared_tickets)
          end
        end

        if @safe_update_timestamp.present?
          @ticket.safe_update_timestamp = @safe_update_timestamp
        end
      end

      def apply_changes_from_params
        ticket_params = @params[:ticket].dup

        via                 = ticket_params.delete(:via)
        requester_data      = ticket_params.delete(:requester_data)
        ticket_fields       = filtered_ticket_fields(ticket_params.delete(:fields))
        @sharing_agreements = ticket_params.delete(:sharing_agreements)
        req = ticket_params.delete(:request)
        @request_params ||= Zendesk::Tickets::HttpRequestParameters.new(req)
        satisfaction_probability = ticket_params.delete(:satisfaction_probability)

        if @account.has_log_updating_ticket_tags_without_ticket_tagging?
          unless @account.settings.ticket_tagging?
            ZendeskAPM.trace(
              'without-ticket-tagging',
              service: APM_SERVICE_NAME
            ) do |span|
              span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
              span.set_tag("zendesk.account_id", @account.id)
              span.set_tag("zendesk.account_subdomain", @account.subdomain)
              span.set_tag("zendesk.ticket_tagging_fix", @account.has_ticket_tagging_fix?)
              span.set_tag("zendesk.user_can_edit_tags", @user.can?(:edit_tags, @ticket))
              span.set_tag("zendesk.new_record", @ticket.new_record?)
              span.set_tag("zendesk.ticket_params.via_id", @via_id)
              span.set_tag("zendesk.ticket_params.tags", ticket_params[:tags])
              span.set_tag("zendesk.ticket_params.set_tags", ticket_params[:set_tags])
              span.set_tag("zendesk.ticket_params.additional_tags", ticket_params[:additional_tags])
              span.set_tag("zendesk.ticket_params.remove_tags", ticket_params[:remove_tags])
              adjusted_tags = [
                ticket_params[:tags],
                ticket_params[:set_tags],
                ticket_params[:additional_tags],
                ticket_params[:remove_tags]
              ].any?(&:present?)
              span.set_tag("zendesk.ticket_params.adjusted_tags", adjusted_tags)
            end
          end
        end

        apply_tag_changes_from_params(ticket_params)

        ticket_params.delete(:original_recipient_address) if @user.is_light_agent? && @params[:id].present?
        ticket_params.delete(:ticket_form_id) unless @account.has_ticket_forms?
        ticket_params.delete(:tags) unless @user.can?(:edit_tags, @ticket) || @ticket.new_record?
        ticket_params.delete(:tags) if !@account.settings.ticket_tagging? && @account.has_ticket_tagging_fix?

        ticket_params.delete(:attribute_value_ids) unless @user.can?(:edit_attribute_values, Ticket)

        # Ignore ticket field changes for users that can't edit the ticket properties.
        ticket_fields = {} unless @user.can?(:edit_properties, @ticket) || @ticket.new_record?

        # Ignore system field changes for users that can't edit the ticket properties.
        if @account.has_system_fields_permission_check?
          unless @user.can?(:edit_properties, @ticket) || @ticket.new_record?
            ticket_params.delete(:status_id)
            ticket_params.delete(:priority_id)
            ticket_params.delete(:ticket_type_id)
          end
        end

        # Do not allow requester changes to an existing ticket unless relevant settings are enabled
        # https://zendesk.atlassian.net/browse/EM-3011
        is_existing_ticket = @params.key?('id') || @params.key?(:id)

        # Do not allow requester changes if all collaboration is disabled.
        #   * The `is_collaboration_enabled?` only applies for legacy collaboration (before CCs/Followers)
        #   * The `agent_can_change_requester` only applies for CCs/Followers.
        if is_existing_ticket && !@account.has_agent_can_change_requester_enabled? && !@account.is_collaboration_enabled?
          ticket_params.delete(:requester_id)
          requester_data = nil
        end

        if requester_data.present?
          # if we have both email and id, trust email
          ticket_params.delete(:requester_id)
          @ticket.requester_data = requester_data
        end

        if @params[:ticket][:assignee_id].present? && @params[:ticket][:assignee_email].present?
          ticket_params.delete(:assignee_email)
        end

        tde_workspace_param = ticket_params.delete(:tde_workspace)

        # The initializer often receives ticket params that are not valid Ticket attributes. Previously, the protected_attributes gem silently dropped
        # these excess params. Now we must remove them from the params used to create/update a ticket.
        excess_ticket_params = [ # please keep these sorted
          :email_ccs,
          :followers,
          :id,
          :macro_id,
          :macro_ids,
          :raw_subject,
          :url,
        ] +
          Ticket.reflect_on_all_associations.map(&:name) +
          Ticket.ignored_columns

        @ticket.attributes = ticket_params.except(*excess_ticket_params)

        @ticket.macro_ids = begin
          Array(
            ticket_params[:macro_ids] ||
              ticket_params[:macro_id] ||
              @params[:macro_applied]
          )
        end

        if via
          if via[:channel].present?
            channel_via = if via[:channel].to_s =~ /\d+/ && Zendesk::Types::ViaType[via[:channel]]
              via[:channel].to_i
            else
              Zendesk::Types::ViaType.fuzzy_find(via[:channel])
            end

            # In the case of followup tickets we want to keep the currently set @via_id of ViaType.CLOSED
            # or things can get weird later. We can set the original_via_id to be what the user sent us in the params
            if Arturo.feature_enabled_for?(:override_via_for_followups, @account) && @ticket.via_followup_source_id.present? && @ticket.new_record?
              @ticket.original_via_id = channel_via if channel_via
            else
              # Warning: this as is will blow away the previously set @via_id.
              @via_id = channel_via ? channel_via : @via_id # rubocop:disable Style/RedundantCondition
            end

            unless channel_via
              Rails.logger.info("Could not find via channel \"#{via[:channel]}\", setting via_id to #{@via_id}")
              statsd_client.increment('channel.via.invalid')
            end
          end

          @via_reference_id = via[:source] if via[:source]
        end

        if satisfaction_probability && @account.has_satisfaction_prediction_enabled?
          @ticket.satisfaction_probability = satisfaction_probability
        end

        apply_followup if @ticket.new_record?

        if @params[:comment].present?
          @params[:comment][:is_public] = Zendesk::CommentPublicity.is_comment_public?(@user, @params[:comment][:is_public], @ticket, @via_id)
          @ticket.add_comment(@params[:comment])
          @ticket.comment.via_id = @via_id
        elsif @params[:voice_comment].present?
          @ticket.add_voice_api_comment(@params[:voice_comment])
          @ticket.comment.via_id = @via_id
        end

        if @ticket.new_record?
          @ticket.requester ||= @user

          @ticket.via_id      = @via_id
          @ticket.via_reference_id = @via_reference_id

          @ticket.current_user = @user.is_anonymous_user? ? @ticket.requester : @user

          if @ticket.submitter.nil? || @ticket.submitter.is_anonymous_user? || !@ticket.submitter.can?(:edit, @ticket)
            @ticket.submitter = default_submitter(@ticket)
          end

          if @ticket.comment
            @ticket.comment.body     ||= @ticket.description
            @ticket.description      ||= @ticket.comment.body

            @ticket.comment.created_at = @ticket.created_at
          end
        else
          if @ticket.comment
            @ticket.comment.author ||= @user
            @ticket.comment.body || @ticket.comment.html_body
          end
        end

        # TODO: Move the client info setting to the metadata
        if @request_params
          case @request_params
          when Hash, ActionController::Parameters
            add_client_info_from_hash(@ticket, @request_params)
          when Zendesk::Tickets::HttpRequestParameters
            add_client_info_from_request(@ticket, @request_params)
          end
        end

        # we need to set the audit before setting ticket fields because we potentially create a TicketFieldRedactEvent
        @ticket.will_be_saved_by(@ticket.current_user, via_id: @via_id, via_reference_id: @via_reference_id)

        if @account.has_contextual_workspaces_in_js?
          @ticket.add_contextual_workspace_audit_event(tde_workspace_param)
        end

        @ticket.fields = ticket_fields

        @ticket.attribute_value_ids = ticket_params[:attribute_value_ids] if ticket_params.key?(:attribute_value_ids)
      end

      # gets overridden by Zendesk::Tickets::V2::Initializer
      def default_submitter(ticket)
        ticket.current_user
      end

      def apply_tag_changes_from_params(params)
        # handle some ordered hash wackiness
        tags = params.delete(:set_tags)
        @ticket.set_tags = tags unless tags.nil?

        tags = params.delete(:additional_tags)
        @ticket.additional_tags = tags unless tags.nil?

        tags = params.delete(:remove_tags)
        @ticket.remove_tags = tags unless tags.nil?
      end

      def apply_followup
        @ticket.set_followup_source(@user)

        if (source_ticket = @ticket.followup_source)
          @via_reference_id = source_ticket.id
          @ticket.requester ||= source_ticket.requester
          locale_followup = @ticket.requester.translation_locale

          unless @params[:ticket].key?(:set_collaborators) || @params[:ticket].key?(:set_email_ccs) || @params[:ticket].key?(:set_followers)
            legacy_cc_ids = source_ticket.collaborations.legacy_ccs.map(&:user_id)

            if legacy_cc_ids.empty?
              email_cc_ids = source_ticket.collaborations.email_ccs.map(&:user_id)
              @ticket.set_email_ccs = @ticket.user_ids_to_collaborators_hashes(email_cc_ids) if email_cc_ids.present?

              follower_ids = source_ticket.collaborations.followers.map(&:user_id)
              @ticket.set_followers = @ticket.user_ids_to_collaborators_hashes(follower_ids) if follower_ids.present?
            else
              @ticket.set_collaborators = legacy_cc_ids
            end
          end

          if @account.field_subject.is_active?
            @ticket.subject ||= ::I18n.t('ticket.followup.subject', source_ticket_subject: source_ticket.subject.to_s, locale: locale_followup).to_s
          end

          @ticket.description.present? ? "\n\n #{@ticket.description}" : ""
        end
      end

      def private_comment_present?(params)
        # params[:comment][:public] is used here as this is called before the comments params normalization
        # after normalization, params[:comment][:public] becomes params[:comment][:is_public]

        comment_public = if params.dig(:comment).respond_to?(:dig)
          params.dig(:comment, :public)
        elsif params.dig(:ticket, :comment).respond_to?(:dig)
          params.dig(:ticket, :comment, :public)
        end

        ['0', 'false', false, 0].include?(comment_public)
      end

      def normalize_request_and_params(request_params, params, user)
        params[:ticket] ||= params.delete(:request) || {}
        params = Api::V2::Tickets::TicketParams.new(params).to_hash

        params[:ticket][:entry_id] = params[:entry_id] if params[:entry_id].present?
        params.delete(:entry_id)

        params[:ticket][:requester_id] ||= params[:requester_id] if params[:requester_id].present?
        params.delete(:requester_id)

        if @account.has_follower_and_email_cc_collaborations_enabled?
          if params[:ticket].key?(:followers)
            params[:ticket][:set_followers] = normalize_collaborators(params[:ticket].delete(:followers))
          end

          if @account.has_email_ccs_update_for_private_comment_fix?
            if private_comment_present?(params) && params[:ticket].key?(:email_ccs)
              params[:ticket].delete(:email_ccs)
            end
          end

          if params[:ticket].key?(:email_ccs) && user.can?(:publicly, Comment)
            normalized_email_ccs = normalize_collaborators(params[:ticket].delete(:email_ccs))

            check_max_email_cc_count(normalized_email_ccs.count { |email_cc| email_cc[:action] == :put })

            params[:ticket][:set_email_ccs] = normalized_email_ccs
          end

          if !params[:ticket][:set_email_ccs] && !params[:ticket][:set_followers]
            normalize_legacy_collaborators(params)
            if user.is_agent? && !user.can?(:publicly, Comment)
              normalize_legacy_collaborators_with_follower_type(params)
            elsif @user.is_end_user?
              normalize_legacy_collaborators_with_email_cc_type(params)
            end
          end
        else
          if params_contains_legacy_collaborator_fields?(params[:ticket])
            normalize_legacy_collaborators(params)
          else
            collaborators = combine_ccs_followers_params(params[:ticket])
            params[:ticket][:additional_collaborators] = collaborators if collaborators.any?
          end
        end

        params[:comment] ||= params[:ticket].delete(:comment) if params[:ticket][:comment]
        params[:voice_comment] ||= params[:ticket].delete(:voice_comment) if params[:ticket][:voice_comment]

        if params[:ticket][:uploads]
          params[:comment] ||= {}
          params[:comment][:uploads] = params[:ticket].delete(:uploads)
        end

        params[:comment] = {body: params.delete(:comment)} if
          params[:comment].is_a?(String) &&
          params[:comment].present?

        unless params[:comment].blank?
          params[:comment][:uploads] = Array(params[:comment][:uploads]).select(&:present?)
          if params[:comment][:uploads].empty?
            params[:comment].delete(:uploads)
          elsif params[:comment][:uploads].size == 1
            params[:comment][:uploads] = params[:comment][:uploads][0]
          elsif params[:comment][:uploads].size > 1
            # remove duplicate tokens
            params[:comment][:uploads].uniq!
          end
        end

        if params[:comment].present?
          Comments::Initializer.new(user, params[:comment]).normalize
        end

        if params[:ticket].key?(:sharing_agreement_ids)
          sharing_agreement_ids = params[:ticket].delete(:sharing_agreement_ids) || []
          params[:ticket][:sharing_agreements] = sharing_agreement_ids.map { |id| { id: id } }
        end

        if params[:ticket].key?(:status_id)
          if ACCEPTED_STATUS_TYPES.exclude?(params[:ticket][:status_id].to_i) && account.has_validate_api_ticket_status?
            raise Zendesk::UnknownAttributeError, "Supported status types: #{StatusType.name_ids.select { |_, v| ACCEPTED_STATUS_TYPES.include?(v) }.keys.join(', ')}"
          end
        end

        normalize_boolean(params[:ticket], :force_status_change)
        normalize_boolean(params[:ticket], :solved)
        normalize_tags(params[:ticket], user)
        normalize_via_id(params[:ticket], request_params, params[:from_api])
        normalize_requester_data(params)
        normalize_subject(params[:ticket]) if params[:ticket][:subject].present?

        [request_params, params]
      end

      def check_max_email_cc_count(count)
        if count > max_email_ccs_limit
          raise ZendeskApi::Controller::Errors::TooManyValuesError.new("email_ccs", max_email_ccs_limit)
        end
      end

      def normalize_collaborators(collaborator_params)
        (collaborator_params || []).map do |collaborator|
          {
            user_id: to_integer_or_nil(collaborator[:user_id]),
            email: collaborator[:user_email]&.downcase,
            name: collaborator[:user_name],
            action: to_collaboration_action(collaborator[:action])
          }
        end
      end

      def to_integer_or_nil(value)
        value.to_i.to_s == value.to_s ? value.to_i : nil
      end

      def to_boolean(value)
        ['1', 'true'].include?(value.to_s.strip.downcase)
      end

      def to_collaboration_action(value)
        normalized_value = value.to_s.strip.downcase
        ["put", "delete"].include?(normalized_value) ? normalized_value.to_sym : :put
      end

      def normalize_subject(ticket_params)
        unless ticket_params[:subject].is_a?(String)
          Rails.logger.info("Info: ZD2466837 - subject not passed in as String")
        end
        ticket_params[:subject] = ticket_params[:subject].to_s
      end

      def normalize_tags(ticket_params, user)
        return if user.can?(:edit_tags, Ticket)
        ticket_params.delete(:set_tags)
        ticket_params.delete(:remove_tags)
      end

      def allow_via_override?(via_id)
        OVERRIDABLE_VIA_TYPES.include?(via_id)
      end

      def internal_client?(request_params)
        request_params.respond_to?(:internal_client?) && request_params.internal_client?
      end

      def normalize_via_id(ticket_params, request_params, from_api)
        via_id = nil
        original_via_id = nil

        if from_mobile_app?(request_params)
          via_id = ViaType.MOBILE
        elsif from_api
          via_id = ViaType.WEB_SERVICE
        elsif request_params.present? && request_params.xml_or_json_format
          via_id = (ticket_params[:via_id] || ViaType.WEB_SERVICE).to_i
          via_id = ViaType.WEB_SERVICE unless allow_via_override?(via_id)

          # Internal exceptions
          if ViaType.LOTUS == via_id || (internal_client?(request_params) && ViaType.SATISFACTION_PREDICTION != via_id)
            via_id = ViaType.WEB_FORM
          end
        elsif ticket_params[:entry_id].present?
          via_id = ViaType.TOPIC
        elsif ticket_params[:via_id] == ViaType.GITHUB
          via_id = ViaType.GITHUB
        else
          via_id = ViaType.WEB_FORM
        end

        unless @account.has_fix_voice_via_override_on_followup? && ticket_params[:via_followup_source_id].present?
          if VOICE_VIA_TYPES.include?(ticket_params[:via_id].to_i)
            ticket_params[:via] = { channel: ticket_params[:via_id] }
          end
        end

        if ticket_params[:via_followup_source_id].present?
          original_via_id = via_id
          via_id = ViaType.CLOSED_TICKET
        end

        ticket_params[:via_id] = via_id
        ticket_params[:original_via_id] = original_via_id
      end

      def from_mobile_app?(request_params)
        request_params.present? &&
        Zendesk::Accounts::Source.zendesk_app_only_user_agent?(request_params.user_agent)
      end

      def normalize_requester_data(params)
        ticket_params = params[:ticket]
        # had to comment this line out because of this ticket #168518
        # we should get this line back in when on v2
        # return if ticket_params[:requester_id].present?

        name      = ticket_params.delete(:requester_name)
        email     = ticket_params.delete(:requester_email) || params.delete(:email)
        requester = ticket_params.delete(:requester)

        if email.present? && name.present?
          ticket_params[:requester_data] = { email: email, name: name }
        elsif email.present? && !name.present?
          ticket_params[:requester_data] = email
        elsif name.present? # The string case
          ticket_params[:requester_data] = name
        elsif requester # Nested API case
          # `requester` can be a string (email)
          # `locale` takes precedent over `locale_id`
          requester.delete(:locale_id) if HashParam.ish?(requester) && requester.key?(:locale)

          ticket_params[:requester_data] = requester
        end

        # if ticket_params[:requester_data].is_a?(String)
        #   ticket_params[:requester_data] = parse_requester_data(ticket_params[:requester_data])
        # end
      end

      # Is this field editable by end users?
      def editable_field?(field)
        field.is_active? && field.is_editable_in_portal?
      end

      def filtered_ticket_fields(fields)
        normalize_ticket_fields(fields).select { |id, _| allowed_ticket_fields.include?(id.to_i) }
      end

      def allowed_ticket_fields
        # Used to ensure that the user can create entries for these fields
        @allowed_ticket_fields ||= begin
          fields = @account.ticket_fields(include_voice_insights: true).active.custom
          fields = fields.visible_in_portal.editable_in_portal if @user.is_end_user?
          fields.pluck(:id)
        end
      end

      # Normalizes ticket fields trying as hard as possible
      # to get the passed in fields into the format
      # { id => val, id2 => val2 }
      #
      # Accepts arrays and singular hashes of the formats:
      # { :id => "ticket_field_id", :value => "test_value" }
      # { "ticket_field_id" => "test_value" }
      #
      # Discards any other input.
      def normalize_ticket_fields(fields)
        if fields.is_a?(Array)
          fields.inject({}) do |accum, field|
            accum.merge!(normalize_ticket_field_hash(field))
          end
        elsif HashParam.ish?(fields)
          normalize_ticket_field_hash(fields)
        else
          {}
        end
      end

      def normalize_ticket_field_hash(field)
        return {} unless HashParam.ish?(field)

        if field.key?(:id)
          { field[:id] => field[:value] }
        else
          field
        end
      end

      def max_email_ccs_limit
        Ticket::MAX_COLLABORATORS_V2
      end

      def normalize_legacy_collaborators(params)
        if params[:ticket].key?(:collaborators)
          params[:ticket][:set_collaborators] = params[:ticket].delete(:collaborators)
        elsif params[:ticket].key?(:collaborator_ids)
          params[:ticket][:set_collaborators] = params[:ticket].delete(:collaborator_ids)
        end
      end

      def normalize_legacy_collaborators_with_email_cc_type(params)
        collaborators = params[:ticket].delete(:set_collaborators) if params[:ticket].key?(:set_collaborators)
        additional_collaborators = params[:ticket].delete(:additional_collaborators) if params[:ticket].key?(:additional_collaborators)

        check_max_email_cc_count([(collaborators || []).size, (additional_collaborators || []).size].max)

        params[:ticket][:set_collaborators_with_email_cc_type] = collaborators if collaborators
        params[:ticket][:additional_collaborators_with_email_cc_type] = additional_collaborators if additional_collaborators
      end

      def normalize_legacy_collaborators_with_follower_type(params)
        collaborators = params[:ticket].delete(:set_collaborators) if params[:ticket].key?(:set_collaborators)
        additional_collaborators = params[:ticket].delete(:additional_collaborators) if params[:ticket].key?(:additional_collaborators)

        params[:ticket][:set_collaborators_with_follower_type] = collaborators if collaborators
        params[:ticket][:additional_collaborators_with_follower_type] = additional_collaborators if additional_collaborators
      end

      def params_contains_legacy_collaborator_fields?(ticket_params)
        legacy_fields = [
          :collaborators,
          :collaborator_ids,
          :additional_collaborators
        ]

        legacy_fields.any? { |param| ticket_params.key?(param) }
      end

      def combine_ccs_followers_params(ticket_params)
        collaborators = []

        collaborators.concat(
          normalize_collaborators(ticket_params[:email_ccs])
        ) if ticket_params.key?(:email_ccs)

        collaborators.concat(
          normalize_collaborators(ticket_params[:followers])
        ) if ticket_params.key?(:followers)

        collaborators.map do |collaborator|
          if collaborator[:action] == :put
            next collaborator[:user_id] if collaborator[:user_id]
            next email_format(collaborator) if collaborator[:email]
          end
        end.compact
      end

      def email_format(collaborator)
        if collaborator[:name]
          { email: collaborator[:email], name: collaborator[:name] }
        else
          collaborator[:email]
        end
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'ticket')
      end
    end
  end
end
