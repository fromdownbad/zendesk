module Zendesk
  module Tickets
    module ControllerSupport
      include Zendesk::Auth::InternalRequest

      def self.included(base)
        base.before_action :authorize_ticket_deletion, only: [:destroy, :destroy_many]
        base.before_action :authorize_ticket_update,
          :ensure_original_ticket, only: [:update, :print]
        base.before_action :rate_limit_ticket_creation, only: [:create], unless: :internal_request?
        base.before_action :verify_safe_update,         only: [:update], if: :safe_update_enabled?
      end

      protected

      def authorize_ticket_deletion
        unless current_user.can?(:delete, ticket_initializer.ticket)
          raise Access::Denied.new("Not authorized!", :delete, ticket_initializer.ticket)
        end
      end

      def authorize_ticket_update
        if current_user.is_end_user? && !current_user.can?(:edit, ticket_initializer.ticket)
          raise Access::Denied.new("Not authorized!", :edit, ticket_initializer.ticket)
        end
      end

      def do_not_render_private_comments(&block)
        Comment.where(is_public: true).scoping(&block)
      end

      def ticket_initializer
        @ticket_initializer ||= Zendesk::Tickets::Initializer.new(current_account, current_user, params, include: ticket_finder_includes, request_params: request_params)
      end

      def initialized_ticket
        @initialized_ticket ||= ensure_access_to_ticket(ticket_initializer.ticket)
      end

      # Needs to put this here since some V1 controllers are using ControllerSupport
      def request_params
        @request_params ||= Zendesk::Tickets::HttpRequestParameters.new(request)
      end

      def requested_ticket
        return @requested_ticket unless @requested_ticket.nil?

        ticket = current_account.tickets.find_by_nice_id!(params[:id], include: ticket_finder_includes)

        ticket.current_user = current_user
        @requested_ticket = ensure_access_to_ticket(ticket)
      end

      def ensure_access_to_ticket(ticket)
        unless ticket.current_user.can?(:view, ticket)
          recent_ticket_manager.remove_ticket(ticket)
          raise Access::Denied.new("Not authorized!", :view, ticket)
        end

        ticket
      end

      def ensure_original_ticket
        ticket = current_account.tickets.find_by_nice_id!(params[:id])
        ticket.current_user = current_user
        ensure_access_to_ticket(ticket)
      end

      def ticket_finder_includes
        []
      end

      # Store ticket in "recent tickets" list
      def update_recent_tickets
        ticket = @ticket || @new_ticket || @initialized_ticket || @requested_ticket

        if ticket && ticket.nice_id
          recent_ticket_manager.add_ticket(ticket)
        end
      end

      def rate_limit_ticket_creation
        return unless current_user.is_end_user?
        return if Zendesk::Auth::Warden::BasicStrategy.basic_auth?(request)

        throttle_options = { threshold: current_account.settings.end_user_portal_ticket_rate_limit }
        throttle_key     = (current_user.id || request.remote_ip)

        Prop.throttle!(:portal_tickets, throttle_key, throttle_options)
      end

      def safe_update_enabled?
        params[:ticket] && params[:ticket][:safe_update] == true
      end

      def verify_safe_update
        if params[:ticket][:updated_stamp].nil?
          render json: {
            error: "UnprocessableEntity",
            description: "Safe Update requires an updated_stamp"
          }, status: :unprocessable_entity
        end
      end

      def find_requests
        options = { page: get_page }
        options[:order] = Output.new.set_order(params)
        options[:order] ||= 'created_at, id DESC'

        if params[:filter] == 'cc'
          if current_account.has_comment_email_ccs_allowed_enabled?
            current_user.ccd_tickets.archived_paginate(options)
          elsif current_account.is_collaboration_enabled? && !current_account.has_follower_and_email_cc_collaborations_enabled?
            current_user.collaborated_tickets.archived_paginate(options)
          else
            WillPaginate::Collection.new(1, 1, 0)
          end
        else
          if current_account.field_assignee.is_visible_in_portal?
            options[:select] = "tickets.*, #{Zendesk::Fom::Field.for(:assignee).join_field_alias}"
            options[:joins] = Zendesk::Fom::Field.for(:assignee).join
          end

          if params[:filter] == 'solved'
            current_user.tickets.solved.archived_paginate(options)
          else
            current_user.tickets.working.paginate(options)
          end
        end
      end

      # For Cursor Based Pagination of Tickets we support:
      #   * 'id' (default order)
      #   * 'updated_at'
      def sort_tickets_for_cursor(scope)
        sort = params[:sort].to_s
        order = {}
        index = ''

        case sort
        when '', 'id'
          order = { nice_id: :asc }
          index = Ticket::ACCOUNT_AND_NICE_ID_INDEX
        when '-id'
          order = { nice_id: :desc }
          index = Ticket::ACCOUNT_AND_NICE_ID_INDEX
        when 'updated_at'
          order = { updated_at: :asc, id: :asc }
          index = Ticket::ACCOUNT_AND_STATUS_AND_UPDATED_INDEX
        when '-updated_at'
          order = { updated_at: :desc, id: :desc }
          index = Ticket::ACCOUNT_AND_STATUS_AND_UPDATED_INDEX
        else
          raise Zendesk::CursorPagination::InvalidPaginationParameter, "sort is not valid"
        end

        scope.use_index(index).reorder(order)
      end
    end
  end
end
