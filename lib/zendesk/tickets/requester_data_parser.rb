require 'zendesk_mail'

module Zendesk
  module Tickets
    module RequesterDataParser
      protected

      # Parses out a proper name & email from a name or email or Hash
      def parse_requester_data(data)
        return nil if data.blank?

        if data.is_a?(Hash)
          email = data[:email]
          name  = data[:name]
        elsif data.is_a?(String)
          email = data.strip
        else
          return nil
        end

        return unless email.to_s.include?("@")

        record = Zendesk::Mail::Address.parse(email)

        return nil unless record

        name   = record.name if name.blank?
        email  = record.address

        { name: name, email: email }
      end
    end
  end
end
