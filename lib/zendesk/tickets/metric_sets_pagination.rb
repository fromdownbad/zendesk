# This class should not be used anywhere else other than
# ticket_metrics_controller
#
# Why is this module needed?
# Short answer: To support proper pagination of ticket metric sets after we
# split the query into 2 parts.
#
# Long answer:
# We decided to split the existing ticket metrics query into 2 different
# queries. The first one to load all the latest tickets and the second one to
# load the ticket_metric_sets based on the ticket_ids.
#
# Our pagination/caching mechanism does not support caching multiple queries,
# we can only cache results of one query. Hence we are caching the ticket query
# that loads all the tickets from Ticket table based on created_at. This will
# return at max 100 tickets. Hence the second query is pretty light weight as
# it always at max loads 100 rows from ticket_metric_sets. We copy over all the
# pagination related attributes from ticket query object to the
# ticket_metric_sets object. This will help us correctly populate the
# `next_page` and `previous_page` for API response
module Zendesk::Tickets
  module MetricSetsPagination
    def self.extended(base)
      class << base
        attr_accessor :current_page,
          :total_entries,
          :next_page,
          :previous_page,
          :per_page
      end
    end

    # Add pagination attributes from ticket to the ticket_metrics_sets
    # association object.
    #
    # This is needed because we are not directly paginating on the
    # ticket_metric_sets table, instead we are leveraging the pagination on
    # ticket table which gets paginated/cached with the will_paginate gem
    def add_pagination_options(from_ticket)
      self.current_page  = from_ticket.current_page
      self.next_page     = from_ticket.next_page
      self.previous_page = from_ticket.previous_page
      self.per_page      = from_ticket.per_page
      self.total_entries = from_ticket.total_entries
    end
  end
end
