require 'zendesk/tickets/recent_ticket_manager'

module Zendesk::Tickets
  # Manages recent ticket lists for users.
  #
  # See Zendesk::Tickets::RecentTicketManager for more information.
  #
  # Examples
  #
  #   # In controllers and views:
  #   recent_tickets
  #   # => [#<Ticket id: 1, ...>, #<Ticket id: 2, ...>]
  #
  module RecentTicketManagement
    def self.included(base)
      base.helper_method :recent_tickets
    end

    protected

    # Get the most recent tickets for the current user.
    #
    # Returns an Array of full ticket objects or an empty one if there is no user.
    def recent_tickets
      return [] if current_user.nil?
      recent_ticket_manager.recent_tickets
    end

    # Get the recent ticket manager for the current user.
    #
    # Returns a Zendesk::Tickets::RecentTicketManager that manages the recent tickets list for
    #   the current user.
    # Raises an ArgumentError if there is no current user.
    def recent_ticket_manager
      @recent_ticket_manager ||= RecentTicketManager.new(current_user)
    end
  end
end
