module Zendesk
  module Tickets
    module Sms
      def create_sms_response
        # return unless should_create_sms_response?
        validate_and_create_sms_response
      end

      def should_create_sms_response?
        @should_create_sms_response ||=
          via_id != comment.via_id && # If the comment came from a channel, don't channel it back
          via?(:sms) &&
          comment.is_public? &&
          (comment.author.is_agent? || comment.author.foreign_agent?) &&
          account.voice_feature_enabled?(:sms)
      end

      def validate_and_create_sms_response
        outgoing_text = Zendesk::Liquid::TicketContext.render(
          comment.try(:body), self, comment.author, false, requester.translation_locale
        )

        ::Sms::RequestMessageJob.enqueue(account_id, id, comment.author_id, outgoing_text)
      end
    end
  end
end
