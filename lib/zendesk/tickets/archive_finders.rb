module Zendesk
  module Tickets
    module ArchiveFinders
      def find(*args)
        if args.first.is_a?(Integer) || args.first.is_a?(String)
          find_by_id!(*args)
        else
          super
        end
      end

      def first_with_archive_and_conditions(conditions, options)
        where(conditions).scoping do
          first_with_archived(options)
        end
        # Raised when `id` is larger than supported by MySQL.
        # See https://github.com/rails/rails/blob/7a48f586/activerecord/lib/active_record/relation/finder_methods.rb#L85-L8
      rescue ::RangeError
        nil
      end

      def find_by_id(id, options = {})
        first_with_archive_and_conditions({id: id}, options)
      end

      def find_by_id!(id, options = {})
        find_by_id(id, options) || raise(ActiveRecord::RecordNotFound, id)
      end

      def find_by_nice_id(nice_id, options = {})
        first_with_archive_and_conditions({nice_id: nice_id}, options)
      end

      def find_by_nice_id!(nice_id, options = {})
        find_by_nice_id(nice_id, options) || raise(ActiveRecord::RecordNotFound, nice_id)
      end
    end
  end
end
