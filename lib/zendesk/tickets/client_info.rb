module Zendesk
  module Tickets
    module ClientInfo
      private

      def add_client_info_from_request(ticket, request)
        add_client_info_from_hash(ticket, user_agent: request.user_agent, remote_ip: request.remote_ip)
      end

      def add_client_info_from_hash(ticket, options)
        client_info = []

        if user_agent = options[:user_agent]
          client_info << "Client: #{user_agent}"
        end

        unless (remote_ip = options[:remote_ip]).blank?
          client_info << "IP address: #{remote_ip}"
        end

        ticket.client = client_info.join("\n") if client_info.any?
      end
    end
  end
end
