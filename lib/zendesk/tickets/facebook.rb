module Zendesk
  module Tickets
    module Facebook
      def self.included(base)
        base.scope :via_facebook, -> { base.where(via_id: ViaType.FACEBOOK_POST) }
      end

      def via_facebook?
        via?(:facebook_post) || via?(:facebook_message)
      end

      def facebook_update?
        via_facebook? && audit.via_facebook?
      end
    end
  end
end
