module Zendesk
  module Tickets
    # Public: Import tickets into Zendesk.
    #
    class Importer
      class ImportException < StandardError
        attr_reader :record, :errors

        def initialize(record = nil)
          if record
            @record = record
            @errors = record.errors
          end
        end

        def message
          if @errors.present?
            @errors.full_messages.join('; ')
          else
            "Could not save ticket"
          end
        end
      end

      class ImportFailed < ImportException; end

      class InvalidParams < ImportException; end

      # Public: Creates a new TicketImporter.
      #
      # Options
      #
      # :account - the Account to which the tickets should be imported.
      # :user    - the User that is doing the import.
      #
      def initialize(options = {})
        @account             = options.fetch(:account)
        @user                = options.fetch(:user)
        @archive_immediately = options.fetch(:archive_immediately, false)
      end

      # Public: Imports a ticket.
      #
      # params - the Hash of parameters with which to create the Ticket.
      #
      # Returns the created Ticket.
      # Raises InvalidParams if `params` were not valid when creating the Ticket.
      # Raises ImportFailed if the import failed for some other reason.
      def import(params = {})
        raise InvalidParams if params.nil?

        @params = params.with_indifferent_access.
          reverse_merge(via: { channel: ViaType.WEB_SERVICE })

        @comments_params = @params.delete(:comments)

        construct_ticket
        validate_tickets_future_dates
        validate_comments_created_at
        initialize_ticket

        begin
          @ticket.validate!
        rescue ActiveRecord::RecordInvalid, Fraud::SpamDetector::SpamTicketFoundException => e
          Rails.logger.error("Tickets Importer ticket validation failed: #{e.message}")
          raise InvalidParams, @ticket
        end

        @ticket.set_nice_id

        Ticket.transaction do
          set_timestamps
          save_ticket
          add_comments
          add_rating
          archive_ticket
        end

        @ticket
      end

      private

      attr_reader :params, :comments_params

      def initializer_class
        Zendesk::Tickets::Initializer
      end

      def construct_ticket
        @ticket = initializer_class.new(@account, @user, ticket: params).ticket
      end

      def initialize_ticket
        @ticket.will_be_saved_by(@user)
        @ticket.audit.disable_triggers = true

        if comments_params.present? && params[:description].blank?
          # Sort the comments and find the earliest created_at date or, if none have one, the first comment
          comments_params.sort_by! { |c| c[:created_at].to_i }
          comment_param = comments_params.detect { |comment| comment[:created_at] } || comments_params.first

          comment = Tickets::Comments::Initializer.new(@user, comments_params.delete(comment_param)).comment

          comment[:is_public] = Zendesk::CommentPublicity.is_comment_public?(@user, comment[:is_public], @ticket, ViaType.WEB_SERVICE)

          @ticket.add_comment(comment.except(:metadata))
          @ticket.description = @ticket.comment.body

          @ticket.audit.try(:apply_metadata, :custom, comment.delete(:metadata))
        end
      end

      def save_ticket
        Ticket.record_timestamps = false

        Prop.disabled do
          @ticket.disable_notifications = true

          unless @ticket.save(validate: false)
            raise InvalidParams, @ticket
          end
        end
      ensure
        Ticket.record_timestamps = true
      end

      def set_timestamps
        @ticket.solved_at  = params[:solved_at]
        @ticket.created_at = params[:created_at]
        if @ticket.created_at
          if @ticket.created_at < Time.at(0)
            @ticket.created_at = Time.at(0)
          end
        else
          @ticket.created_at = Time.now
        end

        @ticket.updated_at = params[:updated_at]
        if @ticket.updated_at
          if @ticket.updated_at < Time.at(0)
            @ticket.updated_at = Time.at(0)
          end
        else
          @ticket.updated_at = @ticket.created_at
        end

        @ticket.audit.created_at = @ticket.created_at
      end

      # The exception shows they are sending the comments malformed (it should an array of hashes):
      # "comments"=>
      # {"author_id"=>"290123986",
      #  "created_at"=>"2012-12-12T20:45:18Z",
      #  "value"=>"this is a test",
      #  "public"=>"false"}
      def add_comments
        return if comments_params.blank?
        raise InvalidParams, @ticket unless comments_params.is_a?(Array)

        new_comments = comments_params.map do |comment|
          add_comment(Tickets::Comments::Initializer.new(@user, comment).comment)
        end

        updated_at_needs_to_reflect_latest_comment =
          params[:updated_at].blank? &&
          @ticket.updated_at < last_comment.created_at

        # Check if saving ticket is required to re-evaluate privacy after adding comments.
        privacy_needs_to_be_evaluated =
          @ticket.is_private? &&
          new_comments.any?(&:is_public?)

        if updated_at_needs_to_reflect_latest_comment
          @ticket.updated_at = last_comment.created_at
        end

        if updated_at_needs_to_reflect_latest_comment || privacy_needs_to_be_evaluated
          save_ticket
        end
      end

      def last_comment
        @last_comment ||= @ticket.comments.last
      end

      def add_comment(comment_params)
        unless comment_params[:value].present? ||
               comment_params[:body].present? ||
               comment_params[:html_body].present?
          @ticket.errors.add(:comment, :blank)
          raise InvalidParams, @ticket
        end

        if comment_params[:author_id]
          begin
            User.find(comment_params[:author_id])
          rescue ActiveRecord::RecordNotFound
            @ticket.comment.errors.add(:author, :invalid)
            raise InvalidParams, @ticket.comment
          end
        else
          comment_params[:author_id] = @user.id
        end

        audit = @ticket.audits.build(
          disable_triggers: true,
          created_at: comment_params[:created_at],
          author_id: comment_params[:author_id],
          via_id: ViaType.WEB_SERVICE,
          client: @ticket.client, # client info - e.g. IP address, browser/OS data
          account: @account
        )

        audit.apply_metadata(:custom, comment_params.delete(:metadata))
        audit.save!

        comment_params[:is_public] = Zendesk::CommentPublicity.is_comment_public?(@user, comment_params[:is_public], @ticket, ViaType.WEB_SERVICE)
        params = comment_params.merge(via_id: ViaType.WEB_SERVICE, account: @account, audit: audit, ticket: @ticket)
        Comment.create!(params)
      end

      def add_rating
        return unless rating = @params.delete(:satisfaction_rating)
        Zendesk::Tickets::Initializer.set_rating(@ticket, rating)
        raise InvalidParams, @ticket unless @ticket.satisfaction_score # ticket validates the score, but we call save without validations ...
        @ticket.will_be_saved_by(@user)
        save_ticket
      end

      def archive_ticket
        return unless @ticket.status_id == StatusType.CLOSED && @archive_immediately
        Ticket.allow_without_entity_publication do
          @ticket.archive!(key_version: Zendesk::Archive::Archiver::KEY_VERSION)
        end
      end

      def validate_tickets_future_dates
        if !params[:created_at].blank? && a_date_string?(params[:created_at]) && params[:created_at] > Time.now
          @ticket.errors.add(:created_at, :future_date)
          raise InvalidParams, @ticket
        end

        if !params[:updated_at].blank? && a_date_string?(params[:updated_at]) && params[:updated_at] > Time.now
          @ticket.errors.add(:updated_at, :future_date)
          raise InvalidParams, @ticket
        end

        if !params[:solved_at].blank? && a_date_string?(params[:solved_at]) && params[:solved_at] > Time.now
          @ticket.errors.add(:solved_at, :future_date)
          raise InvalidParams, @ticket
        end
      end

      def validate_comments_created_at
        return unless comments_params.present? && comments_params.is_a?(Array)
        if comments_params.any? { |c| c[:created_at] && !c[:created_at].between?(Time.at(0), Time.now) }
          @ticket.errors.add(:comment, :invalid_timestamp)
          raise InvalidParams, @ticket
        end
      end

      def a_date_string?(string)
        valid_date = true
        begin
          string.to_datetime
        rescue ArgumentError
          valid_date = false
        end
        valid_date
      end
    end
  end
end
