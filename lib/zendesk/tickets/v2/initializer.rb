module Zendesk
  module Tickets
    module V2
      class Initializer < Tickets::Initializer
        private

        def apply_changes_from_params
          super

          if @ticket.new_record? && @ticket.via_id != ViaType.CLOSED_TICKET &&
            @ticket.comment && !@ticket.comment.author
            @ticket.comment.author = @ticket.submitter
          end

          if @params[:ticket][:raw_subject].present?
            unless @params[:ticket][:raw_subject].is_a?(String)
              Rails.logger.info("Info: ZD2466837 - raw_subject not passed in as String")
            end
            @ticket.subject = @params[:ticket][:raw_subject].to_s
          end

          @ticket.audit.disable_triggers = @params[:disable_triggers]
        end

        # Overrides Tickets::Initializer#apply_followup. The ticket description is sent in the response,
        # so we need to make sure it is updated with any user changes to the default message.
        def set_followup_comment(comment) # rubocop:disable Naming/AccessorMethodName
          @params[:description] ||= comment
        end

        def default_submitter(ticket)
          ticket.requester
        end

        def apply_changes
          super

          add_flags
        end

        def add_flags
          return unless account.has_on_behalf_of_user_flag?

          # Add flag with translation attributes
          if @ticket.comment && @ticket.comment.author && @ticket.comment.author != @user && !valid_impersonate
            add_flag(EventFlagType.ON_BEHALF_OF_USER, message: { user: @user.name })
          end
        end

        def add_flag(flag, options = {})
          @ticket.add_flags!([flag], flag => options)
        end

        # Do add flags when agents make comments as agents
        # Do add flags when agents make comments as admins
        # Do add flags when admins make comments as admins
        # Do NOT add flags when admins make comments as agents
        # Do NOT add flags when agents make comments as end-users
        def valid_impersonate
          @ticket.comment.author.roles == Role::END_USER.id ||
          (@ticket.comment.author.roles == Role::AGENT.id && @user.roles == Role::ADMIN.id) ||
          (@ticket.comment.author.roles == Role::LEGACY_AGENT.id && @user.roles == Role::ADMIN.id)
        end
      end
    end
  end
end
