module Zendesk
  module Tickets
    module V2
      class Importer < Tickets::Importer
        private

        def initializer_class
          Zendesk::Tickets::V2::Initializer
        end
      end
    end
  end
end
