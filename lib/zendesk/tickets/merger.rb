module Zendesk::Tickets
  # Merge a set of source tickets into a target ticket.
  #
  # This class contains the logic needed to perform a merger of multiple tickets.
  #
  # Examples
  #
  #   ticket_merger = Tickets::Merger.new(:user => current_user)
  #   ticket_merger.merge(
  #     :target         => some_ticket,
  #     :sources        => [source1, source2],
  #     :target_comment => "Merged some tickets into this one, yo!",
  #     :source_comment => "This a duplicate, yo!")
  #
  # This will merge `source1` and `source2` into `some_ticket`, leaving a comment on each ticket.
  # These comments will be public unless specified otherwise with `:source_is_public => false`
  # and `:target_is_public => false`.
  #
  class Merger
    def self.different_requesters_not_allowed?(current_account, sources)
      # when ccs are disabled the requester cannot be added as a cc onto the target, so only merges with the same requester are acceptable
      return true if !current_account.has_follower_and_email_cc_collaborations_enabled? && !current_account.is_collaboration_enabled?

      if current_account.has_follower_and_email_cc_collaborations_enabled? && !current_account.has_comment_email_ccs_allowed_enabled?
        # if both comment_email_ccs_allowed and ticket_followers_allowed are disabled we cannot add any ccs or followers so requesters must be the same
        return true unless current_account.has_ticket_followers_allowed_enabled?

        # if all requesters are agents we can allow the merge even with emails_ccs_allowed disabled because we can add them all as followers to the target
        return sources.any? && sources.all? { |s| s.requester.is_agent? } ? false : true
      end
      # if comment_email_ccs_allowed is enabled then we can add ccs so we can merge tickets with different requesters
      false
    end

    class MergeFailed < StandardError; end

    class SourceInvalid < StandardError
      attr_reader :ticket

      def initialize(ticket)
        @ticket = ticket
      end

      def message
        [
          'Invalid source during merge',
          "for Ticket ##{ticket.nice_id}",
          "on Account ##{ticket.account.id}:",
          ticket.errors.full_messages
        ].join(' ')
      end
    end

    class TargetInvalid < StandardError
      attr_reader :ticket
      attr_reader :user

      def initialize(ticket, user)
        @ticket = ticket
        @user = user
      end

      def message
        I18n.t("txt.ticket_merge.target_not_valid",
          account_subdomain: @ticket.account.subdomain,
          error_messages: @ticket.errors.full_messages,
          locale: @user.translation_locale)
      end
    end

    # Create a new ticket merger.
    #
    # Options
    #
    # :user - the User responsible for the merge.
    #
    def initialize(options = {})
      @user                = options.fetch(:user)
      @transaction_context = options[:transaction_context] || Ticket
    end

    # Merge a set of source tickets into a target ticket.
    #
    # Options
    #
    # :target           - the target Ticket.
    # :sources          - the Array of source Ticket objects.
    # :target_comment   - the String comment to leave on the target Ticket.
    # :source_comment   - the String comment to leave on the source Ticket objects.
    # :source_is_public - the Boolean indicating whether `:source_comment` should
    #                     be public (default: true).
    # :target_is_public - the Boolean indicating whether `:target_comment` should
    #                     be public (default: true).
    #
    # Returns nothing.
    # Raises TargetInvalid if `:target` was invalid during the merge.
    # Raises SourceInvalid if any of `:sources` were invalid during the merge.
    # Raises MergeFailed if the merge failed for some other reason.
    def merge(options = {})
      @target  = options.fetch(:target)
      @sources = options.fetch(:sources)

      publicity_default = Zendesk::CommentPublicity.merge_comment_publicity_default(@target, @sources)
      @source_is_public = options.fetch(:source_is_public, publicity_default)
      @target_is_public = options.fetch(:target_is_public, publicity_default)

      if @target.account.has_override_comment_publicity_in_ticket_merge_endpoint?
        merge_comment_can_be_public = Zendesk::CommentPublicity.merge_comment_can_be_public?(@target, @sources)
        @source_is_public &&= merge_comment_can_be_public
        @target_is_public &&= merge_comment_can_be_public
      end

      @source_comment = options.fetch(:source_comment)
      @target_comment = options.fetch(:target_comment)

      @source_public_attachments  = []
      @source_private_attachments = []

      merge_sources_into_target
      extract_collaborator_ids
      mark_target_as_merged
      save_tickets
    end

    private

    def merge_sources_into_target
      Rails.logger.info "[TicketMerger:Target:PreMerge] account_id:#{@target.account_id} ticket_id:#{@target.id} generated_timestamp:#{@target.generated_timestamp.to_i}"
      @sources.each do |source|
        # get list of all attachments to be copied over, but don't actually copy them until the very end
        if source && source.comments.present?
          source.comments.each do |comment|
            next unless comment.attachments.present?
            valid_attachments = filter_attachments_to_valid(comment.attachments)
            if comment.is_public
              @source_public_attachments  += valid_attachments
            else
              @source_private_attachments += valid_attachments
            end
          end
        end
        Rails.logger.info "[TicketMerger:Source:PreMerge] account_id:#{source.account_id} ticket_id:#{source.id} generated_timestamp:#{source.generated_timestamp.to_i}"
        merge_source(source)
      end
    end

    def filter_attachments_to_valid(attachments)
      attachments.select { |att| att.stores.any? }
    end

    def merge_source(source)
      unless source.merge_into(@target, @user, @source_comment, @source_is_public)
        raise MergeFailed, I18n.t("txt.ticket_merge.source_not_valid",
          account_subdomain: source.account.subdomain,
          source_id: source.id,
          target_id: @target.id,
          error_messages: source.errors.full_messages,
          locale: @user.translation_locale)
      end
    end

    def extract_collaborator_ids
      @follower_ids = @target.follower_ids
      @collaborator_ids = []
      @email_cc_ids = []
      source_requester_ids = @sources.map(&:requester_id)
      account = @target.account

      if account.has_email_ccs?
        sources_and_target_ticket_ids = @sources.map(&:id) << @target.id
        legacy_cc_ids = Collaboration.where(account_id: account.id, ticket_id: sources_and_target_ticket_ids, collaborator_type: CollaboratorType.LEGACY_CC).pluck(:user_id)
        curr_email_cc_ids = Collaboration.where(account_id: account.id, ticket_id: sources_and_target_ticket_ids, collaborator_type: CollaboratorType.EMAIL_CC).pluck(:user_id)
        curr_follower_ids = Collaboration.where(account_id: account.id, ticket_id: sources_and_target_ticket_ids, collaborator_type: CollaboratorType.FOLLOWER).pluck(:user_id)
        agent_requesters = User.where(id: source_requester_ids, account_id: account.id, roles: User::AGENT_ROLES)

        # If there are any legacy_ccs present then we will want to set the collaborators in legacy mode
        if legacy_cc_ids.any?
          @collaborator_ids.concat(legacy_cc_ids)
        end

        # Add all the current email_ccs and followers from the source tickets and target ticket.
        @email_cc_ids.concat(curr_email_cc_ids)
        @follower_ids.concat(curr_follower_ids)

        if account.has_comment_email_ccs_allowed_enabled?
          # Requesters on source tickets that differ from the target ticket get copied as Email CCs
          @email_cc_ids.concat(source_requester_ids)

          # If some of the source ticket requesters are agents then also add those agents as followers
          if account.has_agent_email_ccs_become_followers_enabled? && agent_requesters.any?
            @follower_ids.concat(agent_requesters.map(&:id))
          end
        elsif account.has_ticket_followers_allowed_enabled? && (agent_requesters.count == source_requester_ids.count)
          # If all requesters on the source tickets are agents, then add them as followers
          @follower_ids.concat(source_requester_ids)
        elsif account.is_collaboration_enabled?
          # The account has not opted into CCs/Followers yet but has the legacy collbaorations enabled,
          # so make sure the source requester IDs are added as legacy collaborations
          @collaborator_ids.concat(source_requester_ids)
        end
      else
        # If in legacy collaboration mode then all requesters on source tickets get copied as legacy_ccs
        @follower_ids.concat(source_requester_ids)

        # Add all followers/collaborators from the source tickets.
        # NOTE: follower_ids will return legacy CCs + email CCs + followers when follower_and_email_cc_collaborations disabled
        @follower_ids.concat(@sources.map(&:follower_ids).flatten)
      end

      [@collaborator_ids, @email_cc_ids, @follower_ids].each { |c| c.delete(@target.requester.id); c.uniq!; c.sort! }
    end

    def mark_target_as_merged
      @target.mark_with(@sources, @target_comment, @target_is_public)
      @target.mark_as_merged(@sources, user: @user, follower_ids: @follower_ids, email_cc_ids: @email_cc_ids, collaborator_ids: @collaborator_ids)
    end

    def save_tickets
      @transaction_context.transaction do
        @sources.each do |source|
          source.save || raise(SourceInvalid, source)
          Rails.logger.info "[TicketMerger:Source:PostMerge] account_id:#{source.account_id} ticket_id:#{source.id} generated_timestamp:#{source.reload.generated_timestamp.to_i}"
        end

        @target.save || raise(TargetInvalid.new(@target, @user))
        Rails.logger.info "[TicketMerger:Target:PostMerge] account_id:#{@target.account_id} ticket_id:#{@target.id} generated_timestamp:#{@target.reload.generated_timestamp.to_i}"

        add_attachments_to_last_comment(@source_public_attachments)

        if @source_private_attachments.any?
          if @target.reload.comments.last.is_public
            @target.add_comment(body: I18n.t("txt.ticket_merge.private_attachments_comment.value", locale: @user.translation_locale), is_public: false)
            @target.will_be_saved_by(@user, via_id: ViaType.MERGE, via_reference_id: @target.id)
            @target.save || raise(TargetInvalid.new(@target, @user))
          end

          add_attachments_to_last_comment(@source_private_attachments)
        end
      end
    end

    def add_attachments_to_last_comment(attachments)
      attachments.each do |attachment|
        begin
          new_attachment = Attachment.from_attachment(attachment)
          next if new_attachment.nil?
          new_attachment.source = @target.reload.comments.last
          new_attachment.save!
        rescue StandardError => e
          Rails.logger.error("Merge Failed for attachment #{attachment.id}," \
            " account #{attachment.account_id} on error #{e.inspect}")
          mfe = MergeFailed.new I18n.t("txt.ticket_merge.unable_to_copy_attachment",
            attachment_id: attachment.id,
            attachment_display_filename: attachment.display_filename,
            account_subdomain: @target.account.subdomain,
            error_message: e.message,
            locale: @user.translation_locale)
          mfe.set_backtrace e.backtrace
          raise mfe
        end
      end
    end
  end
end
