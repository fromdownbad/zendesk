module Zendesk::Tickets
  # Stores a list of objects for a user.
  #
  class RecentTicketStore
    # Create a new RecentTicketStore.
    #
    # user - the User to which the list belongs.
    #
    def initialize(user)
      @user = user
    end

    # Read the list from the backend.
    #
    # Returns the Array of objects returned from the backend, or an empty Array
    #   if the backend has no list.
    def read
      cache.read(key) || []
    end

    # Overwrite the list in the backend.
    #
    # list - an Array of objects to be stored.
    #
    # Returns nothing.
    def write(list)
      if list.empty?
        Rails.logger.debug("RecentTicketStore list empty. Deleting key: #{key}")
        cache.delete(key)
      else
        if cache.write(key, list)
          Rails.logger.debug("RecentTicketStore write succeeded #{key}:#{list}")
        else
          Rails.logger.debug("RecentTicketStore write failed #{key}:#{list}")
        end
      end
    end

    private

    def cache
      Rails.cache
    end

    def key
      "recent-tickets-for-#{@user.account_id}-#{@user.id}-v2"
    end
  end
end
