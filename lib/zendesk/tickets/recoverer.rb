module Zendesk::Tickets
  class Recoverer
    class RecoveryFailed < StandardError
      attr_reader :recovered_tickets, :failed_tickets

      def initialize(options = {})
        @failed_tickets = options.fetch(:failed_tickets, [])
        @recovered_tickets = options.fetch(:recovered_tickets, [])
      end

      def message
        "Failed to import #{failed_tickets.count} ticket#{failed_tickets.count > 1 ? 's' : ''}"
      end
    end

    def initialize(options)
      @account = options.fetch(:account)
      @user = options.fetch(:user)
    end

    # AKA "recover_many"
    def recover(suspended_tickets, options = {})
      recover_as_user = options.fetch(:recover_as_user, nil)

      @recovered = []
      @failed = []

      if recover_as_user.present?
        recover_suspended_tickets_as_user(suspended_tickets, recover_as_user)
      else
        authors_with_tickets =
          if profiling_enabled?
            statsd_client.time(:recover_authors) do
              recover_authors(suspended_tickets)
            end
          else
            recover_authors(suspended_tickets)
          end

        authors_with_tickets.each do |author, tickets|
          recover_suspended_tickets_by(author, tickets)
        end
      end

      if profiling_enabled?
        recover_as = recover_as_user.present? ? :user : :authors
      end

      if @failed.any?
        if profiling_enabled?
          Rails.logger.info "Recovery failed on #{@failed&.size}/#{suspended_tickets&.size} SuspendedTickets for '#{@account&.subdomain}' account, recovered as #{recover_as}"
          statsd_client.increment("recovery_failed", tags: ["caller:recover_many", "recover_as:#{recover_as}"])
        end

        raise RecoveryFailed, recovered_tickets: @recovered, failed_tickets: @failed
      else
        if profiling_enabled?
          Rails.logger.info "Recovery finished on #{suspended_tickets&.size} SuspendedTickets for '#{@account&.subdomain}' account, recovered as #{recover_as}"
          statsd_client.increment("recovery_finished", tags: ["caller:recover_many", "recover_as:#{recover_as}"])
        end

        @recovered
      end
    end

    # AKA "recover_single"
    def recover_ticket(suspended_ticket, recover_as_user)
      recovered_ticket = suspended_ticket.recover(@user, recover_as_user)

      if recovered_ticket.nil?
        if profiling_enabled?
          Rails.logger.info "Recovery failed on #{suspended_ticket&.id} SuspendedTicket for '#{@account&.subdomain}' account, recovered as user"
          statsd_client.increment("recovery_failed", tags: ["caller:recover_single", "recover_as:user"])
        end

        raise Zendesk::Tickets::Recoverer::RecoveryFailed, failed_tickets: [suspended_ticket]
      else
        if profiling_enabled?
          Rails.logger.info "Recovery finished on #{suspended_ticket&.id} SuspendedTicket for '#{@account&.subdomain}' account, recovered as user"
          statsd_client.increment("recovery_finished", tags: ["caller:recover_single", "recover_as:user"])
        end

        recovered_ticket
      end
    end

    private

    # Recover the authors of `suspended_tickets` and return a mapping of them to their (still suspended) tickets.
    def recover_authors(suspended_tickets)
      tickets_by_email = suspended_tickets.group_by { |t| t.from_mail.downcase }

      tickets_by_email.each_with_object({}) do |(email, tickets), result|
        name = tickets.select { |t| t.from_name.present? }.map(&:from_name).first
        user = find_or_create_user(email, name, send_verify_email: send_verify_email?(tickets))

        if user
          result[user] ||= []
          result[user] += tickets
        end
      end
    end

    def recover_suspended_tickets_as_user(suspended_tickets, recover_as_user)
      suspended_tickets.each do |suspended|
        if profiling_enabled?
          statsd_client.time(:create_or_assume_identity) do
            UserEmailIdentity.create_or_assume_identity(user: recover_as_user, email: suspended.from_mail)
          end
        else
          UserEmailIdentity.create_or_assume_identity(user: recover_as_user, email: suspended.from_mail)
        end

        recover_suspended_ticket(suspended, recover_as_user)
      end
    end

    def recover_suspended_tickets_by(author, suspended_tickets)
      suspended_tickets.each do |suspended_ticket|
        recover_suspended_ticket(suspended_ticket, author)
      end
    end

    def find_or_create_user(email, name, options = {})
      if profiling_enabled?
        statsd_client.time(:find_or_create_user) do
          @account.find_or_create_user_by_email(email, name: name, send_verify_email: options[:send_verify_email])
        end
      else
        @account.find_or_create_user_by_email(email, name: name, send_verify_email: options[:send_verify_email])
      end
    end

    def recover_suspended_ticket(suspended_ticket, recover_as_user)
      if profiling_enabled?
        statsd_client.time(:recover_suspended_ticket) do
          if ticket = suspended_ticket.recover(@user, recover_as_user)
            @recovered << ticket
          else
            @failed << suspended_ticket
          end
        end
      else
        if ticket = suspended_ticket.recover(@user, recover_as_user)
          @recovered << ticket
        else
          @failed << suspended_ticket
        end
      end
    end

    def send_verify_email?(suspended_tickets)
      @account.is_welcome_email_when_agent_register_enabled? &&
        suspended_tickets.any? { |t| t.cause != SuspensionType.SIGNUP_REQUIRED }
    end

    def profiling_enabled?
      @profiling_enabled ||= @account&.has_email_metrics_on_suspended_tickets_recovery?
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(
        namespace: %w[suspended_tickets recoverer]
      )
    end
  end
end
