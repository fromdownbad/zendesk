module Zendesk
  module Tickets
    module CollaborationSupport
      def legacy_or_modern_collaboration_enabled?
        if account.has_follower_and_email_cc_collaborations_enabled?
          account.has_comment_email_ccs_allowed_enabled? || account.has_ticket_followers_allowed_enabled?
        else
          account.is_collaboration_enabled?
        end
      end

      private

      def permitted_collaborator_email?(email)
        account.allows_collaborator?(email)
      end

      def new_users_allowed?
        return false if ticket_collaborators_disabled?
        return true if current_user.blank?

        if current_user.is_end_user?
          account.is_open? && !account.is_collaborators_addable_only_by_agents?
        else
          current_user.can?(:create_new, User)
        end
      end

      def ticket_collaborators_disabled?
        account.has_follower_and_email_cc_collaborations_enabled? &&
          !account.has_ticket_followers_allowed_enabled? &&
          !account.has_comment_email_ccs_allowed_enabled?
      end
    end
  end
end
