module Zendesk
  module CursorPagination
    module ControllerSupport
      DEFAULT_CURSOR_ORDERING = "asc".freeze
      CURSOR_PAGINATION_MAX_SIZE = 100
      DEFAULT_SORTABLE_FIELDS = { 'id' => :id }.freeze

      protected

      CURSOR_PAGINATION_V2_PAGE_PARAMETERS = %w[size before after].to_set.freeze
      CURSOR_PAGINATION_V1_PARAMETERS = %w[limit cursor].to_set.freeze

      # CBP v2:
      #   * page[size]=5
      #   * page[size]=5&page[after]=xxx
      #   * page[size]=5&page[before]=yyy
      def with_cursor_pagination_v2?
        return false unless HashParam.ish?(params[:page])

        page_keys = params[:page].keys.map(&:to_s).to_set
        page_keys.intersect?(CURSOR_PAGINATION_V2_PAGE_PARAMETERS)
      end

      # CBP v1:
      #   * limit=5
      #   * limit=5&cursor=xxx
      def with_cursor_pagination_v1?
        param_keys = params.keys.map(&:to_s).to_set
        param_keys.intersect?(CURSOR_PAGINATION_V1_PARAMETERS)
      end

      def with_cursor_pagination?
        with_cursor_pagination_v1? || with_cursor_pagination_v2?
      end

      # Uses CBPv2 implemented in 'zendesk_cursor_pagination' gem.
      def paginate_with_cursor(scope)
        scope.paginate_with_cursor(
          cursor_pagination_version: 2,
          page: params[:page],
          max_size: CURSOR_PAGINATION_MAX_SIZE
        )
      end

      # Uses order_hash_for_cursor to get an order hash from the received 'sort'
      # parameter and reorder the given scope.
      def sorted_scope_for_cursor(scope, sortable_fields = DEFAULT_SORTABLE_FIELDS, multiple_levels_allowed: false)
        custom_sort = order_hash_for_cursor(
          sortable_fields,
          multiple_levels_allowed: multiple_levels_allowed
        )

        custom_sort.present? ? scope.reorder(custom_sort) : scope
      end

      # Returns an order hash from the received 'sort' parameter:
      #
      #   * 'age'     => { age: :asc }
      #   * '-id'     => { id: :desc }
      #   * '-age,id' => { age: :desc, id: :asc }
      #   * ''        => {}
      #
      # Raises an error when we receive multiple levels and they're not allowed.
      # Raises an error when we receive a parameter that is not part of the sortable fields.
      def order_hash_for_cursor(sortable_fields, multiple_levels_allowed: false)
        sort = params[:sort].to_s.split(',')

        raise_invalid_sort_for_cursor if sort.size > 1 && !multiple_levels_allowed

        sort.each_with_object({}) do |attr, hash|
          if attr[0] == '-'
            attr_name = sortable_fields[attr[1..-1]]
            raise_invalid_sort_for_cursor unless attr_name
            hash[attr_name.to_sym] = :desc
          else
            attr_name = sortable_fields[attr]
            raise_invalid_sort_for_cursor unless attr_name
            hash[attr_name.to_sym] = :asc
          end
        end
      end

      def raise_invalid_sort_for_cursor
        raise Zendesk::CursorPagination::InvalidPaginationParameter, "sort is not valid"
      end
    end
  end
end
