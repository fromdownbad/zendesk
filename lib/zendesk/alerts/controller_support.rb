module Zendesk
  module Alerts
    module ControllerSupport
      def self.included(base)
        base.helper_method :current_user_alerts
      end

      protected

      def current_user_alerts(options = {})
        options[:custom] = []
        options[:custom] << :remote_auth_retired if auth_via?(:remote) && current_user.is_admin?

        Alert.for(current_user, options)
      end
    end
  end
end
