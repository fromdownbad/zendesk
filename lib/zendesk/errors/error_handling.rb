module Zendesk::Errors::ErrorHandling
  protected

  def render_failure(args = {})
    @args = args
    warden.custom_failure! if @args[:status] == :unauthorized

    # Retarded accept headers sent by IE make the browser show the default 404 page instead of our customized page: http://tinyurl.com/2fl4tbe
    request.format = :html if ie_request?

    respond_to do |format|
      format.html      { render shared_parameters.merge(theme: error_theme) }
      format.mobile_v2 { render shared_parameters.merge(theme: error_theme) }
      format.xml       { render shared_parameters }
      format.json      { render shared_parameters }
      format.any       { render plain: any_error_text, status: status, layout: false }
    end

    false
  end

  private

  def layout_for_error
    if current_account
      'help_center'
    elsif !current_user || !current_user.id || current_user.is_end_user?
      'user'
    else
      'agent'
    end
  end

  def any_error_text
    "#{@args[:title]}\n#{@args[:message]}".strip
  end

  def shared_parameters
    {template: template, locals: @args, status: status, layout: layout_for_error}
  end

  def ie_request?
    msie? && request.accepts.compact.map(&:to_sym).include?(:all)
  end

  def error_theme
    if [:html, :mobile_v2].include?(request.format.symbol)
      layout_for_role
    else
      false
    end
  end

  def status
    @args[:status] || :internal_server_error
  end

  def template
    if current_account && request.format == :html
      'shared/_error_hc_message'
    else
      'shared/_message'
    end
  end
end
