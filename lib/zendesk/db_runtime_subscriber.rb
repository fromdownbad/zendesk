module Zendesk
  class DbRuntimeSubscriber < ActiveSupport::LogSubscriber
    def process_action(event)
      current_span = Datadog.tracer.active_span
      payload      = event.payload

      if current_span && payload.key?(:db_runtime)
        db_runtime = payload[:db_runtime].to_f.round(3)
        current_span.set_tag('zendesk.db.sql_time', db_runtime)
      end
    end
  end
end

Zendesk::DbRuntimeSubscriber.attach_to :action_controller
