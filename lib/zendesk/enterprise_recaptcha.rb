# We expect this module to be used with controller
# verify_recaptcha is defined on controllers

module Zendesk::EnterpriseRecaptcha
  private

  RECAPTCHA_VERIFICATION_URL = "https://recaptchaenterprise.googleapis.com/v1beta1/projects/sunco-recaptcha/assessments".freeze

  def valid_recaptcha?(request)
    if current_account.has_orca_classic_recaptcha_enterprise?
      verify_enterprise_recaptcha(request)
    else
      verify_recaptcha.tap do |result|
        if result
          statsd_client.increment(:verification, tags: ['result:success_free'])
        else
          statsd_client.increment(:verification, tags: ['result:failed_free'])
        end
      end
    end
  end

  def verify_enterprise_recaptcha(request)
    recaptcha_site_key = ENV.fetch('ZENDESK_RECAPTCHA_ENTERPRISE_SITE_KEY')
    api_key = ENV.fetch('ZENDESK_RECAPTCHA_ENTERPRISE_API_KEY')
    g_recaptcha_response = request.request_parameters['g-recaptcha-response']
    verify_hash = { "event": { "token": g_recaptcha_response, "siteKey": recaptcha_site_key} }
    http = Net::HTTP
    uri = URI.parse("#{RECAPTCHA_VERIFICATION_URL}?key=#{api_key}")
    header = { 'Content-Type': 'text/json', 'Referer': request.host }
    http_instance = http.new(uri.host, uri.port)
    http_instance.read_timeout = http_instance.open_timeout = 3
    http_instance.use_ssl = true if uri.port == 443
    req = Net::HTTP::Post.new(uri.request_uri, header)
    req.body = verify_hash.to_json
    response = http_instance.request(req)
    JSON.parse(response.body).dig('tokenProperties', 'valid').tap do |token|
      if token
        statsd_client.increment(:verification, tags: ['result:success_ent'])
      else
        statsd_client.increment(:verification, tags: ['result:failed_ent_token'])
      end
    end
  rescue StandardError => e
    Rails.logger.error("recaptcha_verification error: #{e.backtrace.join("\n")}")
    ZendeskExceptions::Logger.record(
      e,
      location: self,
      message: "recaptcha_verification error: #{e.message}",
      fingerprint: 'recaptcha_verification'
    )
    statsd_client.increment(:verification, tags: ['result:failed_ent_serror'])
    false
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'google.recaptcha')
  end
end
