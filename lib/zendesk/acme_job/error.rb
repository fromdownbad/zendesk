module Zendesk::AcmeJob
  class Error < StandardError
  end
  class WrappedError < Error
    attr_reader :original

    def initialize(original, message = nil)
      @original = original
      @message = message
    end

    def to_s
      "#{original.class}: #{original}"
    end
  end

  class RetryError < WrappedError
    attr_reader :retry_in

    def initialize(original, retry_in: 300)
      @original = original
      @retry_in = retry_in
    end
  end

  class FatalError < WrappedError
  end

  class CertificateUploadFailure < Error
    attr_reader :error_code

    def initialize(error_code)
      @error_code = error_code
    end
  end

  class AuthorizationFailedError < Error
    attr_reader :status

    def initialize(status, message = nil)
      @status = status
      @message = message
    end
  end

  class AuthorizationPendingError < Error
    attr_reader :authorizations

    def initialize(authorizations)
      @authorizations = authorizations
    end
  end

  class InstallIntermediateError < Error; end
  class CertificateNotActiveError < Error; end

  class RegistrationError < WrappedError
  end

  class CertificateRequestError < WrappedError
  end

  class AuthorizationError < WrappedError
  end

  class JobAlreadyRunning < Error
  end
end
