require 'zendesk/internal_api/client'

module Zendesk::AnswerBotService
  class InternalApiClient
    ANSWER_BOT_SERVICE_ROOT = '/api/v2/answer_bot'.freeze
    DEFLECTION_ENDPOINT = '/deflection'.freeze
    AVAILABILTY_ENDPOINT = '/availability'.freeze

    CONNECTION_ERRORS = [
      SocketError,
      Errno::ETIMEDOUT,
      Errno::EHOSTUNREACH,
      Errno::ECONNRESET,
      Net::OpenTimeout,
      Faraday::TimeoutError,
      Faraday::ConnectionFailed,
      OpenSSL::SSL::SSLError
    ].freeze

    CLIENT_ERRORS = [
      Kragle::UnprocessableEntity,
      Kragle::BadRequest,
      Kragle::Forbidden,
      Kragle::ResourceNotFound
    ].freeze

    def initialize(account)
      @account = account
    end

    def deflect(ticket:, deflection_channel_id:, deflection_channel_reference_id:, label_names:)
      params = deflection_params(
        ticket: ticket,
        options: {
          label_names: label_names, # either an array of strings, or nil.
          via_id: ticket.via_id,
          deflection_channel_id: deflection_channel_id,
          deflection_channel_reference_id: deflection_channel_reference_id
        }
      )
      fetch_deflection(params)
    end

    def available(deflection_channel_id)
      result = check_availability(deflection_channel_id)
      result.empty? ? false : result['available']
    end

    def delete_embeddings
      connection.delete("/api/v2/answer_bot/embeddings/private/articles/account/#{@account.id}/destroy")
    rescue StandardError => e
      Rails.logger.warn("failed to delete embeddings after account move for account #{@account.id} with error #{e}")
    end

    private

    def fetch_deflection(params)
      connection.post(deflection_endpoint, params).body
    rescue *CONNECTION_ERRORS
      {}
    rescue *CLIENT_ERRORS => e
      message = "ticket_deflection.batch_analytics.client_error -- #{e.class} -- #{e.response.body}"
      Rails.logger.warn(message)
      {}
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: "Could not fetch deflection articles for ticket: #{params[:ticket_id]} account: #{@account.id}", fingerprint: 'd5cb08605890ebc916b8658306a5ab488ad40502')
      {}
    end

    def deflection_params(ticket:, options: {})
      {
        ticket_id: ticket.nice_id,
        user_id: ticket.requester_id,
        brand_id: ticket.brand_id,
        subject: ticket.subject || '', # customers can and do hide this from end users so it becomes nil. Empty > description for the models
        description: ticket.description,
        automatic_answers_threshold: @account.settings.automatic_answers_threshold
      }.merge(options)
    end

    def deflection_endpoint
      ANSWER_BOT_SERVICE_ROOT + DEFLECTION_ENDPOINT
    end

    def connection
      @connection ||= KragleConnection.build_for_answer_bot_service(@account)
    end

    def check_availability(deflection_channel_id)
      url = ANSWER_BOT_SERVICE_ROOT + AVAILABILTY_ENDPOINT
      params = { deflection_channel_id: deflection_channel_id }
      connection.get(url, params).body
    rescue *CONNECTION_ERRORS
      {}
    rescue *CLIENT_ERRORS => e
      message = "answer_bot.availability.client_error -- #{e.class} -- #{e.response.body}"
      Rails.logger.warn(message)
      {}
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: "Could not fetch availability for account: #{@account.id}, channel_id: #{deflection_channel_id}", fingerprint: 'd5cb08605890ebc916b8658306a5ab488ad40502')
      {}
    end
  end
end
