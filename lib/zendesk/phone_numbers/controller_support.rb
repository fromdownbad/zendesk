module Zendesk
  # FIXME: Cannot be Voice because then Zendesk::Voice takes precedence over ::Voice and we get an exception
  module PhoneNumbers
    module ControllerSupport
      include ::Voice::Core::NumberSupport

      def self.included(base)
        base.send(:include, ::Voice::Core::NumberSupport)
      end

      protected

      def find_by_number(number)
        current_account.phone_numbers.find_by_number(::Voice::Core::NumberSupport::E164Number.internal(number))
      end
    end
  end
end
