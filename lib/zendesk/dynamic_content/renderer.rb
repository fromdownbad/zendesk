module Zendesk
  module DynamicContent
    class Renderer
      # Will match a general placeholder format of {{AAA.ZZZ}}
      # This will not match if the first brace is escaped. i.e \{{AAA.ZZZ}}
      #
      # $1 => the whole placeholder: '{{AAA.ZZZ}}'
      # $2 => the identifier prefix: 'AAA'
      # $3 => the identifier itself: 'ZZZ'
      PLACEHOLDER = /(?<!\\)({{ *(\w+)\.(\w+) *}})/

      # Only render nested dynamic content 5 levels deep
      MAX_NESTING = 5

      # The prefixes saying where to look for an identifer in a placeholder
      DYNAMIC_CONTENT_PREFIX = 'dc'.freeze
      SYSTEM_CONTENT_PREFIX  = 'zd'.freeze

      # This will return a string where the placeholders have been expanded if
      # possible.
      #
      # Instantiate a new renderer and immediately render and return with the
      # given text. If a possible substitution is not found for the placeholder
      # then the placeholder will be left in the string.
      #
      # Params:
      #  - account: the Account to use when finding dynamic content
      #  - locale: the TranslationLocale (language) we want the content in
      #  - text: the String to search and replace placeholders in
      #  - options: set of options accepted. Right now only :dc_cache is used
      #
      def self.render(account, locale, text, options = {})
        new(account, locale, options).render(text)
      end

      def initialize(account, locale = ::I18n.translation_locale, options = {})
        @account  = account
        @locale   = locale
        @dc_cache = options[:dc_cache] || dc_cache
      end

      # This is a special render method for the Role class that checks arturo
      # features and account settings.
      def render_role_name(role_name)
        if @account.has_dc_in_role_names? || @account.settings.default_roles_with_sys_dc?
          render(role_name)
        else
          role_name
        end
      end

      def render(text)
        MAX_NESTING.times do
          break unless text.gsub!(PLACEHOLDER) { lookup($2, $3, $1) }
        end

        text
      end

      private

      def dc_cache
        @dc_cache ||= DynamicContent::AccountContent.cache(@account, @locale)
      end

      def zd_cache
        @zd_cache ||= DynamicContent::SystemContent.cache(@locale)
      end

      def lookup(prefix, identifier, placeholder)
        case prefix
        when DYNAMIC_CONTENT_PREFIX
          dc_cache.fetch identifier, placeholder
        when SYSTEM_CONTENT_PREFIX
          zd_cache.fetch identifier, placeholder
        else
          placeholder
        end
      end
    end
  end
end
