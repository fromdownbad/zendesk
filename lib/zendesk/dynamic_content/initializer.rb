module Zendesk
  module DynamicContent
    class Initializer
      def initialize(account, params)
        @account = account
        @params = params
      end

      def new_variant(params = @params[:variant])
        scope.new(normalized_parameters(params))
      end

      def variant(id = @params[:id])
        scope.find(id)
      end

      def variants
        scope
      end

      def item
        @item ||= @account.cms_texts.find(@params[:item_id])
      end

      def scope
        @scope ||= item.variants
      end

      def normalized_parameters(params = @params[:variant])
        params ||= {}
        ATTRIBUTE_MAPPING.each do |k, v|
          params[v] = params.delete(k) if params[k]
        end
        params
      end

      ATTRIBUTE_MAPPING = {
        locale_id: :translation_locale_id,
        content: :value,
        default: :is_fallback
      }.freeze
    end
  end
end
