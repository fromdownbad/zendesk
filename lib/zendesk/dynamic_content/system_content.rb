module Zendesk
  module DynamicContent
    module SystemContent
      ROLE_PLACEHOLDERS = {
        staff:       '{{zd.staff_role}}',
        team_leader: '{{zd.team_leader_role}}',
        advisor:     '{{zd.advisor_role}}',
        light_agent: '{{zd.light_agent_role}}'
      }.freeze

      # Lookup the value for system placholders based on the identifier.
      #
      # This takes an optional locale argument to be consistent with the other
      # dynamic content rendering methods.  It will default to the current
      # locale used for translations otherwise.
      #
      # Params:
      #  - locale: the locale to translate the content into
      #
      def self.cache(locale = ::I18n.locale)
        {
          # Views
          'your_unsolved_tickets'     => ::I18n.t('txt.default.views.your_unsolved.title', locale: locale),
          'unsolved_tickets_in_group' => ::I18n.t('txt.default.views.group_unsolved.title', locale: locale),
          'new_tickets_in_group'      => ::I18n.t('txt.default.views.new_in_groups.title', locale: locale),
          'recently_solved_tickets'   => ::I18n.t('txt.default.views.recently_solved.title', locale: locale),
          'all_unsolved_tickets'      => ::I18n.t('txt.default.views.all_unsolved.title', locale: locale),
          'unassigned_tickets'        => ::I18n.t('txt.default.views.unassigned.title', locale: locale),
          'recently_updated_tickets'  => ::I18n.t('txt.default.views.recently_updated.title', locale: locale),
          'pending_tickets'           => ::I18n.t('txt.default.views.pending.title', locale: locale),
          'current_tasks'             => ::I18n.t('txt.default.views.current_tasks.title', locale: locale),
          'overdue_tasks'             => ::I18n.t('txt.default.views.overdue_tasks.title', locale: locale),
          'recently_rated_tickets'    => ::I18n.t('txt.models.account.customer_satisfaction_support.default_view_name', locale: locale),

          # Roles
          'staff_role'       => ::I18n.t('txt.default.roles.staff.name', locale: locale),
          'team_leader_role' => ::I18n.t('txt.default.roles.team_leader.name', locale: locale),
          'advisor_role'     => ::I18n.t('txt.default.roles.advisor.name', locale: locale),
          'light_agent_role' => ::I18n.t('txt.default.roles.light_agent.name', locale: locale),
          'contributor_role' => ::I18n.t('txt.default.roles.contributor.name', locale: locale),

          # Ticket fields
          'subject_field_title'           => ::I18n.t('txt.default.fields.subject.title', locale: locale),
          'subject_field_description'     => ::I18n.t('txt.default.fields.subject.description', locale: locale),
          'description_field_title'       => ::I18n.t('txt.default.fields.description.title', locale: locale),
          'description_field_description' => ::I18n.t('txt.default.fields.description.description', locale: locale),
          'status_field_title'            => ::I18n.t('txt.default.fields.status.title', locale: locale),
          'status_field_description'      => ::I18n.t('txt.default.fields.status.description', locale: locale),
          'title_field_title'             => ::I18n.t('txt.default.fields.type.title', locale: locale),
          'title_field_description'       => ::I18n.t('txt.default.fields.type.description', locale: locale),
          'priority_field_title'          => ::I18n.t('txt.default.fields.priority.title', locale: locale),
          'priority_field_description'    => ::I18n.t('txt.default.fields.priority.description', locale: locale),
          'group_field_title'             => ::I18n.t('txt.default.fields.group.title', locale: locale),
          'group_field_description'       => ::I18n.t('txt.default.fields.group.description', locale: locale),
          'assignee_field_title'          => ::I18n.t('txt.default.fields.assignee.title', locale: locale),
          'assignee_field_description'    => ::I18n.t('txt.default.fields.assignee.description', locale: locale),

          # Statuses
          'status_new'     => ::I18n.t('type.status.new', locale: locale),
          'status_open'    => ::I18n.t('type.status.open', locale: locale),
          'status_pending' => ::I18n.t('type.status.pending', locale: locale),
          'status_hold'    => ::I18n.t('type.status.hold', locale: locale),
          'status_solved'  => ::I18n.t('type.status.solved', locale: locale),
          'status_closed'  => ::I18n.t('type.status.closed', locale: locale),

          # Status descriptions
          'status_new_description'     => ::I18n.t('type.custom_status.new.description', locale: locale),
          'status_open_description'    => ::I18n.t('type.custom_status.open.description', locale: locale),
          'status_pending_description' => ::I18n.t('type.custom_status.pending.description', locale: locale),
          'status_hold_description'    => ::I18n.t('type.custom_status.hold.description', locale: locale),
          'status_solved_description'  => ::I18n.t('type.custom_status.solved.description', locale: locale)
        }
      end
    end
  end
end
