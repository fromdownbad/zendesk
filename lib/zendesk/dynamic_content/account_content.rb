module Zendesk
  module DynamicContent
    module AccountContent
      # Lookup the dynamic content values for a given account in a given language.
      #
      # This returns a hash where the keys are the dc identifiers and the
      # values are the variant values given the current user's locale. If
      # no variant is found for the account in the given language, it will
      # fetch the variant marked as default.
      #
      # Params:
      #  - account: the Account the dynamic content belongs to
      #  - locale: the TranslationLocale to find the variants for
      #
      def self.cache(account, locale)
        if account.has_cache_dynamic_content_account_content?
          Rails.cache.fetch(account.scoped_cache_key(:"dynamic_content/#{locale.locale}")) do
            fetch(account, locale)
          end
        else
          fetch(account, locale)
        end
      end

      def self.fetch(account, locale)
        Hash[
          ActiveRecord::Base.on_slave.connection.execute("SELECT ct.identifier, cv.value FROM cms_texts ct \
            INNER JOIN (select cv1.cms_text_id AS cms_text_id, IFNULL(cv2.value, cv1.value) \
            AS value FROM cms_variants cv1 LEFT OUTER JOIN cms_variants cv2 \
            ON cv1.cms_text_id = cv2.cms_text_id AND cv2.translation_locale_id = #{locale.id} AND cv2.active = 1\
            WHERE cv1.account_id = #{account.id} AND cv1.is_fallback = 1 AND cv1.active = 1) cv \
            ON ct.id = cv.cms_text_id WHERE ct.account_id = #{account.id}").to_a
        ]
      end

      private_class_method :fetch
    end
  end
end
