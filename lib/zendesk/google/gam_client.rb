module Zendesk
  module Google
    class GAMClient
      GOOGLE_API_URL_ROOT = "https://www.googleapis.com".freeze
      GOOGLE_ADMIN_API_URL_ROOT = "https://apps-apis.google.com".freeze

      def initialize(access_token:, domain:)
        @access_token = access_token
        @domain = domain
      end

      def json_connection
        Faraday.new(url: GOOGLE_API_URL_ROOT) do |connection|
          connection.adapter Faraday.default_adapter
          connection.headers['Content-Type'] = 'application/json'
          add_auth_header(connection)
        end
      end

      def xml_connection
        Faraday.new(url: GOOGLE_ADMIN_API_URL_ROOT) do |connection|
          connection.adapter Faraday.default_adapter
          connection.headers['Content-Type'] = 'application/atom+xml'
          add_auth_header(connection)
        end
      end

      ### Email Settings API
      ### https://developers.google.com/admin-sdk/email-settings/

      def create_email_forwarding_filter(email_owner_username:, forward_from_email:, forward_to_email:)
        url = filter_email_settings_url(email_owner_username)

        data = <<~XML
          <?xml version="1.0" encoding="utf-8"?>
          <atom:entry xmlns:atom="http://www.w3.org/2005/Atom" xmlns:apps="http://schemas.google.com/apps/2006">
            <apps:property name="to" value="#{forward_from_email}" />
            <apps:property name="forwardTo" value="#{forward_to_email}" />
          </atom:entry>
        XML

        Rails.logger.info "#{__method__} Request url: #{url}; data: #{data}"
        response = xml_connection.post url, data

        Rails.logger.info "#{__method__} Response: #{response.body}"

        hash = Hash.from_xml(response.body).with_indifferent_access
        return hash[:AppsForYourDomainErrors] if hash[:AppsForYourDomainErrors]
        hash[:entry] if hash[:entry]
      end

      ### Site Verification API
      ### https://developers.google.com/site-verification/v1/getting_started#auth

      def get_site_verification_token(domain:)
        url = "/siteVerification/v1/token"

        data = {
          verificationMethod: "FILE",
          site: {
            identifier: "http://#{domain}",
            type: "SITE"
          }
        }

        gam_request(:post, url, data)
      end

      def verify_site_token(domain:)
        url = "/siteVerification/v1/webResource?verificationMethod=FILE"

        data = {
          site: {
            identifier: "http://#{domain}",
            type: "SITE"
          }
        }

        gam_request(:post, url, data)
      end

      ### Directory API

      # DomainAliases.insert (https://developers.google.com/admin-sdk/directory/v1/reference/domainAliases/insert)
      def add_domain_alias(customer_id:, domain_alias:)
        url = "/admin/directory/v1/customer/#{customer_id}/domainaliases"

        data = {
          domainAliasName: domain_alias,
          parentDomainName: @domain,
        }

        gam_request(:post, url, data)
      end

      def get_domain_aliases(customer_id:)
        url = "/admin/directory/v1/customer/#{customer_id}/domainaliases"
        gam_request(:get, url)
      end

      def delete_domain_alias(customer_id:, domain_alias:)
        url = "/admin/directory/v1/customer/#{customer_id}/domainaliases/#{domain_alias}"
        gam_request(:delete, url)
      end

      # https://developers.google.com/admin-sdk/directory/v1/guides/manage-group-members
      def add_group_member(group_email:, member_email:)
        url = "/admin/directory/v1/groups/#{group_email}/members"
        data = {
          email: member_email,
          role: 'MEMBER'
        }

        gam_request(:post, url, data)
      end

      ### Group Settings API

      # https://developers.google.com/admin-sdk/groups-settings/v1/reference/groups#resource-representations
      def enable_public_group_emails(group_email:)
        url = "/groups/v1/groups/#{group_email}"
        data = { whoCanPostMessage: 'ANYONE_CAN_POST' }

        gam_request(:patch, url, data)
      end

      private

      def add_auth_header(connection)
        connection.headers['Authorization'] = "Bearer #{@access_token}"
      end

      def email_settings_url(username:, setting:)
        raise "Username cannot be nil for the Email Settings API" unless username
        "/a/feeds/emailsettings/2.0/#{@domain}/#{username}/#{setting}"
      end

      def filter_email_settings_url(username)
        email_settings_url(username: username, setting: 'filter')
      end

      def gam_request(method, url, data = nil)
        Rails.logger.info "#{__method__} Request url: #{url}; data: #{data}"

        args = [method, url]
        args.push data.to_json if data
        response = json_connection.send(*args)

        Rails.logger.info "#{__method__} Response: #{response.body}"
        body = JSON.parse(response.body)
        body.with_indifferent_access if body
      end
    end
  end
end
