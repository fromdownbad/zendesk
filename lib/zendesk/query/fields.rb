class IllegalMappingException < StandardError
end

module Zendesk::Query::Fields
  # The abstract field contains generic helper mechanisms
  class AbstractField
    # Operator mappings
    @@equality_operators               = { 'is' => '=', 'is_not' => '!=' }
    @@numeric_operators                = { 'less_than' => '<', 'greater_than' => '>' }.merge(@@equality_operators)
    @@null_operators                   = { 'is' => 'IS', 'is_not' => 'IS NOT' }
    @@match_operators                  = { 'includes' => 'REGEXP', 'not_includes' => 'NOT REGEXP' }
    @@match_operators_for_taggers      = { 'is' => 'REGEXP', 'is_not' => 'NOT REGEXP' }
    @@hours_operators                  = { 'less_than_business_hours' => '<', 'greater_than_business_hours' => '>', 'is_business_hours' => '=' }.merge(@@numeric_operators)

    # To SQL mapping
    # @@default_event_mapper  = Proc.new { |table, column, operator, value|
    #  [ "#{table}.value_reference = '#{column}' AND #{table}.value #{operator} ? ", value]
    # }

    attr_reader :identifier

    def initialize(identifier, name, type, nullable, operators)
      @identifier = identifier
      @name       = name
      @nullable   = nullable
      @type       = type
      @operators  = operators
    end

    def parse(operator, value, legal_values)
      value = nil if value.blank?
      value = value.to_i if @type == Integer && !value.nil?

      raise IllegalMappingException.new, "Field must be compared to a value" if value.nil? && !@nullable
      raise IllegalMappingException.new, "Invalid operator '#{operator.inspect}' when testing for empty field" if value.nil? && !@@null_operators.member?(operator)

      return [@identifier, @@null_operators[operator], nil] if value.nil?

      raise IllegalMappingException.new, "Illegal comparison value '#{value}' #{legal_values.inspect}" if legal_values && !legal_values.member?(value)
      raise IllegalMappingException.new, "Invalid operator '#{operator.inspect}'" if @operators[operator].nil?

      [@identifier, @operators[operator], value.to_s]
    end
  end

  class GroupField < AbstractField
    def initialize
      super('group_id', 'Group', Integer, true, @@equality_operators)
    end

    def legal_values(account = nil)
      account.groups.map(&:id)
    end
  end

  class PriorityField < AbstractField
    def initialize
      super('priority_id', 'Priority', Integer, true, @@numeric_operators)
    end

    def legal_values(_account = nil)
      PriorityType.fields
    end
  end

  class StatusField < AbstractField
    def initialize
      super('status_id', 'Status', Integer, false, @@numeric_operators)
    end

    def legal_values(_account = nil)
      StatusType.fields
    end
  end

  class ResolutionTimeField < AbstractField
    def initialize
      super('resolution_time', 'Resolution time', Integer, false, @@numeric_operators)
    end

    def legal_values(_account = nil)
      nil
    end
  end

  class TicketTypeField < AbstractField
    def initialize
      super('ticket_type_id', 'Ticket type', Integer, true, @@numeric_operators)
    end

    def legal_values(_account = nil)
      TicketType.fields
    end
  end

  class ViaField < AbstractField
    def initialize
      super('via_id', 'Via', Integer, true, @@equality_operators)
    end

    def legal_values(_account = nil)
      ViaType.fields
    end
  end

  class AssigneeField < AbstractField
    def initialize
      super('assignee_id', 'Assignee', Integer, true, @@equality_operators)
    end

    def legal_values(account = nil)
      account.agents.map(&:id)
    end
  end

  class RequesterField < AbstractField
    def initialize
      super('requester_id', 'Requester', Integer, true, @@equality_operators)
    end

    def legal_values(_account = nil)
      nil
    end
  end

  class OrganizationField < AbstractField
    def initialize
      super('organization_id', 'Organization', Integer, true, @@equality_operators)
    end

    def legal_values(_account = nil)
      nil
    end
  end

  class TagsField < AbstractField
    def initialize
      super('current_tags', 'Tag', String, false, @@match_operators)
    end

    def legal_values(_account = nil)
      nil
    end

    def parse(operator, value, _legal_values)
      raise IllegalMappingException.new, "Invalid operator '#{operator.inspect}'" if @operators[operator].nil?
      [@identifier, @operators[operator], value.to_s.downcase.split(' ').sort.join(' ')]
    end
  end

  class MetricsField < AbstractField
    def initialize(identifier)
      if identifier.include?("in_minutes")
        super(identifier, identifier.titleize, Float, false, @@hours_operators)
      else
        super(identifier, identifier.titleize, Integer, false, @@numeric_operators)
      end
    end

    def legal_values(_account = nil)
      nil
    end
  end

  class CustomField < AbstractField
    def initialize(identifier)
      super(identifier, identifier.titleize, String, false, @@match_operators_for_taggers)
    end

    def legal_values(_account = nil)
      nil
    end
  end

  class SatisfactionScoreField < AbstractField
    def initialize
      super('satisfaction_score', 'Satisfaction Score', Integer, false, @@numeric_operators)
    end

    def legal_values(_account = nil)
      SatisfactionType.fields
    end
  end

  class TranslationLocaleField < AbstractField
    def initialize
      super('locale_id', "Requester's Language", Integer, false, @@equality_operators)
    end

    def legal_values(account = nil)
      account.available_languages.map(&:id)
    end
  end

  class TicketFormField < AbstractField
    def initialize
      super('ticket_form_id', "Ticket Form", Integer, true, @@equality_operators)
    end

    def legal_values(account = nil)
      account.ticket_forms.map(&:id)
    end
  end

  class BrandField < AbstractField
    def initialize
      super('brand_id', "Brand", Integer, true, @@equality_operators)
    end

    def legal_values(account = nil)
      account.brands.active.map(&:id)
    end
  end

  def self.lookup(field, account = nil)
    fields(account).find { |f| f.identifier == field }
  end

  def self.metrics_fields
    TicketMetricSet.attributes_for_reports.map { |attribute| MetricsField.new(attribute) }
  end

  LOOKUP_FIELDS = ([
    PriorityField.new, StatusField.new, TicketTypeField.new, ViaField.new, GroupField.new,
    AssigneeField.new, RequesterField.new, ResolutionTimeField.new, TagsField.new, OrganizationField.new,
    SatisfactionScoreField.new, TranslationLocaleField.new, TicketFormField.new, BrandField.new
  ] + metrics_fields).freeze

  def self.fields(account = nil)
    LOOKUP_FIELDS + custom_fields(account)
  end

  def self.custom_fields(account)
    return [] unless account
    (account.field_taggers.active + account.field_checkboxes.active).map { |field| CustomField.new("ticket_fields_#{field.id}") }
  end
end
