# The arturo scientist
#
# Feature off: only execute control
# Feature rollout: return control, but compare against candidate, report exceptions
# Feature on: only execute candidate
#
# Zendesk::Artist.compare(
#   :foobar, current_account,
#   control: -> {  },
#   candidate: -> {  }
# )
#
# Optional:
#   report_mismatch: (candidate, control) -> { Rails.logger.error('..') }
#   eq: (candidate, control) -> { candidate.custom_compare == control.custom_compare }
module Zendesk
  class Artist
    STATSD_NAMESPACE = 'artist'.freeze

    class << self
      attr_accessor :statsd

      def compare(feature_name, account, control:, candidate:, report_mismatch: nil, eq: nil)
        feature = Arturo::Feature.find_feature(feature_name)
        if feature && feature.phase == 'on'
          candidate.call
        elsif feature && feature.enabled_for?(account)
          control_result, control_time = report_timing("#{feature_name}.control", &control)
          candidate_result, candidate_time = capture_exceptions(feature_name) do
            report_timing("#{feature_name}.candidate", &candidate)
          end
          if candidate_time
            statsd.timing "#{STATSD_NAMESPACE}.#{feature_name}.delta", (control_time - candidate_time)
          end
          success = equal?(control_result, candidate_result, eq)
          report_mismatch.call(control_result, candidate_result) if !success && report_mismatch
          statsd.increment "#{STATSD_NAMESPACE}.#{feature_name}.#{success ? "matched" : "mismatched"}"
          control_result
        else
          control.call
        end
      end

      private

      def equal?(control_result, candidate_result, eq)
        if eq
          eq.call(control_result, candidate_result)
        else
          control_result == candidate_result
        end
      end

      def report_timing(metric)
        result = nil
        time = Benchmark.realtime { result = yield }
        statsd.timing "#{STATSD_NAMESPACE}.#{metric}", time
        [result, time]
      end

      def capture_exceptions(feature)
        yield
      rescue
        ZendeskExceptions::Logger.record($!, location: self, message: "Artist candidate error for #{feature} -- #{$!.message}", fingerprint: 'fb295b3da89ff2451b6c3e44d0428cb20e1d598e')
        :exception
      end
    end
  end
end
