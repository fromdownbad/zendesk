require 'kragle'

module Zendesk
  module Revere
    class ApiClient
      def update_subscription(user, subscription)
        account = user.account
        if subscription && arturo_enabled?(:new_subscribers_params) == false
          # subscribe
          user_data = {
              zendesk_user_id: user.id,
              alert_type: 'EmailAlert',
              alert_destination: user.email,
              pod: account.pod_id,
              status: "subscribed",
              account: {
                  zendesk_account_id: account.id,
                  subdomain: account.subdomain,
                  uses_help_center: [:enabled, :restricted].include?(account.settings.help_center_state),
                  uses_talk: account.voice_enabled?,
                  uses_chat: !!account.zopim_integration
              }
          }
          connection.post('/api/internal/subscribers.json', subscribers: [user_data])
        # Arturo feature is on - GA
        elsif subscription && arturo_enabled?(:new_subscribers_params)
          # subscribe with new params
          user_data = {
            alert_type: 'EmailAlert',
            alert_destination: user.email,
            pod: account.pod_id,
            sudomain: account.subdomain,
            status: "subscribed"
          }
          connection.post('/api/internal/subscribers.json', subscribers: [user_data])
        else
          # unsubscribe
          if arturo_enabled?(:new_subscribers_params)
            delete_from(user.email, account.subdomain)
          else
            connection.delete("/api/internal/accounts/#{account.id}/subscribers/#{user.id}.json?alert_type=EmailAlert") unless arturo_enabled?(:new_subscribers_params)
          end
        end
        true
      end

      def unsubscribed
        Rails.logger.info("Processing unsubscribed users from Revere")
        response = connection.get("/api/internal/subscribers/unsubscribed.json")
        if response.status != 200
          raise "Failed to get unsubscribed users from Revere. Status Code (#{response.status})"
        end

        response.body['unsubscribed'].map { |u| Hashie::Mash.new(u) }
      end

      def delete_unsubscribed(unsubscribed_revere_ids)
        Rails.logger.info("Removing unsubscribed users from Revere. ids [#{unsubscribed_revere_ids.join(',')}]")
        # Faraday assumes DELETE uses GET style in-url params.
        # While the spec allows for either params or body.
        # So we must force the body to be used when in Faraday.
        response = connection.delete do |req|
          req.url "/api/internal/subscribers/unsubscribed.json"
          req.body = { unsubscribed: unsubscribed_revere_ids }
        end
        if response.status != 204 # :no_content
          raise "Failed to delete unsubscribed users from Revere. Status Code (#{response.status})"
        end
        true
      end

      def delete_from(email, subdomain)
        Rails.logger.info("Removing unsubscribed users from Revere. [#{email}, #{subdomain}]")
        # Faraday assumes DELETE uses GET style in-url params.
        # While the spec allows for either params or body.
        # So we must force the body to be used when in Faraday.
        response = connection.delete do |req|
          req.url "/api/internal/subscribers/delete_from.json"
          req.body = { email: email, subdomain: subdomain, alert_type: 'EmailAlert' }
        end
        if response.status != 204 # :no_content
          raise "Failed to delete unsubscribed user from Revere. Status Code (#{response.status})"
        end
        true
      end

      def arturo_enabled?(name)
        feature = Arturo::Feature.find_feature(name)
        feature.present? && !feature.off?
      end

      private

      def env_revere_url
        if ENV['REVERE_SERVICE_URL']
          ENV['REVERE_SERVICE_URL']
        elsif Rails.env.master?
          "https://status-master.zendesk.com"
        elsif Rails.env.staging?
          "https://status-staging.zendesk.com"
        elsif Rails.env.production?
          "https://status.zendesk.com"
        else
          raise "No default revere url for environment #{Rails.env}. You must provide env var REVERE_SERVICE_URL"
        end
      end

      def connection
        revere_url = env_revere_url
        api_key = ENV.fetch('REVERE_API_KEY')

        connection = ::Kragle.new('revere', signing: false) do |faraday|
          faraday.url_prefix = revere_url
          faraday.headers["Authorization"] = "Token token=#{api_key}"
        end

        connection
      end
    end
  end
end
