require 'zendesk/revere/api_client'

module Zendesk
  module Revere
    module HasApiClient
      private

      def revere_api_client
        @revere_api_client ||= Zendesk::Revere::ApiClient.new
      end
    end
  end
end
