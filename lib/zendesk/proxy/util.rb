module Zendesk
  module Proxy
    class Util
      def self.is_valid_domain?(uri) # rubocop:disable Naming/PredicateName
        domain = extract_domain(uri)

        return false if domain.blank?
        return false if domain == "localhost"
        return false if domain =~ /\.?zendesk(\-staging)?.com$/
        return false if domain =~ /^(10|127|192\.168)\./
        return false if domain =~ /^172\.(#{(16..31).to_a.join('|')})\./

        true
      end

      def self.extract_domain(uri)
        unless uri.blank?
          uri = "http://#{uri}" unless uri =~ /^[a-z]+?\:\/\//
          uri = Addressable::URI.parse(uri)
          return if uri.host.blank? || (uri.scheme !~ /^https?$/)
          uri.host.downcase
        end
      rescue Addressable::URI::InvalidURIError
        nil
      end
    end
  end
end
