require 'zendesk/proxy/rest_client'

module Zendesk::Proxy
  class Resource < ::REST::Client
    attr_accessor :cache

    # Zendesk wrapper for the resource. Initialize using a block, like so:
    #
    # @service = Zendesk::Proxy::Resource.new do |r|
    #   r.host   = params[:domain]
    #   r.userid = params[:user]
    # end
    def initialize
      raise "Initialize using a block" unless block_given?
      yield(self)
    end

    def inspect
      { host: @host, userid: @userid, password: @password, use_ssl: @use_ssl,
        user_agent: @user_agent, media_type: @media_type, cache_gets: @cache}.inspect
    end

    def cache_key
      [@host, @userid, @password, @use_ssl, @user_agent, @media_type, @cache].join('_').hash
    end
  end
end
