# I should probably have used this one instead of rolling my own:
# http://www.coffeepowered.net/2008/09/27/powerful-easy-dry-multi-format-rest-apis/

module Zendesk::Proxy
  class XmlConverter
    # stolen from activesupport-2.3.14/lib/active_support/core_ext/hash/conversions.rb
    XML_PARSING = { # rubocop:disable Style/MutableConstant
      "symbol"       => proc  { |symbol|  symbol.to_sym },
      "date"         => proc  { |date|    ::Date.parse(date) },
      "datetime"     => proc  { |time|    ::Time.parse(time).utc rescue ::DateTime.parse(time).utc },
      "integer"      => proc  { |integer| integer.to_i },
      "float"        => proc  { |float|   float.to_f },
      "decimal"      => proc  { |number|  BigDecimal(number) },
      "boolean"      => proc  { |boolean| %w[1 true].include?(boolean.strip) },
      "string"       => proc  { |string|  string.to_s },
      "yaml"         => proc  { |yaml|    YAML.load(yaml) rescue yaml },
      "base64Binary" => proc  { |bin|     Base64.decode64(bin) },
      "file"         => proc do |file, entity|
        f = StringIO.new(Base64.decode64(file))
        f.extend(FileLike)
        f.original_filename = entity['name']
        f.content_type = entity['content_type']
        f
      end
    }

    XML_PARSING.update(
      "double"   => XML_PARSING["float"],
      "dateTime" => XML_PARSING["datetime"]
    )

    XML_PARSING.freeze

    def self.xml_to_hash(xml)
      hash = {}
      doc = Nokogiri::XML(xml)
      doc.elements.each do |element|
        hash[element.name.tr('-', '_')] = element_to_value(element)
      end
      hash
    end

    def self.element_to_value(element)
      if element.children.empty?
        nil
      elsif element.children.size == 1 && element.children.first.is_a?(Nokogiri::XML::Text)
        child = element.children.first
        if parser = XML_PARSING[element["type"]]
          parser.call(child.to_s)
        else
          child.to_s
        end
      else
        if element['type'] == 'array'
          element.children.reject { |e| e.children.empty? }.map { |e| element_to_value(e) }
        else
          hash = {}
          element.elements.each do |e|
            name = e.name.tr('-', '_')
            hash[name] = if hash[name]
              Array(hash[name]) + Array(element_to_value(e))
            else
              element_to_value(e)
            end
          end
          hash
        end
      end
    end
  end
end
