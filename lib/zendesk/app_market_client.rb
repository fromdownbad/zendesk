require 'zendesk/internal_api/client'

module Zendesk
  class AppMarketClient < Zendesk::InternalApi::Client
    APPS_REQUIREMENTS_TYPES = %w[automations macros targets views ticket_fields triggers user_fields].freeze

    class ApiError < StandardError
    end

    def app_id_requirements(klass, account)
      type = klass.name.underscore.pluralize
      raise "Unsupported type #{type.inspect}" unless APPS_REQUIREMENTS_TYPES.include?(type)

      requirements = fetch_requirements
      resource_ids = account.public_send(type).pluck(:id)

      requirements.each_with_object({}) do |reqs, mapping|
        next unless reqs['requirement_type'] == type && resource_ids.include?(reqs['requirement_id']) && reqs['installation']
        mapping[reqs['requirement_id']] = reqs['installation']['app_id']
      end
    end

    def requirements(klass, account)
      type = klass.name.underscore.pluralize
      raise "Unsupported type #{type.inspect}" unless APPS_REQUIREMENTS_TYPES.include?(type)

      requirements = fetch_requirements
      resource_ids = account.public_send(type).pluck(:id)

      requirements.each_with_object({}) do |reqs, mapping|
        next unless reqs['requirement_type'] == type && resource_ids.include?(reqs['requirement_id']) && reqs['installation']
        mapping[reqs['requirement_id']] = reqs['installation']['settings']['title']
      end
    end

    def installations(ids)
      fetch_requirements(ids).
        each_with_object({}) do |required_rule, requirements|
          next unless required_rule.key?('requirement_id')

          requirements.store(
            required_rule['requirement_id'],
            required_rule['installation']
          )
        end
    end

    def fetch_installations
      connection.get do |req|
        req.url '/api/v2/apps/installations.json'
      end.body['installations']
    end

    def feature_change(feature, old_value, new_value)
      response = connection.post do |req|
        req.url '/api/v2/apps/feature_changes.json'
        req.params[:feature] = feature
        req.params[:old_value] = old_value
        req.params[:new_value] = new_value
      end
      raise ApiError, response.body unless response.status == 201
    end

    def update_installation(installation_id, params)
      connection.put do |req|
        req.url "/api/v2/apps/installations/#{installation_id}.json"
        req.params = params
      end
    end

    private

    def fetch_requirements(ids = nil)
      connection.options.timeout = 10
      connection.get do |req|
        req.url '/api/v2/apps/requirements.json'

        req.params[:includes] = 'installation'
        req.params[:ids] = ids if ids.present?
      end.body.fetch('requirements')
    rescue StandardError => e
      record_requirement_fetch_error(e)
      {}
    end

    def record_requirement_fetch_error(exception)
      Rails.logger.warn("Error occurred when requesting apps requirements for #{connection.url_prefix} -- #{exception.message}")
      Rails.application.config.statsd.client.increment('internal_api.error.app_market_requirements')
    end
  end
end
