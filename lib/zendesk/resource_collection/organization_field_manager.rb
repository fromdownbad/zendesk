require 'zendesk/resource_collection/resource_manager'

class ResourceCollection::OrganizationFieldManager < ResourceCollection::ResourceManager
  def create(params)
    manager.build(params).tap(&:save!)
  end

  def update(params, organization_field)
    manager.update(organization_field, params).tap(&:save!)
  end

  private

  def manager
    Zendesk::CustomField::CustomFieldManager.new(account, user, 'Organization')
  end
end
