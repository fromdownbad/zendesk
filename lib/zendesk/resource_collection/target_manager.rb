require 'zendesk/resource_collection/resource_manager'

class ResourceCollection::TargetManager < ResourceCollection::ResourceManager
  def create(params)
    manager.build(params).tap(&:save!)
  end

  def update(params, target)
    manager.set(target, params).tap(&:save!)
  end

  private

  def manager
    Zendesk::Targets::Initializer.new(account: account, via: :api)
  end
end
