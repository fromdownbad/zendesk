require 'zendesk/resource_collection/resource_manager'

class ResourceCollection::TicketFieldManager < ResourceCollection::ResourceManager
  def create(params)
    manager.build(params).tap(&:save!)
  end

  def update(params, ticket_field)
    manager.update(ticket_field, params).tap(&:save!)
  end

  private

  def manager
    Zendesk::Tickets::TicketFieldManager.new(account)
  end
end
