class ResourceCollection::ResourceManager
  attr_reader :account, :user, :type_name

  def initialize(account, user, options = {})
    @account = account
    @user = user
    @type_name = options[:type_name]
  end

  def destroy(resource)
    resource.destroy
  end
end
