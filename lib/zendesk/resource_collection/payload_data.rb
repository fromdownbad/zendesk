class ResourceCollection::PayloadData
  attr_reader :payload

  def initialize(payload)
    @payload = payload
  end

  def all_resources
    @all_resources ||= payload.flat_map do |type_name, definitions|
      definitions.map do |identifier, definition|
        {
          type_name: type_name,
          identifier: identifier,
          definition: HashWithIndifferentAccess.new(definition)
        }
      end
    end
  end

  def passive_resources
    @passive_resources ||= all_resources.select do |resource_info|
      %w[ticket_fields user_fields organization_fields targets].include?(resource_info[:type_name])
    end
  end

  def relational_resources
    @relational_resources ||= all_resources - passive_resources
  end

  def resources_ordered_by_dependency
    [*passive_resources, *relational_resources]
  end
end
