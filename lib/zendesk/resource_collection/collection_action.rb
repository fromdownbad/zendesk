class ResourceCollection::CollectionAction
  attr_reader :collection, :account, :user, :payload_data

  def initialize(collection, options = {})
    @collection = collection
    @payload_data = options[:payload_data]
    @account = options[:account]
    @user = options[:user]
  end
end
