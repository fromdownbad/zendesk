require 'zendesk/resource_collection/collection_action'
require 'zendesk/resource_collection/collection_create'
require 'zendesk/resource_collection/update_relational_definitions_service'

class ResourceCollection::CollectionUpdate < ResourceCollection::CollectionAction
  def update_resources
    # payload_data gets modified during every iteration by UpdateRelationalDefinitionsService
    payload_data.resources_ordered_by_dependency.map do |resource_info|
      resource = update_resource(resource_info)
      yield(resource)
      # we need to reload the fields, otherwise they don't exists for the next step.
      account.expire_scoped_cache_key(:ticket_fields)
      resource
    end
  end

  def update_resource(resource_info)
    type_name, identifier, definition = resource_info.values_at(:type_name, :identifier, :definition)
    Rails.logger.info("Updating resource #{identifier}")

    unless ResourceCollectionResource.exists?(account_id: account, identifier: identifier, resource_collection_id: collection.id)
      return collection_create.create_resource(resource_info)
    end

    manager = ResourceCollectionResource.manager_for(account, user, type_name)
    resource_reference = collection.collection_resources.find_by_identifier(identifier)
    resource_reference.resource.overwrite_resource_permission = true
    manager.update(definition, resource_reference.resource)

    if payload_data.passive_resources.include?(resource_info)
      ResourceCollection::UpdateRelationalDefinitionsService.new(
        identifier, resource_reference.resource, payload_data
      ).perform!
    end

    resource_reference.tap(&:touch)
  end

  private

  def collection_create
    @collection_create ||= ResourceCollection::CollectionCreate.new(collection,
      payload_data: payload_data, account: account, user: user)
  end
end
