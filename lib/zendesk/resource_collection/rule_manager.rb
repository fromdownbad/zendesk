require 'zendesk/resource_collection/resource_manager'

class ResourceCollection::RuleManager < ResourceCollection::ResourceManager
  def create(params)
    build(type_key => params).tap(&:save!)
  end

  def update(params, rule)
    build(type_key => params, :id => rule.id, :rule => rule).tap(&:save!)
  end

  def destroy(rule)
    rule.soft_delete(validate: false)
  end

  private

  def type_key
    type_name.singularize.to_sym
  end

  def build(params)
    Zendesk::Rules::Initializer.new(
      account,
      account.send(type_name),
      params,
      type_key
    ).rule.tap do |rule|
      rule.current_user = user
    end
  end
end
