require 'zendesk/resource_collection/collection_action'
require 'zendesk/resource_collection/update_relational_definitions_service'

class ResourceCollection::CollectionCreate < ResourceCollection::CollectionAction
  def create_resources
    # payload_data gets modified during every iteration by UpdateRelationalDefinitionsService
    payload_data.resources_ordered_by_dependency.map do |resource_info|
      resource = create_resource(resource_info)
      yield(resource)
      # we need to reload the fields, otherwise they don't exists for the next step.
      account.expire_scoped_cache_key(:ticket_fields)
      resource
    end
  end

  def create_resource(resource_info)
    type_name, identifier, definition = resource_info.values_at(:type_name, :identifier, :definition)
    Rails.logger.info("Creating resource #{identifier}")

    manager = ResourceCollectionResource.manager_for(account, user, type_name)
    resource = manager.create(definition)

    if payload_data.passive_resources.include?(resource_info)
      ResourceCollection::UpdateRelationalDefinitionsService.new(identifier, resource, payload_data).perform!
    end

    ResourceCollectionResource.create! do |r|
      r.account = account
      r.resource_collection = collection
      r.resource = resource
      r.identifier = identifier
    end
  end
end
