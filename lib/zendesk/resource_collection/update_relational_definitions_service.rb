class ResourceCollection::UpdateRelationalDefinitionsService
  attr_reader :identifier, :resource, :payload_data

  def initialize(identifier, resource, payload_data)
    @identifier = identifier
    @resource = resource
    @payload_data = payload_data
  end

  def perform!
    payload_data.relational_resources.each do |resource_info|
      definition = resource_info[:definition]

      conditions = definition.fetch(:conditions, 'any' => definition.fetch('any', []), 'all' => definition.fetch('all', []))
      interpolate_conditions(conditions, identifier, resource)

      actions = definition.fetch(:actions, {})
      interpolate_actions(actions, identifier, resource)
    end
  end

  private

  def interpolate_conditions(conditions, identifier, resource)
    %w[all any].each do |type|
      (conditions[type] || []).each do |condition|
        if condition[:field]&.match?(field_matcher(identifier))
          condition[:field] = field_interpolation(resource)
        end
      end
    end
  end

  def interpolate_actions(actions, identifier, resource)
    actions.each do |action|
      if action[:field] == 'notification_target' && action[:value].try(:first) == identifier
        action[:value][0] = resource.id
      elsif action[:field]&.match?(field_matcher(identifier))
        action[:field] = field_interpolation(resource)
      end

      interpolatable_value_indexes(action[:field]).each do |i|
        action[:value][i].gsub!(placeholder_field_matcher(identifier), placeholder_field_interpolation(resource))
      end
    end
  end

  def interpolatable_value_indexes(action_field)
    case action_field
    when 'notification_user' then [1, 2]
    when 'notification_group' then [1, 2]
    when 'notification_target' then [1]
    when 'tweet_requester' then [0]
    else []
    end
  end

  def field_matcher(identifier)
    /\Acustom_fields_#{identifier}\Z/
  end

  def field_interpolation(resource)
    if resource.methods.include? :owner
      case resource.owner
      when 'Organization' then return "organization.custom_fields.#{resource.key}"
      when 'User' then return "requester.custom_fields.#{resource.key}"
      end
    end
    "custom_fields_#{resource.id}"
  end

  def placeholder_field_matcher(identifier)
    /{{ticket.ticket_field_#{identifier}}}/
  end

  def placeholder_field_interpolation(resource)
    "{{ticket.ticket_field_#{resource.id}}}"
  end
end
