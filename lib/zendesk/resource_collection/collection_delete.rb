require 'zendesk/resource_collection/collection_action'

class ResourceCollection::CollectionDelete < ResourceCollection::CollectionAction
  def delete_resources
    # ticket fields check that they are not used by any rule when deleting, so delete them last
    sorted = collection.collection_resources.sort_by { |r| r.resource.is_a?(Rule) ? 0 : 1 }

    sorted.each do |resource|
      yield(resource)
      delete_resource(resource)
      resource
    end
  end

  def delete_resource(resource)
    # sometimes the resource.resource get magically deleted
    if resource.resource.present?
      manager = resource.manager_for(account, user)
      resource.resource.overwrite_resource_permission = true
      manager.destroy(resource.resource)
    end

    resource.soft_delete
  end

  def cleanup_resources
    outdated_resources.each do |resource|
      delete_resource(resource)
    end
  end

  def outdated_resources
    collection.collection_resources.reject do |resource|
      identifiers_in_payload.include?(resource.identifier)
    end
  end

  def identifiers_in_payload
    @identifiers_in_payload ||= payload_data.all_resources.map do |resource_info|
      resource_info[:identifier]
    end
  end
end
