require 'zendesk/resource_collection/resource_manager'

class ResourceCollection::UserFieldManager < ResourceCollection::ResourceManager
  def create(params)
    manager.build(params).tap(&:save!)
  end

  def update(params, user_field)
    manager.update(user_field, params).tap(&:save!)
  end

  private

  def manager
    Zendesk::CustomField::CustomFieldManager.new(account, user, 'User')
  end
end
