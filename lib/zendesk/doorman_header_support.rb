module Zendesk::DoormanHeaderSupport
  private

  def parsed_doorman_header
    @parsed_doorman_header ||= begin
      doorman_header = request.headers['HTTP_X_ZENDESK_DOORMAN']
      return {} unless doorman_header.present?
      JWT.decode(doorman_header, doorman_secret).first
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: 'Failed doorman header parsing', fingerprint: 'a3f0e2fd44c5b5be5af2e79ecb7d691a')
      {}
    end
  end

  def doorman_secret
    @doorman_secret ||= ENV['DOORMAN_SECRET'] || Zendesk::Configuration.fetch(:doorman_secret)
  end
end
