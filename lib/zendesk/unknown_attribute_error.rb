# Since Rails 3, we've been using customs ActiveRecord::UnknownAttributeError with a message as a parameter
# In Rails 4.1, ActiveRecord::UnknownAttributeError require 2 parameters: object and attribute and it will generate a message using both
# This offers a unique interface for Rails 3.2 and 4.1+ and overrides the message to make sure we display what we expect.
# This class can be removed by replacing any `raise ActiveRecord::UnknownAttributeError` with another exception kind and catching it properly in the controllers
module Zendesk
  class UnknownAttributeError < ActiveRecord::UnknownAttributeError
    def initialize(message)
      @message = message
      super(nil, nil)
    end

    attr_reader :message
  end
end
