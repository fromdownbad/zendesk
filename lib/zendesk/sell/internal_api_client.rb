require 'zendesk/internal_api/client'
require 'faraday_middleware/stale_response'
require 'faraday-http-cache'

module Zendesk::Sell
  class InternalApiClient
    REQUEST_TIMEOUT = 3.seconds.freeze
    NETWORK_ERRORS = [Faraday::Error::ConnectionFailed, ZendeskAPI::Error::NetworkError, Kragle::ServerError].freeze

    def initialize(subdomain, timeout: REQUEST_TIMEOUT)
      @account = Account.with_deleted { Account.find_by_subdomain!(subdomain) }
      @connection = KragleConnection.build_for_sell(@account)
      @timeout = timeout
    end

    def delete_account
      post('usuvator/api/v1/zendesk/account', account_id: @account.id, notification_type: 'account_deleted')
    end

    %i[get post put patch delete].each do |verb|
      define_method(verb) do |url, params = {}, &block|
        make_request(verb, url, params, &block)
      end
    end

    private

    attr_reader :connection, :timeout

    def make_request(verb, url, params = {})
      connection.send(verb, url, params) do |req|
        req.options.timeout = timeout
        yield(req) if block_given?
      end
    end
  end
end
