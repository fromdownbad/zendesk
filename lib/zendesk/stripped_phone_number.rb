#
# To be used on formatted numbers,
# which will return a stripped phone number (without formatting).
# Inputting a value deemed not to be a phone number will return the original value.
#
# Valid Request:
# input:  "+1 (202) 803-7065"
# output: "+12028037065"
#
# Invalid Request:
# input:  "+1 1800-CALL-NOW"
# output: "+1 1800-CALL-NOW"
#
module Zendesk
  module StrippedPhoneNumber
    FORMATTING_CHARS = /[-| ( | ) |.| ]/.freeze

    class << self
      def strip(formatted_number:)
        return formatted_number unless formatted_number.is_a?(String)

        stripped_number = formatted_number.gsub(FORMATTING_CHARS, '')
        return formatted_number unless plausible(stripped_number)

        stripped_number
      end

      private

      def plausible(stripped_number)
        valid_chars = ('0'..'9').to_a + ['+', 'x']
        (stripped_number.chars - valid_chars).empty?
      end
    end
  end
end
