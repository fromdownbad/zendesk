module Zendesk
  module CsvScrubber
    def self.scrub_output(value)
      # escape '=' at the beginning of strings to
      #   prevent malicious macro injection
      value = "'#{value}" if value.is_a?(String) && value.start_with?("=", "+", "-", "@")
      value
    end
  end
end
