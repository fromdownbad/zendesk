module Zendesk
  module Reports
    # Handles enqueuing of reports. When a consumer asks for report data, there are
    # 3 possible outcomes:
    #
    # 1. Report is processed immediately if you have less than 20K tickets
    # 2. Report is enqueued if you have more than 20K tickets, you get last reports data
    # 3. Report is enqueued if you have more than 20K tickets, there's no last report
    class Processor
      def self.process(account, user, report)
        processor = new(account, user, report)
        processor.process
      end

      def initialize(account, user, report)
        @account = account
        @user    = user
        @report  = report
      end

      def process
        if runs_in_background?
          begin
            ReportJob.enqueue(@account.id, @user.id, @report.id)
            @enqueued = true
          rescue Resque::ThrottledError
            @throttled = true
          end
        end

        self
      end

      def message
        return unless runs_in_background?

        notice = if enqueued?
          ::I18n.t('txt.admin.lib.zendesk.reports.processor.an_update_is_being_created')
        elsif throttled?
          ::I18n.t('txt.admin.lib.zendesk.reports.processor.this_report_can_only_be_rebuild_every')
        else
          raise "This should never happend"
        end

        notice << " " <<
          if has_earlier_data?
            ::I18n.t('txt.admin.lib.zendesk.reports.processor.now_presenting_the_previous_result')
          else
            ::I18n.t('txt.admin.lib.zendesk.reports.processor.please_check_back_in_a_few_minutes')
          end

        notice
      end

      def enqueued?
        !!@enqueued
      end

      def throttled?
        !!@throttled
      end

      def has_earlier_data? # rubocop:disable Naming/PredicateName
        @report.result.present?
      end

      def report
        if runs_in_background?
          @report.data  = @report.result
          @report.title = @report.title + " (#{@report.last_run_at.to_format(with_time: true, time_format: @user.time_format_string, time_zone: @user.time_zone)})"
        end

        @report
      end

      def runs_in_background?
        if @runs_in_background.nil?
          @runs_in_background = @account.tickets.capped_count(20_000).capped?
        end

        @runs_in_background
      end
    end
  end
end
