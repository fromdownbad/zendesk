module Zendesk
  module Reports
    module ControllerSupport
      protected

      def check_view_access
        deny_access unless current_user.can?(:view, Report)
      end

      def check_manage_access
        deny_access unless current_user.can?(:manage, Report)
      end

      def requested_report
        @requested_report ||= current_account.reports.find(params[:id])
      end

      def report_result
        processor = Zendesk::Reports::Processor.process(current_account, current_user, requested_report)
        if !processor.runs_in_background?
          [:ok, processor.report]
        elsif processor.has_earlier_data?
          [:accepted, processor.report]
        else
          [:accepted, { message: processor.message }]
        end
      end
    end
  end
end
