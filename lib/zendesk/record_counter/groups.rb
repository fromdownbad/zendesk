module Zendesk::RecordCounter
  class Groups < Zendesk::RecordCounter::Base
    def scope
      the_scope = account.groups
      the_scope = the_scope.joins(:memberships).where(memberships: {user_id: user_id}) unless user_id.nil?

      the_scope
    end

    private

    def user_id
      options[:user_id]
    end
  end
end
