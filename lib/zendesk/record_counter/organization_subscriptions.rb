module Zendesk::RecordCounter
  class OrganizationSubscriptions < Zendesk::RecordCounter::Base
    def scope
      Zendesk::OrganizationSubscriptions::Finder.new(
        account,
        current_user,
        options
      ).scope.where(source_type: source_type)
    end

    private

    def source_type
      options[:source_type]
    end
  end
end
