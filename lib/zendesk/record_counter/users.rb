module Zendesk::RecordCounter
  class Users < Zendesk::RecordCounter::Base
    def scope
      the_scope = account.users
      the_scope = the_scope.joins(:memberships).where(memberships: {group_id: group_id}) unless group_id.nil?
      the_scope = the_scope.joins(:organization_memberships).where(organization_memberships: {organization_id: organization_id}) unless organization_id.nil?
      the_scope = the_scope.where(roles: role_ids) if role_ids

      the_scope
    end

    private

    def role_ids
      return nil unless role.present?
      roles = role.is_a?(Array) ? role : [role]

      roles.map { |role| role_name_to_id(role) }.compact.map(&:id).presence
    end

    def organization_id
      options[:organization_id]
    end

    def group_id
      options[:group_id]
    end

    def role
      options[:role]
    end

    def role_name_to_id(role)
      Role.all.detect do |r|
        /\A\d+\Z/.match?(role.to_s) ? r.id == role.to_i : r.name.downcase == role
      end
    end
  end
end
