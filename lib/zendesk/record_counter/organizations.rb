module Zendesk::RecordCounter
  class Organizations < Zendesk::RecordCounter::Base
    def scope
      the_scope = account.organizations
      the_scope = the_scope.joins(:organization_memberships).where(organization_memberships: { user_id: user_id }) unless user_id.nil?

      the_scope
    end

    private

    def user_id
      options[:user_id]
    end
  end
end
