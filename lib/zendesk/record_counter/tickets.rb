module Zendesk::RecordCounter
  class Tickets < Zendesk::RecordCounter::Base
    def scope
      Zendesk::Tickets::Finder.new(
        account,
        current_user,
        options
      ).tickets
    end
  end
end
