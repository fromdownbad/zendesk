module Zendesk::RecordCounter
  class OrganizationMemberships < Zendesk::RecordCounter::Base
    def scope
      Zendesk::OrganizationMemberships::Finder.new(
        account,
        options
      ).membership_scope.organization_memberships.active
    end
  end
end
