module Zendesk::RecordCounter
  class Incidents < Zendesk::RecordCounter::Base
    def scope
      Zendesk::Incidents::Finder.new(
        account,
        current_user,
        options
      ).incidents
    end
  end
end
