module Zendesk::RecordCounter
  class OrganizationFields < Zendesk::RecordCounter::Base
    def scope
      account.custom_fields.for_owner(owner)
    end

    private

    def owner
      options[:owner]
    end
  end
end
