module Zendesk::RecordCounter
  class TagScores < Zendesk::RecordCounter::Base
    def scope
      account.tag_scores
    end
  end
end
