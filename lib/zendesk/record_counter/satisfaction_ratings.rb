module Zendesk::RecordCounter
  class SatisfactionRatings < Zendesk::RecordCounter::Base
    def scope
      Zendesk::SatisfactionRatings::Finder.new(
        account,
        options
      ).satisfaction_ratings
    end
  end
end
