module Zendesk::RecordCounter
  class DeletedUsers < Zendesk::RecordCounter::Base
    def scope
      if include_redacted?
        account.users.inactive
      else
        account.users.inactive_and_not_redacted
      end
    end

    private

    def include_redacted?
      options[:include_redacted].present?
    end
  end
end
