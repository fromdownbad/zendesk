module Zendesk::RecordCounter
  class Base
    CACHE_KEY = 'record_counter'.freeze

    attr_reader :account, :options

    # Base class with logic to handle count endpoints.
    #
    # When the collection size is bigger than 100,000, the maximum 'temporary'
    # number is presented and an asynchronous job is enqueued to calculate
    # the real collection size and cache that result for 24 hours.
    # Subsequent calls will see the cached number.
    #
    # class Foos < Zendesk::RecordCounter::Base
    #   def scope
    #     if options[:option_a]
    #       account.foos.where(option_a: options[:option_a])
    #     else
    #       account.foos
    #     end
    #   end
    # end
    #
    # Foos.new(
    #   account: account,
    #   options: { option_a: 'test' }
    # ).present
    #
    # For big collections, the size will be cached in the async job as:
    #   "record_counter/foos/123/4d2278875a62910709cf9635fadcb3495dcddc1d"
    def initialize(account:, options: {})
      @account = account
      @options = options
    end

    # Presentation:
    #
    # * When the collection size is smaller than the maximum:
    #    count: {
    #      value: 13,
    #      refreshed_at: '2020-07-23T03:21:55+00:00'
    #    }
    #
    # * When the collection size is bigger than the maximum:
    #    count: {
    #      value: 100000,
    #      refreshed_at: nil
    #    }
    #
    # * When the collection size was calculated in the async job and is cached:
    #    count: {
    #      value: 120302,
    #      refreshed_at: '2020-07-23T03:21:55+00:00'
    #    }
    def present
      if cached_count.present?
        count_and_time = cached_count.split('/')
        presentation(count_and_time[0].to_i, count_and_time[1])
      else
        refreshed_at = iso_date
        if limited_count >= MAX_COUNT_BEFORE_ASYNC
          RecordCounterJob.enqueue(
            record_counter_class: class_name,
            account_id: account.id,
            options: options
          )
          refreshed_at = nil
        end

        presentation(limited_count, refreshed_at)
      end
    end

    # Called from RecordCounterJob. Generates a unique string
    # for this counter and the current scope:
    #   i.e.: "record_counter/foos/123/4d2278875a62910709cf9635fadcb3495dcddc1d"
    def cache_key
      [
        CACHE_KEY,
        class_name.split('::').last.snake_case,
        account.id,
        Digest::SHA1.hexdigest(scope.to_sql)
      ].join('/')
    end

    # Called from RecordCounterJob. Makes the unlimited count for the current scope
    # and generates a string that includes that count and the current time:
    #   i.e.: "123700/2020-07-23T03:21:55+00:00"
    def cache_value
      "#{unlimited_count}/#{iso_date}"
    end

    # Implement the scope in the subclasses.
    def scope
      raise 'implement me'
    end

    protected

    # Reusable code to pass and load current user in counters.
    def current_user_id
      options[:current_user_id]
    end

    def current_user
      @current_user ||= begin
        if current_user_id == User.system_user_id
          User.find(User.system_user_id)
        else
          account.users.find(current_user_id)
        end
      end
    end

    private

    def class_name
      self.class.name
    end

    def presentation(count, refreshed_at)
      {
        value: count,
        refreshed_at: refreshed_at
      }
    end

    def limited_count
      @limited_count ||= scope.limit(MAX_COUNT_BEFORE_ASYNC).count
    end

    def unlimited_count
      @unlimited_count ||= scope.unscope(:limit).count
    end

    def iso_date
      DateTime.now.iso8601
    end

    def cached_count
      @cached_count ||= Rails.cache.read(cache_key)
    end
  end
end
