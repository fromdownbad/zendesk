module Zendesk::RecordCounter
  class GroupMemberships < Zendesk::RecordCounter::Base
    def scope
      Zendesk::GroupMemberships::Finder.new(
        account,
        options
      ).membership_scope.memberships.active(account.id)
    end
  end
end
