module Zendesk::RecordCounter
  class Identities < Zendesk::RecordCounter::Base
    def scope
      account.user_identities.native.where(user_id: user_id)
    end

    private

    def user_id
      options[:user_id]
    end
  end
end
