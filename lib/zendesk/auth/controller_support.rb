module Zendesk
  module Auth
    module ControllerSupport
      protected

      def redirect_to_mapped_host
        # Ensure that the values that we set are not overwritten by the
        # values passed in as parameters. Z1#5096879
        redirect_to url_for(mapped_host_redirect_params.reverse_merge(params))
      end

      def mapped_host_redirect_params
        current_url_provider.url_params.merge(Zendesk::CnameUtils.encoded_flash_params(flash))
      end

      def host_is_incorrect?
        current_url_provider.host_name_without_port != request.host
      end

      def build_user_from_params
        @user = current_account.users.new(params[:user].try(:slice, :name, :email))
      end
    end
  end
end
