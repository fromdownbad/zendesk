module Zendesk
  module Auth
    class OauthCallbackHelper
      OAUTH_CALLBACK_URL = "https://support.#{Zendesk::Configuration.fetch(:host)}/ping/redirect_to_account".freeze

      def initialize(options = {})
        @subdomain = options[:subdomain]
        @oauth_callback_path = options[:oauth_callback_path]
      end

      def state
        "#{@subdomain}:#{@oauth_callback_path}"
      end

      def twitter_oauth_callback_url
        "#{OAUTH_CALLBACK_URL}?state=#{CGI.escape(state)}"
      end
    end
  end
end
