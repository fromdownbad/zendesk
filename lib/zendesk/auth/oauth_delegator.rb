class Zendesk::Auth::OauthDelegator
  def self.valid_auth_type?(profile)
    Zendesk::Auth::OAUTH_PROFILES[profile].present?
  end

  # `env` is necessary for fb_graph_api_v9 Arturo lookup;
  # remove after Arturo rollout is complete
  def self.request_oauth!(profile, callback_url, use_xauth = false, env = nil, authorize_url_options = {})
    unless Zendesk::Auth::OAUTH_PROFILES[profile]
      raise Zendesk::Auth::InvalidProfileException, profile
    end

    begin
      if profile == "twitter_readonly" || profile == "twitter"
        @twitter = Zendesk::Auth::Twitter.new(profile)
        @twitter.request_oauth(callback_url, use_xauth)
      elsif profile == "facebook"
        @facebook = Zendesk::Auth::Facebook.new(env)
        @facebook.request_oauth(callback_url, authorize_url_options)
      end
    rescue Errno::ECONNRESET, Timeout::Error, Errno::ETIMEDOUT
      Rails.logger.error("Error requesting oauth for #{profile}")
    end
  end
end
