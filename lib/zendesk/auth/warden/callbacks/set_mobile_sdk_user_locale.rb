module Zendesk::Auth::Warden::Callbacks
  class SetMobileSdkUserLocale
    def initialize(user, warden)
      @user = user
      @warden = warden
    end

    def set_locale
      if sdk_strategy?
        if account.has_sdk_auth_synchronous_user_locale?
          set_locale_sync
        else
          set_locale_async
        end
      end
    end

    private

    def set_locale_async
      SetUserLocaleJob.enqueue(
        account_id: account.id,
        user_id: @user.id,
        language: http_accept_language
      )
    end

    def set_locale_sync
      header_language = HttpAcceptLanguage::Parser.new(http_accept_language)

      available_translation_locales = account.available_languages
      settle = Zendesk::I18n::LanguageSettlement.new(header_language, available_translation_locales)
      translation_locale = settle.find_matching_zendesk_locale || account.translation_locale

      if @user.translation_locale != translation_locale
        @user.update_attribute(:translation_locale, translation_locale)
      end
    rescue StandardError => e
      Rails.application.config.statsd.client.increment('auth.warden.callbacks.set_mobile_sdk_user_locale.exception', tags: ["exception:#{e.class}"])
      Rails.logger.error("auth.warden.callbacks.set_mobile_sdk_user_locale.exception:#{e.class} #{e.message}, account_id:#{account.id}")
    end

    def account
      @account ||= @user.account
    end

    def sdk_strategy?
      @warden.winning_strategy.is_a?(Zendesk::Auth::Warden::SdkAnonymousStrategy) ||
      @warden.winning_strategy.is_a?(Zendesk::Auth::Warden::SdkJwtStrategy)
    end

    def http_accept_language
      env = @warden.request.env
      env["HTTP_ACCEPT_LANGUAGE"] || ENGLISH_BY_ZENDESK
    end
  end
end
