# Defining the module to make it easier to extract callbacks to the ./callbacks
# folder.
module Zendesk::Auth::Warden::Callbacks; end

Warden::Manager.after_authentication do |user, warden, _opts|
  Time.use_zone("UTC") do
    if warden.winning_strategy.class != Zendesk::Auth::Warden::BasicStrategy
      last_login = user.last_login || 1.hour.ago
      Zendesk::GeoLocation.locate_user!(user, warden.request.remote_ip) if last_login < 20.minutes.ago
    end

    if warden.winning_strategy.class != Zendesk::Auth::Warden::MasterTokenStrategy && !(warden.winning_strategy.class == Zendesk::OAuth::Warden::TokenStrategy && warden.winning_strategy.token.internal?)
      user.update_last_login
    end

    if warden.winning_strategy.zendesk_account_assumption? && !warden.current_account.assumable?
      Rails.logger.info("Notifying admins that Zendesk has bypassed assumption_control: assuming with user_id: #{user.id} of account_id: #{warden.current_account.id}")
      CIA::Event.create!(
        account: warden.current_account,
        actor:   User.system,
        source:  warden.current_account,
        action: 'audit',
        visible: true,
        message: "txt.admin.views.reports.tabs.audits.assumption_bypass"
      )
      SecurityMailer.deliver_assumption_bypass(warden.current_account)
    end
  end
end

Warden::Manager.after_set_user do |user, warden, _opts|
  warden.response.headers["X-Zendesk-User-Id"] = user.id.to_s
end

Warden::Manager.after_set_user do |user, warden, _opts|
  if warden.winning_strategy && !warden.winning_strategy.allowed?(user)
    Rails.logger.warn("LOGIN WITH DISALLOWED SERVICE | User: #{user.id}, Agent?: #{user.is_agent?}, Account: #{warden.current_account.id}, Strategy: #{warden.winning_strategy.class}")

    warden.logout
    warden.winning_strategy.flash[:error] = I18n.t('txt.lib.zendesk.auth.warden.callbacks.use_available_options')
  end
end

Warden::Manager.after_authentication do |user, warden, _opts|
  warden.raw_session.delete(:locale)
  warden.request.shared_session.delete(:locale_id)
  I18n.locale = user.translation_locale || ENGLISH_BY_ZENDESK
end

Warden::Manager.after_authentication do |user, warden, opts|
  unless opts[:store]
    Rails.logger.debug("Not storing session for #{warden.winning_strategy.class}")
  end

  if warden.current_account && warden.winning_strategy.is_resuming_previous_session?
    Rails.logger.debug "Not creating a new auth session since challenge token found an existing session"
  else
    Zendesk::AuthenticatedSession.create(
      session: warden.request.shared_session,
      account: warden.current_account,
      user: user,
      store: opts[:store],
      via: warden.winning_strategy.name,
      invalidate_id: warden.winning_strategy.invalidate_id?,
      remember_me_requested: warden.request.params[:remember_me].to_i != 0
    )
  end
end

Warden::Manager.after_authentication do |user, warden, opts|
  strategy = warden.winning_strategy

  if opts[:store] || (strategy.is_a?(Zendesk::OAuth::Warden::TokenStrategy) && strategy.token.mobile?) || strategy.mobile_auth?
    account  = warden.current_account
    storage  = warden.request.permanent_cookies

    params = {
      ip: warden.request.remote_ip,
      user_agent: warden.request.user_agent
    }

    is_mobile_auth = strategy.respond_to?(:mobile_auth?) && strategy.mobile_auth?

    if is_mobile_auth && strategy.mobile_device_information
      params.update(
        token: strategy.mobile_device_information["identifier"],
        name: strategy.mobile_device_information["name"],
        type: :mobile
      )

    elsif strategy.is_a?(Zendesk::OAuth::Warden::TokenStrategy) || is_mobile_auth
      params.update(token: strategy.token.id, type: :mobile)
    end

    device = Zendesk::Devices::Manager.new(account, storage, user, strategy, params).track
    # we won't return a device if we aren't tracking devices - for example user assumption
    if device && is_mobile_auth && strategy.mobile_device_information
      device.register_token!(strategy.token)
    end
  end
end

# Needs to be in a callback since we need to run after we create the AuthenticatedSession
Warden::Manager.after_authentication do |user, warden, _opts|
  strategy = warden.winning_strategy

  if strategy.is_a?(Zendesk::Auth::Warden::PasswordStrategy) && !Arturo.feature_enabled_for?(:scramble_password_on_password_expiration, user.account)
    strategy.enforce_password_expiration!
  end
end

# Set user role in shared_session to enable external systems to make decisions based on user role.
# Currently zendesk pass through proxy only allows requests made by agents and admins.
# Check here: https://github.com/zendesk/zendesk_proxy
Warden::Manager.after_set_user do |user, warden, _opts|
  warden.request.shared_session[:user_role] = user.roles
end

Warden::Manager.before_failure do |env, opts|
  account = env['zendesk.account']
  Rails.logger.info("Warden failure #{opts[:action]} - message: #{opts[:message]}") if account.try(:has_log_warden_failures?)
end

# Ensure Mobile SDK anonymous are using the requested language which is sent in
# the Accept-Language header.
Warden::Manager.after_authentication do |user, warden, _opts|
  require_relative 'callbacks/set_mobile_sdk_user_locale'
  Zendesk::Auth::Warden::Callbacks::SetMobileSdkUserLocale.new(user, warden).set_locale
end

# Support-ASK integration v1
Warden::Manager.after_authentication do |user, warden, _opts|
  if user
    Rails.application.config.statsd.client.increment('auth.warden.callbacks.login_events', tags: ["winning_strategy:#{warden.winning_strategy.class}"])

    # TODO: only emit web / real user sessions
    Zendesk::Auth::AuthenticationEventPublisher.new(user: user).publish_login_event!
    Rails.logger.debug("[ASK] account: #{user.account.id}, user: #{user.id} has logged in")
  end
end

Warden::Manager.before_logout do |user, warden, _opts|
  if user
    Rails.application.config.statsd.client.increment('auth.warden.callbacks.logout_events', tags: ["winning_strategy:#{warden.winning_strategy.class}"])

    # log auth_via for active sessions
    active_sessions = user&.shared_sessions&.unexpired&.count
    if active_sessions && active_sessions > 1
      Rails.application.config.statsd.client.increment('auth.warden.callbacks.logout_events.multiple_sessions', tags: ["count:#{active_sessions}"])

      active_auth_strategies = user&.shared_sessions&.unexpired&.pluck(:data)&.map { |data| data['auth_via'] }
      Rails.logger.info("[ASK] active_sessions: #{active_sessions}: #{active_auth_strategies}")
    end

    # TODO: only check active web user sessions and emit on last session
    Zendesk::Auth::AuthenticationEventPublisher.new(user: user).publish_logout_event!
    Rails.logger.debug("[ASK] account: #{user.account.id}, user: #{user.id} has logged out")
  end
end
