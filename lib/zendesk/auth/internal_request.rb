module Zendesk
  module Auth
    module InternalRequest
      private

      def internal_request?
        request.internal_client?
      end
    end
  end
end
