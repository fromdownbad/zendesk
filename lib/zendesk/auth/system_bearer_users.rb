# This adds a "system user" implementation on top
# of the anonymous client_credentials grant for global OAuth clients.
# For our internal OAuth system users implementation, we use the
# MAC Authorization strategy, but for external system users we
# will only be using the plaintext Bearer strategy [1].
#
# 1: http://tools.ietf.org/html/rfc6749#section-7.1
module Zendesk::Auth::SystemBearerUsers
  class SystemUserException < StandardError; end

  extend ActiveSupport::Concern

  protected

  def gooddata_request?
    subsystem_gooddata_request? || authorized_system_bearer_user?('gooddata')
  end

  def bime_request?
    request.env['zendesk.bime.trusted_request'] || subsystem_bime_request?
  end

  def subsystem_gooddata_request?
    current_user.is_system_user? && current_subsystem_name == 'gooddata'
  end

  def subsystem_bime_request?
    current_user.is_system_user? && current_subsystem_name == 'bime'
  end

  def zopim_request?
    subsystem_zopim_request? || authorized_system_bearer_user?('zopim')
  end

  def subsystem_zopim_request?
    current_user.is_system_user? && current_subsystem_name == 'zopim'
  end

  def sandbox_orchestrator_request?
    current_user && current_user.is_system_user? && current_subsystem_name == 'sandbox_orchestrator'
  end

  def authenticate_user
    super.tap do |_authenticated|
      # Necessary because of https://github.com/rails/rails/issues/9703
      # Can't skip_before_action agent / admin requirements
      if authorized_system_bearer_user? # User is anonymous
        current_user.roles = Role::ADMIN.id
      end
    end
  end

  def authorized_system_bearer_user?(user = nil)
    if user
      _authorized_system_bearer_user?(user)
    else
      Zendesk::Configuration.fetch(:system_global_clients).any? do |u, _|
        _authorized_system_bearer_user?(u)
      end
    end
  end

  def _authorized_system_bearer_user?(user)
    user = user.to_s
    client_id = Zendesk::Configuration.fetch(:system_global_clients)[user]

    if client_id
      oauth_access_token.present? && oauth_access_token.authorized_globally?(user, client_id)
    else
      Rails.logger.error("Could not find system global client #{user}!")
      false
    end
  end

  def check_sandbox_orchestrator_permissions
    # The sandbox_orchestrator system user should only be allowed to copy from a production account to a sandbox account
    if request.method != "GET" && !current_account.is_sandbox?
      deny_access
    end
  end

  module ClassMethods
    def allow_gooddata_user(options = {})
      allow_subsystem_user :gooddata, options.dup
      allow_system_bearer_user :gooddata, options.dup
    end

    def allow_bime_user(options = {})
      allow_subsystem_user :bime, options.dup
    end

    def allow_outbound_user(options = {})
      allow_subsystem_user :outbound, options.dup
    end

    def allow_pigeon_user(options = {})
      allow_subsystem_user :pigeon, options.dup
    end

    def allow_sandbox_orchestrator_user(options = {})
      allow_subsystem_user :sandbox_orchestrator, options.dup
      before_action :check_sandbox_orchestrator_permissions, if: :sandbox_orchestrator_request?
    end

    def allow_zopim_user(options = {})
      allow_subsystem_user :zopim, options.dup
      allow_system_bearer_user :zopim, options.dup
    end

    def require_zopim_user(_options = {})
      allow_zopim_user
      before_action :deny_access, unless: :zopim_request?
    end

    def require_gooddata_user
      allow_gooddata_user
      before_action :deny_access, unless: :gooddata_request?
    end

    # Takes options:
    # only: Only these actions are allowed, defaults to all
    #
    # NOTE: This method does not inherit, do not include it into any base class
    def allow_system_bearer_user(user, options = {})
      allow_anonymous_users *options.fetch(:only, [:all]), if: lambda { |c| c.send(:authorized_system_bearer_user?, user) }
    end

    # Takes options:
    # only: Only these actions are allowed, defaults to all
    #
    # NOTE: This method does not inherit, do not include it into any base class
    def require_system_bearer_user(user, options = {})
      allow_system_bearer_user(user, options)
      before_action :deny_access, options.slice(:only).merge(unless: lambda { |c| c.send(:authorized_system_bearer_user?, user) })
    end
  end
end
