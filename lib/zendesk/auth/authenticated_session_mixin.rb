module Zendesk::Auth
  module AuthenticatedSessionMixin
    protected

    def authentication
      @authentication ||= Zendesk::AuthenticatedSession.new(shared_session).tap do |auth|
        auth.current_account = current_account
        auth.current_user    = current_user
      end
    end

    def clear_authentication_cache
      @authentication = nil
    end

    def is_assuming_user? # rubocop:disable Naming/PredicateName
      authentication.is_assuming_user?
    end

    def is_assuming_user_from_monitor? # rubocop:disable Naming/PredicateName
      authentication.is_assuming_user_from_monitor?
    end

    def original_user
      authentication.original_user
    end

    def original_user=(user)
      authentication.original_user = user
    end

    def auth_via
      authentication.via
    end

    def auth_via?(identifier)
      auth_via == identifier.to_s
    end

    def not_auth_via?(identifier)
      !auth_via?(identifier)
    end
  end
end
