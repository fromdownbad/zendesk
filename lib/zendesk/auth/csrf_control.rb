module Zendesk::Auth::CsrfControl
  def self.included(base)
    base.rescue_from ActionController::InvalidAuthenticityToken do |e|
      Rails.logger.warn(%{InvalidAuthenticityToken caught for Account(#{current_account ? current_account.id : "No-Account"}) #{request.fullpath}})
      render_failure(
        status: :forbidden,
        title: "Forbidden",
        message: I18n.t("auth.csrf.invalid_authenticity_token")
      )

      if Arturo.feature_enabled_for?('log_csrf_invalid_token', current_account)
        authenticity_token = request.params[:authenticity_token]
        if current_account&.shared_sessions.present?
          data = current_account.shared_sessions&.first&.try(:data)
          session_token = if data.present?
            data.dig('csrf_token')
          end

          Rails.logger.warn("InvalidAuthenticityToken: Authenticity token from session #{session_token} with request token as #{authenticity_token}")

          if session_token.present? && authenticity_token.present?
            statsd_client.increment('invalid_token.mismatch')
          else
            if session_token.present?
              statsd_client.increment('invalid_token.request_token_missing')
            else
              statsd_client.increment('invalid_token.session_token_missing')
            end
          end
        else
          Rails.logger.warn("InvalidAuthenticityToken: Authenticity token missing from session with only request token as #{authenticity_token}")
          statsd_client.increment('invalid_token.session_token_missing')
        end
      end

      if Arturo.feature_enabled_for?('log_csrf_invalid_token_debug', current_account)
        ZendeskExceptions::Logger.record(e, location: self, message: "Invalid authenticity token in #{e.message}", fingerprint: '612ec5d7c5f532461c4348878ce9b681d202d3c9')
      end
    end
    base.extend ClassMethods
  end

  protected

  # this is no longer necessary once we have unified sessions
  # allows to post from http pages to https pages, not needed for api,
  # not needed if the account has ssl always enabled
  def csrf_check_disabled_for_http_to_https_action?
    request.ssl? &&
      !current_account.try(:ssl_should_be_used?) &&
      action_requires_ssl? &&
      current_account && is_mobile_request?
  end

  def is_mobile_request? # rubocop:disable Naming/PredicateName
    !current_account.has_restrict_csrf_bypass_to_mobile? || request.is_mobile?
  end

  # do not check for authenticity tokens on
  # - API requests via Basic Auth
  # - non-browser requests (just copied logic from rails 2, was removed from rails 3, might be a security issue)
  def verified_request?
    result = csrf_check_disabled_for_http_to_https_action? ||
      auth_via?(:basic) ||
      auth_via?(:token) ||
      auth_via?(:basic_warden) ||
      auth_via?(:signed_warden) ||
      super

    if Arturo.feature_enabled_for?('log_csrf_token_bypass', current_account) && csrf_check_disabled_for_http_to_https_action?
      log_csrf_token_bypass(super)
    end

    result
  end

  def handle_unverified_request
    raise(ActionController::InvalidAuthenticityToken)
  end

  private

  def log_csrf_token_bypass(verified)
    auth_bypass = auth_via?(:basic) || auth_via?(:token) || auth_via?(:basic_warden) || auth_via?(:signed_warden)
    client = Zendesk::StatsD::Client.new(namespace: ["csrf"])
    client.increment('ssl_bypass', tags: ["verified:#{!!verified}", "auth_bypass:#{!!auth_bypass}", "mobile:#{!!request.is_mobile?}"])

    Rails.logger.info "csrf protection bypassed - verified:#{verified} auth_bypass:#{auth_bypass} form_authenticity_token:#{form_authenticity_token} mobile:#{request.is_mobile?}"
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'csrf_control')
  end

  module ClassMethods
    # overwritten to not use prepend_before_action -> verified_request? must run after authenticate_user or auth_via is never set
    # -> test/integration/api/rest_json_controller_test.rb
    def protect_from_forgery(options = {})
      self.request_forgery_protection_token ||= :authenticity_token
      before_action :verify_authenticity_token, options
    end
  end
end
