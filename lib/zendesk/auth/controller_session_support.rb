module Zendesk
  module Auth
    module ControllerSessionSupport
      protected

      # Prevents SharedSession::Middleware::User from setting a session cookie:
      # https://github.com/zendesk/zendesk_shared_session/blob/affbf32cbf298b6862e99147cf5b23b1d6098824/lib/zendesk_shared_session/middleware/user.rb#L57-L60
      def disable_session_set_cookie
        Rails.logger.warn "Disabling session Set-Cookie response headers for #{controller_path}"
        request.env['rack.session.options'][:skip] = true
      end
    end
  end
end
