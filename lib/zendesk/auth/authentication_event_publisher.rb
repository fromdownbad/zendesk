module Zendesk
  module Auth
    class AuthenticationEventPublisher
      attr_reader :user

      def initialize(user:)
        @user = user
      end

      def publish_login_event!
        publish_event!(encoder_class: UserLoggedInEventProtobufEncoder)
      end

      def publish_logout_event!
        publish_event!(encoder_class: UserLoggedOutEventProtobufEncoder)
      end

      def publish_event!(encoder_class:)
        return if user.is_system_user?
        return unless user.account.has_publish_user_authentication_events?
        return unless user.is_agent?

        EscKafkaMessage.create!(
          value: encoder_class.new(user).to_proto,
          account_id: user.account_id,
          topic: 'platform.standard.user_authentication_events',
          partition_key: user.id
        )
      rescue StandardError => e
        Rails.logger.error(e)
        statsd_client.increment('user_authentication_events.errors', tags: ["exception:#{e.class}"])
        ZendeskExceptions::Logger.record(e, location: self, message: "AuthenticationEventPublisher: #{e.message}", fingerprint: 'c617dea35300391e96b4a3b4c0c5a5c5')
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['support', 'event_bus', 'authentication_event'])
      end
    end
  end
end
