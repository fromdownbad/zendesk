require "zendesk/lotus_redirector"

module Zendesk::Auth::AccountHome
  extend ActiveSupport::Concern
  include Zendesk::Accounts::Multiproduct

  included do
    helper_method :current_url_provider
  end

  private

  def secure_account_home
    current_url_provider.url + "/hc"
  end

  def help_center_home?
    current_brand.try(:help_center_in_use?) && current_account.help_center_enabled?
  end

  def central_admin_home
    current_url_provider.url + '/admin'
  end

  def sell_home(after_verification)
    if after_verification == "1" && Arturo.feature_enabled_for?(:sell_only_after_verification, current_account)
      current_url_provider.url + '/sell' + "?after_verification=" + after_verification.to_s
    else
      current_url_provider.url + '/sell'
    end
  end

  def account_home
    if Arturo.feature_enabled_for?(:owner_with_no_entitlements_redirect, current_account) && current_account.multiproduct && owner_with_no_entitlements?
      statsd_client.increment('access.account_home.redirect', tags: ['redirect_path:central_admin'])
      return central_admin_home
    end

    if sell_only_user_redirect?
      statsd_client.increment('access.account_home.redirect', tags: ['redirect_path:sell'])

      return sell_home(params[:after_verification])
    end

    # If it's a multiproduct account with only one account and support isn't active, redirect to other first active product.
    # Ordering by Account-Service will dictate hierarchy.
    # TODO: We should implement at some point a logic that would redirect agents to the product they work primarily.
    # Something similar has already been written for sell-only users.
    # This new method should be run before this one.
    if multiproduct_should_redirect?
      statsd_client.increment('access.account_home.redirect', tags: ['redirect_path:multiproduct'])
      return multiproduct_home
    end

    if Arturo.feature_enabled_for?(:agent_redirect_support, current_account) && support_product.try(:active?) && current_user.is_agent?
      statsd_client.increment('access.account_home.redirect', tags: ['redirect_path:support'])
      return current_url_provider.url + "/#{PRODUCT_TYPE_HOMES[:support]}"
    elsif multiproduct? && support_product.try(:active?) && current_user.is_agent?
      statsd_client.increment('access.account_home.redirect', tags: ['redirect_path:support'])
      return current_url_provider.url + "/#{PRODUCT_TYPE_HOMES[:support]}"
    end

    # Secure sessions are only used for accounts using help center
    if help_center_home?
      statsd_client.increment('access.account_home.redirect', tags: ['redirect_path:hc'])
      return secure_account_home
    end

    args = { id: nil, controller: '/access', action: 'index' }

    # If you request from a mobile client AND request the mobile format. The format doesn't cut it alone
    # as we allow force changing the mobile session after the format as been set.
    if logged_in? && (lotus_url = Zendesk::LotusRedirector.redirect(current_account, current_user, nil))
      statsd_client.increment('access.account_home.redirect', tags: ['redirect_path:lotus_url'])
      return lotus_url
    elsif logged_in?
      statsd_client.increment('access.account_home.redirect', tags: ['redirect_path:web_portal_and_public_forums'])
      return secure_account_home
    end

    args[:theme] = 'hc' if current_brand.try(:help_center_in_use?)

    # disable ssl for dev without ssl, otherwise use default
    args.merge!(current_url_provider.url_params(ssl: ssl_environment? && nil))
    statsd_client.increment('access.account_home.redirect', tags: ['redirect_path:web_portal_and_public_forums'])

    main_app.url_for(args)
  end

  def multiproduct_home
    Rails.logger.info "Multiproduct: redirect to #{first_active_product.name} for account #{current_account.id}"
    if params[:after_verification] == "1" && Arturo.feature_enabled_for?(:sell_only_after_verification, current_account)
      current_url_provider.url + "/#{PRODUCT_TYPE_HOMES.fetch(first_active_product.name.to_sym)}" + "?after_verification=" + params[:after_verification].to_s
    else
      current_url_provider.url + "/#{PRODUCT_TYPE_HOMES.fetch(first_active_product.name.to_sym)}"
    end
  end

  def multiproduct_should_redirect?
    old_condition = multiproduct? && first_active_product && first_active_product.name.to_s != Zendesk::Accounts::Client::SUPPORT_PRODUCT

    # With this new logic, we only redirect you to the multiproduct_home if user is logged in.
    if Arturo.feature_enabled_for?(:new_multiproduct_redirect, current_account)
      current_user && !current_user.is_anonymous_user? && old_condition
    else
      old_condition
    end
  end

  def owner_with_no_entitlements?
    return false unless current_user && current_user.id == current_account.owner_id
    Zendesk::StaffClient.new(current_account).get_entitlements!(current_user.id).values.compact.count == 0
  end

  def sell_only_user_redirect?
    return false unless Arturo.feature_enabled_for?(:sell_only_user_redirect, current_account)
    # We need to add the `is_contributor?` check as Classic roles have not been completely backfilled
    # into StaffService. To clarify this, we use the contributor "role". A user that is an agent in another
    # product but not in Support, will become a contributor in Support.
    return false if !current_user || current_user.is_anonymous_user? || !current_user.is_contributor?
    entitlements = begin
      Zendesk::StaffClient.new(current_account).get_entitlements!(current_user.id)
    rescue *Zendesk::StaffClient::RETRYABLE_ERRORS
      {}
    end

    accessible_products = entitlements.select { |_k, v| v && v.to_s != 'viewer' }.keys
    accessible_products == ['sell']
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['classic'])
  end
end
