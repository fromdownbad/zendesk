module Zendesk::Auth
  # Used to determine if a given email address is valid in respect to whitelist and blacklist
  # settings. The list logic is:
  #
  #  - If something is allowed by the whitelist (ie. whitelisted), allow it regardless of the blacklist
  #  - If something is not whitelisted, allow it according to the blacklist
  #
  module ListUtils
    STAR = '*'.freeze
    COLON = ':'.freeze

    class << self
      def address_in_list?(address, list)
        list.to_s.strip.tokenize.any? do |pattern|
          address_matches?(address, pattern)
        end
      end

      def address_matches?(address, pattern)
        return true if pattern == STAR
        return false if pattern.include?(COLON)
        return false if address.blank?

        case pattern
        when DOMAIN_PATTERN
          address =~ /(@|\.)#{Regexp.escape(pattern)}$/i
        when EMAIL_PATTERN
          address =~ /^#{Regexp.escape(pattern)}$/i
        else # not a valid whitelist value
          false
        end
      end
    end
  end
end
