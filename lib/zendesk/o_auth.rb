module Zendesk
  module OAuth
    PROFILES = Zendesk::Auth::OAUTH_PROFILES

    def self.client(profile_id = :twitter)
      profile = PROFILES[profile_id]
      ::OAuth::Consumer.new(profile[:key], profile[:secret], site: profile[:site])
    end
  end
end
