require 'open3'

module Zendesk
  module UnicornLoadRateLimiting
    TOTAL_UNICORN_CACHE_KEY = 'host_total_unicorn_count'.freeze

    class << self
      def unicorn_load_exceeds?(percentage)
        (active_unicorns / total_unicorns) > percentage
      rescue StandardError => e
        ZendeskExceptions::Logger.record(e, location: self, message: "Failed to calculate current Unicorn load: #{e.message}", fingerprint: '08dc43f67f29e4858227e305d7d6702ebab4e742')

        true
      end

      def active_unicorns
        _, unicorn_stats = Raindrops::Linux.tcp_listener_stats.find do |addr, _|
          addr.match(/([^:]*\d\z)/)[0] == ENV['UNICORN_TCP_PORT']
        end

        raise 'Could not find active unicorns' unless unicorn_stats

        unicorn_stats.active.to_f
      end

      def total_unicorns
        raise 'Could not find total unicorns' unless ENV['UNICORN_WORKERS']

        ENV['UNICORN_WORKERS'].to_f
      end
    end
  end
end
