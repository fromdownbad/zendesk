module Zendesk
  module Assets
    module Configuration
      private

      # all the configured default store names
      def default_stores
        region = Zendesk::Configuration.dig(:pods, Zendesk::Configuration.fetch(:pod_id), :region)

        return [] unless Zendesk::Configuration.dig(:default_cloud_stores, region)

        Zendesk::Configuration.dig(:default_cloud_stores, region).map(&:to_sym)
      end
    end
  end
end
