module Zendesk
  module Assets
    class Path
      # This assumes the URI has a partitioned ID, with 2+ parts, each exactly 4 digits. This is a
      # legacy assumption and breaks when IDs have > 12 digits as the partitioning code uses 12.
      # Unfortunately this pattern needs to stay around forever to ensure that any old and
      # externally referenced URLs can still be retrieved appropriately.
      LEGACY_PATH_REGEX = %r{(system)\/(logos|photos|voice\/uploads|o_auth\/logos|brands)\/([0-9]{4})\/([0-9]{4})(\/([0-9]{4}))?\/(.+)\z}

      # This updated pattern assumes the ID is un-partitioned, and will just look for a continuous
      # set of numbers.
      PATH_REGEX = /
        system\/
        (?<object_type>logos|photos|voice\/uploads|o_auth\/logos|brands)\/
        (?<asset_id>[0-9]+)\/
        (?<filename>.+)\z
      /x

      attr_reader :object_type
      attr_reader :asset_id
      attr_reader :uri

      def initialize(uri_param)
        PATH_REGEX.match(uri_param) do |match|
          @asset_id = match[:asset_id].to_i
          @object_type = match[:object_type]
          @uri = "public/system/#{@object_type}/#{@asset_id}/#{match[:filename]}"
        end

        return unless @url.nil?

        LEGACY_PATH_REGEX.match(uri_param) do |match|
          @asset_id = "#{match[3]}#{match[4]}#{match[6]}".to_i
          @object_type = match[2]
          @uri = "public/#{match[1]}/#{match[2]}/#{@asset_id}/#{match[7]}"
        end
      end
    end
  end
end
