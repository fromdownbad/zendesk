module Zendesk
  module Assets
    module S3
      include Zendesk::Assets::Configuration

      private

      def s3_backend
        store = Zendesk::Stores::StoresSynchronizerHelper.new(@asset.class).preferred_s3_store(@asset, default_stores)
        return nil unless store
        @asset.send(store)
      end

      def set_s3_headers
        if s3_backend
          response.headers['X-S3-Storage-Url'] = s3_backend.bucket.object(s3_backend.full_filename).presigned_url(:get).to_s
        end
      end
    end
  end
end
