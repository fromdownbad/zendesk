module Zendesk
  module HtmlRedaction
    DEFAULT_REDACTION_CHARACTER = "▇".freeze
    def self.redact(value, text, redaction_character = DEFAULT_REDACTION_CHARACTER)
      redact!(value.to_s.dup, text, redaction_character)
    end

    # Below we're iterating through several character encoding strategies and
    # returning the first one that changes something. Under normal
    # circumstances, it's :basic and we break the loop there.
    #
    # Encoding strategies may vary for several reasons. Chiefly, because the
    # libxml version that Nokogiri relies on varies with architecture and OS
    # version. Additionally, several entity types are valid and may be
    # submitted via the API.
    #
    # e.g.:
    #
    #   Production: "Jag är en sköldpadda"
    #   Docker/ZDI: "Jag &auml;r en sk&ouml;ldpadda"
    #   API will accept: "Jag &#228;r en sk&#246;ldpadda"
    #   API will also accept: "Jag &#xe4;r en sk&#xf6;ldpadda"
    #
    # Unrelated: The default redaction_character is U+2587, assigned to the
    # constant DEFAULT_REDACTION_CHARACTER above. It's convenient to override
    # in tests as it occupies two columns in console output and can be a pain
    # to debug.
    def self.redact!(value, text, redaction_character = DEFAULT_REDACTION_CHARACTER)
      [:basic, :named, :decimal, :hexadecimal].each do |strategy|
        val = value.gsub!(
          /#{Regexp.escape(HTMLEntities.new.encode(text, strategy))}/i,
          text.gsub(/[^ ]/, redaction_character)
        )
        return value unless val.nil?
      end
      nil
    end

    def self.redact_part_or_all!(value, text, redact_full_comment)
      (text.nil? && redact_full_comment) ? "▇▇▇▇▇" : redact!(value, text)
    end
  end
end
