require "aws-sdk-s3"

module Zendesk
  module AttachmentFromCloud
    attr_reader :s3_copy_source

    def self.included(receiver)
      receiver.alias_method :default_attachment_stores_without_cloud_copying, :default_attachment_stores
      receiver.alias_method :default_attachment_stores, :default_attachment_stores_with_cloud_copying
      receiver.after_commit :delete_cloud_copy_source

      Technoweenie::AttachmentFu::Backends::S3Backend.send(:include, S3BackendMixin)
    end

    def default_attachment_stores_with_cloud_copying
      return [primary_s3_store] if @s3_copy_source.present?

      default_attachment_stores_without_cloud_copying
    end

    S3_URL_REGEXP = /^https?:\/\/([^\/]+)\.s3[a-z\d\.-]*\.amazonaws\.com\/([^\?]+)/

    def copy_from_cloud(cloud_url)
      raise "You can only copy from cloud on new attachments" unless new_record?

      return if copy_from_remote_files(cloud_url)

      if matched_url = cloud_url.match(S3_URL_REGEXP)
        copy_from_cloud_params(
          cloud: :s3,
          directory: CGI.unescape(matched_url[1]),
          key: CGI.unescape(matched_url[2])
        )
        cloud_attachment_statsd.increment("from_s3_url")
      else
        raise "#{cloud_url.inspect} is not a valid S3 URL"
      end
    end

    def copy_from_cloud_params(params)
      raise "Unknown cloud #{params[:cloud]}" unless params[:cloud] == :s3

      s3_identifier = params[:s3_identifier] || primary_s3_store
      s3_options = Attachment.attachment_backends[s3_identifier][:options].
        slice(:s3_access_key, :s3_secret_key, :region)
      auth = {
        access_key_id: s3_options.fetch(:s3_access_key),
        secret_access_key: s3_options.fetch(:s3_secret_key),
        region: s3_options.fetch(:region)
      }
      connection = Aws::S3::Client.new(auth)
      @s3_copy_source = {bucket: params.fetch(:directory), key: params.fetch(:key)}
      meta = connection.head_object(@s3_copy_source)

      self.stores         = primary_s3_store
      self.filename     ||= File.basename(params[:key])
      self.content_type ||= meta.content_type
      self.size           = meta.content_length

      Rails.logger.info("Assigned attachment store %s for %s" % [
        primary_s3_store,
        params[:key]
      ])

      @no_thumbnails = true # do not create thumbnails
      @no_resize = true # do not resize yourself
      # This works because we are a new_record and setup_stores just sets stores_to_add = default stores
    end

    ConfigurationError = Class.new(StandardError)

    def primary_s3_store
      @_primary_s3_store ||= Attachment.default_s3_stores.first.tap do |store|
        if store.nil?
          exception = ConfigurationError.new("Default S3 store is not specified")
          Zendesk::InboundMail::StatsD.client.increment("processing_error.error", tags: ["detail:#{exception.class}"])
          raise exception
        end
      end
    end

    def copy_from_remote_files(cloud_url)
      RemoteFiles::CONFIGURATIONS.values.each do |config|
        next unless config.configured?
        next unless remote_file = config.file_from_url(cloud_url)

        store = remote_file.stores.first

        if store.is_a?(RemoteFiles::FogStore)
          cloud = store.options[:provider]
          raise "wrong cloud #{cloud}" unless cloud == 'AWS'

          copy_from_cloud_params(
            cloud: :s3,
            directory: store.directory_name,
            key: remote_file.identifier,
            s3_identifier: store.identifier
          )
          cloud_attachment_statsd.increment("from_fogstore_s3")
        else
          remote_file.retrieve!
          self.filename     ||= File.basename(remote_file.identifier)
          self.content_type ||= remote_file.content_type
          set_temp_data remote_file.content
          cloud_attachment_statsd.increment("from_remote_file")
        end

        return true
      end

      false
    end

    def delete_cloud_copy_source
      if @s3_copy_source.present?
        if defined?(account)
          if account.has_no_mail_delimiter_enabled?
            Rails.logger.info("Not deleting copied S3 object #{@s3_copy_source[:bucket]}/#{@s3_copy_source[:key]}")
          else
            Rails.logger.info("Deleting copied S3 object #{@s3_copy_source[:bucket]}/#{@s3_copy_source[:key]}")
            s3.connection.bucket(@s3_copy_source[:bucket]).object(@s3_copy_source[:key]).delete
          end
        else
          Rails.logger.info("#delete_cloud_copy_source — account is missing")
          Rails.logger.info("Deleting copied S3 object #{@s3_copy_source[:bucket]}/#{@s3_copy_source[:key]}")
          s3.connection.bucket(@s3_copy_source[:bucket]).object(@s3_copy_source[:key]).delete
        end
      end

      @s3_copy_source = nil
    end

    def cloud_attachment_statsd
      @cloud_attachment_statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["cloud_attachments"])
    end

    module S3BackendMixin
      def self.included(receiver)
        receiver.alias_method :save_to_storage_without_s3_copying, :save_to_storage
        receiver.alias_method :save_to_storage, :save_to_storage_with_s3_copying
      end

      def save_to_storage_with_s3_copying
        if @obj.respond_to?(:s3_copy_source) && @obj.s3_copy_source.present?
          source_bucket = @obj.s3_copy_source[:bucket]
          source_key = @obj.s3_copy_source[:key]
          stored_object = @obj.s3.connection.bucket(source_bucket).object(source_key)
          stored_object.copy_to(
            bucket: bucket_name,
            key: full_filename,
            server_side_encryption: 'AES256'
          )
        else
          save_to_storage_without_s3_copying
        end
      end
    end
  end
end
