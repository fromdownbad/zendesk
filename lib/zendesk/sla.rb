require 'biz'

module Zendesk
  module Sla
    APM_SERVICE_NAME = 'classic-sla'.freeze
  end
end

require 'zendesk/sla/null_metric'
require 'zendesk/sla/null_policy'
require 'zendesk/sla/ticket_metric'
require 'zendesk/sla/metric_selection'
require 'zendesk/sla/ticket_status'
require 'zendesk/sla/stage'
require 'zendesk/sla/policy_manager'
require 'zendesk/sla/policy_assignment'
