# frozen_string_literal: true

module Zendesk::Mailer::ReadOnly
  def read_only
    ActiveRecord::Base.on_slave { yield }
  end
end
