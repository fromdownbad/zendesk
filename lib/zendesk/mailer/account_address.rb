module Zendesk::Mailer
  class AccountAddress
    attr_reader :account
    attr_accessor :author

    def initialize(options = {})
      @account   = options.fetch(:account)
      @author    = options[:author]
      @brand     = options[:brand]
    end

    def noreply
      "#{encode(account_name)} <#{noreply_email}>".strip
    end

    def noreply_email
      if brand.present?
        "noreply@#{brand.backup_email_address.split("@").last}"
      else
        "noreply@#{account.route.default_host}"
      end
    end

    def from(for_agent: false)
      if brand.present?
        [encode(name(for_agent)), "<#{address}>"].compact.join(" ")
      else
        noreply
      end
    end

    def personalize?
      author.present? && !author.is_system_user? &&
        account.is_personalized_reply_enabled?
    end

    protected

    def name(for_agent)
      if personalize?
        author_name_with_account_name(for_agent)
      else
        account_name
      end
    end

    def author_name_with_account_name(for_agent)
      [author_name(for_agent), parenthesize(account_name)].compact.join(" ")
    end

    def address
      brand&.reply_address
    end

    def account_name
      if brand && brand.default_recipient_address
        brand.default_recipient_address.name.presence
      else
        account.name
      end
    end

    def brand
      @brand || account.default_brand
    end

    def author_name(for_agent)
      author.safe_name(for_agent)
    end

    def parenthesize(name)
      "(#{name})" if name.present?
    end

    def encode(name)
      ApplicationMailer.encode_address_name(name, account) if name.present?
    end
  end
end
