require 'zendesk_mail/address'

Mail::Message.class_eval { attr_accessor :gmail_api }

module Zendesk::Mailer
end

module Zendesk::Mailer::GmailSending
  FAILED_TOKEN_REQUEST = "FAILED".freeze
  UNKNOWN_ERROR = -1
  RATE_LIMITED = 429
  BAD_REQUEST = 400
  BAD_CREDENTIALS = 401
  BACKEND_ERROR = 500
  ERROR = 300..999

  def self.from(from)
    return unless from = Array(from).first.presence
    Zendesk::Mail::Address.parse(from).try(:address)
  end

  class GmailDeliveryMethod
    def initialize(*)
    end

    def deliver!(mail)
      account = mail.gmail_api.fetch(:account)
      from = mail.gmail_api.fetch(:from)
      access_token = mail.gmail_api.fetch(:access_token)

      if account.has_email_logs_for_gmail_sending?
        Rails.logger.info("Account ##{account.id} is trying to deliver mail from '#{from}' address")
      end

      # bcc headers get removed by gmail and are not sent to recipients
      encoded = mail.encoded
      if mail.bcc && bcc = mail.bcc.join(",").presence
        encoded.prepend "Bcc: #{bcc}\n"
      end

      Rails.logger.info("encoded body: #{encoded}") if account.has_email_real_attachments_send_via_gmail?

      # gmail signs and modifies the email, so our signature or gmail buttons would be invalid
      2.times { encoded.sub!(/^DKIM-Signature:.*?^([A-Z]|$)/m, "\\1") }

      response = deliver_via_api(encoded, access_token)

      if response.status == RATE_LIMITED
        # break the throttle so we stop running into the limit
        Zendesk::Mailer::GmailSending.rate_limit(account, from, 999999)
        fallback_deliver_via_smtp(mail, response.status, "Gmail rate limit exceeded")
      elsif response.status == BAD_REQUEST && response.body.include?("Mail service not enabled")
        disable_gmail_sending(account, from)
        fallback_deliver_via_smtp(mail, response.status, "Mail service not enabled")
      elsif response.status == BAD_CREDENTIALS && response.body.include?("Invalid Credentials")
        disable_gmail_sending(account, from)
        fallback_deliver_via_smtp(mail, response.status, "Invalid credentials")
      elsif response.status == BACKEND_ERROR && response.body.include?("Backend Error")
        fallback_deliver_via_smtp(mail, response.status, "gmail failure")
      elsif ERROR.include?(response.status)
        fallback_deliver_via_smtp(mail, response.status, "Unexpected response status #{response.status} -- #{response.body}")
      else
        Rails.logger.info("Successfully sent by Gmail, response code is #{response.status}")
        Rails.application.config.statsd.client.increment('gmail_sending.sent', tags: ["response_code:#{response.status}"])
      end
    rescue StandardError => e
      fallback_deliver_via_smtp(mail, UNKNOWN_ERROR, e)
    end

    # called after sending the mail to check if :return_response is set ... just ignore ... mail/lib/mail/message.rb:250
    def settings
      {}
    end

    private

    # disable gmail sending account-wide ... a bit dangerous but otherwise smtp settings are hidden
    def disable_gmail_sending(account, from)
      account.settings.send_gmail_messages_via_gmail = false
      account.settings.save
      AccountsMailer.deliver_gmail_sending_api_disabled(account, from)
    end

    def deliver_via_api(encoded, access_token)
      connection = Faraday.new('https://www.googleapis.com')
      connection.authorization :Bearer, access_token
      connection.post do |request|
        request.url '/upload/gmail/v1/users/me/messages/send?uploadType=media'
        request.headers['Content-Type'] = 'message/rfc822'
        request.body = encoded
      end
    end

    def fallback_deliver_via_smtp(mail, status_code, error)
      # To be included in https://app.datadoghq.com/dash/137059 as "GMail delivery erros by status":
      Rails.application.config.statsd.client.increment('gmail_sending.delivery_error', tags: ["response_code:#{status_code}"])

      error = RuntimeError.new(error) if error.is_a?(String)
      error.message << " -- From: #{mail.from} -- To: #{mail.to} -- #{mail.message_id}"

      Rails.logger.error(error.to_s)

      ActionMailer::Base.wrap_delivery_behavior(mail, ActionMailer::Base.delivery_method)
      mail.delivery_method.deliver!(mail)
    end
  end

  class << self
    def token_for_account(account, from, options)
      unless from
        Rails.logger.info "This delivery doesn't have a 'From' address set" if account.has_email_logs_for_gmail_sending?
        return
      end

      unless account.settings&.send_gmail_messages_via_gmail
        Rails.logger.info "Account ##{account.id} has sending via Gmail disabled" if account.has_email_logs_for_gmail_sending?
        return
      end

      unless account.recipient_addresses # a stub might be passed in here
        Rails.logger.info "Account ##{account.id} doesn't have any valid recipient addresses" if account.has_email_logs_for_gmail_sending?
        return
      end

      unless credential = account.recipient_addresses.find_by_email(from).try(:external_email_credential)
        Rails.logger.info "Account ##{account.id}'s recipient address '#{from}' doesn't have a valid external email credential" if account.has_email_logs_for_gmail_sending?
        return
      end

      if rate_limit(account, from, [options[:to], options[:cc], options[:bcc]].compact.flatten.size)
        Rails.logger.info "Account ##{account.id} has a rate-limit in progress" if account.has_email_logs_for_gmail_sending?
        return
      end

      generate_access_token credential
    end

    # https://support.google.com/a/answer/166852?hl=en
    # - 10000 recipients per day
    # - 2000 unique recipients per day
    # - 2000 email per day
    # -> we enforce 1500 recipients per day
    def rate_limit(account, from, count)
      throttle_options = {increment: count}
      throttle_options[:threshold] = 9500 if [700621, 742767].include?(account.id) # accounts with a raised limit to 10_000 from gmail
      throttled = Prop.throttle(:gmail_recipients, from, throttle_options)
      if throttled == :first_throttled
        Rails.application.config.statsd.client.increment 'gmail_sending.warning_sent'
        AccountsMailer.deliver_gmail_sending_rate_limit_reached(account, from)
      end

      !!throttled
    end

    private

    def generate_access_token(credential)
      refresh_token = credential.decrypted_refresh_token
      key = "#{credential.account.id}/gmail_sending.access_token.#{credential.encrypted_value}"

      token = Rails.cache.read(key) || begin
        token, expires = uncached_access_token(refresh_token)
        Rails.cache.write(key, token, expires_in: (expires * 0.8).to_i) # do not cache for the full duration, or we might try to send emails with a token that is 1s before expiration
        token
      end

      token unless token == FAILED_TOKEN_REQUEST
    rescue
      Rails.cache.write(key, FAILED_TOKEN_REQUEST, expires_in: 1.hour) # do not hit the api for every mail when we already know it does not work
      message = "Error generating gmail access token for Account: #{credential.account_id} / Credential: #{credential.id}"
      report_error(message)
      nil
    end

    def report_error(message)
      Rails.application.config.statsd.client.increment 'gmail_sending.token_error'
      if Rails.env.production?
        Rails.logger.error("#{message} (#{caller.first})")
      else
        raise message
      end
    end

    def uncached_access_token(refresh_token)
      response = Faraday.post(
        'https://accounts.google.com/o/oauth2/token',
        client_id: Zendesk::Configuration.dig!(:external_email_credential, :client_id),
        client_secret: Zendesk::Configuration.dig!(:external_email_credential, :client_secret),
        refresh_token: refresh_token,
        grant_type: 'refresh_token'
      ).body
      token, expires = JSON.load(response).values_at("access_token", "expires_in")
      raise "Invalid gmail response: token: #{token.to_s.truncate(10)}, expires: #{expires}" unless token && expires
      [token, expires]
    end
  end

  def mail(options)
    # Our current integration with the GMail API does not support sending attachments
    unless account.has_email_real_attachments_send_via_gmail?
      if attachments.any?
        Rails.logger.info "Avoiding Gmail API since the email includes attachments (currently not supported)" if account.has_email_logs_for_gmail_sending?
        return super(options)
      end
    end

    from = Zendesk::Mailer::GmailSending.from(options[:from])
    Rails.logger.info "Obtaining delivery method for '#{from}' address" if account.has_email_logs_for_gmail_sending?

    if access_token = Zendesk::Mailer::GmailSending.token_for_account(account, from, options)
      Rails.logger.info "Using Gmail API to deliver mail from '#{from}' address" if account.has_email_logs_for_gmail_sending?
      options[:delivery_method] = :gmail_api

      @_message.gmail_api = {
        access_token: access_token,
        account: account,
        from: from
      }
    end

    super(options)
  end
end
