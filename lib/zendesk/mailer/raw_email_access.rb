require 'sanitize'

module Zendesk::Mailer
  class RawEmailAccess
    # e.g. "X-Rspamd-Authentication-Results"
    X_HEADER_PREFIX = /\Ax-/i.freeze
    DOS_NEWLINE = "\r\n".freeze
    UNIX_NEWLINE = "\n".freeze
    MAX_NOKOGIRI_TREE_DEPTH = -1 # Removes limit on tree depth

    PROCESSING_STATE_CONTENT_REQUIRING_CID_RESOLUTION = %w[
      html_full
      html_quoted
      html_reply
    ].freeze

    attr_reader :eml_identifier, :full_message_json_identifier, :json_identifier

    def initialize(eml_identifier, options = {})
      @eml_identifier = eml_identifier
      @json_identifier = eml_identifier.blank? ? options[:json_identifier] : eml_identifier.gsub(/\.eml$/, '.json')
      @full_message_json_identifier = json_identifier.gsub(/\.json$/, '.eml_body_content.json') if json_identifier
      @attachment_index = options[:attachments]
      @email_fix_no_delimiter_redaction = options[:email_fix_no_delimiter_redaction]
      @tiff_prevent_inline = options[:tiff_prevent_inline]
    end

    def blank?
      eml_identifier.blank? && json_identifier.blank?
    end

    # Do not remove after :eml_without_x_headers functionality is GA-ed.
    # This is used to train Rspamd in Zendesk::InboundMail::RspamdTrain.
    def eml
      return nil if eml_identifier.blank?

      @eml ||= remote_eml_file.content
    rescue RemoteFiles::NotFoundError
      nil
    end

    def eml_without_x_headers
      return nil unless eml

      @eml_without_x_headers ||= begin
        mail_from_eml = Mail.read_from_string(eml)
        headers = mail_from_eml.header.fields

        headers_string = ""
        headers.each do |header|
          next if header.name.match?(X_HEADER_PREFIX)

          # Must retrieve the actual instance variable here, because the mail
          # gem's Mail::Field#value strips out newlines, tabs, and other
          # whitespace that we need to preserve when reconstructing the eml.
          #
          # Also note that the mail gem uses DOS-style "\r\n" newlines.
          headers_string << header.instance_variable_get(:@raw_value).gsub(DOS_NEWLINE, UNIX_NEWLINE) + UNIX_NEWLINE
        end

        headers_string + UNIX_NEWLINE + mail_from_eml.body.raw_source
      rescue StandardError => e
        Rails.logger.info("Failed to generate eml_without_x_headers for file #{eml_identifier}: #{e.inspect}")
        statsd_client.increment('eml_without_x_headers_parsing_failure', tags: ["exception:#{e.class.to_s.underscore}"])
        nil
      end
    end

    def full_message_body_json
      return nil if full_message_json_identifier.blank?

      @full_message_body_json ||= remote_full_message_json_file.content
    rescue RemoteFiles::NotFoundError
      nil
    end

    def json
      return nil if json_identifier.blank?

      @json ||= remote_json_file.content
    rescue RemoteFiles::NotFoundError
      nil
    end

    def json_without_x_headers
      return nil unless parsed_json

      @json_without_x_headers ||= begin
        duped_parsed_json = parsed_json.deep_dup

        duped_parsed_json["headers"].keys.each do |header_key|
          if header_key.match?(X_HEADER_PREFIX)
            duped_parsed_json["headers"].delete(header_key)
          end
        end

        duped_parsed_json.to_json
      rescue StandardError => e
        Rails.logger.info("Failed to generate json_without_x_headers for file #{json_identifier}: #{e.inspect}")
        statsd_client.increment('json_without_x_headers_parsing_failure', tags: ["exception:#{e.class.to_s.underscore}"])
        nil
      end
    end

    def text
      @text ||= first_of_type(Mime[:text].to_s)
    end

    def html
      @html ||= first_of_type(Mime[:html].to_s)
    end

    def sanitized_html
      return nil if html.nil?
      return nil unless html.is_a?(String)

      @sanitized_html ||= begin
        html = self.html.scrub
        if @attachment_index.present? && parsed_json
          parsed_json["parts"].each do |part|
            next unless part["headers"].present?
            next unless part_content_id = part["headers"]["content-id"].try(:join)
            next unless part_file_name = part["headers"]["file-name"].try(:join)
            next unless inline_attachment_url = find_attachment_by_filename_and_body(part_file_name, part['body'])

            html.gsub!(/src=(['"])cid:#{part_content_id}(['"])/, "src=#{$1}#{inline_attachment_url}#{$2}")
          end
        end

        ZendeskCommentMarkup::SanitizationFilter.to_html(html, from_agent: true)
      end
    end

    def message
      return @message if @message
      return nil unless parsed_json

      @message ||= Zendesk::Mail::Serializers::InboundMessageSerializer.load(parsed_json)
    end

    def eml_file_name
      File.basename(eml_identifier) if eml_identifier
    end

    def json_file_name
      File.basename(json_identifier) if json_identifier
    end

    def resolve_json_cid_images_for_identifier!(content_id_map, json, identifier, should_reset: false, logger: Rails.logger)
      return unless content_id_map.present? && @attachment_index.present?
      return unless json.present?

      email_hash = Zendesk::InboundMail::Support.parse_json(json)
      total_modifications = []

      if (processing_state_content = email_hash.fetch('processing_state', nil)&.fetch('content', nil))
        PROCESSING_STATE_CONTENT_REQUIRING_CID_RESOLUTION.each do |key|
          html = processing_state_content.fetch(key, nil)

          next if html.nil?

          modifications, body = resolve_cid_images(html, content_id_map)
          total_modifications << modifications
          processing_state_content.merge!(key => body)
        end
      end

      if total_modifications.flatten.compact.empty?
        logger.info("Not replacing .json file #{identifier} — no cid resolution required")
        return
      end

      logger.info("About to replace .json file #{identifier} due to cid resolution")
      @remote_full_message_json_file = remote_file_perform(:store_once!, identifier, content: email_hash.to_json, content_type: "application/json")
      logger.info("Completed cid resolution for #{identifier}. should_reset: #{should_reset}")

      # Make sure everything gets reloaded if anyone accesses via this object again
      @remote_eml_file = @remote_full_message_json_file = @remote_json_file = @text = @html = @json = @full_message_body_json = @message = @sanitized_html = @parsed_json = nil if should_reset
    end

    def resolve_json_cid_images!(content_id_map, logger = Rails.logger)
      statsd_client.time('resolve_json_cid_images.time') do
        resolve_json_cid_images_for_identifier!(content_id_map, json, json_identifier, should_reset: true, logger: logger)
        resolve_json_cid_images_for_identifier!(content_id_map, full_message_body_json, full_message_json_identifier, should_reset: true, logger: logger)
      end
    end

    def inline?(attachment_or_model)
      !@tiff_prevent_inline || attachment_or_model.content_type != Mime[:tiff]
    end

    def resolve_cid_images(html, content_id_map)
      body = Nokogiri::HTML5.fragment(html, max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH)
      modifications = body.css('img').map do |image|
        next unless content_id = image['src'].to_s[/^cid:(.*)/, 1]
        next unless attachment = content_id_map[content_id]

        attachment.inline = inline?(attachment)
        image['src'] = attachment.url
      end
      [modifications, body.to_html]
    rescue SystemStackError
      [[], html]
    end

    def redact_json_files!(phrase_to_redact)
      return if json.blank?

      Rails.logger.info("Checking whether .json file: #{json_identifier} needs to be redacted")

      email_hash = Zendesk::InboundMail::Support.parse_json(json)

      # Collect the various things we need to run redaction on
      text_needing_redaction = []
      text_needing_redaction << email_hash.fetch('headers', {}).fetch('subject', [])[0]
      text_needing_redaction << email_hash.fetch('body', nil)
      if email_hash['parts']
        textish_parts(email_hash['parts']).each do |part|
          text_needing_redaction << part.fetch('body', nil)
        end
      end

      if @email_fix_no_delimiter_redaction &&
          (processing_state_content = email_hash.fetch('processing_state', nil)&.fetch('content', nil))
        text_needing_redaction << processing_state_content.fetch('html_full', nil)
        text_needing_redaction << processing_state_content.fetch('html_quoted', nil)
        text_needing_redaction << processing_state_content.fetch('html_reply', nil)
        text_needing_redaction << processing_state_content.fetch('plain_full', nil)
        text_needing_redaction << processing_state_content.fetch('plain_quoted', nil)
        text_needing_redaction << processing_state_content.fetch('plain_reply', nil)
      end

      full_message_body_hash = nil
      if full_message_body_json
        begin
          full_message_body_hash = Zendesk::InboundMail::Support.parse_json(full_message_body_json)
          if (content = full_message_body_hash.fetch('processing_state', nil)&.fetch('content', nil))
            text_needing_redaction << content.fetch('html_quoted', nil)
            text_needing_redaction << content.fetch('plain_quoted', nil)
          end
        rescue StandardError => error
          Rails.logger.warn("Error parsing full email message body json: #{error.class} #{error.message}: #{error.backtrace}")
        end
      end

      text_needing_redaction.compact!

      # Redact the strings in-place
      number_of_redactions = text_needing_redaction.count do |value|
        Zendesk::TextRedaction.redact!(value, phrase_to_redact)
      end

      if number_of_redactions > 0
        Rails.logger.info("About to replace .json file #{json_identifier} due to redaction")
        @remote_json_file = remote_file_perform(:store_once!, json_identifier, content: email_hash.to_json, content_type: "application/json")
        Rails.logger.info("Completed redacting #{json_identifier}")

        if full_message_body_hash
          Rails.logger.info("About to replace .eml_body_content.json file #{full_message_json_identifier} due to redaction")
          @remote_full_message_json_file = remote_file_perform(:store_once!, full_message_json_identifier, content: full_message_body_hash.to_json, content_type: "application/json")
          Rails.logger.info("Completed redacting #{full_message_json_identifier}")
        end

        # Make sure everything gets reloaded if anyone accesses via this object again
        @text = @html = @json = @full_message_body_json = @message = @sanitized_html = @parsed_json = nil
      end
    end

    def redact_attachment_from_json!(filename)
      return if json.blank?

      Rails.logger.info("Checking if .json file: #{json_identifier} has attachment #{filename} to redact.")

      email_hash = Zendesk::InboundMail::Support.parse_json(json)
      original_email_parts = email_hash['parts']
      email_parts_without_attachment_to_be_redacted = original_email_parts.reject do |part|
        part['headers'].key?('file-name') && part['headers']['file-name'].first == filename
      end

      attachments_with_matching_filename_count = original_email_parts.size - email_parts_without_attachment_to_be_redacted.size

      if attachments_with_matching_filename_count == 0
        Rails.logger.info("Attachment #{filename} not found in .json file: #{json_identifier}. Not redacting.")
        return
      end

      if attachments_with_matching_filename_count > 1
        Rails.logger.info("Found #{attachments_with_matching_filename_count} attachments with name #{filename} to redact.")
      end

      email_hash['parts'] = email_parts_without_attachment_to_be_redacted
      json_with_attachment_parts_redacted = email_hash.to_json

      Rails.logger.info("Overwriting .json file: #{json_identifier} with new version with attachment #{filename} redacted!")

      @remote_full_message_json_file = remote_file_perform(:store_once!, json_identifier, content: json_with_attachment_parts_redacted, content_type: "application/json")

      # Make sure everything gets reloaded if anyone accesses via this object again
      @text = @html = @json = @full_message_body_json = @message = @sanitized_html = @parsed_json = @json_without_x_headers = @remote_json_file = nil
    end

    def delete_eml_file!
      return if eml_identifier.blank?

      Rails.logger.info("About to delete .eml file: #{eml_identifier} due to redaction")
      remote_file_perform(:delete_now!, eml_identifier)
      @eml_identifier = nil
      @eml = nil
    end

    def delete_json_files!
      return if json_identifier.blank?

      Rails.logger.info("About to delete .json file: #{json_identifier}")
      remote_file_perform(:delete_now!, json_identifier)

      if full_message_body_json
        Rails.logger.info("About to delete .eml_body_content.json file: #{full_message_json_identifier}")
        remote_file_perform(:delete_now!, full_message_json_identifier)
        @full_message_json_identifier = nil
        @full_message_body_json = nil
      end

      @json_identifier = nil
      @json = nil
    end

    protected

    def parsed_json
      return nil if json.blank?

      @parsed_json ||= Zendesk::InboundMail::Support.parse_json(json)
    end

    def textish_parts(parts)
      parts.select do |part|
        content_type_of_part = part.fetch(:headers, {}).fetch(:'content-type', nil).try(:first)
        Zendesk::Mail::InboundMessage.textish_content_types.include? content_type_of_part
      end
    end

    def first_of_type(type)
      message.try(:first_of_type, type).try(:body)
    end

    def remote_eml_file
      @remote_eml_file ||= remote_file_perform(:retrieve!, eml_identifier)
    end

    def remote_json_file
      @remote_json_file ||= remote_file_perform(:retrieve!, json_identifier)
    end

    def remote_full_message_json_file
      @remote_full_message_json_file ||= remote_file_perform(:retrieve!, full_message_json_identifier)
    end

    def remote_file_perform(action, identifier, content: nil, content_type: nil)
      options = {
        configuration: Zendesk::Mail.raw_remote_files.name
      }

      options[:stored_in] = Zendesk::Mail.raw_remote_files.stores unless action == :store_once!
      options[:content] = content if content
      options[:content_type] = content_type if content_type

      RemoteFiles::File.new(identifier, options).tap do |remote_file|
        remote_file.send(action)
      end
    end

    def find_attachment_by_filename_and_body(filename, body)
      possible = @attachment_index.select { |a| a.filename == filename }
      attachment = if possible.size > 1
        possible.detect do |a|
          a.ensure_md5!
          a.md5 == Digest::MD5.hexdigest(body)
        end
      else
        possible.first
      end
      attachment.try(:url)
    end

    private

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'raw_email_access')
    end
  end
end
