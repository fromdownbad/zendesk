module Zendesk::Mailer::DeprecatedMailerAPI
  def self.included(base)
    class << base
      alias_method :method_missing_without_deprecated, :method_missing

      # convert old create/deliver_foo to foo().deliver (.create is handled via action_mailer-enqueable)
      def method_missing(name, *args, &block)
        if match = /^(create|deliver)_([_a-z]\w*)/.match(name.to_s)
          method_missing_without_deprecated(match[2], *args, &block).send(match[1])
        else
          method_missing_without_deprecated(name, *args, &block)
        end
      end
    end
  end
end
