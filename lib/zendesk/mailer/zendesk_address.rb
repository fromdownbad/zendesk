module Zendesk::Mailer
  class ZendeskAddress
    attr_reader :name, :address

    def initialize(name = "Zendesk", address = "support@support.#{Zendesk::Configuration.fetch(:host)}")
      @name = name
      @address = address
    end

    def from
      "#{name} <#{address}>"
    end
  end
end
