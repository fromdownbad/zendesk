module Zendesk::Mailer
  class TicketAddress < AccountAddress
    attr_reader :account, :ticket
    attr_accessor :author
    undef_method :noreply
    undef_method :noreply_email

    def initialize(options = {})
      @ticket = options.fetch(:ticket)
      super
    end

    def reply_to(for_agent)
      [encode(account_name), "<#{reply_address(for_agent)}>"].compact.join(" ")
    end

    def personalized_reply_to(for_agent)
      [encode(name(for_agent)), "<#{reply_address(for_agent)}>"].compact.join(" ")
    end

    def account_name
      recipient_address.try(:name) || super
    end

    private

    def reply_address(for_agent)
      email = address

      if email.to_s.end_with?("@" << Zendesk::Configuration.fetch(:host), "." << Zendesk::Configuration.fetch(:host)) # @zendesk.com is needed for Zendesk-owned accounts
        reply_id = (for_agent ? ticket.encoded_id : ticket.nice_id)
        user, domain = email.split('@')
        "#{user}+id#{reply_id}@#{domain}"
      else
        email
      end
    end

    def address
      recipient_address.try(:email) || super
    end

    def brand
      ticket.brand || super
    end

    def recipient_address
      return @recipient_address if defined?(@recipient_address)
      address = ticket.recipient_address
      @recipient_address = address.try(:send_emails?) ? address : nil
    end
  end
end
