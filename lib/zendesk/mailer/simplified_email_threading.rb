# frozen_string_literal: true

module Zendesk::Mailer::SimplifiedEmailThreading
  REPLACEMENT_PLACEHOLDER = "{{ticket.latest_comment_html}}"
  PUBLIC_REPLACEMENT_PLACEHOLDER = "{{ticket.latest_public_comment_html}}"

  def email_ccs_added?(notification)
    if account.has_follower_and_email_cc_collaborations_enabled?
      return false if notification.type != "NotificationWithCcs"

      email_cc_change_event = notification.audit.events.reverse.find { |e| e.type == "EmailCcChange" }
      email_cc_change_event.present? && !(email_cc_change_event.value - email_cc_change_event.value_previous).empty?
    elsif account.has_collaboration_enabled?
      return false if notification.type != "Cc"

      current_collaborators_change_event = notification.audit.events.reverse.find { |e| e.type == "Change" && e.field_name == "current_collaborators" }
      current_collaborators_change_event.present? && !(format_email_string(current_collaborators_change_event.value) - format_email_string(current_collaborators_change_event.value_previous)).empty?
    else
      false
    end
  end

  def show_last_three_comments?(template_body)
    return false if account.nil?

    account.has_email_simplified_threading? && @email_ccs_added && !for_agent && (template_body.include?(REPLACEMENT_PLACEHOLDER) || template_body.include?(PUBLIC_REPLACEMENT_PLACEHOLDER))
  end

  private

  # `email_string` takes on one of the following format:
  # * "Admin Extraordinaire <admin@zendesk.com>, Agent Ordinaire <agent@zendesk.com>"
  # * "admin@zendesk.com, agent@zendesk.com"
  #
  # Returns an array of email addresses:
  # ["admin@zendesk.com", "agent@zendesk.com"]
  def format_email_string(email_string)
    return [] if email_string.nil?

    email_string.scan(FIND_MULTIPLE_EMAIL_PATTERN).map { |email, _| email.downcase }.uniq
  end
end
