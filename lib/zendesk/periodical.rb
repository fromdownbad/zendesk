module Zendesk
  # Does something once,
  # if it has not been done in a given while (period)
  #
  # Note:
  # "DONE" is measured at the 'end' of when it's done.
  # So, it will execute as:
  # [ 2 sec task ] ...1 sec gap...[ 2 sec task ]
  class Periodical
    def initialize(period)
      @period = period
      @last_done = nil
    end

    def perform
      if !@last_done || (Time.now - @last_done) > @period
        yield
        @last_done = Time.now
      end
    end
  end
end
