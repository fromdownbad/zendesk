module Zendesk::ImportExport::JobEnqueue
  private

  def set_job_klass
    unless klass
      render_failure(
        status: :not_found,
        title: "No such type of job",
        message: 'The specified type of job does not exist.'
      )
    end
  end

  private *delegate(:klass, :klass_name, to: :job_policy)

  def job_policy
    @job_policy ||= ImportExportJobPolicy.new(current_account, current_user, params[:type])
  end

  def unavailable_error
    respond(
      :error, flash_error(
        I18n.t('public.controllers.job_controller.this_feature_is_not_available_for_your_plan')
      )
    )
  end

  def log_exceeded_max
    Rails.logger.info(
      "#{klass} enqueued with more than #{Zendesk::Export::TicketExportOptions::MAXIMUM_COUNT} tickets. Total number of tickets: #{current_account.tickets.count_with_archived} for #{current_account.id}."
    )
  end

  def respond(kind, message)
    respond_to do |format|
      format.html do
        flash[kind] = message.html_safe
        redirect_to redirect_url
      end
      format.json { render json: { kind => message } }
    end
  end

  def notice_message
    if klass.respond_to?(:enqueued_translated)
      klass.enqueued_translated
    else
      I18n.t('txt.admin.controllers.jobs_controller.job_submitted_email_message')
    end
  end

  def enqueue
    case klass_name
    when "ViewCsvJob"
      klass.enqueue(current_account.id, current_user.id, params[:job][:rule])
    when "OrganizationsJob"
      klass.enqueue(current_account.id, current_user.id, params[:job][:update_records] == "true", params[:job][:token], request.referrer)
    when "UsersJob"
      klass.enqueue(current_account.id, current_user.id, params[:job][:update_records] == "true", params[:job][:token], params[:job][:password_email_change_csv_import], request.referrer, params[:job][:allow_agent_downgrade] == 'true', params[:job][:report_external_id] == 'true')
    else
      klass.enqueue(current_account.id, current_user.id)
    end
  end

  def throttled_error_message
    [klass.throttled_translated, link_to_latest_throttled_export].compact.join(" ").html_safe
  end

  def link_to_latest_throttled_export
    return unless klass.latest(current_account)
    return if klass == ViewCsvJob

    view_context.link_to(
      klass.latest_translated,
      klass.latest(current_account).url(token: true),
      class: "title"
    )
  end

  def redirect_url
    case klass_name
    when "OrganizationsJob"
      organizations_path
    when "UsersJob"
      people_path
    when "TwoFactorCsvJob"
      settings_security_path
    when *ImportExportJobPolicy::EXPORT_JOB_CLASS_NAMES
      reports_path(anchor: 'export')
    else
      account_home
    end
  end
end
