require 'json/jwt'

class Zendesk::Attachments::Token
  TTL = 15 * 60
  ALLOWED_TIME_TRAVEL = 30
  SHA_SIZE = 256
  ENCRYPTION_ALGORITHM = "A128CBC-HS#{SHA_SIZE}".freeze

  [:account_id, :attachment_id, :iat].each do |method|
    define_method(method) do
      claims.fetch(method.to_s)
    end
  end

  def initialize(args)
    if args[:token]
      decrypt_token(args[:token])
    else
      claims[:account_id] = args[:account].id
      claims[:user_id] = args[:user].id
      claims[:attachment_id] = args[:attachment].id
    end
  end

  def claims
    @claims ||= {
      iat: Time.now.to_i, # iat => issued_at_time,
      jti: SecureRandom.uuid # jti => JWT ID
    }.with_indifferent_access
  end

  def user_id
    claims[:user_id]
  end

  def encrypt
    JSON::JWT.new(claims).encrypt(key, :dir, ENCRYPTION_ALGORITHM).to_s
  end

  def expired?
    Time.now.to_i > claims[:iat] + TTL
  end

  def used!
    return true if @claims[:jti].blank?

    cache_key = ["attachment-token-jti", @claims[:jti], account_id].join("/")

    if Rails.cache.read(cache_key)
      return true
    else
      Rails.cache.write(cache_key, @claims[:jti], expires_in: 30.minutes)
    end

    false
  end

  private

  # OpenSSL would previously ignore any extra bytes when digesting values, but now
  # it will raise an error if the value is too long. I'm not sure why this value
  # needs to be twice as long as the OpenSSL Cipher dictates, but the value came
  # from https://github.com/nov/json-jwt/blob/a3f31186/lib/json/jwe.rb#L187-L188
  def key
    Zendesk::Configuration.fetch(:attachment_token_key).byteslice(0, SHA_SIZE / 8)
  end

  def decrypt_token(encrypted_token)
    jwt = JSON::JWT.decode(encrypted_token, key)
    claims = JSON::JWT.decode(jwt.plain_text)

    validate_claims(claims)
    @claims = claims.to_hash.with_indifferent_access
  end

  def validate_claims(claims)
    if Time.now.to_i + ALLOWED_TIME_TRAVEL < claims[:iat]
      error "token issued more then #{ALLOWED_TIME_TRAVEL} seconds in the future"
    elsif !claims[:account_id]
      error "missing account_id"
    elsif !claims[:attachment_id]
      error "missing attachment_id"
    end
  end

  def error(msg)
    Rails.logger.error("Error decoding attachment jwe token: #{msg}")
    raise(InvalidJWE, msg)
  end

  class InvalidJWE < StandardError
  end
end
