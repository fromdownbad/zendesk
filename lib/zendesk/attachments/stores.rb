module Zendesk
  module Attachments
    module Stores
      def self.included(base)
        base.extend(ClassMethods)
      end

      def synchronize_stores
        Zendesk::Stores::StoresSynchronizer.new.add_to_preferred_stores(self)
      end

      def enqueue_stores_synchronization_job
        StoresSynchronizationJob.enqueue(account_id, id)
      end

      def has_s3_storage? # rubocop:disable Naming/PredicateName
        !(self.class.s3_stores & stores).empty?
      end

      def region
        Zendesk::Configuration.dig(:pods, Zendesk::Configuration.fetch(:pod_id), :region)
      end

      def cloud_stores
        Zendesk::Configuration.dig(:default_cloud_stores, region).map(&:to_sym)
      end

      def get_s3_storage # rubocop:disable Naming/AccessorMethodName
        preferred = Zendesk::Stores::StoresSynchronizerHelper.new(self.class).preferred_s3_store(self, cloud_stores)

        # This is protected by check that attachment has an s3 store, so should never see nil here
        raise StandardError, "No S3 store for attachment #{id}" unless preferred

        send preferred
      end

      def header_for_s3
        get_s3_storage.authenticated_s3_url
      end

      # go with the first dimension for now; also ignore flags
      def thumbnail_dimension
        attachment_options[:thumbnails].values.first.chomp(">").chomp("<") rescue nil
      end

      def thumbnail?
        return false unless respond_to? :parent_id
        parent_id.present?
      end

      # With exception of default_s3_stores,
      # there's a pending change to attachment_fu to move these methods
      #
      module ClassMethods
        # The stores to which the blob should be uploaded
        def preferred_stores
          (primary_stores | secondary_stores)
        end

        # Stores to which the blob will be uploaded during sync
        def secondary_stores
          attachment_backends.
            select { |_, option| option[:options][:secondary] }.
            map &:first
        end

        # The stores to which the blob will initially be uploaded
        def primary_stores
          # one backend makes it the primary
          return [attachment_backends.keys.first] if attachment_backends.length == 1

          attachment_backends.
            select { |_, option| option[:options][:default] }.
            map &:first
        end

        # Stores using S3 (any s3 bucket, not specifcially the store keyed as :s3)
        def s3_stores
          attachment_backends.
            select { |_, option| option[:options][:storage] == :s3 }.
            map &:first
        end

        # stores using s3 also flagged as default (used for email attachments
        # as the initial store to allow s3-to-s3 copying)
        def default_s3_stores
          attachment_backends.select do |_, option|
            option[:options][:storage] == :s3 && option[:options][:default]
          end.map &:first
        end
      end
    end
  end
end
