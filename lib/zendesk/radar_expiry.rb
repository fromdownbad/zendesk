require 'zendesk/radar_factory'

module Zendesk
  class RadarExpiry
    def self.get(account)
      if !@last_account || @last_account.subdomain != account.subdomain
        @last_expiry_object = new(account)
        @last_account = account
      end
      @last_expiry_object
    end

    def initialize(account)
      @account = account
    end

    def expire_radar_cache_key(scope)
      return if skip_expire_radar_cache_key?
      return if @account.has_disable_lotus_expiry?

      if Prop.throttle(:radar_expiry, "#{@account.id}/#{scope}")
        record_throttle_stats(scope)
      else
        value = { 'updated_at' => Time.now.to_i }
        send_expiry(scope, value)
        record_expiry_stats(scope)
      end
    end

    private

    def skip_expire_radar_cache_key?
      @account.new_record? ||
        @account.pre_account_creation.try(:is_binding?) ||
        @account.route.nil?
    end

    def radar_cluster
      @cluster ||= begin
        cluster_config = Zendesk::Radar::Configuration.cluster_for_shard(@account.shard_id)
        cluster_config["redis_cluster"].split("_").last.to_i
      rescue
        "unknown"
      end
    end

    def datadog_tags(scope)
      scope = scope.to_s.gsub(/\d+/, "<id>")
      ["scope:#{scope}", "cluster:#{radar_cluster}"]
    end

    def record_throttle_stats(scope)
      tags = datadog_tags(scope)
      statsd_client.increment("expiry.throttle", tags: tags)
    end

    def record_expiry_stats(scope)
      tags = datadog_tags(scope)
      statsd_client.increment("expiry.set", tags: tags)
    end

    def radar_client
      @radar_client ||= ::RadarFactory.create_radar_client(@account)
    end

    def statsd_client
      @statsd_client ||= ::Zendesk::StatsD::Client.new(namespace: 'radar')
    end

    def send_expiry(scope, value)
      radar_client.status("expiry/#{scope}").set(scope, value)
    end
  end
end
