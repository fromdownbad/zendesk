module Zendesk
  module PodRelay
    protected

    def relay_request_cross_pod(pod_id)
      path = Zendesk::Accounts::DataCenter.xpod_xaccel_redirect_path(
        pod_id, request.ssl?, request.fullpath
      )
      headers['X-Accel-Redirect'] = path
      logger.info("relaying request to pod #{pod_id}: #{path}")
      render plain: ''
    end

    def can_relay_request?
      # don't create a relay loop
      !request.headers['X-Zendesk-Pod-Relay']
    end
  end
end
