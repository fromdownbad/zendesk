module Zendesk
  module Routing
    class TicketSkillMatcher
      def initialize(user)
        @user = user
      end

      def matching_tickets(ticket_ids)
        ticket_requirements.
          fulfilled_ticket_ids(user, ticket_ids).
          map(&:to_i)
      rescue Faraday::Error => exception
        # Could not get to Deco, proceed without it
        Rails.
          logger.
          error("Error calling Deco for ticket filtering: #{exception}")

        ticket_ids
      end

      protected

      attr_reader :user

      private

      def ticket_requirements
        @ticket_requirements ||=
          Zendesk::Deco::Request::TicketRequirements.new(deco)
      end

      def deco
        @deco ||= Zendesk::Deco::Client.new(user.account)
      end
    end
  end
end
