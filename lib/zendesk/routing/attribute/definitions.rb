module Zendesk
  module Routing
    module Attribute
      class Definitions
        include Api::V2::Tickets::AttributeMappings

        def initialize(account, user)
          @account = account
          @user    = user
        end

        def conditions_all
          ZendeskRules::Definitions::Conditions.
            definitions_as_json(condition_context(:all)).
            map do |definition|
              Condition.for_definition(definition, account: account)
            end
        end

        def conditions_any
          ZendeskRules::Definitions::Conditions.
            definitions_as_json(condition_context(:any)).
            map do |definition|
              Condition.for_definition(definition, account: account)
            end
        end

        protected

        attr_reader :account,
          :user

        private

        def condition_context(condition_type)
          Zendesk::Rules::Context.new(
            account,
            user,
            component_type: :condition,
            condition_type: condition_type,
            rule_type:      :attribute
          )
        end
      end
    end
  end
end
