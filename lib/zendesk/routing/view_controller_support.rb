module Zendesk::Routing::ViewControllerSupport
  DECO_OVERSAMPLING_FACTOR = 10
  DECO_MAX_TIME = 10.seconds
  DECO_MAX_LOOPS = 10

  private

  def match_tickets(requested_tickets_count)
    statsd_client.time('match_tickets_with_deco') do
      perform_initial_pagination_setup(requested_tickets_count)

      scrolled_tickets_count = 0
      matching_tickets = []

      max_time = processing_end_time

      DECO_MAX_LOOPS.downto(1) do |remaining_loops|
        tickets = view_tickets
        next unless tickets.present?

        scrolled_tickets_count += tickets.length

        ticket_ids_from_deco = skill_matcher.matching_tickets(tickets.map(&:id))

        tickets.each do |ticket|
          if ticket_ids_from_deco.include?(ticket.id) &&
             matching_tickets.length < requested_tickets_count
            matching_tickets << ticket
          end
        end

        if matching_tickets.length >= requested_tickets_count ||
           tickets.length < params[:per_page]
          Rails.logger.info('Successfully matched number of requested tickets')
          log_match_tickets_metrics(remaining_loops - 1, scrolled_tickets_count)

          return matching_tickets
        elsif Time.current > max_time
          Rails.
            logger.
            info('Processing time exceeded')

          mark_insufficient_results
          log_match_tickets_metrics(remaining_loops - 1, scrolled_tickets_count)

          return matching_tickets
        end

        increment_page_number
      end

      mark_insufficient_results
      log_match_tickets_metrics(0, scrolled_tickets_count)

      matching_tickets
    end
  end

  def skill_matcher
    @skill_matcher ||= Zendesk::Routing::TicketSkillMatcher.new(current_user)
  end

  def perform_initial_pagination_setup(requested_tickets_count)
    params[:per_page] = oversample_page_size(requested_tickets_count)
    params[:page] = 1
  end

  def increment_page_number
    params[:page] += 1
  end

  def processing_end_time
    Time.current + DECO_MAX_TIME
  end

  def mark_insufficient_results
    Rails.logger.info('Marking as insufficent result')

    statsd_client.increment('insufficient_results')

    includes << :insufficient_results
  end

  def oversample_page_size(page_size)
    page_size * DECO_OVERSAMPLING_FACTOR
  end

  def statsd_client
    @statsd_client ||=
      Zendesk::StatsD::Client.new(namespace: 'routing_view_rendering')
  end

  def log_match_tickets_metrics(remaining_loops, scrolled_tickets_count)
    statsd_client.count(
      'match_tickets_deco_loops',
      DECO_MAX_LOOPS - remaining_loops
    )
    statsd_client.count('scrolled_tickets_count', scrolled_tickets_count)
  end
end
