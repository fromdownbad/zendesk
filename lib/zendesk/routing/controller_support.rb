module Zendesk::Routing::ControllerSupport
  UNKNOWN_ERROR       = 0
  REF_INTEGRITY_ERROR = 1
  CONFLICT_ERROR      = 2
  LIMIT_EXCEEDED      = 3

  protected

  def parse_error(e) # rubocop:disable Naming/MethodParameterName
    errors = (e.response && e.response.body && e.response.body['errors']) || []
    error_code = error_code(errors.first)

    if error_code == LIMIT_EXCEEDED
      return present_error_with_description(errors.first)
    end

    {code: error_code}
  end

  def with_error_handling
    yield
  rescue Kragle::BadRequest => e
    render json: parse_error(e), status: :bad_request
  rescue Kragle::ResponseError => e
    render json: parse_error(e),
           status: ((e.response && e.response.status) || :internal_server_error)
  end

  def present_error_with_description(error)
    if error['title'].include?('Too Many Attributes')
      error_title = 'Too Many Attributes'
      error_description = 'Limit reached. Too many attributes have ' \
                 'already been created.'
    elsif error['title'].include?('Too Many Attribute Values')
      error_title = 'Too Many Attribute Values'
      error_description = 'Limit reached. Too many attribute values have ' \
                 'already been created.'
    end

    {
      code: LIMIT_EXCEEDED,
      error: error_title,
      description: error_description
    }
  end

  def with_views_error_handling
    with_error_handling do
      yield
    end
  rescue Zendesk::Rules::RuleExecuter::UnprocessableEntity
    render status: :unprocessable_entity, json: {
      error:       'UnprocessableEntity',
      description: 'View could not be executed. Please check if view ' \
                   'definition is valid.'
    }
  rescue Zendesk::Rules::RuleExecuter::UnknownOccamError
    render status: :service_unavailable, json: {
      error:       'ServiceUnavailable',
      description: 'View service unavailable. Please try again later.'
    }
  end

  private

  def error_code(error)
    return UNKNOWN_ERROR if error.nil?

    title = error['title']

    return REF_INTEGRITY_ERROR if title.include?('Ref Integrity Error')
    return CONFLICT_ERROR if title.include?('Exists')
    return LIMIT_EXCEEDED if title.include?('Too Many')

    UNKNOWN_ERROR
  end
end
