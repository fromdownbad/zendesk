# report every job class/queue as separate tag so we can build alerts on them failing
class Zendesk::DatadogFailureReporter < Resque::Failure::Base
  def save
    tags = ["queue:#{queue}", "job:#{payload["class"].demodulize}"]
    tags << "exception_class:#{@exception.class.name}".first(200) if @exception.present?
    ZendeskJob::Resque.statsd_client.increment("resque.errors", tags: tags) if ZendeskJob::Resque.statsd_client
  end
end
