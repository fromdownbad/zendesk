# AgentDowngrader handles entitlements downgrade logic for a given agent and a list of products
# This is an interim solution before agent entitlements are all managed by Metropolis

module Zendesk
  module Users
    class AgentDowngrader
      private_class_method :new
      SUPPORT = Accounts::Client::SUPPORT_PRODUCT.to_sym
      CHAT = Accounts::Client::CHAT_PRODUCT.to_sym

      def self.perform(agent:, products:)
        new(agent: agent, products: products).downgrade
      end

      def initialize(agent:, products:)
        @agent = agent
        @products = products.map(&:to_sym) & [SUPPORT, CHAT]
      end

      def downgrade
        return unless products.any?
        return unless agent.is_agent?

        downgrade_chat if products.include?(CHAT)
        downgrade_support if products.include?(SUPPORT)

        # Validation is causing issues when re-activating expired trial subscriptions, see BILL-5996 for details
        Zendesk::SupportUsers::User.new(agent).save(validate: false)
      end

      private

      attr_reader :agent, :products

      delegate :account, to: :agent

      def downgrade_chat
        if (account.multiproduct? || account.spp?) && cp4_chat_agent?
          disable_in_staff_service
        else
          agent.zopim_identity.try(:destroy)
          downgrade_to_end_user if cp3_chat_only_agent?
        end
      end

      def disable_in_staff_service
        disable_chat_entitlement
      rescue Kragle::UnprocessableEntity => e
        if tried_to_disable_last_chat_admin?(e)
          # try again, ignoring validations this time
          disable_chat_entitlement(skip_validation: true)
          # and then make the account owner a chat admin instead
          make_chat_admin(account.owner)
        end
      end

      def tried_to_disable_last_chat_admin?(error)
        # this is a bit brittle; we're relying on a JSON string value in the http
        # response to determine that we attempted to delete the last chat admin
        error.response.body['errors'].detect do |error_hash|
          error_hash['title'].casecmp('chat admin required').zero?
        end.present?
      end

      def disable_chat_entitlement(skip_validation: false)
        disabled_chat_entitlement = { chat: { is_active: false } }
        staff_client.update_full_entitlements!(agent.id, disabled_chat_entitlement, skip_validation: skip_validation)
      end

      def make_chat_admin(agent)
        admin_chat_entitlement = { chat: { name: 'admin', is_active: true } }
        staff_client.update_full_entitlements!(agent.id, admin_chat_entitlement)
      end

      def downgrade_support
        if PermissionSet.contributor_exists?(account)
          downgrade_to_contributor
        elsif cp3_chat_only_agent?
          # Do nonthing
        elsif cp3_agent_with_chat?
          downgrade_to_chat_agent
        else
          downgrade_to_end_user
        end
      end

      def staff_client
        @staff_client ||= Zendesk::StaffClient.new(account)
      end

      def agent_keeping_chat?
        # is currently a chat agent, and we're not downgrading Chat for them
        cp4_chat_agent? && !products.include?(CHAT)
      end

      def cp3_chat_only_agent?
        agent.permission_set.try(:is_chat_agent?)
      end

      def cp4_chat_agent?
        agent.cp4_chat_agent?
      end

      def cp3_agent_with_chat?
        if products.include?(CHAT)
          false # We don't want to check stale existence of zopim_identity if Chat entitlements is downgraded
        else
          agent.zopim_identity.present? && account.has_chat_permission_set?
        end
      end

      def downgrade_to_contributor
        log_downgrade('Contributor')
        save_agent_data
        contributor_permission_set = account.permission_sets.find_by_role_type(PermissionSet::Type::CONTRIBUTOR)
        agent.roles = Role::AGENT.id
        agent.permission_set = contributor_permission_set
      end

      def downgrade_to_chat_agent
        if account.has_ocp_chat_only_agent_deprecation?
          PermissionSet.enable_contributor!(account)
          downgrade_to_contributor
        else
          log_downgrade('Chat-only agent')
          agent.roles = Role::AGENT.id
          agent.permission_set = account.chat_permission_set
        end
      end

      def downgrade_to_end_user
        log_downgrade('End user')

        save_agent_data
        agent.memberships.delete_all
        agent.permission_set = nil
        agent.roles = Role::END_USER.id
      end

      def save_agent_data
        increment_agent_downgrade_audit_count
        AgentDowngradeAudit.from_user(agent)
      end

      def increment_agent_downgrade_audit_count
        statsd_client = Zendesk::StatsD::Client.new(namespace: 'billing')
        statsd_client.increment('make_end_user')
      end

      def log_downgrade(downgrade_to_role)
        Rails.logger.info("Agent Downgrade to #{downgrade_to_role}: #{account.id}/#{account.subdomain} - #{agent.id}/#{agent.name}")
      end
    end
  end
end
