module Zendesk
  module Users
    module Identities
      module UserIdentityLimit
        def whitelisted_from_user_limits?
          !!account.owner && account.owner.is_verified? && !account.free_mail?
        end

        # NOTE
        #   If either of these methods change please look at the other method to ensure that
        #   the validation works for each use case

        private

        # This is enforced on the following models for the specified reasons (PR #28215):
        #
        # 1) user_identity - to prevent the creation of the identity itself
        #                    using new, build, create
        #
        # 2) user          - to mark the user as invalid so as to cause
        #                    zendesk_auth to fail to save and return HTTP 401
        #                    back to the client
        #
        # number_of_identities is the number_of_identities after this object would be saved
        def enforce_user_identity_limit(user)
          return unless user
          return unless account.has_user_identity_limit? && !whitelisted_from_user_limits?

          # The logic here says if we've loaded up the identities & added new records then we
          #   are confident that we called `user.save` vs saving an individual identity with `UserIdentity.save`.
          #
          # If the user.identities are loaded but `user.identities.any?(&:new_record?) == false` then
          #   the code path to get here would be `UserIdentity.save` (not User.save)
          #   Thus user.identities.size just gives the # of records you have in memory not
          #   the number of records persisted plus the one you are trying to save.
          number_of_identities = user.identities.loaded? && user.identities.any?(&:new_record?) ? user.identities.size : user.identities.count + 1

          if number_of_identities > user.account.settings.max_identities
            message = ::I18n.t("txt.errors.models.identities.user_limit_exceeded", limit: account.settings.max_identities)
            user.errors.add(:base, Api.error(message, error: "LimitExceeded", raw: true)) if user.errors[:base].blank?
            errors.add(:base, Api.error(message, error: "LimitExceeded", raw: true)) if errors[:base].blank?
          end
        end

        # This method is added for saving the user object itself.
        def enforce_identity_limit_for_user(user)
          return unless account.has_user_identity_limit? && !whitelisted_from_user_limits?

          if user.identities.size > account.settings.max_identities
            message = ::I18n.t("txt.errors.models.identities.user_limit_exceeded", limit: account.settings.max_identities)
            errors.add(:base, Api.error(message, error: "LimitExceeded", raw: true)) if errors[:base].blank?
          end
        end
      end
    end
  end
end
