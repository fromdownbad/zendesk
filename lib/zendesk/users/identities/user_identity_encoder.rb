module Zendesk
  module Users
    module Identities
      module UserIdentityEncoder
        IDENTITY_MAP = {
          "UserAnyChannelIdentity"      => ::Zendesk::Protobuf::Support::Users::V2::IdentityType::ANY_CHANNEL,
          "UserEmailIdentity"           => ::Zendesk::Protobuf::Support::Users::V2::IdentityType::EMAIL,
          "UserFacebookIdentity"        => ::Zendesk::Protobuf::Support::Users::V2::IdentityType::FACEBOOK,
          "UserForeignIdentity"         => ::Zendesk::Protobuf::Support::Users::V2::IdentityType::FOREIGN,
          "UserPhoneNumberIdentity"     => ::Zendesk::Protobuf::Support::Users::V2::IdentityType::PHONE_NUMBER,
          "UserTwitterIdentity"         => ::Zendesk::Protobuf::Support::Users::V2::IdentityType::TWITTER,
          "UserVoiceForwardingIdentity" => ::Zendesk::Protobuf::Support::Users::V2::IdentityType::AGENT_FORWARDING,
          "UserSDKIdentity"             => ::Zendesk::Protobuf::Support::Users::V2::IdentityType::SDK
        }.freeze

        def encode_identity_type(type)
          IDENTITY_MAP[type] || ::Zendesk::Protobuf::Support::Users::V2::IdentityType::UNKNOWN_IDENTITY_TYPE
        end
      end
    end
  end
end
