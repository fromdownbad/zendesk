module Zendesk
  module Users
    module Identities
      class Initializer
        attr_reader :account, :current_user, :user, :params

        def initialize(account, current_user, user, params)
          @account = account
          @current_user = current_user
          @user = user
          @params = params
          @params = params[:identity] if params[:identity].present?
        end

        def identity
          @identity ||= email_identity || twitter_identity ||
            facebook_identity || google_identity ||
            agent_forwarding_identity || phone_number_identity
        end

        def send_verification_email?
          !dont_send_verification_email?
        end

        private

        def email_identity
          if value = value_for_type(:email)
            @user.send_verify_email = send_verification_email?
            identity = UserEmailIdentity.new(user: @user, account: @account, value: value.to_s.downcase, is_verified: verified?)
            identity.set_primary_when_verifying! if @params[:primary]

            identity
          end
        end

        def twitter_identity
          if value = value_for_type(:twitter)
            UserTwitterIdentity.new(user: @user, account: @account, screen_name: value)
          end
        end

        def facebook_identity
          if value = value_for_type(:facebook)
            UserFacebookIdentity.new(user: @user, account: @account, value: value)
          end
        end

        def google_identity
          if value = value_for_type(:google)
            @user.send_verify_email = true
            identity = UserEmailIdentity.new(user: @user, account: @account, value: value, is_verified: verified?)
            identity.build_google_profile
            identity.google_profile.user_identity = identity
            identity
          end
        end

        def agent_forwarding_identity
          if value = value_for_type(:agent_forwarding)
            UserVoiceForwardingIdentity.new(user: @user, account: @account, value: value)
          end
        end

        def phone_number_identity
          if value = value_for_type(:phone_number)
            identity = UserPhoneNumberIdentity.new(user: @user, account: @account, value: value)
            identity.skip_phone_number_validation = @current_user.is_system_user? || !@account.apply_phone_number_validation?
            identity
          end
        end

        def dont_send_verification_email?
          verified? || exempt_from_verification?
        end

        def verified?
          !!@params[:verified]
        end

        def exempt_from_verification?
          return true if signup_not_required_for_user?
          return false if !@user.is_verified? && @account.is_welcome_email_when_agent_register_enabled? # not exempt if primary email is unverified, but account setting will still be checked
          return false if @current_user == @user # not exempt if adding an email for oneself

          verification_email_disabled?
        end

        def verification_email_disabled?
          @current_user.is_agent? && !@account.is_welcome_email_when_agent_register_enabled?
        end

        def signup_not_required_for_user?
          @user.new_record? && !@account.is_signup_required?
        end

        def value_for_type(type)
          # This is the v2 API params structure
          if @params[:type].to_s.downcase == type.to_s
            @params[:value]
          # This is the old style structure
          else
            @params[type]
          end
        end
      end
    end
  end
end
