module Zendesk
  module Users
    module Identities
      module ControllerSupport
        def self.included(base)
          base.before_action :ensure_manage_permissions, except: :index
          base.before_action :ensure_list_permissions,   only: :index
          base.before_action :ensure_valid_request, only: [:create]
          base.before_action :ensure_edit_access, only: [:create, :destroy]
        end

        protected

        def ensure_identity_verifiable
          head :bad_request unless identity.respond_to?(:send_verification_email)
        end

        def should_skip_verification_email?
          return false unless current_user.is_agent? && current_user != user
          return true if params[:identity] && params[:identity].fetch(:skip_verify_email, false)
          !current_account.is_welcome_email_when_agent_register_enabled? || user.has_verified_email?(user.email) && !!params[:verified]
        end

        def ensure_manage_permissions
          deny_access unless current_user.can?(:manage_identities_of, user) || current_user.can?(:manage_external_accounts_of, user)
        end

        def ensure_list_permissions
          # If you are viewing a user profile, you should have full view of the profile, Ticket: #428231
          deny_access unless current_user.can?(:edit, user) || current_user.can?(:view, user)
        end

        def ensure_valid_request
          head :bad_request unless new_identity.present?
        end

        def ensure_edit_access
          # current_user refers to the person who is editing and user is the person who is being edited
          deny_access if user.login_allowed?(:remote) && current_user != user && !current_user.is_agent?
        end

        def user
          @user ||= current_account.users.find(params[:user_id])
        end

        def identities
          user.identities
        end

        def identity
          @identity ||= user.identities.find(params[:id])
        end

        def new_identity
          @new_identity ||= initializer.identity
        end

        private

        def initializer
          @initializer ||= Zendesk::Users::Identities::Initializer.new(current_account, current_user, user, params)
        end
      end
    end
  end
end
