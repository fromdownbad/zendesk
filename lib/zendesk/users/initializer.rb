module Zendesk
  module Users
    # Used from a users controller to stage users. Takes a current_user and params as
    # argument, e.g. user = Zendesk::Users::Initializer.new(current_user, params).user
    # The returned user instance is ready to save.
    class Initializer
      USER_ACCESSIBLE_ATTRS  = [:name, :phone, :time_zone, :identities, :locale_id, :locale].freeze
      ADMIN_ACCESSIBLE_ATTRS = [:groups, :group_ids, :role_or_permission_set].freeze

      attr_reader :params, :current_user, :current_account, :user

      def initialize(current_account, current_user, params, change_by_staff_service = false)
        @params          = params
        @current_user    = current_user || current_account.anonymous_user
        @current_account = current_account
        # permit! is necessary here. Otherwise, we get an unpermitted
        # container, which we could just do with {}
        @params[:user] ||= ActionController::Parameters.new.permit!
        @change_by_staff_service = change_by_staff_service
      end

      def apply(user)
        @user = user
        user.current_user = current_user

        stage_params

        sanitize_accessible_params
        sanitize_tags_params
        sanitize_suspension_params
        sanitize_signature
        sanitize_custom_fields
        sanitize_password_params
        sanitize_email
        sanitize_phone
        apply_group_association
        apply_organization_association
        strip_excess_organization_keys
        prevent_undesired_ticket_access_changes

        # Set the via setting if present
        if via_setting = params[:user].try(:delete, :via)
          user.via = via_setting
        end

        apply_identities

        if locale = params[:user]&.delete(:locale)
          user.locale = locale
        end

        custom_created_at = params[:user].delete(:custom_created_at)
        migrated_from     = params[:user].delete(:migrated_from)

        # NOTE: Zendesk::Tickets::Initializer#normalize_collaborators
        # explicitly splices in user_id and action for API purposes. At the
        # time it was done, mass assignment filtered out the extra params,
        # but that goes away with Rails 5, so they need to be excised.
        user.attributes = params[:user].except(:action,
          :active_brand_id,
          :migrated_from,
          :overwrite_existing_name,
          :user_fields,
          :user_id)

        # order is important as all methods end up modifying the user.role and user.permission_set_id properties
        [:roles, :role_or_permission_set, :role, :custom_role_id, :role_id].each do |protected_attr|
          if protected_attr == :custom_role_id && params[:user].key?(:custom_role_id) # nil or other value
            user.custom_role_id = params[:user][:custom_role_id]

            if Arturo.feature_enabled_for?(:permissions_allow_admin_permission_sets, user.account)
              # Set user role to corresponding value if custom_role_id has been set
              user.role = user.permission_set.legacy_role_type.name if user.custom_role_id
            else
              # Set user role to be an agent if custom_role_id has been set
              user.role = 'Agent' if user.custom_role_id
            end
          else
            user.send("#{protected_attr}=", params[:user][protected_attr]) if params[:user][protected_attr]
          end
        end

        user.set_photo(params[:photo])
        user.set_password(params[:password])
        user.set_signature(params[:signature], rich: false) if params.key?(:signature)
        user.set_signature(params[:html_signature], rich: true) if params.key?(:html_signature)

        user.custom_field_values.update_from_hash(params[:user_fields]) if params.key?(:user_fields)

        custom_field_changes = user.custom_field_values.field_changes
        if custom_field_changes.present?
          custom_field_changes.each do |cf_change|
            user.add_domain_event(UserCustomFieldChangedProtobufEncoder.new(cf_change[0], cf_change[1], cf_change[2]).to_safe_object)
          end
        end

        if params[:user][:active_brand_id] && current_account.has_multiple_active_brands?
          user.active_brand_id = params[:user][:active_brand_id]
        end

        if custom_created_at && user.new_record?
          user.updated_at = Time.now.utc
          user.created_at = custom_created_at || user.updated_at
          user.record_timestamps = false
          user.settings.set(migration_settings(user.updated_at, migrated_from))
        end

        user
      end

      private

      # Order matters here because we want to do the plural option first I guess
      ORGANIZATION_ID_KEYS   = [:organization_ids, :organizations, :organization_id].freeze
      ORGANIZATION_NAME_KEYS = [:organization].freeze
      ORGANIZATION_KEYS      = (ORGANIZATION_ID_KEYS + ORGANIZATION_NAME_KEYS).freeze

      def apply_organization_association
        if current_user.agent_restriction?(:organization) || current_user.permission_set &&
            current_user.permission_set.permissions.end_user_profile.eql?('edit-within-org')
          user.organization = current_user.organization
        elsif organization_ids
          user.organizations = organization_ids
        end
      end

      def prevent_undesired_ticket_access_changes
        # When a user has a permission set (and access to that through has_permission_sets, has_chat_premission_set or has_light_agents features),
        # we handle the ticket access from the custom role.
        # The UI in lotus to change ticket access is disabled and this is to prevent API changes.
        if user.has_permission_set?
          params[:user].delete(:restriction_id)
        end
      end

      def strip_excess_organization_keys
        ORGANIZATION_KEYS.each { |key| params[:user].delete(key) }
      end

      def organization_ids
        @organization_ids ||=
          if k = organization_id_key
            Array.wrap(params[:user].delete(k))
          elsif k = organization_name_key
            name = params[:user].delete(k)[:name].to_s.downcase
            new_org_ids = current_account.organizations.where('lower(name) = ?', name).pluck(:id)
            user.organization_ids | new_org_ids # when given a named org, we add it to the list, not replace
          end
      end

      def organization_id_key
        ORGANIZATION_ID_KEYS.detect { |key| params[:user].key?(key) }
      end

      def organization_name_key
        ORGANIZATION_NAME_KEYS.detect do |key|
          # ensure that the key contains a hash so we can lookup the name param
          params.dig(:user, key).respond_to?(:key?)
        end
      end

      def stage_params
        @params = Api::V2::UserParams.new(params).to_hash

        if user.new_record?
          params[:user][:skip_verify_email] ||= !@current_account.is_welcome_email_when_agent_register_enabled?
        end
      end

      def sanitize_email
        return unless email = params[:user].delete(:email).presence
        params[:user][:identities] ||= []
        params[:user][:identities] += [{ type: :email, value: email }]
      end

      def sanitize_phone
        param_phone = params[:user][:phone].presence
        param_shared_phone_number = if params[:user][:direct_phone_line].nil?
          params[:user].delete(:shared_phone_number)
        else
          params[:user].delete(:direct_phone_line).to_s == 'false'
        end

        user.skip_phone_number_validation = @current_user.is_system_user? || !@current_account.apply_phone_number_validation?

        return unless param_phone

        extension = param_phone.to_s.match(/\S+(x\s*\d+)$/).try(:captures).try(:first)
        user.remove_shared_phone_number_extension
        if extension.present?
          user.add_shared_phone_number_extension(extension)

          # remove the extension from the phone number
          param_phone[extension] = ''
        end

        phone_number = ::Users::PhoneNumber.new(@current_account, param_phone)

        if param_shared_phone_number.nil? && phone_number.valid_and_unique?
          user.remove_phone_number_identity(param_phone)
          add_phone_number_identity(param_phone)
        end

        if param_shared_phone_number == false
          return if @current_account.is_end_user_phone_number_validation_enabled? && !phone_number.valid?
          add_phone_number_identity(param_phone)
        end

        if param_shared_phone_number == true
          user.remove_phone_number_identity(param_phone)
        end
      end

      def add_phone_number_identity(phone)
        params[:user][:identities] ||= []
        params[:user][:identities] += [{ type: :phone_number, value: phone }]
      end

      def sanitize_custom_fields
        if params[:user][:user_fields].present?
          params[:user_fields] = params[:user].delete(:user_fields)
          unless current_user.is_system_user?
            params[:user_fields].delete_if { |k| current_account.system_user_custom_field_keys.include?(k) }
          end
        elsif params[:user][:user_fields]
          params[:user].delete :user_fields
        end
      end

      def sanitize_signature
        if params[:user].key?(:html_signature)
          params[:html_signature] = params[:user][:html_signature]
        elsif params[:user].key?(:signature)
          if params[:user][:signature].is_a?(String) || params[:user][:signature].nil?
            params[:signature] = params[:user][:signature]
          elsif params[:user][:signature].key?(:value)
            params[:signature] = params[:user][:signature][:value]
          end
        end

        params[:user].delete(:html_signature)
        params[:user].delete(:signature)
      end

      # Removes params that the current_user is not allowed to access,
      # unless the change is made by the staff service on behalf of some user.
      # The staff service implements its own accessibility check,
      # and should be allowed to make changes to any field on the user table.
      def sanitize_accessible_params
        return if @change_by_staff_service
        # ID is not accessible, and will throw
        # a warning and be filtered out anyway
        params[:user].delete(:id)

        if current_user.is_end_user?
          # Allow only these attributes
          params[:user].slice!(*USER_ACCESSIBLE_ATTRS)
        elsif !current_user.is_admin?
          # Do not allow these attributes
          params[:user] = params[:user].except(*ADMIN_ACCESSIBLE_ATTRS)
        end
      end

      def sanitize_password_params
        if params[:password].blank? && params[:user][:password].present?
          params[:password] = params[:user][:password]
        end

        params[:user].delete(:password)
      end

      # Agents can only add tags to end-users. Admins can add tags to everybody.
      def sanitize_tags_params
        unless current_user.can?(:modify_user_tags, user)
          params[:user].delete(:tags)
          params[:user].reject! { |key, _value| key.to_s =~ /_tag[s]?\z/ }
        end
      end

      # Owners cannot be suspended by anyone - this should go into CanCan once we begin using
      # that on a model level in e.g. before_save
      def sanitize_suspension_params
        if user.is_account_owner? || user == current_user
          params[:user].delete(:suspended)
        end
      end

      def apply_identities
        identities = params[:user].delete(:identities).presence
        if identities.present?
          identities.each do |param_identity|
            next unless identity = Zendesk::Users::Identities::Initializer.new(current_account, current_user, user, param_identity).identity
            next if user.identities.any? do |actual_identity|
              if actual_identity.type == identity.type
                actual_identity.type == "UserEmailIdentity" ? actual_identity.value.casecmp(identity.value) == 0 : actual_identity.value == identity.value
              end
            end

            param_verified = params[:user][:is_verified]
            unless param_verified.nil?
              identity.is_verified = param_verified
              params[:user].delete(:is_verified)
            end
            user.identities << identity
          end
        end

        unless params[:user][:is_verified].nil?
          if user.new_record? || !user.identities.exists?
            params[:user].delete(:is_verified)
          end
        end
      end

      def apply_group_association
        if params[:user].key?(:group_ids)
          user.groups = params[:user].delete(:group_ids)
        end
      end

      def migration_settings(migration_time, migration_from)
        {
          migrated_at: migration_time,
          migrated_from: migration_from
        }
      end
    end
  end
end
