require 'zendesk/search'

module Zendesk
  module Users
    # Used from a users controller to find users. Takes account and params hash as
    # arguments, e.g. @users = Zendesk::Users::Finder.new(current_user, params).users
    class Finder
      DEFAULT_ORDER = { id: :asc }.freeze

      INDEX_ON_ACCOUNT_ID_IS_ACTIVE_ROLES = 'index_users_on_account_id_and_is_active_and_roles'.freeze
      INDEX_ON_ACCOUNT_ID_ID = 'index_users_on_account_id_id'.freeze

      attr_reader :account, :query, :params

      def initialize(account, user, params)
        @user     = user
        @account  = account
        @query    = params[:query].present? ? params[:query].strip : nil
        @params   = params
      end

      def users(options = {})
        ensure_permissions

        options[:order]   ||= DEFAULT_ORDER
        options[:include] ||= Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES

        # A finder method will return nil if it refuses to try the find or
        # if it finds nothing. An empty ([]) result is a valid result.
        result   = find_by_direct_lookup(options)
        result ||= find_by_association(options)
        result ||= find_by_permissions(options)
        result ||= find_by_roles(options)
        result ||= find_by_database_scan(options)
        result ||= find_by_search(options)

        result
      end

      def many_users(ids, options = {})
        options[:order]   ||= DEFAULT_ORDER
        options[:include] ||= Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES

        find_by_id_list(ids, options).tap do |users|
          if user = users.detect { |u| !@user.can?(:view, u) }
            Rails.logger.info("User #{@user.id} cannot view user #{user.id}")
            raise Access::Denied.new("Not authorized!", :view, user)
          end
        end
      end

      # Methods for finding a user via a well defined (functionally primary) key
      def find_by_direct_lookup(options)
        return if params[:external_id].nil? && sanitized_email.nil?

        conditions = if params[:external_id].present?
          { roles: roles, external_id: params[:external_id] }
        else
          { roles: roles, user_identities: { value: sanitized_email, type: UserEmailIdentity.name, account_id: account.id }}
        end

        find_users(options.merge(conditions: conditions))
      end

      # Method for finding a bunch of users by id
      def find_by_id_list(ids, options)
        key = options.delete(:key) || :ids
        key = key.to_s.chomp('s').to_sym
        find_users(options.merge(conditions: { key => ids }))
      end

      # Methods for finding users via a specific association
      def find_by_association(options)
        kind = nil

        if key = (@params[:group] || @params[:group_id])
          kind = :groups
        elsif key = (params[:organization] || params[:organization_id])
          kind = :organizations
        end

        return if kind.nil?

        record = if key.is_a?(Integer) || key =~ /^[0-9]+$/
          account.send(kind).find_by_id(key)
        else
          account.send(kind).find_by_name(key)
        end

        options[:conditions] = restricted_conditions({})

        options[:conditions][:roles] = roles if params[:role].present?

        if params[:permission_set].present? && permission_set.try(:id)
          options[:conditions][:permission_set_id] = permission_set.id
        end

        if record.present?
          scope = if @account.has_fix_group_filter_for_agent_search? && query.present?
            record.users.where("name LIKE ?", "%#{query}%")
          else
            record.users
          end

          self.class.fetch_users(scope, options)
        end
      end

      def find_by_permissions(options)
        return if params[:permission_set].nil?

        permission_set_id = permission_set.try(:id)

        return [] if permission_set_id.nil?

        conditions = if Arturo.feature_enabled_for?(:permissions_allow_admin_permission_sets, account)
          { permission_set_id: permission_set_id }
        else
          { roles: Role::AGENT.id, permission_set_id: permission_set_id }
        end

        find_users(options.merge(conditions: conditions))
      end

      def find_by_roles(options)
        return if query.present?

        use_index, conditions = if params[:legacyagents].present?
          [true, { roles: Role::AGENT.id, permission_set_id: nil }]
        elsif params[:seat_type].present?
          # Setting `use_index` to false here because it won't benefit from the use
          # of the `index_users_on_account_id_and_is_active_roles`. With the `:seat_type` present,
          # the `find_users` function will query using the `UserSeat` model instead of using
          # `account.users`
          [false, { roles: roles, seat_type: params[:seat_type] }]
        else
          [!all_roles?, { roles: roles }]
        end

        result = find_users(options.merge(conditions: conditions))

        if !params[:seat_type].present? && !params[:legacyagents].present?
          if roles.count == 1 || (roles.count == 2 && no_end_users?)
            result.use_index(INDEX_ON_ACCOUNT_ID_IS_ACTIVE_ROLES)
          else
            result.use_index(INDEX_ON_ACCOUNT_ID_ID)
          end
        else
          if use_index && !inactive_users_included?(options)
            result.use_index(INDEX_ON_ACCOUNT_ID_IS_ACTIVE_ROLES)
          else
            result
          end
        end
      end

      def find_by_database_scan(options)
        return if query.blank? || !allow_database_scan?

        conditions = [
          "roles IN (?) AND (users.external_id = ? OR users.name LIKE ? OR "\
          "user_identities.value LIKE CONVERT(? USING LATIN1) AND user_identities.account_id = ?)",
          roles, query, "%#{query}%", "#{query}%", account.id
        ]

        result = find_users(options.merge(conditions: conditions, references: :user_identities))
        return if result.blank?
        result
      end

      def find_by_search(options)
        # The search interface doesn't like "users.id ASC" as an order.
        Zendesk::Search.search(@user, query, options.except(:order).merge(type: :user,
                                                                          incremental: true,
                                                                          with: restricted_conditions(user_role: roles)))
      end

      def find_users(options)
        options[:conditions] = restricted_conditions(options[:conditions])
        if options[:conditions].is_a?(Hash) && options[:conditions][:seat_type].present?
          # return this scope early since it does not need further filtering
          return UserSeat.joins(:user).preload(:user).where(account_id: account.id, seat_type: options[:conditions][:seat_type]).map(&:user)
        end

        scope = if inactive_users_included?(options)
          account.all_users
        else
          account.users
        end

        if references = options.delete(:references)
          scope = scope.references(references)
        end

        self.class.fetch_users(scope, options)
      end

      def self.fetch_users(scope, options)
        where = options[:conditions]
        order = options[:order]
        includes = options[:include]

        scope = scope.where(where) if where.present?
        scope = scope.order(order) if order.present?
        scope = scope.includes(includes) if includes.present?

        scope
      end

      def roles
        @resolved_roles ||= begin
          roles = params[:type] || params[:role] || [Role::END_USER.id, Role::AGENT.id, Role::ADMIN.id]
          roles = roles.split("/") if roles.is_a?(String) # Pagination writes roles to a single / delimited parameter
          roles.map { |role| Role.all.detect { |r| /\A\d+\Z/.match?(role.to_s) ? r.id == role.to_i : r.name.downcase == role } }.compact.map(&:id)
        end
      end

      def all_roles?
        @all_roles ||= roles.count == Role.all.reject { |r| r == Role::LEGACY_AGENT }.count
      end

      def permission_set
        @permission_set ||= account.permission_sets.find_by_id(params[:permission_set])
      end

      def sanitized_email
        query.present? ? Zendesk::Mail::Address.sanitize(query) : nil
      end

      # If there is no query, always go to the database and paginate on known indexes.
      # When there is a query, you are allowed to scan the database only if you're an account
      # with a contained number of users
      def allow_database_scan?
        query.blank? || no_end_users? || limited_user_count?
      end

      def limited_user_count?
        !account.users.capped_count(1_000).capped?
      end

      def no_end_users?
        !roles.member?(Role::END_USER.id)
      end

      def restricted_conditions(conditions)
        organization_ids = @user.organization_memberships.map(&:organization_id)

        if organization_ids.present? && @user.restriction_id == RoleRestrictionType.ORGANIZATION
          if conditions.is_a?(Hash)
            conditions[:organization_id] = organization_ids
          elsif conditions.is_a?(Array)
            conditions.first << " AND organization_id IN (?)"
            conditions       << organization_ids
          end
        end

        conditions
      end

      private

      def ensure_permissions
        if @params.key?(:organization_id)
          organization = @account.organizations.find(params[:organization_id])
          unless @user.can?(:view, organization)
            Rails.logger.info("User #{@user.id} cannot view organization #{organization.id}")
            raise Access::Denied.new("Not authorized!", :view, organization)
          end
        elsif query.present? && @user.can?(:search, User)
          nil
        elsif !@user.can?(:list, User)
          Rails.logger.info("User #{@user.id} cannot list users")
          raise Access::Denied.new("Not authorized!", :list, User)
        end
      end

      def inactive_users_included?(options)
        @inactive_users_included ||= options.delete(:include_inactive) && account.has_api_enable_show_many_deleted_users?
      end
    end
  end
end
