require 'users/password_invalid'

module Zendesk
  # Manages the users in an account.
  #
  # All actions are performed on behalf of an "acting user".
  #
  class UserManager
    class Error < StandardError; end

    class PasswordInvalid < Error; end

    class PasswordUnchanged < Error; end

    class PasswordIncorrect < Error; end

    class Unauthorized < Error; end

    MAX_PASSWORD_LENGTH = 128

    # Create a new user manager for a given account.
    #
    # Options
    #
    # :authentication - The current authentication context
    #
    # Raises ArgumentError if the authentication context is invalid or
    #   if the acting user does not belong to the account.
    def initialize(authentication)
      @authentication = authentication
      @acting_user    = @authentication.current_user
      @account        = @authentication.current_account

      if @account.nil? || @acting_user.nil? || @acting_user.account != @account
        raise ArgumentError
      end
    end

    # Change the password for a user.
    #
    # user - the User whose password should be changed.
    #
    # Options
    #
    # :from - the String password that is currently used by the user.
    # :to   - the String password that should be the user's password henceforth.
    # :shared_session_record_id - Specifies the shared_session record id that is requesting this password reset.
    #
    # Examples
    #
    #   manager = UserManager.new(authentication)
    #   manager.change_password_for(user, :from => "monkeys", :to => "dolphins")
    #
    # Returns nothing.
    # Raises PasswordIncorrect if the current password is not correct.
    # Raises PasswordInvalid if the new password is not valid, given the user's security policy.
    # Raises PasswordUnchanged if the new password is the same as the old.
    # Raises Unauthorized if the user does not belong to the account.
    def change_password_for(user, options = {})
      current_password = options.fetch(:from)
      new_password     = options.fetch(:to)

      if password_unchanged?(user, new_password)
        user.errors.add(:password, :taken)
        raise PasswordUnchanged
      end

      unless user.authenticated?(current_password)
        user.errors.add(:base, ::I18n.t('txt.users.update_password.incorrect'))
        raise PasswordIncorrect
      end

      reset_password_for(user, to: new_password, verify_identity: false, shared_session_record_id: options[:shared_session_record_id])
    rescue Unauthorized => e
      user.errors.add :base, :invalid
      raise e
    rescue PasswordInvalid => e
      ensure_password_error_added(user)
      raise e
    end

    # Reset the password for a user.
    #
    # Allows a password to be reset without authenticating with an existing password.
    #
    # user - the User whose password should be reset.
    #
    # Options
    #
    # :to               - the String password that should be the user's password henceforth.
    # :verify_identity  - whether or not to verify the primary identity of the user
    # :shared_session_record_id - Specifies the shared_session record id that is requesting this password reset.
    #
    # Examples
    #
    #   manager = UserManager.new(authentication)
    #   manager.reset_password_for(user, :to => "dolphins")
    #
    # Returns nothing.
    # Raises PasswordInvalid if the new password is not valid, given the user's security policy.
    # Raises Unauthorized if the user does not belong to the account.
    def reset_password_for(user, options = {})
      new_password = options.fetch(:to)

      if user.account != @account
        user.errors.add(:base, :invalid)
        raise Unauthorized
      end

      user.current_user = @acting_user

      begin
        if new_password.length > MAX_PASSWORD_LENGTH
          user.errors.add(:password, ::I18n.t('txt.security_policy.requirements.text_max_length'))
          raise PasswordInvalid
        end

        user.change_password!(new_password)

        @authentication.password_changed_for_user!(user, shared_session_record_id: options[:shared_session_record_id])
        verify_identity!(user) if options[:verify_identity]
      rescue ::Users::PasswordInvalid
        ensure_password_error_added(user)
        raise PasswordInvalid
      end
    end

    private

    def password_unchanged?(user, new_password)
      user.security_policy.has_password_history? && user.authenticated?(new_password)
    end

    def ensure_password_error_added(user)
      user.errors.add(:password, :invalid) unless user.errors.include?(:password)
    end

    def verify_identity!(user)
      return if user.is_verified?
      identity = user.identities.email.first
      if !identity.google? && !identity.is_verified?
        identity.verify!
        Rails.logger.warn("zd876092: Verifying identity due to manual password change. [#{identity.value}, #{identity.id}, #{identity.account_id}]")
      end
    end
  end
end
