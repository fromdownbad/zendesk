# require 'zendesk/search'

module Zendesk
  module Users
    class FinderWithPagination < Finder
      attr_reader :page, :per_page

      def initialize(account, user, params, page = 1, per_page = 100)
        super(account, user, params)
        @page     = page
        @per_page = per_page
      end

      def users(options = {})
        setup_pagination(options)
        super
      end

      def many_users(ids, options = {})
        setup_pagination(options)
        super
      end

      def find_by_association(options)
        super.try(:paginate, sanitize_options_for_paginate(options))
      end

      def find_users(options)
        super.paginate(sanitize_options_for_paginate(options))
      end

      private

      def setup_pagination(options)
        options[:page]     ||= page
        options[:per_page] ||= per_page
      end

      def sanitize_options_for_paginate(options)
        options.slice(:page, :per_page)
      end
    end
  end
end
