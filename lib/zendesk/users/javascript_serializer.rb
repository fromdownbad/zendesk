module Zendesk
  module Users
    class JavascriptSerializer
      attr_reader :user

      def initialize(user, options = {})
        @user = user
        @account = user.try(:account)
        @is_assumed_identity = options[:is_assumed_identity] || false
        @ssl_environment = options[:ssl_environment] || false
        @is_mobile_agent = options[:is_mobile_agent] || false
      end

      def as_json
        {
          account: account_as_json,
          user: user_as_json,
          organization: organization_as_json,
          voice: voice_as_json,
          misc: {
            toTangoID: Zendesk::Configuration.fetch(:saas_pulse_id),
            isMobileAgent: @is_mobile_agent
          }
        }
      end

      # Temporarily support the JS user model format until we've migrated to the JSON version.
      module Deprecated
        def user_to_js
          if user
            "new User(#{user_as_json.to_json})".html_safe
          else
            "null"
          end
        end

        def account_to_js
          if user && user.account
            "new Account(#{account_as_json.to_json})".html_safe
          else
            "null"
          end
        end

        def organization_to_js
          if user && user.organization
            "new Organization(#{organization_attributes.to_json})".html_safe
          else
            "null"
          end
        end
      end
      include Deprecated

      private

      def user_as_json
        return {} if user.nil?
        user_attributes
      end

      def account_as_json
        return {} if user.nil? || user.account.nil?
        account_attributes
      end

      def organization_as_json
        return {} if user.nil? || user.organization.nil?
        organization_attributes
      end

      def voice_as_json
        return {} if user.nil? || @account.nil? || !user.is_admin?
        voice_attributes
      end

      def voice_attributes
        {
          account_sid: Zendesk::Configuration.dig(:voice, :twilio, :account_sid),
          api_version: '2010-04-01'
        }
      end

      def organization_attributes
        {
          id: user.organization.id,
          name: user.organization.name,
          isShared: user.organization.is_shared
        }
      end

      def user_attributes
        {
          id: user.id,
          externalId: user.external_id,
          name: user.name,
          first_name: user.first_name,
          role: user.roles,
          isOwner: user.is_account_owner?,
          accessibleForums: (user.accessible_forums_count > 0 || user.is_admin?),
          hasEmail: user.email.present?,
          email: user.email,
          restriction: user.restriction_id,
          uses12HourClock: user.uses_12_hour_clock?,
          passwordExpiresAt: user.password_expires_at,
          voiceClientName: user.client_name,
          voicePresenceName: user.presence_name,
          version: user.cache_key,
          localeVersion: locale_version,
          tags: user.tag_array,
          managePinnedOrder: user.can?(:manage_pinned_order, Entry),
          locale_id: user.translation_locale.id,
          localeIsDifferent: locale_is_different_string,
          assumed: @is_assumed_identity,
          sharedOrganizations: shared_organizations_as_json(user),
          availabilityControlsEnabled: user.availability_controls_enabled?
        }
      end

      def account_attributes
        return {} if @account.subscription.nil?

        is_trial = @account.subscription.is_trial? && !@account.subscription.is_sponsored?
        is_trial &&= @account.subscription.manual_discount_expires_on.to_date <= 30.days.from_now.to_date if @account.subscription.manual_discount_expires_on

        features = [].tap do |f|
          f << 'businessHours'                if @account.business_hours_active?
          f << 'customerSatisfaction'         if @account.subscription.has_customer_satisfaction? && @account.has_customer_satisfaction_enabled?
          f << 'ticketSharingTriggers'        if @account.has_ticket_sharing_triggers?
          f << "cms"                          if @account.has_cms?
          f << "lotus" # can be removed if no client uses it
          f << "lotusForAgents"
          f << "placeholderSuggestions"
          f << "useStatusHold"                if @account.use_status_hold?
          f << "mobile"                       if @account.has_mobile_switch_enabled?
          f << "secureSessions"               if @account.ssl_should_be_used?
          f << 'untilDueDate'
          f << "userModel"
          f << "multipleOrganizations"        if @account.has_multiple_organizations_enabled?
          f << "voiceClassicTwilio13"         if @account.has_voice_classic_twilio_13?
          f << "multibrand"                   if @account.has_multibrand?
          f << "userAndOrganizationTags"      if @account.has_user_and_organization_tags?
          f << "chatProductTray"              if @account.has_chat_product_tray?
          f << "automaticAnswers"             if @account.has_automatic_answers_enabled?
          f << "voiceHtml5PlayerForGreetings" if @account.has_voice_html5_player_for_greetings?
          f << "hasEmailCcsLightAgentsV2"     if @account.has_email_ccs_light_agents_v2?
        end

        host = Zendesk::Configuration[:host]
        port = Zendesk::Configuration[:port]

        host += ":#{port}" if port.present? && ![80, 443].include?(port.to_i)

        {
          id: @account.id,
          name: @account.name,
          showUserProfile: @account.is_end_user_profile_visible?,
          showChangePassword: @account.is_end_user_password_change_visible?,
          ValidatePhoneNumbers: @account.is_end_user_phone_number_validation_enabled?,
          hasRemoteAuthentication: user.login_allowed?(:remote),
          subdomain: @account.subdomain,
          domain: host, # For hostmapped accounts, are we on which zendesk environment?
          isZendesk: @account.subdomain == 'support',
          isSandbox: @account.is_sandbox?,
          isOpen: @account.is_open?,
          isInTrial: @account.subscription.is_trial?,
          isPayingCustomer: @account.subscription.credit_card.present?,
          forumsTitle: @account.render_forum_title(user),
          urlPrefix: @account.url(mapped: true),
          secureUrlPrefix: @account.url(mapped: true, protocol: @ssl_environment ? 'https' : 'http'),
          creationDate: @account.subscription.created_at,
          lastTrialDay: is_trial ? @account.subscription.last_day_of_trial : nil,
          daysLeftInTrial: is_trial ? @account.subscription.days_left_in_trial : nil,
          planType: @account.subscription.plan_type,
          planName: @account.subscription.plan_name,
          showExtendedTicketMetrics: @account.extended_ticket_metrics_visible?,
          rulesCanReferenceMacros: @account.has_rules_can_reference_macros?,
          assetHost: Zendesk::AccountCdn.provider_pool.account_provider(@account).try(:host),
          features: features,
          twitter_accounts: twitter_accounts_as_json(@account),
          facebookPages: facebook_pages_as_json(@account),
          isLotusVisibleToAgents: true,
          doAgentsPreferLotus: true,
          clientSideLogging: @account.has_client_side_logging?
        }
      end

      def locale_is_different_string
        if user.is_agent? && ::I18n.translation_locale != user.translation_locale
          ::I18n.t('txt.views.shared.header.revert_to_my_language', locale: user.translation_locale)
        else
          false
        end
      end

      def twitter_accounts_as_json(account)
        MonitoredTwitterHandle.where(account_id: account.id).map do |handle|
          {
            id: handle.id,
            screen_name: handle.mention_name,
            authorized: handle.active?,
            mention_autoconversion_enabled_at: handle.mention_autoconversion_enabled_at.try(:to_i)
          }
        end
      end

      def shared_organizations_as_json(user)
        return [] if user.nil? || !user.account.has_multiple_organizations_enabled? || user.organizations.nil?

        shared_organizations = user.restriction_id == 2 ? user.organizations : user.organizations.select(&:is_shared)
        shared_organizations.map do |organization|
          {
            id: organization.id,
            name: organization.name,
            isShared: organization.is_shared
          }
        end
      end

      def facebook_pages_as_json(account)
        ::Facebook::Page.where(account_id: account.id).monitored.map do |page|
          json_page = {
            id: page.id,
            name: page.name,
            unauthorized: page.unauthorized?
          }

          if page.unauthorized?
            json_page[:reason_for_unauthorization] = page.page_settings["reason_for_unauthorization"]
          end

          json_page
        end
      end

      def locale_version
        # TODO: figure out where a request's locale *actually* is and keep it there.
        locale = ::I18n.translation_locale
        "#{locale.id}.#{locale.try(:updated_at).try(:to_i)}"
      end
    end
  end
end
