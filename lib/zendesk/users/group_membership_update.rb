module Zendesk
  module Users
    class GroupMembershipUpdate
      attr_reader :user, :groups_being_set, :default_group_id_being_set

      def initialize(user, groups_or_group_ids_being_set, default_group_id_being_set)
        @user = user
        @groups_being_set = groups_or_group_ids_being_set
        @default_group_id_being_set = default_group_id_being_set
      end

      def update
        if user.group_ids.empty?
          group_update_without_existing_groups
        else
          group_update_with_existing_groups
        end
      end

      private

      def group_update_without_existing_groups
        # assume no current default group
        case groups_being_set
        when nil
          group_update_without_groups_without_groups_being_set
        when []
          group_update_without_groups_with_groups_set_to_no_groups
        else
          group_update_without_groups_with_groups_being_set
        end
      end

      def group_update_without_groups_without_groups_being_set
        user.set_default_group(default_group_id_being_set || user.account.default_group.try(:id))
      end

      def group_update_without_groups_with_groups_set_to_no_groups
        if default_group_id_being_set
          user.errors.add(:default_group_id, ::I18n.t("txt.errors.users.default_group.message"))
        else
          user.set_default_group(user.account.default_group.try(:id))
        end
      end

      def group_update_without_groups_with_groups_being_set
        if default_group_id_being_set
          if group_ids_being_added.include?(default_group_id_being_set)
            user.add_group_memberships(group_ids_being_added)
            user.set_default_group(default_group_id_being_set)
          else
            user.errors.add(:default_group_id, ::I18n.t("txt.errors.users.default_group.message"))
          end
        else
          user.add_group_memberships(group_ids_being_added)
          user.set_default_group(groups_being_set.first.is_a?(Group) ? groups_being_set.first.id : groups_being_set.first)
        end
      end

      def group_update_with_existing_groups
        case groups_being_set
        when nil
          group_update_with_groups_without_groups_being_set
        when []
          group_update_with_groups_with_groups_being_set_to_no_groups
        else
          group_update_with_groups_with_groups_being_set
        end
      end

      def group_update_with_groups_without_groups_being_set
        if default_group_id_being_set
          if user.group_ids.include?(default_group_id_being_set)
            user.set_default_group(default_group_id_being_set) if user.default_group_id != default_group_id_being_set
          else
            user.errors.add(:default_group_id, ::I18n.t("txt.errors.users.default_group.message"))
          end
        elsif !user.default_group_id
          user.set_default_group(user.memberships.first.group_id)
        end
      end

      def group_update_with_groups_with_groups_being_set_to_no_groups
        user.errors.add(:default_group_id, ::I18n.t("txt.errors.users.default_group.message"))
      end

      def group_update_with_groups_with_groups_being_set
        if (user.group_ids + group_ids_being_added - group_ids_being_removed).include?(default_group_id_being_set || user.default_group_id)
          user.add_group_memberships(group_ids_being_added)
          user.remove_group_memberships(group_ids_being_removed)
          user.set_default_group(default_group_id_being_set) if user.default_group_id != default_group_id_being_set
        else
          user.errors.add(:default_group_id, ::I18n.t("txt.errors.users.default_group.message"))
        end
      end

      def group_ids_being_removed
        @groups_being_removed ||= group_ids_difference && (user.group_ids_being_removed = group_ids_difference.removed)
      end

      def group_ids_being_added
        @groups_being_added ||= group_ids_difference && group_ids_difference.added
      end

      def group_ids_difference
        @group_ids_difference ||= begin
          return unless groups_being_set
          IdSetsDifference.new(
            user.group_ids, Array.wrap(groups_being_set)
          )
        end
      end
    end
  end
end
