module Zendesk
  module Users
    module ControllerSupport
      UserMergeError = Class.new(StandardError)
      UserAuthenticationError = Class.new(StandardError)

      def self.included(base)
        base.after_action :update_session_locale, only: [:update]
      end

      protected

      def update_session_locale
        return unless [current_user.id.to_s, 'me'].include?(params[:id].to_s) # only update session if updating current user
        return unless shared_session[:locale_id] # if it isn't currently set, no reason to change it
        return unless params[:user].key?(:locale_id) # don't update the session if the user language didn't change

        shared_session[:locale_id] = current_user.translation_locale.id
      end

      def finder(per_page: nil, cursor_pagination: false)
        if cursor_pagination
          Zendesk::Users::Finder.new(
            current_account,
            current_user,
            params.except("controller", "action", "format")
          )
        else
          Zendesk::Users::FinderWithPagination.new(
            current_account,
            current_user,
            params.except("controller", "action", "format"),
            get_page,
            per_page || params[:per_page] || 100
          )
        end
      end

      def users(options = {})
        per_page = options[:per_page]
        cursor_pagination = options[:cursor_pagination]
        cursor_pagination_v2 = options[:cursor_pagination_v2]
        users = finder(per_page: per_page, cursor_pagination: cursor_pagination).users

        if cursor_pagination
          if cursor_pagination_v2 || current_account.arturo_enabled?('cursor_pagination_remove_v1_users')
            paginate_with_cursor(sorted_scope_for_cursor(users))
          else
            params[:limit] = Zendesk::CursorPagination::ControllerSupport::CURSOR_PAGINATION_MAX_SIZE if params[:limit].blank?

            if current_account.arturo_enabled?('cursor_pagination_product_limit_users')
              if params[:limit] && params[:limit] > Zendesk::CursorPagination::ControllerSupport::CURSOR_PAGINATION_MAX_SIZE
                params[:limit] = Zendesk::CursorPagination::ControllerSupport::CURSOR_PAGINATION_MAX_SIZE
              end
            end

            params[:order] ||= Zendesk::CursorPagination::ControllerSupport::DEFAULT_CURSOR_ORDERING
            params[:order_without_created_at] = true

            users.paginate_with_cursor(params)
          end
        # Cache key is set using to_sql on collection so need to check users is an object that can use this
        elsif users.respond_to?(:to_sql)
          # unscope(:from) removes the USE INDEX clause as it is not optimal for counts in certain cases. Allow query optimizer to choose index.
          if capped_count = options.delete(:capped_count)
            # Use Finder instead of FinderWithPagination for capped count so we don't use will_paginate's count implementation, which does not limit correctly!
            paginate(users, options.merge(total_entries: finder(cursor_pagination: true).users.unscope(:from).capped_count(capped_count)))
          else
            paginate(users, options.merge(total_entries: cached_pagination_count(users.unscope(:from))))
          end
        else
          users
        end
      end

      def many_users(many_ids, options = {})
        key = options.delete(:key) || :ids
        include_inactive = options.delete(:include_inactive) || false
        finder(per_page: options[:per_page], cursor_pagination: options[:cursor_pagination]).many_users(many_ids, key: key, include_inactive: include_inactive)
      end

      def create_many_users_limit
        current_account.settings.many_users_limit || max_per_page
      end

      def requested_user
        @requested_user ||= begin
          user = current_account.users.includes(user_finder_includes).find(params[:id])
          user.current_user = current_user
          user
        end
      end

      def new_user
        @new_user ||= current_account.users.new.tap do |new_user|
          user_initializer.apply(new_user)
        end
      end

      def user_initializer
        Zendesk::Users::Initializer.new(current_account, current_user, params)
      end

      ALLOWED_USER_INCLUDES = {
        identities: {
          methods: [:identity_type, :screen_name],
          only: [:value, :is_verified]
        },
        organization: {}
      }.freeze

      def user_serialization_options
        @user_serialization_options ||= begin
          {}.tap do |options|
            options[:methods] = [:locale]

            if current_user.is_agent?
              options[:methods] << :current_tags
            else
              options[:except] = [:details, :notes]
            end

            options[:include] = { groups: {} }

            if params[:include].present?
              custom_includes = ALLOWED_USER_INCLUDES.slice(*params[:include].map(&:to_sym))
              options[:include].merge!(custom_includes)
            end
          end
        end
      end

      def user_finder_includes
        user_serialization_options[:include].keys
      end

      def merge_users_with_auth!(winner, loser, winner_password = "")
        Prop.throttle!(:password_attempt, winner.id) if winner
        if winner && !winner_password.blank? && winner.authenticated?(winner_password)
          Prop.reset(:password_attempt, winner.id)
          merge_users!(winner, loser)
        else
          raise UserAuthenticationError
        end
      end

      def merge_users!(winner, loser)
        @merge_status = ::Users::Merge.validate_and_merge_with_tickets_now(winner, loser)
        if @merge_status == :merge_complete
          return true
        else
          raise UserMergeError
        end
      end
    end
  end
end
