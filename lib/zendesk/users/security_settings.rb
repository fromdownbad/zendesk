module Zendesk
  module Users
    class SecuritySettings
      attr_reader :role, :user, :account

      delegate :role_settings, to: :account

      def initialize(user, account)
        @user    = user
        @role    = extract_role(user)
        @account = account
      end

      def security_policy
        security_policy_for_role.tap { |p| p.user = user }
      end

      def login_allowed?(service)
        role_settings.login_allowed_for_role?(service, role)
      end

      def only_login_service_allowed?(service)
        role_settings.only_login_service_allowed_for_role?(service, role)
      end

      private

      def extract_role(user)
        user.is_agent? ? :agent : :end_user
      end

      def security_policy_id
        role_settings.security_policy_id_for_role(role)
      end

      def security_policy_for_role
        Zendesk::SecurityPolicy.find(security_policy_id, account)
      end
    end
  end
end
