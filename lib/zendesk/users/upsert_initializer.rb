module Zendesk
  module Users
    class UpsertInitializer
      class UpsertError < StandardError; end
      class RequesterIdNotFound < StandardError; end

      attr_reader :current_account, :current_user, :params, :error, :is_upsert_operation
      alias :requires_save? :is_upsert_operation

      def initialize(current_account, current_user, params)
        @params = params
        @current_account = current_account
        @current_user = current_user
        @is_upsert_operation = false
      end

      def apply
        check_external_id_validity

        handle_existing_user_name_update unless user.new_record?

        if user.new_record? || update_user?
          @is_upsert_operation = true
          Zendesk::Users::Initializer.new(current_account, current_user, params).apply(user)
        end

        add_datadog_metrics

        user
      end

      def user
        @user ||= find_user_by_params || current_account.users.new
      end

      private

      def user_id
        params[:user][:id]
      end

      def external_id
        params[:user][:external_id]
      end

      def user_identities
        params[:user][:identities]
      end

      def handle_existing_user_name_update
        return unless current_user_name = params[:user].delete(:overwrite_existing_name).presence
        params[:user].delete(:name) unless current_user_name == user.name
      end

      def check_external_id_validity
        if !external_id.nil? && external_id.blank?
          raise UpsertError, @error = { external_id: [invalid_param_value_error('external_id')] }
        end
      end

      def user_from_external_id
        current_account.users.find_by_external_id(external_id.to_s)
      end

      def find_user_by_params
        user = current_account.users.find(user_id) if user_id.present?

        user_id_from_identity = process_identities

        if user
          validate_external_id(user) if external_id.present?
          validate_identities(user, user_id_from_identity) if user_id_from_identity.present?
        end

        if external_id.present? && user.nil?
          user = user_from_external_id
          validate_identities(user, user_id_from_identity) if user && user_id_from_identity.present?
        end

        if user_id_from_identity.present? && user.nil?
          user = current_account.users.find(user_id_from_identity)
          validate_external_id(user) if user && external_id.present?
        end

        user
      end

      def process_identities
        return unless user_identities.present?

        identities = create_conditions_and_fetch_identities
        if identities.present?
          result = identities.group_by { |identity| identity[:user_id] }
          raise UpsertError, @error = { identities: [multiple_existing_users_error] } if result.count > 1
          result.keys.first
        end
      end

      def create_conditions_and_fetch_identities
        @conditions ||= create_conditions_for_type_and_value
        @identities_by_type_value ||= current_account.user_identities.where(@conditions.join(" OR "))
      end

      def create_conditions_for_type_and_value
        conditions = []
        user_identities.each do |identity|
          check_identity_params_validity(identity)

          type = UserIdentity::IDENTITY_TYPE_MAP[identity[:type]]

          raise UpsertError, @error = { type: [unknown_identity_type_error(identity[:type])] } unless type.present?

          conditions << UserIdentity.sanitize_sql_for_type_value(type, identity[:value])
        end
        conditions
      end

      def check_identity_params_validity(identity)
        raise UpsertError, @error = { type: [missing_identity_value_error('type')] } unless identity[:type].present?

        raise UpsertError, @error = { type: [missing_identity_value_error('value')] } if identity[:value].nil?

        if !identity[:value].nil? && identity[:value].blank?
          raise UpsertError, @error = { identity: [invalid_param_value_error('identity')] }
        end
      end

      def validate_external_id(user)
        user_external_id = user_from_external_id

        if user_external_id && user.id != user_external_id.id
          raise UpsertError, @error = { external_id: [multiple_existing_users_error] }
        end

        if user.external_id.present? && user.external_id != external_id
          raise UpsertError, @error = { type: [external_id_present_error] }
        end
      end

      def validate_identities(user, identity_user_id)
        raise UpsertError, @error = { identity: [multiple_existing_users_error] } unless identity_user_id == user.id
      end

      def update_user?
        update_name? || update_external_id? || update_identities?
      end

      def update_name?
        return true if !params[:user][:name].nil? && params[:user][:name].blank?
        return false unless params[:user][:name].present?
        params[:user][:name] != user.name
      end

      def update_external_id?
        return false unless external_id.present?
        !user.external_id.present?
      end

      def update_identities?
        return false unless user_identities.present?

        identities = create_conditions_and_fetch_identities
        identities.size != user_identities.size
      end

      def add_datadog_metrics
        current_span = rack_request_span || active_span
        current_span.set_tag('is_upsert_operation', @is_upsert_operation) if current_span
        statsd_client.increment("users.upsert_initializer.apply", tags: ["is_upsert_operation:#{@is_upsert_operation}"])
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['classic'])
      end

      def active_span
        @active_span ||= Datadog.tracer.active_span
      end

      def rack_request_span
        parent_span = active_span&.parent
        parent_span if parent_span&.name == 'rack.request'
      end

      def multiple_existing_users_error
        { description: 'The provided identifiers resolve to multiple existing users', error: 'MultipleExistingUsers' }
      end

      def external_id_present_error
        { description: 'A different external ID is present on the user', error: 'ExternalIdPresent' }
      end

      def unknown_identity_type_error(type)
        { description: "Type: #{type} is not a valid identity type", error: 'UnknownIdentityType' }
      end

      def missing_identity_value_error(missing_param)
        { description: "Identity #{missing_param} is missing", error: 'MissingValueError' }
      end

      def invalid_param_value_error(param)
        { description: "Invalid value is given for #{param}", error: 'InvalidValueError' }
      end
    end
  end
end
