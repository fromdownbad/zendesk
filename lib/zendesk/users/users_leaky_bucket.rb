module Zendesk
  module Users
    module UsersLeakyBucket
      def self.configure_rate_limit(account_api_rate_limit, default_interval = 6)
        user_leaky_burst_rate = (account_api_rate_limit / 2).to_i + 1
        user_leaky_threshold = (account_api_rate_limit / 10).to_i
        user_leaky_interval = default_interval
        Prop.configure(:user_write_endpoints, strategy: :leaky_bucket, burst_rate: user_leaky_burst_rate, threshold: user_leaky_threshold, interval: user_leaky_interval.seconds)
      end
    end
  end
end
