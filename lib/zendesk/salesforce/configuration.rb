module Salesforce
  class Configuration

    MISSING_OBJECT = "missing_object"
    MISSING_FIELDS = "missing_fields"
    MISSING_SALESFORCE_MAPPED_FIELD = "missing_salesforce_mapped_field"

    def initialize(account)
      @account = account
    end

    def self.default
      {
        "Contact" => {
                       "fields" => [ "Name", "Email", "Account::Name" ],
                       "labels" => { "Name" => "Full Name",
                                     "Email" => "Email",
                                     "Account::Name" => "Account Name" },
                       "relationships" => { "Account" => { "object" => "Account", "type" => "child" } },
                       "mapping"  => [ "Email", "ticket.requester.email" ],
                       "sf_field_type" => "email"
                     }
      }
    end

    def object(object_name)
      configuration[object_name] || {}
    end

    def fields(object_name)
      object(object_name)["fields"] || []
    end

    # Returns sorted objects using position
    def objects
      configuration.keys.sort_by {|k| configuration[k]["position"] || 99}
    end

    def organized_fields(object_name, include_missing = false)
      missing_fields = errors(object_name)[MISSING_FIELDS] || []
      primary = []
      related = {}

      fields(object_name).each do |field|
        next if !include_missing && missing_fields.include?(field)

        if field.include?("::")
          class_name, field_name = field.split("::")
          related[class_name] ||= []
          related[class_name] << field_name
        else
          primary << field
        end
      end

      [primary, related]
    end

    def mapping(object_name)
      object(object_name)["mapping"] || []
    end

    def errors(object_name)
      object(object_name)["errors"] || {}
    end

    def filters(object_name)
      object(object_name)["filters"] || {}
    end

    def relationship_filters(object_name, relationship_name)
      filters(object_name)[relationship_name] || {}
    end

    def relationship_filter_fields(object_name, relationship_name)
      relationship_filters(object_name, relationship_name)["fields"] || []
    end

    def relationship_limit(object_name, relationship_name)
      relationship_filters(object_name, relationship_name)["limit"] || 5
    end

    def labels(object_name)
      object(object_name)["labels"] || {}
    end

    def relationships(object_name)
      object(object_name)["relationships"] || {}
    end

    def salesforce_mapped_field(object_name)
      mapping(object_name)[0] || ""
    end

    def zendesk_mapped_field(object_name)
      mapping(object_name)[1] || ""
    end

    def salesforce_mapped_field_type(object_name)
      object(object_name)["sf_field_type"] || "string"
    end

    def position(object_name)
      object(object_name)["position"] || 99
    end

    def object_label(object_name)
      object(object_name)["object_label"] || object_name
    end

    def add(object_name, fields, labels, relationships, salesforce_field, zendesk_field, salesforce_field_type, object_label)
      configuration[object_name] = { "fields"        => fields,
                                     "labels"        => labels,
                                     "relationships" => relationships,
                                     "mapping"       => [salesforce_field, zendesk_field],
                                     "sf_field_type" => salesforce_field_type,
                                     "position"      => position(object_name),
                                     "filters"       => filters(object_name),
                                     "object_label"  => object_label }

      save
    end

    def add_filters(object_name, relationship_name, limit, fields)
      filters = filters(object_name)
      filters[relationship_name] = { "limit" => limit, "fields" => fields }
      object(object_name)["filters"] = filters
      save
    end

    def add_errors(object_name, errors)
      obj_config = object(object_name)
      obj_config["errors"] = errors
      configuration[object_name] = obj_config
      save
    end

    def remove(object_name)
      configuration.delete(object_name)
      save
    end

    def sort(object_names)
      object_names.each_with_index do |object_name, position|
        object(object_name)["position"] = position
      end
      save
    end

    def friendly_name(name)
      name.gsub(/__r|__c|__pr/, "").gsub(/__|_/, " ").titleize
    end

    private

    def configuration
      @configuration ||= begin
        json = JSON.parse(@account.texts.salesforce_configuration || "")
        json ? json : Configuration.default
      end
    end

    def save
      @account.texts.salesforce_configuration = configuration.to_json
      @account.save
    end

  end
end
