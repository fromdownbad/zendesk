require_dependency 'zendesk/salesforce/object_response'

module Salesforce
  module ObjectManagement

    SALESFORCE_API_VERSION = "48.0"
    APP_NAMESPACE = 'Zendesk'.freeze

    CHILD_RELATIONSHIP  = 'child'.freeze
    PARENT_RELATIONSHIP = 'parent'.freeze
    ACCOUNT_SF_OBJECT = 'Account'.freeze

    IGNORED_ERROR_MESSAGES = [
      'invalid ID field',
      'API is disabled for this User',
      'implementation restriction on ActivityHistories',
      'TotalRequests Limit exceeded',
      'value of filter criterion for field',
      'reference your WSDL',
      'Server too busy',
      'implementation restriction on OpenActivities',
      'Your query request was running for too long'
    ].freeze

    OPTIONS_TO_SHOW_PARENT = [
      'show_parent_only',
      'show_parent_and_ultimate_parent'
    ].freeze

    def fetch_parent_accounts(account_records)
      # filter out accounts with parentId = null
      account_ids = []
      account_records.each do |acc_record|
        account_ids.push acc_record["Id"] if acc_record['ParentId']
      end
      return {} if account_ids.empty?

      # account hierarchy callout
      account_hierarchy = get_account_hierarchy(account_ids)
      return {} if account_hierarchy.empty?

      # construct parent map
      parent_map = {}
      account_ids.each do |account_id|
        parent_records = account_hierarchy[account_id]
        parent_map[account_id] = Salesforce::ObjectResponse.new(ACCOUNT_SF_OBJECT,
          {"records" => parent_records},
          base_url: salesforce_instance_url,
          configuration: configuration).objects
      end

      parent_map
    end

    def get_account_hierarchy(account_ids)
      show_parent_settings = account.salesforce_integration.show_parent
      fields, related = configuration.organized_fields(ACCOUNT_SF_OBJECT)
      fields += related_soql_expr(ACCOUNT_SF_OBJECT, related)

      response = http_get(account_hierarchy_endpoint, ids: account_ids.join(","), fields: fields.join(","), type: show_parent_settings)
      if response.status == 200
        JSON.parse(response.body)
      else
        log_error("SALESFORCE ERROR, failed to fetch account hierarchy.")
        {}
      end
    end

    def log_error(err_msg)
      Rails.logger.error(err_msg.slice(0, 10_000))
    end

    def account_hierarchy_endpoint
      namespace = (Rails.env.production? || Rails.env.staging?) ? "/#{APP_NAMESPACE}/" : '/'
      "/services/apexrest#{namespace}app/account/hierarchy"
    end

    def fetch_ticket_info(params)
      sf_objects = configuration.objects
      query_filters = fetch_query_filters(params)

      sf_records = []

      sf_objects.each do |sf_object|
        if config_ok?(sf_object)
          sf_records.concat salesforce_query(sf_object, query_filters)
        end
      end

      { records: sf_records }
    end

    def config_ok?(sf_object)
      config_error = configuration.errors(sf_object)
      config_has_error = config_error[Salesforce::Configuration::MISSING_SALESFORCE_MAPPED_FIELD]
      if config_has_error
        sf_object = config_error[Salesforce::Configuration::MISSING_OBJECT]
        sf_fields = config_error[Salesforce::Configuration::MISSING_FIELDS]
        log_error("SALESFORCE ERROR, Salesforce Mapped Fields Missing #{sf_object} =>  #{sf_fields}.")
      end

      !config_has_error
    end

    def fetch_query_filters(params)
      if params.is_a? Ticket
        ticket_values(params)
      else
        params
      end
    end

    def salesforce_query(sf_object, filter_values)
      # construct SOQL
      soql = soql_expr(sf_object, filter_values)
      return [] if soql.blank?

      # query callout
      begin
        response = http_get("/services/data/v#{SALESFORCE_API_VERSION}/query", q: soql)

        # query for parent records for Account
        parent_records = {}
        if sf_object == ACCOUNT_SF_OBJECT && show_parent? && response.status == 200
          account_records = JSON.parse(response.body)["records"]
          parent_records = fetch_parent_accounts(account_records)
        end

        records = Salesforce::ObjectResponse.new(sf_object,
          response,
          base_url: salesforce_instance_url,
          configuration: configuration,
          parents: parent_records).objects
      rescue => e
        handle_query_error(e)
        []
      end
    end

    def show_parent?
      OPTIONS_TO_SHOW_PARENT.include?(account.salesforce_integration.show_parent)
    end

    def handle_query_error(e)
      if e.message.include?("No such column")
        SalesforceErrorCheckingJob.enqueue(account.id)
      elsif IGNORED_ERROR_MESSAGES .any? { |m| e.message.include?(m) }
        log_error("SALESFORCE ERROR, returned ignored message: #{e.message}, returning empty set.")
      else
        raise e
      end
    end

    def soql_expr(object_name, values)
      condition = soql_condition(object_name, values)
      return nil if condition.blank?

      fields, related = configuration.organized_fields(object_name)
      fields += related_soql_expr(object_name, related)
      # include parentId in the query if SHOW_PARENT is true
      if object_name == ACCOUNT_SF_OBJECT && show_parent?
        fields.push 'ParentId'
      end
      fields.push 'Id'
      fields.uniq!

      "SELECT #{fields.join(", ")} FROM #{object_name} WHERE #{condition}"
    end

    def related_soql_expr(object_name, related)
      relationships = configuration.relationships(object_name)

      expressions = []
      related.each do |relationship_name, fields|
        relationship = relationships[relationship_name]
        if relationship['type'] == CHILD_RELATIONSHIP
          fields.each do |field|
            expressions << "#{relationship_name}.#{field}"
          end
        else
          conditions = configuration.relationship_filter_fields(object_name, relationship_name).map do |filter|
            field_condition(filter["field"], filter["type"], filter["operator"], filter["value"])
          end.compact

          condition = conditions.any? ? " WHERE #{conditions.join(' AND ')}" : ""

          expressions << "(SELECT #{fields.join(", ")} FROM #{relationship_name}#{condition} ORDER BY CreatedDate DESC)"
        end
      end

      expressions
    end

    def list_objects
      response = http_get("/services/data/v#{SALESFORCE_API_VERSION}/sobjects")
      if response.status == 200
        JSON.parse(response.body)["sobjects"]
      else
        []
      end
    end

    def list_fields(object_name)
      describe_sobject(object_name)["fields"].collect { |f| f["name"] }.compact
    end

    def describe_fields(object_name)
      describe_sobject(object_name)["fields"].sort_by { |hash| hash["label"] }
    end

    def list_related_fields(object_name)
      fields = []

      # child-to-parent relationships
      describe_sobject(object_name)["fields"].each do |f|
        if f["relationshipName"].present? && f["referenceTo"].present?
          referenceTo = f["referenceTo"].is_a?(Array) ? f["referenceTo"].first : f["referenceTo"]
          fields << { relationship: f["relationshipName"],
            object: referenceTo,
            type: CHILD_RELATIONSHIP }
        end
      end

      # parent-to-child relationships
      describe_sobject(object_name)["childRelationships"].each do |f|
        if f["relationshipName"].present? && f["childSObject"].present?
          fields << { relationship: f["relationshipName"],
            object: f["childSObject"],
            type: PARENT_RELATIONSHIP }
        end
      end

      fields.sort_by { |hash| hash[:relationship] }
    end

    def fields(object_name, selected_fields, relationship_name)
      result = []

      describe_fields(object_name).each do |field|
        key = relationship_name ? "#{relationship_name}::#{field['name']}" : field['name']
        result << { title: field['label'],
          key: key,
          select: selected_fields.include?(key),
          type: field['type'],
          scale: field['scale'],
          picklistValues: field['picklistValues'],
          length: field['length'],
          updateable: field['updateable'] }
      end

      result
    end

    def related_fields(object_name, selected_fields)
      result = []

      relationships = configuration.relationships(object_name).keys

      list_related_fields(object_name).each do |hash|
        related_object    = hash[:object]
        relationship_name = hash[:relationship]
        type              = hash[:type]

        related_fields = []

        if relationships.include?(relationship_name)
          related_fields = fields(related_object, selected_fields, relationship_name)
        end

        result << { title: configuration.friendly_name(relationship_name),
          object: related_object,
          relationship: relationship_name,
          type: type,
          isFolder: true,
          fields: related_fields }
      end

      result.sort_by { |k| relationships.index(k[:relationship]) || 99 }
    end

    def check_for_errors
      error_found = false

      configuration.objects.each do |object_name|
        primary, related = configuration.organized_fields(object_name, true)
        relationships = configuration.relationships(object_name)
        errors = {}
        if primary.present?
          remote_fields = list_fields(object_name) rescue nil

          if remote_fields.nil?
            # The configured object is not in Salesforce anymore
            errors[Salesforce::Configuration::MISSING_OBJECT] = true
            error_found = true
          else
            errors[Salesforce::Configuration::MISSING_OBJECT] = false
            # The configured mapped field is not in Salesforce anymore
            errors[Salesforce::Configuration::MISSING_SALESFORCE_MAPPED_FIELD] = !remote_fields.include?(configuration.salesforce_mapped_field(object_name))
            # Add missing objects that are configured but are not in Salesforce
            missing = primary - remote_fields

            # Check related fields
            related.each do |relationship_name, related_fields|
              relationship = relationships[relationship_name]
              # Get related object fields
              remote_related_fields = list_fields(relationship['object']) rescue nil

              if remote_related_fields.nil?
                # Related object is not in Salesforce. Mark all the fields as missing.
                missing += related_fields.map { |related_field| "#{relationship_name}::#{related_field}" }
              else
                # Get missing fields and add relationship_name before adding as missing
                missing += (related_fields - remote_related_fields).map { |related_field| "#{relationship_name}::#{related_field}" }
              end
            end

            errors[Salesforce::Configuration::MISSING_FIELDS] = missing
            error_found = error_found || errors[Salesforce::Configuration::MISSING_SALESFORCE_MAPPED_FIELD] || missing.present?
          end
        end

        # Save errors in configuration object
        configuration.add_errors(object_name, errors)
      end

      error_found
    end

    def http_get(path, parameters = {})
      connection_handler.http_get(path, parameters)
    end

    def http_post(path, parameters = {})
      connection_handler.http_post(path, parameters)
    end

    def salesforce_instance_url
      uri = URI.parse(get_salesforce_session[:server_url])
      "#{uri.scheme}://#{uri.host}"
    end

    def describe_sobject(object_name)
      @__sobjects ||= {}
      @__sobjects[object_name] ||= begin
        result = http_get("/services/data/v#{SALESFORCE_API_VERSION}/sobjects/#{object_name}/describe")
        unless result.success?
          raise Faraday::Error::ClientError.new("Salesforce Request Failed (Account: #{account.subdomain}): SObject Describe request failed for object #{object_name}")
        end

        if result.body.nil? || result.body.empty?
          raise Faraday::Error::ClientError.new("Salesforce Request Failed (Account: #{account.subdomain}): SObject Describe request gave an empty response for object #{object_name}")
        end

        JSON.parse(result.body)
      end
    end

    def ticket_values(ticket)
      values = {}
      configuration.objects.each do |object_name|
        placeholder = configuration.zendesk_mapped_field(object_name)
        value = ::Zendesk::Liquid::TicketContext.render("{{#{placeholder}}}", ticket, nil, true)
        values[placeholder] = value
      end
      values["ticket.organization.external_id"] = ::Zendesk::Liquid::TicketContext.render("{{ticket.organization.external_id}}", ticket, nil, true)
      values["ticket.requester.external_id"] = ::Zendesk::Liquid::TicketContext.render("{{ticket.requester.external_id}}", ticket, nil, true)

      values
    end

    def is_valid_salesforce_id?(salesforce_id)
      /^([a-zA-Z0-9]{15}|[a-zA-Z0-9]{18})$/.match(salesforce_id)
    end

    def soql_condition(object_name, values)
      conditions = []
      salesforce_field = configuration.salesforce_mapped_field(object_name)
      field_type = configuration.salesforce_mapped_field_type(object_name)
      value = values[configuration.zendesk_mapped_field(object_name)]
      field_mapping = field_condition(salesforce_field, field_type, "=", value)

      if field_mapping.present?
        conditions << field_condition(salesforce_field, field_type, "=", value)
      end

      if object_name == "Account" && is_valid_salesforce_id?(values["ticket.organization.external_id"])
        conditions << field_condition("Id", "string", "=", values["ticket.organization.external_id"])
      elsif (object_name == "Lead" || object_name == "Contact") && is_valid_salesforce_id?(values["ticket.requester.external_id"])
        conditions << field_condition("Id", "string", "=", values["ticket.requester.external_id"])
      end
      conditions.join(" OR ").presence
    end

    def field_condition(field, type, operator, value)
      return nil unless field.present? && operator.present? && value.present?

      case type
      when "int", "double", "percent"
        "#{field} #{operator} #{value.to_i}"
      when "boolean"
        "#{field} #{operator} #{condition_to_boolean(value)}" rescue nil
      when "date"
        "#{field} #{operator} #{Date.parse(value).iso8601}" rescue nil
      when "datetime"
        "#{field} #{operator} #{Time.parse(value).utc.iso8601}" rescue nil
      else
        # Sanitize condition to prevent soql injection
        # (Ticket is an arbitrary model, just need something real)
        Ticket.send(:sanitize_sql, ["#{field} #{operator} ?", value])
      end
    end

    def condition_to_boolean(value)
      if value == true || value =~ (/(true|t|yes|y|1)$/i)
        true
      elsif value == false || value.blank? || value =~ (/(false|f|no|n|0)$/i)
        false
      else
        raise Exception.new("The value for the condition is not a boolean")
      end
    end

  end
end
