module Zendesk::Salesforce
  module ApiControllerSupport
    private

    def handle_kragle_exception
      yield
    rescue Salesforce::NextGeneration::RefreshTokenError => e
      render json: { errors: e.try(:message) }, status: :unauthorized
    rescue Kragle::ResponseError => e
      render json: { errors: e.try(:response).try(:body)['errors'] }, status: e.try(:response).try(:status)
    rescue Faraday::ClientError => e
      render json: { errors: e.try(:response)[:body] }, status: e.try(:response)[:status]
    end

    def require_salesforce_configuration!
      raise_missing_configuration_failure unless current_account.salesforce_configuration_enabled?
    end

    def raise_missing_configuration_failure
      render_failure(
        message: I18n.t("txt.admin.views.settings.extensions._salesforce_init.zendesk_for_salesforce",
          zendesk_for_salesforce_link: "Zendesk for Salesforce app"),
        status: :unauthorized,
        title: 'Unauthorized'
      )
    end
  end
end
