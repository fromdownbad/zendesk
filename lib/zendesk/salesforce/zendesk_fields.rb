module Zendesk::Salesforce
  module ZendeskFields
    OPTIONS = %w[
      ticket.requester.name ticket.requester.last_name ticket.requester.email ticket.requester.phone
      ticket.requester.details ticket.requester.notes ticket.organization.name ticket.organization.details
      ticket.organization.notes ticket.assignee.name ticket.assignee.last_name ticket.assignee.email
      ticket.title ticket.id ticket.group.name ticket.requester.external_id ticket.organization.external_id
    ].freeze

    SUPPORTED_DATA_TYPES = %w[FieldText FieldTagger FieldDecimal FieldRegexp].freeze

    private

    def mappable_zendesk_fields
      @options ||= field_options
    end

    def field_options
      options = OPTIONS.map { |ph| { label: I18n.t("placeholder.#{ph}"), key: ph } }

      if current_account.has_multibrand?
        options << { label: I18n.t("placeholder.ticket.brand.name"), key: "ticket.brand.name" }
      end

      # Custom ticket fields
      ticket_fields = current_account.ticket_fields.where(is_active: true, type: SUPPORTED_DATA_TYPES).to_a
      options += ticket_fields.map do |ticket_field|
        { label: ticket_field.title, key: "ticket.ticket_field_#{ticket_field.id}" }
      end

      options
    end

    def mapped_zendesk_field(configured_zendesk_field)
      mappable_zendesk_fields.find { |zf| zf[:key] == configured_zendesk_field }[:label] rescue nil
    end
  end
end
