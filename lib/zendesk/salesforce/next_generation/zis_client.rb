require_dependency 'zendesk/salesforce/next_generation/zis_connection'
require_dependency 'zendesk/salesforce/next_generation/zis_config'

module Salesforce::NextGeneration
  class ZisClient
    def initialize(account)
      @account = account
    end

    def access_token
      zis_connection.access_token
    end

    def refresh_access_token
      zis_connection.refresh_access_token
    end

    def salesforce_organization_uuid
      zis_config.fetch_salesforce_organization_uuid
    end

    def salesforce_organization_url
      zis_config.fetch_salesforce_organization_url
    end

    def latency
      zis_config.fetch_latency
    end

    def data_cache_enabled?
      zis_config.has_data_cache_enabled?
    end

    def enabled?
      zis_config.has_enabled_config?
    end

    def configured?
      zis_config.has_connection_uuid?
    end

    private

    attr_reader :account

    def zis_config
      @zis_config ||= Salesforce::NextGeneration::ZisConfig.new(account)
    end

    def zis_connection
      @zis_connection ||= Salesforce::NextGeneration::ZisConnection.new(account, salesforce_organization_uuid)
    end
  end
end
