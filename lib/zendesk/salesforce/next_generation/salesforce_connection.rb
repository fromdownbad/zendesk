module Salesforce::NextGeneration
  class SalesforceConnection

    def initialize(zis_client)
      @zis_client = zis_client
    end

    def get(path)
      execute_get(zis_client.access_token, path)
    rescue Faraday::ClientError => error
      handle_error(error) { execute_get(zis_client.refresh_access_token, path) }
    end

    def post(path, parameters)
      execute_post(zis_client.access_token, path, parameters)
    rescue Faraday::ClientError => error
      handle_error(error) { execute_post(zis_client.refresh_access_token, path, parameters) }
    end

    private

    attr_reader :zis_client

    def client(token)
      Faraday.new(url: zis_client.salesforce_organization_url) do |faraday|
        faraday.request  :url_encoded             # form-encode POST params
        faraday.response :logger                  # log requests and responses to $stdout
        faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        faraday.headers['Authorization'] = "Bearer #{token}"
        faraday.headers['Content-Type']  = 'application/json'
        faraday.use Faraday::Response::RaiseError
      end
    end

    def execute_get(token, path)
      client(token).get(path)
    end

    def execute_post(token, path, parameters)
      client(token).post(path, parameters) do |req|
        req.body = parameters[:body].to_s
        req.headers['Content-Type'] = parameters[:headers]['Content-Type']
      end
    end

    def handle_error(error)
      unless error.try(:response)[:status] == 401
        raise error
      end
      yield
    end
  end
end
