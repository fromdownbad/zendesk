module Salesforce::NextGeneration::ZisConfigs
  module General

    def fetch_salesforce_organization_id
      salesforce_organization['id']
    end

    def fetch_salesforce_organization_uuid
      salesforce_organization['connection_uuid']
    end

    def fetch_salesforce_organization_url
      salesforce_organization['url']
    end

    def has_enabled_config?
      !salesforce_organization.blank?
    end

    def has_connection_uuid?
      fetch_salesforce_organization_uuid.present?
    end

    private

    def salesforce_organization
      @salesforce_organization ||= begin
        config_list = find_configs_by_scope('/general/')
        config = find_enabled_config(config_list)
        config.blank? ? {} : config['config']['salesforce']['organization']
      end
    rescue NoMethodError => e
      Rails.logger.error "Unexpected body message: #{e}"
      {}
    end
  end
end
