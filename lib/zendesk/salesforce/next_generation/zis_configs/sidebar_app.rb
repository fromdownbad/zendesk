module Salesforce::NextGeneration::ZisConfigs
  module SidebarApp

    def fetch_latency
      siderbar_app['latency']
    end

    def has_data_cache_enabled?
      !siderbar_app.blank? && siderbar_app['enabled']
    end

    private

    def siderbar_app
      @siderbar_app ||= begin
        config_list = find_configs_by_scope("salesforce-organization-id:#{salesforce_organization_id}/sidebar_app/")
        config = find_enabled_config(config_list)
        config.blank? ? {} : config['config']
      end
    end
  end
end
