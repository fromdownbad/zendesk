module Salesforce::NextGeneration
  module CrmData

    SALESFORCE = 'Salesforce'.freeze

    def integration_name
      SALESFORCE
    end

    def parse_salesforce_xml(xml, base_url)
      Zendesk::Crm::Integration.parse_user_info_xml(xml, salesforce_ui_url(base_url))
    end

    def crm_data(user, create)
      if create && user.salesforce_data.nil?
        user.create_salesforce_data
      else
        user.salesforce_data
      end
    end

    def crm_ticket_data(ticket, create)
      if create && ticket.salesforce_ticket_data.nil?
        ticket.create_salesforce_ticket_data(ticket: ticket)
      else
        ticket.salesforce_ticket_data
      end
    end

    def configuration
      @__configuration ||= Salesforce::Configuration.new(account)
    end

    def destroy_other_integrations
      account.sugar_crm_integration.try(:destroy)
      account.ms_dynamics_integration.try(:destroy)
    end

    private

    def salesforce_ui_url(base_url)
      # horrible hacks for ZD#213355 & ZD#345845
      base_url.gsub!(/^eu0/, 'emea')
      base_url.gsub!(/^na0-api/, 'ssl') if account.has_use_salesforce_ssl_pod?
      "https://#{base_url.gsub('-api', '')}"
    end

    def fetch_user_info(user, driver)
      if account.has_salesforce_integration_lookup?
        case lookup_type
        when 'UserTags' then
          return empty_user_info if user.tags.empty?
          argument = Salesforce::Integration::GetUserFieldsByList.new(user.tags)
        when 'OrganizationName' then
          return empty_user_info unless user.organization.present?
          argument = Salesforce::Integration::GetUserFieldsByList.new([user.organization.name])
        else
          return empty_user_info unless user.email.present?
          argument = Salesforce::Integration::GetUserFieldsByList.new([user.email])
        end
        driver.getUserFieldsByList(argument).result
      else
        return empty_user_info unless user.email.present?
        argument = Salesforce::Integration::GetUserFields.new(user.email)
        driver.getUserFields(argument).result
      end
    end

    def driver
      driver = Salesforce::Integration::Driver.get_driver(get_salesforce_session)
      driver.options['protocol.http.receive_timeout'] = 20
      driver.options['protocol.http.connect_timeout'] = 5
      driver.options['protocol.http.send_timeout']    = 5
      driver
    end

    def empty_user_info
      '<records></records>'
    end
  end
end
