require_dependency 'zendesk/salesforce/next_generation/zis_base'
require_dependency 'zendesk/salesforce/next_generation/zis_configs/general'
require_dependency 'zendesk/salesforce/next_generation/zis_configs/sidebar_app'

module Salesforce::NextGeneration
  class ZisConfig < ZisBase
    include Salesforce::NextGeneration::ZisConfigs::General
    include Salesforce::NextGeneration::ZisConfigs::SidebarApp

    ZIS_CONFIG = 'zis_config'.freeze

    def service_name
      ZIS_CONFIG
    end

    def salesforce_organization_id
      fetch_salesforce_organization_id
    end

    def find_configs_by_scope(scope)
      all_configs.select { |config| config['scope'].include?("#{scope}") }
    end

    def find_enabled_config(configs)
      configs.find { |config| config['config']['enabled'] == true }
    end

    private

    def all_configs
      @all_configs ||= begin
        response = service.get("#{base_url}/integrations/salesforce/configs", { filter: { scope: 'salesforce-organization-id:*/' } } )
        response.status == 200 ? response.body['configs'] : []
      end
    rescue Kragle::ResourceNotFound => e
      Rails.logger.warn "No ZIS configurations found: #{e}"
      []
    end
  end
end
