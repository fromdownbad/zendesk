module Salesforce::NextGeneration
  class ZisBase

    def initialize(account)
      @account = account
    end

    def service_name
      raise 'should implement service name in the subclass'
    end

    private

    attr_reader :account

    def service
      @service ||= Kragle.new(service_name)
    end

    def base_url
      "https://#{account.default_host}/api/services/zis"
    end
  end
end
