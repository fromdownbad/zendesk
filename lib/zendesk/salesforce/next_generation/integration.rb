require_dependency 'zendesk/crm/integration'
require_dependency 'zendesk/salesforce'
require_dependency 'zendesk/salesforce/integration'
require_dependency 'zendesk/salesforce/next_generation/zis_client'
require_dependency 'zendesk/salesforce/next_generation/salesforce_connection'
require_dependency 'zendesk/salesforce/next_generation/crm_data'
require_dependency 'zendesk/salesforce/object_management'
require_dependency 'zendesk/salesforce/configuration'

module Salesforce::NextGeneration
  class Integration
    include Salesforce::ObjectManagement
    include Salesforce::NextGeneration::CrmData

    SHOW_PARENT = 'none'.freeze

    def initialize(account)
      @account = account
    end

    def token
      access_token
    end

    def access_token
      @access_token ||= zis_client.access_token
    end

    def instance_url
      zis_client.salesforce_organization_url
    end

    def app_latency
      zis_client.latency
    end

    def show_parent
      SHOW_PARENT
    end

    def get_salesforce_session
      { :server_url => instance_url, :session_id => token }
    end

    def data_cache_enabled?
      zis_client.data_cache_enabled?
    end

    def configured?
      zis_client.configured?
    end

    def configured_and_enabled?
      configured? && zis_client.enabled?
    end

    def http_get(path, parameters = {})
      salesforce_connection.get(retrieve_url(path, parameters))
    end

    def http_post(path, parameters = {})
      salesforce_connection.post(path, parameters)
    end

    def fetch_info_for(user)
      begin
        xml = fetch_user_info(user, driver)
      rescue StandardError => e
        raise e
      end
      base_url = URI.parse(get_salesforce_session[:server_url]).host
      parse_salesforce_xml(xml, base_url)
    end

    private

    attr_reader :account

    def zis_client
      @zis_client ||= Salesforce::NextGeneration::ZisClient.new(account)
    end

    def salesforce_connection
      @salesforce_connection ||= Salesforce::NextGeneration::SalesforceConnection.new(zis_client)
    end

    def retrieve_url(path, parameters)
      parameters.present? ? "#{path}?#{parameters.to_query}" : path
    end
  end
end
