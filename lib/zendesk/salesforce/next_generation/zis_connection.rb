require_dependency 'zendesk/salesforce/next_generation/zis_base'

module Salesforce::NextGeneration
  class ZisConnection < ZisBase

    attr_reader :connection_uuid

    ZIS_CONNECTION = 'zis_connection'.freeze

    def initialize(account, connection_uuid)
      super(account)
      @connection_uuid = connection_uuid
    end

    def service_name
      ZIS_CONNECTION
    end

    def access_token
      fetch_access_token.body['AccessToken']
    end

    def refresh_access_token
      fetch_refresh_token.body['AccessToken']
    rescue Kragle::UnprocessableEntity => e
      Rails.logger.error("Salesforce refresh token error (Account: #{account.subdomain}): #{e.response.body}")
      raise RefreshTokenError
    end

    private

    def fetch_access_token
      service.get("#{base_url}/connections/salesforce", { uuid: connection_uuid })
    end

    def fetch_refresh_token
      service.get("#{base_url}/connections/refresh/salesforce", { uuid: connection_uuid })
    end
  end

  class RefreshTokenError < Kragle::UnprocessableEntity
    def message
      'RefreshTokenError Exception'
    end
  end
end
