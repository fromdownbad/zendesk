module Salesforce
  class ObjectResponse
    MAX_FIELD_SIZE = 1000

    def initialize(object_name, response, base_url:, configuration:, parents:nil)
      @object_name   = object_name
      @base_url      = base_url
      @json          = json_response?(response)
      @configuration = configuration
      @mapped_field  = configuration.salesforce_mapped_field(object_name)
      @labels        = configuration.labels(object_name)
      @object_label  = configuration.object_label(object_name)
      @primary, @related = configuration.organized_fields(object_name)
      @parents = parents
      @objects = parse_collection(@object_name, @json)
    end

    def objects
      @objects
    end

    def parse_collection(object_name, json, parent_object_name = nil)
      handle_errors
      objects = []
      json["records"].first(object_limit(object_name, parent_object_name)).each do |hash|
        objects += parse_record(object_name, hash, parent_object_name)
      end

      objects
    end

    def parse_record(object_name, hash, parent_object_name = nil)
      attributes = hash.delete("attributes")
      record_id = attributes["url"].split("/").last
      url = @base_url + "/" + record_id
      # Get field for link and remove it from the list of fields
      label = object_label(object_name, hash, parent_object_name)
      fields = []
      related_objects = []

      parents = @parents && @parents.fetch(record_id, [])

      ordered_fields(object_name, hash, parent_object_name).each do |key, value|
        if HashParam.ish?(value)
          if value["records"]
            related_objects += parse_collection(key, value, object_name)
          else
            related_objects += parse_record(key, value, object_name)
          end
        elsif value.present? && is_field_included(object_name, key)
          value = value.truncate(MAX_FIELD_SIZE) if value.is_a? String
          fields << { "label" => field_label(object_name, key),
            "value" => value }
        end
      end

      record_type_value = attributes.fetch("display_type", record_type(object_name, parent_object_name))

      objects = [ { "url" => url,
        "record_id" => record_id,
        "label" => label,
        "record_type" => record_type_value,
        "fields" => fields,
        "parents" => parents } ]
      objects += related_objects
    end

    def record_type(object_name, parent_object_name)
      if parent_object_name.present?
        "#{friendly_name(object_name)} via #{friendly_name(@object_label)}"
      else
        friendly_name(@object_label)
      end
    end

    def object_label(object_name, hash, parent_object_name)
      fields = parent_object_name.nil? ? @primary : @related[object_name]
      fields.each do |field|
        return hash.delete(field) if hash[field].present?
      end

      object_name
    end

    def ordered_fields(object_name, hash, parent_object_name)
      fields = parent_object_name.nil? ? @primary : @related[object_name]
      hash.sort_by { |k, v| fields.index(k) || 99 }
    end

    def field_label(object_name, field)
      @labels["#{object_name}::#{field}"] || @labels[field] || field
    end

    def friendly_name(name)
      @configuration.friendly_name(name).underscore
    end

    def object_limit(object_name, parent_object_name)
      if parent_object_name.present?
        @configuration.relationship_limit(parent_object_name, object_name).to_i
      else
        5
      end
    end

    def is_field_included(object_name, key)
      @primary.include?(key) || (@related.key?(object_name) && @related[object_name].include?(key))
    end

    def handle_errors
      if @json.is_a?(Array) && @json.first["errorCode"].present?
        if @json.first["message"] =~ /RuntimeError/
          raise Salesforce::Integration::RuntimeError
        else
          raise @json.first["message"]
        end
      elsif @json.is_a?(String)
        # server not found
        raise Salesforce::Integration::RuntimeError
      end
    end

    private

    def json_response?(response)
      response.class == OAuth2::Response || response.class == Faraday::Response ? JSON.parse(response.body) : response
    rescue JSON::ParserError
      response.body
    end
  end
end
