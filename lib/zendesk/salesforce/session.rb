module Salesforce
  module Session
    Config = ::Zendesk::Configuration.fetch(:salesforce)

    def get_session(account, oauth_token, is_sandbox)
      @__session ||= begin
        response = oauth_token.post(is_sandbox ? Config['login_url_sandbox'] : Config['login_url'], '', {'Content-Type' => 'application/x-www-form-urlencoded'})

        if response.code != "200" || (doc = Nokogiri::XML(response.body)).at("/response/sessionId").nil?
          raise Salesforce::Integration::LoginFailed.new("Salesforce Login Failed (Account: #{account.subdomain}): #{response.body}")
        end

        {
          :session_id => doc.at("/response/sessionId").text,
          :server_url => doc.at("/response/serverUrl").text
        }
      end
    end

  end
end
