module Salesforce
  module Tools
    class SalesforceAPIException < StandardError; end

    autoload :REST,          'zendesk/salesforce/internal/rest'
    autoload :Configuration, 'zendesk/salesforce/internal/configuration'
  end
end
