module Salesforce::Tools
  module Configuration
    def self.config(root = :salesforce_internal)
      Zendesk::Configuration.dig(root)
    end

    def self.client
      REST.new(self.config)
    end
  end
end
