module Salesforce::Tools
  class REST
    TOKEN_URL = '/services/oauth2/token'

    def initialize(credentials)
      credentials.assert_valid_keys('login', 'password', 'consumer_key', 'consumer_secret', 'site')
      @credentials = credentials.with_indifferent_access
    end

    def query(statement)
      url      = URI(token['instance_url'])
      url.path = '/services/data/v20.0/query'
      params   = { :q => statement }
      headers  = { :headers => { 'Authorization' => "Bearer #{token.token}" } }
      response = token.get(url.to_s, { :params => params }.merge(headers))
      loop do
        data = response.parsed
        records = data['records']
        yield records, records.size
        break if data['done']
        url.path = data['nextRecordsUrl']
        response = token.get(url.to_s, headers)
      end
    end

    def session
      { :session_id => token.token, :server_host => URI(token['instance_url']).host }
    end

    def token
      @token ||= get_oauth_access_token(@credentials)
    end

    private

    def get_oauth_access_token(opts = { })
      key, secret, site = *opts.values_at(:consumer_key, :consumer_secret, :site)
      client            = OAuth2::Client.new(key, secret, :site => site, :token_url => TOKEN_URL)
      login, password   = *opts.values_at(:login, :password)
      client.password.get_token(login, password, :params => { :scope => 'api' })
    end
  end
end
