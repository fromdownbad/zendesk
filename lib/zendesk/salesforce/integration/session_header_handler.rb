require 'soap/header/simplehandler'

module Salesforce
  module Integration

    class SessionHeaderHandler < SOAP::Header::SimpleHandler
      HeaderName = XSD::QName.new('urn:partner.soap.sforce.com', 'SessionHeader')

      attr_accessor :session_id

      def initialize(session_id)
        super(HeaderName)
        @session_id = session_id
      end

      def on_simple_outbound
        if @session_id
      {'sessionId' => @session_id}
        else
          nil       # no header
        end
      end
    end

  end
end
