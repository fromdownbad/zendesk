require 'xsd/qname'

module Salesforce; module Integration


# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}LogInfo
#   category - Salesforce::Integration::LogCategory
#   level - Salesforce::Integration::LogCategoryLevel
class LogInfo
  attr_accessor :category
  attr_accessor :level

  def initialize(category = nil, level = nil)
    @category = category
    @level = level
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}LogCategory
class LogCategory < ::String
  All = LogCategory.new("All")
  Apex_code = LogCategory.new("Apex_code")
  Apex_profiling = LogCategory.new("Apex_profiling")
  Callout = LogCategory.new("Callout")
  Db = LogCategory.new("Db")
  System = LogCategory.new("System")
  Validation = LogCategory.new("Validation")
  Visualforce = LogCategory.new("Visualforce")
  Workflow = LogCategory.new("Workflow")
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}LogCategoryLevel
class LogCategoryLevel < ::String
  Debug = LogCategoryLevel.new("Debug")
  Error = LogCategoryLevel.new("Error")
  Fine = LogCategoryLevel.new("Fine")
  Finer = LogCategoryLevel.new("Finer")
  Finest = LogCategoryLevel.new("Finest")
  Info = LogCategoryLevel.new("Info")
  Internal = LogCategoryLevel.new("Internal")
  Warn = LogCategoryLevel.new("Warn")
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}LogType
class LogType < ::String
  Callout = LogType.new("Callout")
  Db = LogType.new("Db")
  Debugonly = LogType.new("Debugonly")
  Detail = LogType.new("Detail")
  None = LogType.new("None")
  Profiling = LogType.new("Profiling")
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}DebuggingInfo
#   debugLog - SOAP::SOAPString
class DebuggingInfo
  attr_accessor :debugLog

  def initialize(debugLog = nil)
    @debugLog = debugLog
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}DebuggingHeader
#   categories - Salesforce::Integration::LogInfo
#   debugLevel - Salesforce::Integration::LogType
class DebuggingHeader
  attr_accessor :categories
  attr_accessor :debugLevel

  def initialize(categories = [], debugLevel = nil)
    @categories = categories
    @debugLevel = debugLevel
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}CallOptions
#   client - SOAP::SOAPString
class CallOptions
  attr_accessor :client

  def initialize(client = nil)
    @client = client
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}SessionHeader
#   sessionId - SOAP::SOAPString
class SessionHeader
  attr_accessor :sessionId

  def initialize(sessionId = nil)
    @sessionId = sessionId
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}AllowFieldTruncationHeader
#   allowFieldTruncation - SOAP::SOAPBoolean
class AllowFieldTruncationHeader
  attr_accessor :allowFieldTruncation

  def initialize(allowFieldTruncation = nil)
    @allowFieldTruncation = allowFieldTruncation
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}GetAPIVersion
class GetAPIVersion
  def initialize
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}GetAPIVersionResponse
#   result - SOAP::SOAPString
class GetAPIVersionResponse
  attr_accessor :result

  def initialize(result = nil)
    @result = result
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}GetOrganizationFields
#   name - SOAP::SOAPString
class GetOrganizationFields
  attr_accessor :name

  def initialize(name = nil)
    @name = name
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}GetOrganizationFieldsResponse
#   result - SOAP::SOAPString
class GetOrganizationFieldsResponse
  attr_accessor :result

  def initialize(result = nil)
    @result = result
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}GetUserFields
#   emailAddress - SOAP::SOAPString
class GetUserFields
  attr_accessor :emailAddress

  def initialize(emailAddress = nil)
    @emailAddress = emailAddress
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}GetUserFieldsResponse
#   result - SOAP::SOAPString
class GetUserFieldsResponse
  attr_accessor :result

  def initialize(result = nil)
    @result = result
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}GetUserFieldsByList
class GetUserFieldsByList < ::Array
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}GetUserFieldsByListResponse
#   result - SOAP::SOAPString
class GetUserFieldsByListResponse
  attr_accessor :result

  def initialize(result = nil)
    @result = result
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}SyncTicketData
#   ticketXML - SOAP::SOAPString
class SyncTicketData
  attr_accessor :ticketXML

  def initialize(ticketXML = nil)
    @ticketXML = ticketXML
  end
end

# {http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI}SyncTicketDataResponse
class SyncTicketDataResponse
  def initialize
  end
end


end; end
