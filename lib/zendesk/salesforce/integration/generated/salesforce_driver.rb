require 'zendesk/salesforce/integration/generated/salesforce.rb'
require 'zendesk/salesforce/integration/generated/salesforce_mapping_registry.rb'
require 'soap/rpc/driver'

module Salesforce::Integration

class ZendeskAPIPortType < ::Zendesk::SoapBase
  DefaultEndpointUrl = "https://na2-api.salesforce.com/services/Soap/class/Zendesk/ZendeskAPI"

  def self.generate_method(name)
    [ "", name,
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI", name.capitalize]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI", "#{name.capitalize}Response"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ]
  end

  Methods = [
    generate_method("getAPIVersion"),
    generate_method("getOrganizationFields"),
    generate_method("getUserFields"),
    generate_method("getUserFieldsByList"),
    generate_method("syncTicketData")
  ]
end


end
