require 'zendesk/salesforce/integration/generated/salesforce.rb'
require 'soap/mapping'

module Salesforce; module Integration

module DefaultMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new
  NsZendeskAPI = "http://soap.sforce.com/schemas/class/Zendesk/ZendeskAPI"

  def self.generate_params(type)
    { :class => "Salesforce::Integration::#{type}".constantize,
      :schema_type => XSD::QName.new(NsZendeskAPI, type) }
  end

  def self.schema_element_soap(name)
    { :schema_element => [[name, "SOAP::SOAPString"]] }
  end

  def self.schema_element_soap_xsd(name)
    { :schema_element => [
        [name, ["SOAP::SOAPString", XSD::QName.new(NsZendeskAPI, name.capitalize)]]
      ] }
  end

  EncodedRegistry.register(generate_params("LogInfo").merge({
    :schema_element => [
      ["category", "Salesforce::Integration::LogCategory"],
      ["level", "Salesforce::Integration::LogCategoryLevel"]
    ]
  }))

  EncodedRegistry.register(generate_params("LogCategory"))

  EncodedRegistry.register(generate_params("LogCategoryLevel"))

  EncodedRegistry.register(generate_params("LogType"))

  LiteralRegistry.register(generate_params("LogInfo").merge({
    :schema_element => [
      ["category", "Salesforce::Integration::LogCategory"],
      ["level", "Salesforce::Integration::LogCategoryLevel"]
    ]
  }))

  LiteralRegistry.register(generate_params("LogCategory"))

  LiteralRegistry.register(generate_params("LogCategoryLevel"))

  LiteralRegistry.register(generate_params("LogType"))

  LiteralRegistry.register(generate_params("DebuggingInfo").merge(schema_element_soap("debugLog")))

  LiteralRegistry.register(generate_params("DebuggingHeader").merge({
    :schema_element => [
      ["categories", "Salesforce::Integration::LogInfo[]", [0, nil]],
      ["debugLevel", "Salesforce::Integration::LogType"]
    ]
  }))

  LiteralRegistry.register(generate_params("CallOptions").merge(schema_element_soap("client")))

  LiteralRegistry.register(generate_params("SessionHeader").merge(schema_element_soap("sessionId")))

  LiteralRegistry.register(generate_params("AllowFieldTruncationHeader").merge(schema_element_soap("allowFieldTruncation")))

  LiteralRegistry.register(generate_params("GetAPIVersion").merge({:schema_element => []}))

  LiteralRegistry.register(generate_params("GetAPIVersionResponse").merge(schema_element_soap("result")))

  LiteralRegistry.register(generate_params("GetOrganizationFields").merge(schema_element_soap_xsd("name")))

  LiteralRegistry.register(generate_params("GetOrganizationFieldsResponse").merge(schema_element_soap("result")))

  LiteralRegistry.register(generate_params("GetUserFields").merge(schema_element_soap_xsd("emailAddress")))

  LiteralRegistry.register(generate_params("GetUserFieldsResponse").merge(schema_element_soap("result")))

  LiteralRegistry.register(generate_params("GetUserFieldsByList").merge({
    :schema_element => [
      ["iDs", ["SOAP::SOAPString[]", XSD::QName.new(NsZendeskAPI, "IDs")], [0, nil]]
    ]
  }))

  LiteralRegistry.register(generate_params("GetUserFieldsByListResponse").merge(schema_element_soap("result")))

  LiteralRegistry.register(generate_params("SyncTicketData").merge(schema_element_soap_xsd("ticketXML")))

  LiteralRegistry.register(generate_params("SyncTicketDataResponse").merge({ :schema_element => [] }))

end

end; end
