module Salesforce
  module Integration
    class LoginFailed < StandardError; end;
    class RuntimeError < StandardError; end;
  end
end
