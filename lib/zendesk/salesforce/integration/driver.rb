module Salesforce
  module Integration
    class Driver
      def self.get_driver(session_info)
        endpoint = salesforce_endpoint(session_info[:server_url], ZendeskAPIPortType::DefaultEndpointUrl)
        driver = ZendeskAPIPortType.new(endpoint)
        header = SessionHeaderHandler.new(session_info[:session_id])
        driver.headerhandler << header
        driver
      end

      def self.salesforce_endpoint(server_url, default_endpoint)
        URI::HTTPS.build(:host => URI.parse(server_url).host,
                         :path => URI.parse(default_endpoint).path)
      end

    end
  end
end
