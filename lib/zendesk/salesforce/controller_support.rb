module Zendesk::Salesforce
  class ControllerSupport
    attr_accessor :account, :params

    # Creates an instance of the controller support
    #
    # Examples:
    #
    #    controller_support = Zendesk::Salesforce::ControllerSupport.new({
    #      :account => current_account,
    #      :params => {}
    #    })
    def initialize(options)
      @account = options.fetch(:account)
      @params = options.fetch(:params)
    end

    # Returns objects for the add/edit modal pulldown
    # Excludes objects that can't be used in queries
    # Returns empty array if there's a login error.
    #
    # Examples
    #
    #    1) Adding object (returns all objects that are not already configured):
    #
    #    controller_support = Zendesk::Salesforce::ControllerSupport.new({
    #      :account => current_account,
    #      :params => {}
    #    })
    #
    #    >> controller_support.objects
    #    => [
    #         { "name" => "Account", "label" => "Account Label" },
    #         { "name" => "Contact", "label" => "Contact Label" },
    #         ...
    #       ]
    #
    #    2) Editing object (returns only the object to edit):
    #
    #    controller_support = Zendesk::Salesforce::ControllerSupport.new({
    #      :account => current_account,
    #      :params => { :object => "Account" }
    #    })
    #
    #    >> controller_support.objects
    #    => [{ "name" => "Account", "label" => "Account Label" }]
    #
    def objects
      if object_name.present?
        [{ "name" => object_name, "label" => object_label }]
      else
        begin
          available_objects.reject { |obj| selected_objects.include?(obj["name"]) || !obj["queryable"] }
        rescue Salesforce::Integration::LoginFailed => e
          []
        end
      end
    end

    # Returns an array of primary and related fields properly organized in a hash
    # with all the necessary information to let views build
    # the fields tree when adding/editing a Salesforce object
    #
    # Examples:
    #
    # >> controller_support.fields
    # =>
    # [
    #   {
    #     :title => "Contact ID",
    #     :key => "Id",
    #     :select => false,
    #     :type => "id"
    #   },
    #   {
    #     :title => "Email",
    #     :key => "Email",
    #     :select => true,
    #     :type => "email"
    #   },
    #   {
    #     :fields => [
    #       {
    #         :key => "Account::Name",
    #         :select => true,
    #         :title => "Account Name",
    #         :type => "string"
    #       }
    #     ],
    #     :isFolder => true,
    #     :object => "Account",
    #     :relationship => "Account",
    #     :title => "Account",
    #     :type => "child"
    #   },
    # ]
    def fields
      begin
        primary_fields + related_fields
      rescue Salesforce::Integration::LoginFailed => e
        []
      end
    end

    # Returns an array of the field names already configured by the user for the given object
    #
    # Examples:
    #
    # >> controller_support.selected_fields
    # => ["Name", "Opportunities::Name", "Opportunities::Probability"]
    def selected_fields
      configuration.fields(object_name)
    end

    # Returns an array containing the configured Salesforce and Zendesk fields to use in the SOQL condition
    #
    # Examples:
    #
    # >> controller_support.selected_mapping
    # => ["Name", "ticket.organization.name"]
    def selected_mapping
      configuration.mapping(object_name)
    end

    # Returns an array of fields used by the modal window that defines the filter for related objects
    #
    # The main array contains 2 arrays:
    # The first array contains the fields with type date or datetime
    # The second array contains all the fields except those with type date, datetime and textarea
    #
    # The fields are hashes with all the necessary information used by the view
    #
    # Examples:
    #
    # >> controller_support.available_filter_fields
    # =>
    # [
    #   [
    #     {
    #       :title => "Close Date",
    #       :key => "CloseDate",
    #       :select => false,
    #       :type => "date"
    #     }
    #   ],
    #   [
    #     {
    #       :title => "Name",
    #       :key => "Name",
    #       :select => false,
    #       :type => "string"
    #     }
    #   ]
    # ]
    def available_filter_fields
      fields = relationship_fields

      [date_fields(fields), regular_fields(fields)]
    end

    # Returns an array of the filters already defined by the user for the given object and related object
    # The filters returned are hashes with all the information the modal window needs to select the current values
    #
    # Examples:
    #
    # >> controller_support.available_filter_fields
    # => [{"field"=>"Probability", "type"=>"percent", "operator"=>"=", "value"=>"76"}]
    def selected_filter_fields
      fields = configuration.relationship_filter_fields(object_name, relationship_name)

      [date_fields(fields).first || {}, regular_fields(fields).first || {}]
    end

    # Returns the defined number of objects that needs to be returned for the given object and related object
    def relationship_limit
      configuration.relationship_limit(object_name, relationship_name)
    end

    # Returns an array of fields of the given related object
    # The fields are returned in the same format as in "controller_support.fields" except that
    # the :key contains the relationship_name to use in the configuration definition: "#{relationship_name}::#{field['name']}"
    def related_object_fields
      begin
        integration.fields(object_name, [], relationship_name)
      rescue Salesforce::Integration::LoginFailed => e
        []
      end
    end

    # Saves the defined filters in the Salesforce configuration
    def save_filter
      filters = params[:filters].map { |filter| JSON.parse(filter) }

      configuration.add_filters(object_name, relationship_name, params[:limit], filters)
    end

    # Saves to the configuration, the object defined in the modal window to add/edit Salesforce object
    def save_object
      configuration.add(params[:salesforce_object],
                        params[:fields],
                        JSON.parse(params[:labels]),
                        JSON.parse(params[:relationships]),
                        params[:mapping_salesforce_field],
                        params[:mapping_zendesk_field],
                        params[:mapping_salesforce_field_type],
                        params[:object_label])
    end

    # Removes the given object from the configuration
    def remove_object
      configuration.remove(object_name)
    end

    # Saves the new defined order
    # This information is used to show the objects in the Salesforce App in a customized order
    def sort_objects
      configuration.sort(params["salesforce_selected_objects_sort_list"])
    end

    def relationship_fields
      relationships  = configuration.relationships(object_name)
      related_object = relationships[relationship_name]['object']

      begin
        integration.fields(related_object, [], nil)
      rescue Salesforce::Integration::LoginFailed => e
        []
      end
    end

    private

    def object_name
      params[:object]
    end

    def relationship_name
      params[:relationship_name]
    end

    def object_label
      configuration.object_label(object_name) if object_name.present?
    end

    def available_objects
      @available_objects ||= integration.list_objects
    end

    def date_fields(fields)
      fields.select { |field| ["date", "datetime"].include? field.symbolize_keys[:type] }
    end

    def regular_fields(fields)
      fields.reject { |field| ["date", "datetime", "textarea"].include? field.symbolize_keys[:type] }
    end

    def primary_fields
      integration.fields(object_name, selected_fields, nil)
    end

    def related_fields
      integration.related_fields(object_name, selected_fields)
    end

    def selected_objects
      @selected_objects ||= configuration.objects
    end

    def integration
      account.salesforce_integration
    end

    def configuration
      integration.configuration
    end

  end
end
