module Salesforce
  module AppSettings

    DEFAULT_DELAY = 60

    def stale?
      updated_at < delay.ago
    end

    def records
      data && data[:records]
    end

    def delete_cache
      self.data = data
      delete
    end

    private

    def delay
      custom_delay.try(:minutes) || (Rails.env.production? ? DEFAULT_DELAY.minutes : 20.seconds)
    end

    def custom_delay
      if has_central_admin_salesforce_integration?
        ngsfdc_integration.app_latency
      elsif has_configurable_salesforce_app_latency?
        account.salesforce_integration.app_latency
      end
    end

    def has_central_admin_salesforce_integration?
      return false unless account.has_central_admin_salesforce_integration?
      ngsfdc_integration.configured_and_enabled? && ngsfdc_integration.data_cache_enabled?
    end

    def has_configurable_salesforce_app_latency?
      account.has_configurable_salesforce_app_latency? && account.salesforce_integration.app_latency
    end

    def ngsfdc_integration
      @ngsfdc_integration ||= Salesforce::NextGeneration::Integration.new(account)
    end
  end
end
