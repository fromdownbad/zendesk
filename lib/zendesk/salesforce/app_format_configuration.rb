require_dependency 'zendesk/salesforce/zendesk_fields'

module Zendesk::Salesforce
  module AppFormatConfiguration
    include Zendesk::Salesforce::ZendeskFields

    private

    def app_format
      { objects: app_format_objects }
    end

    def save_app_format_object(object)
      fields, labels, relationships = db_format(object)
      configuration.add(
        object[:api_name],
        fields,
        labels,
        relationships,
        object[:mapping][:sf][:api_name],
        object[:mapping][:zd][:key],
        "",
        object[:label]
      )
    end

    def save_app_format_object_filter(filter)
      configuration.add_filters(
        filter[:parent],
        filter[:relationship],
        filter[:limit],
        db_format_filter_fields(filter[:filter_fields])
      )
    end

    def reorder_app_format_objects(objects)
      objects_position = objects.each_with_index do |object, index|
        configuration.object(object[:api_name])["position"] = index
      end
      configuration.sort(objects_position)
    end

    def app_format_objects
      configuration.objects.map { |object_name| app_format_object(object_name) }
    end

    def app_format_object(object_name)
      fields, related_objects = app_format_fields(object_name)
      {
        api_name: object_name,
        label: configuration.object_label(object_name),
        mapping: app_format_mapping(object_name),
        fields: fields,
        related_objects: related_objects
      }
    end

    def app_format_fields(object_name)
      primary = []
      related = []
      labels = configuration.labels(object_name)

      configuration.fields(object_name).each do |field|
        if related_object_field?(field)
          relationship_name, field_api_name = field.split("::")

          related_object = related.find { |r| r[:relationship] == relationship_name }
          if related_object.nil?
            related_object = app_format_related_object(object_name, relationship_name)
            related << related_object
          end
          related_object[:fields] << { api_name: field_api_name, label: labels[field] }

        else
          primary << { api_name: field, label: labels[field] }
        end
      end
      [primary, related]
    end

    def app_format_related_object(object_name, relationship_name)
      relationships = configuration.relationships(object_name)
      {
        label: configuration.friendly_name(relationship_name),
        relationship: relationship_name,
        object: relationships[relationship_name]["object"],
        type: relationships[relationship_name]["type"],
        limit: configuration.relationship_limit(object_name, relationship_name),
        filter_fields: app_format_filters(configuration.relationship_filter_fields(object_name, relationship_name)),
        fields: []
      }
    end

    def app_format_filters(filters)
      filters.map { |filter| app_format_filter(filter) }
    end

    def app_format_filter(filter)
      {
        api_name: filter["field"],
        data_type: filter["type"],
        operator: filter["operator"],
        value: filter["value"]
      }
    end

    def app_format_mapping(object_name)
      sf_api_name = configuration.salesforce_mapped_field(object_name)
      zd_mapped_field = configuration.zendesk_mapped_field(object_name)

      {
        zd: {
          key: zd_mapped_field,
          label: mapped_zendesk_field(zd_mapped_field)
        },
        sf: {
          api_name: sf_api_name,
          label: configuration.labels(object_name)[sf_api_name]
        }
      }
    end

    def db_format(object)
      fields = []
      labels = {}
      relationships = {}

      object[:fields].each do |field|
        fields << field[:api_name]
        labels[field[:api_name]] = field[:label]
      end

      if object[:related_objects]
        object[:related_objects].each do  |related_object|
          related_object[:fields].each do  |field|
            key = "#{related_object[:relationship]}::#{field[:api_name]}"
            fields << key
            labels[key] = field[:label]

            relationships[related_object[:relationship]] = {
              object: related_object[:object],
              type: related_object[:type]
            }.stringify_keys
          end
        end
      end
      [fields, labels, relationships]
    end

    def db_format_filter_fields(filter_fields)
      filter_fields.map do |fields|
        {
          field: fields[:api_name],
          type: fields[:data_type],
          operator: fields[:operator],
          value: fields[:value]
        }.stringify_keys
      end
    end

    def related_object_field?(field)
      field.include?("::")
    end
  end
end
