require 'zendesk/salesforce/oauth/oauth.rb'
require 'zendesk/salesforce/oauth/oauth1.rb'
require 'zendesk/salesforce/oauth/oauth2.rb'

module Salesforce
  module Oauth

    # ConnectionHandler hides oauth1/oauth2 implementation and uses one or the other depending on arturo feature and current available keys.
    # Acts as an interface for the controllers and models to handle OAuth process and REST queries.
    #
    # When used from a controller, OAuth1 or OAuth2 will be used depending on the arturo feature.
    # When used from a model, the requests will use OAuth1 or OAuth2 depending on the current available tokens.
    #
    # Examples
    #
    #    1) Using in a controller to handle OAuth process:
    #
    #    connection_handler = Salesforce::Oauth::ConnectionHandler.new({
    #      :account => current_account,
    #      :params => params,
    #      :session => session
    #    })
    #
    #    Starting process:
    #    redirect_to connection_handler.authorize_url
    #
    #    Callback:
    #    connection_handler.callback
    #
    #    2) Using in a model to make REST queries, get session info and update token:
    #
    #    connection_handler = Salesforce::Oauth::ConnectionHandler.new({
    #      :account => account,
    #      :salesforce_integration => account.salesforce_integration
    #    })
    #
    #    REST:
    #    connection_handler.http_get("/services/data/v23.0/sobjects")
    #    connection_handler.http_get("/services/data/v23.0/query", :q => soql_expression)
    #
    #    Session info:
    #    * OAuth1: Logs in to get session id
    #    * OAuth2: Uses the access_token as session id
    #    connection_handler.session_info
    #    >> { :server_url => url, :session_id => session_id }
    #
    #    Update token:
    #    connection_handler.update_token
    #
    class ConnectionHandler
      attr_accessor :account, :params, :session, :use_oauth2

      Config = ::Zendesk.config

      def initialize(options)
        @account = options.fetch(:account)
        @params = options.fetch(:params, nil)
        @session = options.fetch(:session, nil)
        @salesforce_integration = options.fetch(:salesforce_integration, nil)

        @use_oauth2 = @salesforce_integration ? @salesforce_integration.configured_with_oauth2? : true
      end

      def authorize_url
        session[:salesforce_environment] = params[:salesforce_environment]

        oauth.authorize_url
      end

      def callback
        oauth.callback

        salesforce_integration.destroy_other_integrations
        salesforce_integration.create_target
      end

      def http_get(path, parameters = {})
        oauth.http_get(path, parameters)
      end

      def http_post(path, parameters = {})
        oauth.http_post(path, parameters)
      end

      def session_info
        oauth.session_info
      end

      def update_token
        oauth.update_token
      end

      private

      def callback_url
        "https://#{account.default_host}/agent?redirect=salesforce_auth"
      end

      def static_callback_url
        host = "support.#{Config[:host]}"
        port = Config[:port].present? ? ":#{Config[:port]}" : nil
        "https://#{host}#{port}/ping/redirect_to_account"
      end

      def salesforce_integration
        @salesforce_integration ||= begin
          if session[:salesforce_environment] == "sandbox"
            account.salesforce_sandbox || account.create_salesforce_sandbox
          else
            account.salesforce_production || account.create_salesforce_production
          end
        end
      end

      def url_builder
        @url_builder ||= UrlBuilder.new(account)
      end

      def oauth
        @oauth ||= use_oauth2 ? Oauth2.new(self) : Oauth1.new(self)
      end

      class UrlBuilder
        # Use this module instead after upgrading to rails 3:
        # include Rails.application.routes.url_helpers
        include Rails.application.routes.url_helpers
        def initialize(account)
          default_url_options[:host] = account.host_name
        end
      end
    end
  end
end
