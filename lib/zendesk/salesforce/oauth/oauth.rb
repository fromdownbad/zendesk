module Salesforce
  module Oauth
    # Abstract Class
    class Oauth
      attr_reader :handler

      # Delegate to private methods
      [:salesforce_integration, :params, :session, :callback_url, :static_callback_url, :account].each do |m|
        define_method(m) do |*args, &block|
          handler.send(m, *args, &block)
        end
      end

      Config = ::Zendesk.config.fetch(:salesforce)

      def initialize(handler)
        @handler = handler
      end

      def authorize_url
        raise "Must be defined in subclass"
      end

      def callback
        raise "Must be defined in subclass"
      end

      def config_options
        config_option = salesforce_integration.is_sandbox? ? :oauth_options_sandbox : :oauth_options
        ::Zendesk::Configuration.dig(:salesforce, config_option)
      end
    end
  end
end
