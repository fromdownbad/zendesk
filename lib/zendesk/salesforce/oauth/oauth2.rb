# Salesforce OAuth2 requires a static callback url in the Remote Access configuration and
# the callback sent during the process must be exactly the same as the static one defined.
#
# Static callbacks are a problem for our urls with subdomains.
#
# Fortunately, Salesforce authorization endpoint provides a "state" parameter,
# that can be any state the consumer wants reflected back to it after approval.
# We can send the callback path and account id in the "state" parameter and
# redirect using PingController#redirect_to_account.
#
# These are the static callback urls that need to be defined in the Remote Access configuration:
#   * Production: https://support.zendesk.com/ping/redirect_to_account
#   * Acceptance: https://support.zendesk-acceptance.com/ping/redirect_to_account
#   * Dev: https://support.localhost:3000/ping/redirect_to_account

module Salesforce
  module Oauth
    class Oauth2 < Oauth
      def authorize_url
        client.auth_code.authorize_url(:redirect_uri => static_callback_url, :state => state)
      end

      def callback
        access_token = client.auth_code.get_token(params[:code], :redirect_uri => static_callback_url)

        token = access_token.token
        refresh_token = access_token.refresh_token
        instance_url = access_token.params['instance_url']

        if token.present? && refresh_token.present? && instance_url.present?
          salesforce_integration.update_attributes(access_token: token,
                                                   refresh_token: refresh_token,
                                                   encrypted_access_token: token,
                                                   encrypted_refresh_token: refresh_token,
                                                   instance_url: instance_url)
        else
          raise Salesforce::Integration::LoginFailed.new("Salesforce Login Failed (Account: #{account.subdomain}): OAuth2 process failed")
        end
      end

      def http_get(path, parameters = {})
        url = "#{instance_url}#{path}"
        url << "?#{parameters.to_query}" if parameters.present?

        response = access_token.get(url)
        if response.status == 401
          raise Salesforce::Integration::LoginFailed.new("Salesforce Query Failed (Account: #{account.subdomain}): OAuth2 token could not be refreshed")
        end

        response
      end

      def http_post(path, parameters = {})
        response = access_token.post("#{instance_url}#{path}", parameters)

        if response.status == 401
          raise Salesforce::Integration::LoginFailed.new("Salesforce Post Failed (Account: #{account.subdomain}): OAuth2 token could not be refreshed")
        end

        response
      end

      def session_info
        { :server_url => instance_url, :session_id => token }
      end

      def update_token
        access_token.update_token
      end

      private

      def state
        uri = URI.parse(callback_url)
        # Remove first '/' character
        # otherwise ping_controller will build url with 2
        path = uri.path[1..-1]
        path << "?#{uri.query}" if uri.query
        "#{account.subdomain}:#{path}"
      end

      def instance_url
        salesforce_integration.instance_url
      end

      def token
        salesforce_integration.decrypted_access_token
      end

      def refresh_token
        salesforce_integration.decrypted_refresh_token
      end

      def client
        @client ||= begin
          oauth_options = {
            :site               => config_options["site"],
            :authorize_url      => '/services/oauth2/authorize',
            :token_url          => '/services/oauth2/token',
            :raise_errors       => false
          }

          OAuth2::Client.new(Config["consumer_key"], Config["consumer_secret"], oauth_options)
        end
      end

      def access_token
        @access_token ||= begin
          options = {
            :access_token => token,
            :refresh_token => refresh_token,
            :salesforce_integration => salesforce_integration,
            :header_format => 'OAuth %s'
          }

          ForceToken.from_hash(client, options)
        end
      end

      # Subclass OAuth2::AccessToken so we can do auto-refresh and save the new token.
      class ForceToken < OAuth2::AccessToken
        def request(verb, path, opts={}, &block)
          response = super(verb, path, opts, &block)
          if response.status == 401 && refresh_token
            update_token
            response = super(verb, path, opts, &block)
          end
          response
        end

        def update_token
          @token = refresh!.token
          self[:salesforce_integration].update_attributes(access_token: @token, encrypted_access_token: @token)
        end
      end

    end
  end
end
