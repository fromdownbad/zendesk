require 'zendesk/salesforce/session.rb'

module Salesforce
  module Oauth
    class Oauth1 < Oauth
      include Salesforce::Session

      def authorize_url
        request = consumer.get_request_token(:oauth_callback => callback_url)

        session[:request_token]          = request.token
        session[:request_token_secret]   = request.secret

        request.authorize_url(:oauth_consumer_key => Config['consumer_key'])
      end

      def callback
        begin
          access = request_token.get_access_token(:oauth_verifier => params[:oauth_verifier])

          session[:request_token] = session[:request_token_secret] = nil

          salesforce_integration.update_attributes(token: access.token,
                                                   secret: access.secret,
                                                   encrypted_token: access.token,
                                                   encrypted_secret_value: access.secret)
        rescue OAuth::Unauthorized
          raise Salesforce::Integration::LoginFailed.new("Salesforce Login Failed (Account: #{account.subdomain}): OAuth1 process failed")
        end
      end

      def http_get(path, parameters = {})
        conn = Faraday.new(:url => "#{instance_url}#{path}")
        conn.get do |req|
          req.headers['Authorization'] = "OAuth #{session_info[:session_id]}"
          req.params = parameters
        end
      end

      def http_post(path, parameters = {})
        headers = { 'Authorization' => "OAuth #{session_info[:session_id]}" }

        conn = Faraday.new(:url => "#{instance_url}#{path}")
        conn.post do |req|
          req.headers = headers.merge(parameters[:headers] || {})
          req.body = parameters[:body]
        end
      end

      def session_info
        get_session(account, access_token, salesforce_integration.is_sandbox?)
      end

      def update_token
        raise "Update token is not implemented in Oauth1"
      end

      private

      def instance_url
        salesforce_integration.salesforce_instance_url
      end

      def consumer
        OAuth::Consumer.new(Config["consumer_key"], Config["consumer_secret"], config_options.symbolize_keys)
      end

      def access_token
        OAuth::AccessToken.new(consumer, salesforce_integration.decrypted_token, salesforce_integration.decrypted_secret_value)
      end

      def request_token
        OAuth::RequestToken.new(consumer, session[:request_token], session[:request_token_secret])
      end

    end
  end
end
