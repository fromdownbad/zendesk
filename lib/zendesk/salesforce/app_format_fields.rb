module Zendesk::Salesforce
  module AppFormatFields
    private

    def app_format_fields
      primary = []
      related = []

      salesforce_support.fields.map do |field|
        if field[:fields].nil?
          primary << app_format_field(field)
        else
          related << app_format_related(field)
        end
      end
      { fields: primary, related_objects: related }
    end

    def app_format_related_fields
      salesforce_support.relationship_fields.map { |field| app_format_field(field) }
    end

    def app_format_related(field)
      {
        object: field[:object],
        label: field[:title],
        type: field[:type],
        relationship: field[:relationship]
      }
    end

    def app_format_field(field)
      field_name = field_name(field)
      {
        api_name: field_name,
        label: field[:title],
        data_type: field[:type],
        picklist_values: field[:picklistValues],
        length: field[:length],
        updateable: field[:updateable],
        scale: field[:scale]
      }
    end

    def field_name(field)
      field[:key].include?("::") ? field[:key].split("::")[1] : field[:key]
    end
  end
end
