require 'zendesk_mail'
require 'zendesk_mtc_mpq_migration'
require 'uuidtools'

require 'zendesk/inbound_mail/clients/client'
require 'zendesk/inbound_mail/clients/unknown'

require 'zendesk/inbound_mail/clients/aol_mail'
require 'zendesk/inbound_mail/clients/apple_mail'
require 'zendesk/inbound_mail/clients/blackberry'
require 'zendesk/inbound_mail/clients/facebook_mail'
require 'zendesk/inbound_mail/clients/gmail'
require 'zendesk/inbound_mail/clients/iphone'
require 'zendesk/inbound_mail/clients/lotus_notes'
require 'zendesk/inbound_mail/clients/mac_mail'
require 'zendesk/inbound_mail/clients/microsoft_exchange'
require 'zendesk/inbound_mail/clients/microsoft_outlook'
require 'zendesk/inbound_mail/clients/thunderbird'
require 'zendesk/inbound_mail/clients/yahoo'
require 'zendesk/inbound_mail/clients/zimbra'

require 'zendesk/inbound_mail/inbound_message/processor_chain_support'

module Zendesk
  module InboundMail
    MAX_UNDELIVERABLE_COUNT = 10

    class StatsD
      def self.client
        @@client ||= Zendesk::StatsD::Client.new(namespace: "mail_ticket_creator")
      end
    end

    class SyntaxError < RuntimeError
    end

    class Halt < StandardError
    end

    class HardMailRejectException < Halt
    end

    class DuplicateMailRejectException < Halt
    end

    class SQSMaxReceivesError < StandardError
    end

    class MemoryLimitError < StandardError
    end
  end
end

Zendesk::Mail.log_file_path = "#{Rails.root}/log/mail_processing_#{Rails.env}.log"

Zendesk::Mail::InboundMessage.class_eval do
  include Zendesk::MtcMpqMigration::ZendeskMail::TicketIdentification
  include Zendesk::MtcMpqMigration::ZendeskMail::ProcessorChainSupport
  include Zendesk::Mail::Recipient
  include Zendesk::MtcMpqMigration::ZendeskMail::Sender
  include Zendesk::InboundMail::MailParsingQueue

  def self.generate_message_id
    "<#{::Mail.random_tag}_sprut@#{Zendesk::Configuration.fetch(:host)}>"
  end
end
