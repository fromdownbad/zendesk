# Contains methods for rate limiting the creation of entries and posts for an account.
#
# Example:
#
#   class PostsController
#     include ForumRateLimiting
#
#     before_action :rate_limit_posts, :only => :create
#
#     def create
#       ...
#     end
#   end
#
module Zendesk
  module ForumRateLimiting
    protected

    # This method is used both for post & entries.
    def rate_limit_web_portal_end_user_content
      # (we keep the "comments" taxonomy for historical reason)
      rate_limit :forum_comments, current_account.settings.end_user_comment_rate_limit
    end

    def rate_limit(handle, threshold)
      unless current_user.is_agent?
        Prop.throttle!(handle, current_user.id, threshold: threshold)
        Prop.throttle!(handle, [current_account.id, request.remote_ip], threshold: threshold)
      end
    end
  end
end
