module Zendesk
  module PushNotifications
    module Gdpr
      # Pull GDPR requests from Amazon SQS for Push Notification actions
      class GdprSqsUserDeletionSubscriber < GdprSqsSubscriber
        def initialize(sqs_support_queue_url: nil, region: nil, access_key: nil, secret_key: nil, session_token: nil)
          @sqs_support_queue_url = sqs_support_queue_url || Zendesk::Gdpr::Configuration.fetch(:sqs_support_queue_url)
          @region                = region                || Zendesk::Gdpr::Configuration.fetch(:region)
          @access_key            = access_key            || Zendesk::Gdpr::Configuration.dig(:writer_aws_access_key) || Zendesk::Gdpr::Configuration.fetch(:aws_access_key)
          @secret_key            = secret_key            || Zendesk::Gdpr::Configuration.dig(:writer_aws_secret_key) || Zendesk::Gdpr::Configuration.fetch(:aws_secret_key)
          @session_token         = session_token         || Zendesk::Gdpr::Configuration.dig(:aws_session_token)
        end
      end
    end
  end
end
