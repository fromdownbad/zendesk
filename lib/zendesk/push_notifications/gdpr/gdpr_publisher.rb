module Zendesk
  module PushNotifications
    module Gdpr
      # Push GDPR requests to Amazon SNS for Push Notification actions
      class GdprPublisher
        attr_reader :compliance_deletion_status, :publisher

        delegate :user_id, :account_id, :account, :executer_id, to: :compliance_deletion_status

        # compliance_deletion_status is an instance of ComplianceDeletionStatus
        # publisher                  is an instance of GdprSnsPublisher for publishing to SNS/SQS
        def initialize(compliance_deletion_status, publisher)
          @compliance_deletion_status = compliance_deletion_status
          @publisher = publisher
        end

        def publish
          raise ComplianceDeletionError, 'Implement publish method in subclass of GdprPublisher'
        end

        def publish_to_incomplete!(applications)
          applications.each do |app|
            publisher.publish(
              subject: "GDPR Request for #{app}",
              message: user_message_for_application(app)
            )
          end
        end

        private

        def user_message_for_application(app)
          {
            user_id: user_id,
            account_id: account_id,
            account_subdomain: account.subdomain,
            action: ComplianceDeletionStatus::REQUEST_DELETION,
            application: app,
            executer_id: executer_id,
            pod: account.pod_id
          }
        end
      end
    end
  end
end
