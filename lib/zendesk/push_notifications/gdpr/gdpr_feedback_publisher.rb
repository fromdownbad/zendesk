module Zendesk
  module PushNotifications
    module Gdpr
      # Push GDPR requests to Amazon SNS for Push Notification actions
      class GdprFeedbackPublisher < GdprPublisher
        def publish(feedback_action)
          publisher.publish(
            subject: "GDPR Feedback",
            message: feedback_message(feedback_action)
          )
        end

        private

        def feedback_message(feedback_action)
          {
            user_id: user_id,
            account_id: account_id,
            account_subdomain: account.subdomain,
            action: feedback_action,
            application: ComplianceDeletionStatus::CLASSIC,
            pod: account.pod_id
          }
        end
      end
    end
  end
end
