require 'aws-sdk-sqs'

module Zendesk
  module PushNotifications
    module Gdpr
      # Pull GDPR requests from Amazon SQS for Push Notification actions
      class GdprSqsSubscriber
        # sqs_support_queue_url => The SQS queue URL
        # region    => AWS region where SNS exists  (like us-west-2)
        # access_key => public AWS identifier
        # secret_key => Secret AWS identifier
        # session_token => Optional. Used in dev environment for assumed roles.
        def initialize(sqs_support_queue_url: nil, region: nil, access_key: nil, secret_key: nil, session_token: nil)
          @sqs_support_queue_url = sqs_support_queue_url
          @region                = region
          @access_key            = access_key
          @secret_key            = secret_key
          @session_token         = session_token
        end

        def delete_message(sqs_message)
          sqs_client.delete_message(queue_url: @sqs_support_queue_url,
                                    receipt_handle: sqs_message.receipt_handle)
        end

        def batch_delete_messages(messages)
          full_messages = messages.map { |msg| msg[0] }

          entries = full_messages.map do |message|
            {
              id: message.message_id,
              receipt_handle: message.receipt_handle
            }
          end

          resp = sqs_client.delete_message_batch(
            queue_url: @sqs_support_queue_url,
            entries: entries
          )

          if resp && resp.failed
            resp.failed.each do |failed_message|
              Rails.logger.error "Failed to delete message in batch_delete_messages. Message id: #{failed_message.id}, code: #{failed_message.code}, sender fault: #{failed_message.sender_fault}, message: #{failed_message.message}"
            end
          end
        end

        # returns the format of
        # 1) raw_message from the publisher of the message
        #    In this case it is the raw SQS response (A Kafka response could be returned if we create a kafka subscriber)
        #
        # 2) json_message in the extracted from the message
        #
        #    json_message = {
        #      user_id:           user_id,
        #      account_id:        account_id,
        #      account_subdomain: subdomain,
        #      action:            action,
        #      application:       application,
        #      ...
        #    }.to_json
        #
        def receive_next_message
          sqs_message = sqs_client.receive_message(queue_url: @sqs_support_queue_url,
                                                   attribute_names: ["All"],
                                                   message_attribute_names: ["All"],
                                                   max_number_of_messages: 1,
                                                   visibility_timeout: 0,
                                                   wait_time_seconds: 1).messages[0]
          return false unless sqs_message
          json_message = parse_message(sqs_message)
          if json_message
            [sqs_message, json_message]
          else
            receive_next_message
          end
        end

        # returns the format of
        # 1) raw_message from the publisher of the message
        #    In this case it is the raw SQS response (A Kafka response could be returned if we create a kafka subscriber)
        #
        # 2) json_message in the format to process the GDPR request
        #
        #    json_message = {
        #      user_id:           user_id,
        #      account_id:        account_id,
        #      account_subdomain: subdomain,
        #      action:            action,
        #      application:       application,
        #    }.to_json
        #
        # raw_message is in the following format:
        #
        # <struct Aws::SQS::Types::Message
        #   message_id="d2e477a9-8d34-47b2-9d08-167546200aa5",
        #   receipt_handle="AQEB2sotw3ZBSLaR8Nea9XpUUZTQj+ISXkRk79m13brH51upb8oSfodglQ0Zd7cc4gIRX0DnoN7egFfHupZVsagPIjThS+AXK//JN0e/34u1kf+nVdIEh0WoD8aIlX3AxDQDevEg8+iN8w+exLC1aAVaxzaTmwkRLWO4ErodRYtYOXo4YK9RL46/slo2Z/9Qv+0fvk8ED/KPxb1HU5rMGgohw4nBTOTW/XLTyQ7oe/LR+zGJrrlaWgtrSplGBKxN3OUdt+m1vdSBNbd3kA2nNt9yq+dq1z598x9GT5YZb1JTkxNYEEyaVhIHkVy5XTQAcN6npcklBhEDsCB7/qmSqtF5tzuZgtTe42/mL2zT8Ai3Ryxowjz78UYsZBuUC/yNJ2I1ATOASrCDWtf+Bec+a3cVb+MyEJad/RbvskU8QJh4wH4=",
        #   md5_of_body="c2d37d03ea2156b725a02aea1e4f6abc",
        #   body="{\n
        #      \"Type\" : \"Notification\",\n
        #      \"MessageId\" : \"da117057-95c0-5485-a37d-235050ddbcc9\",\n
        #      \"TopicArn\" : \"arn:aws:sns:us-west-2:724781030999:staging_us_gdpr_topic\",\n
        #      \"Subject\" : \"GDPR Request\",\n
        #      \"Message\" : \"[{\\\"user_id\\\":10227,\\\"account_id\\\":2,\\\"account_subdomain\\\":\\\"dev\\\",\\\"action\\\":\\\"request_deletion\\\",\\\"application\\\":\\\"all\\\",\\\"executer_id\\\":10012,\\\"development_namespace\\\":\\\"drhenner\\\"}]\",\n
        #      \"Timestamp\" : \"2018-02-21T19:05:01.043Z\",\n
        #      \"SignatureVersion\" : \"1\",\n
        #      \"Signature\" : \"MgNRIeK1eF42dunsJTHPQR+bHf7X0RVajN/KSraLvzNeMdNYSf1otRTFb2v57Dy//iqYtYWwR2B0ae658r/4rSDg8vEzH7V93KwZkv6IzQDv0mTQpeWoAUe11wA6MFaZSrLmVyQfbXwfCt66dN/qsDCBU76wrWRpQug5hDaY+Forl7s3UHwHCcP8gN/Il1rM/XVzPrnS20LuHdf18Mpi6ZIDw5Rj0RmJrMQ469+oJlPMhJy/jd9S2WigJRW5TXeQa+K9lIA+ZAdhAXkCUks2ymZAdFa8u04ol7166jFvfxBDNzTf/bvN5S7MsPEj6WM53TflYWa0gNP7gxgjB0BhOQ==\",\n
        #      \"SigningCertURL\" : \"https://sns.us-west-2.amazonaws.com/SimpleNotificationService-433026a4050d206028891664da859041.pem\",\n
        #      \"UnsubscribeURL\" : \"https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:724781030999:staging_us_gdpr_topic:c1931acf-f965-4f44-9344-e0236d23545d\"\n}",
        #   attributes={"SenderId"=>"AIDAIYLAVTDLUXBIEIX46", "ApproximateFirstReceiveTimestamp"=>"1519240499311", "ApproximateReceiveCount"=>"2", "SentTimestamp"=>"1519239901134"}, md5_of_message_attributes=nil, message_attributes={}>, [{"user_id"=>10227, "account_id"=>2, "account_subdomain"=>"dev", "action"=>"request_deletion", "application"=>"all", "executer_id"=>10012, "development_namespace"=>"drhenner"}]]
        #
        #
        # Note: max_number_of_messages is set to 10, which is the max allowed https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-limits.html#limits-queues
        def receive_next_messages
          sqs_messages = sqs_client.receive_message(queue_url: @sqs_support_queue_url,
                                                    attribute_names: ["All"],
                                                    message_attribute_names: ["All"],
                                                    max_number_of_messages: 10,
                                                    visibility_timeout: 30.minutes.to_i,
                                                    wait_time_seconds: 20).messages

          parsed_messages(sqs_messages)
        end

        private

        def parsed_messages(sqs_messages)
          messages = sqs_messages.map do |message|
            json_message = parse_message(message)
            next unless json_message
            [message, json_message]
          end.compact

          messages.present? || sqs_messages.blank? ? messages : receive_next_messages
        end

        def parse_message(message)
          json_message = JSON.parse(JSON.parse(message.body)["Message"])
          json_message = json_message['message'] if json_message.keys.include?("message")
          json_message
        rescue JSON::ParserError
          Rails.logger.debug(
            "Removing malformed message, message: #{message}"
          )
          delete_message(message)
          nil
        end

        def sqs_client
          @sqs_client ||= Aws::SQS::Client.new(
            region: @region,
            credentials: credentials
          )
        end

        def credentials
          Aws::Credentials.new(@access_key, @secret_key, @session_token)
        end
      end
    end
  end
end
