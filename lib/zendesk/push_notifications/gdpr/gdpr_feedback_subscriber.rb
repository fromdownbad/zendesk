module Zendesk
  module PushNotifications
    module Gdpr
      # Push GDPR requests to Amazon SNS for Push Notification actions
      class GdprFeedbackSubscriber < GdprSubscriber
        RETRYABLE_ERRORS = [
          GlobalUid::ConnectionTimeoutException,
          GlobalUid::NoServersAvailableException,
          GlobalUid::TimeoutException,
          Redis::BaseConnectionError,
          Redis::CommandError,
          Seahorse::Client::NetworkingError,
          Zendesk::Accounts::Locking::LockedException
        ].freeze

        # Require that subscriber.receive_next_message should return in the following format:
        #
        #    json_message = {
        #      user_id:           user_id,
        #      account_id:        account_id,
        #      account_subdomain: subdomain,
        #      action:            action,
        #      application:       application,
        #    }.to_json
        #
        #  subscriber.receive_next_message => raw_message_from_the_data_queue, json_message
        #
        # @return number of messages processed
        def process_queue
          messages = receive_next_messages
          current_pod = Zendesk::Configuration.fetch(:pod_id).to_i

          # if there aren't messages then return false to stop processing
          return 0 if !messages || messages.empty?

          # messages that will not be deleted from the queue so they are retried
          # after 10 unsuccessful retries they move to the dead-letter queue
          retry_messages = []

          messages.each do |next_message|
            full_message, json_message = next_message

            catch_and_report_errors(json_message, full_message, false) do
              begin
                next unless valid_message?(json_message)

                # account will be nil if the account doesn't exist on this pod (hence ignore request)
                if !json_message['pod'] || json_message['pod'].to_i == current_pod
                  account = Account.find_by_id(json_message['account_id'])
                  if account && account.pod_local? && allowed_transaction?(json_message)
                    account.on_shard do
                      process_message(account, full_message, json_message)
                    end
                  end
                end
              rescue ActiveRecord::StatementInvalid => e
                if e.is_a?(ZendeskDatabaseSupport::MappedDatabaseExceptions::TemporaryException) ||
                  e.message.start_with?("Mysql2::Error: Can't connect to") ||
                  e.message.start_with?('Mysql2::Error: Unknown MySQL server host')
                  retry_messages << next_message
                  instrument_retry(exception: e, json_message: json_message)
                else
                  raise e
                end
              rescue *RETRYABLE_ERRORS => e
                retry_messages << next_message
                instrument_retry(exception: e, json_message: json_message)
              end
            end
          end

          messages_to_delete = messages - retry_messages
          batch_delete_messages(messages_to_delete) unless messages_to_delete.empty?

          # processed messages
          messages.size
        end

        private

        def instrument_retry(exception:, json_message:)
          statsd_client.increment('retry_feedback', tags: ["error_type:#{exception.class.name}", "account_id:#{json_message['account_id']}"])
          log_msg = "Retrying feedback message #{json_message}. Error:#{exception.message}"
          Rails.logger.info(log_msg)
        end

        def valid_message?(json_message)
          application = json_message['application']
          action = json_message['action']

          if ComplianceDeletionStatus::APPLICATONS.exclude?(application)
            statsd_client.increment('invalid_message', tags: ['reason:invalid_application', "action:#{action}", "application:#{application}"])
            false
          elsif action != ComplianceDeletionFeedback::COMPLETE
            statsd_client.increment('invalid_message', tags: ['reason:invalid_action', "action:#{action}", "application:#{application}"])
            false
          else
            true
          end
        end

        def statsd_client
          @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['gdpr', 'gdpr_feedback_subscriber'])
        end

        def process_message(account, full_message, json_message)
          user = user_from_message(account, json_message)
          application = json_message['application']

          initial_request = ComplianceDeletionStatus.where(account: account, user: user).first

          msg = "compliance deletion status record is missing for account:#{account.idsub}, user:#{user.id}"
          raise ComplianceDeletionError, msg if initial_request.nil?

          update_feedback_record(
            account: account,
            application: application,
            full_message: full_message,
            initial_request: initial_request
          )
        end

        def statsd_feedback_counter(application:, duplicate:)
          # 'action' can now only be 'complete'. However, we need to keep it as a tag
          # so we don't break existing DataDog monitors and dashboards
          tags = [
            "application:#{application}",
            "action:#{ComplianceDeletionFeedback::COMPLETE}",
            "repeated:#{duplicate}"
          ]

          statsd_client.increment('record_feedback', tags: tags)
        end

        def update_feedback_record(account:, application:, full_message:, initial_request:)
          # use `find` here instead of a `where` clause to load all feedback to
          # avoid additional query in the try_completing_request check
          app_feedback = initial_request.compliance_deletion_feedback.find do |feedback|
            feedback.application == application
          end

          if app_feedback.nil?
            msg = "feedback record is missing for application:#{application}, account:#{account.idsub}, user:#{initial_request.user_id}"
            raise ComplianceDeletionError, msg
          end

          duplicate_feedback = app_feedback.complete?

          # converting epoch time in milliseconds to ruby time
          message_received_time = Time.at(full_message.attributes['SentTimestamp'].to_i / 1000)

          unless duplicate_feedback
            # if pod_ids don't match this means the account moved and we need to redo the request
            if account.pod_id == app_feedback.pod_id
              app_feedback.complete!
              try_completing_request(initial_request) if app_feedback.required?
            else
              app_feedback.redo!
              initial_request.publish_to_incomplete!([application])
              statsd_client.increment('redo_request', tags: ["application:#{application}"])
            end

            start_timestamp = app_feedback.redo? ? app_feedback.updated_at : app_feedback.created_at

            instrument_application_response_time(
              application: application,
              is_redo: duplicate_feedback ? false : app_feedback.redo?,
              received_time: message_received_time,
              start_timestamp: start_timestamp
            )
          end

          instrument_time_to_process_feedback(application: application, received_time: message_received_time)

          statsd_feedback_counter(application: application, duplicate: duplicate_feedback)
        end

        def try_completing_request(initial_request)
          all_complete = initial_request.compliance_deletion_feedback.all? do |feedback|
            feedback.required? ? feedback.complete? : true
          end

          initial_request.complete! if all_complete
        end

        # How long it takes us to process messages in the SQS feedback queue
        def instrument_time_to_process_feedback(application:, received_time:)
          time_to_process_message = (Time.now - received_time).to_i

          # 'action' can now only be 'complete'. However, we need to keep it as a tag
          # so we don't break existing DataDog monitors and dashboards
          tags = [
            "action:#{ComplianceDeletionFeedback::COMPLETE}",
            "application:#{application}"
          ]

          statsd_client.distribution('time_to_process_message', time_to_process_message, tags: tags)
        end

        # How long it takes applications to send feedback to SNS feedback topic
        def instrument_application_response_time(application:, is_redo:, received_time:, start_timestamp:)
          time_to_feedback = (received_time - start_timestamp).to_i

          # 'action' can now only be 'complete'. However, we need to keep it as a tag
          # so we don't break existing DataDog monitors and dashboards
          tags = [
            "action:#{ComplianceDeletionFeedback::COMPLETE}",
            "application:#{application}",
            "redo:#{is_redo}",
            "sla_tag:#{ComplianceDeletionStatus.sla_tag(time_to_feedback)}"
          ]

          statsd_client.distribution('worker_time_to_feedback', time_to_feedback, tags: tags)
        end
      end
    end
  end
end
