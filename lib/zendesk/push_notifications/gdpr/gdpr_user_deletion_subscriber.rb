module Zendesk
  module PushNotifications
    module Gdpr
      # Push GDPR requests to Amazon SNS for Push Notification actions
      class GdprUserDeletionSubscriber < GdprSubscriber
        # Require that subscriber.receive_next_message should return in the following format:
        #
        #    json_message = {
        #      user_id:           user_id,
        #      account_id:        account_id,
        #      account_subdomain: subdomain,
        #      action:            action,
        #      application:       application,
        #      executer_id:       executer_id
        #    }.to_json
        #
        #  subscriber.receive_next_message => raw_message_from_the_data_queue, json_message
        #
        # @return number of messages processed
        def process_queue
          if Arturo.feature_enabled_for_pod?(:batch_process_gdpr_deletion_requests, Zendesk::Configuration.fetch(:pod_id))
            # Process the messages in batch
            messages = receive_next_messages
            # no more messages to process
            return 0 if !messages || messages.empty?

            messages.each do |(full_message, json_message)|
              process_message full_message, json_message
            end

            # processed messages
            messages.size
          else
            full_message, json_message = receive_next_message
            # no more messages to process
            return 0 if json_message.nil?

            process_message full_message, json_message

            # only one message processed
            1
          end
        end

        private

        def process_message(full_message, json_message)
          catch_and_report_errors(json_message, full_message) do
            # if the message is for 'talk'/'chat'/... classic would never process it on any pod
            unless message_for_classic?(json_message)
              delete_message(full_message)
              return true
            end

            account = Account.find_by_id(json_message['account_id'])
            if account && account.pod_local? && allowed_transaction?(json_message)
              account.on_shard do
                if account.all_users.exists?
                  delete_user_from_support(account, json_message)
                end
              end
            end

            delete_message(full_message)
          end
        end

        def message_for_classic?(message)
          message['application'] == ComplianceDeletionStatus::ALL_APPLICATONS ||
          message['application'] == ComplianceDeletionStatus::CLASSIC
        end

        def delete_user_from_support(account, message)
          options = {
            action: ComplianceDeletionFeedback::COMPLETE,
            application: ComplianceDeletionStatus::CLASSIC
          }
          user = user_from_message(account, message)
          user.current_user ||= executer_from_message(account, user, message)
          if user.ultra_delete!(options)
            statsd_record_user_deletion_completed_from_support(user, account)
          else
            raise ComplianceDeletionError, "Ultra Delete failed for user_id #{user.id}, #{user.errors.messages}"
          end
        end

        # TODO: should deprecate this in favor of a similar metric emitted by GDPR Orchestration Service
        # See https://zendesk.atlassian.net/wiki/spaces/SPY/pages/779065683/GDPR+Classic+Orchestration+metrics#GDPR(Classic)Orchestrationmetrics-Workerefficiency
        def statsd_record_user_deletion_completed_from_support(user, account)
          initial_status_request = ComplianceDeletionStatus.where(
            account: account,
            user: user
          ).first
          if initial_status_request.nil?
            Rails.logger.warn "Cannot find compliance deletion status initial request in #statsd_record_user_deletion_completed_from_support. account_id: #{account.id}, user_id: #{user.id}"
            return
          end
          time_to_complete_deletion = Time.now - initial_status_request.created_at
          self.class.statsd_client.distribution('time_to_complete_deletion', time_to_complete_deletion.to_i, tags: ["account_id:#{account.id}"])
        end
      end
    end
  end
end
