module Zendesk
  module PushNotifications
    module Gdpr
      # Push GDPR requests to Amazon SNS for Push Notification actions
      class GdprSnsUserDeletionPublisher < GdprSnsPublisher
        # topic_arn => the identifier for the SNS service you are pushing a message to
        # region    => AWS region where SNS exists  (like us-west-2)
        # access_key => public AWS identifier
        # secret_key => Secret AWS identifier
        # session_token => Optional. Used in dev environment for assumed role.
        # sns_endpoint => Optional. Used in dev environment to target a local/mocked sns service (like localstack)
        def initialize(topic_arn: nil, region: nil, access_key: nil, secret_key: nil, session_token: nil, sns_endpoint: nil)
          @topic_arn     = topic_arn     || Zendesk::Gdpr::Configuration.fetch(:topic_arn)
          @region        = region        || Zendesk::Gdpr::Configuration.fetch(:region)
          @access_key    = access_key    || Zendesk::Gdpr::Configuration.dig(:writer_aws_access_key) || Zendesk::Gdpr::Configuration.fetch(:aws_access_key)
          @secret_key    = secret_key    || Zendesk::Gdpr::Configuration.dig(:writer_aws_secret_key) || Zendesk::Gdpr::Configuration.fetch(:aws_secret_key)
          @session_token = session_token || Zendesk::Gdpr::Configuration.dig(:aws_session_token)
          @sns_endpoint  = sns_endpoint  || Zendesk::Gdpr::Configuration.dig(:sns_endpoint)
        end

        def publish(message:, subject: "GDPR Request")
          super
        end
      end
    end
  end
end
