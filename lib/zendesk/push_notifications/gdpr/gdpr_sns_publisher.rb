require 'aws-sdk-sns'

module Zendesk
  module PushNotifications
    module Gdpr
      # Push GDPR requests to Amazon SNS for Push Notification actions
      class GdprSnsPublisher
        # topic_arn => the identifier for the SNS service you are pushing a message to
        # region    => AWS region where SNS exists  (like us-west-2)
        # access_key => public AWS identifier
        # secret_key => Secret AWS identifier
        # session_token => Optional. Used in dev environment for assumed role.
        # sns_endpoint => Optional. Used in dev environment to target a local/mocked sns service (like localstack)
        def initialize(topic_arn: nil, region: nil, access_key: nil, secret_key: nil, session_token: nil, sns_endpoint: nil)
          @topic_arn  = topic_arn
          @region     = region
          @access_key = access_key
          @secret_key = secret_key
          @session_token = session_token
          @sns_endpoint = sns_endpoint
        end

        def publish(message:, subject: "GDPR Request")
          json_message = add_development_name(message)
          sns_message = {
            default: json_message,
            APNS_SANDBOX: { aps: json_message },
            APNS: { aps: json_message }
          }

          sns_client.publish(
            topic_arn: @topic_arn,
            subject: subject,
            message: JSON::Schema.stringify(sns_message.to_json),
            message_structure: 'json',
            message_attributes: sns_message_attributes(message)
          )
        end

        private

        def sns_message_attributes(message)
          {
            application: { string_value: message[:application], data_type: "String" },
            pod: { string_value: "POD#{message[:pod]}", data_type: "String" },
            action: { string_value: message[:action], data_type: "String" },
            account_subdomain: { string_value: message[:account_subdomain], data_type: "String" }
          }
        end

        def sns_client
          sns_client_options = {
              region: @region,
              access_key_id: @access_key,
              secret_access_key: @secret_key,
              session_token: @session_token
          }
          sns_client_options[:endpoint] = @sns_endpoint if @sns_endpoint
          @sns_client ||= Aws::SNS::Client.new(sns_client_options)
        end

        # Used in development to prevent data collisions between developers
        def add_development_name(message)
          message = if Rails.env.development?
            message.merge!(development_namespace: Zendesk::Gdpr::Configuration.fetch(:development_namespace))
          else
            message
          end
          JSON::Schema.stringify(message.to_json)
        end
      end
    end
  end
end
