module Zendesk
  module PushNotifications
    module Gdpr
      # Push GDPR requests to Amazon SNS for Push Notification actions
      class GdprUserDeletionPublisher < GdprPublisher
        def publish
          return unless account.is_active? # inactive accounts eventually get wiped from the DB
          publisher.publish(
            subject: "GDPR Request",
            message: user_message
          )
        end

        private

        def user_message
          @user_message ||= {
            user_id: user_id,
            account_id: account_id,
            account_subdomain: account.subdomain,
            action: ComplianceDeletionStatus::REQUEST_DELETION,
            application: ComplianceDeletionStatus::ALL_APPLICATONS,
            executer_id: executer_id,
            pod: account.pod_id
          }
        end
      end
    end
  end
end
