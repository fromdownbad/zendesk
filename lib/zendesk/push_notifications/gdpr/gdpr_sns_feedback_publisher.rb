module Zendesk
  module PushNotifications
    module Gdpr
      # Push GDPR requests to Amazon SNS for Push Notification actions
      class GdprSnsFeedbackPublisher < GdprSnsPublisher
        def initialize(topic_arn: nil, region: nil, access_key: nil, secret_key: nil, session_token: nil)
          @topic_arn     = topic_arn     || Zendesk::Gdpr::Configuration.fetch(:audit_feedback_topic_arn)
          @region        = region        || Zendesk::Gdpr::Configuration.fetch(:region)
          @access_key    = access_key    || Zendesk::Gdpr::Configuration.dig(:writer_aws_access_key) || Zendesk::Gdpr::Configuration.fetch(:aws_access_key)
          @secret_key    = secret_key    || Zendesk::Gdpr::Configuration.dig(:writer_aws_secret_key) || Zendesk::Gdpr::Configuration.fetch(:aws_secret_key)
          @session_token = session_token || Zendesk::Gdpr::Configuration.dig(:aws_session_token)
        end

        def publish(message:, subject: "GDPR Feedback")
          super
        end
      end
    end
  end
end
