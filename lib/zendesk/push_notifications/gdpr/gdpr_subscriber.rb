module Zendesk
  module PushNotifications
    module Gdpr
      # Push GDPR requests to Amazon SNS for Push Notification actions
      class GdprSubscriber
        attr_reader :subscriber

        GDPR_WORKER_MAX_PROCESS_TIME_MINUTES = Integer(ENV.fetch('GDPR_WORKER_MAX_PROCESS_TIME_MINUTES', 5))

        delegate :receive_next_message, :receive_next_messages, :delete_message, :batch_delete_messages, to: :subscriber
        # subscriber is an instance of GdprSqsSubscriber for subscribing to SQS
        def initialize(subscriber)
          @subscriber = subscriber
        end

        def process_queues
          number_of_messages = 0
          max_number_of_messages = 25_000
          loop do
            processed_messages = process_queue
            break if processed_messages.zero? # if process_queue doesn't return a total of messages processed, we are done
            number_of_messages += processed_messages
            break if number_of_messages > max_number_of_messages # ensure we don't loop infinitely
            break if Time.now > max_process_age # if we run more than 30 minutes kill the process
          end
        end

        # Require that subscriber.receive_next_message should return in the following format:
        #
        #    json_message = {
        #      user_id:           user_id,
        #      account_id:        account_id,
        #      account_subdomain: subdomain,
        #      action:            action,
        #      application:       application,
        #      executer_id:       executer_id
        #    }.to_json
        #
        #  subscriber.receive_next_message => raw_message_from_the_data_queue, json_message
        #
        # @return number of messages processed
        def process_queue
          raise 'Implement Me'
        end

        def self.statsd_client
          @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['gdpr', 'subscriber'])
        end

        protected

        def max_process_age
          @max_process_age ||= GDPR_WORKER_MAX_PROCESS_TIME_MINUTES.minutes.from_now
        end

        def catch_and_report_errors(json_message, full_message, delete_errored_sqs_message = true)
          yield
        rescue StandardError => e
          class_name = self.class.name.demodulize
          message = "Deletion request from user_id #{json_message['user_id']} account_id #{json_message['account_id']} failed due #{e.message}"
          self.class.statsd_client.event("Delete message can not be processed", message, alert_type: 'warning', tags: ["class:#{class_name}"])
          self.class.statsd_client.increment('failed_deletions', tags: ["class:#{class_name}", "error_type:#{e.class}", "account_id:#{json_message['account_id']}"])
          message_to_report = error_message(json_message, e)
          Rails.logger.warn(message_to_report)
          ZendeskExceptions::Logger.record(e, location: self, message: message_to_report, fingerprint: '7e4c21c6a8d417c15b8835998f0fe92732316ab2')
          if delete_errored_sqs_message
            delete_message(full_message) # delete messages that aren't processable
          end
        end

        def error_message(json_message, e)
          "Error in #{self.class.name.demodulize} processing message: #{json_message} - #{e.message}"
        end

        def user_from_message(account, json_message)
          user_id = json_message['user_id']
          user = account.all_users.find_by_id(user_id)
          if user.nil?
            raise ComplianceDeletionError, "Can't find the user that is being logged for deletion. user_id: #{user_id}"
          elsif user.is_active?
            raise ComplianceDeletionError, "Can't delete a user that is active. user_id: #{user_id}"
          end

          user
        end

        def executer_from_message(account, user, json_message)
          executer = account.users.find_by_id(json_message['executer_id'])
          if executer.present? && executer.can?(:delete, user)
            executer
          else
            account.owner
          end
        end

        private

        # In development we are using a namespace defined in gdpr.yml
        # to ensure developers do not smash on each other, each developer
        # should assign a unique key to `development_namespace`
        def allowed_transaction?(message)
          return true unless Rails.env.development?
          return false if message['development_namespace'].blank?
          return false if message['development_namespace'] == 'your_github_username'
          return true if message['development_namespace'] == Zendesk::Gdpr::Configuration[:development_namespace]
          false
        end
      end
    end
  end
end
