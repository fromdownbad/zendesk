module Zendesk
  module PushNotifications
    module Payloads
      module Helper
        ELLIPSIS = "\u2026".freeze
        NEW_LINE_PATTERN = /(\r\n)+|(\n)+/m

        private

        def truncate_message(message, payload, max_payload_size)
          new_message = replace_newlines_with_space(message)
          max_msg_size = calc_max_msg_size(max_payload_size, payload)
          truncate(new_message, max_msg_size)
        end

        def calc_max_msg_size(max_payload_size, payload)
          max_payload_size - payload.to_json.bytesize
        end

        def replace_newlines_with_space(message)
          message.gsub(NEW_LINE_PATTERN, ' ')
        end

        # Push notification services has limitations on requested characters
        # count in byte size. This method truncates the given message to respect
        # the push notification bytesize limitation. Also, replaces the last
        # word of the truncated message with the elipis chars.
        def truncate(msg, max_msg_size)
          msg = msg.truncate(max_msg_size)
          available_bytes = max_msg_size - serialization_overhead(msg)
          msg.each_char.each_with_object('') do |chr, truncated_message|
            if (truncated_message + chr).bytesize >= (available_bytes - 2)
              return truncated_message.gsub(/\s\w+\s*$/, ELLIPSIS)
            end
            truncated_message.concat(chr)
          end
        end

        def serialization_overhead(truncated)
          truncated.to_json.size - truncated.size - ''.to_json.size
        end
      end
    end
  end
end
