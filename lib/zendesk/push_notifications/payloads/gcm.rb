module Zendesk
  module PushNotifications
    module Payloads
      # GCM push payload
      class GCM
        MAX_PAYLOAD_SIZE = 1024
        include Zendesk::PushNotifications::Payloads::Helper

        def initialize(alert:, badge:, extra_payload:)
          @alert = alert
          @badge = badge
          @extra_payload = extra_payload
        end

        def payload
          empty_payload = generate_payload('')
          @alert = truncate_message(@alert, empty_payload, MAX_PAYLOAD_SIZE)
          generate_payload(@alert)
        end

        private

        def generate_payload(msg)
          gcm_payload = @extra_payload[:enable_collapse_key] ? {collapse_key: Time.now.to_f.to_s} : {}
          gcm_payload[:data] = {
            alert: msg,
            extra: {
              tid: @extra_payload[:ticket_id]
            }
          }
          gcm_payload
        end
      end
    end
  end
end
