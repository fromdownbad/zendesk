module Zendesk
  module PushNotifications
    module Payloads
      # APNS push payload
      class APNS
        MAX_PAYLOAD_SIZE = 256
        SOUND = 'notification.aif'.freeze
        include Zendesk::PushNotifications::Payloads::Helper

        def initialize(alert:, badge:, extra_payload:)
          @alert = alert
          @badge = badge
          @extra_payload = extra_payload
        end

        def payload
          @alert = truncate_message(@alert, generate_payload, MAX_PAYLOAD_SIZE)
          generate_payload(@alert)
        end

        private

        def generate_payload(msg = '')
          {
            aps: {
              alert: msg,
              badge: @badge,
              sound: SOUND
            },
            id: @extra_payload[:user_id],
            tid: @extra_payload[:ticket_id]
          }
        end
      end
    end
  end
end
