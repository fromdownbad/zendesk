require 'aws-sdk-sns'

module Zendesk
  module PushNotifications
    module Adapters
      # Amazon SNS Adapter for Push Notification actions
      class SNS
        IPHONE = 'iphone'.freeze
        IPAD = 'ipad'.freeze
        IOS = 'ios'.freeze
        ANDROID = 'android'.freeze
        NAME = 'amazon_sns'.freeze
        FAILURE = 'failure'.freeze
        SUCCESS = 'success'.freeze
        MESSAGE_STRUCTURE = 'json'.freeze
        PUSH_SERVICE_KEYS = { IOS => 'APNS', ANDROID => 'GCM'}.freeze

        attr_reader :statsd_tags, :region

        def initialize(options = {})
          @region, access_key_id, secret_access_key = fetch_config
          @client = Aws::SNS::Client.new(
            region: @region,
            access_key_id: access_key_id,
            secret_access_key: secret_access_key
          )
          @statsd_tags = options.fetch(:statsd_tags, [])
        end

        def register(mobile_app_identifier:, device_type:, device_identifier_id:, device_token:)
          platform_application_arn = find_platform_application_arn(mobile_app_identifier, device_type)
          resp = @client.create_platform_endpoint(
            platform_application_arn: platform_application_arn,
            token: device_token
          )

          Zendesk::PushNotifications::Hooks::SNS.after_register_success(
            device_identifier_id, resp.endpoint_arn, @statsd_tags
          )
        rescue => exception
          Zendesk::PushNotifications::Hooks::SNS.after_register_failure(
            device_identifier_id, exception, @statsd_tags
          )
        end

        def unregister(device_identifier_id:, endpoint_arn:)
          @client.delete_endpoint(
            endpoint_arn: endpoint_arn
          )

          Zendesk::PushNotifications::Hooks::SNS.after_unregister_success(
            device_identifier_id, @statsd_tags
          )
        rescue => exception
          Zendesk::PushNotifications::Hooks::SNS.after_unregister_failure(
            device_identifier_id, exception, @statsd_tags
          )
        end

        def push(device_type:, device_identifier_id:, endpoint_arn:, payload:)
          os_type = find_os_type(device_type)
          return if endpoint_arn.blank?
          return unless os_type == IOS
          resp = @client.publish(
            target_arn: endpoint_arn,
            message: { PUSH_SERVICE_KEYS[os_type] => payload.to_json }.to_json,
            message_structure: MESSAGE_STRUCTURE
          )

          Zendesk::PushNotifications::Hooks::SNS.after_push_success(
            device_identifier_id, resp.message_id, @statsd_tags
          )
        rescue => exception
          Zendesk::PushNotifications::Hooks::SNS.after_push_failure(
            device_identifier_id, exception, @statsd_tags
          )
        end

        private

        def find_os_type(device_type)
          case device_type
          when IPHONE, IPAD
            IOS
          when ANDROID
            ANDROID
          end
        end

        def find_platform_application_arn(mobile_app_identifier, device_type)
          os_type = find_os_type(device_type)
          push_service = PUSH_SERVICE_KEYS[os_type]
          find_app_arn(push_service, mobile_app_identifier)
        end

        # Amazon Platform(App) ARN responses with in a format like
        # 'arn:aws:sns:eu-west-1:000000000000:app/APNS/com.zendesk.agent'
        # To register a device to AMAZON SNS, we need app arn value.
        # This helper method matches and finds the app arn for given
        # push_service and mobile_app_identifier.
        # Note: Null case is handled automatically on register method.
        def find_app_arn(push_service, mobile_app_identifier)
          suffix = "#{push_service}/#{mobile_app_identifier}"
          suffix_length = suffix.length
          app_arns.find do |app_arn|
            app_arn_length = app_arn.length
            app_arn[app_arn_length - suffix_length, app_arn_length] == suffix
          end
        end

        def app_arns
          Rails.cache.fetch("amazon_sns_app_arns_#{@region}", expires_in: 60.minutes) do
            @client.list_platform_applications.platform_applications.flat_map(&:platform_application_arn)
          end
        end

        def fetch_config
          [
            Zendesk::Configuration.fetch(:classic_aws_sns_region),
            Zendesk::Configuration.fetch(:classic_aws_sns_access_key),
            Zendesk::Configuration.fetch(:classic_aws_sns_secret_key)
          ]
        end
      end
    end
  end
end
