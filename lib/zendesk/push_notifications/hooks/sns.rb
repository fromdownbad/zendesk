require 'aws-sdk-sns'

module Zendesk
  module PushNotifications
    module Hooks
      # Push Notifications after callbacks for SNS adapter
      class SNS
        SERVICE_NAME = 'amazon_sns'.freeze
        STATSD_NAMESPACE = ['jobs', 'push_notifications'].freeze

        class << self
          def after_register_success(device_identifier_id, endpoint_arn, statsd_tags)
            device_identifier = ::PushNotifications::DeviceIdentifier.find(device_identifier_id)
            device_identifier.update_attribute(:sns_target_arn, endpoint_arn)
            increment_metrics(['register:success'], statsd_tags)
          end

          def after_register_failure(device_identifier_id, exception, statsd_tags)
            Rails.logger.error("#{name}.#{__method__} - Inputs: #{{device_identifier_id: device_identifier_id}} - #{exception}")
            increment_metrics(['register:failure'], statsd_tags)
          end

          def after_unregister_success(device_identifier_id, statsd_tags)
            device_identifier = ::PushNotifications::DeviceIdentifier.find(device_identifier_id)
            device_identifier.destroy
            increment_metrics(['unregister:success'], statsd_tags)
          end

          def after_unregister_failure(device_identifier_id, exception, statsd_tags)
            Rails.logger.error("#{name}.#{__method__} - Inputs: #{{device_identifier_id: device_identifier_id}} - #{exception}")
            increment_metrics(['unregister:failure'], statsd_tags)
          end

          def after_push_success(device_identifier_id, message_id, statsd_tags)
            device_identifier = ::PushNotifications::DeviceIdentifier.find(device_identifier_id)
            device_identifier.update_attribute(:badge_count, device_identifier.badge_count + 1)

            Rails.logger.info("#{name}.#{__method__} - Message id: #{message_id}")
            increment_metrics(['push:success'], statsd_tags)
          end

          def after_push_failure(device_identifier_id, exception, statsd_tags)
            case exception
            when Aws::SNS::Errors::EndpointDisabled
              device_identifier = ::PushNotifications::DeviceIdentifier.find(device_identifier_id)
              device_identifier.destroy
              Rails.logger.error("#{name}.#{__method__} - Device with id##{device_identifier_id} deleted or disabled push notifications: #{exception}")
            else
              Rails.logger.error("#{name}.#{__method__} - Inputs: #{{device_identifier_id: device_identifier_id}} - #{exception}")
            end
            increment_metrics(['push:failure'], statsd_tags)
          end

          def increment_metrics(action_tags, statsd_tags)
            tags = action_tags + statsd_tags
            statsd_client = ::Zendesk::StatsD::Client.new(namespace: STATSD_NAMESPACE)
            statsd_client.increment(SERVICE_NAME, tags: tags)
          rescue
            Rails.logger.error("#{name} could not send Amazon SNS push notification job data to statsd")
          end
        end
      end
    end
  end
end
