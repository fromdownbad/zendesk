module Zendesk
  module Home
    module ControllerSupport
      include Zendesk::SharedControllerSupport

      def self.included(base)
        base.extend(Filters)
      end

      protected

      module Filters
        def allow_anonymous_access_to_public_forums
          allow_anonymous_users if: :public_forums?
        end

        def cache_index_action
          caches_action :index, if: :is_action_cache_safe?,
                                cache_path: proc { |c| c.send(:action_cache_key_for_index) },
                                expires_in: 1.hour
        end
      end

      def action_cache_key_for_index
        action_cache_key(
          forums: current_account.scoped_cache_key(:forums),
          permissions: current_user.permissions,
          locale: ::I18n.locale
        )
      end

      def public_forums?
        current_account.has_public_forums?
      end
    end
  end
end
