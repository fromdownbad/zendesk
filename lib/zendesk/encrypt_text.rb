# Adapted From: string-encrypt GEM (added Base64 encoding)

#
# == Author
# Branden Giacoletto, Carbondata Digital Services
#
# == Copyright
# Copyright (c) 2009 Carbondata Digital Services
# Licensed under the same terms as Ruby

require 'openssl'
require 'base64'

# Opens the String class providing encryption and zipping methods
module Zendesk::EncryptText
  CYPHER = "aes-256-cbc".freeze
  MAGIC = "bcf83841".freeze # "Salted__"
  SALT_LEN = 8

  class << self
    # decrypts the string using either and string password
    def decrypt(password, input)
      Encryptor.new(password).decrypt_and_verify(input)
    rescue ActiveSupport::MessageEncryptor::InvalidMessage, ActiveSupport::MessageVerifier::InvalidSignature
      deprecated_decrypt(password, input)
    end

    # encrypts the string using either and string password
    def encrypt(password, input)
      Encryptor.new(password).encrypt_and_sign(input)
    end

    private

    def deprecated_decrypt(password, input)
      begin
        not64 = Base64.decode64(input)
        salt = not64[(MAGIC.length)...(SALT_LEN + MAGIC.length)]
        c = OpenSSL::Cipher::Cipher.new(CYPHER)
        c.decrypt
        c.pkcs5_keyivgen(password, salt, 1)
        result = (c.update(not64[SALT_LEN + MAGIC.length..-1]) + c.final)
      rescue
        result = nil
      end

      result
    end
  end

  class Encryptor < ActiveSupport::MessageEncryptor
    def initialize(secret, options = {})
      super
      @verifier = ActiveSupport::MessageVerifier.new(@secret, serializer: NullSerializer, digest: 'SHA256')
    end
  end
end
