require 'active_record/comments'

module Zendesk
  module CommentOnQueries
    def self.included(base)
      base.around_action :comment_on_queries
    end

    protected

    def comment_on_queries(&block)
      return yield unless current_account&.has_comment_on_queries?

      lotus_app = request.headers["X-Zendesk-App-Id"].to_i.nonzero? || "no"

      comment = {
        service: "classic-rails-controller",
        resource_name: "#{self.class.name}##{action_name}",
        code_owner: Codeowner.resolve_controller(self.class),
        account_id: current_account&.id,
        user_id: current_user&.id,
        from_app: lotus_app,
        trace_id: datadog_active_span.try(:trace_id),
        kill_flag: ENV.fetch('ENABLE_QUERY_KILLER', false),
        timeout: ENV.fetch('MYSQL_READ_TIMEOUT', 60)
      }
      ActiveRecord::Comments.comment(comment.to_json, &block)
    end

    def datadog_active_span
      Datadog&.tracer&.active_root_span
    end
  end
end
