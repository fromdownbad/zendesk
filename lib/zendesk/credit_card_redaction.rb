require 'credit_card_sanitizer'

module Zendesk
  module CreditCardRedaction
    extend ActiveSupport::Concern

    mattr_accessor :credit_card_sanitizer
    self.credit_card_sanitizer = CreditCardSanitizer.new

    included do
      cattr_accessor :credit_card_attrs
    end

    def redact_credit_card_numbers!
      return unless account.has_credit_card_sanitization_enabled?
      redacted = false
      options = {
        use_groupings: true,
        exclude_tracking_numbers: true,
        parse_flanking: true
      }
      credit_card_attrs.each do |field|
        text = credit_card_sanitizer.sanitize!(read_attribute(field).to_s, options)
        next if text.blank?

        write_attribute(field, text)
        redacted = true
      end
      redacted
    end

    module ClassMethods
      def credit_card_redaction_attrs(*fields)
        self.credit_card_attrs = fields
      end
    end
  end
end
