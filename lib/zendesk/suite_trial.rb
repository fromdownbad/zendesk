module Zendesk
  class SuiteTrial
    SUPPORT = Accounts::Client::SUPPORT_PRODUCT
    GUIDE = Accounts::Client::GUIDE_PRODUCT
    CHAT = Accounts::Client::CHAT_PRODUCT
    TALK = Accounts::Client::TALK_PRODUCT
    SOCIAL_MESSAGING = Accounts::Client::SOCIAL_MESSAGING_ADDON

    PRODUCTS = [
      SUPPORT,
      GUIDE,
      CHAT,
      TALK,
      SOCIAL_MESSAGING
    ].freeze

    def initialize(account)
      @account = account
    end

    def activate
      # Enable Agent Workspace flags
      @account.activate_agent_workspace_for_trial

      create_or_update_products
      staff_client.update_full_entitlements!(account.owner_id, chat_entitlements_payload) if account.has_ocp_support_shell_account_creation?
      Omnichannel::GuideAccountSyncJob.enqueue(account_id: account.id)
      instrument
    end

    private

    attr_reader :account

    def instrument
      statsd_client = Zendesk::StatsD::Client.new(namespace: ['suite_trial'])

      tags = { 'account.subdomain': account.subdomain }
      tags = tags.to_a.map! { |tag| tag[0].to_s + ":" + tag[1].to_s }

      statsd_client.increment("activated", tags: tags)
    end

    def chat_entitlements_payload
      { 'chat' => { 'name' => 'admin' } }
    end

    def create_or_update_products
      PRODUCTS.each do |product_name|
        create_or_update_product_with_conflict_resolution(product_name)
      end
    end

    # If there is a conflicting change,
    # re-fetch the products and try again;
    # only 1 retry per product
    def create_or_update_product_with_conflict_resolution(product_name)
      create_or_update_product(product_name)
    rescue Kragle::Conflict, Kragle::PreconditionFailed
      fetch_products
      create_or_update_product(product_name)
    end

    def create_or_update_product(product_name)
      product = find_product(product_name)

      if product
        # If the trial has started in the meanwhile, only set suite to true
        if product.trial? && !product.plan_settings['suite']
          params = { product: { plan_settings: { suite: true } } }
          account_service.update_product!(product_name, params, if_unmodified_since: product.updated_at, context: "suite_trial_update")
        end
      else
        account_service.create_product!(product_name, product_params(product_name), context: "suite_trial_create")
      end
    rescue Kragle::UnprocessableEntity => e
      errors = e.response.body['errors']
      if errors && errors.first['detail'].include?('AccountNotTrialEligible')
        Rails.logger.warn("SuiteTrialJob - #{errors.first['detail']}")
      else
        raise e
      end
    end

    def product_params(product_name)
      case product_name
      when SUPPORT
        Zendesk::Accounts::ProductPayload.for_support(account, suite: true)
      when GUIDE
        Guide::Trial.guide_product_payload(account, suite: true)
      when CHAT
        Zopim::Trial.chat_product_payload(account, suite: true, max_agents: account.subscription.max_agents)
      when TALK
        ::Voice::Trial.product_payload
      when SOCIAL_MESSAGING
        Zendesk::Accounts::ProductPayload.for_social_messaging(account, suite: true)
      end
    end

    def find_product(product_name)
      products.find { |product| product.name.to_s == product_name }
    end

    def products
      @products || fetch_products
    end

    def fetch_products
      @products = account_service.products
    end

    def staff_client
      @staff_client ||= Zendesk::StaffClient.new(account)
    end

    def account_service
      @account_service ||= Accounts::Client.new(
        account.id,
        Accounts::Client::DEFAULT_SUBDOMAIN,
        retry_options: {
          max: 3, # retry up to 3 times
          interval: 1, # 1st retry after 1 second
          backoff_factor: 1.5, # wait time until 2nd retry: 1.5 sec, 3rd retry: 2.25 sec
          exceptions: Accounts::Client::RETRYABLE_ERRORS,
          methods: [] # retry all actions if error is included in RETRYABLE_ERRORS
        },
        timeout: 10
      )
    end
  end
end
