module Zendesk::Organization::SharedLogic
  extend ActiveSupport::Concern

  included do
    # Organization#organization_domains and Organization#organization_emails
    # should return soft-deleted organization domain and email associations
    has_soft_deletion default_scope: false

    belongs_to :account
    belongs_to :organization

    attr_accessible :account, :organization, :value

    before_validation :downcase_value
    before_validation :set_account, if: :organization

    validate :validate_value
    validates_presence_of :account, :organization, :value
    validates_uniqueness_of :value, scope: :organization_id
    validates_lengths_from_database only: :value
  end

  private

  def downcase_value
    self.value = value.downcase if value
  end

  def set_account
    self.account = organization.account
  end

  def validate_value
    raise 'Must be implemented in subclass'
  end
end
