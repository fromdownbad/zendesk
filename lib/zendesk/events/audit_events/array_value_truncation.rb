module Zendesk
  module Events
    module AuditEvents
      module ArrayValueTruncation
        DEFAULT_MAX_VALUE_SIZE = 66535
        TRUNCATED_VALUE_INDICATOR = '...'.freeze
        SERIALIZED_TRUNCATED_VALUE_INDICATOR = YAML.dump(TRUNCATED_VALUE_INDICATOR).freeze

        def self.truncate_value(value, klass)
          return value unless need_to_truncate?(value, klass)
          handle_truncation(value, klass)
        end

        def self.need_to_truncate?(value, klass)
          return false if value.blank?

          serialized_value = YAML.dump(value)
          serialized_value.bytesize >= column_size_limit(klass)
        end

        def self.handle_truncation(value, klass)
          serialized_value = YAML.dump(value)
          # `strip` and `chomp("-")` to avoid the issue in which a trailing "-"
          # YAML delimiter ends up throwing an exception. This happens because
          # Classic uses the `syck` gem for YAML parsing rather than the
          # standard `psych` implementation.
          #
          # e.g.
          # ["user_one@one.com", "user_two@two.com", "user_three@three.com"]
          # With a serialization byte limit of 47, this truncates to:
          # "---\n- user_one@one.com\n- user_two@two.com\n-"
          # Deserializing this with `YAML.load` using the `syck` gem throws
          # the following exception:
          #
          # ArgumentError: syntax error on line 3, col 1: `'
          #   from /bundle/gems/syck-1.3.0/lib/syck.rb:142:in `load'
          #   from /bundle/gems/syck-1.3.0/lib/syck.rb:142:in `load'
          #   ...
          truncated_serialized_value =
            serialized_value[0...column_size_limit(klass) - TRUNCATED_VALUE_INDICATOR.bytesize].strip.chomp("-")

          deserialized_truncated_value = YAML.load(truncated_serialized_value)
          truncated_value_last_item = deserialized_truncated_value.last

          # NOTE: zendesk.event_changes.truncated in DataDog
          statsd_client.increment(:truncated, tags: ["type:#{klass}"])

          # Discard the final item in truncated_serialized_value if it is incomplete
          if value.include?(truncated_value_last_item)
            deserialized_truncated_value + [TRUNCATED_VALUE_INDICATOR]
          else
            deserialized_truncated_value[0...-1] + [TRUNCATED_VALUE_INDICATOR]
          end
        rescue StandardError => exception
          ZendeskExceptions::Logger.record(exception, location: self, message: 'Error in ArrayValueTruncation#truncate_value', fingerprint: 'b0673eade5052bfc963f93dcb64512c08499d35d')
          statsd_client.increment(:error, tags: ["error:#{exception.class}", "type:#{klass}"])
          value
        end

        def self.column_size_limit(klass)
          column = klass.columns_hash.fetch("value")
          Zendesk::Extensions::TruncateAttributesToLimit.limit(column) || DEFAULT_MAX_VALUE_SIZE
        end

        def self.ends_with_truncation_indicator?(value)
          return false if value.nil?
          value.last == TRUNCATED_VALUE_INDICATOR
        end

        def self.statsd_client
          @statsd_client ||= Zendesk::StatsD::Client.new(namespace: :event_changes)
        end

        private_class_method :statsd_client
      end
    end
  end
end
