module Zendesk
  module ExternalPermissions
    class PermissionSetPoliciesHelper
      attr_reader :permissions_service_client

      def initialize(subdomain, user_id, **options)
        @permissions_service_client = Zendesk::ExternalPermissions::PermissionsServiceClient.new(subdomain, user_id, options)
      end

      def policies_url
        "#{permissions_service_client.service_url}#{Zendesk::ExternalPermissions::PermissionsServiceClient::POLICIES_PATH}"
      end

      def create_policy!(permission_set, ps_permissions)
        values = ps_permissions.select { |_resource_scope, enabled| enabled == "1" }
        permissions_service_client.create_policy!(create_request_body(permission_set, values))
      end

      def handle_update!(permission_set, update_permission_set_handler, permissions_service_failure_handler, ps_permissions)
        if permission_set.account.has_permissions_custom_role_policy_dual_save?
          begin
            update_policy_response = update_policy!(permission_set, ps_permissions)
            if update_policy_response.success?
              update_permission_set_handler.call(permission_set)
            else
              permissions_service_failure_handler.call(permission_set)
            end
          rescue Kragle::ResponseError, Kragle::ClientError, Kragle::ServerError, Faraday::ClientError, Faraday::ConnectionFailed, Faraday::TimeoutError
            permissions_service_failure_handler.call(permission_set)
          end
        else
          update_permission_set_handler.call(permission_set)
        end
      end

      def update_policy!(permission_set, ps_permissions)
        policy = get_policy!(permission_set)
        if policy.nil?
          create_policy!(permission_set, ps_permissions)
        else
          result = {}
          policy_permissions = policy_service_permissions(permission_set)
          ps_permissions.each do |resource_scope, enabled|
            if !policy_permissions[resource_scope].nil? && policy_permissions[resource_scope] != enabled || enabled == "1"
              result[resource_scope] = ps_permissions[resource_scope] == "1"
            end
          end
          permissions_service_client.update_policy!(policy.id, create_request_body(permission_set, result))
        end
      end

      def create_request_body(permission_set, values)
        statements = values.map do |resource_scope, enabled|
          resource_scopes = Access::ExternalPermissions::Permissions::EXTERNAL_PERMISSIONS_MAP[resource_scope].split(':')
          {"effect": enabled ? "allow" : "deny",
           "resource": resource_scopes[0],
           "scopes": [resource_scopes[1]] }
        end.compact
        { policy: { name: permission_set.name, subject: { id: permission_set.id.to_s, type: 'custom_role' }, statements: statements } }
      end

      def get_policy!(permission_set)
        @policy ||= begin
          get_response = permissions_service_client.get_policies!({subject_id: permission_set.id.to_s, subject_type: 'custom_role'})
          if get_response.success?
            policies = get_response.body['policies']
            unless policies.empty?
              Access::ExternalPermissions::ExternalPolicy.new(policies[0])
            end
          end
        end
      end

      def policy_service_permissions(permission_set)
        policy = get_policy!(permission_set)
        policy.nil? ? {} : policy.statements.each_with_object({}) { |p, hash| p.scopes.each { |s| hash[s + "_" + p.resource] = p.effect == "allow" } }
      end

      def handle_delete!(permission_set, delete_permission_set_handler, permissions_service_failure_handler)
        if permission_set.account.has_permissions_custom_role_policy_dual_save?
          begin
            delete_policy_response = delete_policy!(permission_set)
            if delete_policy_response.nil? || delete_policy_response.success?
              delete_permission_set_handler.call(permission_set)
            else
              permissions_service_failure_handler.call(permission_set)
            end
          rescue Kragle::ResourceNotFound, Faraday::ResourceNotFound
            delete_permission_set_handler.call(permission_set)
          rescue Kragle::ResponseError, Kragle::ClientError, Kragle::ServerError, Faraday::ClientError, Faraday::ConnectionFailed, Faraday::TimeoutError
            permissions_service_failure_handler.call(permission_set)
          end
        else
          delete_permission_set_handler.call(permission_set)
        end
      end

      def delete_policy!(permission_set)
        policy = get_policy!(permission_set)
        permissions_service_client.delete_policy!(policy.id) unless policy.nil?
      end
    end
  end
end
