module Zendesk
  module ExternalPermissions
    class PermissionsServiceClient
      RETRYABLE_ERRORS = [
        Faraday::ConnectionFailed,
        Faraday::TimeoutError
      ].freeze
      SERVICE_NAME = "permissions".freeze
      BASE_PATH = "/api/v2".freeze
      POLICIES_PATH = "#{BASE_PATH}/policies".freeze
      DEFAULT_POLICIES_PATH = "#{POLICIES_PATH}/default".freeze
      POLICY_PATH = ->(id) { "#{POLICIES_PATH}/#{id}" }

      attr_reader :subdomain, :user_id, :circuit_options, :timeout, :retry_options

      def initialize(subdomain, user_id, **options)
        @subdomain = subdomain
        @user_id = user_id
        @circuit_options = options.fetch(:circuit_options, failure_threshold: 10, reset_timeout: 5)
        @timeout = options.fetch(:timeout, 5)
        @retry_options = options.fetch(:retry_options, max: 2, exceptions: RETRYABLE_ERRORS)
      end

      def create_policy!(request_body)
        service_connection.post(POLICIES_PATH, request_body)
      rescue Kragle::ResponseError, Faraday::ClientError => e
        Rails.logger.error("Failed to create policy. Account Subdomain: #{subdomain}, User ID: #{user_id}, Message: #{e.message}\n#{e.backtrace.join('\n ')}")
        raise
      end

      def create_default_policy!(request_body)
        service_connection.post(DEFAULT_POLICIES_PATH, request_body)
      rescue Kragle::ResponseError, Faraday::ClientError => e
        Rails.logger.error("Failed to create default policy. Account Subdomain: #{subdomain}, User ID: #{user_id}, Message: #{e.message}\n#{e.backtrace.join('\n ')}")
        raise
      end

      def get_policies!(query_params = {})
        page_size = query_params.fetch(:page_size, 100)
        page_after = query_params.fetch(:page_after, '')
        subject_id = query_params.fetch(:subject_id, -1)
        subject_type = query_params.fetch(:subject_type, '')
        if subject_id == -1 || subject_type.empty?
          default_uri = "#{POLICIES_PATH}?page[size]=#{page_size}"
          uri = page_after.empty? ? Addressable::URI.encode(default_uri) : Addressable::URI.encode("#{default_uri}&page[after]=#{page_after}")
        else
          uri = "#{POLICIES_PATH}?subject_id=#{subject_id}&subject_type=#{subject_type}"
        end
        service_connection.get(uri)
      rescue Kragle::ResponseError, Faraday::ClientError => e
        Rails.logger.error("Failed to get policies. Account Subdomain: #{subdomain}, User ID: #{user_id}, Message: #{e.message}\n#{e.backtrace.join('\n ')}")
        raise
      end

      def get_policy!(policy_id)
        uri = POLICY_PATH.call(policy_id)
        service_connection.get(uri)
      rescue Kragle::ResponseError, Faraday::ClientError => e
        Rails.logger.error("Failed to get policy. Policy Id: #{policy_id}, Account Subdomain: #{subdomain}, User ID: #{user_id}, Message: #{e.message}\n#{e.backtrace.join('\n ')}")
        raise
      end

      def update_policy!(policy_id, request_body)
        uri = POLICY_PATH.call(policy_id)
        service_connection.put(uri, request_body)
      rescue Kragle::ResponseError, Faraday::ClientError => e
        Rails.logger.error("Failed to update policy. Policy Id: #{policy_id}, Account Subdomain: #{subdomain}, User ID: #{user_id}, Message: #{e.message}\n#{e.backtrace.join('\n ')}")
        raise
      end

      def delete_policy!(policy_id)
        uri = POLICY_PATH.call(policy_id)
        service_connection.delete(uri)
      rescue Kragle::ResponseError, Faraday::ClientError => e
        Rails.logger.error("Failed to delete policy. Policy Id: #{policy_id}, Account Subdomain: #{subdomain}, User ID: #{user_id}, Message: #{e.message}\n#{e.backtrace.join('\n ')}")
        raise
      end

      def service_url
        base_uri = ENV.fetch("PERMISSIONS_SERVICE_URL")
        uri = Addressable::URI.parse(base_uri)
        scheme = uri.scheme.nil? ? 'http' : uri.scheme
        "#{scheme}://#{uri.host}"
      end

      private

      def zendesk_host
        ENV.fetch("ZENDESK_HOST")
      end

      def service_connection
        @service_connection ||= begin
          Kragle.new(SERVICE_NAME, circuit_options: circuit_options, retry_options: retry_options).tap do |connection|
            connection.options.timeout = timeout
            connection.url_prefix = service_url
            connection.headers['Host'] = "#{subdomain}.#{zendesk_host}"
            connection.headers['X-Zendesk-Internal-User'] = user_id.to_s if user_id
          end
        end
      end
    end
  end
end
