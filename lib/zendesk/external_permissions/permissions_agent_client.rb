module Zendesk
  module ExternalPermissions
    class PermissionsAgentClient
      RETRYABLE_ERRORS = [
        Faraday::ConnectionFailed,
        Faraday::TimeoutError
      ].freeze
      SERVICE_NAME = "permissions-agent".freeze
      AUTHORIZE_PATH = "/v1/data/permissions/allow".freeze
      ALLOW_SCOPES_PATH = "/v1/data/permissions/allow_scopes".freeze

      attr_reader :circuit_options, :timeout, :retry_options

      def initialize(**options)
        @circuit_options = options.fetch(:circuit_options, failure_threshold: 10, reset_timeout: 5)
        @timeout = options.fetch(:timeout, 5)
        @retry_options = options.fetch(:retry_options, max: 2, exceptions: RETRYABLE_ERRORS)
      end

      def allow!(request)
        agent_connection.post(AUTHORIZE_PATH, request)
      rescue Kragle::ResponseError, Faraday::ClientError => e
        Rails.logger.error("Failed to authz with permissions_agent. Request: #{request}, Message: #{e.message}\n#{e.backtrace.join('\n ')}")
        raise
      end

      def allow_scopes!(request)
        agent_connection.post(ALLOW_SCOPES_PATH, request)
      rescue Kragle::ResponseError, Faraday::ClientError => e
        Rails.logger.error("Failed to evaluate allowed_scopes with permissions_agent. Request: #{request}, Message: #{e.message}\n#{e.backtrace.join('\n ')}")
        raise
      end

      def request(account, user, resource_scopes)
        {
          input: {
            account: account,
            user: user,
            resource_scopes: resource_scopes
          }
        }
      end

      def agent_url
        ENV.fetch("PERMISSIONS_AGENT_URL")
      end

      private

      def agent_connection
        @agent_connection ||= begin
          Kragle.new(SERVICE_NAME, circuit_options: circuit_options, retry_options: retry_options, signing: false).tap do |connection|
            connection.options.timeout = timeout
            connection.url_prefix = agent_url
          end
        end
      end
    end
  end
end
