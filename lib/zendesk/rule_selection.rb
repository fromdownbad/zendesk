require 'zendesk_search/client'

module Zendesk
  module RuleSelection
    AND_JOIN = ' AND '.freeze
    OR_JOIN  = ' OR '.freeze
  end
end

require 'zendesk/rule_selection/context'
require 'zendesk/rule_selection/filter'
require 'zendesk/rule_selection/macro_usage'
require 'zendesk/rule_selection/option_filter'
require 'zendesk/rule_selection/rule_usage'
require 'zendesk/rule_selection/scope'
require 'zendesk/rule_selection/search'
require 'zendesk/rule_selection/sort'
require 'zendesk/rule_selection/user_filter'
