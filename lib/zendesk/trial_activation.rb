module Zendesk
  class TrialActivation
    def self.activate_support_trial!(account, user = nil)
      new(account, user).perform!
    end

    def self.create_support_product(account, retry_max: 3)
      new(account, nil, retry_max).create_trial_support_product
    end

    def initialize(account, user, retry_max = 3, timeout = 10)
      @account = account
      @user = user
      @retry_max = retry_max
      @timeout = timeout
    end

    def perform!
      @support_product = account_client.product(Zendesk::Accounts::Client::SUPPORT_PRODUCT)

      ::I18n.locale = account.translation_locale # Make sure we provision resources in account locale

      # A few shard db resources depend on Subscription record
      # So we call activate_shard_db_records_in_transaction first
      account.activate_accounts_db_records_in_transaction
      create_not_started_support_product
      account.activate_shard_db_records_in_transaction(user)

      generate_sample_ticket_by_locale

      10.times do
        # This is a hack for Accounts DB replication lag
        sleep(0.1)
        break if account_subscription_exists?
      end

      account.create_guide_plan
      account.start_suite_trial

      TrialLimit.reset_limit_count(account, :user_identity)

      zuora_account_id = zuora_billing_id
      update_support_product_as_trial
      update_subscription_trial_expires_at
      trigger_zuora_sync(zuora_account_id)

      Rails.logger.info("Activated trial for account #{account.id}")
      instrument_trial_activation(tags: ['result:success'])
      true
    rescue StandardError => e
      instrument_trial_activation(tags: ['result:failure', "account:#{account.id}", "exception:#{e.class.name.underscore}"])
      msg = "Failed to activate Support trial for account #{account.try(:idsub) || account.id}: #{e.message}"
      ZendeskExceptions::Logger.record(e, location: self.class, message: msg, fingerprint: '04eabf96b859f99a314836ad5e6572b36341cc58')
      Rails.logger.error("TrialActivation failed for account #{account.try(:idsub) || account.id}: #{e.message}\n#{e.backtrace.join('\n ')}")

      raise e
    end

    def create_trial_support_product
      Rails.logger.info("TrialActivation.create_trial_support_product: creating Support product in account service for #{account.idsub}")

      payload = Zendesk::Accounts::ProductPayload.for_support(account)
      account_client.create_product!('support', payload, context: "trial_activation")
    rescue Kragle::Conflict
      instrument_skip
    end

    private

    attr_reader :account, :user, :retry_max, :timeout, :support_product

    def account_client
      @account_client ||= Zendesk::Accounts::Client.new(
        account,
        retry_options: {
          max: retry_max,
          interval: 2,
          exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS,
          methods: [:post]
        },
        timeout: timeout
      )
    end

    def create_not_started_support_product
      if support_product.nil?
        payload = { product: { state: Zendesk::Accounts::Product::NOT_STARTED } }
        account_client.create_product!('support', payload, context: "trial_activation")
      end
    end

    def agent_workspace_eligibility
      return [false, 'gated'] unless Arturo.feature_enabled_for?(:agent_workspace_for_support_only_trial, account)
      # not needed for suite account
      return [false, 'suite'] if account.settings.suite_trial? && !account.spp?
      # not for essential plan type (should be has_groups, but feature bits may not be available yet)
      return [false, 'essential'] if account.subscription.plan_type == ZBC::Zendesk::PlanType::Essential.plan_type
      # must not have a chat product that's created well before now
      chat_product = account_client.product(Zendesk::Accounts::Client::CHAT_PRODUCT)
      return [false, 'chat'] if chat_product && chat_product.created_at < 1.hour.ago

      [true, 'eligible']
    end

    def activate_agent_workspace
      should_activate, reason = agent_workspace_eligibility
      Rails.logger.append_attributes(
        default_agent_workspace: {
          eligibility: reason,
          activated: should_activate
        }
      )
      account.activate_agent_workspace_for_trial if should_activate
    end

    def update_support_product_as_trial
      if support_product.nil? || support_product.not_started?
        activate_agent_workspace

        payload = Zendesk::Accounts::ProductPayload.for_support(account)
        account_client.update_product!('support', payload, context: "trial_activation")
      end
    end

    def update_subscription_trial_expires_at
      # to keep subscription.trial_expires_on in sync with the product's trial_expires_at value
      if support_product.present? && account.spp?
        account.subscription.trial_expires_on = support_product.trial_expires_at
        account.subscription.save!
      end
    end

    def trigger_zuora_sync(zuora_account_id)
      return unless zuora_account_id.present?

      if ::Billing::OOC::ZuoraParticipator.call(account)
        ZBC::Billing::Zuora::Ping.request(zuora_account_id)
      else
        ZBC::Zuora::Jobs::AccountSyncJob.enqueue(zuora_account_id)
      end
    end

    def zuora_billing_id
      account_client.zuora_billing_id(use_cache: false)
    end

    def account_subscription_exists?
      Subscription.find_by_account_id(account.id).present?
    end

    # Sample ticket for en locale includes an English video.
    # Video & accompanying text is removed for non-english locales
    def generate_sample_ticket_by_locale
      unless Ticket.where(account_id: account.id).any?
        Ticket.generate_sample_ticket_without_notifications(account, account.owner)
      end
    end

    def instrument_trial_activation(tags:)
      statsd_client.increment('support_trial_activation', tags: tags)
    end

    def instrument_skip
      Rails.logger.info("Support product already exists for account #{account.id}, Skipping")
      statsd_client.increment('support_product_creation', tags: ['result:skipping'])
    end

    def statsd_client
      Rails.application.config.statsd.client
    end
  end
end
