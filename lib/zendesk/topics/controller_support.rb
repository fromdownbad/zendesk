module Zendesk::Topics::ControllerSupport
  def self.included(base)
    base.extend(Filters)
  end

  protected

  module Filters
    # Most access checks on topics are done using `for_user(current_user)` scopes
    def authorize_access
      skip_before_action :require_agent!
      allow_anonymous_users :show, :index, if: :public_forums?

      require_that_user_can! :view, :topic, only: [:show]
      require_that_user_can! :edit, :topic, only: [:update]
      require_that_user_can! :add_topic, :forum, only: [:create]

      before_action :authorize_index_access, only: [:index, :show_many]
    end
  end

  def topics
    scope = topic_scope.entries.for_user(current_user)
    scope = scope.where(id: many_ids) if many_ids?
    scope = scope.is_public if respond_to?(:jsonp_request?, true) && jsonp_request?

    scope.order(created_at: :desc, id: :desc).paginate(pagination).
      preload([{attachments: :thumbnails}, :forum])
  end

  def topic
    @topic ||= begin
      topic = current_account.entries.find(params[:id])
      topic.account      = current_account
      topic.current_user = current_user
      topic
    end
  end

  def new_topic
    @new_topic ||= begin
      # Make sure account is set before passing params
      topic = forum.entries.new
      topic.account      = current_account
      topic.current_user = current_user
      topic.attributes = params[:topic]
      topic
    end
  end

  def forum
    @forum ||= begin
      forum_id = params[:forum_id] || params.dig(:topic, :forum_id)

      current_account.forums.find(forum_id)
    end
  end

  def topic_scope
    @topic_scope ||= begin
      if params[:user_id].present?
        current_account.users.find(params[:user_id])
      elsif params[:forum_id].present?
        forum
      else
        current_account
      end
    end
  end

  def public_forums?
    skip_after_action? && current_account.has_public_forums?
  end

  def public_topic?
    skip_after_action? && topic.try(:is_public?)
  end

  # rails 3 performs after filters even if before_actions halted
  def skip_after_action?
    !performed? || response.status.to_i == 200
  end

  def authorize_index_access
    if topic_scope.is_a?(Forum)
      deny_access unless current_user.can?(:view, topic_scope)
    else
      # Scope is either a user or an account. Only agents can see topics in such scope.
      deny_access unless current_user.is_agent?
    end
  end
end
