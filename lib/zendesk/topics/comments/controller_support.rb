module Zendesk::Topics::Comments::ControllerSupport

  def self.included(base)
    base.extend(Filters)
  end

  protected

  module Filters
    def authorize_access
      skip_before_action :require_agent!
      allow_anonymous_users :if => :public_forums?

      before_action :authorize_index_access,   :only => [ :index ]
      before_action :authorize_create_access,  :only => [ :create ]
      before_action :authorize_show_access,    :only => [ :show ]
      before_action :authorize_update_access,  :only => [ :update ]
      before_action :authorize_destroy_access, :only => [ :destroy ]
    end
  end

  def new_topic_comment
    @new_topic_comment ||= begin
      comment = topic.posts.new(params[:topic_comment])
      comment.account = current_account
      comment.user    = current_user unless current_user.is_agent?
      comment.user  ||= current_user
      comment
    end
  end

  def topic_comments
    conditions = current_user.entry_conditions
    topic_comment_scope.posts.joins(:entry).where(conditions).order(created_at: :desc, id: :desc).paginate(pagination)
  end

  def topic_comment
    @topic_comment ||= topic_comment_scope.posts.find(params[:id])
  end

  def topic
    @topic ||= begin
      topic_id   = params[:topic_id]
      topic_id ||= params[:topic_comment][:topic_id] if params[:topic_comment]

      current_account.entries.find(topic_id)
    end
  end

  def authorize_index_access
    deny_access unless visible_topic_comment_scope?
  end

  def authorize_create_access
    if topic.is_locked? || !current_user.can?(:edit, new_topic_comment)
      deny_access
    end
  end

  def authorize_show_access
    deny_access unless current_user.can?(:view, topic_comment)
  end

  def authorize_update_access
    deny_access unless current_user.can?(:edit, topic_comment)
  end

  def authorize_destroy_access
    deny_access unless current_user.can?(:edit, topic_comment)
  end

  def public_forums?
    current_account.has_public_forums?
  end

  def topic_comment_scope
    @topic_comment_scope ||= begin
      if params[:user_id].present?
        current_account.users.find(params[:user_id])
      else
        topic
      end
    end
  end

  def visible_topic_comment_scope?
    if topic_comment_scope.is_a?(User) && !current_user.is_agent? # Only agents can see topic comments by a given user across topics
      false
    elsif topic_comment_scope.is_a?(Entry) && !current_user.can?(:view, topic_comment_scope)
      false
    else
      true
    end
  end

end
