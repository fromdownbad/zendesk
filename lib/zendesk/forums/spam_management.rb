# Public: Spam management. Remove, approve, previously suspended entries and posts.
# It can also globally suspend a user and delete all their entries and posts.
#
# Examples
#
#   spam_management = Zendesk::Forums::SpamManagement.new(account, suspended_entry_ids: [1,2,3], operation: :delete)
#   spam_management.run
#   # => true
module Zendesk::Forums
  class SpamManagement
    # Public: Returns the String error occurred if any.
    attr_reader :error

    # Public: Create a new Spam management.
    # It operates on two collections at the same time, one of entries and the other
    # of posts.
    #
    # account - The Account on which operate on
    # options - The Hash options used to define the action (default: {}):
    #           :suspended_entry_ids - The Array of ids of suspended Entries (optional).
    #           :suspended_post_ids - The Array of ids of suspended Posts (optional).
    #           :operation - The Symbol defining the operation to execute on the
    #                        collections valid options are:
    #                        - :delete, it soft deletes the given items
    #                        - :not_spam, it unsuspends (publishes) the given items
    #                        - :suspend_authors, suspends the authors of the given items
    #                          and soft deletes all their produced content (both suspended and not)
    def initialize(account, options = {})
      @account = account
      @suspended_entry_ids = to_array_of_ids(options.fetch(:suspended_entry_ids, []))
      @suspended_post_ids = to_array_of_ids(options.fetch(:suspended_post_ids, []))
      @operation = options.fetch(:operation).to_sym
    end

    # Public: Execute the spam management defined during initialization.
    #
    # Returns true if all operations went fine, false otherwise.
    def run
      User.transaction do
        case @operation
        when :delete
          SuspendedEntry.for_account(@account).where(id: @suspended_entry_ids).each(&:soft_delete!)
          SuspendedPost.for_account(@account).where(id: @suspended_post_ids).each(&:soft_delete!)

        when :not_spam
          SuspendedEntry.for_account(@account).where(id: @suspended_entry_ids).each(&:unsuspend!)
          SuspendedPost.for_account(@account).where(id: @suspended_post_ids).each(&:unsuspend!)

        when :suspend_authors
          suspend_users!

          # Soft delete both entries and suspended entries that belong to authors
          (authors_entries + suspended_entries).each do |entry|
            entry.watchings.clear
            entry.soft_delete!
          end

          # Soft delete both posts and suspended posts that belong to authors
          @account.posts.authored_by(authors_to_suspend_ids).each(&:soft_delete!)
          SuspendedPost.for_account(@account).authored_by(authors_to_suspend_ids).each(&:soft_delete!)

        else
          @error = "Invalid operation for Zendesk::Forums::SpamManagement, valid are :delete, :not_spam, :suspend_authors"
          return false
        end
      end

      true
    rescue ActiveRecord::RecordInvalid => e
      Rails.logger.append_attributes(:moderation => { :backtrace => e.backtrace })
      Rails.logger.info("[MODERATION] Unknown suspension error: \"#{e.message}\"")

      @error = I18n.t("txt.moderation.suspension.unknown_error")
      false
    end

    def success?
      error.nil?
    end

    private

    def suspend_users!
      @account.users.where(id: authors_to_suspend_ids).each(&:suspend!)
    end

    def authors_to_suspend_ids
      @authors_to_suspend_ids ||= begin
        entries_user_ids = SuspendedEntry.for_account(@account).where(id: @suspended_entry_ids).pluck(:submitter_id)
        posts_user_ids = SuspendedPost.for_account(@account).where(id: @suspended_post_ids).pluck(:user_id)

        entries_user_ids | posts_user_ids
      end
    end

    def to_array_of_ids(elements)
      Array.wrap(elements).map(&:to_i)
    end

    def suspended_entries
      SuspendedEntry.for_account(@account).submitted_by(authors_to_suspend_ids)
    end

    def authors_entries
      @account.entries.submitted_by(authors_to_suspend_ids)
    end
  end
end
