module Zendesk::Forums
  class SpamPreventionEnabler
    attr_accessor :account, :subscription

    # Activate spam filter for account on starter who doesn't have
    # any form of spam prevention active.
    def self.enable_if_compulsory!(account)
      new(account).enable_if_compulsory!
    end

    def initialize(account)
      @account = account
      @subscription = account.subscription
    end

    def enable_if_compulsory!
      if subscription.with_compulsory_spam_prevention? && account.spam_prevention == :off
        account.spam_prevention = :sf
        account.save!
      end
    end
  end
end
