module Zendesk
  class MethodCallInstrumentation
    def initialize(method_name:, exec_context:, source_pattern:)
      @method_name = method_name
      @exec_context = exec_context
      @source_pattern = source_pattern
    end

    def perform
      trace = @exec_context.send :caller_locations
      file, line = method_called_from_file(trace)

      unless file
        log_trace(trace)
        file = 'unknown'
      end

      tags = ["method_name: #{@method_name}", "source:#{file}", "line:#{line}"]
      statsd_client.increment("#{@method_name}_called", tags: tags)
    end

    private

    def method_called_from_file(trace)
      # trace lines are of the format <filename>:<line number>:in '<function name>'
      first_trace_with_pattern = trace.find { |line| line.to_s.split(":")[0] =~ Regexp.new(@source_pattern) }

      if first_trace_with_pattern
        matching_result = first_trace_with_pattern.to_s.match(Regexp.new("((?:#{@source_pattern}).*\.(?:rb))\:(\\d+)")) || []

        [matching_result[1], matching_result[2]]
      end
    end

    def statsd_client
      Zendesk::StatsD::Client.new(namespace: 'method_call_instrumentation')
    end

    def log_trace(trace)
      Rails.logger.info("Unexpected call detected for #{@method_name}, trace: #{trace}")
    end
  end
end
