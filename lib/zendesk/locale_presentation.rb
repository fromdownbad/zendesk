# Module expects a locale method which should return a BCP-47 valid locale. (https://tools.ietf.org/html/bcp47)

module Zendesk
  module LocalePresentation
    # TODO: Fix TranslationLocale "name" to come from ICU then fix places like
    # `locale.localized_name(display_in: locale)` to just use `locale.name`.

    # `display_in` which language to display `locale` name in
    def localized_name(display_in:)
      ICU::Locale.new(locale_identifier(locale)).display_name(display_in.locale)
    end

    def display_name(display_in:)
      native_name     = localized_name(display_in: self)
      translated_name = localized_name(display_in: display_in)

      if native_name == translated_name
        native_name
      else
        # If the current locale is RTL but the native name of the locale in the
        # language dropdown is LTR, wraps the native name in LTR control characters,
        # to ensure that any parentheses are rendered properly.

        # NOTE: If a similar issue arises in the reverse situation (i.e., the current
        # locale is LTR and an RTL native locale is misrendered properly), it can be
        # fixed using the RTL control character - \u202E. E.g.:

        # if current_locale.rtl? && !self.rtl?
        #   native_name = "\u202D#{native_name}\u202C"
        # elsif !current_locale.rtl? && self.rtl?
        #   native_name = "\u202E#{native_name}\u202C"
        # end

        native_name = "\u202D#{native_name}\u202C" if display_in.rtl?

        "#{translated_name} - #{native_name}"
      end
    end

    private

    # Allow overriding ICU defaults for certain use cases, ie.
    # politically-sensitive locale identifiers.
    def locale_identifier(locale)
      case locale.downcase
      when 'zh-cn' then 'zh-Hans'
      when 'zh-tw' then 'zh-Hant'
      else
        locale
      end
    end
  end
end
