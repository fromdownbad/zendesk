ActionController::Base.class_eval do
  before_action :force_path_to_utf8

  private

  def force_path_to_utf8
    force_utf8_encoding!(request.env["PATH_INFO"])
    # Clear memoized value
    request.instance_variable_set(:@fullpath, nil)
  end

  def force_utf8_encoding!(str)
    str.force_encoding(Encoding::UTF_8)

    unless str.valid_encoding?
      str.encode!(Encoding::UTF_8, Encoding::ISO_8859_1)
    end
  end
end
