module Zendesk
  module Targets
    module Helper
      MAX_TRIAL_ACCOUNT_TARGETS_THRESHOLD = 2

      def exceed_maximum_trial_targets_limit?(account)
        account.has_trial_account_target_limit? && account.is_trial? && account.targets.count >= MAX_TRIAL_ACCOUNT_TARGETS_THRESHOLD
      end
    end
  end
end
