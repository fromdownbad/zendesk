module Zendesk
  module Targets
    # Tester allows testing Target configurations.
    #
    # Examples
    #
    #   tester = Tester.new(:account => current_account, :user => current_user)
    #   tester.test(target)
    #
    class Tester
      class NoTicketsInAccount < StandardError
      end

      class TargetInvalid < StandardError
        def initialize(target = nil)
          @target = target
        end

        def error_messages
          @target.errors.full_messages
        end
      end

      class TestingNotSupported < StandardError
        attr_reader :reason

        def initialize(reason = nil)
          @reason = reason
        end
      end

      class TestFailed < StandardError
        attr_reader :error

        def initialize(error)
          @error = error
        end
      end

      # Create a new Tester.
      #
      # Options
      #
      # :account - the Account on which to test.
      # :user    - the User who's doing the testing.
      # :mailer  - a mailer class (default: TargetsMailer).
      #
      def initialize(options = {})
        @account = options.fetch(:account)
        @user    = options.fetch(:user)
        @mailer  = options.fetch(:mailer, TargetsMailer)
      end

      # Test a target.
      #
      # The target will be executed with a test message.
      #
      # target - the Target that is to be tested.
      #
      # Options
      #
      # :message - the String that will be used as a the target's custom message
      #
      # These are only necessary if the target requires authorization.
      #
      # :token  - the String access token used for authorization.
      # :secret - the String access secret used for authorization.
      #
      # Returns nothing.
      # Raises TestFailed if the test failed.
      # Raises TargetInvalid if the target is not valid.
      # Raises TestingNotSupported if the target does not support testing.
      # Raises NoTicketsInAccount if the account has no tickets.
      def test(target, options = {})
        raise TargetInvalid, target unless target.valid?
        raise TestingNotSupported, target.test_no_supported_reason unless target.is_test_supported?

        @message = options[:message]

        (ticket = find_ticket) || raise(NoTicketsInAccount)

        # doesn't seem to matter too much as long as it can be serialized
        event        = ticket.events.first
        event.author = @user
        event.via_id = ViaType.WEB_FORM

        begin
          target.send_test_message(test_message, event)
        rescue StandardError => e
          # seeing 500s for tests? add to handled_errors array below
          if @@handled_errors.any? { |h| e.message.index(h) }
            raise TestFailed, e
          else
            raise e
          end
        end
      end

      private

      attr_reader :message

      def find_ticket
        @account.tickets.not_closed.first
      end

      def test_message
        message || "Test message from Zendesk sent on: #{Time.now.utc.strftime("%Y-%m-%d %H:%M:%S")} UTC"
      end

      def build_external(ticket, user)
        ticket.will_be_saved_by(user)
        external = External.new(via_id: ViaType.WEB_FORM, audit: ticket.audit, ticket: ticket, author: user)
        ticket.audit.events << external
        external
      end

      @@handled_errors = [
        "Campfire login failed", "HTTP client call failed", "Invalid campfire room",
        "Authentication failed", "Your account requires SSL", "bad URI", "HTTP request path is empty",
        "Name or service not known", "Connection refused", "execution expired", "Connection timed out",
        "Yammer failed with response", "Invalid consumer and secret key", "HTTPBadResponse",
        "Project not found", "Authorization Required", "Internal Server Error",
        "Error saving the message in basecamp", "Project not found in basecamp",
        "Failed with 403 Forbidden", "wrong status line", "Failed with 302 Found",
        "Twitter is returning a 408", "Connection reset by peer", "The url or credentials are wrong",
        "Can't send a message without a project id", "pivotal story creation failed",
        "Twitter is returning a 403", "Could not authenticate you", "Couldn't find room",
        "Could not send Clickatell message. Authentication failed. Check your username, password and API ID.",
        "Could not send Clickatell message. Unknown username or password.",
        "Could not send Clickatell message. Invalid or missing data.",
        "Could not send Clickatell message. Invalid destination address, check the phone number.",
        "Could not send Clickatell message. Invalid Source Address.",
        "Could not send Clickatell message. Please check your API ID.",
        "Could not send Clickatell message. Please check your phone number – could not route message.",
        "Project members retrieval", "Salesforce Login Failed", "Error creating MSDynamics case"
      ]
    end
  end
end
