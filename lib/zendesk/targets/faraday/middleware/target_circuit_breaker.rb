require 'johnny_five'
require 'faraday'

module FaradayMiddleware
  class TargetCircuitBreaker < Faraday::Middleware
    include ::JohnnyFive::CircuitBreaker

    ERROR_STATUS_CODES = (400...600).freeze

    class HTTPFailedResponseError < Faraday::ClientError; end

    def initialize(app, args)
      @app = app
      @options = {
        is_failure: ->(e) { e.is_a? Faraday::Error },
      }.merge(args[:options])
    end

    def call(env)
      with_circuit(@options) do
        @app.call(env).tap do |res|
          # Only raise the HTTPFailedResponseError with 4xx and 5xx responses in order to trigger circuit breaker.
          # other faraday client errors will still trigger circuit breaker such as timeout or connection errors
          # and be rescued in target job.
          raise HTTPFailedResponseError.new("resp:#{res.status}", res) if ERROR_STATUS_CODES.include?(res.status)
        end
      end
    rescue HTTPFailedResponseError => e
      # This middleware swallows since it is only for triggering circuit breaker.
      Rails.logger.debug "Failed URL target response: #{e.message}"
      e.response
    end
  end
end
