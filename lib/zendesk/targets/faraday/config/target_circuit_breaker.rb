require 'johnny_five'

module FaradayConfiguration
  module TargetCircuitBreaker
    # Use memcache to be circuit breaker cache store if dalli client exists, otherwise fallback to default memory store.
    #
    # Other team has done circuit breaker implementation and did johnny_five circuit break configuration
    # already with configuration johnny_five.rb under initializer folder only optionally initialize memcache client
    # once dalli client is available. It looks like we use AWS memcached version elasticache. Environment variable
    # `USE_DALLI_ELASTICACHE`controls whether dalli_store is configured or not which impacts the dalli client existence.
    # You can find more details about existing dalli_store in config/application.rb file.
    def circuit_store
      if Rails.cache.try(:dalli)
        JohnnyFive::MemcachedStore.new(ttl: circuit_breaker_failure_ttl)
      else
        JohnnyFive::MemoryStore.instance
      end
    end

    def circuit_options(name:, failure_threshold: nil, reset_timeout: nil)
      {
        name: name,
        store: circuit_store,
        failure_threshold: failure_threshold || default_failure_threshold,
        reset_timeout: reset_timeout || default_reset_timeout
      }
    end

    private

    def default_failure_threshold
      (ENV['TARGET_CIRCUIT_BREAKER_FAILURE_THRESHOLD'] || 30).to_i
    end

    def default_reset_timeout
      (ENV['TARGET_CIRCUIT_BREAKER_RESET_TIMEOUT'] || 5).to_i
    end

    def circuit_breaker_failure_ttl
      (ENV['TARGET_CIRCUIT_BREAKER_FAILURE_TTL'] || 900).to_i
    end
  end
end
