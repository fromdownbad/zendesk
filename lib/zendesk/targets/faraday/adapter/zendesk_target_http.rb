module Faraday
  class Adapter
    class ZendeskTargetHttp < NetHttp
      attr_reader :raw_http_capture

      def initialize(app, options = {})
        @raw_http_capture = options[:raw_http_capture]
        super(app)
      end

      def net_http_connection(env)
        super.tap { |http| http.set_debug_output raw_http_capture }
      end
    end
  end
end
