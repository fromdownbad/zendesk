require 'faraday'
require 'faraday_middleware'
require 'faraday/restrict_ip_addresses'
require 'zendesk/targets/faraday/adapter/zendesk_target_http'
require 'zendesk/targets/faraday/config/target_circuit_breaker'
require 'zendesk/targets/faraday/middleware/target_circuit_breaker'
require 'zendesk/targets/metrics/statsd_client'

module Zendesk
  class UrlTargetClient
    include FaradayConfiguration::TargetCircuitBreaker
    include TargetsMetrics::StatsDClient

    GET  = "get".freeze
    POST = "post".freeze
    PUT  = "put".freeze

    CIRCUIT_BREAKER_PREFIX = 'circuit-breaker-url-target-v1'.freeze

    attr_reader :method, :username, :password, :locale, :target

    def initialize(options = {})
      @url      = options.fetch(:url)
      @method   = options.fetch(:method)
      @method   = [GET, PUT].member?(@method) ? @method : POST
      @params   = options.fetch(:params, {})
      @account  = options.fetch(:account)
      @user     = options.fetch(:user, nil)
      @ticket   = options.fetch(:ticket, nil)
      @username = options.fetch(:username, nil)
      @password = options.fetch(:password, nil)
      @target   = options.fetch(:target)
      @locale   = locale_for_liquid

      # TODO: this code should be removed after we understand the production circuit breaker behaviour.
      subscribe_circuit_breaker_events
    end

    def invoke
      # TODO: use Faraday.new(url) when 0.9.0 is no longer an RC
      connection = Faraday.new(nil, ssl: { verify: false}) do |builder|
        builder.use FaradayMiddleware::FollowRedirects, limit: 3
        builder.use Faraday::RestrictIPAddresses, allow_url: lambda { |url| Zendesk::Net::AddressUtil.safe_url?(url.to_s) }
        # only start to blocking request by circuit breaker when this arturo is on
        circuit_breaker_options = if target.account.has_url_target_v1_circuit_breaker?
          # dry_run:false indicates circuit breaker will start to block the requests once it's triggered
          circuit_options.merge(dry_run: false)
        else
          # dry_run: true indicates circuit breaker will only report the error without blocking the requests
          circuit_options.merge(dry_run: true)
        end
        builder.use FaradayMiddleware::TargetCircuitBreaker, options: circuit_breaker_options
        builder.use Faraday::Adapter::ZendeskTargetHttp, raw_http_capture: target.raw_http_capture
      end

      connection.basic_auth(username, password) if username.present? && password.present?
      connection.options[:timeout]      = 10
      connection.options[:open_timeout] = 10

      # TODO: use url when initializing Faraday when 0.9.0 is no longer an RC
      connection.send(method, url) do |request|
        scope = [@ticket, @user].detect { |m| m.present? && m.account.subdomain }

        request.headers['User-Agent'] = if scope
          "Zendesk URL Target (#{scope.account.subdomain})"
        else
          "Zendesk URL Target"
        end
        unless method == GET
          request.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8'
          request.body = query
        end
      end
    rescue ::JohnnyFive::CircuitTrippedError => e
      statsd_client.increment(:circuit_tripped, tags: %W[source:#{target.metrics_name}])
      raise e
    end

    def statsd_client
      @statsd_client ||= create_statsd_client
    end

    # For GET requests, the URL contains all parameters. For POST/PUT we put those
    # in the body on the client side.
    def url
      if method == GET
        base_url + "?" + query
      else
        base_url
      end
    end

    def query
      result = ""
      parameters.keys.sort.each do |key|
        result << "&" unless result.blank?
        result << "#{key}=#{parameters[key]}"
      end
      result
    end

    def base_url
      interpolate(@url.split("?").first)
    end

    def parameters
      @parameters ||= begin
        result = {}

        # Retrieve the user specified params from the URL
        parts = @url.split("?")[1].to_s
        parts.split("&").each do |part|
          key, value = part.split("=")
          result[key] = interpolate(value) unless key == "locale"
        end

        # Add the manually specified params
        @params.each_pair do |key, value|
          if method == GET
            result[key] = Rack::Utils.escape(value)
          else
            result[key] ||= Rack::Utils.escape(value)
          end
        end

        result
      end
    end

    def circuit_options
      super(name: "#{CIRCUIT_BREAKER_PREFIX}-#{target.id}")
    end

    protected

    def locale_for_liquid
      translation = TranslationLocale.find_by_id($1) if @url =~ /locale=(\d+)/
      translation ? translation : fallback_translation
    end

    def fallback_translation
      @user.account.translation_locale if @user.present?
    end

    def interpolate(segment)
      if @user.present? && @ticket.present? && segment =~ /\{\{(.*?)\}\}/
        segment_with_encoding_filter = insert_url_encode_filter(segment)
        Zendesk::Liquid::TicketContext.render(segment_with_encoding_filter, @ticket, @user, true, @locale)
      else
        segment
      end
    end

    def insert_url_encode_filter(template)
      template.gsub(%r|\{\{(.*?)\}\}|, '{{\1 | url_encode}}')
    end

    # TODO: this code should be removed after we understand the production circuit breaker behaviour.
    def subscribe_circuit_breaker_events
      # subscribe soft circuit tripped event in order to understand the circuit breaker
      # behaviour in production before enable the real one.
      @@statsd_client ||= create_statsd_client
      @@circuit_subscriber ||= ActiveSupport::Notifications.subscribe(JohnnyFive::Breaker::EVENTS[:tripped]) do |*args|
        name = args.extract_options![:name]
        unless name.nil? || !name.start_with?(CIRCUIT_BREAKER_PREFIX)
          @@statsd_client.increment(:soft_circuit_tripped, tags: %w[source:url_target])
        end
      end
    end
  end
end
