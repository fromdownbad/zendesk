module TargetsMetrics
  module StatsDClient
    def create_statsd_client
      Zendesk::StatsD::Client.new(namespace: :classic_target)
    end
  end
end
