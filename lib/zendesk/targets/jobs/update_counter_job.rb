require 'zendesk/targets/metrics/statsd_client'

module Zendesk::Targets::Jobs
  class UpdateCounterJob < Zendesk::Maintenance::Jobs::PerShardJob
    extend ZendeskJob::Resque::BaseJob
    extend TargetsMetrics::StatsDClient
    TARGETS_TO_UPDATE_KEY = 'targets_to_update_shard'.freeze

    def self.work_on_shard(shard_id)
      targets_to_update = Rails.cache.fetch("#{TARGETS_TO_UPDATE_KEY}#{shard_id}", expires_in: 1.day) do
        Target.where(is_active: true).pluck(:id)
      end
      targets_to_update.each do |target_id|
        begin
          target_cache = read_target_cache(target_id)
          next unless target_cache[:sent] > 0 || target_cache[:failures] > 0
          target = Target.find(target_id)
          target.attributes = {
            sent: target.sent + target_cache[:sent],
            failures: target_cache[:sent] > 0 ? target_cache[:failures] : target.failures + target_cache[:failures],
            error: target_cache[:error]
          }
          target.save if target.changed?
          reset_target_cache(target_id, target_cache)
          statsd_client.increment("bulk_counter.success", tags: ['source:update_target_counter'])
        rescue StandardError => e
          log_args = { error_class: e.class.name, error_message: e.message, target_id: target_id }
          log_args[:account_id] = target.account_id unless target.nil?
          resque_log(["exception", args_to_log.merge(log_args)])
        end
      end
    end

    def self.args_to_log(*_args)
      {
        job: name.demodulize,
        time: Time.now.to_s,
      }
    end

    def self.statsd_client
      @@statsd_client ||= create_statsd_client
    end

    def self.read_target_cache(target_id)
      {
        sent: Rails.cache.read(Target.cached_success_count_key(target_id)).to_i,
        failures: Rails.cache.read(Target.cached_failure_count_key(target_id)).to_i,
        error: Rails.cache.read(Target.cached_exception_message_key(target_id)) || ""
      }
    end

    def self.reset_target_cache(target_id, target_cache)
      Rails.cache.decrement(Target.cached_success_count_key(target_id), target_cache[:sent])
      Rails.cache.decrement(Target.cached_failure_count_key(target_id), target_cache[:failures])
      Rails.cache.delete(Target.cached_exception_message_key(target_id))
    end
  end
end
