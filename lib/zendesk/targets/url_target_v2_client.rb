require 'faraday'
require 'faraday_middleware'
require 'zendesk/targets/faraday/adapter/zendesk_target_http'
require 'zendesk/targets/faraday/config/target_circuit_breaker'
require 'zendesk/targets/faraday/middleware/target_circuit_breaker'
require 'zendesk/targets/metrics/statsd_client'

module Zendesk
  class UrlTargetV2Client
    include FaradayConfiguration::TargetCircuitBreaker
    include TargetsMetrics::StatsDClient

    CIRCUIT_BREAKER_PREFIX = 'circuit-breaker-url-target-v2'.freeze

    attr_reader :target, :message, :event

    delegate :ticket, :author, to: :event
    delegate :url, :method, :content_type, :username, :password, :attribute, to: :target

    def initialize(options)
      @target = options[:target]
      @message = options[:message]
      @event = options[:event]
      @message = ticket_json_with_safe_update if ticket_update?

      # TODO: this code should be removed after we understand the production circuit breaker behaviour.
      subscribe_circuit_breaker_events
    end

    def invoke
      connection.send(method, full_url) do |request|
        if target.needs_content_type?
          request.headers['Content-Type'] = "#{content_type}; charset=utf-8"
          request.body = body.presence
        end

        if target.account.has_log_targets_v2_requests?
          Rails.logger.info("target request created, { "\
            "target_id: #{target.id}, "\
            "method: #{method}, "\
            "url: #{full_url}, "\
            "content_type: #{request.headers["Content-Type"]}, "\
            "body: #{body}}")
        end
      end
    rescue ::JohnnyFive::CircuitTrippedError => e
      statsd_client.increment(:circuit_tripped, tags: %W[source:#{target.metrics_name}])
      raise e
    end

    def circuit_options
      super(name: "#{CIRCUIT_BREAKER_PREFIX}-#{target.id}")
    end

    def statsd_client
      @statsd_client ||= create_statsd_client
    end

    private

    def same_subdomain?
      parsed_url.host == URI.parse(event.account.url(mapping: false)).host
    end

    def ticket_update?
      # Since the addition of safe_update relies on behavior that is currently only present
      # in our tickets endpoint, the path is hard-coded for now.
      ticket.account.has_ticket_target_retry? &&
      same_subdomain? &&
      targeted_ticket_id &&
      method == "put" &&
      content_type == "application/json"
    end

    def targeted_ticket_id
      @ticket_id ||= parsed_url.path.match(/^\/api\/v2\/tickets\/(\d+)(?:\.[^.]*)?$/) do |id_portion|
        id_portion[1].to_i
      end
    end

    # see https://support.zendesk.com/agent/tickets/1274115.
    # URL targets that make requests to the Zendesk API are very good at finding race conditions,
    # so we introduce `safe_update` to allow us to catch simultaneous requests and retry them
    # in sequence.
    def ticket_json_with_safe_update
      update_json = JSON.parse(message)
      if update_json["ticket"]
        update_json["ticket"]["safe_update"] = true
        update_json["ticket"]["updated_stamp"] = if targeted_ticket_id == ticket.nice_id
          ticket.updated_at.iso8601
        else
          ticket.account.tickets.find_by_nice_id(targeted_ticket_id).updated_at.iso8601
        end
      end
      update_json.to_json
    rescue JSON::ParserError, KeyError, TypeError
      message
    end

    def full_url
      if method == 'get' && message.is_a?(Array)
        parsed_url.query = url_query_with_message_params.presence
      end

      parsed_url.to_s
    end

    def url_query_with_message_params
      parameters = URI.decode_www_form(parsed_url.query.to_s)
      parameters = parameters.concat(message)

      URI.encode_www_form(parameters)
    end

    def parsed_url
      @parsed_url ||= URI.parse(interpolate(url))
    end

    def connection
      Faraday.new(nil) do |connection|
        connection.use FaradayMiddleware::FollowRedirects, limit: 3
        connection.use Faraday::RestrictIPAddresses, allow_url: lambda { |url| Zendesk::Net::AddressUtil.safe_url?(url.to_s) || Rails.env.development? }
        # only start to blocking request by circuit breaker when this arturo is on
        circuit_breaker_options = if target.account.has_url_target_v2_circuit_breaker?
          # dry_run:false indicates circuit breaker will start to block the requests once it's triggered
          circuit_options.merge(dry_run: false)
        else
          # dry_run: true indicates circuit breaker will only report the error without blocking the requests
          circuit_options.merge(dry_run: true)
        end
        connection.use FaradayMiddleware::TargetCircuitBreaker, options: circuit_breaker_options
        connection.use Faraday::Adapter::ZendeskTargetHttp, raw_http_capture: target.raw_http_capture
        connection.basic_auth(username, password) if username.present?
        connection.options[:timeout]      = 10
        connection.options[:open_timeout] = 10
        connection.headers[:user_agent]   = 'Zendesk Target'
      end
    end

    def interpolate(string)
      if author.present? && ticket.present? && string =~ /\{\{(.*?)\}\}/
        Zendesk::Liquid::TicketContext.render(string, ticket, author, true, locale)
      else
        string
      end
    end

    def locale
      translation_locale = TranslationLocale.find_by_id($1) if url =~ /locale=(\d+)/
      translation_locale || ticket.account.translation_locale
    end

    def body
      if content_type == UrlTargetV2::CONTENT_TYPES['Form encoded'] && message.is_a?(Array) && !message.empty?
        URI.encode_www_form(message)
      else
        message.to_s
      end
    end

    # TODO: this code should be removed after we understand the production circuit breaker behaviour.
    def subscribe_circuit_breaker_events
      # subscribe soft circuit tripped event in order to understand the circuit breaker
      # behaviour in production before enable the real one.
      @@statsd_client ||= create_statsd_client
      @@circuit_subscriber ||= ActiveSupport::Notifications.subscribe(JohnnyFive::Breaker::EVENTS[:tripped]) do |*args|
        name = args.extract_options![:name]
        unless name.nil? || !name.start_with?(CIRCUIT_BREAKER_PREFIX)
          @@statsd_client.increment(:soft_circuit_tripped, tags: %w[source:url_target_v2])
        end
      end
    end
  end
end
