module Zendesk
  module Targets
    class Initializer
      InvalidTargetType = Class.new(StandardError)
      LEGACY_API_ATTRIBUTE_MAP = { target_url: :url, active: :is_active }.freeze

      def initialize(options = {})
        @account = options.fetch(:account)
        @via = options.fetch(:via)
      end

      def build(attributes)
        requested_type = attributes.delete(:type)

        # prevent creation of targets that need integration via api
        if @via == :api && ["ms_dynamics_target", "sugar_crm_target", "salesforce_target"].include?(requested_type)
          raise InvalidTargetType
        end

        unless type = target_type(requested_type)
          raise InvalidTargetType, "No target type named '#{requested_type}'"
        end

        map_api_attributes!(attributes)
        attributes[:is_active] = true unless attributes.key? :is_active
        attributes[:account] = @account
        type.new(attributes).tap(&:encrypt!)
      end

      def set(target, attributes)
        attributes.delete(:type) # to mirror build
        map_api_attributes!(attributes)
        target.attributes = attributes
        target.encrypt!
        target
      end

      def target_types_map
        {
          "campfire_target" => CampfireTarget,
          "url_target" => UrlTarget,
          "url_target_v2" => UrlTargetV2,
          "http_target" => UrlTargetV2,
          "email_target" => EmailTarget,
          "clickatell_target" => ClickatellTarget,
          "twitter_target" => TwitterTarget,
          "get_satisfaction_target" => GetSatisfactionTarget,
          "yammer_target" => YammerTarget,
          "basecamp_target" => BasecampTarget,
          "jira_target" => JiraTarget,
          "pivotal_target" => PivotalTarget,
          "twilio_target" => TwilioTarget,
          "salesforce_target" => SalesforceTarget,
          "sugar_crm_target" => SugarCrmTarget,
          "ms_dynamics_target" => MsDynamicsTarget,
        }
      end

      private

      def map_api_attributes!(attributes)
        LEGACY_API_ATTRIBUTE_MAP.each do |from, to|
          attributes[to] = attributes.delete(from) if attributes.key?(from)
        end
      end

      def target_type(type_name)
        target_types_map[type_name.to_s]
      end
    end
  end
end
