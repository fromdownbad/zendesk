module Zendesk
  module OrganizationMemberships
    class Finder
      attr_reader :account, :params

      def initialize(account, params)
        @account = account
        @params = params
      end

      def membership_scope
        if user_id
          account.users.find(user_id)
        elsif organization_id
          account.organizations.find(organization_id)
        else
          account
        end
      end

      private

      def user_id
        params[:user_id]
      end

      def organization_id
        params[:organization_id]
      end
    end
  end
end
