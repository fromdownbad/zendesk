module Zendesk
  module GroupMemberships
    class Finder
      attr_reader :account, :params

      def initialize(account, params)
        @account = account
        @params = params
      end

      def membership_scope
        if user_id
          account.users.find(user_id)
        elsif group_id
          account.groups.find(group_id)
        else
          account
        end
      end

      private

      def user_id
        params[:user_id]
      end

      def group_id
        params[:group_id]
      end
    end
  end
end
