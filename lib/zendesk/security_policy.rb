module Zendesk
  module SecurityPolicy
    NON_CUSTOM_SECURITY_POLICY_IDS = [100, 200, 300].freeze
    MAX_PASSWORD_LENGTH = 128

    def self.find(policy_id, account = nil)
      model = all.find { |policy| policy.id == policy_id.to_i }

      if model.blank?
        raise ArgumentError, "Unknown security policy #{policy_id}"
      end

      model.new(account)
    end

    def self.all
      [High, Medium, Low, Custom]
    end

    class Low
      include Enforcement

      class_attribute :id, :password_length, :max_sequence
      self.id = 100
      self.password_length = 5

      validations[:unencrypted_password] += [:minimum_password_length, :maximum_password_length, :difference_password_username]

      class << self
        def title
          name.demodulize
        end

        def display_name
          ::I18n.t('txt.admin.views.settings.security._password_policy.policy_title_' + title.downcase)
        end

        def to_i
          id
        end

        alias_method :old_to_s, :to_s

        def to_s
          id.to_s
        end

        def inspect
          old_to_s
        end

        def ===(other)
          id == other.id
        end
      end

      def initialize(account = nil)
        @account = account
      end

      def title
        self.class.title
      end

      def display_name
        self.class.display_name
      end

      # Check whether the policy keeps a history of previous passwords.
      #
      # Returns true if it does, false otherwise.
      def has_password_history? # rubocop:disable Naming/PredicateName
        false
      end

      def password_expiring?
        false
      end

      def password_expired?
        password_expired_for_security_incident? || password_expired_according_policy?
      end

      def password_expired_according_policy?
        false
      end

      def password_expired_for_security_incident?
        return false unless Arturo.feature_enabled_for?(:expire_old_passwords, user.account)
        previous_password = user.password_changes.previous(1).first

        password_created_at = (previous_password && previous_password.created_at) || user.created_at

        Time.parse('2016-11-01') > password_created_at
      end

      def password_expires_at
        nil
      end

      def deliver_expiring_password_warnings
        []
      end

      def policy_level
        id
      end

      def level?(level)
        title.downcase == level.to_s
      end

      def random_password
        Token.generate(15, false)
      end

      def minimum_password_length
        Validation::Length.new(minimum: password_length)
      end

      def maximum_password_length
        Validation::MaxLength.new(maximum: MAX_PASSWORD_LENGTH)
      end

      def difference_password_username
        Validation::DifferenceWithUsername.new.tap do |validations|
          validations.username = user.email if user
        end
      end

      def max_consecutive_letters_and_numbers_validation
        Validation::MaxCharsNumbersInSequence.new.tap do |validations|
          validations.max_consecutive_letters_and_numbers = max_consecutive_letters_and_numbers
        end
      end

      # Returns the default password attempt threshold defined in Prop.
      def password_attempt_threshold
        Prop::Limiter.handles[:password_attempt][:threshold]
      end

      alias_method :to_i, :id
      alias_method :old_to_s, :to_s

      def to_s
        id.to_s
      end

      def inspect
        old_to_s
      end

      def ===(other)
        id == other.id
      end
    end

    class Medium < Low
      self.validations = validations.dup

      self.id = 200
      self.password_length = 6

      validations[:unencrypted_password] += [Validation::AlphanumericComplexity, Validation::SpecialCharacter]

      def random_password
        upper_and_lowercase = nil
        10.times do
          upper_and_lowercase = super
          break unless Validation::AlphanumericComplexity.new.deny?(upper_and_lowercase) # sometimes super only generates lowercase
        end

        special_character   = ('!'..'/').to_a.sample
        number              = (0..9).to_a.sample

        randomized = "#{upper_and_lowercase}#{special_character}#{number}".split('').shuffle!
        randomized.join
      end

      module Expiry
        def self.included(base)
          base.class_attribute :stale_password_duration
          base.stale_password_duration = 5.days
        end

        def password_expired?
          return true if password_expired_for_security_incident?
          password_expired_according_policy?
        end

        def password_expired_according_policy?
          password_expires_at.present? && Time.now >= password_expires_at
        end

        def password_expiring?
          password_expires_at.present? && password_expires_at >= Time.now && password_expires_at <= stale_password_duration.from_now
        end

        def password_expires_at
          if policy_level > user.password_security_policy_id
            created_at + stale_password_duration
          end
        end

        protected

        def created_at
          (latest_security_policy_change || user.account).created_at
        end

        # For older users without password audits, the password was created before the security policy.
        def password_created_at
          (previous_password && previous_password.created_at) || created_at - 1.week
        end

        def previous_password
          all_previous_passwords(1).first
        end

        def all_previous_passwords(count)
          user.password_changes.previous(count).all # always use old auditing, passwords are only recorded for agents in new auditing
        end

        private

        def latest_security_policy_change
          user.account.role_settings.cia_attribute_changes.
            on_attribute("#{user_role}_security_policy_id").previous.first
        end

        def user_role
          user.is_agent? ? 'agent' : 'end_user'
        end
      end
      include Expiry
    end

    class High < Medium
      self.validations = validations.dup

      class_attribute :password_history_length, :password_duration
      self.id = 300
      self.password_history_length = 5
      self.password_duration = 90

      validations[:password] += [:password_not_previously_used]

      def has_password_history? # rubocop:disable Naming/PredicateName
        true
      end

      def password_expires_at
        dates = [super]
        dates.compact!
        dates << password_created_at + password_duration.days if password_duration?
        dates.min
      end

      def requirements_in_words
        requirements = super
        requirements[:password] << ::I18n.t('txt.security_policy.requirements.expiration_in_days', count: password_duration) if password_duration?
        requirements
      end

      def password_duration?
        !password_duration.to_i.zero?
      end

      protected

      def password_not_previously_used
        Validation::UniqueHistory.new.tap do |validation|
          validation.history       = previous_passwords
          validation.history_limit = password_history_length
        end
      end

      def previous_passwords
        if password_history_available?
          all_previous_passwords(password_history_length).map(&:new_value)
        else
          []
        end
      end

      def password_history_available?
        user && !user.new_record? && !password_history_length.zero?
      end
    end

    class Custom < High
      self.validations = validations.dup

      class_attribute :password_complexity, :password_in_mixed_case, :failed_attempts_allowed,
        :custom_policy_level, :max_consecutive_letters_and_numbers, :disallow_local_part_from_email
      attr_reader :custom_security_policy
      self.id = 400

      def initialize(account)
        raise ArgumentError, "Provide an account to create a custom security policy" unless account

        @custom_security_policy = account.custom_security_policy || CustomSecurityPolicy.build_from_current_policy(account)
        initialize_policy

        super
      end

      # Use custom_policy_level if defined as policy_level.
      #
      # This is used to expire weak user passwords with minor password_security_policy_id,
      # after the admin has increased the custom policy security
      #
      # If the security of the custom policy has been increased,
      # the policy_level will be 401, 402 or 40x depending on how many times that has happened.
      def policy_level
        custom_policy_level || super
      end

      def has_password_history? # rubocop:disable Naming/PredicateName
        password_history_length && password_history_length > 0
      end

      def password_attempt_threshold
        return super if failed_attempts_allowed.nil? || failed_attempts_allowed == 0
        failed_attempts_allowed
      end

      def disallow_local_part
        Validation::LocalPartFromEmail.new.tap do |validation|
          validation.email = user.email if user
        end
      end

      protected

      def initialize_policy
        self.custom_policy_level                 = custom_security_policy.policy_level
        self.password_length                     = custom_security_policy.password_length
        self.password_history_length             = custom_security_policy.password_history_length
        self.password_duration                   = custom_security_policy.password_duration
        self.password_complexity                 = custom_security_policy.password_complexity
        self.password_in_mixed_case              = custom_security_policy.password_in_mixed_case?
        self.failed_attempts_allowed             = custom_security_policy.failed_attempts_allowed
        self.max_consecutive_letters_and_numbers = custom_security_policy.max_sequence
        self.disallow_local_part_from_email = custom_security_policy.disallow_local_part_from_email

        validations[:password] = []
        validations[:unencrypted_password] = []

        validations[:password] << :password_not_previously_used if has_password_history?

        validations[:unencrypted_password] += [:minimum_password_length, :difference_password_username]
        validations[:unencrypted_password] += [Validation::NumberComplexity] if password_complexity == CustomSecurityPolicy::NUMBERS_COMPLEXITY
        validations[:unencrypted_password] += [Validation::NumberComplexity, Validation::SpecialCharacter] if password_complexity == CustomSecurityPolicy::SPECIAL_COMPLEXITY
        validations[:unencrypted_password] += [Validation::MixedComplexity] if password_in_mixed_case?
        validations[:unencrypted_password] += [:max_consecutive_letters_and_numbers_validation] if max_consecutive_letters_and_numbers
        validations[:unencrypted_password] += [:disallow_local_part] if disallow_local_part_from_email?
        validations[:unencrypted_password] += [:maximum_password_length]
      end
    end
  end
end
