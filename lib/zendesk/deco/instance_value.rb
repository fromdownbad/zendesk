module Zendesk
  module Deco
    InstanceValue = Struct.new(
      :id,
      :instance_id,
      :attribute_value_id,
      :type_id,
      :created_at,
      :updated_at,
      :deleted_at
    ) do
      def self.from_json(json)
        new(
          json['id'],
          json['instance_id'],
          json['attribute_value_id'],
          json['type_id'],
          json['created_at'],
          json['updated_at'],
          json['deleted_at']
        )
      end
    end
  end
end
