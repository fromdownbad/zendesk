require 'kragle'

module Zendesk
  module Deco
    class Client
      APP_NAME = 'deco'.freeze
      URL = '%{deco_url}/%{account_id}/%{path}'.freeze

      TYPE_ID_TICKET = 10
      TYPE_ID_AGENT  = 20
      DEFAULT_REQUEST_TIMEOUT_SECS = 5
      DEFAULT_OPEN_TIMEOUT_SECS = 2

      # Kragle settings
      MAX_RETRIES = 3
      CIRCUIT_BREAKER_FAILURE_THRESHOLD = 3
      CIRCUIT_BREAKER_RESET_TIMEOUT_SECS = 5

      attr_reader :account

      private_constant :APP_NAME
      private_constant :URL

      def initialize(account)
        @account = account
      end

      def get(path,
        json_body = nil,
        timeout = DEFAULT_REQUEST_TIMEOUT_SECS,
        open_timeout = DEFAULT_OPEN_TIMEOUT_SECS)
        connection.get do |req|
          setup_request(req, path, json_body, timeout, open_timeout)
        end
      end

      def post(path,
        params,
        timeout = DEFAULT_REQUEST_TIMEOUT_SECS,
        open_timeout = DEFAULT_OPEN_TIMEOUT_SECS)
        record_using_skills

        connection.post do |req|
          setup_request(req, path, params, timeout, open_timeout)
        end
      end

      def put(path,
        params,
        timeout = DEFAULT_REQUEST_TIMEOUT_SECS,
        open_timeout = DEFAULT_OPEN_TIMEOUT_SECS)
        record_using_skills

        connection.put do |req|
          setup_request(req, path, params, timeout, open_timeout)
        end
      end

      def delete(path,
        timeout = DEFAULT_REQUEST_TIMEOUT_SECS,
        open_timeout = DEFAULT_OPEN_TIMEOUT_SECS)
        connection.delete do |req|
          setup_request(req, path, nil, timeout, open_timeout)
        end
      end

      def attributes(includes = nil)
        Request::Attributes.new(self, includes: includes)
      end

      def attribute_values
        Request::AttributeValues.new(self)
      end

      def attribute(id)
        Request::Attribute.new(self, id: id)
      end

      def attribute_value(attribute_id, id)
        Request::AttributeValue.new(self, attribute_id: attribute_id, id: id)
      end

      def incremental_instance_values
        Request::IncrementalInstanceValues.new(self)
      end

      def base_url
        ENV['DECO_URL'] || kubernetes_service_url
      end

      private

      def url(path)
        format(
          URL,
          deco_url:   base_url,
          account_id: account.id,
          path:       path
        )
      end

      def kubernetes_service_url
        "http://deco.pod#{ENV['ZENDESK_POD_ID']}.svc.cluster.local."
      end

      def connection
        retry_options = {
          max: MAX_RETRIES
        }
        circuit_options = {
          failure_threshold: CIRCUIT_BREAKER_FAILURE_THRESHOLD,
          reset_timeout: CIRCUIT_BREAKER_RESET_TIMEOUT_SECS,
          dry_run: false
        }
        @connection ||= Kragle.new(APP_NAME,
          retry_options: retry_options,
          circuit_options: circuit_options)
      end

      def setup_request(req, path, params, timeout, open_timeout)
        req.url url(path)
        if params
          req.body = params
          req.headers['Content-Type'] = 'application/json'
        end

        req.options.timeout = timeout
        req.options.open_timeout = open_timeout
      end

      def record_using_skills
        # Record that this account is using skills based routing, if it is not
        # already recorded
        settings = @account.settings
        return if settings.using_skill_based_routing

        settings.using_skill_based_routing = true
        settings.save!
      end
    end
  end
end
