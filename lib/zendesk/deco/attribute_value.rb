module Zendesk
  module Deco
    AttributeValue = Struct.new(
      :attribute_id,
      :id,
      :name,
      :created_at,
      :updated_at,
      :deleted_at
    ) do
      def self.from_json(json)
        new(
          json['attribute_id'],
          json['id'],
          json['name'],
          json['created_at'],
          json['updated_at'],
          json['deleted_at']
        )
      end
    end
  end
end
