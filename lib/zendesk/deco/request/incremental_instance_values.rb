module Zendesk
  module Deco
    module Request
      class IncrementalInstanceValues
        def initialize(client)
          @client = client
        end

        def incremental(start_time, cursor)
          start_time ||= 0 if cursor.nil?
          query = {start_time: start_time, cursor: cursor}.compact.to_query
          timeout = client.account.settings.deco_client_incremental_timeout
          client.
            get("instance_values/incremental?#{query}", nil, timeout).
            body['instance_values'].
            map { |json| Zendesk::Deco::InstanceValue.from_json(json) }
        end

        protected

        attr_reader :client
      end
    end
  end
end
