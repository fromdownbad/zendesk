module Zendesk
  module Deco
    module Request
      class Attributes
        def initialize(client, includes:)
          @client = client
          @includes = includes
        end

        def index
          url = 'attributes'
          url << "?include=#{includes}" if includes.present?
          format_payload(client.get(url).body)
        end

        def incremental(start_time, cursor)
          start_time ||= 0 if cursor.nil?
          query = {start_time: start_time, cursor: cursor}.compact.to_query
          format_payload(client.get("attributes/incremental?#{query}").body)
        end

        def create(params)
          Zendesk::Deco::Attribute.from_json(
            client.post('attributes', params).body['attribute']
          )
        end

        protected

        attr_reader :client, :includes

        private

        def format_payload(payload)
          sideloads = includes.present? ? includes.split(',') : []
          json = {attributes: format_attributes(payload['attributes'])}

          if sideloads.include?('attribute_values')
            json[:attribute_values] =
              format_attribute_values(payload['attribute_values'] || [])
          end

          if sideloads.include?('agent_count')
            json[:agent_count] = payload['agent_count']
          end

          json
        end

        def format_attributes(attributes)
          attributes.map do |attribute|
            Zendesk::Deco::Attribute.from_json(attribute)
          end
        end

        def format_attribute_values(attribute_values)
          attribute_values.map do |attribute_value|
            Zendesk::Deco::AttributeValue.from_json(attribute_value)
          end
        end
      end
    end
  end
end
