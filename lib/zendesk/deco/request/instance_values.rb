module Zendesk
  module Deco
    module Request
      class InstanceValues
        PATH = 'attributes/%{attribute_id}/values/' \
          '%{attribute_value_id}/instance_values'.freeze

        private_constant :PATH

        def initialize(client, attribute_id:, attribute_value_id:)
          @client             = client
          @attribute_id       = attribute_id
          @attribute_value_id = attribute_value_id
        end

        def create(instance_id:, type_id:)
          client.post(path, instance_id: instance_id, type_id: type_id)
        end

        def index(type_id)
          client.get("#{path}?type_id=#{type_id}").
            body['instance_values'].
            map { |json| Zendesk::Deco::InstanceValue.from_json(json) }
        end

        protected

        attr_reader :client,
          :attribute_id,
          :attribute_value_id

        private

        def path
          format(
            PATH,
            attribute_id:       attribute_id,
            attribute_value_id: attribute_value_id
          )
        end
      end
    end
  end
end
