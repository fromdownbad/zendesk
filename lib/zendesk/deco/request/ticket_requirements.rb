module Zendesk
  module Deco
    module Request
      class TicketRequirements
        def initialize(client)
          @client = client
        end

        def fulfilled_ticket_ids(agent, ticket_ids)
          ticket_id_json = {requiring_ids: ticket_ids}.to_json
          # TODO: On timeout, log and return nil
          client.
            get("requirements/#{Zendesk::Deco::Client::TYPE_ID_TICKET}/" \
                 'fulfilled_by/' \
                 "#{Zendesk::Deco::Client::TYPE_ID_AGENT}/#{agent.id}",
              ticket_id_json).
            body['fulfilled_ids']
        end

        protected

        attr_reader :client
      end
    end
  end
end
