module Zendesk
  module Deco
    module Request
      class BulkInstanceValues
        PATH = 'attributes/%{attribute_id}/values/' \
          '%{attribute_value_id}/bulk_instance_values'.freeze
        INSTANCE_VALUES_PATH = 'types/%{type_id}/instances/%{instance_id}/' \
          'bulk_instance_values'.freeze

        private_constant :PATH

        def initialize(client)
          @client = client
        end

        def post(attribute_id,
          attribute_value_id,
          create_instance_ids: [],
          delete_instance_ids: [],
          type_id:)
          client.post(
            path(attribute_id, attribute_value_id),
            create_instance_ids: create_instance_ids,
            delete_instance_ids: delete_instance_ids,
            type_id:             type_id
          )
        end

        def post_instance_values(type_id, instance_id, attribute_value_ids)
          client.post(
            instance_values_path(type_id, instance_id),
            attribute_value_ids: attribute_value_ids
          ).
            body['items'].
            map do |att|
              att['values'].map do |val|
                Zendesk::Deco::AttributeValue.from_json val
              end
            end.flatten
        end

        protected

        attr_reader :client

        private

        def path(attribute_id, attribute_value_id)
          format(
            PATH,
            attribute_id:       attribute_id,
            attribute_value_id: attribute_value_id
          )
        end

        def instance_values_path(type_id, instance_id)
          format(
            INSTANCE_VALUES_PATH,
            type_id:     type_id,
            instance_id: instance_id
          )
        end
      end
    end
  end
end
