module Zendesk
  module Deco
    module Request
      class AttributeValues
        PATH = 'attributes/%{attribute_id}/values'.freeze
        PATH_FOR_PAGINATION = "#{PATH}?cursor=%{cursor}&limit=%{limit}".freeze
        PATH_FOR_INSTANCE = 'type/%{type_id}/instance/%{instance_id}/' \
          'attribute_values'.freeze

        private_constant :PATH
        private_constant :PATH_FOR_INSTANCE

        def initialize(client)
          @client = client
        end

        def index(attribute_id, cursor: nil, limit: nil)
          client.
            get(path(attribute_id, cursor, limit)).
            body['attribute_values'].
            map { |json| Zendesk::Deco::AttributeValue.from_json(json) }
        end

        def incremental(start_time, cursor)
          start_time ||= 0 if cursor.nil?
          query = {start_time: start_time, cursor: cursor}.compact.to_query
          client.
            get("attribute_values/incremental?#{query}").
            body['attribute_values'].
            map { |json| Zendesk::Deco::AttributeValue.from_json(json) }
        end

        def create(attribute_id, params)
          Zendesk::Deco::AttributeValue.from_json(
            client.post(path(attribute_id), params).body['attribute_value']
          )
        end

        def for_instance(type_id, instance_id)
          client.
            get(path_for_instance(type_id, instance_id)).
            body['items'].
            map do |att|
              att['values'].map do |val|
                Zendesk::Deco::AttributeValue.from_json(val)
              end
            end.flatten
        end

        def set_for_instance(type_id, instance_id, attribute_value_ids); end

        protected

        attr_reader :client,
          :attribute_id

        private

        def path(attribute_id, cursor = nil, limit = nil)
          if cursor
            return format(
              PATH_FOR_PAGINATION,
              attribute_id: attribute_id,
              cursor: cursor,
              limit: limit
            )
          end

          format(PATH, attribute_id: attribute_id)
        end

        def path_for_instance(type_id, instance_id)
          format(PATH_FOR_INSTANCE, type_id: type_id, instance_id: instance_id)
        end
      end
    end
  end
end
