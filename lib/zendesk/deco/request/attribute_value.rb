module Zendesk
  module Deco
    module Request
      class AttributeValue
        PATH = 'attributes/%{attribute_id}/values/%{id}'.freeze

        private_constant :PATH

        def initialize(client, attribute_id:, id:)
          @client       = client
          @attribute_id = attribute_id
          @id           = id
        end

        def show
          Zendesk::Deco::AttributeValue.from_json(
            client.get(path).body['attribute_value']
          )
        end

        def update(params)
          Zendesk::Deco::AttributeValue.from_json(
            client.put(path, params).body['attribute_value']
          )
        end

        def delete(timeout = nil)
          client.delete(
            path,
            timeout || Zendesk::Deco::Client::DEFAULT_REQUEST_TIMEOUT_SECS
          ).status == 204
        end

        def associate(instance, type_id:)
          instance_values.create(instance_id: instance.id, type_id: type_id)
        end

        def bulk_instances(create_instances: [], delete_instances: [], type_id:)
          bulk_instance_values.post(
            attribute_id,
            id,
            create_instance_ids: create_instances.map(&:id),
            delete_instance_ids: delete_instances.map(&:id),
            type_id:             type_id
          )
        end

        def associations(type_id)
          instance_values.index(type_id)
        end

        def instances_with_value(instances:, type_id:)
          client.
            get(
              "#{path}/instances_with_value",
              instance_ids: instances.map(&:id),
              type_id:      type_id
            ).
            body['instance_values'].
            map { |json| Zendesk::Deco::InstanceValue.from_json(json) }
        end

        protected

        attr_reader :client,
          :attribute_id,
          :id

        private

        def instance_values
          @instance_values ||= begin
            Request::InstanceValues.new(
              client,
              attribute_id:       attribute_id,
              attribute_value_id: id
            )
          end
        end

        def bulk_instance_values
          @bulk_instance_values ||= Request::BulkInstanceValues.new(client)
        end

        def path
          format(PATH, attribute_id: attribute_id, id: id)
        end
      end
    end
  end
end
