module Zendesk
  module Deco
    module Request
      class Attribute
        PATH = 'attributes/%{id}'.freeze

        private_constant :PATH

        def initialize(client, id:)
          @client = client
          @id     = id
        end

        def show
          Zendesk::Deco::Attribute.from_json(client.get(path).body['attribute'])
        end

        def update(params)
          Zendesk::Deco::Attribute.from_json(
            client.put(path, params).body['attribute']
          )
        end

        def delete
          client.delete(path).status == 204
        end

        def values
          Request::AttributeValues.new(client)
        end

        def value(value_id)
          Request::AttributeValue.new(client, attribute_id: id, id: value_id)
        end

        protected

        attr_reader :client,
          :id

        private

        def path
          format(PATH, id: id)
        end
      end
    end
  end
end
