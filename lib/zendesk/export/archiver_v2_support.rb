module Zendesk
  module Export
    module ArchiverV2Support
      include Api::V2::Tickets::EventViaPresenter

      private

      def tickets_with_archived(results)
        submitters_map = collection_cache(account.users, results, pluck_value(results, 'submitter_id'))
        requesters_map = collection_cache(account.users, results, pluck_value(results, 'requester_id'))
        ticket_forms_map = collection_cache(account.ticket_forms, results, pluck_value(results, 'ticket_form_id'))
        ticket_fields_map = ticket_fields(results)

        results.map do |ticket|
          entry = output_columns.each_with_object({}) do |column, acc|
            case column
            when :group
              acc['group_name'] = ticket['group_name'] || "-"
            when :assignee
              acc["assignee_name"] = ticket['assignee_name'] || ""
            when :assignee_external_id
              acc['assignee_external_id'] = ticket['assignee_external_id'] || ""
            when :organization
              acc["organization_name"] = ticket['organization_name'] || ""
            when :ticket_requester_id
              acc['req_id'] = ticket['requester_id'].to_s || ""
            when :requester_external_id
              acc['req_external_id'] = ticket['req_external_id'] || ""
            when :requester_email
              acc['req_email'] = ticket['req_email'] || ""
            when :brand
              acc['brand_name'] = ticket['brand_name'] || "-"
            when :ticket_form
              acc['ticket_form_name'] = ticket_forms_map[ticket['ticket_form_id']].try(:name) || "-"
            when :requester
              acc['req_name'] = requesters_map[ticket['requester_id']] && requesters_map[ticket['requester_id']].name || ""
            when :status
              acc['status'] = StatusType.to_s(ticket['status_id'].to_i)
            when :via
              acc['via'] = ViaType[ticket['via_id']].to_s || ticket['via_id']
            when :priority
              acc['priority'] = PriorityType.to_s(ticket['priority_id'].to_i)
            when :satisfaction_score
              acc['satisfaction_score'] = SatisfactionForGoodDataType.to_s(ticket['satisfaction_score'].to_i).gsub("Unoffered", "Not Offered")
            when :submitter
              acc["submitter_name"] = submitter_name(submitters_map, ticket)
            when :generated_timestamp
              acc['generated_timestamp'] = date_as_unix_timestamp(ticket, 'generated_timestamp')
            when :created_at
              acc['created_at'] = date_as_unix_timestamp(ticket, 'created_at')
            when :updated_at
              acc['updated_at'] = date_as_unix_timestamp(ticket, 'updated_at')
            when :assigned_at
              acc['assigned_at'] = date_as_unix_timestamp(ticket, 'assigned_at')
            when :due_date
              acc['due_date'] = date_as_unix_timestamp(ticket, 'due_date')
            when :initially_assigned_at
              acc['initially_assigned_at'] = date_as_unix_timestamp(ticket, 'initially_assigned_at')
            when :solved_at
              acc['solved_at'] = date_as_unix_timestamp(ticket, 'solved_at')
            else
              acc[column.to_s] = ticket[column.to_s] || ""
            end
          end

          if ticket['is_archived'] != 0
            archived = archived_custom_fields(ticket_fields_map, ticket)
            entry.merge!(archived) unless archived.nil?
          end
          entry
        end
      end

      def submitter_name(submitters_map, ticket)
        submitters_map[ticket['submitter_id']] &&
          submitters_map[ticket['submitter_id']].name ||
          ticket['submitter_id'] &&
          ::I18n.t("txt.admin.models.user.identification.deleted_label") ||
          ""
      end

      def date_as_unix_timestamp(ticket, field)
        return '' if ticket[field].nil?
        if ticket['is_archived'] != 0
          DateTime.parse(ticket[field].to_s).to_i
        else
          ticket[field]
        end
      end

      def archived_custom_fields(tfe_map, ticket)
        return if ticket['ticket_field_entries'].blank?
        @ticket_field_cache ||= {}
        ticket['ticket_field_entries'].each_with_object({}) do |tfe, acc|
          @ticket_field_cache[tfe['ticket_field_id']] ||= tfe_map[tfe['ticket_field_id']]
          ticket_field = @ticket_field_cache[tfe['ticket_field_id']]
          acc["field_#{ticket_field.id}"] = value_for_incremental_export(ticket_field, tfe['value']) if ticket_field
        end
      end

      def archive_v2_conditions
        return {} if include_deleted?
        # RAILS5UPGRADE: to remove this conditions hash, probably we need to modify https://github.com/zendesk/zendesk_archive/blob/master/lib/zendesk_archive/export_support.rb#L5
        {
          conditions: ["status_id in (?)", (StatusType.fields - Zendesk::Types::StatusType::INACCESSIBLE_LIST)]
        }
      end

      def archive_v2_options
        archive_v2_conditions.merge(
          extra_columns: select_sql,
          main_table_options: {
            joins: join_sql("tickets", "index_account_id_and_generated_timestamp_and_status_id")
          },
          stub_table_options: {
            joins: join_sql("tickets", nil)
          },
          order: "`tickets`.generated_timestamp",
          group: "`tickets`.nice_id",
          limit: limit_sql.gsub!(/LIMIT /, '')
        )
      end

      def ticket_fields(results)
        ticket_field_entries = pluck_value(results, 'ticket_field_entries')
        tfe_ids = ticket_field_entries.flat_map { |e| e.map { |a| a["ticket_field_id"] } }.uniq.compact
        collection_cache(account.ticket_fields, results, tfe_ids)
      end

      def pluck_value(array, value)
        array.map { |e| e[value] }.compact.uniq
      end

      def collection_cache(collection, _results, keys)
        return {} if keys.empty?
        collection.where(id: keys).to_a.each_with_object({}) do |u, acc|
          acc[u.id] = u
        end
      end
    end
  end
end
