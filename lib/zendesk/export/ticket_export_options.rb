module Zendesk
  module Export
    module TicketExportOptions
      MAXIMUM_COUNT = 10000
      DEFAULT_TIME  = "00".freeze

      def self.included(target)
        target.extend(ClassMethods)
      end

      module ClassMethods
        def build_options_hash(account)
          {
            timezone: linux_time_zone(account.time_zone),
            hour_offset: hour_offset_for_export(account),
          }
        end

        def linux_time_zone(time_zone)
          ActiveSupport::TimeZone[time_zone].tzinfo.identifier
        end

        def hour_offset_for_export(account)
          return "hourly" if has_hourly_report?(account)
          scheduled_hour_offset(account) || DEFAULT_TIME
        end

        def scheduled_hour_offset(account)
          return nil unless account.gooddata_integration
          Time.parse(account.gooddata_integration.scheduled_at).strftime('%H')
        end

        def has_hourly_report?(account) # rubocop:disable Naming/PredicateName
          account.subscription.has_hourly_incremental_export?
        end
      end
    end
  end
end
