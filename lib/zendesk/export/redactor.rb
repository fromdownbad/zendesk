require 'digest/md5'

module Zendesk
  module Export
    class Redactor
      DEFAULT_REDACTED_FIELDS = %w[req_email].freeze

      attr_accessor :redact_fields

      def self.for_account(account)
        new.tap do |t|
          t.redact_fields = redact_fields_for_account(account)
        end
      end

      def self.redact_fields_for_account(account)
        return [] unless account.has_redact_fields_for_incremental_export?
        DEFAULT_REDACTED_FIELDS
      end

      def redact(ticket)
        redact_fields.each do |f|
          ticket[f] = Digest::MD5.hexdigest(ticket[f]) if ticket[f]
        end
      end
    end
  end
end
