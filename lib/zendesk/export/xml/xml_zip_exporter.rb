require 'zip/zip'
require 'zip/zipfilesystem'

module Zendesk::Export
  module Xml
    class XmlZipExporter
      attr_accessor :batch_size
      attr_accessor :session

      EXPORT_MODELS   = [Ticket, Organization, User, Group, Category, Forum, Entry, Post].freeze
      EXPORT_INCLUDES = {
        Ticket => Ticket::SERIALIZATION_FINDER_INCLUDES.merge(linkings: { external_link: {} }),
        User   => Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES,
        Post   => Post::FINDER_INCLUDES,
        Entry  => Entry::DEFAULT_INCLUDES
      }.freeze
      # add serialization options to export user tags and group
      EXPORT_OPTIONS = {
          User => {include: [:groups], methods: [:current_tags] }
      }.freeze

      def self.build_for_account(account, session = Time.now.to_i)
        xml_zip_exporter = new(account, session)
        xml_zip_exporter.direct_export
      end

      def initialize(account, session)
        @account = account
        @session = session
        @export_date = Time.now.strftime("%Y%m%d")
        @xml_dir_prefix = "xml-export"
        @include_account = true
        # We keep the batch size low by default given that tickets include all
        # comments and attchments per batch.
        @batch_size = 100
      end

      def xml_file_path(model)
        "#{@xml_output_dir_name}/#{model.to_s.pluralize.downcase}.xml"
      end

      def xml_dir_name
        @account.subdomain + "-" + @export_date
      end

      def direct_export
        @xml_output_dir_name = xml_dir_name
        zipfile = Tempfile.new(["#{@xml_dir_prefix}-#{@xml_output_dir_name}", ".zip"])
        Zip::OutputStream.open(zipfile.path) do |zos|
          if @include_account
            zos.put_next_entry(xml_file_path(Account))
            zos.write(@account.to_xml)
          end

          self.class::EXPORT_MODELS.each do |model|
            model_tag = model.to_s.pluralize.downcase
            logger.info("#{self.class.name}: Exporting #{model_tag} for #{zipfile.path} at #{Time.now} [#{session}]")
            zos.put_next_entry(xml_file_path(model))
            zos.puts(%(<?xml version="1.0" encoding="UTF-8"?>))
            zos.puts(%(<#{model_tag} report_date="#{@export_date}">))

            find_options = { conditions: "account_id = #{@account.id}", batch_size: @batch_size }
            if includes = self.class::EXPORT_INCLUDES[model]
              find_options[:include] = includes
            end

            # :skip_instruct and :indent are the default serialization options for all objects
            serialization_options = {skip_instruct: :true, indent: 2}
            # add serialization options to show user tags and groups
            if options = self.class::EXPORT_OPTIONS[model]
              serialization_options.merge!(options)
            end

            ActiveRecord::Base.with_slave do
              add_to_xml(zos, model, find_options, serialization_options)
              if model == Ticket
                stub_options = find_options.clone
                stub_options.delete(:include)
                batch_size = stub_options[:batch_size]
                TicketArchiveStub.where(stub_options[:conditions]).find_in_batches(batch_size: batch_size) do |stubs|
                  tickets = Ticket.archive_fill_from_stubs(stubs)
                  tickets.each do |t|
                    zos.puts(t.to_xml(serialization_options))
                  end
                end
              end
            end

            zos.puts(%(</#{model_tag}>))
            logger.info("#{self.class.name}: Finished exporting #{model_tag} for #{zipfile.path} at #{Time.now} [#{session}]")
          end # each @models

          zos.put_next_entry("#{@xml_output_dir_name}/README")
          zos.puts(readme)
        end
        logger.info("#{self.class.name}: Finished exporting to #{zipfile.path} at #{Time.now} [#{session}]")
        zipfile
      end

      # TODO: this could be factored out to some fancier templating once we
      #   decide if we need to provide any details in here.
      def readme
        <<~README
          This is an export for the following Zendesk data sets:
          #{print_data_sets}

          Date: #{@export_date}
          Account: #{@account.name}
          Subdomain: #{@account.subdomain}
        README
      end

      def print_data_sets
        "\n  " + (@include_account ? "Accounts\n  " : "") + self.class::EXPORT_MODELS.collect { |m| m.to_s.pluralize }.join("\n  ")
      end

      def add_to_xml(zos, model, find_options, serialization_options = {})
        # Tickets->Comments->Attachments eager loading SQL queries get too large and slow with some account. They time out in mysql.
        # Keep the batch size below 50 tickets.
        batch_size = find_options[:batch_size]
        batch_size = 50 if model == Ticket && batch_size > 50

        model_with_where = model.where(find_options[:conditions])
        model_with_where = model_with_where.includes(find_options[:include]) if find_options[:include]

        model_with_where.find_in_batches(batch_size: batch_size) do |group|
          group.each do |record|
            zos.puts(record.to_xml(serialization_options))
          end
        end
      end

      protected

      def logger
        XmlExportJob.logger
      end
    end
  end
end
