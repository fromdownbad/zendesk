module Zendesk::Export
  module Xml
    class UserXmlZipExporter < XmlZipExporter
      EXPORT_MODELS   = [Organization, User, Group].freeze
      # add :groups and :current_tags serialization options so the bulk export is consistent with .../users.xml
      EXPORT_OPTIONS  = {
        User => {include: [:groups], methods: [:current_tags] }
      }.freeze

      def initialize(account, session)
        super(account, session)
        @xml_dir_prefix = "user-xml-export"
        @include_account = false
      end

      protected

      def logger
        UserXmlExportJob.logger
      end
    end
  end
end
