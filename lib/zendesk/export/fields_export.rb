module Zendesk
  module Export
    module FieldsExport
      def fields_for_export(organization_fields, exclude_non_exportable_fields)
        if exclude_non_exportable_fields
          organization_fields.select { |key, _value| exportable_field_keys.include?(key) }
        else
          organization_fields
        end
      end

      def exportable_field_keys
        # Need to explicitly check for false here because there is no default on `is_exportable`
        # and AR returns NULL DB values as false.
        @exportable_field_keys ||= @custom_fields.select { |f| f.read_attribute(:is_exportable) != false }.map(&:key)
      end
    end
  end
end
