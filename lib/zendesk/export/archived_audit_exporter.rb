module Zendesk
  module Export
    class ArchivedAuditExporter
      AUDIT_PARENT_EVENT_TYPES = %w[Audit TicketMergeAudit].freeze
      AUDIT_CHILD_EVENT_TYPES = %w[FacebookComment VoiceComment VoiceApiComment Comment Change Create].freeze

      def initialize(account, archive_items, limit)
        @account = account
        @archive_items = archive_items
        @limit = limit
      end

      def get_archived_audits # rubocop:disable Naming/AccessorMethodName
        return [] unless @archive_items && @archive_items.any?

        stubs = archived_ticket_stubs
        archived_ticket_blobs = blobs_from_archive(stubs)
        events = events_from_archived_tickets(archived_ticket_blobs)

        audit_events = audit_events(events)
        child_events = child_events(events)

        associate_child_events(audit_events, child_events)
        associate_tickets(audit_events, stubs)

        audit_events.map { |a| a.merge(source: :archive) }
      end

      private

      def audit_events(events)
        events.select { |e| AUDIT_PARENT_EVENT_TYPES.include?(e[:type]) }.sort_by { |e| e[:created_at] }
      end

      def child_events(events)
        events.select { |e| AUDIT_CHILD_EVENT_TYPES.include?(e[:type]) }
      end

      def events_from_archived_tickets(blobs)
        blobs.flat_map do |ticket|
          ticket.data['events'].each do |event|
            event.symbolize_keys!
            event[:created_at] = Time.zone.parse(event[:created_at] || event[:updated_at])
          end
        end
      end

      def archived_ticket_stubs
        stub_ids = @archive_items.map { |item| item[:id] }
        Ticket.on_slave do
          @account.ticket_archive_stubs.where(id: stub_ids).to_a
        end
      end

      def blobs_from_archive(archived_ticket_stubs)
        ZendeskArchive.router.get_multi_list(archived_ticket_stubs)
      end

      def associate_tickets(audits, stubs)
        stub_data = stubs.map { |stub| stub.attributes.transform_keys(&:to_sym) }
        stubs_by_id = stub_data.group_by { |t| t[:id] }
        audits.each do |audit|
          audit[:ticket] = stubs_by_id[audit[:ticket_id]] && stubs_by_id[audit[:ticket_id]].first
        end
      end

      def associate_child_events(audits, child_events)
        events_by_parent_id = child_events.group_by { |e| e[:parent_id] }
        audits.each do |audit|
          audit[:events] = events_by_parent_id[audit[:id]] || []
        end
      end
    end
  end
end
