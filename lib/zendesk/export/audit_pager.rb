# Take 1 or 2 lists of audit raw data objects and make them pageable by
# the limit provided and their created_at date. Oldest audits first.
class Zendesk::Export::AuditPager
  def initialize(limit, start_time)
    @limit = limit
    @start_datetime = Time.at(start_time)
  end

  def page(database_events, archive_events = [])
    if database_events.empty? && archive_events.empty?
      records = []
      next_timestamp = nil
      return [records, next_timestamp]
    end

    page_combined_sources(database_events, archive_events)
  end

  protected

  def page_combined_sources(database_events, archive_events)
    # Events are stored in the archiver as a single unsplittable group,
    # so we need to output either all the events from an archived ticket
    # or none.
    # For standard ticket events, we can process them individually.
    archive_event_groups = group_archive_events_by_ticket(archive_events)
    database_event_groups = group_database_events_by_event(database_events)
    ordered_groups = (archive_event_groups + database_event_groups).sort_by { |group| group[:end_time] }

    # Return objects cut down to the limit if the resulting set would have different created_at dates at the start and
    # end, otherwise return *all* of the audits for the oldest created_at date because there may be more records with the same
    # time than the limit allows
    take_limited_events(ordered_groups)
  end

  def group_archive_events_by_ticket(archive_events)
    sorted_events = archive_events.sort_by { |a| a[:created_at] }
    events_by_ticket_id = sorted_events.group_by { |event| event[:ticket_id] }
    # Archived ticket events must always be exported as a group, they can have varying timestamps
    # We use the end_time from the archive ticket for sorting the events, the actual event time is unchanged.
    events_by_ticket_id.collect do |ticket_id, events|
      if events.first[:ticket].present?
        {
          events: events,
          end_time: events.first[:ticket][:generated_timestamp]
        }
      else
        Rails.logger.info("Skipping audit #{events.first[:id]}: No ticket could be associated for event.ticket_id ##{ticket_id}")
        nil
      end
    end.compact
  end

  def group_database_events_by_event(database_events)
    database_events.collect do |event|
      generated_timestamp = event[:ticket] && event[:ticket][:generated_timestamp]
      end_time = if event[:created_at] > generated_timestamp
        generated_timestamp
      else
        event[:created_at]
      end

      {
        events: [event],
        end_time: end_time
      }
    end
  end

  def take_limited_events(groups)
    minimum_end_time = [groups.first[:end_time], @start_datetime].max
    count = 0

    # Output the events, group by group until we go over the limit
    # or get past the minimum end time
    groups_in_page = groups.take_while do |group|
      not_enough_events = count < @limit
      count += group[:events].length

      not_enough_events || group[:end_time] <= minimum_end_time
    end

    events = groups_in_page.flat_map { |group| group[:events] }

    # return the last event's end_time instead of incrementing the last event end time by
    # one because there can be more items at that end time not included in the results.
    # In the case where there are more items with the same time than the limit return all items
    # and return the last events end_time incremented by one
    end_time = groups_in_page.last[:end_time]
    end_time = minimum_end_time + 1 if end_time <= minimum_end_time && events.length >= @limit

    [events, end_time.to_i]
  end
end
