module Zendesk
  module Export
    class TicketExport
      TICKETS_HEADER = [
        'Summation column', 'Id', 'Requester', 'Requester id', 'Requester external id', 'Requester email', 'Requester domain', 'Submitter', 'Assignee', 'Group', 'Subject', 'Tags',
        'Status', 'Priority', 'Via', 'Ticket type', 'Created at', 'Updated at', 'Assigned at', 'Organization', 'Due date',
        'Initially assigned at', 'Solved at', 'Resolution time'
      ].freeze

      TICKET_COLUMNS = [
        :nice_id, :requester, :ticket_requester_id, :requester_external_id, :requester_email, :domain, :submitter, :assignee, :group, :subject, :current_tags, :status, :priority, :via, :ticket_type,
        :created_at, :updated_at, :assigned_at, :organization, :due_date, :initially_assigned_at, :solved_at, :resolution_time
      ].freeze

      DATE_COLUMNS = [:created_at, :updated_at, :assigned_at, :due_date, :initially_assigned_at, :solved_at].freeze

      RAW_COLUMNS = [:generated_timestamp, :nice_id].freeze

      CUSTOM_FIELD_TYPES = {
        'FieldTagger' => 'list', 'FieldCheckbox' => 'flag', 'FieldText' => 'txt', 'FieldInteger' => 'int', 'FieldDecimal' => 'dec', 'FieldRegexp' => 'txt', 'FieldMultiselect' => 'list'
      }.freeze

      def headers(has_multiple_ticket_forms: nil)
        headers = TICKETS_HEADER.dup

        if show_metrics?
          headers << 'Satisfaction Score'
          headers += metrics_headers
        else
          headers.delete('Requester id')
          headers.delete('Requester external id')
          headers.delete('Requester email')
        end

        if account.has_multiple_brands?
          headers << 'Brand'
        end

        if has_multiple_ticket_forms || (has_multiple_ticket_forms.nil? && account.ticket_forms.limit(2).count > 1)
          headers << 'Ticket Form'
        end

        headers |= additional_columns.map(&:first) unless additional_columns.blank?

        headers + custom_headers
      end

      def show_metrics?
        account.extended_ticket_metrics_visible? &&
        (account.settings.only_extended_metrics? || show_metrics)
      end

      attr_accessor :account, :updated_on, :show_metrics, :min_ts, :limit, :escape_fields, :map_nice_id, :additional_columns

      module Sql
        def include_deleted?
          false
        end

        def sql
          <<-SQL
            SELECT #{select_sql.join(', ')}
            FROM tickets #{join_sql('tickets', 'index_account_id_and_generated_timestamp_and_status_id')}
            GROUP BY tickets.nice_id
            #{order_sql}
            #{limit_sql}
          SQL
        end

        def order_sql
          "ORDER BY #{order} "
        end

        def limit_sql
          @limit ? "LIMIT #{limit}" : ""
        end

        protected

        def select_sql
          select = []
          select << "1 AS 'Summation column'"
          select << column_selection_sql
          select.flatten
        end

        def join_sql(table, subselect_force_key)
          fields = output.fields_to_fom
          fields -= [Zendesk::Fom::SubmitterField, Zendesk::Fom::TicketFormField]
          joins = fields.collect(&:csv_join).compact.uniq.join(" ")

          joins << " LEFT OUTER JOIN user_identities as requester_identity"
          joins << " ON (requester_identity.user_id = requester_.id"
          joins << " AND #{UserEmailIdentity.type_condition_sql.sub('`user_identities`', '`requester_identity`')}"
          joins << " AND requester_identity.priority = 1)"

          force_key = subselect_force_key ? "FORCE KEY(`#{subselect_force_key}`)" : nil

          joins << " JOIN (select id from `#{table}` #{force_key} where #{conditions_sql} order by #{order_column} #{limit_sql}) ticket_conds on ticket_conds.id = `tickets`.id"

          if show_metrics?
            joins << " LEFT OUTER JOIN ticket_metric_sets ON ticket_metric_sets.ticket_id = tickets.id"
          end

          joins
        end

        def conditions_sql
          exclude_status_ids = [StatusType.ARCHIVED]
          exclude_status_ids << StatusType.DELETED unless include_deleted?
          status_ids = (StatusType.fields - exclude_status_ids)

          "account_id = #{account.id.to_i} AND status_id in (#{status_ids.join(',')})"
        end

        def outfile_sql
        end

        def column_selection_sql
          columns = output.columns
          columns = columns - TICKET_COLUMNS - [:satisfaction_score, :generated_timestamp, :ticket_form]
          columns -= additional_columns_names unless additional_columns.blank?
          columns += [:organization, :domain, :requester_email, :requester_external_id, :ticket_requester_id, :status, :ticket_type, :group, :assignee, :assignee_external_id]

          columns.map do |column|
            name =
              case column
              when :domain
                "IFNULL(LOWER(SUBSTRING(requester_identity.value,INSTR(requester_identity.value,'@')+1)), '') AS 'domain'"
              when :requester_email
                "IFNULL(requester_identity.value, '') AS 'req_email'"
              when :ticket_requester_id
                "IFNULL(requester_.id, '') AS 'req_id'"
              when :requester_external_id
                "IFNULL(requester_.external_id, '') AS 'req_external_id'"
              when :status
                to_enumerable_sql(StatusType, column)
              when :priority
                to_enumerable_sql(PriorityType, column)
              when :via
                to_enumerable_sql(ViaType, column)
              when :ticket_type
                to_enumerable_sql(TicketType, column)
              when :satisfaction_score
                to_enumerable_sql(SatisfactionForGoodDataType, column).gsub(/-|_id/, "").gsub("Unoffered", "Not Offered")
              when :assignee
                "IFNULL(assignee_.name, '') AS 'assignee_name'"
              when :assignee_id
                "IFNULL(assignee_.id, '') AS 'assignee_id'"
              when :assignee_external_id
                "IFNULL(assignee_.external_id, '') AS 'assignee_external_id'"
              when :group
                "IFNULL(groups.name, '-') AS 'group_name'"
              when :group_id
                "IFNULL(groups.id, '') AS 'group_id'"
              when :brand
                "IFNULL(brands.name, '-') AS 'brand_name'"
              when :id
                "tickets.id AS 'real_id'"
              when :nice_id
                "tickets.nice_id AS 'nice_id'"
              when :generated_timestamp
                "UNIX_TIMESTAMP(tickets.generated_timestamp) AS generated_timestamp"
              else
                if show_metrics? && metrics.include?(column)
                  "IFNULL(ticket_metric_sets.#{column}, '') AS #{column}"
                else
                  field = Zendesk::Fom::Field.for(column)
                  if join_field = field.try(:csv_join_field_alias)
                    join_field
                  else
                    db_field = "REPLACE(IFNULL(tickets.#{field.try(:db_field) || column}, ''), '\0','')"
                    db_field = "#{db_field} AS #{column}" unless DATE_COLUMNS.include?(column)
                    db_field
                  end
                end
              end
            if DATE_COLUMNS.include?(column)
              "IFNULL(#{in_time_zone(name)}, '') AS #{column}"
            elsif !RAW_COLUMNS.include?(column) && escape_fields
              escape(name)
            else
              name
            end
          end
        end

        def to_enumerable_sql(type, column)
          list = Array.new(type.fields.max + 1).each_with_index.map do |_, id|
            "'#{type[id].try(:to_s) || type.default_placeholder}'"
          end
          "IFNULL(ELT(tickets.#{column}_id+1, #{list.join(',')}), '#{type.default_placeholder}') AS #{column}"
        end

        # note: this method is overridden in IncrementalTicketExport#in_time_zone
        def in_time_zone(date_field)
          timezone = ActiveSupport::TimeZone[account.time_zone]
          tz_info_name = timezone.try(:tzinfo).try(:name)
          mysql_date_format("CONVERT_TZ(#{date_field}, 'UTC', '#{tz_info_name}')", timezone)
        end

        def mysql_date_format(date_field, _timezone)
          "DATE_FORMAT(#{date_field}, '%Y-%m-%d %H:%i')"
        end

        # http://tools.ietf.org/html/rfc4180
        def escape(field)
          column, name = field.split(' AS ')
          escaped_sql  = "REPLACE(#{column}, '\"', '\"\"')"
          sql_alias    = name ? name : column

          escaped_sql << " AS #{sql_alias}"
          escaped_sql
        end

        def header_title_for_field(field)
          if show_metrics?
            "#{field.title} [#{CUSTOM_FIELD_TYPES[field.class.name]}]"
          else
            field.title
          end
        end

        def custom_headers
          custom.map { |field| header_title_for_field(field) }
        end

        def custom
          @custom ||= begin
            custom = account.ticket_fields.active.custom.exportable.where(type: CUSTOM_FIELD_TYPES.keys).to_a
            truncated_custom_fields(custom)
          end
        end

        def metrics
          @metrics ||= TicketMetricSet.attributes_for_reports
        end

        def metrics_headers
          metrics.map(&:humanize)
        end

        def output_columns
          columns = TICKET_COLUMNS.dup

          if show_metrics?
            columns << :satisfaction_score
            columns += metrics
          else
            columns.delete(:ticket_requester_id)
            columns.delete(:requester_external_id)
            columns.delete(:requester_email)
          end

          if account.has_multiple_brands?
            columns << :brand
          end

          if account.ticket_forms.count > 1
            columns << :ticket_form
          end

          # add extra columns - must be grabbed already
          columns |= additional_columns_names unless additional_columns.blank?

          columns
        end

        def additional_columns_names
          additional_columns.map(&:last)
        end

        def order_column
          :nice_id
        end

        def order
          order_column
        end

        def output
          @output ||= Output.create(type: 'table', columns: output_columns, order: order, order_asc: true, format: :csv)
        end
      end
      include Sql
    end
  end
end
