module Zendesk
  module Export
    class CommentsFinder
      attr_reader :account, :ticket_ids, :id_waterline, :comment_id_waterline, :max_comments

      def initialize(account, ticket_ids, id_waterline, comment_id_waterline, limit)
        @account = account
        @ticket_ids = ticket_ids
        @id_waterline = id_waterline.to_i
        @comment_id_waterline = comment_id_waterline.to_i
        @max_comments = limit
      end

      def find_comments
        all_comments.slice(0, max_comments)
      end

      def all_comments
        tickets_with_comments_query.inject([]) do |result, ticket|
          break result if result.size >= max_comments
          result + comments_above_waterline(ticket)
        end
      end

      def comments_above_waterline(ticket)
        comments = ticket.comments.to_a
        waterline_ticket?(ticket) ? remaining(comments) : comments
      end

      def waterline_ticket?(ticket)
        ticket.nice_id == id_waterline
      end

      def remaining(comments)
        index = comment_id_start_index(comments)
        comments.slice(index, comments.size)
      end

      def query_ids
        @query_ids ||= ticket_ids.slice(start_index, ticket_ids.size)
      end

      def start_index
        return 0 if id_waterline.zero?
        ticket_ids.index(id_waterline) || 0
      end

      def comment_id_start_index(comments)
        return 0 if comment_id_waterline.zero?
        index = comments.find_index { |comment| comment['id'] == comment_id_waterline }
        index.nil? ? 0 : index + 1
      end

      def tickets_with_comments_query
        riak_ids = query_ids - mysql_tickets.map(&:nice_id)
        (riak_proxies(riak_ids) + mysql_tickets).sort_by(&:nice_id)
      end

      def riak_proxies(riak_ids)
        return [] if riak_ids.empty?
        @account.tickets.where(nice_id: riak_ids).all_with_archived(stub_only: true).to_a
      end

      def mysql_tickets
        @mysql_tickets ||= begin
          # this section is only for non-archived tickets, archived are not returned by this query
          tickets = @account.tickets.where(nice_id: query_ids).order('nice_id ASC').to_a
          return [] if tickets.blank? # all tickets in the page are archived ones

          comments = []
          ticket_ids = tickets.map(&:id)

          # the check ensures that if the waterline ticket is an archived, we won't use its associated comment_waterline
          if waterline_ticket?(tickets.first)
            comments.concat Comment.where(ticket_id: ticket_ids.shift, account_id: @account.id).
              where("id > ?", comment_id_waterline).
              includes(:attachments).
              order("id ASC").
              limit(max_comments)
          end

          if comments.size < max_comments
            comments.concat Comment.where(ticket_id: ticket_ids, account_id: @account.id).
              joins(:ticket).
              includes(:attachments).
              order("tickets.nice_id ASC, events.id ASC").
              limit(max_comments - comments.size)
          end

          comments_index = comments.group_by(&:ticket_id)
          tickets.each { |ticket| preload_comments(comments_index, ticket) }
          tickets
        end
      end

      def preload_comments(comments_index, ticket)
        association = ticket.association(:comments)
        association.loaded!
        comments = comments_index.fetch(ticket.id, [])
        association.target.concat(comments)
        comments.each { |comment| association.set_inverse_instance(comment) }
      end
    end
  end
end
