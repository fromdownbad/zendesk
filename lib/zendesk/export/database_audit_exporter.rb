module Zendesk
  module Export
    class DatabaseAuditExporter
      AUDIT_CHILD_EVENT_TYPES = %w[FacebookComment VoiceComment VoiceApiComment Comment Change Create TicketSharingEvent TicketUnshareEvent CommentRedactionEvent].freeze
      def initialize(options)
        @db = options[:db]
        @audits = options[:audits]
        @event_scope = options[:event_scope]
        @ticket_scope = options[:ticket_scope]
      end

      def get_audits # rubocop:disable Naming/AccessorMethodName
        populate_all_related_tickets(@audits, @ticket_scope)
        populate_all_child_events(@audits, @event_scope)

        @audits.each do |audit|
          audit[:source] = :database
        end
        filter_out_audits_with_no_ticket(@audits)
      end

      def filter_out_audits_with_no_ticket(audits)
        audits.select { |a| a[:ticket] }
      end

      def populate_all_child_events(audits, event_scope)
        audit_ids = audits.map { |a| a[:id] }
        events = @db.get_raw_data(event_scope.where(parent_id: audit_ids).where(type: AUDIT_CHILD_EVENT_TYPES))

        events_by_parent_id = events.group_by { |e| e[:parent_id] }
        audits.each do |audit|
          audit[:events] = events_by_parent_id[audit[:id]] || []
        end
      end

      def populate_all_related_tickets(audits, ticket_scope)
        related_ticket_ids = audits.map { |a| a[:ticket_id] }
        tickets = @db.get_raw_data(ticket_scope.where(id: related_ticket_ids))

        associate_tickets(audits, tickets)
      end

      def associate_tickets(audits, tickets)
        tickets_by_id = tickets.group_by { |t| t[:id] }
        audits.each do |audit|
          audit[:ticket] = tickets_by_id[audit[:ticket_id]] && tickets_by_id[audit[:ticket_id]].first
        end
      end
    end
  end
end
