class Zendesk::Export::IncrementalExportArchiveFinder < Zendesk::Export::IncrementalExportFinder
  # Remediation for inc-2019-07-15-b
  #
  # The MySQL 5.6 query planner already correct picks this index but a bug
  # causes it to use a "ref" comparison instead of "range" comparison, causing
  # multi-minute query times. Forcing the index causes a correct range "type".
  FORCE_INDEX = "index_account_id_and_generated_timestamp_and_status_id".freeze
  CURSOR_FORCE_INDEX = "index_account_id_generated_timestamp_nice_id".freeze

  def cursor_results
    query = super

    # If we have a cursor present, the time conditions will have been added by
    # zendesk_cursor_pagination, otherwise we need this here
    query = query.where(Ticket.send(:sanitize_sql, ["#{@update_ts_field} >= ?", Time.at(@start_time)])) unless @cursor

    query = query.where(Ticket.send(:sanitize_sql, ["#{@update_ts_field} < ?", 1.minute.ago])) if @enforce_last_minute_limit

    query.all_with_archived(
      force_index: CURSOR_FORCE_INDEX,
      archive_force_index: CURSOR_FORCE_INDEX,
      order: "#{@update_ts_field} asc, nice_id asc",
      limit: @limit,
      include: @includes
    )
  end

  def limited_results
    query = @export_association.where(Ticket.send(:sanitize_sql, ["#{@update_ts_field} >= ?", Time.at(@start_time)]))

    query = query.where(Ticket.send(:sanitize_sql, ["#{@update_ts_field} < ?", 1.minute.ago])) if @enforce_last_minute_limit

    query = query.where.not(status_id: StatusType.DELETED) if @exclude_deleted

    query.all_with_archived(
      force_index: FORCE_INDEX,
      archive_force_index: FORCE_INDEX,
      order: @update_ts_field,
      limit: @limit,
      include: @includes
    )
  end

  def unlimited_results_for_time(exact_ts)
    @export_association.where(Ticket.send(:sanitize_sql, ["#{@update_ts_field} = ?", Time.at(exact_ts)])).all_with_archived(
      force_index: FORCE_INDEX,
      archive_force_index: FORCE_INDEX,
      order: "id",
      include: @includes
    )
  end
end
