module Zendesk
  module Export
    class IncrementalTicketMetricEventsFinder
      def initialize(options = {})
        @account     = options[:account]
        @association = options[:association]
        @start_time  = options[:start_time]
        @limit       = options[:limit]

        @metric_events = association.arel_table
        @current_time  = Time.now
      end

      def results
        @results ||= begin
          events.map do |event|
            EventResult.new(
              event,
              event.ticket || archived_tickets[event.ticket_id]
            )
          end
        end
      end

      def next_timestamp
        @next_timestamp ||= begin
          if limit_for_time_exceeded?(results)
            results.last.time.to_i.succ
          else
            results.last.try { |event| event.time.to_i }
          end
        end
      end

      protected

      attr_reader :account,
        :association,
        :current_time,
        :limit,
        :metric_events,
        :start_time

      private

      def limit_for_time_exceeded?(results)
        results.length >= limit && results.first.time == results.last.time
      end

      def archived_tickets
        @archived_tickets ||= begin
          account.
            ticket_archive_stubs.
            where(id: events.pluck(:ticket_id)).
            each_with_object({}) do |archived_ticket, archived_tickets|
              archived_tickets.store(archived_ticket.id, archived_ticket)
            end
        end
      end

      def events
        @events ||= begin
          if limit_for_time_exceeded?(limited_results)
            unlimited_results_for_time(limited_results.first.time)
          else
            limited_results
          end
        end
      end

      def limited_results
        @limited_results ||= begin
          association.where(
            metric_events[:time].gteq(Time.at(start_time)).
              and(metric_events[:time].lteq(current_time))
          ).from(
            'ticket_metric_events FORCE INDEX ' \
              '(index_ticket_metric_events_on_account_id_and_time)'
          ).limit(limit)
        end
      end

      def unlimited_results_for_time(exact_ts)
        association.where(metric_events[:time].eq(Time.at(exact_ts))).
          order(:id).
          from(
            'ticket_metric_events FORCE INDEX ' \
              '(index_ticket_metric_events_on_account_id_and_time)'
          )
      end

      class EventResult < SimpleDelegator
        def initialize(event, ticket)
          @ticket = ticket

          super(event)
        end

        attr_reader :ticket
      end
    end
  end
end
