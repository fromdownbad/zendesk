require "zendesk/internal_api/client"

module Zendesk
  module Export
    class ConfigurationNotifier
      def initialize(account)
        self.account = account
      end

      def create_enable_exports_request
        create_support_ticket(attributes_for_enable_ticket)
      end

      def create_exceeded_export_max_notice
        create_support_ticket(attributes_for_exceeded_max_ticket)
      end

      private

      attr_accessor :account

      def create_support_ticket(attributes)
        internal_client.tickets.create(attributes)
      rescue Faraday::Error::ClientError
        false
      end

      SERVICES_GROUP_NAME         = "Services".freeze
      PROGRAM_SERVICES_GROUP_NAME = "Support (PS)".freeze

      PRODUCTION_GROUP_NAME_ID_MAP = {
        SERVICES_GROUP_NAME         => 20097032,
        PROGRAM_SERVICES_GROUP_NAME => 20751853
      }.freeze

      def attributes_for_enable_ticket
        common_attributes_for_ticket.merge(
          subject: enable_subject,
          description: enable_description,
          group_id: group_id_for(PROGRAM_SERVICES_GROUP_NAME)
        )
      end

      def attributes_for_exceeded_max_ticket
        common_attributes_for_ticket.merge(
          subject: exceeded_max_subject,
          description: exceeded_max_description,
          group_id: group_id_for(SERVICES_GROUP_NAME)
        )
      end

      def group_id_for(name)
        group_id = internal_client.groups.detect { |g| g.name == name }.try(:id)

        return group_id if group_id
        return PRODUCTION_GROUP_NAME_ID_MAP[name] if Rails.env.production?
      end

      def common_attributes_for_ticket
        ticket_fields_hash.merge(
          type: TicketType.TASK.to_s,
          requester: account.owner.email,
          submitter_id: internal_client.current_user.id
        )
      end

      def internal_client
        @internal_client ||= Zendesk::InternalApi::Client.new("support")
      end

      def ticket_fields_hash
        if subdomain_id
          { fields: {subdomain_id => account.subdomain} }
        else
          {}
        end
      end

      def subdomain_id
        internal_client.ticket_fields.detect { |f| f.title == "Subdomain" }.try(:id)
      end

      def exceeded_max_subject
        ::I18n.t("txt.admin.controllers.jobs_controller.export_job_maximum_exceeded_subject")
      end

      def exceeded_max_description
        ::I18n.t(
          "txt.admin.controllers.jobs_controller.export_job_maximum_exceeded_description",
          subdomain: account.subdomain
        )
      end

      def enable_subject
        ::I18n.t("txt.admin.controllers.settings.export_configuration_controller.data_export_request_subject")
      end

      def enable_description
        ::I18n.t(
          "txt.admin.controllers.settings.export_configuration_controller.data_export_request_description",
          subdomain: account.subdomain
        )
      end
    end
  end
end
