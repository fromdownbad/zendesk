module Zendesk::Export
  class BillingReport < BaseReport
    require 'zip/zip'
    require 'zip/zipfilesystem'

    class << self
      def nfs_filename
        "#{output_path}/billing_report.zip"
      end

      def filename
        'billing_report.zip'
      end

      def execute
        report_types = [
          {
            class: ::Account,
            order: 'id',
            fields: %w[
              id
              is_active
              name
              subdomain
              created_at
              owner_id
              is_serviceable
              shard_id
            ]
          },
          {
            class: ::Subscription,
            order: 'id',
            fields: %w[
              id
              account_id
              billing_cycle_type
              pricing_model_revision
              created_at
              updated_at
              plan_type
              max_agents
              copy_invoices_to
              manual_discount
              manual_discount_expires_on
              comment
              trial_expires_on
            ]
          },
          {
            class: ::Payment,
            order: 'charged_at ASC',
            fields: %w[
              id
              account_id
              period_begin_at
              period_end_at
              created_at
              updated_at
              status
              transaction_id
              charged_at
              amount
              vat
              invoice_no
              failures
              net
              discount
              is_one_time
              token
              plan_id
              billing_cycle_id
              subscription_id
              billing_cycle_type
              plan_type
              max_agents
              manual_discount
              payment_version_type
              payment_gateway_type
              payment_gateway_reference
              payment_method_type
              feature_type
            ]
          }
        ]

        report_types.each { |type| type[:report] = generate_report(type) }

        zip_temp = Tempfile.new('billing_report')

        Zip::OutputStream.open(zip_temp.path) do |zos|
          report_types.each do |type|
            zos.put_next_entry("billing_#{type[:class].name.downcase}.tsv")
            zos << type[:report]
          end
        end

        begin
          write_file(nfs_filename, zip_temp)
          write_to_cloud(zip_temp)
        ensure
          zip_temp.close
          zip_temp.unlink
        end

        true
      end

      private

      def generate_report(type)
        CSV.generate(col_sep: "\t") do |csv|
          csv << type[:fields]
          ::Account.without_locking do
            ActiveRecord::Base.with_slave do
              type[:class].find_each do |record|
                account = record.is_a?(::Account) ? record : record.account
                next if account.nil?
                csv << type[:fields].map { |f| format_field(record.send(f.to_sym)) }
              end
            end
          end
        end
      end
    end
  end
end
