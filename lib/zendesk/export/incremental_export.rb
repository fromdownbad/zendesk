class Zendesk::Export::IncrementalExport
  def initialize(options = {})
    @association = options[:association]
    @association_includes = options[:association_includes]
    @limit = options[:limit]
    @start_time = options[:start_time]
    @presenter = options[:presenter]
    @url_builder = options[:url_builder]
    @page_url_method = options[:page_url_method]
    @custom_finder = options[:custom_finder]
    @update_ts_field = options[:update_ts_field] || 'updated_at'
    @next_page_parameters = options[:next_page_parameters] || {}
    @navigation = options.fetch(:navigation, :time)
    @cursor = options[:cursor]
    @exclude_deleted = options[:exclude_deleted]
    @enforce_last_minute_limit = options[:enforce_last_minute_limit]
  end

  def as_json
    results, next_timestamp = export

    json = @presenter.present(results)

    json[:end_of_stream] = results.count < @limit if @limit

    return json if @navigation == :cursor

    # incremental pages aren't discrete, previous makes no sense here
    json.delete(:previous_page)

    # we're adding a microsecond here to prevent constantly picking up the last updated ticket.
    json.merge(
      next_page: results.empty? ? nil : url(next_timestamp),
      end_time: next_timestamp
    )
  end

  protected

  def url(next_timestamp)
    params = @url_builder.params.
      merge(start_time: next_timestamp || @start_time).
      merge(@next_page_parameters)

    params = if RAILS4
      params.symbolize_keys
    else
      # this is buggy on rails 4, initializer stringifies no matter what.
      params.transform_keys(&:to_sym)
    end

    @url_builder.send(@page_url_method, params)
  end

  def export
    finder = @custom_finder || Zendesk::Export::IncrementalExportFinder.new(
      export_association: @association,
      includes: @association_includes,
      update_ts_field: @update_ts_field,
      limit: @limit,
      start_time: @start_time,
      cursor: @cursor,
      exclude_deleted: @exclude_deleted,
      enforce_last_minute_limit: @enforce_last_minute_limit
    )

    [finder.results, finder.next_timestamp]
  end
end
