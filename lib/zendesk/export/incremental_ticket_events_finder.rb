# This class finds and returns a data-structure, *not* ActiveRecord models.
# Reasoning is because building the AR object graph for Events is too slow
# for export purposes.

# TicketArchiveStub are placeholders for tickets which have been archived to Riak
# The tickets are fetched from ArchivedAuditExporter which fetches the child events from tickets.
# This class combines Audits from the DB and TicketArchiveStubs from Riak
# Pagination happens by timestamp up to a limit and the results are returned from
# AuditPager as: [event_data_object, ...], next_page_time
# Paginating by timestamp and limiting records can lead to some items with the same
# timestamp being cut off: when this happens the next_page_time does not get incremented so
# all events for that timestamp can be fetched in the next request.

module Zendesk
  module Export
    class IncrementalTicketEventsFinder
      DEFAULT_ARCHIVE_TICKETS_PER_PASS = 50

      attr_reader :results, :next_timestamp

      def initialize(options = {})
        benchmark_start = Time.now

        @account = options[:account]
        @exclude_deleted = options.fetch(:exclude_deleted, false)
        @archive_tickets_per_pass = options[:archive_tickets_per_pass] || DEFAULT_ARCHIVE_TICKETS_PER_PASS

        @db = RawDatabaseExportInterface.new

        event_scope = Event.where(account_id: account.id).where('ticket_id is not null')
        start_time = options[:start_time]
        limit = options[:limit]

        audits, archived_audits = get_audits_with_archive(event_scope, start_time, limit)
        combine_archived_and_live_audits_into_pageable_result(archived_audits, audits, start_time, limit)

        duration = Time.now - benchmark_start
        statsd_client.gauge("full_run.count", results.count)
        statsd_client.histogram("full_run.duration", duration)
      end

      private

      attr_reader :account, :exclude_deleted, :archive_tickets_per_pass

      def all_tickets_scope(account)
        account.tickets.unscoped.where(account_id: account.id)
      end

      def get_audits_with_archive(event_scope, start_time, limit)
        @potential_export_items = []
        audits = []
        archived_audits = []
        item_offset = 0
        union_start_time = start_time

        loop do
          items_to_export, item_offset = get_items_to_export(account.id, item_offset, union_start_time, start_time, limit)
          union_start_time = next_timestamp_for(@potential_export_items, limit)

          grouped_items = items_to_export.group_by { |item| item[:source_type] }

          archive_exporter = ArchivedAuditExporter.new(account, grouped_items['TicketArchiveStub'], limit)
          archived_audits += archive_exporter.get_archived_audits
          audits += get_audits_from_stub_rows(grouped_items['Audit'], event_scope, all_tickets_scope(account), limit)

          break if enough_items_to_export?(audits, archived_audits, limit) || items_to_export.empty?
        end

        [audits, archived_audits]
      end

      def enough_items_to_export?(database_audits, archived_audits, limit)
        (database_audits.count + archived_audits.count) >= limit
      end

      def get_items_to_export(account_id, item_offset, union_start_time, start_time, limit)
        return [[], item_offset] if item_offset > @potential_export_items.count

        items_to_export, item_offset = filter_export_set(@potential_export_items, item_offset, start_time)

        if items_to_export.empty?
          previous_timestamp = next_timestamp_for(@potential_export_items, limit)
          previous_result_count = @potential_export_items.count
          @potential_export_items = get_potential_export_items(account_id, union_start_time, limit)
          union_start_time = next_timestamp_for(@potential_export_items, limit)

          # we have reached the end of the export for this account
          return [[], 0] if previous_timestamp == union_start_time && previous_result_count == @potential_export_items.count

          item_offset = 0
          items_to_export, item_offset = filter_export_set(@potential_export_items, item_offset, start_time)
        end

        [items_to_export, item_offset]
      end

      def get_audits_from_stub_rows(stub_audit_rows, event_scope, ticket_scope, _limit)
        return [] unless stub_audit_rows && stub_audit_rows.any?

        audit_scope = event_scope.where(id: stub_audit_rows.map { |item| item[:id] })

        full_audit_exporter = DatabaseAuditExporter.new(
          db: @db,
          event_scope: event_scope,
          audits: @db.get_raw_data(audit_scope),
          ticket_scope: ticket_scope
        )

        full_audit_exporter.get_audits
      end

      def next_timestamp_for(records, limit)
        latest_update_time = records.present? && records.last[:ts].to_i

        records_were_fetched_under_limit = records.count < limit
        records_spanned_more_than_one_timestamp = records.present? && records.first[:ts] != records.last[:ts]

        if records_were_fetched_under_limit || records_spanned_more_than_one_timestamp
          latest_update_time
        else
          latest_update_time + 1
        end
      end

      def filter_export_set(export_set, offset, start_time)
        return [[], offset] if offset > export_set.count
        stub_count = 0

        items = export_set[offset..-1].take_while do |item|
          stub_count += 1 if item[:source_type] == 'TicketArchiveStub'
          stub_count <= archive_tickets_per_pass || item[:ts] == Time.at(start_time)
        end

        if items.count == export_set.count || (items.count + offset >= export_set.count)
          [items, export_set.count + 1]
        else
          [items, offset + items.count]
        end
      end

      def limited_export_items(account_id, start_time, limit)
        events_string = if account.has_audit_remove_force_index?
          "events"
        else
          "events force index (index_account_id_created_at)"
        end

        metric = account.has_audit_remove_force_index? ? "with_index" : "without_index"

        sql = <<-SQL
      (select id, 'TicketArchiveStub' as source_type, generated_timestamp as ts from
      ticket_archive_stubs force index (index_account_id_and_generated_timestamp_and_status_id)
      where generated_timestamp >= from_unixtime(?)
      #{"and status_id = 4" if exclude_deleted}
      and account_id = ? order by generated_timestamp, id limit ?)
      UNION ALL
      (select events.id, 'Audit' as source_type, events.created_at as ts from #{events_string}
      #{"inner join tickets on tickets.id = events.ticket_id" if exclude_deleted}
      where type in ('Audit', 'TicketMergeAudit')
      #{"and tickets.status_id != 5" if exclude_deleted}
      and events.created_at >= from_unixtime(?)
      and events.account_id = ? order by ts, id limit ?)
      order by ts, id limit ?;
        SQL

        items = Ticket.on_slave do
          statsd_client.time("index_wrapper.#{metric}.execution.time", tags: []) do
            events_sql = Event.send(:sanitize_sql, [sql, start_time, account_id, limit, start_time, account_id, limit, limit])
            ActiveRecord::Base.connection.select_all(events_sql).map(&:symbolize_keys)
          end
        end
        items
      end

      def unlimited_export_items_for_time(account_id, export_time)
        sql = <<-SQL
      (select id, 'TicketArchiveStub' as source_type, generated_timestamp as ts from
      ticket_archive_stubs force index (index_account_id_and_generated_timestamp_and_status_id)
      where generated_timestamp = from_unixtime(?)
      #{"and status_id = 4" if exclude_deleted}
      and account_id = ? order by generated_timestamp, id )
      UNION ALL
      (select id, 'Audit' as source_type, created_at as ts from events
      #{"inner join tickets on tickets.id = events.ticket_id" if exclude_deleted}
      where type in ('Audit', 'TicketMergeAudit')
      #{"and tickets.status_id != 5" if exclude_deleted}
      and created_at = from_unixtime(?)
      and account_id = ? order by ts, id )
      order by ts, id;
        SQL

        items = Ticket.on_slave do
          sql = Event.send(:sanitize_sql, [sql, export_time, account_id, export_time, account_id])
          ActiveRecord::Base.connection.select_all(sql).map(&:symbolize_keys)
        end

        items
      end

      def get_potential_export_items(account_id, start_time, limit)
        items = limited_export_items(account_id, start_time, limit)

        returned_items_hits_limit = items.present? && items.count == limit
        items_all_have_the_same_timestamp = items.present? && items.first[:ts].to_i == items.last[:ts].to_i

        if returned_items_hits_limit && items_all_have_the_same_timestamp
          export_time = items.first[:ts].to_i
          items = unlimited_export_items_for_time(account_id, export_time)
        end

        items
      end

      def combine_archived_and_live_audits_into_pageable_result(archived_audits, audits, start_time, limit)
        pager = Zendesk::Export::AuditPager.new(limit, start_time)
        @results, @next_timestamp = pager.page(audits, archived_audits)
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'incremental_ticket_events')
      end
    end
  end
end
