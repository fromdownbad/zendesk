module Zendesk
  module Export
    class TicketFieldEntriesRawDataExporter
      def initialize(account, tickets)
        @tickets = tickets
        @account = account
      end

      def raw_ticket_field_entries
        return {} if @tickets.empty?

        raw_tfe_cache = entries_from_db.merge(entries_from_archive)

        @tickets.each_with_object({}) do |ticket, by_ticket_cache|
          by_ticket_cache[ticket.id] = raw_tfe_cache[ticket.id] || {}

          # Populate the in-memory cache which is used in Zendesk::Fom::CustomField to prevent crazy
          # database lookups there.
          if @account.has_build_tfe_cache_for_raw_exports?
            ticket.ticket_field_entries_by_ticket_field_id_cache = raw_tfe_cache[ticket.id]
          end
        end
      end

      private

      def entries_from_db
        RawDatabaseExportInterface.new.get_raw_data(
          TicketFieldEntry.where(
            account_id: @account.id,
            ticket_id: @tickets
          )
        ).
          group_by { |ticket_field_entry| ticket_field_entry[:ticket_id] }.
          map { |ticket_id, entries| [ticket_id, entries_by_ticket_field_id(entries)] }.
          to_h
      end

      def entries_from_archive
        @tickets.select(&:archived?).map do |archived_ticket|
          [
            archived_ticket.id,
            entries_by_ticket_field_id(
              archived_ticket.instance_variable_get(:@archived_associations)['ticket_field_entries'].map(&:symbolize_keys)
            )
          ]
        end.to_h
      end

      def entries_by_ticket_field_id(entries)
        entries.
          group_by { |entry| entry[:ticket_field_id] }.
          map { |ticket_field_id, entry| [ticket_field_id, entry.first] }.
          to_h
      end
    end
  end
end
