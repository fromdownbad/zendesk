module Zendesk::Export
  class BenchmarkingReport < BaseReport
    HEADERS = [
      'Account ID',
      'Subdomain',
      'Account Name',
      'Plan Size',
      'Max. Agents',
      'In Trial?',
      'Industry',
      'Employee Count',
      'Job Role',
      'Target Audience',
      'Full-Time Agents',
      'Primary Support Tool',
      'Last Updated',
      'Account Type',
      'Account Status'
    ].freeze

    class << self
      def nfs_filename
        "#{output_path}/benchmarking.tsv"
      end

      def filename
        'benchmarking.tsv'
      end

      def execute
        report = generate_report

        write_file(nfs_filename, report)
        write_to_cloud(report)

        true
      end

      private

      def generate_report
        CSV.generate(col_sep: "\t") do |csv|
          csv << HEADERS
          ActiveRecord::Base.on_all_shards do
            Account.without_locking do
              Account::SurveyResponse.with_slave.includes(account: { subscription: :credit_card }).readonly(true).find_each do |response|
                next if response.account.blank? ||
                        response.account.is_locked? ||
                        response.account.subscription.nil?

                csv << survey_response_as_array(response)
              end
            end
          end
        end
      end

      protected

      def survey_response_as_array(response)
        account = response.account
        subscription = account.subscription

        [
          response.account_id,
          account.subdomain,
          account.name,
          plan_type_to_csv(subscription.plan_type),
          subscription.max_agents,
          boolean_to_yes_no(subscription.is_trial?),
          response.industry,
          response.employee_count,
          response.job_role,
          response.target_audience,
          response.full_time_agents,
          response.primary_support,
          response.updated_at,
          subscription.account_type,
          subscription.account_status
        ]
      end

      def plan_type_to_csv(plan_type)
        case plan_type.to_s
        when '1'
          'Small'
        when '2'
          'Medium'
        when '3'
          'Large'
        else
          plan_type.to_s
        end
      end

      def boolean_to_yes_no(b)
        b ? 'Yes' : 'No'
      end
    end
  end
end
