module Zendesk
  module Export
    class Configuration
      def initialize(account)
        self.account  = account
        self.settings = account.settings
      end

      def all_accessible?
        (ImportExportJobPolicy::DEFAULT_JOBS - export_accessible_types).empty?
      end

      def any_accessible?
        (ImportExportJobPolicy::EXPORT_JOBS & export_accessible_types).any?
      end

      def maximum_exceeded?(ticket_count)
        ticket_count > settings.export_ticket_max
      end

      def type_accessible?(type)
        export_accessible_types.include?(
          identifier_for(type)
        )
      end

      def whitelisted?(user)
        return true if user.is_account_owner?
        return true if export_whitelisted_domain.blank?

        export_whitelisted_domain == user.email_domain
      end

      private

      attr_accessor :account, :settings
      delegate :export_whitelisted_domain, :export_accessible_types,
        to: :settings

      def identifier_for(type)
        type.is_a?(Class) ? class_job_mapping[type.to_s] : type.to_s
      end

      def class_job_mapping
        @class_job_mapping ||= ImportExportJobPolicy::JOB_CLASS_MAPPING.invert
      end
    end
  end
end
