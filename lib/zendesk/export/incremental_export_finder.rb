class Zendesk::Export::IncrementalExportFinder
  def initialize(options = {})
    @export_association = options[:export_association]
    @update_ts_field = options[:update_ts_field]
    @limit = options[:limit]
    @start_time = options[:start_time]
    @includes = options[:includes]
    @navigation = options[:navigation]
    @cursor = options[:cursor]
    @exclude_deleted = options[:exclude_deleted]
    @enforce_last_minute_limit = options[:enforce_last_minute_limit]
  end

  def results
    get_results_until_pageable unless @results
    @results
  end

  def next_timestamp
    get_results_until_pageable unless @results
    @next_timestamp
  end

  protected

  def cursor_params
    {
      start_time: @start_time,
      cursor: @cursor,
      id_column: 'nice_id',
      timestamp_column: 'generated_timestamp',
      order: 'asc',
      limit: @limit,
      enforce_last_minute_limit: @enforce_last_minute_limit
    }
  end

  def get_results_until_pageable # rubocop:disable Naming/AccessorMethodName
    if @navigation == :cursor
      @results = cursor_results
      # none of the following next_timestamp calculation logic is needed for cursors
      return
    end

    results = limited_results

    latest_update_time = results.last && results.last.attributes[@update_ts_field].to_i

    less_results_than_limit = less_results_than_limit?(results)
    if less_results_than_limit || results_span_across_updated_times?(results, latest_update_time)
      @results = results
      @next_timestamp = latest_update_time
    else
      # We want to investigate replacing `update_ts_field` with a proper watermark using ticket nice ids or ids.
      # Track how many times we execute queries without a limit.
      cause = less_results_than_limit ? 'span_across_updated_times' : 'more_results_than_limit'
      Rails.application.config.statsd.increment('incremental_exporter_finder.unlimited_results', tags: ["cause:#{cause}"])

      @results = unlimited_results_for_time(latest_update_time)
      @next_timestamp = latest_update_time + 1
    end
  end

  def cursor_results
    @export_association.
      paginate_with_cursor(cursor_params).
      includes(@includes)
  end

  def limited_results
    r = @export_association.
      where("#{@update_ts_field} >= ?", Time.at(@start_time)).
      order("#{@update_ts_field}, id").
      includes(@includes).
      limit(@limit)

    r = r.where.not(status_id: StatusType.DELETED) if @exclude_deleted

    r = r.where(Ticket.send(:sanitize_sql, ["#{@update_ts_field} < ?", 1.minute.ago])) if @enforce_last_minute_limit

    r.to_a
  end

  def unlimited_results_for_time(exact_ts)
    @export_association.
      where("#{@update_ts_field} = ?", Time.at(exact_ts)).
      order(:id).
      includes(@includes).
      to_a
  end

  def less_results_than_limit?(results)
    results.length < @limit
  end

  def results_span_across_updated_times?(results, latest_update_time)
    results.first.attributes[@update_ts_field].to_i != latest_update_time
  end
end
