class Zendesk::Export::IncrementalTicketExport < Zendesk::Export::TicketExport
  include Zendesk::Export::TicketExportOptions
  include Zendesk::Export::ArchiverV2Support
  include Rails.application.routes.url_helpers

  attr_accessor :redactor, :custom_field_limit, :options, :v2_url

  def self.for_account(account, min_ts, limit, options = {})
    report = new
    report.account       = account
    report.min_ts        = min_ts
    report.limit         = limit
    report.show_metrics  = !!options[:show_metrics]
    report.escape_fields = !!options[:escape_fields]
    report.map_nice_id   = !!options[:map_nice_id]
    report.custom_field_limit = options[:custom_field_limit]
    report.additional_columns = options[:additional_columns]
    report.redactor      = Zendesk::Export::Redactor.for_account(account)
    report.v2_url        = !!options[:v2_url]
    report.options       = build_options_hash(account)
    report
  end

  def find_tickets
    with_benchmark do
      # Override the default scope to include deleted tickets
      account.tickets.unscoped.where("status_id != #{Zendesk::Types::StatusType.Archived}").incremental_export_with_archived(archive_v2_options)
    end
  end

  def tickets
    Ticket.on_slave do
      tickets = tickets_with_archived(find_tickets)

      account_url = "https://#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}"

      ticket_map = tickets.each_with_object({}) do |t, accum|
        key = t['real_id'] || t['id']
        accum[key.to_i] = t
      end

      fetch_custom_field_entries(tickets, ticket_map)

      tickets.map do |t|
        t.delete('id')
        t.delete('real_id')
        t.delete('Summation column')
        t.delete('is_archived')
        t.delete('account_id')
        t.delete('submitter_id')
        t['generated_timestamp'] = t['generated_timestamp'].to_i

        if map_nice_id
          t['id'] = nice_id = t.delete('nice_id').to_i
        else
          t['nice_id'] = nice_id = t['nice_id'].to_i
        end

        t['url'] = if v2_url
          "#{account_url}#{api_v2_ticket_path(nice_id, format: :json)}"
        else
          "#{account_url}/tickets/#{nice_id}"
        end

        Time.use_zone(account.time_zone) do
          DATE_COLUMNS.each do |c|
            c = c.to_s
            # here we assume that Time.zone is setup correctly for us
            # by the web request
            t[c] = if ["0", ""].include?(t[c])
              nil
            else
              Time.zone.at(t[c].to_i).to_s
            end
          end
        end
        redactor.redact(t) if redactor
        t
      end
    end
  end

  def field_map(has_multiple_ticket_forms: nil)
    # ignore summation column, add a header for the generated_timestamp
    headers = ["Ticket update timestamp"] + self.headers(has_multiple_ticket_forms: has_multiple_ticket_forms)[1..-1]

    # we put the id column in to make custom field grabbing better, but we don't want to expose it.
    columns = output_columns - [:id]

    # we didn't get the custom fields in the first select, but we want them in the output.
    columns += custom.map { |c| "field_" + c.id.to_s }

    h = {}

    headers.zip(columns) do |header, col|
      field = Zendesk::Fom::Field.for(col)
      field_alias = field.try(:csv_field_alias).try(:last)

      if field_alias && !h.key?(field_alias)
        h[field_alias] = header
      else
        h[col.to_s] = header
      end
    end

    h['url'] = "Ticket URL"
    h['req_external_id'] = h.delete('requester_external_id') if h['requester_external_id']
    h['req_email'] = h.delete('requester_email') if h['requester_email']
    h['req_id'] = h.delete('ticket_requester_id') if h['ticket_requester_id']
    h['id'] = h.delete('nice_id') if map_nice_id
    h
  end

  def order_column
    "generated_timestamp"
  end

  # since we muck around with the meaning of "generated_timestamp" in the SELECT, we need to
  # explicitly tell mysql to order on the *table's* copy of generated_timestamp, not the SELECT's.
  def order
    "tickets.generated_timestamp"
  end

  attr_accessor :exact_ts

  def include_deleted?
    true
  end

  def conditions_sql
    operator = exact_ts ? "=" : ">="
    super + " AND generated_timestamp #{operator} from_unixtime(#{min_ts.utc.to_i})"
  end

  # we'll get it into proper format in ruby land
  def in_time_zone(date_field)
    "UNIX_TIMESTAMP(#{date_field})"
  end

  def output_columns
    columns = super

    # grab the real ticket-id so we can easily fill the ticket_field_entries
    columns.unshift(:id)

    # grab the generated timestamp to sort everything
    columns.unshift(:generated_timestamp)
  end

  private

  def fetch_ticket_field_entries(conditions)
    # Force index was necessary due to Mysql picking the wrong one and affecting the performance badly
    sql = TicketFieldEntry.select("ticket_id, ticket_field_id, value").
      use_index(:index_ticket_field_entries_on_ticket_id_and_ticket_field_id).
      where(conditions).to_sql
    TicketFieldEntry.connection.select_all(sql).to_a
  end

  def tagger_values_map
    @tagger_values_map ||= begin
      account.fetch_active_taggers.each_with_object({}) do |tagger, lookup|
        tagger.custom_field_options.each { |o| lookup[o.value] = o.name }
      end
    end
  end

  def value_for_incremental_export(tfe, value)
    case tfe
    when FieldCheckbox
      value == "1" ? "Yes" : "No"
    when FieldMultiselect
      value.to_s.split.map { |v| tagger_values_map[v] }.join(", ")
    when FieldTagger
      tagger_values_map[value]
    else
      value
    end
  end

  def fetch_custom_field_entries(tickets, ticket_map)
    conditions = {account_id: @account.id, ticket_id: ticket_map.keys, ticket_field_id: custom.map(&:id)}

    entries = fetch_ticket_field_entries(conditions)
    field_map = account.ticket_fields.where(id: custom.map(&:id)).to_a.each_with_object({}) do |field, accum|
      accum[field.id.to_i] = field
    end

    entries.each do |entry|
      next unless ticket = ticket_map[entry['ticket_id'].to_i]
      ticket_field_id = entry['ticket_field_id'].to_i
      entry['ticket_field'] = field_map[ticket_field_id]
      ticket["field_" + ticket_field_id.to_s] = value_for_incremental_export(entry['ticket_field'], entry['value'])
    end

    tickets.each do |t|
      custom.each do |c|
        t["field_" + c.id.to_s] ||= nil
      end
    end
  end

  def truncated_custom_fields(custom)
    # gooddata has a 60 custom-field limit, so ensure that the higher priority ones are the ones we send.
    if custom_field_limit
      custom.first(custom_field_limit)
    else
      custom
    end
  end

  def with_benchmark
    tickets = []
    elapsed_time = Benchmark.realtime do
      tickets = yield
    end
    Rails.logger.info("Incremental export: account_id:#{account.id}, tickets:#{tickets.length}, min_ts:#{min_ts.to_i}, limit:#{limit}, time:#{elapsed_time}s")
    tickets
  end
end
