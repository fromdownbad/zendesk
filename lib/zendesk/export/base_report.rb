require 'tmpdir'

module Zendesk::Export
  class BaseReport
    class << self
      def output_path
        ['staging', 'production', 'master'].member?(Rails.env) ? '/data/zendesk/shared/data' : Dir.tmpdir
      end

      def format_field(value)
        return nil if value.nil?
        return value.strftime('%d-%b-%Y %H:%m') if value.is_a?(Time)
        return value.delete(',').delete('"').delete("'").delete("\t") if value.is_a?(String)
        value.to_s
      end

      def write_file(name, value)
        File.unlink(name) if File.exist?(name)

        File.open(name, 'w') do |file|
          file.write(value)
        end
      end

      def filename
        raise 'This method must be implemented in subclasses.'
      end

      def write_to_cloud(value)
        report_store = Zendesk::S3ReportingStore.new(filename)
        report_store.store!(value)
      end
    end
  end
end
