module Zendesk
  module Export
    class RawDatabaseExportInterface
      def get_raw_data(scope)
        Ticket.on_slave do
          ActiveRecord::Base.connection.select_all(scope.to_sql).map(&:symbolize_keys)
        end
      end
    end
  end
end
