module Zendesk
  class OffsetPaginationLimiter
    attr_reader :entity, :controller_name, :account_id, :account_subdomain, :page, :per_page, :log_only

    ceiling_rate_limit_offset = ENV['BOLT_CEILING_RATE_LIMIT_OFFSET'].to_i

    MIN_OFFSET_FOR_INDEX_CEILING_RATE_LIMIT = (ceiling_rate_limit_offset > 0) ? ceiling_rate_limit_offset : 1_000_000

    def initialize(entity:, account_id:, account_subdomain:, page:, per_page:, log_only:)
      @entity = entity                                     # "organization_memberships"
      @controller_name = "#{entity.camelize}Controller"    # "OrganizationMembershipsController"
      @account_id = account_id
      @account_subdomain = account_subdomain
      @page = page
      @per_page = per_page
      @log_only = log_only
    end

    def throttle_index_with_deep_offset_pagination
      offset = (page - 1) * per_page
      return if offset < MIN_OFFSET_FOR_INDEX_CEILING_RATE_LIMIT

      throttle_key = "index_#{entity}_with_deep_offset_pagination_ceiling_limit".to_sym
      if log_only
        throttled = Prop.throttle(throttle_key, account_id)
        if throttled
          Rails.logger.info("[#{controller_name}] Log only: would have rate limited #index by account: #{account_subdomain}, page: #{page}, per_page: #{per_page}, throttle key: #{throttle_key}")
          statsd_client.increment("api_v2_#{entity}_index_ceiling_rate_limit_log_only", tags: ["account_id:#{account_id}"])
        end
      else
        Prop.throttle!(throttle_key, account_id)
      end
    end

    private

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: entity)
    end
  end
end
