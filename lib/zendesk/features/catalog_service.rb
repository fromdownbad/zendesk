module Zendesk::Features
  class CatalogService
    attr_reader :account, :current_catalog

    PATAGONIA_GA_DATETIME = ActiveSupport::TimeZone["Pacific Time (US & Canada)"].parse('11-11-2015 11:30:00').utc

    def initialize(account)
      @account         = account
      @current_catalog = find_current_catalog
    end

    private

    def find_current_catalog
      if use_patagonia_catalog?
        Zendesk::Features::Catalogs::Catalog
      else
        Zendesk::Features::Catalogs::LegacyCatalog
      end
    end

    def use_patagonia_catalog?
      (boosted? && boosted_after_patagonia_ga?) || patagonia_pricing_model?
    end

    def boosted?
      proposed_plan_type != subscription.plan_type
    end

    def proposed_plan_type
      @plan_type_service ||= Zendesk::Features::PlanTypeService.new(account).
        proposed_plan_type
    end

    def subscription
      @subscription ||= account.subscription
    end

    def boosted_after_patagonia_ga?
      account.on_shard do
        account.feature_boost.present? &&
          account.feature_boost.updated_at > PATAGONIA_GA_DATETIME
      end
    end

    def patagonia_pricing_model?
      current_pricing_model_revision >=
        ZBC::Zendesk::PricingModelRevision::PATAGONIA
    end

    def current_pricing_model_revision
      if zuora_subscription.present? && !zuora_subscription.approval_required
        zuora_subscription.pricing_model_revision
      else
        subscription.pricing_model_revision
      end
    end

    def zuora_subscription
      @zuora_subscription ||= subscription.zuora_subscription
    end
  end
end
