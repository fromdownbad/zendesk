require 'zendesk_feature_framework'

module Zendesk::Features
  class SubscriptionFeatureService
    attr_reader :account, :zuora_plan_type, :zuora_addons, :zuora_pricing_model,
      :skip_feature_triggers, :fast_execute

    def initialize(account, zuora_pricing_model = nil, zuora_plan_type = nil,
      zuora_addons = {}, skip_feature_triggers = false,
      fast_execute: false)
      @account               = account
      @zuora_pricing_model   = zuora_pricing_model
      @zuora_plan_type       = zuora_plan_type
      @zuora_addons          = zuora_addons
      @skip_feature_triggers = skip_feature_triggers
      @fast_execute          = fast_execute
    end

    def execute!
      return if account.id == SYSTEM_ACCOUNT_ID || subscription.nil?

      current_feature_changes = changes
      Rails.logger.info("Account #{account.id}: feature changes:\n" + changes.to_json)
      return if current_feature_changes.empty? && fast_execute

      feature_setup(current_feature_changes)
    end

    def create_sandbox_features!(master_subscription)
      active_features = {}
      master_subscription.active_features.each { |f| active_features[f.name] = true }

      feature_setup(active_features)
    end

    private

    def feature_setup(features_hash)
      subscription.features.set(features_hash)
      result = subscription.save!

      invalidate_features_cache

      call_feature_change_triggers(features_hash) unless skip_feature_triggers
      result
    end

    def invalidate_features_cache
      # Destroy the cache manually to avoid use cases when the subscription
      # `updated_at` value would stay the same (fast-paced to the second)
      subscription.invalidate_features_cache

      # Keep so that any other cache on the subscription model will be expired
      # as well
      subscription.smart_touch
    end

    def changes
      features = {}
      feature_changes[:additions].each { |name| features[name] = true }
      feature_changes[:removals].each { |name| features[name] = false }
      Rails.logger.info("Account #{account.id}:  feature_changes: #{features.to_json}")

      account.spp? ? features.merge(spp_addon_changes(features)) : features
    end

    def current_features
      @current_features ||= subscription.active_features.map do |f|
        f.name.to_sym
      end
    end

    def proposed_addons
      Zendesk::Features::AddonService.new(account, zuora_addons).proposed_addons
    end

    def proposed_plan_type
      Zendesk::Features::PlanTypeService.new(account, zuora_plan_type).proposed_plan_type
    end

    def feature_changes
      @changes ||= begin
        if pricing_model <= ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE
          legacy_override!
        else
          patagonia_override!
        end
        plan_change_calculator.changes
      end
    end

    def call_feature_change_triggers(changes)
      changes.each do |feature, value|
        name  = feature.to_s.camelize
        klass = "::Zendesk::Features::Change::#{name}Change".safe_constantize
        if klass.present?
          feature_change_class = klass.new(subscription)
          value ? feature_change_class.upgrade : feature_change_class.downgrade
        end
      end
    end

    def plan_change_calculator
      @plan_change_calculator ||= ZendeskFeatureFramework::PlanChangeCalculator.new(
        current_features,
        proposed_plan_type,
        proposed_addons,
        catalog,
        set_options
      )
    end

    def pricing_model
      @pricing_model ||= zuora_pricing_model || subscription.pricing_model_revision
    end

    def legacy_override!
      return if pricing_model > ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE
      case pricing_model
      when ZBC::Zendesk::PricingModelRevision::INITIAL
        initial_pricing_model_overrides
      when ZBC::Zendesk::PricingModelRevision::APRIL_2010
        april2010_pricing_model_overrides
      when ZBC::Zendesk::PricingModelRevision::UCSF_CAUSEWARE
        ucsf_causware_model_overrides
      when ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE
        enterprise_elite_model_overrides
      end

      settings_based_overrides
      legacy_enterpise_support_collaboration_override
      plan_change_calculator
    end

    def patagonia_override!
      web_portal_override
    end

    def spp_addon_changes(features)
      changes = Zendesk::Features::Overrides::SimplePricePackaging.changes_for(account, proposed_features: features)
      Rails.logger.info("Account #{account.id}: spp addon changes:\n" + changes.to_json)
      spp_override_addons!(changes)
      changes
    end

    def spp_override_addons!(changes)
      ensure_productivity_pack_addon if changes[:enterprise_productivity_pack]
      remove_inactive_boosted_addons(changes)
    rescue StandardError => e
      msg = "Account: #{account.id}, error while updating SP&P addons:"
      Rails.logger.error("#{msg}: #{e.message} - #{e.backtrace}")
    end

    def ensure_productivity_pack_addon
      return if account.subscription_feature_addons.any? do |addon|
        addon.name == "enterprise_productivity_pack"
      end
      account.subscription_feature_addons.create!(
        name: 'enterprise_productivity_pack',
        # ensure account addons have a non-nil rate plan ID
        zuora_rate_plan_id: account_client.zuora_billing_id || 1
      )
    end

    def remove_inactive_boosted_addons(changes)
      changes.each do |addon_name, on_off_flag|
        remove_boosted_addon(addon_name) unless on_off_flag
      end
    end

    def remove_boosted_addon(addon_name)
      active_addons.each do |addon|
        if addon.is_boosted? && addon.name.to_sym == addon_name
          addon.destroy
        end
      end
      account.subscription_feature_addons.reload
    end

    def active_addons
      account.subscription_feature_addons.non_secondary_subscriptions
    end

    def account_client
      @account_client ||= Zendesk::Accounts::Client.new(account, { timeout: 15 })
    end

    def ucsf_causware_model_overrides
      if proposed_plan_type == SubscriptionPlanType.Inbox
        plan_change_calculator.override(:host_mapping, true)
      else
        host_mapping_override
      end
    end

    def enterprise_elite_model_overrides
      host_mapping_override
    end

    def host_mapping_override
      plan_change_calculator.override(:host_mapping, true) if account.settings.grandfathered_host_mapping?
    end

    def initial_pricing_model_overrides
      case proposed_plan_type
      when SubscriptionPlanType.Small
        plan_type_1_overrides
      when SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
        plan_change_calculator.override(:forums2_toggle, true)
      end
      plan_change_calculator.override(:personal_rules, true) if account.settings.personal_rules?
    end

    def april2010_pricing_model_overrides
      case proposed_plan_type
      when SubscriptionPlanType.Small
        plan_type_1_overrides
      when SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge, SubscriptionPlanType.Inbox
        plan_change_calculator.override(:forums2_toggle, true)
      end
    end

    def plan_type_1_overrides
      plan_change_calculator.override(:forums2_toggle, true)
      plan_change_calculator.override(:host_mapping, true)
      plan_change_calculator.override(:view_csv_export, true)
    end

    def settings_based_overrides
      help_center_ga_override
      web_portal_override
    end

    def help_center_ga_override
      plan_change_calculator.override(:help_center_google_analytics, true) if account.has_help_center_grandfather_ga?
    end

    def web_portal_override
      if account.settings.grandfathered_web_portal?
        plan_change_calculator.override(:community, true)
        plan_change_calculator.override(:customizable_help_center_themes, true)
        plan_change_calculator.override(:internal_help_center, true)
      end
    end

    def catalog
      Zendesk::Features::CatalogService.new(account).current_catalog
    end

    def subscription
      # When new account, reading from slave DB might fail due to replication lag. Fallback to account object.
      # This is safe as it won't be stale during new account-creation.
      @subscription ||= Subscription.find_by_account_id(account.id) || account.subscription
    end

    def subscription_is_trial?
      subscription.is_trial? && zuora_plan_type.nil?
    end

    # Don't give free ticket_threads for enterprise legacy and elite customers
    def legacy_enterpise_support_collaboration_override
      return unless proposed_plan_type == SubscriptionPlanType.ExtraLarge
      return if active_support_collaboration_addon?
      plan_change_calculator.override(:ticket_threads, false)
    end

    def active_support_collaboration_addon?
      account.subscription_feature_addons.find_by_name(:support_collaboration).try(:active?)
    end

    def set_options
      options = {}
      options[:trial] = true if subscription_is_trial?
      options[:boosted_plan] = true if account.feature_boost.try(:is_active?)
      options
    end
  end
end
