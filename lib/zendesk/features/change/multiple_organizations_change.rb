module Zendesk
  module Features
    module Change
      class MultipleOrganizationsChange < FeatureChange
        def downgrade
          current_account.on_shard do
            if current_account.organization_memberships.regular.any?
              Rails.logger.info "Account #{current_account.id} removing non-default organization memberships due to plan change."
              RemoveRegularOrganizationMembershipsJob.enqueue(current_account.id)
            end
          end
        end
      end
    end
  end
end
