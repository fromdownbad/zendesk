module Zendesk
  module Features
    module Change
      class UnlimitedAutomationsChange < UnlimitedRulesChange
        def class_name
          'Automation'
        end
      end
    end
  end
end
