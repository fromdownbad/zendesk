module Zendesk
  module Features
    module Change
      class UnlimitedMultibrandChange < MultibrandChange
        def downgrade
          if @subscription.has_limited_multibrand?
            # Losing unlimited_multibrand but keeping limited_multibrand so we only need to deactivate extra brands
            deactivate_extra_brands
          else
            # Losing unlimited_multibrand and no limited_multibrand so we can only keep one active brand
            fix_agent_route
            deactivate_all_brands
          end
        end

        private

        def deactivate_extra_brands
          return unless account.brands.active.count(:all) > ::Brand::MAXIMUM_ACTIVE_BRANDS

          brands_to_keep = [default_brand, brand_with_agent_route]
          brands_to_keep += account.brands.active.sort_by(&:created_at)
          brands_to_keep = brands_to_keep.uniq.slice(0..::Brand::MAXIMUM_ACTIVE_BRANDS - 1)
          account.brands.active.each { |brand| brand.update_attributes!(active: false) unless brands_to_keep.include?(brand) }
          adjust_help_center_state(account.brands.active)
        end
      end
    end
  end
end
