module Zendesk
  module Features
    module Change
      class LightAgentsChange < FeatureChange
        def downgrade
          account.light_agents.each do |agent|
            Rails.logger.info("Agent Downgrade: #{account.id}/#{account.subdomain} - #{agent.id}/#{agent.name}")
            Zendesk::Users::AgentDowngrader.perform(agent: agent, products: [:support])
          end

          if account.light_agent_permission_set
            Rails.logger.info("Deleting Light Agent permission set: #{account.id}/#{account.subdomain}")
            ::PermissionSet.destroy_light_agent!(account)
          end

          sync_account_service
        end

        def upgrade
          unless account.light_agent_permission_set
            Rails.logger.info("Creating Light Agent permission set: #{account.id}/#{account.subdomain}")
            ::PermissionSet.create_light_agent!(account)
          end

          sync_account_service
        end

        def sync_account_service
          AccountProductFeatureSyncJob.enqueue(account.id, 'light_agents', 'light_agents')
        end
      end
    end
  end
end
