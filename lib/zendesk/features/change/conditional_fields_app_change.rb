module Zendesk
  module Features
    module Change
      class ConditionalFieldsAppChange < AppsFeatureChange
        def feature_name
          'conditional_fields_app'
        end
      end
    end
  end
end
