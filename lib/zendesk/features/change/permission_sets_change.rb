module Zendesk::Features::Change
  class PermissionSetsChange < FeatureChange
    def upgrade
      if account.permission_sets.custom.any?
        role_sync(enabled: true)
      else
        ::I18n.locale = account.translation_locale
        account.add_default_permission_sets
      end
      enqueue_talk_account_sync_job
    end

    def downgrade
      account.subscription.map_role_permissions_to_agents
      role_sync(enabled: false)
      enqueue_talk_account_sync_job
      enqueue_guide_account_sync_job
    end

    private

    CONTEXT = 'updated_plan'.freeze
    AGENT_ROLE_NAME = 'agent'.freeze

    def role_sync(enabled:)
      support_account = Zendesk::SupportAccounts::Account.new(record: account)
      return unless support_account.role_sync_enabled?
      account.permission_sets.custom.each { |permission_set| sync_custom_roles(permission_set, enabled) }
    end

    def sync_custom_roles(permission_set, enabled)
      Omnichannel::RoleSyncJob.enqueue(
        account_id: account.id,
        permission_set_id: permission_set.id,
        role_name: permission_set.pravda_role_name,
        enabled: enabled,
        context: CONTEXT
      )
    end

    def enqueue_talk_account_sync_job
      Omnichannel::TalkAccountSyncJob.enqueue(account_id: account.id)
    end

    def enqueue_guide_account_sync_job
      Omnichannel::GuideAccountSyncJob.enqueue(account_id: account.id)
    end
  end
end
