module Zendesk
  module Features
    module Change
      class CategorizedForumsChange < FeatureChange
        def downgrade
          Rails.logger.info("Account #{account.id} losing forum categories due to plan change")
          account.categories.destroy_all
        end
      end
    end
  end
end
