module Zendesk
  module Features
    module Change
      class AppsFeatureChange < FeatureChange
        def feature_name
          raise NotImplementedError
        end

        def upgrade
          Rails.logger.info("Account #{account.id} gaining #{feature_name.humanize} feature")
          UpdateAppsFeatureJob.enqueue(account.id, feature_name, false, true)
        end

        def downgrade
          Rails.logger.info("Account #{account.id} losing #{feature_name.humanize} feature")
          UpdateAppsFeatureJob.enqueue(account.id, feature_name, true, false)
        end
      end
    end
  end
end
