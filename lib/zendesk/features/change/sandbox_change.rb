module Zendesk
  module Features
    module Change
      class SandboxChange < FeatureChange
        def downgrade
          Rails.logger.info("Account #{account.id} losing sandbox due to plan change")
          account.destroy_sandbox
        end
      end
    end
  end
end
