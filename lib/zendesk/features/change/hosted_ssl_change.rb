module Zendesk
  module Features
    module Change
      class HostedSslChange < FeatureChange
        def downgrade
          account.certificates.active.where("sni_enabled = false").each do |cert|
            Rails.logger.warn("Converting certificate #{cert.id} on #{account.id} to SNI since plan no longer has hosted ssl")
            cert.convert_to_sni!
          end
        end
      end
    end
  end
end
