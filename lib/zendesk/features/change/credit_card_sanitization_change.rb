module Zendesk
  module Features
    module Change
      class CreditCardSanitizationChange < FeatureChange
        def downgrade
          Rails.logger.info("Account #{account.id} losing credit card redaction due to plan change")
          account.settings.lookup(:credit_card_redaction).disable
        end
      end
    end
  end
end
