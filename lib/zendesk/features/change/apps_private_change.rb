module Zendesk
  module Features
    module Change
      class AppsPrivateChange < AppsFeatureChange
        def feature_name
          'apps_private'
        end
      end
    end
  end
end
