module Zendesk
  module Features
    module Change
      class TalkCtiPartnerChange < FeatureChange
        def downgrade
          partner_account = account.voice_partner_edition_account
          return if partner_account.blank?
          return if partner_account.legacy?
          return unless partner_account.active

          Rails.logger.info("Deactivating Talk Partner Edition Account.")
          partner_account.update_attribute(:active, false)
        end

        def upgrade
          Rails.logger.info("Activating Talk Partner Edition Account.")
          ::Voice::PartnerEditionAccount.find_or_create_by_account(account).update_attribute(:active, true)
        end
      end
    end
  end
end
