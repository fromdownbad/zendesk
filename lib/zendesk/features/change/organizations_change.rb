module Zendesk
  module Features
    module Change
      class OrganizationsChange < FeatureChange
        def downgrade
          Rails.logger.info("Account #{account.id} losing organizations feature")
          DowngradeOrganizationsAccessJob.enqueue(account.id)
        end
      end
    end
  end
end
