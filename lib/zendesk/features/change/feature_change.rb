module Zendesk
  module Features
    module Change
      class FeatureChange
        attr_reader :subscription, :had_feature

        delegate :account, to: :subscription

        def initialize(subscription)
          @subscription = subscription
          @had_feature  = old_subscription.try(:send, "has_#{feature}?") || false
        end

        def upgrade
          # Subclass can implement this for upgrade changes
        end

        def downgrade
          # Subclass can implement this for downgrade changes
        end

        def execute
          new_value = has_feature?
          return if new_value == had_feature
          new_value ? upgrade : downgrade
        end

        private

        def feature
          self.class.to_s.demodulize[0...-6].underscore
        end

        def has_feature? # rubocop:disable Naming/PredicateName
          subscription.send("has_#{feature}?")
        end

        def old_subscription
          @old_subscription ||= Subscription.find_by_id(subscription.id)
        end

        def current_account
          @current_account ||= subscription.account.reload
        end
      end
    end
  end
end
