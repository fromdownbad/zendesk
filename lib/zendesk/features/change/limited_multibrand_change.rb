module Zendesk
  module Features
    module Change
      class LimitedMultibrandChange < MultibrandChange
        def downgrade
          # Don't do anything if the account is losing limited_multibrand but gaining unlimited_multibrand
          return if @subscription.has_unlimited_multibrand?
          fix_agent_route
          deactivate_all_brands
        end
      end
    end
  end
end
