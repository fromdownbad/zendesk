module Zendesk
  module Features
    module Change
      class CustomSessionTimeoutChange < FeatureChange
        def downgrade
          return if account.has_security_settings_for_all_accounts?
          Rails.logger.info("Account #{account.id} losing custom session timeout")

          account.settings.session_timeout = account.settings.default(:session_timeout)
          account.settings.save!
        end
      end
    end
  end
end
