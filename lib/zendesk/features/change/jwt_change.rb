module Zendesk::Features::Change
  class JwtChange < RemoteAuthenticationChange
    def downgrade
      return if account.has_security_settings_for_all_accounts?
      jwt = account.remote_authentications.active.jwt.first
      jwt.update_attribute(:is_active, false) unless jwt.nil?
      super
    end
  end
end
