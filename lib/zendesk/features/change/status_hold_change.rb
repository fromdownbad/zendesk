module Zendesk::Features::Change
  class StatusHoldChange < FeatureChange
    def downgrade
      Rails.logger.info "Account #{account.id} / #{account.subdomain} losing StatusType.HOLD due to plan change."
      OpenTicketsOnHoldJob.enqueue(account.id)
    end
  end
end
