module Zendesk
  module Features
    module Change
      class TicketThreadsChange < FeatureChange
        def downgrade
          sync_account_service
        end

        def upgrade
          sync_account_service
        end

        def sync_account_service
          AccountProductFeatureSyncJob.enqueue(account.id, 'ticket_threads', 'side_conversations')
        end
      end
    end
  end
end
