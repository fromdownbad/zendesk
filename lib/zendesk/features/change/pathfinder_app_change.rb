module Zendesk
  module Features
    module Change
      class PathfinderAppChange < AppsFeatureChange
        def feature_name
          'pathfinder_app'
        end
      end
    end
  end
end
