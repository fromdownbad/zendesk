module Zendesk
  module Features
    module Change
      class UnlimitedViewsChange < UnlimitedRulesChange
        def class_name
          'View'
        end
      end
    end
  end
end
