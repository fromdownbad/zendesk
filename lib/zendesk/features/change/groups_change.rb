module Zendesk
  module Features
    module Change
      class GroupsChange < FeatureChange
        def upgrade
          sync_agent_workspace(true)
          expire_cache
        end

        def downgrade
          Rails.logger.info("Account #{account.id} losing groups feature")
          DowngradeAgentGroupsAccessJob.enqueue(account.id)
          AddAllAgentsToDefaultGroupJob.enqueue(account.id)
          sync_agent_workspace(false)
          expire_cache
        end

        private

        def sync_agent_workspace(available)
          return unless Arturo.feature_enabled_for?(:agent_workspace_for_support_only_trial, account)
          Rails.logger.append_attributes(
            subscription: {
              zuora_managed: account.zuora_managed?,
              is_trial: account.is_trial?
            }
          ) if Rails.logger.respond_to?(:append_attributes)
          if account.zuora_managed?
            Rails.logger.info("Setting agent workspace for #{account.id} to #{available}")
            SetAgentWorkspaceAvailabilityJob.enqueue(account.id, available)
          else
            Rails.logger.info("Not setting agent workspace for #{account.id} to #{available}")
          end
        end

        def expire_cache
          expiry.expire_radar_cache_key(:views)
          account.expire_scoped_cache_key(:views)
          account.expire_scoped_cache_key(:rules)
          account.expire_scoped_cache_key(:rule_analysis)
        end

        def expiry
          @expiry ||= Zendesk::RadarExpiry.get(account)
        end
      end
    end
  end
end
