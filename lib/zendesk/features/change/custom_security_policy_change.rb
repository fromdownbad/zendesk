module Zendesk
  module Features
    module Change
      class CustomSecurityPolicyChange < FeatureChange
        def downgrade
          return if account.has_security_settings_for_all_accounts?
          Rails.logger.info("Account #{account.id} losing custom security policy due to plan change")

          if account.agent_security_policy.level?(:custom)
            account.role_settings.update_attributes!(
              agent_security_policy_id: Zendesk::SecurityPolicy::High.id
            )
          end

          if account.end_user_security_policy.level?(:custom)
            account.role_settings.update_attributes!(
              end_user_security_policy_id: Zendesk::SecurityPolicy::High.id
            )
          end
        end
      end
    end
  end
end
