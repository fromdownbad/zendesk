module Zendesk::Features::Change
  class RemoteAuthenticationChange < FeatureChange
    def downgrade
      if account.remote_authentications.active.empty? && account.role_settings.agent_remote_login
        account.role_settings.agent_remote_login = false
        account.role_settings.agent_zendesk_login = true
      end

      if account.remote_authentications.active.empty? && account.role_settings.end_user_remote_login
        account.role_settings.end_user_remote_login = false
        account.role_settings.end_user_zendesk_login = true
      end

      account.role_settings.save!
    end
  end
end
