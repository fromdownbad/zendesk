module Zendesk
  module Features
    module Change
      class TicketFormsChange < FeatureChange
        def upgrade
          return unless account.ticket_forms.empty?

          # If we are creating a brand new sandbox account, do not create the ticket
          # form here as the default ticket fields have not been created yet for the sandbox account.
          # Let add_default_form_to_sandbox do the work in that case
          return if account.is_sandbox?

          Rails.logger.info("Account #{account.id} adding a default ticket form")

          TicketForm.init_default_form(account).save!
        end
      end
    end
  end
end
