module Zendesk
  module Features
    module Change
      class IpRestrictionsChange < FeatureChange
        def downgrade
          # HACK: allows multiproduct accounts (with zopim ip_restrictions plan) to not downgrade based on support plan.
          # TODO: redo this once the global_feature_framework service is built.
          return if account.has_security_settings_for_all_accounts?
          return if account.multiproduct? && account.has_ip_restriction?
          Rails.logger.info("Account #{account.id} losing ip restrictions due to plan change")
          account.settings.lookup(:ip_restriction_enabled).disable
        end
      end
    end
  end
end
