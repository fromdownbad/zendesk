module Zendesk
  module Features
    module Change
      class CustomerSatisfactionChange < FeatureChange
        def downgrade
          Rails.logger.info("Account #{account.id} losing customer satisfaction rating due to plan change")
          account.settings.where(name: "customer_satisfaction").delete_all

          csat_automations = account.automations.where(title: ::I18n.t('txt.customer_satisfaction_rating.automation_v2.title', is_active: true))
          csat_automations.each do |a|
            Rails.logger.info("Account #{account.id} deactivating csat automation id #{a.id} due to plan change")
            a.update_column(:is_active, false)
          end

          csat_views = account.views.where(title: '{{zd.recently_rated_tickets}}', is_active: true)
          csat_views.each do |v|
            Rails.logger.info("Account #{account.id} deactivating csat view id #{v.id} due to plan change")
            v.update_column(:is_active, false)
          end
        end
      end
    end
  end
end
