module Zendesk::Features::Change
  class PlayTicketsAdvancedChange < FeatureChange
    def downgrade
      account.permission_sets.each do |permission_set|
        next unless permission_set.permissions.view_access == 'playonly'
        permission_set.permissions.view_access = 'readonly'

        permission_set.save!
      end
    end
  end
end
