module Zendesk::Features::Change
  class GroupRulesChange < FeatureChange
    def downgrade
      Rails.logger.info "Account #{account.id} losing group rules due to plan change"

      rules = account.all_rules.where(owner_type: 'Group')
      rules.each do |rule|
        rule.owner = account
        rule.save
      end
    end
  end
end
