module Zendesk
  module Features
    module Change
      class IndividualLanguageSelectionChange < FeatureChange
        def downgrade
          Rails.logger.info("Account #{account.id} losing individual language support due to plan change")
          ResetEndUsersLocaleJob.enqueue(account.id)
        end
      end
    end
  end
end
