module Zendesk
  module Features
    module Change
      class UnlimitedTriggersChange < UnlimitedRulesChange
        def class_name
          'Trigger'
        end
      end
    end
  end
end
