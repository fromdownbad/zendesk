module Zendesk
  module Features
    module Change
      class UnlimitedRulesChange < FeatureChange
        def class_name
          raise 'Subclass of UnlimitedRulesChange must implement class_name!'
        end

        def downgrade
          with_guard("#{account.id}:#{feature}:downgrade") do
            Rule.transaction do
              deactivate_rules
              create_default_rules
            end
          end
        end

        private

        def client
          @client ||= Zendesk::RedisStore.client
        end

        def with_guard(key)
          return unless client.set(key, true, nx: true, px: 10000)

          yield

          client.del(key)
        end

        def deactivate_rules
          account.send("#{class_name.underscore}s").active.update_all(is_active: false)
        end

        def create_default_rules
          ::I18n.with_locale(account.translation_locale) do
            account.becomes(::Accounts::Base).load_rule_class_from_yaml(class_name)
          end
        end
      end
    end
  end
end
