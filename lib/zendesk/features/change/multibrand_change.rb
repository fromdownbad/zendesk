module Zendesk
  module Features
    module Change
      class MultibrandChange < FeatureChange
        def fix_agent_route
          return unless account.brands.active.size > 1 && account.route.present?

          if brand_with_agent_route != default_brand
            account.update_attributes(default_brand: brand_with_agent_route)
            Rails.logger.info("Default brand changed in account #{account.id} due to feature change")
          end
        end

        def deactivate_all_brands
          account.brands.active.each { |brand| brand.update_attributes!(active: false) if brand != brand_with_agent_route }

          # account name and brand name have to be in sync on a single-brand account
          account.update_attributes(name: brand_with_agent_route.name) if account.name != brand_with_agent_route.name

          adjust_help_center_state(account.brands.active)
        end

        def brand_with_agent_route
          account.route.brand
        end

        def default_brand
          account.default_brand
        end

        def upgrade
          adjust_help_center_state(account.brands)
        end

        private

        def adjust_help_center_state(brands)
          if brands.any?(&:help_center_enabled?)
            account.enable_help_center! unless account.help_center_enabled?
          elsif brands.any?(&:help_center_in_use?)
            account.restrict_help_center! unless account.help_center_restricted?
          else # all are archived, disabled, or do not exist
            account.disable_help_center! unless account.help_center_disabled?
          end
        end
      end
    end
  end
end
