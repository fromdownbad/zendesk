module Zendesk
  module Features
    module Change
      class IndividualTimeZoneSelectionChange < FeatureChange
        def downgrade
          Rails.logger.info("Account #{account.id} losing user time zones due to plan change")
          AlignUserTimeZonesJob.enqueue(account.id)
        end
      end
    end
  end
end
