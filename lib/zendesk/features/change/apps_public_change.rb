module Zendesk
  module Features
    module Change
      class AppsPublicChange < AppsFeatureChange
        def feature_name
          'apps_public'
        end
      end
    end
  end
end
