module Zendesk::Features::Change
  class PciChange < FeatureChange
    def downgrade
      account.ticket_fields.where(type: 'FieldPartialCreditCard', is_active: true).find_each do |tf|
        tf.update_attributes!(is_active: false)
      end
    end
  end
end
