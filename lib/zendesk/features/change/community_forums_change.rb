module Zendesk
  module Features
    module Change
      class CommunityForumsChange < FeatureChange
        def downgrade
          Rails.logger.info("Account #{account.id} losing forum article type due to plan change")
          account.forums.update_all(display_type_id: ForumDisplayType::ARTICLES.id)
        end
      end
    end
  end
end
