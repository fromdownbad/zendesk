module Zendesk
  module Features
    module Change
      class HostMappingChange < FeatureChange
        def downgrade
          Rails.logger.info("Account #{account.id} losing host mapping due to plan change")
          account.update_attribute(:host_mapping, nil) unless account.host_mapping.nil?

          # Removing host mapping infers that we can safely revoke certificates
          # for the host mapping.
          #
          # This is performed during `host_mapping` downgrade instead of the
          # `hosted_ssl` downgrade because we'd prefer to convert those
          # certificates to SNI to avoid complications with maintaining
          # host mapping, including HSTS and customer DNS changes.
          account.certificates.active.each do |certificate|
            Rails.logger.info("Revoking certificate #{certificate.id}")
            certificate.revoke!
            certificate.clear_ipm!
          end
        end
      end
    end
  end
end
