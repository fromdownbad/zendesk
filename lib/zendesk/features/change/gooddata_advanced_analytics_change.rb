module Zendesk
  module Features
    module Change
      class GooddataAdvancedAnalyticsChange < FeatureChange
        def downgrade
          Zendesk::Gooddata::IntegrationProvisioning.destroy_for_account(account)
        end
      end
    end
  end
end
