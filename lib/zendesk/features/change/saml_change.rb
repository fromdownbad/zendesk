module Zendesk::Features::Change
  class SamlChange < RemoteAuthenticationChange
    def downgrade
      return if account.has_security_settings_for_all_accounts?
      saml = account.remote_authentications.active.saml.first
      saml.update_attribute(:is_active, false) unless saml.nil?
      super
    end
  end
end
