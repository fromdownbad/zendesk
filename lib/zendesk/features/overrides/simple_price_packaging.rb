module Zendesk::Features::Overrides
  module SimplePricePackaging
    class << self
      def changes_for(account, proposed_features: {})
        classic_features = current_features_in_classic(account)

        current_features_from_account_service(account).reject do |feature_name, on_off_flag|
          classic_features.include?(feature_name) &&
            on_off_flag &&
            (proposed_features[feature_name].nil? || proposed_features[feature_name] == on_off_flag)
        end
      end

      private

      def current_features_in_classic(account)
        account.subscription.active_features.map do |f|
          f.name.to_sym
        end
      end

      def current_features_from_account_service(account)
        return {} unless account.spp?
        addons = get_addons(account)

        # keys: classic features, values: whether the corresponding account service addon is active
        {
          skill_based_ticket_routing:   addons[:skills_based_routing]&.active?,
          light_agents:                 addons[:light_agents]&.active?,
          ticket_threads:               addons[:side_conversations]&.active?,
          unlimited_multibrand:         addons[:multibrand]&.active? && addons[:multibrand]&.plan_settings&.[]('max_brands') == 300,
          limited_multibrand:           addons[:multibrand]&.active? && addons[:multibrand]&.plan_settings&.[]('max_brands') == 5,
          hipaa:                        addons[:hipaa]&.active?,
          enterprise_productivity_pack: addons[:productivity_pack]&.active?,
          high_volume_api:              addons[:high_volume_api]&.active?
        }.merge(get_data_locality(addons)).compact
      end

      def get_addons(account)
        products = account.products(use_cache: false)
        {
          skills_based_routing: find_product(products, 'skills_based_routing'),
          light_agents:         find_product(products, 'light_agents'),
          side_conversations:   find_product(products, 'side_conversations'),
          data_locality:        find_product(products, 'data_locality'),
          multibrand:           find_product(products, 'multibrand'),
          hipaa:                find_product(products, 'hipaa'),
          productivity_pack:    find_product(products, 'productivity_pack'),
          high_volume_api:      find_product(products, 'high_volume_api')
        }
      end

      def get_data_locality(addons)
        case addons[:data_locality]&.plan_settings&.[]('datacenter_location')
        when "data-locality-us" then {us_data_center: addons[:data_locality]&.active?}
        when "data-locality-eu" then {eu_data_center: addons[:data_locality]&.active?}
        when "data-locality-germany" then {germany_data_center: addons[:data_locality]&.active?}
        else {}
        end
      end

      def find_product(products, name)
        products.find { |p| p.name == name.to_sym }
      end
    end
  end
end
