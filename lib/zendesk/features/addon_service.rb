module Zendesk::Features
  class AddonService
    attr_reader :current_addons, :zuora_addons, :account

    # The constructor converts the zuora_addons parameter from a hash with format:
    # { 'light_agents'             =>  { zuora_rate_plan_id: <value> },
    #   'temporary_zendesk_agents' => [{ zuora_rate_plan_id: <value>, quantity: <value>, expires_at: <value> },
    #                                  { zuora_rate_plan_id: <value>, quantity: <value>, expires_at: <value> }],
    #   ...
    # }
    #
    # into an array of hashes such as:
    # [
    #   {
    #     name: 'light_agents',
    #     zuora_rate_plan_id: <value>
    #   },
    #   {
    #     name: 'temporary_zendesk_agents',
    #     zuora_rate_plan_id: <value>,
    #     quantity: <value>,
    #     expires_at: <value>
    #   },
    #   {
    #     name: 'temporary_zendesk_agents',
    #     zuora_rate_plan_id: <value>,
    #     quantity: <value>,
    #     expires_at: <value>
    #   }
    # ]
    #
    # This allows for easier iteration and manipulation of the data.
    def initialize(account, zuora_addons = {})
      @account          = account
      @current_addons ||= account.subscription_feature_addons
      @zuora_addons     = process_zuora_addons(zuora_addons)
    end

    def execute!
      return if current_addons.blank? && zuora_addons.blank?
      remove_addons
      update_addons
      add_new_addons
    end

    def proposed_addons
      if zuora_addons.present?
        (zuora_addon_names + boosted_addon_names).uniq
      else
        current_addon_names
      end
    end

    private

    def process_zuora_addons(addons_hash)
      zuora_addons = []
      addons_hash.each do |k, v|
        if v.is_a?(Array)
          v.each do |addon|
            addon[:name] = k
            zuora_addons.push(addon)
          end
        else
          v[:name] = k
          zuora_addons.push(v)
        end
      end unless addons_hash.nil?
      zuora_addons
    end

    def current_addon_names
      current_addons.map(&:name).map(&:to_sym)
    end

    def zuora_addon_names
      zuora_addons.map { |addon| addon[:name].to_sym }
    end

    def boosted_addon_names
      current_addons.boosted.map(&:name).map(&:to_sym)
    end

    def current_addon_rate_plan_ids
      current_addons.map(&:zuora_rate_plan_id).compact
    end

    def zuora_addon_rate_plan_ids
      zuora_addons.map { |addon| addon[:zuora_rate_plan_id] }
    end

    def remove_addons
      # Note: this will not remove boosted addons, since they will not have a rate plan id
      (current_addon_rate_plan_ids - zuora_addon_rate_plan_ids).map do |rate_plan_id|
        current_addon(rate_plan_id).destroy
      end
    end

    def new_addon_rate_plan_ids
      zuora_addon_rate_plan_ids - current_addon_rate_plan_ids
    end

    def new_addons
      new_addon_rate_plan_ids.map do |rate_plan_id|
        addon = zuora_addon(rate_plan_id)
        {
          name: addon[:name].to_sym,
          zuora_rate_plan_id: rate_plan_id,
          quantity: addon.key?(:quantity) ? addon[:quantity] : nil,
          expires_at: addon.key?(:expires_at) ? addon[:expires_at] : nil,
          starts_at: addon.key?(:starts_at) ? addon[:starts_at] : nil,
          status_id: starting_now?(addon) ? SubscriptionFeatureAddon::STATUS[:started] : nil
        }
      end
    end

    def add_new_addons
      account.subscription_feature_addons.create(new_addons)
    end

    def current_addon(rate_plan_id)
      current_addons.where(zuora_rate_plan_id: rate_plan_id).first
    end

    def zuora_addon(rate_plan_id)
      zuora_addons.find { |addon| addon[:zuora_rate_plan_id] == rate_plan_id }
    end

    def addons_to_update
      current_addons.select do |addon|
        (zuora_addon_rate_plan_ids.include?(addon.zuora_rate_plan_id) && !new_addon_rate_plan_ids.include?(addon.zuora_rate_plan_id)) ||
        (addon.boost_expires_at.present? && zuora_addons.map { |a| a[:name] }.include?(addon.name))
      end
    end

    def update_addons
      addons_to_update.each do |current_addon|
        latest_addon = (zuora_addon(current_addon.zuora_rate_plan_id) || zuora_addons.find { |addon| addon[:name] == current_addon.name })
        current_addon.update_attributes(
          zuora_rate_plan_id: (current_addon.zuora_rate_plan_id || latest_addon[:zuora_rate_plan_id]),
          boost_expires_at: nil,
          quantity: latest_addon.key?(:quantity) ? latest_addon[:quantity] : nil,
          expires_at: latest_addon.key?(:expires_at) ? latest_addon[:expires_at] : nil,
          starts_at: latest_addon.key?(:starts_at) ? latest_addon[:starts_at] : nil
        )
      end
    end

    def starting_now?(addon)
      addon[:starts_at].present? && (addon[:starts_at] < DateTime.tomorrow)
    end
  end
end
