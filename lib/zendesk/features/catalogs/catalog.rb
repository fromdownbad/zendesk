require 'zendesk_feature_framework'

module Zendesk::Features::Catalogs
  module Catalog
    include ZendeskFeatureFramework::CatalogBuilder

    ########################################
    # PLANS
    ########################################
    plan SubscriptionPlanType.Small
    plan SubscriptionPlanType.Medium
    plan SubscriptionPlanType.Large
    plan SubscriptionPlanType.ExtraLarge
    plan SubscriptionPlanType.Inbox

    ########################################
    # DEPRECATED FEATURES
    # (KEEP UNTIL DATABASE CLEAN UP)
    ########################################
    feature :screencasts_for_dropboxes
    feature :guide_advanced

    ########################################
    # FEATURES
    ########################################
    feature :advanced_security do |f|
      f.addon_for SubscriptionPlanType.ExtraLarge
      f.bundles_feature :drp, :encryption_at_rest, :hipaa
      f.boostable false
    end

    feature :agent_collision do |f|
      f.included_for SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :agent_display_names do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    group :api do
      feature :api_limit_200_rpm do |f|
        f.included_for SubscriptionPlanType.Medium
      end

      feature :api_limit_400_rpm do |f|
        f.included_for SubscriptionPlanType.Large
      end

      feature :api_limit_700_rpm do |f|
        f.included_for SubscriptionPlanType.ExtraLarge
      end

      feature :api_limit_700_rpm_legacy
    end

    feature :apps_private do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :apps_public do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :audit_log do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :bcc_archiving do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :business_hours do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :categorized_forums do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :chat do |f|
      f.included_for SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :cms do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :collision_chat do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :community do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :community_forums do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :conditional_fields_app do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :credit_card_sanitization do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :crm_integrations do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :cti_integrations do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :custom_dkim_domain do |f|
      f.included_for SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :custom_security_policy do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :custom_session_timeout do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :customer_satisfaction do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :customizable_help_center_themes do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :dc_in_templates do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :drp
    feature :encryption_at_rest

    feature :eu_data_center do |f|
      f.addon_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :extended_ticket_metrics do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :forum_statistics do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :forums2_toggle

    feature :germany_data_center do |f|
      f.addon_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :gooddata_advanced_analytics do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :gooddata_hourly_synchronization do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :group_rules do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :groups do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge, SubscriptionPlanType.Inbox, SubscriptionPlanType.Small
    end

    feature :hc_manage_requests, group: :manage_requests do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :hc_user_profiles do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :help_center_analytics do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :help_center_article_labels do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :help_center_configurable_spam_filter do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :help_center_content_moderation do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :help_center_google_analytics do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :help_center_unlimited_categories do |f|
      f.included_for SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :enterprise_productivity_pack do |f|
      f.addon_for SubscriptionPlanType.Large
      f.bundles_feature :ticket_forms, :conditional_fields_app, :pathfinder_app
    end

    feature :high_volume_api do |f|
      f.addon_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :hipaa

    feature :host_mapping do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :hosted_ssl do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
      f.trialable false
    end

    feature :hourly_incremental_export do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :inbox do |f|
      f.included_for SubscriptionPlanType.Inbox
    end

    feature :incremental_export do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :individual_language_selection do |f|
      f.included_for SubscriptionPlanType.Inbox, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :individual_time_zone_selection do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :internal_help_center do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :ip_restrictions do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :jwt do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :knowledge_bank_list_management do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :light_agents do |f|
      f.addon_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.bundles_feature :ticket_threads
    end

    feature :limited_multibrand, group: :multibrand do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
      f.addon_for SubscriptionPlanType.Large
    end

    feature :macro_preview do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :macro_search do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :multiple_organizations do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :multiple_schedules do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :nps do |f|
      f.addon_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.bundles_feature :nps_surveys, :user_views
    end

    feature :nps_surveys

    feature :organizations do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :pathfinder_app do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :pci do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :permission_sets do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :personal_rules do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :play_tickets do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :play_tickets_advanced do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :premier_support do |f|
      f.addon_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :premium_sandbox_production do |f|
      f.addon_for SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :premium_sandbox_partial do |f|
      f.addon_for SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :premium_sandbox_metadata do |f|
      f.addon_for SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :priority_support do |f|
      f.addon_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :report_feed do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :reporting_leaderboard do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :rule_analysis do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :rule_usage_stats do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :saml do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :sandbox do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :satisfaction_dashboard do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :satisfaction_prediction do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :screencasts_for_tickets do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :sdk_manage_requests, group: :manage_requests do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :search_statistics do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :service_level_agreements do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :skill_based_attribute_ticket_mapping do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :skill_based_ticket_routing do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :social_messaging do |f|
      f.addon_for SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :status_hold do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :support_collaboration do |f|
      f.addon_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.bundles_feature :light_agents, :ticket_threads
    end

    feature :tag_phrases_for_forum_entries do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :talk_cti_partner do |f|
      f.addon_for SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :targets do |f|
      f.included_for SubscriptionPlanType.Inbox, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :temporary_zendesk_agents do |f|
      f.addon_for SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
      f.trialable false
    end

    feature :ticket_forms do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :ticket_threads

    feature :ticket_sharing_triggers do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :trigger_revision_history do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end

    feature :unlimited_automations do |f|
      f.included_for SubscriptionPlanType.Inbox, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :unlimited_multibrand, group: :multibrand do |f|
      f.addon_for SubscriptionPlanType.ExtraLarge
    end

    feature :unlimited_triggers do |f|
      f.included_for SubscriptionPlanType.Inbox, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :unlimited_views do |f|
      f.included_for SubscriptionPlanType.Inbox, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :us_data_center do |f|
      f.addon_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :user_and_organization_fields do |f|
      f.included_for SubscriptionPlanType.Inbox, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :user_views

    feature :user_xml_export do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :view_csv_export do |f|
      f.included_for SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :voice_business_hours do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :whatsapp_phone_number do |f|
      f.addon_for SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
      f.boostable false
    end

    feature :xml_export do |f|
      f.included_for SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :zopim_chat do |f|
      f.included_for SubscriptionPlanType.Small, SubscriptionPlanType.Medium, SubscriptionPlanType.Large, SubscriptionPlanType.ExtraLarge
    end

    feature :contextual_workspaces do |f|
      f.included_for SubscriptionPlanType.ExtraLarge
    end
  end
end
