module Zendesk::Features
  class PlanTypeService
    attr_reader :account, :zuora_plan_type

    def initialize(account, zuora_plan_type = nil)
      @account         = account
      @zuora_plan_type = zuora_plan_type
    end

    def proposed_plan_type
      if zuora_plan_type
        [zuora_plan_type, boosted_plan_type].max
      else
        [subscription.plan_type, boosted_plan_type].max
      end
    end

    private

    def subscription
      @subscription ||= account.subscription
    end

    def feature_boost
      @feature_boost ||= account.feature_boost if account.feature_boost.try(:is_active?)
    end

    def boosted_plan_type
      return 0 if feature_boost.nil?
      feature_boost.boost_level
    end
  end
end
