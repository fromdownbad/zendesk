module Zendesk::Features
  module Presentation
    READABLE_TRANSLATIONS = {
      nps:             'NPS',
      high_volume_api: 'High volume API'
    }.with_indifferent_access.freeze

    private

    def readable_name(name)
      READABLE_TRANSLATIONS.fetch(name, name.to_s.humanize)
    end
  end
end
