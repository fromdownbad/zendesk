Zendesk::DataDeletion::ParticipantClient = Kragle.clone

# This is creating a new implementation for a Kragle client in order to use
# a different system user without having to change the Kragle class variables.
# It is keeping all of the existing Kragle implementation except for the oauth_key and oauth_secret.
module Zendesk
  module DataDeletion
    module ParticipantClient
      OAUTH_KEY = Zendesk::Configuration.dig :system_user_auth, :mac_key
      OAUTH_SECRET = Zendesk::Configuration.dig :system_user_auth, :account_data_deletion, :oauth

      class << self
        def client(service, retry_options: {}, circuit_options: nil, avro_schemas_path: Kragle.avro_schemas_path, shared_cache: true, signing: true)
          # Make sure we're reporting events.
          report_to_statsd
          report_to_apm
          report_to_log

          service_url = URL_TEMPLATE % service.to_s.tr('_', '-')

          Faraday.new(url: service_url) do |faraday|
            retry_options[:exceptions] ||= RETRYABLE_ERRORS

            # Set some sane default timeouts
            faraday.options.timeout = 5
            faraday.options.open_timeout = 2

            # This should go first: it instruments the entire stack.
            faraday.use Kragle::Middleware::Instrumentation, name: CALL_EVENT, payload: { service: service }

            # If ddtrace is present, we instrument the Faraday connection as a service.
            if @reporting_to_apm
              faraday.use :ddtrace, service_name: apm_service_name(service)
            end

            # Retry needs to follow multipart because multipart isnt idempotent;
            # during multipart reprocessing, it duplicates the boundary in the Content-Type header
            # See : https://github.com/lostisland/faraday/blob/0.9/lib/faraday/request/multipart.rb#L11
            # There's an issue logged with faraday for retrying multipart requests here:
            # https://github.com/lostisland/faraday/issues/668

            faraday.request :multipart

            faraday.request :retry, { max: 0 }.merge(retry_options) # Opt out of retries unless options passed

            # Signing is time-dependent, so it must re-sign after Retry middleware.
            if signing
              faraday.use Middleware::Request::Signing, OAUTH_KEY, OAUTH_SECRET
            end

            # Uses optional circuit breaker middleware
            faraday.use(Kragle::Middleware::CircuitBreaker, circuit_options.merge(name: service)) if circuit_options

            faraday.use Kragle::Middleware::Response::ContentType
            faraday.use Kragle::Middleware::Response::RaiseError

            faraday.request :json
            faraday.request :id_proxy
            faraday.response :json, content_type: /\bjson$/
            faraday.response :avro, schemas_path: avro_schemas_path unless Gem::Specification.find_all_by_name('avro_turf').empty?

            unless Kragle.cache.nil?
              faraday.use :http_cache,
                store: Kragle.cache,
                logger: Kragle.logger,
                shared_cache: shared_cache, # when true then middleware acts as a "shared cache", which means it doesn't cache private responses
                serializer: Marshal # JSON breaks on some UTF8 characters.
            end

            # Sends a custom User-Agent to the server. See `Kragle.user_agent=`.
            faraday.headers['User-Agent'] = user_agent

            faraday.use Kragle::Middleware::Instrumentation, payload: { service: service }

            # Allow overriding the defaults.
            yield faraday if block_given?

            adapter_already_set = faraday.builder.handlers.any? do |h|
              h.klass.ancestors.include?(Faraday::Adapter)
            end

            faraday.adapter Faraday.default_adapter unless adapter_already_set
          end
        end
      end
    end
  end
end
