module Zendesk
  module DataDeletion
    class Runner
      include AccountDeletionKillSwitch

      LOOP_INTERVAL = 1.minute
      begin
        MAX_ACTIVE_DELETIONS = Integer(ENV['DATA_DELETION_RUNNER_MAX_ACTIVE_DELETIONS'])
      rescue StandardError => e
        Rails.logger.error "#{self} - #{e.inspect}: #{e.message}"
        MAX_ACTIVE_DELETIONS = 20
      end

      def run
        install_logger(json_logger)
        Rails.logger.info "Starting #{self.class} in #{Rails.env} environment"
        loop do
          abort "#{$0} Detected orphan process - killing self!" if Process.ppid == 1
          begin
            run_once
          rescue StandardError => e
            log_exception(e)
          ensure
            sleep LOOP_INTERVAL
          end
        end
      end

      def run_once
        Rails.logger.info "Polling for deletions to schedule"
        deletions = deletions_to_run
        statsd_client.gauge("active_audits", deletions.length)
        statsd_client.gauge("enqueued_audits", DataDeletionAudit.enqueued.pod_local_moved.count, tags: ['reason:moved'])
        statsd_client.gauge("enqueued_audits", DataDeletionAudit.enqueued.pod_local_canceled.count, tags: ['reason:cancelled'])
        return if kill_switch_flipped?
        deletions.each do |audit|
          json_logger.append_attributes data_deletion_audit_id: audit.id, account_id: audit.account_id
          Rails.logger.info "Running AccountDeletionManager on id: #{audit.id}"
          begin
            Zendesk::DataDeletion::AccountDeletionManager.work(audit.id)
          rescue StandardError => e
            log_exception(e)
          end
        end
      ensure
        json_logger.attributes.delete :data_deletion_audit_id
        json_logger.attributes.delete :account_id
      end

      def install_logger(logger)
        # Let's not taint the global environment in tests
        unless Rails.env.test?
          Rails.application.config.colorize_logging = false
          Rails.logger.subscribers.clear
          Rails.logger.subscribe(logger)
        end
      end

      def json_logger
        @json_logger ||= Rails.logger.stdout_logger.instance.tap do |logger|
          logger.append_attributes(
            prefix: 'data_deletion_runner',
            pid:    $$,
            host:   Socket.gethostname
          )
        end
      end

      def log_exception(exception)
        Rails.logger.error "#{exception.inspect}: #{exception.message}"
        Rails.logger.error exception.backtrace
        ZendeskExceptions::Logger.record(exception, location: __FILE__, message: "Exception in #{self.class}", fingerprint: '09c71dfabfe3c0edd3080b2945a84acb97fcd5f4')
      end

      def deletions_to_run
        deletions = []
        deletions = query_with_max_limit(deletions, DataDeletionAudit.active.pod_local_moved)
        deletions += query_with_max_limit(deletions, DataDeletionAudit.active.pod_local_canceled)
        deletions += query_with_max_limit(deletions, DataDeletionAudit.enqueued.pod_local_moved)
        deletions += query_with_max_limit(deletions, DataDeletionAudit.enqueued.pod_local_canceled)
        deletions
      end

      def query_with_max_limit(current_deletions, scope)
        slots_available = MAX_ACTIVE_DELETIONS - current_deletions.size
        return [] if slots_available < 1
        scope.limit(slots_available).to_a
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'data_deletion.runner')
      end
    end
  end
end
