# Borrowed entirely from Exodus.
# https://github.com/zendesk/exodus/blob/master/lib/exodus/jobs/heartbeat.rb
module Zendesk
  module DataDeletion
    class JobHeartbeater < BackgroundPoll
      def initialize(job_audit, initial_heartbeat)
        super()
        @job_audit = job_audit
        @last_heartbeat = initial_heartbeat
      end

      def poll
        @last_heartbeat = @job_audit.optimistic_heartbeat!(@last_heartbeat)
        created_at = @job_audit.created_at.to_i
        statsd_client.gauge('data_deletion_heartbeat', @last_heartbeat.to_i - created_at, tags: ["job:#{@job_audit.job}"])
      rescue DataDeletionAuditJob::JobCollision => e
        statsd_client.increment('jobs.collisions', tags: ["job:#{@job_audit.job.demodulize}"])
        Rails.logger.error(e.inspect)
        self.class.exit_now!
      rescue StandardError => e
        Rails.logger.error("Exception in heartbeat thread: #{e.class.name}: #{e.message}")
        ZendeskExceptions::Logger.record(e, location: __FILE__, message: 'Exception in heartbeat thread', fingerprint: '62da7a2ac47fa0fd94db5fd5529faa97d59e7b8b')
        self.class.exit_now!
      ensure
        ActiveRecord::Base.clear_active_connections!
      end

      private

      def statsd_client
        Zendesk::StatsD::Client.new(namespace: 'data_deletion')
      end
    end
  end
end
