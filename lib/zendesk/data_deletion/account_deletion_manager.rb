module Zendesk
  module DataDeletion
    class AccountDeletionManager
      DELETION_SLA = 40.days

      class << self
        # Determines the order to delete account data; DO NOT rearrange the order of items
        #
        def stages
          [
            :independent, # Run independent jobs first, jobs not dependant on mysql data
            :before_db,   # Run jobs that require foreign keys in mysql second
            :db,          # Finally run the job that removes mysql data
            :after_db     # Run job to rename subdomain and routes
          ]
        end

        def work(audit_id)
          # Why on_master? In a fast pod, resque can run the job before the record is replicated to all slaves.
          audit = Account.on_master { DataDeletionAudit.find(audit_id) }

          safe_for_work(audit)
        rescue Zendesk::DataDeletion::PermanentError => e
          audit.fail!(e)
          statsd_client(audit).increment('audits.failed', tags: ["exception:#{e.class}", "permanent:true"])
          raise e
        rescue StandardError => e
          # Job will be re-tried indefinitely.

          # This metric is used by a Datadog Monitor to alert us if an audit is
          # persistently failing. Please update the monitor definition if this
          # metric is changed.
          #
          # https://app.datadoghq.com/monitors#1033661
          statsd_client(audit).increment('audits.failed', tags: ["exception:#{e.class}", "permanent:false"])
          raise e
        end

        def safe_for_work(audit)
          account = audit.account

          # Only work on audits that are enqueued or started
          raise Zendesk::DataDeletion::PermanentError, "Audit status must be 'enqueued' or 'started' (is #{audit.status.inspect})" unless audit.enqueued? || audit.started? || audit.waiting?

          raise Zendesk::DataDeletion::InvalidAudit, "Audit failed validation: #{audit.errors.to_a.inspect}" unless audit.valid?

          if audit.for_shard_move?
            # We can't use the account table to find the correct shard
            raise InvalidAudit, "No shard_id present on an account move deletion" if audit.shard_id.nil?

            # We can't erase the account from it's current shard
            raise Zendesk::DataDeletion::PermanentError, "Account is currently active on shard #{account.shard_id}, can't erase" if account.shard_id == audit.shard_id

          elsif audit.for_cancellation?
            # Only search among soft deleted accounts
            raise Zendesk::DataDeletion::PermanentError, 'Cannot remove an account unless it has been soft deleted' unless account.deleted_at.present?

            # Check that this account is able to be deleted (some accounts must be kept forever for legal reasons)
            account.verify_can_delete_for_cancellation
          else
            # Each manager should only handle one type of deletion (shard moves are handled by another job)
            raise Zendesk::DataDeletion::InvalidAudit, "Invalid DataDeletionAudit#reason: #{audit.reason.inspect})"
          end

          # Audits should begin their lifecyle in the 'enqueued' state
          if audit.enqueued? || audit.waiting?
            audit.start!
            statsd_client(audit).increment('audits.started')
          end

          stages.each do |stage|
            enqueue_stage(audit, stage)

            # Do not proceed to the next stage unless jobs in the current stage are all complete
            break unless stage_complete?(audit, stage)
          end

          if job_klasses.all? { |job_klass| job_complete?(audit, job_klass) }
            audit.finish!
            record_audit_duration(audit)
            statsd_client(audit).increment('audits.completed')
          end
        end

        def job_klasses
          # excluding the base participant job as that should not be included in the jobs to run
          excluded_klasses = [Zendesk::Maintenance::Jobs::BaseDataDeletionParticipantJob]

          # only include jobs with valid stages
          staged_job_klasses = ::Zendesk::Maintenance::Jobs::BaseDataDeleteJob.
            lazy_descendants.
            select do |job_klass|
              stages.include?(job_klass.stage)
            end

          staged_job_klasses - excluded_klasses
        end

        def job_klasses_for_stage(stage)
          job_klasses.select { |job_klass| job_klass.stage == stage }
        end

        def enqueue_stage(audit, stage)
          job_klasses_for_stage(stage).each do |job_klass|
            job_audits = audit.attempts_by_job(job_klass)
            active_jobs = job_audits.select(&:active?)

            raise "There should not be more than one active jobs: #{active_jobs.inspect}" if active_jobs.length > 1
            active_job = active_jobs.first

            if job_audits.empty?
              Rails.logger.info "Enqueuing #{job_klass.name} for audit #{audit.id}"
              audit.build_job_audit_for_klass(job_klass).enqueue!
            elsif job_audits.all?(&:failed?)
              reenqueue_failed_job(job_klass, audit)
            elsif active_job&.timed_out?
              Rails.logger.info "Re-enqueuing #{job_klass.name} for audit #{audit.id} after job #{active_job.id} timed out"
              active_job.enqueue!
              statsd_client(audit).increment('jobs.reenqueue_for_timeout', tags: ["job:#{job_klass.name.demodulize}"])
            # `else`: It's still running or completed, do nothing but wait another round...
            end

            unless job_audits.empty? || job_audits.all?(&:finished?)
              record_job_duration(job_audits)
            end
          end
        end

        def reenqueue_failed_job(job_klass, audit)
          Rails.logger.error "Re-enqueuing failed ADD job. Job name: #{job_klass.name}, audit id: #{audit.id}"
          audit.build_job_audit_for_klass(job_klass).enqueue!(DataDeletionAuditJob::FAILED_JOBS_REENQUEUE_DELAY)
          job_audits = audit.attempts_by_job(job_klass)

          if job_audits.size > 2
            DataDeletionAuditJob.delete_failed_audit_jobs(audit.id, job_klass)
          end
          statsd_client(audit).increment('jobs.reenqueue_for_failure', tags: ["job:#{job_klass.name.demodulize}"])
        end

        def stage_complete?(audit, stage)
          job_klasses_for_stage(stage).all? { |job_klass| job_complete?(audit, job_klass) }
        end

        def job_complete?(audit, job_klass)
          audit.attempts_by_job(job_klass).any?(&:finished?)
        end

        def statsd_client(audit)
          Zendesk::StatsD::Client.new(namespace: 'data_deletion', tags: ["reason:#{audit.try(:reason) || 'unknown'}"])
        end

        def record_job_duration(job_audits)
          # We want to measure how long a job has been running by measuring against the
          # audit start time.
          first_job_audit = job_audits.min_by(&:created_at)
          audit = first_job_audit.audit
          job_duration = (Time.now - audit.started_at).to_i
          statsd_client(audit).distribution('jobs.job_duration', job_duration, tags: ["audit_id:#{audit.id}", "job:#{first_job_audit.job.demodulize}"])
        end

        def record_audit_duration(audit)
          return unless audit.finished?

          audit_duration = (audit.completed_at - audit.started_at).to_i
          statsd_client(audit).distribution('data_deletion_audit_duration', audit_duration, tags: ["audit_id:#{audit.id}", "sla_tag:#{sla_tag(audit_duration)}"])
          statsd_client(audit).increment('data_deletion_audit_duration_inc', tags: ["audit_id:#{audit.id}", "sla_tag:#{sla_tag(audit_duration)}", "slo_tag:#{slo_tag(audit_duration)}"])
        end

        def slo_tag(seconds)
          return :critical if seconds > 40.days
          return :warning if seconds > 30.days
          :normal
        end

        def sla_tag(seconds)
          if seconds <= 7.days
            :within_expected
          elsif seconds <= DELETION_SLA
            :within_sla
          else
            :outside_sla
          end
        end
      end
    end
  end
end
