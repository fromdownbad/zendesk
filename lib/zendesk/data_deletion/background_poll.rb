# Borrowed entirely from Exodus.
# https://github.com/zendesk/exodus/blob/master/lib/exodus/background_poll.rb
module Zendesk
  module DataDeletion
    class BackgroundPoll
      DEFAULT_INTERVAL = 1.minute
      attr_accessor :interval

      def initialize(interval: DEFAULT_INTERVAL)
        @interval = interval
        @mutex    = Mutex.new
        @cv       = ConditionVariable.new
        @stop     = false
        @thread   = nil
      end

      class << self
        # only a separate method for easy stubbing
        def exit_now!
          if Rails.env.test?
            # `abort` stops minitest in its tracks, and is confusing to debug
            warn "Uncaught `#{self.class}.exit_now!`. Please stub this method in your tests with an expectation."
            warn caller
          end

          abort
        end
      end

      def poll
        raise 'Implement `poll` in a child class'
      end

      def start!
        raise "Thread is already running!" if @thread
        @stop = false

        # Perform immediately to reduce heartbeat race condition opportunities
        poll

        @thread = Thread.new do
          until @stop
            poll

            @mutex.synchronize do
              # `wait` releases the mutex, and re-acquires on wakeup
              @cv.wait(@mutex, @interval)
            end
          end
        end
      end

      def stop_and_wait!
        return unless @thread
        # Prevent deadlock if called by the `poll` thread, which can't wait for itself to die.
        return signal_stop! if @thread == Thread.current
        while @thread.alive?
          signal_stop!
          sleep 0.01
        end
        @thread.join
        @thread = nil
      end

      # Signal the `poll` thread to stop looping immediately. Safe to be call from any thread.
      def signal_stop!
        @mutex.synchronize do
          @stop = true
          @cv.signal
        end
      end

      def with_poll
        start!
        yield
      ensure
        stop_and_wait!
        # Final run for activity between the previous poll and the block completing
        poll
      end
    end
  end
end
