module Zendesk
  module DataDeletion
    module AccountDeletionKillSwitch
      class KillSwitchEnabled < StandardError; end
      def kill_switch_flipped?
        if Arturo.feature_enabled_for_pod?(:account_deletion_kill_switch, Zendesk::Configuration.fetch(:pod_id))
          Rails.logger.warn ":account_deletion_kill_switch Arturo flag is enabled"
          true
        elsif Arturo.feature_enabled_for_pod?(:account_deletion_kill_switch_classic, Zendesk::Configuration.fetch(:pod_id))
          Rails.logger.warn ":account_deletion_kill_switch_classic Arturo flag is enabled"
          true
        else
          false
        end
      end
    end
  end
end
