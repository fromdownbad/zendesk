# lets be more selective on what we consider looking like a table
#
# Movie.first(:include=>:car, :conditions=>"(`movies`.`email` = 'pete@foo.com')") -> automatic join
# SELECT "movies"."id" AS t0_r0, "movies"."title" AS t0_r1, "movies"."views" AS t0_r2, "movies"."created_at" AS t0_r3, "movies"."updated_at" AS t0_r4, "cars"."id" AS t1_r0, "cars"."name" AS t1_r1, "cars"."title" AS t1_r2, "cars"."wheel_count" AS t1_r3, "cars"."created_at" AS t1_r4, "cars"."updated_at" AS t1_r5 FROM "movies" LEFT OUTER JOIN "cars" ON "cars"."id" IS NULL WHERE ((`movies`.`id` = 'as.')) LIMIT 1
#
# "INNER JOIN `collaborations` ON `tickets`.id = `collaborations`.ticket_id" -> ["collaborations", "tickets"]
#
# Movie.first(:include=>:car, :conditions=>"(`movies`.`email` = 'pete@foo')") -> no join
# SELECT "movies".* FROM "movies" WHERE ((`movies`.`id` = 'as')) LIMIT 1
#
# will blow up e.g. test/unit/rules/rule_test.rb
ActiveRecord::Relation.class_eval do
  # our tables look like "comments" or "foos", not "gmail.com" or "a.b.c"
  # so we look for 'foos.bar ' or '`foos`.`bar`'
  remove_method :tables_in_string # make sure we overwrite it
  def tables_in_string(string)
    return [] if string.blank?
    found = string.scan(/([a-z_]+s)['"`]?\.['"`]?[a-z_\*]+['"`]?/).flatten.uniq - ['raw_sql_']
    # discard things like "email = 'ops.foo@bar.com'" or "email = 'foo@bars.com'" or "email = 'foo@bars.co.uk'"
    # or "email = 'foos.bar.on@bars.com'" or "email = 'foo@bars.co.uk'"
    found.reject { |f| string =~ /#{f}(\.[a-z\d.]+)?@/ || string =~ /#{f}\.[a-z\d.]+['"]/ }
  end
end
