module ActiveRecord
  class Base
    def present?
      true
    end

    def blank?
      false
    end
  end
end
