require 'soap/streamHandler'

# This is to add code to stop potential redirections to unsafe urls.
#
# This is the place where we follow redirections:
# https://github.com/felipec/soap4r/blob/master/lib/soap/streamHandler.rb#L243
#
# So we need to override the entire SOAP::HTTPStreamHandler#send_post method,
# and add the necessary code to check if the urls are safe before following them.
#
# References:
# * https://zendesk.atlassian.net/browse/AI-3831
# * https://support.zendesk.com/agent/tickets/1106362
# * https://support.zendesk.com/agent/tickets/1089789
# * Similar fix for sugar CRM: https://github.com/zendesk/zendesk/pull/17302
#
# Tested via test/unit/ms_dynamics_integration_test.rb 'following redirections'

module SOAP
  class HTTPStreamHandler < StreamHandler
    # This is to help us test the redirections fix
    def do_post_call(url, send_string, extheader)
      @client.post(url, send_string, extheader)
    end

    def send_post(url, conn_data, charset)
      conn_data.send_contenttype ||= StreamHandler.create_media_type(charset)

      if @wiredump_file_base
        filename = @wiredump_file_base + '_request.xml'
        f = File.open(filename, "w")
        f << conn_data.send_string
        f.close
      end

      extheader = {}
      extheader['Content-Type'] = conn_data.send_contenttype
      extheader['SOAPAction'] = "\"#{conn_data.soapaction}\""
      extheader['Accept-Encoding'] = 'gzip' if send_accept_encoding_gzip?
      send_string = conn_data.send_string
      @wiredump_dev << "Wire dump:\n\n" if @wiredump_dev
      begin
        retry_count = 0
        res = nil
        loop do
          # Calling local method to help us test the redirections fix
          res = do_post_call(url, send_string, extheader)
          if RETRYABLE && HTTP::Status.redirect?(res.status)
            retry_count += 1
            if retry_count >= MAX_RETRY_COUNT
              raise HTTPStreamError, "redirect count exceeded"
            end
            url = res.header["location"][0]

            # This is the security fix to check if the redirection url is safe.
            unless Zendesk::Net::AddressUtil.safe_url?(url.to_s)
              raise HTTPStreamError, "The redirection url is not safe"
            end

            puts "redirected to #{url}" if $DEBUG
          else
            break
          end
        end
      rescue
        @client.reset(url)
        raise
      end
      @wiredump_dev << "\n\n" if @wiredump_dev
      receive_string = res.content
      if @wiredump_file_base
        filename = @wiredump_file_base + '_response.xml'
        f = File.open(filename, "w")
        f << receive_string
        f.close
      end
      case res.status
      when 405
        raise PostUnavailableError, "#{res.status}: #{res.reason}"
      when 200, 202, 500
        # Nothing to do.  202 is for oneway service.
        nil
      else
        raise HTTPStreamError, "#{res.status}: #{res.reason}"
      end

      # decode gzipped content, if we know it's there from the headers
      if res.respond_to?(:header) && !res.header['content-encoding'].empty? &&
          res.header['content-encoding'][0].casecmp('gzip').zero?
        receive_string = decode_gzip(receive_string)
      # otherwise check for the gzip header
      elsif @accept_encoding_gzip && receive_string[0..1] == "\x1f\x8b"
        receive_string = decode_gzip(receive_string)
      end
      conn_data.receive_string = receive_string
      conn_data.receive_contenttype = res.contenttype
      conn_data
    end
  end
end
