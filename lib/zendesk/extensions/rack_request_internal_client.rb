require 'addressable/uri'

module Zendesk
  module Extensions
    module RackRequestInternalClient
      def internal_client?
        signed_internal_request? || help_center?
      end

      def help_center?
        !!(user_agent =~ /HelpCenter/)
      end

      def signed_internal_request?
        signature == request_signature ||
          env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN].try(:internal?)
      end

      protected

      SYSTEM_USER_NAME = 'zendesk'.freeze

      def signature
        sig = Zendesk::SystemUser::Authentication.signature(system_user_key,
          request_method, request_host, path, request_date)
        "#{SYSTEM_USER_NAME}:#{sig}"
      end

      def system_user_key
        Zendesk::Configuration.dig(:system_user_auth, SYSTEM_USER_NAME, :signed).to_s
      end

      def request_host
        uri = Addressable::URI.parse(url)
        (uri.inferred_port != uri.default_port) ? host_with_port : host
      end

      def request_date
        @env['Date'].to_s
      end

      def request_signature
        @env[Zendesk::SystemUser::Authentication::CLIENT_SYSTEM_API_HEADER].to_s
      end
    end
  end
end

Rack::Request.send(:include, Zendesk::Extensions::RackRequestInternalClient)
ActionDispatch::Request.send(:include, Zendesk::Extensions::RackRequestInternalClient)
