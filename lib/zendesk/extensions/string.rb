require 'unicode'

class String
  def parenthesize
    '(' + self + ')'
  end

  def tokenize(tokenizer = /\S+"[^"]*"|[^(,|;|\s)]+/)
    scan(tokenizer)
  end

  def pluralize(count = nil, plural = nil)
    plural ||= ActiveSupport::Inflector.pluralize(self)
    if count.nil?
      plural
    else
      (count == 1 ? "#{count} #{self}" : "#{count} #{plural}")
    end
  end

  # nori / crack / savon all try to define snake_case, so let's clean up their mess
  alias snakecase underscore
  alias snake_case underscore

  # javascript has problems handling the non-breaking line characters
  def clean_separators(replacement = "\n")
    gsub(/\r\n|\u2028|\u2029/, replacement)
  end

  def strip_all
    gsub(/\u200B|\u200C|\u200D|\u202F|\uFEFF/, "").strip
  end

  def strip_all!
    replace strip_all
  end
end
