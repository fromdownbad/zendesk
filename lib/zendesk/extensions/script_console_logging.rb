require 'etc'

#
# When script/console is run,
# log typed commands into a special log file, for audit reasons.
#
# Technique shamelessly stolen from http://blog.nicksieger.com/articles/2006/04/23/tweaking-irb/
# an old version of the readline saved history support for irb.
#
# This doesn't interfere with the existing readline history support in irb.
#

if $0 == "irb"

  module Readline
    module History
      LOG = Rails.root.to_s + "/log/script_console.log"

      def self.write_log(line)
        File.open(LOG, 'ab') do |f|
          f << "#{Time.now}:#{who}:#{line}\n"
        end
      end

      def self.who
        Etc.getlogin
      end

      def self.start_session_log
        write_log("# session start")
        at_exit { write_log("# session stop") }
      end
    end

    alias :old_readline :readline
    def readline(*args)
      begin
        ln = old_readline(*args)
      ensure
        History.write_log(ln)
      end
      ln
    end
  end

  Readline::History.start_session_log
end
