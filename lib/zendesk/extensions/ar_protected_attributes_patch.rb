# RAILS5UPGRADE: this file will be deleted when rails 5 is updated and protected_attributes is removed
if RAILS5
  ActiveRecord::Base.class_eval do
    def self.attr_accessible(*args)
      ActiveSupport::Deprecation.warn("#{__method__} called with #{args.inspect} should be removed")
    end

    def self.attr_protected(*args)
      ActiveSupport::Deprecation.warn("#{__method__} called with #{args.inspect} should be removed")
    end

    def self.accessible_attributes
      ActiveSupport::Deprecation.warn("#{__method__} should be removed")
    end
  end
end
