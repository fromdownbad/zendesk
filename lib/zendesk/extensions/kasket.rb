ActiveRecord::Base.singleton_class.class_eval do
  def kasket_key_for_with_sharding(attribute_value_pairs)
    key = kasket_key_for_without_sharding(attribute_value_pairs)
    if is_sharded?
      if key.is_a?(Array)
        key
      else
        "shard_#{current_shard_selection.shard}/#{key}"
      end
    else
      key
    end
  end
  alias_method :kasket_key_for_without_sharding, :kasket_key_for
  alias_method :kasket_key_for, :kasket_key_for_with_sharding
end
