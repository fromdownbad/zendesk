module SimpleCov
  class LinesClassifier
    module LineEncoding
      def classify(lines)
        encoded_lines = lines.map { |l| l.encode(Encoding.find('ASCII'), invalid: :replace, undef: :replace, replace: '') }
        super(encoded_lines)
      end
    end
  end
end
