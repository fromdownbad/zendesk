class Hash
  # adding two hashes merges them
  def +(other)
    merge(other)
  end
end
