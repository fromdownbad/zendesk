# All our tables use `id` as primary key.
# This avoids some bugs on Rails 4.2 and is faster.
class << ActiveRecord::Base
  # It's unclear exactly why this extension was added in the first place, but it is most
  # likely related to the overhead of Rails calling `self.class.primary_key` in several locations.
  # Since this was solved for in 6.0, it's likely that this extension can be removed at that point.
  # https://github.com/rails/rails/commit/b6828fc91531ae0cc0a0f216705dd19112596301
  raise "Remove me?" if RAILS6
  def primary_key
    name.start_with?("HABTM") ? nil : 'id'
  end
end
