require 'set'
require 'active_record_inherit_assoc'

# The require above will cause the gem to patch the activerecord classes, so we then need to apply
# our patch to those same classes rather than patching the gem's modules. In other words, this patch
# is order dependent, so don't remove the require.
module ActiveRecordAutoInheritAccountId
  private

  def attribute_inheritance_hash
    if ENV['AUTO_INHERIT_ACCOUNT_ID'] == 'true'
      inherit_opts = Array(reflection.options[:inherit]).to_set
      inherit_opts.add(:account_id) if automatically_add_account_id?

      return nil if inherit_opts.empty?

      inherit_opts.each_with_object({}) do |association, where|
        assoc_value = owner.send(association)
        where[association] = assoc_value

        if reflection.options.key?(:through)
          where["#{through_reflection.table_name}.#{association}"] = assoc_value
        end
      end
    else
      if ENV['AUTO_INHERIT_ACCOUNT_ID_LOGGING'] == 'true' && automatically_add_account_id?
        Rails.logger.info "[AccountIDInheritance] model=#{owner.class.name} association=#{reflection.name}"
      end

      super
    end
  end

  def automatically_add_account_id?
    owner&.respond_to?(:account_id) && remote_table_has_account_id?
  end

  def remote_table_has_account_id?
    if reflection&.polymorphic?
      klass = owner&.association(reflection.name)&.klass
      klass&.is_sharded? && klass&.respond_to?(:account_id)
    else
      reflection&.klass&.column_names&.include?('account_id')
    end
  end
end

ActiveRecord::Associations::Association.prepend(ActiveRecordAutoInheritAccountId)
