# RAILS5UPGRADE: Remove this
raise "[R5U-1297] Remove this entire file" if Rails::VERSION::STRING >= "5.1.0"
return if Rails::VERSION::STRING >= "5"

# general improvements to avoid going through method missing craziness
# compare with lib/active_record/relation/delegation.rb
#
# a = Account.find_by_subdomain('support').shard
# t = a.tickets.find_by_nice_id(2)
# au = t.audits.first
# Benchmark.realtime { 10000.times { au.events.index(1) } }
#
# .arel is the main reason this is slow (calls scope.arel), but caching .scope breaks all kinds of tests
# Benchmark.realtime { 10000.times { au.events.arel } }
raise 'check if this is still good ... then bump this check to re-check later' if Rails::VERSION::STRING >= "5.1.0"
ActiveRecord::Associations::CollectionProxy.class_eval do
  methods =
    if Rails::VERSION::MAJOR == 4
      [:index, :sort_by, :group_by, :detect, :collect, :inject, :reduce, :each_with_index, :each_with_object, :each_slice, :partition]
    else
      [:index] # https://github.com/rails/rails/pull/25789
    end

  delegate(*methods, to: (Rails::VERSION::MAJOR == 4 ? :to_a : :records))
end

# calling for example tickets.entries always calls merge! on the association which is super slow ...
# and 90% of the time we just want to call #target_loaded? which does not need it
# compare to lib/active_record/associations/collection_proxy.rb
# see https://github.com/rails/rails/pull/25789
# when trying to update this, test the patch against actual rails/rails activerecord tests since they cover many more edgecases
#
# speed test via
# a = Account.find_by_subdomain('support').shard
# p = Api::V2::Tickets::TicketPresenter.new(a.owner, url_builder: app);nil
# GC.enable; GC.disable; x = Array.new(100).map { Ticket.all.to_a }.flatten; Benchmark.realtime { p.present(x) }
raise 'remove this' if Rails::VERSION::STRING >= "5.1.0"
ActiveRecord::Associations::CollectionProxy.class_eval do
  def initialize(klass, association) #:nodoc:
    @association = association
    if RAILS4
      super klass, klass.arel_table # FIXME: on 5.0.0 this need to be super klass, klass.arel_table, klass.predicate_builder
    else
      super klass, klass.arel_table, klass.predicate_builder
    end
    @merged = false

    # we need methods from ZendeskArchive::HasManyExtension:
    # - extend is cheap so we can do it here
    # - having it extended here and in merge_association! seems to not create super-loops ... so it's a unique extend
    # - other modules might not be as safe if they use self.extended ... so not doing all
    extensions = (association.options[:extend] || [])
    if extensions.include?(ZendeskArchive::HasManyExtension)
      extend ZendeskArchive::HasManyExtension
    end
  end
end

ActiveRecord::Associations::CollectionProxy.prepend(Module.new do
  # all values that are modified by lib/active_record/relation/merger.rb
  # will merge on demand when accessed
  ActiveRecord::Associations::CollectionProxy.instance_methods.grep(/^(:?.*_values?|as_json|respond_to\?)$/).each do |method|
    define_method(method) do |*args, &block|
      if @merged
        # was a normal method and not overwritten
        super(*args, &block)
      else
        merge_association!
        # maybe defined a new method on this class ... so go back to top
        # added for Zendesk::CustomField::HasManyExtension
        # TODO: remove the need for that and then this can just be super
        send(method, *args, &block)
      end
    end
  end

  private

  # methods might be defined on the association ... merge them and then go back to the top
  def method_missing(name, *args, &block)
    if @merged
      super
    else
      merge_association!
      send(name, *args, &block) # method is defined now, call it and do not call super method_missing
    end
  end

  def merge_association!
    @merged = true
    merge! @association.scope(nullify: false)
  end
end)

# lots of AR internal checks call .to_s on arguments, if a .to_s hits an Arel attribute it will produce a descriptive struct
# that nobody needs since it is always used to check if some list includes that .to_s value
# that takes a long time to generate ... we don't need this and want to save that overhead
raise 'check if this is still good ... then bump this check to re-check later' if Rails::VERSION::STRING >= "5.1.0"
class Arel::Attributes::Attribute
  TO_S_STUB = "<struct Arel::Attributes::Attribute ...>".freeze
  def to_s
    TO_S_STUB
  end
end
