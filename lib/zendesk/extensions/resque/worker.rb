# These extensions are taken from Resque 2.0.0 to address a thundering herd
# problem observed in #inc-2020-06-15-d

module ResqueExtensions
  module DataStore
    REDIS_KEY_FOR_WORKER_PRUNING = 'pruning_dead_workers_in_progress'.freeze

    def acquire_pruning_dead_worker_lock(worker, expiry)
      @redis.set(REDIS_KEY_FOR_WORKER_PRUNING, worker.to_s, ex: expiry, nx: true)
    end
  end

  module Worker
    def prune_dead_workers
      return unless data_store.acquire_pruning_dead_worker_lock(self, Resque.heartbeat_interval)

      super
    end
  end
end

# This patch is backported from Resque 2.0.0.
# https://github.com/resque/resque/pull/1594
if Gem::Version.new(Resque::VERSION) < Gem::Version.new('2.0.0')
  Resque::DataStore.prepend(ResqueExtensions::DataStore)
  Resque::Worker.prepend(ResqueExtensions::Worker)
end
