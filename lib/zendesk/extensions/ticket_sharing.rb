require 'ticket_sharing/ticket'
require 'ticket_sharing/agreement'

module TicketSharing
  class NonVerifyAgreement < Agreement
    def send_to(url)
      client = Client.new(url)
      @response = client.post(relative_url, to_json, ssl: {verify: false})
      @response.status
    end

    def update_partner(url)
      client = Client.new(url, authentication_token)
      @response = client.put(relative_url, to_json, ssl: {verify: false})
      @response.status
    end
  end

  class NonVerifyTicket < Ticket
    def send_to(url)
      raise "Agreement not present" unless agreement

      client = Client.new(url, agreement.authentication_token)
      @response = client.post(relative_url, to_json, ssl: {verify: false})
      @response.status
    end

    def update_partner(url)
      client = Client.new(url, agreement.authentication_token)
      @response = client.put(relative_url, to_json, ssl: {verify: false})
      @response.status
    end
  end

  class FakeHttpResponse < Faraday::Response
    def initialize(status, body)
      super(status: status, body: body)
    end
  end

  Request.class_eval do
    def request_with_multi_ssl(method, url, options)
      options[:raise_errors] = true
      options[:headers] ||= {}
      options[:headers]['Accept'] = 'application/json'
      options[:headers]['Content-Type'] = 'application/json'
      if response = Zendesk::Net::MultiSSL.http_request(method, url, options)
        FakeHttpResponse.new(response.status, response.body)
      else
        FakeHttpResponse.new(500, "Failed in MultiSSL")
      end
    end
    alias_method :request_without_multi_ssl, :request
    alias_method :request, :request_with_multi_ssl
  end

  class InternalSharingFailure < StandardError
    attr_reader :response_code
    alias_method :status, :response_code

    def initialize(response_code)
      super
      @response_code = response_code
    end
  end

  class InternalSharingWarning < InternalSharingFailure; end
  class InternalSharingError < InternalSharingFailure; end

  RETRY_WHITELIST = [403, 404, 405, 422].freeze
  RETRY_AND_ERROR_WHITELIST = [401, 408, 429, 503, 504].freeze

  def self.handle_response(object, method, response)
    return if response.status < 400

    if object.is_a?(Sharing::Agreement)
      agreement = object
      ticket_info = nil
    else
      agreement = object.agreement
      ticket_info = " Shared Ticket #{object.id}"
    end

    Rails.logger.info("Ticket Sharing Log: Client or Server Error (#{response.status}) Account #{object.account.id} Subdomain #{object.account.subdomain} Agreement #{agreement.id}#{ticket_info} Method #{method} Response Body: #{Nokogiri::HTML(response.body).text}")

    return unless agreement.with_zendesk_account?

    raise InternalSharingWarning.new(response.status) if RETRY_WHITELIST.include?(response.status) # rubocop:disable Style/RaiseArgs
    raise InternalSharingError.new(response.status) if RETRY_AND_ERROR_WHITELIST.include?(response.status) # rubocop:disable Style/RaiseArgs
  end
end
