# While we run servers with rails 3 and rails 4 we don't want to encrypt our cookie
# once everything is on rails 4 we can by using the upgrade signed to encrypted strategy
# tested via test/integration/rails_compatibility_test.rb

ActionDispatch::Cookies::ChainedCookieJars.class_eval do
  def signed_or_encrypted
    signed
  end
end

# do not update ... compare to action_dispatch/middleware/cookies.rb:184
ActionDispatch::Cookies::UpgradeLegacySignedCookieJar.class_eval do
  def initialize(*args)
    super
    @verifier = @legacy_verifier
  end

  def verify_and_upgrade_legacy_signed_message(name, signed_message)
    deserialize(name, @legacy_verifier.verify(signed_message))
  rescue ActiveSupport::MessageVerifier::InvalidSignature
    nil
  end
end
