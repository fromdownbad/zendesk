# OpenSSL would previously ignore any extra bytes when digesting values, but now
# it will raise an error if the key is too long. I'm not sure why this key
# needs to be twice as long as the OpenSSL Cipher dictates, but the length came
# from https://github.com/nov/json-jwt/blob/a3f31186/lib/json/jwe.rb#L187-L188
#
# This can go away once zendesk_auth correctly handles the keys with something like:
# https://github.com/zendesk/zendesk_auth/pull/911/commits/f86f30e6d0f1232e3abb87265fcaf6c7187d0408
#
# Failure will raise exceptions like:
#
# ArgumentError: key must be 16 bytes
#   vendor/bundle/ruby/2.4.0/gems/json-jwt-1.9.2/lib/json/jwe.rb:33:in `key='
#   vendor/bundle/ruby/2.4.0/gems/json-jwt-1.9.2/lib/json/jwe.rb:33:in `encrypt!'
#   vendor/bundle/ruby/2.4.0/gems/json-jwt-1.9.2/lib/json/jwt.rb:40:in `encrypt'
module EnforceOpenSSLKeyLength
  KEY_LENGTH = 32

  def key
    super.byteslice(0, KEY_LENGTH)
  end
end

Zendesk::Auth::GAMToken.prepend(EnforceOpenSSLKeyLength)
Zendesk::Auth::ZopimToken.prepend(EnforceOpenSSLKeyLength)
