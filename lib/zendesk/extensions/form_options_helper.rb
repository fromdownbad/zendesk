module Zendesk
  class TruncateMap
    def initialize(items, truncate)
      @items = items
      @truncate = truncate
    end

    def map
      @items.map do |item|
        text, value = yield(item)
        if text.size > @truncate
          [text.truncate(@truncate), value, title: text]
        else
          [text, value]
        end
      end
    end
  end
end

ActionView::Helpers::FormOptionsHelper.class_eval do
  def options_from_collection_for_select_with_truncate(collection, value_method, text_method, selected = nil, options = {})
    if truncate = options[:truncate]
      collection = Zendesk::TruncateMap.new(collection, truncate)
    end
    options_from_collection_for_select_without_truncate(collection, value_method, text_method, selected)
  end

  alias_method :options_from_collection_for_select_without_truncate, :options_from_collection_for_select
  alias_method :options_from_collection_for_select, :options_from_collection_for_select_with_truncate
end
