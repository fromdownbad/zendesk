require 'hashie/mash'

# Hashie::Mash doesn't play nice with Rails 4.

# The reason this happens is because active record performs the check when
# executing query methods (eg. where) and when saving records.
#
# ```
#   if attributes.respond_to?(:permitted?) && !attributes.permitted?
#       raise ForbiddenAttributesError
#   end
# ```
#
# Hashie::Mashie will always pass the conditional and raise an error.
#
# This is a catch-all fix to prevent any accidental uses of Hashie Mash with
# active record. As long as classic depends on Hashie::Mashie then this is
# required.
#
# For more information see: https://zendesk.atlassian.net/browse/CT-2067

module Hashie
  class Mash
    alias_method :original_method_missing, :method_missing
    alias_method :original_respond_to_missing?, :respond_to_missing?

    def method_missing(method_name, *args, &block)
      if method_name == :permitted?
        log_hashie_mash_missuse
        fail NoMethodError
      else
        original_method_missing(method_name, *args, &block)
      end
    end

    def respond_to_missing?(method_name, *args, &block)
      if method_name == :permitted?
        log_hashie_mash_missuse

        false
      else
        original_respond_to_missing?(method_name, *args, &block)
      end
    end

    private

    def log_hashie_mash_missuse
      Rails.logger.warn(
        "The `permitted?` method was used on an instance of Hashie::Mash. This " \
        "is most likely because is was called by ActiveRecord code." \
        "\nStacktrace:\n#{caller.first(10).join("\n")}"
      )
    end
  end
end
