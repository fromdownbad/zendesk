raise "remove me: this bug has been fixed in rails 6" if RAILS6

# shamelessly stolen from rails 6 and tested against rails 4 & 5
# (with minor modifications)

module ActiveRecord
  module Inheritance
    module ClassMethods
      def new(attributes = nil, *_args, &block)
        if abstract_class? || self == Base
          raise NotImplementedError, "#{self} is an abstract class and cannot be instantiated."
        end

        attributes ||= {} # rails4

        if has_attribute?(inheritance_column)
          subclass = subclass_from_attributes(attributes)

          # THIS! WE NEED THIS!
          if subclass.nil? && scope_attributes = current_scope&.scope_for_create
            subclass = subclass_from_attributes(scope_attributes)
          end

          if subclass.nil? && base_class == self
            subclass = subclass_from_attributes(column_defaults)
          end
        end

        if subclass && subclass != self
          subclass.new(attributes, &block)
        else
          super
        end
      end

      if RAILS4
        def subclass_from_attributes(attrs)
          attrs = attrs.to_h if attrs.respond_to?(:permitted?)
          if attrs.is_a?(Hash)
            subclass_name = attrs[inheritance_column.to_sym] || attrs[inheritance_column.to_s]

            if subclass_name.present?
              find_sti_class(subclass_name)
            end
          end
        end

        def has_attribute?(attr_name) # rubocop:disable Naming/PredicateName
          _attr.key?(attr_name.to_s)
        end

        def _attr
          @_attr ||= attribute_names.map { |k| [k, k] }.to_h
        end
      end
    end
  end
end
