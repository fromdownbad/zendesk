# Rails 4 manages CSRF token differently from Rails 3
# This patch simulates the same behaviour we use to have in Rails 3
# This patch is required by our controllers and our engines, for that reason we
# need to patch ActionController::Base instead of ApplicationController

if RAILS4
  ActionController::Base.class_eval do
    def form_authenticity_token
      session[:_csrf_token] ||= SecureRandom.base64(48)
    end

    def valid_authenticity_token?(_session, authenticity_token)
      authenticity_token == form_authenticity_token
    end
  end
end
