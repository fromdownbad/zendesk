# Hook into the declaration of action callbacks so we can trace each of those methods with APM.
#
# The `*names.select { |n| n.is_a?(Symbol) }` block is a quick and easy way to ensure that we are
# only hooking callbacks that are defined as methods, because callbacks can also be defined as a
# proc or lambda. For now, proc and lambda callbacks are not traced.
module TraceActionControllerCallbacks
  include ZendeskAPM::MethodTracing

  APM_SERVICE_NAME = 'classic-rails-controller'.freeze

  def before_action(*names, &block)
    trace_methods(
      *names.select { |n| n.is_a?(Symbol) },
      trace_name: 'action_controller.before_action',
      service: APM_SERVICE_NAME
    )

    super
  end

  def around_action(*names, &block)
    trace_methods(
      *names.select { |n| n.is_a?(Symbol) },
      trace_name: 'action_controller.around_action',
      service: APM_SERVICE_NAME
    )

    super
  end

  def after_action(*names, &block)
    trace_methods(
      *names.select { |n| n.is_a?(Symbol) },
      trace_name: 'action_controller.after_action',
      service: APM_SERVICE_NAME
    )

    super
  end
end

# Hook into the overall callback execution for a controller.
#
# Because the module above doesn't hook into lambda callbacks, this can help us see the overall time
# spent for callbacks. Places that have gaps are likely a callback defined as a proc or a lambda.
module TraceActionControllerCallbackChain
  def run_callbacks(*args)
    ZendeskAPM.trace(
      'action_controller.run_callbacks',
      resource: self.class.name,
      service: TraceActionControllerCallbacks::APM_SERVICE_NAME
    ) { super }
  end
end

if ENV['ENABLE_CONTROLLER_CALLBACK_TRACING'] == 'true'
  ActionController::Base.singleton_class.prepend(TraceActionControllerCallbacks)
  ActionController::Base.prepend(TraceActionControllerCallbackChain)
end
