module ArBecomesNew
  def becomes_new(klass)
    new_object = klass.new
    new_object.shard_id = shard_id if respond_to?(:shard_id)
    attributes.keys.each do |attribute_name|
      next if %w[id shard_id].include?(attribute_name)
      value = send(attribute_name)
      new_object.send(:"#{attribute_name}=", value)
    end
    new_object
  end
end

ActiveRecord::Base.include(ArBecomesNew)
