# This is an ugly patch to ensure we return the Content-Length header in Rails 4 requests
# even when it doesn't always make sense. This maintains behavior from Rails 3.
#
# https://support.zendesk.com/agent/tickets/3593669
#
# In Rails 3, the body returned is an Array instance so it responds to the `to_ary` method,
# but in Rails 4 the body returned is a Rack::BodyProxy instance which does not respond to
# `to_ary`. This means the check done by the Rack::ContentLength middleware no longer succeeds
# and the Content-Length header is no longer added.
#
# Rack::ContentLength middleware for Rails 3:
# https://github.com/rack/rack/blob/1.4.7/lib/rack/content_length.rb
#
# Rack::ContentLength middleware for Rails 4:
# https://github.com/rack/rack/blob/1.6.8/lib/rack/content_length.rb
#

Rack::ContentLength.prepend(Module.new do
  def call(env)
    status, headers, body = super(env)

    # Nginx will get mad at us if we set the content-length header but then don't reply with that
    # much data. This is exactly how we render attachments, so in that case we can remove the
    # content-length header and nginx will just be done when it stops receiving data.
    if headers['X-Accel-Redirect']
      headers.delete(::Rack::CONTENT_LENGTH)
      return [status, headers, body]
    end

    # Don't add content-length if it doesn't make sense for the request, like a 204 No Content, or
    # if it's already there.
    if ::Rack::Utils::STATUS_WITH_NO_ENTITY_BODY.include?(status.to_i) || headers[::Rack::CONTENT_LENGTH]
      return [status, headers, body]
    end

    # it looks like we need to add the content-length headers. Do our best to compute the number of
    # bytes in the content.
    length = 0
    body.each { |part| length += ::Rack::Utils.bytesize(part) }

    headers[::Rack::CONTENT_LENGTH] = length.to_s

    [status, headers, body]
  end
end)
