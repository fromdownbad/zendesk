# original_filename is force converted to utf-8, but we want valid utf-8
ActionDispatch::Http::UploadedFile.prepend(Module.new do
  def initialize(*args)
    super
    @original_filename.to_utf8! if @original_filename
  end
end)
