require 'attachment_fu'
require 'fastimage'

Technoweenie::AttachmentFu.mime_type_detector = -> (file_data) do
  content_type = file_data.content_type.blank? ? nil : file_data.content_type
  Zendesk::Utils::MimeTypes.determine_mime_type(file_data, content_type)
end

Technoweenie::AttachmentFu::InstanceMethods.module_eval do
  def foreign_keys
    foreign_keys = []
    with_each_store(true) do |store|
      foreign_keys << store.foreign_key if store.respond_to? :foreign_key
    end
    foreign_keys
  end

  def exist?
    with_each_store(true) do |store|
      return true if store.exist?
    end
    false
  end

  protected

  # set default behavior to same as in attachment_fu, breaking it out for subclasses to override
  def destroy_thumbnails?
    thumbnailable?
  end

  def destroy_thumbnails
    thumbnails.each(&:destroy) if destroy_thumbnails?
  end
end

Technoweenie::AttachmentFu::Backends::S3Backend.class_eval do
  def exist?
    bucket.object(full_filename).exists?
  end
end

Technoweenie::AttachmentFu::Backends::FileSystemBackend.class_eval do
  def exist?
    File.exist?(full_filename)
  end
end

Technoweenie::AttachmentFu::Backends::S3Backend.class_eval do
  def foreign_key
    [:s3, bucket_name, full_filename]
  end
end

Technoweenie::AttachmentFu::Backends::FileSystemBackend.class_eval do
  def foreign_key
    # these are invalid anyways, but I know some accounts still have them.
    # so lets include this to avoid an exception when it finally strikes.
    [:fs]
  end
end
