require 'yajl'
require 'yajl/json_gem' # Overwrites JSON.parse

# Based on activesupport: lib/active_support/json/encoders/enumerable.rb
# May need to be changed for Rails 3
class Array
  # TODO: write a test that that fails without this or remove it
  def to_json(options = {})
    Yajl::Encoder.encode(as_json(options))
  end

  def as_json(options = {})
    map { |v| v.as_json(options) }
  end
end

class Object
  # TODO: write a test that that fails without this or remove it
  def to_json(options = {})
    Yajl::Encoder.encode(as_json(options))
  end
end

ActiveSupport::JSON::Encoding.time_precision = 0

# Don't escape true/false/null, as JSON doesn't
# See https://github.com/brianmario/yajl-ruby/issues/99
class TrueClass
  def as_json(_options = nil)
    true
  end

  def encode_json(_encoder)
    'true'
  end
end

class FalseClass
  def as_json(_options = nil)
    false
  end

  def encode_json(_encoder)
    'false'
  end
end

class NilClass
  def as_json(_options = nil)
    nil
  end

  def encode_json(_encoder)
    'null'
  end
end

require 'json/jwt'
module JSON
  class JWT
    def to_json(options = {})
      Yajl::Encoder.encode(as_json(options))
    end
  end
end
