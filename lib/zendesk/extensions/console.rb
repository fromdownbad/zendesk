require "active_record_shards"

Zendesk::Application.configure do
  console do
    require 'zendesk_core_application/console_helper'

    tmp = Bundler.root.join('tmp')
    FileUtils.chmod_R(0777, tmp) if File.owned?(tmp)

    ::String.class_eval do
      def to_ticket
        subdomain, nice_id = split("/")
        (account = Account.find_by_subdomain(subdomain)) && Rails::ConsoleMethods.shard(account) && account.tickets.find_by_nice_id(nice_id)
      end

      def to_user
        subdomain, email = split("/")
        (account = Account.find_by_subdomain(subdomain)) && Rails::ConsoleMethods.shard(account) && account.user_identities.email.find_by_value(email).try(:user)
      end
    end

    Rails.logger.push_tags "Console:#{ENV["USER"]}" # show username in log
    ActiveRecord::Base.logger = Rails.logger # rails otherwise redirects all sql to only console
    ActiveRecord::Base.logger.level = Logger::DEBUG
    Rails.logger.subscribe(Rails.logger.collator)

    require 'arturo/test_support' unless Rails.env.production?

    require "zendesk/console_helpers"
    Rails::ConsoleMethods.send(:prepend, Zendesk::ConsoleHelpers)

    Rails.configuration.after_initialize do
      Zendesk::ConsoleHelpers.log_user("login")
    end

    # show which connection was picked to debug master/slave slowness when both servers are the same
    require 'active_record_shards/sql_comments'
    ActiveRecordShards::SqlComments.enable

    puts "App version - #{Zendesk::ConsoleHelpers.app_version}"

    at_exit { Zendesk::ConsoleHelpers.log_user("logout") }

    # Temporary opt-in
    unless ENV['USE_PRY'] == 'false'
      puts <<~BANNER
        Classic's console now uses the `pry` extension by default.
        Run `help` for available commands or visit https://github.com/pry/pry/wiki
        If you need to disable paging, run `pry_instance.config.pager = false`
      BANNER
      require 'pry'
      require 'pry-rails/prompt'
      Rails.application.config.console = Pry
      TOPLEVEL_BINDING.eval('self').extend ::Rails::ConsoleMethods
      Pry.config.prompt = Pry::Prompt.all['rails']
    end
  end
end
