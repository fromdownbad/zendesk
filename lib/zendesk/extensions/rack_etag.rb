Rack::ETag.class_eval do
  # The only change here is that we do not skip caching when Cache-Control contains no-cache.
  # See test/integration/http_caching_test.rb
  def skip_caching?(headers)
    headers.key?('ETag') || headers.key?('Last-Modified')
  end
end
