Kasket::ConfigurationMixin.class_eval do
  def kasket_key_prefix
    @kasket_key_prefix ||= "kasket-#{Kasket::Version::PROTOCOL}/#{table_name}/version=#{column_names.join.sum}/"
  end
end
