# Patch URI::Parser#unescape to force inputs and outputs to UTF-8 and
# avoid Encoding::CompatibilityErrors
# This code is based on https://github.com/rails/rails/blob/master/activesupport/lib/active_support/core_ext/uri.rb
URI::Parser.class_eval do
  remove_method :unescape
  def unescape(str, escaped = /%[a-fA-F\d]{2}/)
    str.to_utf8.gsub(escaped) { [$&[1, 2].hex].pack('C').force_encoding(Encoding::UTF_8) }.to_utf8
  end
end
