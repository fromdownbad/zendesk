ActiveRecord::Base.class_eval do
  # copied from http://api.rubyonrails.org/classes/ActiveRecord/Base.html#M002310
  # and made work with scopes

  # we fetch sql segments in a few placed to re-combine scopes
  # TODO: deprecate... switch to using AR/arel if possible
  def self.condition_sql
    conditions = where(nil).where_sql.to_s.sub("WHERE ", "").presence
    if conditions
      bound_values = if RAILS4
        all.bind_values.map(&:second)
      else
        all.bound_attributes.map(&:value)
      end
      conditions = replace_bind_variables(conditions, bound_values)
    end
    conditions
  end

  # TODO: deprecate... switch to using AR/arel if possible: where(nil).arel.orders
  def self.order_sql
    where(nil).order_values.map do |order_value|
      order_value.respond_to?(:to_sql) ? order_value.to_sql : order_value
    end.join(", ").presence
  end

  # TODO: deprecate... switch to using AR/arel if possible
  def self.group_sql
    where(nil).to_sql[/GROUP BY [^A-Z]+/].to_s.strip.sub("GROUP BY ", "").presence
  end
end

# Rails 2 ordering logic order("a").order("b") = "ORDER b, a"
ActiveRecord::QueryMethods.class_eval do
  def order(*args)
    return self if args.blank?
    current_order = order_values
    current_order.present? ? reorder(*args, current_order) : reorder(*args)
  end
end
