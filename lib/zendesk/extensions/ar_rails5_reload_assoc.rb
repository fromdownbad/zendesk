if RAILS4
  module ReloadAssocExtension
    def self.__extend_assoc(assoc_type)
      define_method assoc_type do |name, *args, &blk|
        super(name, *args, &blk)
        define_method "reload_#{name}" do
          send name, true
        end
      end
    end

    __extend_assoc :has_one
    __extend_assoc :belongs_to
  end

  ActiveRecord::Base.extend ReloadAssocExtension
elsif RAILS51
  raise "remove me!"
end
