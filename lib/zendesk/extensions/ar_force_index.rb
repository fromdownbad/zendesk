# We are using this hack because periodically the query execution plan will differ
# on various db servers and when that happens, we'll use this method to force the
# correct index.

class << ActiveRecord::Base
  def use_index(index)
    from("#{table_name} USE INDEX(#{index})")
  end

  def force_index(index)
    from("#{table_name} FORCE INDEX(#{index})")
  end
end
