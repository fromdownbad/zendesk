If you add something to extensions:

 - make sure it cannot be done via a local hack instead
 - consider making it a gem -> easier to test/upgrade
 - always add tests or list tests that will fail if it is removed
 - document: why it was added
 - document: what needs to be done to get rid of it (e.g. upgrade rails or remove api v1 etc)
 - use class_eval and alias_method_chain instead of simply overwriting, so things blow up when e.g. the original method is missing
 - use long descriptive / easy to grep method/class names
