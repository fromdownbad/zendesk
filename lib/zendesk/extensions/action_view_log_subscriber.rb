# Changes log level from info to debug to prevent excessive logging of partial rendering in production
ActionView::LogSubscriber.class_eval do
  def render_template(event)
    message = "  Rendered #{from_rails_root(event.payload[:identifier])}"
    message << " within #{from_rails_root(event.payload[:layout])}" if event.payload[:layout]
    message << (" (%.1fms)" % event.duration)
    debug(message)
  end
  alias :render_partial :render_template
  alias :render_collection :render_template
end
