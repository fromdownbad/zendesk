ActiveRecord::QueryMethods.class_eval do
  def build_joins_with_no_readonly(*args)
    result = build_joins_without_no_readonly(*args)
    @implicit_readonly = false
    result
  end
  alias_method :build_joins_without_no_readonly, :build_joins
  alias_method :build_joins, :build_joins_with_no_readonly
end
