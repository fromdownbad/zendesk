ActionDispatch::Cookies::SignedCookieJar.prepend(Module.new do
  def serializer
    @serializer ||= Zendesk::DisableComplexSessionValues
  end
end)

module Zendesk
  class DisableComplexSessionValues
    SIMPLE = [
      Integer, Float, ActiveSupport::SafeBuffer, String, Date, Time,
      ActiveSupport::TimeWithZone, Hash, Array, FalseClass, TrueClass, NilClass,
      ActiveSupport::HashWithIndifferentAccess, Symbol
    ].freeze

    class << self
      def load(value)
        value = Marshal.load(value)
        ensure_simple(value)
        value
      end

      def dump(value)
        ensure_simple(value)
        Marshal.dump(value)
      end

      def ensure_simple(value)
        unless (all_classes(value, []) - SIMPLE).empty?
          raise ArgumentError, "complex_object_in_session: Trying to serialize non-json serializable object #{value.inspect}"
        end
      end

      private

      def all_classes(value, collected)
        if value.is_a?(Hash) || value.is_a?(Array)
          value.each { |v| all_classes(v, collected) }
        else
          collected << value.class
        end
        collected
      end
    end
  end
end
