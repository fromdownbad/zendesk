require "mail"
puts "refresh those hacks #{__FILE__}" unless Mail::VERSION::STRING > "2.5"

# make email body non-encoding aware, so it does not try to decode itself before encoding
# these hacks will break email parsing with Mail, but we do not use it
Mail::Message.class_eval do
  # overwritten to disable transfer encoding being switched around by .encode
  remove_method :identify_and_set_transfer_encoding
  def identify_and_set_transfer_encoding
  end

  # overwritten to disable encoding being set on body, so body always stays pure
  # and is only encoded into what we want
  remove_method :add_encoding_to_body
  def add_encoding_to_body
  end
end

Mail::Body.class_eval do
  # just use the encoding we want and stop asking questions...
  # also fixes errors when body is nil/empty
  def get_best_encoding(target)
    Mail::Encodings.get_encoding(target)
  end
end
