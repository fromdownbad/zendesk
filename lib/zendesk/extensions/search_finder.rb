class ActiveRecord::Base
  # TODO: here's a query not scoped to account. We must fix this. and carefully.
  def self.find_for_search_results(ids:, stub_only: true, include_models: nil)
    scope = where(id: ids)
    scope = scope.includes(include_models) if include_models

    if respond_to?(:supports_archiver_v2?) && supports_archiver_v2?
      scope.all_with_archived(stub_only: stub_only)
    else
      scope.all
    end
  end
end
