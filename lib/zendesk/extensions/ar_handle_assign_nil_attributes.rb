# Rails used to allow update to receive nil. That changed in 4.1 here: https://github.com/rails/rails/pull/9860
# The protected_attributes gem allowed this behavior to continue. Once protected_attributes is removed, continue
# to allow this behavior, but add a deprecation warning about it so it can be phased out.

module NilAttributeAssignment
  def assign_attributes(new_attributes)
    if new_attributes.nil?
      ActiveSupport::Deprecation.warn("Updating with `nil` attributes is deprecated. Please check the param value first.")
      return
    end
    super
  end
end

unless defined?(ProtectedAttributes)
  ActiveRecord::Base.prepend NilAttributeAssignment
end
