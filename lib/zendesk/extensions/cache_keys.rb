# Use subdomain as cache key rather than full host. To avoid recaching same data for CNAME aliased accounts.
module Zendesk
  module Extensions
    module CacheKeys
      def fragment_cache_key_prefix
        @fragment_cache_key_prefix ||= begin
          parts = [:views]
          parts <<
            if ::I18n.translation_locale
              ::I18n.translation_locale.cache_key
            elsif ::I18n.locale
              ::I18n.locale.to_s
            end
          parts << ::Time.zone.utc_offset
          parts.join('/')
        end
      end

      def fragment_cache_key(key)
        if key.is_a?(Hash)
          key = key.merge(host: current_account.subdomain, account_id: current_account.id) if current_account
          key = url_for(key).split("://").last
        end

        ActiveSupport::Cache.expand_cache_key(key, fragment_cache_key_prefix)
      end
    end
  end
end

ActionController::Base.class_eval do
  include Zendesk::Extensions::CacheKeys
end

ScopedCacheKeys.prepend(Module.new do
  def scoped_cache_key(scope)
    a = if is_a?(Account)
      self
    elsif respond_to?(:account)
      account
    end

    cache_version = a.settings.account_cache_version

    ActiveSupport::Cache.expand_cache_key([a.id, cache_version], super)
  end
end)

ActiveRecord::Base.prepend(Module.new do
  def cache_key
    expansion_key = if respond_to?(:account_id) && account_id
      # Prefer to expand by account_id
      account_id
    elsif created_at
      # If account isn't available we can
      # add created_at to decrease collision chance
      created_at.to_f
    else
      # Lastly, try to prevent collisions for uninitialized records
      Time.now.to_f
    end

    ActiveSupport::Cache.expand_cache_key(expansion_key, super)
  end
end)
