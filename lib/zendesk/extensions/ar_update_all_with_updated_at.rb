ActiveRecord::Base.class_eval do
  def self.update_all_with_updated_at(set, conditions = {})
    if set.is_a?(String)
      set += ", updated_at = now()"
    else
      set[:updated_at] = Time.now.utc
    end
    where(conditions).update_all(set)
  end
end
