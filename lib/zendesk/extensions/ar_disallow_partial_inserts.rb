# Do not use partial writes when creating records.
# When partial INSERTs are supported, and and insert fails, and then the insert
# is retried, a bug in ActiveRecord will cause an INSERT statement that ONLY
# inserts a value for the `id` column, and no other columns.
# See https://support.zendesk.com/agent/tickets/3687276,
# https://docs.google.com/document/d/10YGbwPjtmV-mPtcbh72lwfeSvx4uYggWqyXkrM7KYP8/edit,
# and https://docs.google.com/document/d/1L0tIDNO-yOLBr4m5G7WfSAZium0mZblYjVuqrxqOkj4/edit
module ActiveRecord
  module AttributeMethods
    module Dirty
      # We're overriding _create_record in ActiveRecord version 4.  The
      # method doesn't exist in ActiveRecord version 3.
      def _create_record(*)
        # Original code looked like:
        # partial_writes? ? super(keys_for_partial_write) : super
        super
      end if RAILS4
    end
  end
end
