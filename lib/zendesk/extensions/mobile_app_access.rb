ActionController::Base.class_eval do
  def self.prevent_zendesk_mobile_app_access
    before_action :block_zendesk_mobile_access
  end

  def self.allow_zendesk_mobile_app_access(args = {})
    skip_before_action :block_zendesk_mobile_access, args
  end

  private

  def block_zendesk_mobile_access
    if Zendesk::Accounts::Source.zendesk_app_user_agent?(request.user_agent)
      Rails.logger.error "Blocking mobile app request for #{controller_name}:#{action_name} #{request.url}"
      head 450
    end
  end
end
