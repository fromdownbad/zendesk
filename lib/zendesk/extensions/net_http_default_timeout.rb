require 'net/http'

# Until Ruby 2.3, Net::HTTP did not have a default open_timeout. This means
# network interuptions can cause clients to block indefinitely. This is never
# desirable behavior, but few developers know about it. Instead of reviewing
# every instance of Net::HTTP, Faraday, or its dependencies, just patch
# a global default with a sane value.
#
# After Ruby 2.3, a default timeout of 60 seconds was added. That's still
# unreasonably dangerous.
module NetHTTPDefaultTimeout
  def initialize(*args)
    super
    # Up for debate. I've picked 6 to allow for one DNS lookup packet drop,
    # which are retried after 5 seconds by the kernel and have been common.
    @open_timeout ||= 6
  end
end

Net::HTTP.prepend NetHTTPDefaultTimeout
