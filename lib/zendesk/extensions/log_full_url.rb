# make the logger show full url after the request ends, so we have all info on 1 line
# Completed 200 OK in 2590ms (Views: 29.8ms | ActiveRecord: 66.8ms | http://support.localhost:3000/ping/host)
puts "recheck #{__FILE__}:#{__LINE__}" if RAILS5
ActionController::Instrumentation.class_eval do
  def append_info_to_payload_with_full_url(payload)
    payload[:full_url] = "#{request.protocol}#{request.host_with_port}#{request.fullpath}"
    append_info_to_payload_without_full_url(payload)
  end
  alias_method :append_info_to_payload_without_full_url, :append_info_to_payload
  alias_method :append_info_to_payload, :append_info_to_payload_with_full_url
end

class << ActionController::Base
  def log_process_action_with_full_url(payload)
    log_process_action_without_full_url(payload) + [payload[:full_url]]
  end
  alias_method :log_process_action_without_full_url, :log_process_action
  alias_method :log_process_action, :log_process_action_with_full_url
end
