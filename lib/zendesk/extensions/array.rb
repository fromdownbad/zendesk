puts "Recopy this and minor-bump the check #{__FILE__}:#{__LINE__}" if RAILS51

# Copied from lib/active_support/core_ext/array/conversions.rb to add
# this (reapply when updating):
#
# - add count
# - "objects" --> "records"
#
# look for `ZENDESK` tags below for 2 1-line additions:

Array.class_eval do
  def to_xml(options = {})
    require 'active_support/builder' unless defined?(Builder)

    options = options.dup
    options[:indent] ||= 2
    options[:builder] ||= Builder::XmlMarkup.new(indent: options[:indent])
    options[:root]    ||= \
      if first.class != Hash && all? { |e| e.is_a?(first.class) }
        underscored = ActiveSupport::Inflector.underscore(first.class.name)
        ActiveSupport::Inflector.pluralize(underscored).tr('/', '_')
      else
        # 'objects'
        'records' # ZENDESK
      end

    builder = options[:builder]
    builder.instruct! unless options.delete(:skip_instruct)

    root = ActiveSupport::XmlMini.rename_key(options[:root].to_s, options)
    children = options.delete(:children) || root.singularize
    attributes = options[:skip_types] ? {} : { type: 'array' }
    attributes[:count] = count # ZENDESK

    if empty?
      builder.tag!(root, attributes)
    else
      builder.tag!(root, attributes) do
        each { |value| ActiveSupport::XmlMini.to_tag(children, value, options) }
        yield builder if block_given?
      end
    end
  end
end
