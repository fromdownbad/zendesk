require "cia"
require "zendesk/extensions/truncate_attributes_to_limit"

CIA.exception_handler = lambda do |e|
  Rails.logger.error("Failed to record audit: #{e}\n#{e.backtrace}")

  if Rails.env.production?
    ZendeskExceptions::Logger.record(e, location: CIA, message: "Error during auditing", fingerprint: '86c080e43c7ed86f5d5b124c9dafb222f886f3b4')
  else
    raise e
  end
end

CIA.non_recordable_attributes = [:effective_actor]

CIA::Event.class_eval do
  # Prevent predictive load from preloading because the source may be a record from another app.
  belongs_to :source, polymorphic: true, predictive_load: false

  def self.visible
    where(visible: true)
  end

  # all indices include visible, so specify it without making it do anything
  def self.using_visible_index
    where(visible: [true, false])
  end

  def self.for_account(account)
    where(account_id: account.id)
  end

  def self.for_source_type(type)
    type.present? ? where(source_type: type) : all
  end

  def self.created_between(from, to)
    if from && to
      where(['created_at >= ? and created_at <= ?', from.to_date.beginning_of_day, to.to_date.end_of_day])
    else
      all
    end
  end

  def self.account_for(source)
    source.is_a?(Account) ? source : source.account
  end

  # tested via test/functional/settings/account_controller_test.rb
  def self.assign_exact_source_type(events)
    rule_events = events.select { |e| e.source_type == "Rule" }
    rules = Rule.select("id, type").where(id: rule_events.map(&:source_id).uniq).map { |rule| [rule.id, rule.class.name] }
    rules = Hash[rules]
    rule_events.each { |e| e.source_type = rules[e.source_id] if rules[e.source_id] }
  end

  def self.inexact_source_type(type)
    if type == "Rule" || Rule::SUBTYPES.include?(type)
      "Rule"
    else
      type
    end
  end

  belongs_to :account
  validates_presence_of :account_id

  before_create :prevent_cross_shard_access, :set_visibility
  after_create :report_missing_actor

  def source_with_denormalize=(thing)
    self.source_without_denormalize = thing

    self.account ||= CIA::Event.account_for(source)

    self.source_display_name ||=
      case source_type
      when "Ticket" then "##{source.nice_id}"
      when "TicketArchiveStub" then "##{source.nice_id}"
      when "Rule" then source.title
      when "User" then source.safe_name(true)
      when "UserSetting" then source.user.name
      when "MonitoredTwitterHandle" then source.mention_name
      when "Facebook::Page" then source.name
      when "UserIdentity" then "#{source.value} for #{source.user.name}"
      when "PermissionSet" then source.name
      when "TicketField" then source.title
      when "Organization" then source.name
      when "Brand" then source.name
      when "Route" then source.brand.try(:name)
      when "RecipientAddress" then "#{source.name} #{source.email}".strip
      when "Sla::Policy" then "#{source.title} (#{source.id})"
      when "Zendesk::BusinessHours::Schedule" then source.name.to_s
      when "TicketForm" then source.name
      when "ApiToken" then source.description
      else
        ""
      end
  end
  alias_method :source_without_denormalize=, :source=
  alias_method :source=, :source_with_denormalize=

  truncate_attributes_to_limit :message

  private

  INTERNAL_AUDIT_TYPES = ['Voice::', 'SubscriptionFeature', 'CustomSecurityPolicy', 'RemoteAuthentication', 'TicketFieldCondition', 'InboundMailRateLimit'].freeze
  INTERNAL_AUDIT_NAMES = ['is_abusive', 'is_watchlisted', 'risk_assessment', 'user_identity_limit_for_trialers', 'whitelisted_from_fraud_restrictions'].freeze

  def set_visibility
    return true if INTERNAL_AUDIT_TYPES.map { |internal| source_type.try(:match, /^#{internal}/) }.any?
    return true if account.has_internal_audit_name_visibility_check? && hide_sensitive_account_setting?
    self.visible = (action != "update" || attribute_changes.any?(&:visible?)) unless visible?
    true # continue chain
  end

  def hide_sensitive_account_setting?
    source_type == 'AccountSetting' && INTERNAL_AUDIT_NAMES.include?(source&.name)
  end

  # this can be simplified once old monitor is dead
  def prevent_cross_shard_access
    return unless actor_id
    return if actor_id == User.system_user_id
    return if actor && actor.account_id == account_id

    if actor && actor.account.subdomain == 'support'
      self.via_id = Zendesk::Types::ViaType.IMPORT
      self.via_reference_id = actor_id
      self.actor = User.system
    else
      raise "!! invalid cross shard access !!!" # caught by exception_handler
    end
  end

  def report_missing_actor
    return if actor_id && actor_type
    # cached by error handler in production
    raise "Actor missing -- when registering: `CIA.current_actor = new_user` -- for system changes: `CIA.current_actor = User.system`" # exception_handler
  end
end

require 'zendesk/extensions/truncate_attributes_to_limit'

CIA::AttributeChange.class_eval do
  truncate_attributes_to_limit :new_value, :old_value

  # keep in sync with CIA::AttributeChangePresenter + alphabetical
  CIA::AttributeChange::VISIBLE_ATTRIBUTES = [
    "active",
    "agent_security_policy_id",
    "assumption_duration",
    "assumption_expiration",
    "attribute_values",
    "authorized",
    "billing_cycle_type",
    "crypted_password",
    "default",
    "definition",
    "deleted_at",
    "description",
    "email",
    "email_list",
    "end_date",
    "end_time",
    "end_user_security_policy_id",
    "frequency",
    "host_mapping",
    "include_messages",
    "include_posts_by_page",
    "include_wall_posts",
    "is_active",
    "is_sender",
    "is_trial",
    "max_agents",
    "name",
    "opted_in",
    "owner_id",
    "permission_set_id",
    "plan_type",
    "position",
    "rate_limit",
    "roles",
    "settings",
    "signature_template",
    "start_date",
    "start_time",
    "state",
    "subdomain",
    "ticket_id",
    "time_zone",
    "title",
    "trial_expires_on",
    "value",
    "voice_deleted_at",
    "voice_plan_type",
    "voice_trial",
    "workweek",
  ].freeze
  CIA::AttributeChange::COLUMN_WIDTH = 255

  def self.for_account(account)
    where(account_id: account.id)
  end

  def self.for_source_type(type)
    type.present? ? where(source_type: type) : all
  end

  belongs_to :account
  validates_presence_of :account_id

  # Prevent predictive load from preloading because the source may be a record from another app.
  belongs_to :source, polymorphic: true, predictive_load: false

  before_validation { |o| o.account = o.event.account }

  def visible?
    if AuditableProperty::AUDIT_ALL.include?(source_type) # only load sources if we have to
      source.try(:visible_property?)
    else
      CIA::AttributeChange::VISIBLE_ATTRIBUTES.include?(attribute_name)
    end
  end
end
