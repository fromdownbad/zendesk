class ActiveRecord::Base
  def smart_touch
    return if new_record?
    touch_without_callbacks if updated_at < 1.second.ago
  end

  def touch_without_callbacks(column = :updated_at)
    time = Time.current
    send "#{column}=", time
    self.class.where(id: id).update_all(column => time)
    clear_kasket_indices if respond_to? :clear_kasket_indices
    true
  end
end
