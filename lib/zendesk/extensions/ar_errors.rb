ActiveModel::Errors.class_eval do
  # Pass :version => 2 in ActiveRecord::Errors.to_json() to get an error
  # structure that follows the general "Agent Console" error message format.
  #
  # {
  #   "details": {
  #     "value": [
  #       {
  #         "error": "BlankValue",
  #         "type": "blank",
  #         "description": "can't be blank"
  #       },
  #       {
  #         "error": "InvalidFormat",
  #         "type": "invalid",
  #         "description": " is not properly formatted"
  #       }
  #     ]
  #   },
  #   "description": "RecordValidation errors",
  #   "error": "RecordInvalid"
  # }
  #
  def to_json(options = {})
    if options[:version] == 2
      hash = { error: "RecordInvalid", description: "Record validation errors", details: {} }
      each do |attribute, message|
        hash[:details][attribute] ||= []
        details = { description: message } # type should no longer be used -> we always return blank
        details[:error] = message.error if message.respond_to?(:error)
        hash[:details][attribute] << details
      end
      hash.to_json
    else
      # emulate Rails 2 .to_json
      # {:a => ['x', 'y']} --> [[:a, 'x'], [:a, 'y']]
      JSON.parse(super).flat_map do |attribute, messages|
        messages.map { |m| [attribute, m] }
      end.to_json
    end
  end

  alias_method :all_on, :[]
end
