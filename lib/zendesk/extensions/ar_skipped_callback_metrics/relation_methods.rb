# Instrument ActiveRecord::Relation methods that bypass callbacks.
# We need to check the Relation#model is marked for instrumentation as we are only interested in a small set
module ArSkippedCallbackMetrics
  module RelationMethods
    METHODS =
      if Rails::VERSION::MAJOR == 4
        %i[delete delete_all update_all].freeze
      else
        %i[delete_all update_all].freeze
      end

    # We do not want to instrument every Relation, only specific models. _send_callback_metric is mixed into
    # these models, so it's used to indicate a model of interest
    METHODS.each do |method_name|
      define_method method_name do |*args, &block|
        model.try(:_send_callback_metric, model.name, method_name, caller)
        super(*args, &block)
      end
    end
  end
end

ActiveRecord::Relation.prepend(ArSkippedCallbackMetrics::RelationMethods)
