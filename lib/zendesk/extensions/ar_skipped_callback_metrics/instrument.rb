module ArSkippedCallbackMetrics
  # Instrument ActiveRecord models that bypass callbacks
  module Instrument
    extend ActiveSupport::Concern

    included do
      include(ArSkippedCallbackMetrics::InstanceMethods)
      extend(ArSkippedCallbackMetrics::ClassMethods)
    end

    class_methods do
      def _send_callback_metric(klass, method_name, call_stack)
        return if Thread.current[:allow_without_entity_publication]

        ActiveSupport::Notifications.
          instrument(
            'skipped_callback.ar_metrics',
            klass: klass,
            method: method_name,
            caller: call_stack
          )
      end

      def allow_without_entity_publication
        Thread.current[:allow_without_entity_publication] = true
        yield if block_given?
        Thread.current[:allow_without_entity_publication] = false
      end
    end

    ##
    # Stops publication of metrics that bypass callbacks for duration of the block.
    #
    # WARNING: By default this instrumentation publishes metrics for AR methods which skip callbacks.
    # If you use this method, metrics will not be published for AR methods which skip callbacks.
    # These metrics are used to identify inconsistencies in entity publication.
    # By using this method you might leave ticket entity stream in an inconsistent state with respect to the database,
    # by using this method you're acknowledging this is okay
    # #### Example
    #  @example In ticket model
    #  allow_without_entity_publication { update_attributes(subject: 'new subject') }
    #
    #  @example In Tickets controller
    #  Ticket.allow_without_entity_publication { ticket.update_attributes(subject: 'new subject') }
    #
    def allow_without_entity_publication(&block)
      self.class.allow_without_entity_publication(&block)
    end
  end
end
