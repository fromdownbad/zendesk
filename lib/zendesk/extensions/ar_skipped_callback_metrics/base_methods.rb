# Instrument ActiveRecord methods that bypass callbacks.
#
# AR callbacks are used by Ticket Entity stream.
# Methods that bypass callbacks are instrumented so we can attempt to maintain consistency (VORTEX-770)
module ArSkippedCallbackMetrics
  module InstanceMethods
    METHODS = %i[decrement! delete increment! update_column update_columns update_all].freeze

    # ex.
    # def decrement!(*args, &block)
    #   self.class._send_callback_metric("Ticket", :decrement!, caller)
    #   super(*args, &block)
    # end
    METHODS.each do |method_name|
      define_method method_name do |*args, &block|
        self.class._send_callback_metric(self.class.name, method_name, caller)
        super(*args, &block)
      end
    end
  end

  module ClassMethods
    METHODS = %i[delete delete_all update_all decrement_counter increment_counter update_counters].freeze
    METHODS.each do |method_name|
      define_method method_name do |*args, &block|
        _send_callback_metric(name, method_name, caller)
        super(*args, &block)
      end
    end
  end
end

require_relative "instrument" # force load
