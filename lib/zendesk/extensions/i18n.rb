# Add new methods by setting properties on the config class. This allows us to
# avoid using alias_method_chain.
# https://github.com/svenfuchs/i18n/blob/b1200f0/lib/i18n.rb#L30-L36
module I18nWithTranslationLocale
  def translation_locale
    config.translation_locale || ENGLISH_BY_ZENDESK
  end

  def locale=(locale)
    config.translation_locale = if locale.is_a?(TranslationLocale)
      locale
    else
      TranslationLocale.find_by_language_tag(locale)
    end

    super(locale)
  end

  alias_method :translation_locale=, :locale=

  # We patch with_locale to prevent it from making unnecessary database queries every time.
  # When with_locale starts, it saves a temp locale. This locale is turned into a symbol in the
  # writer method: https://github.com/svenfuchs/i18n/blob/b1200f0/lib/i18n/config.rb#L14 which means
  # when the block exits and we reset the locale we call I18n.locale= with a symbol. Since we patch
  # locale= above, this will cause us to execute the TranslationLocale.find_by_language_tag call
  # at the end of every with_locale call.
  #
  # This patch manipulates the stored values directly on the config object just like the original
  # I18n.locale= method does.
  # https://github.com/svenfuchs/i18n/blob/b1200f0/lib/i18n/config.rb#L7-L15
  def with_locale(tmp_locale)
    original_tl = config.translation_locale
    original_l = config.locale

    I18n.locale = tmp_locale

    yield
  ensure
    config.translation_locale = original_tl
    config.locale = original_l
  end
end

# Keep the same method and format that the base i18n library uses for storing
# attributes like locale.
# https://github.com/svenfuchs/i18n/blob/b1200f0/lib/i18n/config.rb#L7-L15
module I18nConfigWithTranslationLocale
  def translation_locale
    @translation_locale
  end

  def translation_locale=(value)
    @translation_locale = value
  end
end

I18n::Config.include(I18nConfigWithTranslationLocale)
I18n.extend(I18nWithTranslationLocale)
