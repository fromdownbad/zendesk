# In Rack 1.6.8 an additional step was added to the Multipart request parser. Each part of the
# request is now passed to a step that checks the specified encoding and calls `force_encoding`
# on the part `name` and body.
#
# Rails 3:
# The name would be extracted here:
# https://github.com/rack/rack/blob/1.4.7/lib/rack/multipart/parser.rb#L22-L23
#
# and then passed to `get_data` to parse the request and then passed along but not used directly:
# https://github.com/rack/rack/blob/1.4.7/lib/rack/multipart/parser.rb#L38
#
# Rails 4:
# The name is extracted in a similar way:
# https://github.com/rack/rack/blob/1.6.8/lib/rack/multipart/parser.rb#L57-L58
#
# but now after we `get_data`, we also perform encoding validations which is where the error occurs:
# https://github.com/rack/rack/blob/1.6.8/lib/rack/multipart/parser.rb#L73-L74
#
# This patch is very simple and just ensures that the `tag_multipart_encoding` method gets the string
# that it expects for the name.
#
# https://support.zendesk.com/agent/tickets/3616012
#

Rack::Multipart::Parser.prepend(Module.new do
  def tag_multipart_encoding(filename, content_type, name, body)
    valid_string_name = name || ""
    super(filename, content_type, valid_string_name, body)
  end
end)
