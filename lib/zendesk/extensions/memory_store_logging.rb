# this just helps debugging cache issues by adding some more logging on local memory caches
ActiveSupport::Cache::MemoryStore.prepend(
  Module.new do
    if RAILS4
      def clear(options = nil)
        log('clear', '*', nil)
        super
      end

      private

      def log(operation, key, options)
        super("local #{operation}", key, options)
      end
    else
      def clear(options = nil)
        instrument('clear', '*', nil) do
          super
        end
      end

      private

      def instrument(operation, key, options = nil)
        super("local #{operation}", key, options)
      end
    end
  end
)
