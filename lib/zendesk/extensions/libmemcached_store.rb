require 'active_support/cache/libmemcached_store'

# ActiveSupport::Cache::Store.instrument defaults to true on Rails 4 (and false on Rails 3), which caused
# instrumentation to be enabled for LibmemcachedStore.
# Monkey patch LibmemcachedStore to disable instrumentation, to keep the same behavior as Rails 3.
module ActiveSupport
  module Cache
    class LibmemcachedStore
      private

      def instrument?
        false
      end
    end
  end
end
