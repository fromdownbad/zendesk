# TODO: move into active_record_shards
# tested via test/functional/api/v1/base_controller_test.rb
ActiveRecord::Base.class_eval do
  cattr_accessor :on_slave_by_default_override
end

class << ActiveRecord::Base
  def with_on_slave_by_default_override
    old = on_slave_by_default_override
    self.on_slave_by_default_override = true
    yield
  ensure
    self.on_slave_by_default_override = old
  end

  def on_slave_by_default_with_override?
    on_slave_by_default_override || on_slave_by_default_without_override?
  end
  alias_method :on_slave_by_default_without_override?, :on_slave_by_default?
  alias_method :on_slave_by_default?, :on_slave_by_default_with_override?
end
