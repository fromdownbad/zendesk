module Zendesk
  module Extensions
    module TimeZone
      def self.included(base)
        base.class_eval do
          include Zendesk::MomentData

          def translated_name
            ::I18n.t('txt.timezone.'.concat(name.parameterize.tr('-', '_')))
          end

          def to_s
            "(GMT#{now.formatted_offset}) #{translated_name}"
          end

          # offsets reflect current DST in the zones
          def cache_key
            [last_modified, name, now.utc_offset].join("/")
          end

          private

          def last_modified
            @@last_modified ||= begin
              if GIT_TIMESTAMPS.blank? # testing on ci
                Time.now
              else
                Gem::Specification.find_by_name('tzinfo').date
              end
            end
          end

          def self.all_sorted
            sort(all)
          end

          def self.sort(tzs)
            tzs.sort_by { |tz| [tz.now.utc_offset, tz.name] }
          end
        end
      end
    end
  end
end

ActiveSupport::TimeZone.send :include, Zendesk::Extensions::TimeZone
