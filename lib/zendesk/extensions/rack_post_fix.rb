# TODO: make a pull request against rack
Rack::Request.class_eval do
  alias_method :POST_with_nil, :POST
  def POST # rubocop:disable Naming/MethodName
    self.POST_with_nil || {}
  end
end
