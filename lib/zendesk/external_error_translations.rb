module Zendesk
  module ExternalErrorTranslations
    def translate_error(message, locale)
      case message

      # errors from https://github.com/ruby/ruby/blob/trunk/lib/csv.rb
      when /Missing or stray quote in line (\d+)/
        ::I18n.t("txt.external_errors.csv.malformed_csv_error.missing_or_stray_quote", line: $1, locale: locale)

      when /Unquoted fields do not allow \\r or \\n \(line (\d+)\)/
        ::I18n.t("txt.external_errors.csv.malformed_csv_error.unquoted_fields_forbid_newlines", line: $1, locale: locale)

      when /Illegal quoting in line (\d+)/
        ::I18n.t("txt.external_errors.csv.malformed_csv_error.illegal_quoting", line: $1, locale: locale)

      when /Unclosed quoted field on line (\d+)/
        ::I18n.t("txt.external_errors.csv.malformed_csv_error.unclosed_quoted_field", line: $1, locale: locale)

      when /Field size exceeded on line (\d+)/
        ::I18n.t("txt.external_errors.csv.malformed_csv_error.field_size_exceeded", line: $1, locale: locale)

      else
        ::I18n.t("txt.external_errors.unknown_error", message: message, locale: locale)
      end
    end
  end
end
