module Zendesk::Entitlement
  TYPES = {
    support: "support",
    chat: "chat",
    explore: "explore",
    voice: "voice",
    connect: "connect"
  }.freeze

  ROLES = {
    support: {
      agent: "agent",
      admin: "admin",
      light_agent: "light_agent",
      chat_only_agent: "chat_only_agent",
      custom: "custom"
    },
    chat: {
      agent: "agent",
      admin: "admin"
    },
    explore: {
      viewer: "viewer",
      studio: "studio",
      admin: "admin"
    },
    guide: {
      agent: "agent",
      admin: "admin",
      viewer: "viewer",
      light_agent: "light_agent"
    },
    connect: {
      agent: "agent"
    },
    talk: {
      admin: "admin",
      lead: "lead",
      agent: "agent"
    }
  }.freeze
end
