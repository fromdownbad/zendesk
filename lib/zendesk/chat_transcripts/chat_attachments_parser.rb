module Zendesk
  module ChatTranscripts
    class ChatAttachmentsParser
      attr_reader :history

      def initialize(history)
        @history = history
      end

      def upload_tokens
        @tokens ||= @history.
          map { |item| item['support_upload_token'] if item['type'] == 'ChatFileAttachment' }.
          compact
      end

      def attachments(account)
        return [] if upload_tokens.empty?
        account.
          upload_tokens.
          find_all_by_value(upload_tokens).
          flat_map(&:attachments)
      end
    end
  end
end
