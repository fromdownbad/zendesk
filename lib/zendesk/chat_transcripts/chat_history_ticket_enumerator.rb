module Zendesk
  module ChatTranscripts
    class TicketWithComment < ::Delegator
      extend Forwardable

      # hide from active_record detection
      undef_method :class
      undef_method :is_a?

      attr_reader :ticket, :comment
      alias_method :__getobj__, :ticket

      def_delegator :comment, :present?, :comment_added?
      def_delegators :comment, :updated_at

      def initialize(ticket, comment:)
        @ticket = ticket
        @comment = comment
      end
    end

    class ChatMessageComment
      class Author
        def initialize(history_item)
          @item = history_item
        end

        # rubocop:disable Naming/PredicateName
        def is_agent?
          @item['actor_type'] == 'agent'
        end

        def is_end_user?
          @item['actor_type'] == 'end-user'
        end
        # rubocop:enable Naming/PredicateName

        def foreign_agent?
          false
        end
      end

      def initialize(history_item)
        @item = history_item
      end

      def from_end_user?
        author.is_end_user?
      end

      # we ignore the agent as end-user scenario here
      def public_reply?
        true
      end

      def empty?
        false
      end

      alias is_public? public_reply?

      # rubocop:disable Naming/PredicateName
      def is_private?
        false
      end
      # rubocop:enable Naming/PredicateName

      def author
        @author ||= Author.new(@item)
      end

      def updated_at
        Time.at(0, @item['timestamp'], :millisecond)
      end
    end

    class ChatHistoryTicketEnumerator
      def initialize(history)
        @history = history
      end

      def with_first_agent_comment(ticket)
        first_message = @history.find { |item| item['type'] == 'ChatMessage' && item['actor_type'] == 'agent' }
        return unless first_message
        yield TicketWithComment.new(ticket, comment: ChatMessageComment.new(first_message))
      end
    end
  end
end
