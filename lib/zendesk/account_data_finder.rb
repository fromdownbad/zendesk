module Zendesk::AccountDataFinder
  module_function

  def find_data(account)
    account_connection = Account.connection
    account_tables = find_tables(account_connection)
    account_table_data = find_db_data(account_tables, account_connection, account.id)
    shard_table_data = ActiveRecord::Base.on_shard(account.shard_id) do
      shard_connection = ActiveRecord::Base.connection
      shard_tables = find_tables(shard_connection)
      find_db_data(shard_tables, shard_connection, account.id)
    end

    {
      account_tables: account_table_data,
      shard_tables: shard_table_data
    }
  end

  def find_tables(connection)
    connection.tables.select { |table| table_has_account_id_column?(table, connection) }
  end

  def table_has_account_id_column?(table, connection)
    !connection.columns(table).select { |column| column.name == 'account_id' }.empty?
  end

  def find_db_data(tables, connection, account_id)
    count_hash = {}
    tables.each do |table|
      count = count_rows(table, connection, account_id)
      count_hash[table.to_sym] = count if count > 0
    end

    Hash[count_hash.reject { |_, v| v == 0 }]
  end

  def count_rows(table, connection, account_id)
    t = Arel::Table.new(table)
    statement = t.where(t[:account_id].eq(account_id)).project(t[:account_id]).to_sql
    rows = connection.execute(statement)
    rows.count
  end
end
