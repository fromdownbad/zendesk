module Zendesk
  module SatisfactionReasons
    module ControllerSupport
      def self.included(controller)
        controller.before_action :ensure_reasons_present
      end

      protected

      def ensure_reasons_present
        if @account.has_csat_reason_code_admin?
          Satisfaction::Reason.create_system_reasons_for(@account) unless @account.satisfaction_reasons.any?
        end
      end

      def update_satisfaction_reasons
        satisfaction_reasons = JSON.parse(params[:account][:satisfaction_reasons])
        params[:account][:satisfaction_reasons] = satisfaction_reasons
        satisfaction_reasons.each do |reason|
          ["value", "translated_value"].each do |v|
            reason[v] = ActionController::Base.helpers.sanitize(reason[v])
          end

          if reason["id"] < 0
            add_new_satisfaction_reason(reason)
          else
            update_existing_satisfaction_reason(reason)
          end
        end
      end

      def add_new_satisfaction_reason(json_reason)
        new_reason = @account.satisfaction_reasons.create(value: json_reason["value"])
        new_reason.activate_for!(@account.default_brand) if json_reason["active"] && new_reason.valid?
      end

      def update_existing_satisfaction_reason(json_reason)
        reason = @account.satisfaction_reasons.find_by_id(json_reason["id"])
        return if reason.blank?

        update_reason_attributes(json_reason, reason)
        toggle_reason_activation(json_reason, reason)
      end

      def update_reason_attributes(json_reason, reason)
        return if reason.system

        reason.update_attributes!(value: json_reason["value"]) if json_reason["edited"]
        reason.soft_delete! if json_reason["deleted"]
      end

      def toggle_reason_activation(json_reason, reason)
        if json_reason["active"]
          reason.activate_for!(@account.default_brand)
        else
          reason.deactivate_for!(@account.default_brand)
        end
      end
    end
  end
end
