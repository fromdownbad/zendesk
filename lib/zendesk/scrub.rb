module Zendesk
  module Scrub
    SCRUB_TEXT = "X".freeze
    SCRUB_ASSOCIATIONS = ["events", "cia_events", "cia_attribute_changes"].freeze
    SCRUBBED_SUBJECT   = "SCRUBBED".freeze
    EXCLUDE_FROM_SCRUB = ["subject"].freeze
    IGNORED_COLUMNS    = ["latest_recipients"].freeze
    APM_SERVICE_NAME   = "scrubber-ticket-execution".freeze

    NUMERIC_FIELDS = [FieldInteger, FieldDecimal].freeze
    DEFAULT_SCRUB_SERIALIZED_VALUES = {
      VoiceComment        => { 'X' => 'X' },
      TwitterEvent        => ['X'],
      Tweet               => ['X'],
      TranslatableError   => { 'X' => 'X' },
      SmsNotification     => ['X'],
      SlaTargetChange     => { 'X' => 'X' },
      Notification        => ['X'],
      FacebookComment     => { 'X' => 'X' },
      ExternalChangeEvent => { 'X' => 'X' }, # attr_accessible :value, :value_previous, :value_reference
      External            => { 'X' => 'X' },
      Comment             => '',
      ChannelBackEvent    => { source: {}, requester: {} }
    }.freeze

    class << self
      def scrub_ticket_and_associations(ticket)
        save_record = !ticket.archived?
        log_scrubbing_metrics(ticket) do
          Ticket.transaction do
            if ticket.account.has_prevent_deletion_if_churned?
              bypass_scrubbing_ticket_and_associations(ticket)
            else
              _scrub_ticket_and_associations(ticket, save_record)
            end

            ticket.record_deletion_audit unless ticket.deleted?

            updated_fields = {
              subject: SCRUBBED_SUBJECT,
              status_id: StatusType.DELETED
            }
            updated_fields[:description] = SCRUB_TEXT unless ticket.account.has_prevent_deletion_if_churned?

            if ticket.archived?
              ticket.status_id = StatusType.DELETED
              ticket.subject = SCRUBBED_SUBJECT
              ticket.save_to_archive(ticket.ticket_archive_stub) # archive first, then update subject
              # this is a special case: Hard deletion job looks up unscrubbed tickets by subject
              updated_fields[:generated_timestamp] = Time.now
              ticket.ticket_archive_stub.update_columns(updated_fields)
            else
              ticket.update_columns(updated_fields)
            end

            event = ticket.send :record_hard_deletion_audit
            if event && CIA.current_actor && CIA.current_actor.is_system_user?
              event.update_column :visible, false
            end

            scrub_ticket_deflection_enquiry(ticket)

            publish_domain_event!(ticket)
            publish_entity_update!(ticket)
          end
        end
      end

      def scrub_ticket_and_specific_association(ticket, association)
        save_record = !ticket.archived?
        Ticket.transaction do
          if ticket.account.has_prevent_deletion_if_churned?
            bypass_scrubbing_ticket_and_associations(ticket)
          else
            _scrub_ticket_association(ticket, association, save_record)
          end

          if ticket.archived?
            ticket.save_to_archive(ticket.ticket_archive_stub) # archive first, then update subject
          end
        end
      end

      private

      def scrub_ticket_deflection_enquiry(ticket)
        return unless ticket.ticket_deflection

        Rails.logger.info("Scrubbing Ticket Deflection ENQUIRY for ticket_id: #{ticket.id}")
        ticket.ticket_deflection.update(enquiry: SCRUBBED_SUBJECT)
      end

      def publish_domain_event!(ticket)
        ticket.will_be_saved_by(CIA.current_actor)
        ticket.add_domain_event(TicketPermanentlyDeletedProtobufEncoder.new(ticket).to_object)
        ticket.publish_ticket_events_to_bus!
      end

      def publish_entity_update!(ticket)
        return unless ticket.account.has_publish_ticket_on_scrub?

        publisher.publish(ticket, force_tombstone: true)
      end

      def publisher
        @publisher ||= ViewsObservers::EntityPublisher.new(
          ViewsObservers::TicketObserver::TOPIC,
          'views_entity_stream',
          ViewsEncoders::ViewsTicketProtobufEncoder,
          ViewsObservers::TicketChangedMatcher
        )
      end

      def log_scrubbing_metrics(ticket)
        Rails.logger.info("Scrubbing STARTED for ticket_id: #{ticket.id}, archived:#{ticket.archived?}, event_count:#{ticket.events.count}")
        ZendeskAPM.trace(
          'scrubber.execute',
          service: Zendesk::Scrub::APM_SERVICE_NAME
        ) do |span|
          span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
          span.set_tag("zendesk.account_id",        ticket.account_id)
          span.set_tag("zendesk.account_subdomain", ticket.account.subdomain)
          span.set_tag("zendesk.subpoena",          ticket.account.has_prevent_deletion_if_churned?)
          span.set_tag('scrubber.event_count',      ticket.events.count)
          span.set_tag('scrubber.archived',         ticket.archived?)

          yield
        end
        Rails.logger.info("Scrubbing COMPLETE for ticket_id: #{ticket.id}, archived:#{ticket.archived?}, event_count:#{ticket.events.count}")
      end

      def trace_scrubbing_metrics(_ticket, trace_name)
        ZendeskAPM.trace(
          trace_name,
          service: Zendesk::Scrub::APM_SERVICE_NAME
        ) { yield }
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: :ticket_scrubbing)
      end

      def stringy_column_names(klass)
        klass.columns.select { |c| [:text, :string].include? c.type }.map(&:name)
      end

      def _scrub_ticket_and_associations(ticket, save_record)
        soft_deletion_event = ticket.soft_deletion_event
        dependent_associations(ticket).each do |association|
          next unless records = ticket.send(association.name)

          # archived tickets are in riak and hence don't need to be preloaded
          records = preload_associations(association, records) unless ticket.archived?

          names = stringy_column_names association.klass
          Array(records).compact.each do |record|
            next if record == soft_deletion_event

            begin
              scrub_attributes(record, record.attributes.slice(*names), save_record: save_record, ticket: ticket)
            rescue ActiveRecord::SerializationTypeMismatch, Syck::TypeError => e
              Rails.logger.error "scrub failed with a #{e.class} on ticket id #{ticket.id} & record #{record.inspect}"
              raise unless Rails.env.production?
            rescue ArgumentError => e
              Rails.logger.error "scrub failed with ArgumentError on ticket id #{ticket.id} & record #{record.inspect}"
              # "invalid byte sequence in UTF-8" ArgumentErrors don't seem to like Rollbar, so we log them
              # to get more information, all other ArgumentErrors we do want to raise.
              raise unless e.message =~ /invalid byte sequence in UTF-8/
            end
          end
        end

        names = stringy_column_names ticket.class
        scrub_attributes(ticket, ticket.attributes.slice(*names), save_record: save_record, ticket: ticket)
        scrub_ignored_columns(ticket, save_record: save_record)
        remove_tags(ticket)
      end

      # This is code used for a backfill and is only intended to scrub a specific association within Riak
      def _scrub_ticket_association(ticket, association_to_scrub, save_record)
        soft_deletion_event = ticket.soft_deletion_event
        dependent_associations(ticket).each do |association|
          next unless records = ticket.send(association.name)

          # archived tickets are in riak and hence don't need to be preloaded
          records = preload_associations(association, records) unless ticket.archived?

          names = stringy_column_names association.klass
          Array(records).compact.each do |record|
            next if record.class != association_to_scrub
            next if record == soft_deletion_event

            begin
              scrub_attributes(record, record.attributes.slice(*names), save_record: save_record, ticket: ticket)
            rescue ActiveRecord::SerializationTypeMismatch, Syck::TypeError => e
              Rails.logger.error "scrub failed with a #{e.class} on ticket id #{ticket.id} & record #{record.inspect}"
              raise unless Rails.env.production?
            rescue ArgumentError => e
              Rails.logger.error "scrub failed with ArgumentError on ticket id #{ticket.id} & record #{record.inspect}"
              # "invalid byte sequence in UTF-8" ArgumentErrors don't seem to like Rollbar, so we log them
              # to get more information, all other ArgumentErrors we do want to raise.
              raise unless e.message =~ /invalid byte sequence in UTF-8/
            end
          end
        end
      end

      # for legal reasons don't scrub but hide that we keep the data from users
      #
      # Currently when a ticket's status_id == StatusType.DELETED && the subject is 'SCRUBBED'
      # the ticket is hidden from the UI
      #
      # TODO: change this to use a Flag for scrubbed tickets.
      def bypass_scrubbing_ticket_and_associations(ticket)
        ticket.subject = SCRUBBED_SUBJECT
        ticket.record_subject_change_audit
      end

      def remove_tags(ticket)
        ticket.taggings.delete_all
        ticket.archived? ? ticket.current_tags = '' : ticket.update_column(:current_tags, '')
      end

      def scrub_attributes(record, attrs, save_record:, ticket:)
        trace_scrubbing_metrics(ticket, "scrubber.execute.#{record.class}") do
          new_values = {}

          attrs.each do |k, v|
            next if k == "type" || k.end_with?("_type") || v.nil? || v == "" || (record.class == Ticket && EXCLUDE_FROM_SCRUB.include?(k))
            new_value = numeric_field?(record) ? 0 : scrub_value(record, k)
            new_values[k] = new_value
          end

          if save_record
            record.update_columns(new_values) unless new_values.empty?
          else
            new_values.each do |key, val|
              # should use send "#{k}=" ... only ticket description might make issues and can be 1-off
              record.send(:write_attribute, key, val)
            end
          end

          if record.class == CIA::Event
            record.update_column :visible, false
          end
        end
      end

      def scrub_value(record, attribute)
        value = SCRUB_TEXT

        if object_class = object_class(record, attribute)
          value = if DEFAULT_SCRUB_SERIALIZED_VALUES[record.class]
            DEFAULT_SCRUB_SERIALIZED_VALUES[record.class]
          elsif object_class == Hash || record[attribute].is_a?(Hash)
            Hash(value => value)
          else
            Array(value)
          end
        end
        value
      end

      def object_class(record, attribute)
        attr_type = record.type_for_attribute(attribute)
        attr_type.coder.try(:object_class) if attr_type.is_a?(ActiveRecord::Type::Serialized)
      end

      def scrub_ignored_columns(ticket, save_record:)
        IGNORED_COLUMNS.each do |column|
          next if column.blank?
          if save_record
            # need to update directly because the column is set as ignored and give all sort of errors otherwise
            # can't use update_column or update_attribute for example
            count = ticket.class.unscoped.where(account_id: ticket.account_id, id: ticket.id).
              update_all(column => SCRUB_TEXT)
            raise "too many updated rows #{count} for ticket scrubbing #{ticket.id}" if count > 1 # 0 is OK because the field value can be 'X', hence it is unchanged
          else
            ticket.attributes[column] = SCRUB_TEXT
          end
        end
      end

      def dependent_associations(ticket)
        ticket.class.reflect_on_all_associations.select do |a|
          [:destroy, :delete_all].include?(a.options[:dependent]) || SCRUB_ASSOCIATIONS.include?(a.table_name)
        end
      end

      # Used to preload any associations of the specified association
      # being called on the records specified.
      def preload_associations(association, records)
        case association.name
        when :ticket_field_entries
          records.includes(:ticket_field)
        else
          records
        end
      end

      def numeric_field?(record)
        record.class == TicketFieldEntry && NUMERIC_FIELDS.include?(record.ticket_field.class)
      end
    end
  end
end
