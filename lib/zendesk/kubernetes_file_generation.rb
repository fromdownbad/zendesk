module Zendesk
  class K8sRole
    def initialize(role_template)
      role_template.each do |key, value|
        self.class.send(:attr_reader, key)
        instance_variable_set(:"@#{key}", value)
      end
    end

    def get_binding # rubocop:disable Naming/AccessorMethodName
      binding
    end

    # Render any init containers we want to add to all the pod definitions.
    # In this case, we're adding a container that creates iptables rules that
    # prevents connections to the EC2 metadata service, which could be used by an
    # attacker in a SSRF attack.
    def render_init_containers(indent: '')
      yaml_block = <<~EO_INIT_CONTAINER.strip
        initContainers:
        - name: iptables-init
          image: gcr.io/docker-images-180022/apps/kube-debug-console@sha256:20ba8aa96b3b8d23faf108a95adfeb4d7fe43ea67e48df1cd2a28f8bc3ca6b6c
          command:
            - '/bin/sh'
            - '-c'
            - 'iptables -t filter -I OUTPUT -d 169.254.169.254 -j REJECT'
          securityContext:
            capabilities:
              add:
              - NET_ADMIN
          resources:
            requests:
              cpu: 100m
              memory: 50Mi
            limits:
              cpu: 100m
              memory: 50Mi
      EO_INIT_CONTAINER

      yaml_block.gsub("\n", "\n" + indent)
    end
  end

  class KubernetesFileGeneration
    ROOT = Bundler.root.join('kubernetes')
    CONFIG_ROOT = ROOT.join('config')

    class << self
      def run
        config.fetch(:roles).each do |role|
          out_file = ROOT.join("#{role.fetch(:role)}.yml")

          puts "Generating #{out_file}"

          File.open(out_file, 'w') { |f| f.puts output(role) }
        end
      end

      private

      def config
        YAML.load(File.read(CONFIG_ROOT.join('roles.yml'))).deep_symbolize_keys
      end

      def output(role)
        template = role.fetch(:template)
        template_file = File.read(CONFIG_ROOT.join('templates', "#{template}.yml.erb"))
        erb_template = ERB.new(template_file, nil, '-')

        erb_template.result(K8sRole.new(role).get_binding)
      end
    end
  end
end
