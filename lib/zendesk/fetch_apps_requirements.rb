module Zendesk
  module FetchAppsRequirements
    # For getting back information about what app installations
    # depend on which objects. Expected use case is for this module to be
    # extend model classes for e.g. targets, triggers, ticket fields.

    # returns a hash that
    # maps from model id to installation name of the requirements
    def apps_requirements(account)
      Zendesk::AppMarketClient.new(account.subdomain).requirements(self, account)
    end

    # returns a hash that
    # maps from model id to installation app_id of the requirements
    def app_id_requirements(account)
      Zendesk::AppMarketClient.new(account.subdomain).app_id_requirements(self, account)
    end
  end
end
