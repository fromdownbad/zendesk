require 'parallel'

class Zendesk::AccountReadiness
  attr_reader :account_id, :subdomain, :products_activated, :source

  STATE_COMPLETE = 'complete'.freeze
  STATE_PENDING = 'pending'.freeze

  SERVER_CHECK_CACHE_EXPIRY = 10.minutes
  READINESS_CHECK_CACHE_EXPIRY = 1.day
  MAX_THREAD_COUNT = 4

  SCHEME = 'http://'.freeze
  DOORMAN_PATH = '/doorman/v1/auth'.freeze

  def initialize(account_id:, subdomain:, products_activated: [], source:)
    @account_id = account_id
    @subdomain = subdomain
    @products_activated = products_activated
    @source = source
  end

  def readiness_check
    if Rails.cache.read(account_readiness_check_cache_key)
      STATE_COMPLETE
    else
      current_state = STATE_PENDING
      tags = []

      if !classic_account_ready?
        tags = ['cause:classic_account']
        Rails.logger.info("Account creation readiness check: Classic account not ready for #{idsub}")
      elsif !doorman_ready?
        tags = ['cause:doorman']
        Rails.logger.info("Account creation readiness check: Doorman not ready to authenticate requests for #{idsub}")
      elsif !account_ready?
        tags = ['cause:account']
        Rails.logger.info("Account creation readiness check: Account not ready for #{idsub}")
      elsif !products_ready?
        tags = ['cause:products']
        Rails.logger.info("Account creation readiness check: Products not ready for #{idsub}")
      else
        Rails.logger.info("Account creation readiness check: completed for #{idsub}")
        Rails.cache.write(account_readiness_check_cache_key, true, expires_in: READINESS_CHECK_CACHE_EXPIRY)
        current_state = STATE_COMPLETE
      end

      statsd_client.increment("account_setup.state.#{current_state}", tags: tags << "source:#{source}")
      current_state
    end
  end

  private

  def account_readiness_check_cache_key
    "#{account_id}-setup-controller-readiness-check"
  end

  def classic_account_ready?
    account = Account.find(account_id)
    return false unless account

    if account.is_active? && account.is_serviceable?
      true
    else
      account.clear_kasket_cache!
      account.reload && account.is_active? && account.is_serviceable?
    end
  end

  def account_service_servers
    Zendesk::Configuration.servers('account-service', [], pod_local: true)
  end

  def account_ready?
    account_status(account_service_servers.first)
  end

  def account_status(server)
    return true if Rails.cache.read(account_status_cache_key(server))

    conn = Kragle.new('account-service') do |faraday|
      faraday.url_prefix = "#{SCHEME}#{server}"
      faraday.options.timeout = 5
    end

    res = conn.get(account_path) do |req|
      req.headers['Host'] = "#{subdomain}.#{zendesk_host}"
    end

    account = res.body['account']
    if account['is_active']
      Rails.cache.write(account_status_cache_key(server), true, expires_in: SERVER_CHECK_CACHE_EXPIRY)
      true
    else
      Rails.logger.debug("#{idsub} - Account not ready on server #{server}: #{account}")
      false
    end
  rescue Kragle::ClientError, Kragle::ResponseError, Faraday::Error => e
    Rails.logger.warn("#{idsub} - Account service #{server} fetch account result: #{e}")
    return false
  end

  def account_path
    "/api/services/accounts/#{account_id}?disable_cache=#{disable_account_cache?}"
  end

  def disable_account_cache?
    @disable_account_cache ||= begin
      feature = Arturo::Feature.find_feature(:voltron_account_setup_disable_account_cache)
      feature.try(:phase) == 'on' || feature.try(:deployment_percentage) == 100
    end
  end

  def account_status_cache_key(server)
    "#{account_id}-setup-controller-account-status-#{server}"
  end

  def products_ready?
    products_activated.empty? || products_status(account_service_servers.first)
  end

  def products_status(server)
    return true if Rails.cache.read(products_status_cache_key(server))

    conn = Kragle.new('account-service') do |faraday|
      faraday.url_prefix = "#{SCHEME}#{server}"
      faraday.options.timeout = 5
    end

    res = conn.get(products_path) do |req|
      req.headers['Host'] = "#{subdomain}.#{zendesk_host}"
    end

    products = res.body['products'].map { |p| Zendesk::Accounts::Product.new(p) }
    active_products_set = products.select(&:active?).map(&:name).map(&:to_s).to_set
    products_activated_set = products_activated.to_set
    if products_activated_set.subset?(active_products_set)
      Rails.cache.write(products_status_cache_key(server), true, expires_in: SERVER_CHECK_CACHE_EXPIRY)
      true
    else
      Rails.logger.info("#{idsub} - Products activated #{products_activated_set} was not a subset of #{active_products_set} from server #{server}")
      false
    end
  rescue Kragle::ClientError, Kragle::ResponseError, Faraday::Error => e
    Rails.logger.warn("#{idsub} - Account service #{server} fetch products result: #{e}")
    return false
  end

  def products_path
    "/api/services/accounts/#{account_id}/products"
  end

  def products_status_cache_key(server)
    "#{account_id}-setup-controller-products-status-#{server}"
  end

  def doorman_ready?
    Parallel.map(doorman_servers, in_threads: MAX_THREAD_COUNT) { |server| doorman_status(server) }.all?
  end

  def doorman_secret
    @doorman_secret ||= ENV['ZENDESK_DOORMAN_SECRET'] || ENV['DOORMAN_SECRET'] || Zendesk::Configuration.fetch(:doorman_secret)
  end

  def doorman_servers
    Zendesk::Configuration.servers('auth-service', [], pod_local: true)
  end

  def doorman_auth(server)
    conn = Kragle.new('auth-service') do |faraday|
      faraday.url_prefix = "#{SCHEME}#{server}"
      faraday.options.timeout = 5
    end

    conn.get(DOORMAN_PATH) do |req|
      req.headers['Host'] = "#{subdomain}.#{zendesk_host}"
      req.headers['X-Zendesk-Original-URI'] = "#{SCHEME}#{subdomain}.#{zendesk_host}#{DOORMAN_PATH}"
    end
  end

  def doorman_status(server)
    return true if Rails.cache.read(doorman_status_cache_key(server))

    begin
      res = doorman_auth(server)
      status = res.headers['X-Zendesk-Doorman-Auth-Response']
      if status != '200'
        Rails.logger.debug("#{idsub} - Doorman status for server #{server}: #{status}")
        return false
      end
    rescue Kragle::ClientError, Kragle::ResponseError, Faraday::Error => e
      Rails.logger.warn("#{idsub} - Failed to call doorman server #{server}: #{e}")
      return false
    end

    begin
      decoded = JWT.decode(res.headers['X-Zendesk-Doorman'], doorman_secret).first.with_indifferent_access
      if decoded[:account].try(:[], :is_active) == true
        Rails.cache.write(doorman_status_cache_key(server), true, expires_in: SERVER_CHECK_CACHE_EXPIRY)
        return true
      else
        Rails.logger.debug("#{idsub} - Doorman header for server #{server}: #{decoded}")
        return false
      end
    rescue JWT::VerificationError, JWT::DecodeError, JWT::ExpiredSignature => e
      Rails.logger.debug("#{idsub} - Failed to decode doorman header for server #{server}: #{e}")
      return false
    end
  end

  def doorman_status_cache_key(server)
    "#{account_id}-setup-controller-doorman-status-#{server}"
  end

  def zendesk_host
    @zendesk_host ||= Zendesk::Configuration.fetch(:host)
  end

  def statsd_client
    Rails.application.config.statsd.client
  end

  def idsub
    "#{account_id}:#{subdomain}"
  end
end
