module Zendesk
  module SharedControllerSupport
    private

    def find_pinned_entries
      entries = current_account.entries.for_user(current_user).pinned.paginate(page: get_page, per_page: Entry::PINNED_PER_PAGE)
      entries.length # trigger preload or .last etc will cause queries
      entries
    end

    def respond_with_sql_search_result(result)
      page_size = request.format == :html ? API_DEFAULT_PER_PAGE : 100
      @result = result.paginate(per_page: page_size, page: get_page)

      respond_to do |format|
        format.html { render 'people/search/index' }
        format.xml  { render xml: @result.to_xml(user_serialization_options) }
        format.json { render json: @result.to_json(user_serialization_options), callback: params[:callback] }
      end
    end
  end
end
