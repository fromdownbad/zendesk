module Zendesk::SessionManagement
  include Zendesk::DoormanHeaderSupport

  def self.included(base)
    base.class_eval do
      before_action :check_doorman_account_info
      before_action :verify_authenticated_session, if: :authenticated_with_session?
    end
  end

  protected

  def check_doorman_account_info
    return true unless Arturo.feature_enabled_for?(:doorman_header_for_account_logging, current_account)
    return true if parsed_doorman_header.empty? # Skip unless Doorman decorated this request

    mismatches = mismatch_account(parsed_doorman_header)
    if !mismatches.empty?
      Rails.logger.append_attributes(doorman: parsed_doorman_header)
      Rails.logger.info("Doorman account info mismatched: #{mismatches}")
      statsd_client.increment('doorman_account_info.mismatch')
    else
      statsd_client.increment('doorman_account_info.match')
    end
  rescue StandardError => e
    Rails.logger.error "Doorman account info check errored: #{e.inspect}"
  end

  def mismatch_account(doorman_header)
    mismatch = []
    doorman_account_id = doorman_header.dig('account', 'id')
    if doorman_account_id
      accounts_match = doorman_account_id == current_account.id
      if accounts_match
        mismatch.concat mismatch_brand(doorman_header)
        mismatch.concat mismatch_route(doorman_header)
        mismatch.concat mismatch_shard(doorman_header)
        ['is_active', 'subdomain', 'multiproduct', 'lock_state'].each do |attribute_name|
          if doorman_header.dig('account', attribute_name).to_s != current_account.send(attribute_name).to_s
            mismatch << "#{attribute_name} attribute mismatch for Doorman #{doorman_header[attribute_name]} vs Classic #{current_account.send(attribute_name)}"
          end
        end
        unless mismatch.empty?
          statsd_client.increment('account.attributes.mismatch')
        end
      else
        statsd_client.increment('account.mismatch')
        mismatch << "AccountID mismatch in Doorman #{doorman_account_id} vs Classic #{current_account.id}"
      end
    else
      statsd_client.increment('account.missing_account')
      mismatch << "Missing Doorman accountID for account #{current_account.id}"
    end
    mismatch
  end

  def mismatch_brand(doorman_header)
    mismatches = []
    doorman_brand_id = doorman_header.dig('account', 'brand_id')
    if doorman_brand_id && doorman_brand_id == current_brand.id
      statsd_client.increment('brand.match.true')
    elsif current_brand.nil? && doorman_brand_id.nil?
      statsd_client.increment('brand.match.true')
      statsd_client.increment('brand.match.empty')
    else
      statsd_client.increment('brand.match.false')
      if current_brand
        brand_time_created = (Time.now - current_brand.created_at).floor
        brand_time_updated = (Time.now - current_brand.updated_at).floor
        mismatch = "Brand mismatch in Doorman #{doorman_brand_id} vs Classic brand: #{current_brand.id}, age_in_seconds: #{brand_time_created},
 changed_seconds_ago: #{brand_time_updated}"
        if current_brand&.deleted_at
          brand_time_deleted = (Time.now - current_brand.deleted_at).floor
          mismatch << " deleted_seconds_ago #{brand_time_deleted}"
        end
        mismatches << mismatch
      end
    end
    mismatches
  end

  def mismatch_route(doorman_header)
    mismatches = []
    doorman_route_id = doorman_header.dig('account', 'route_id')
    if doorman_route_id && (doorman_route_id.to_s == current_route.id.to_s)
      statsd_client.increment('route.match.true')
    else
      statsd_client.increment('route.match.false')
      mismatches << "Route mismatch in Doorman #{doorman_route_id} vs Classic #{current_route.id}"
    end
    mismatches
  end

  def mismatch_shard(doorman_header)
    mismatches = []
    doorman_shard_id = doorman_header.dig('account', 'shard_id')
    if doorman_shard_id && doorman_shard_id == current_account.shard_id
      statsd_client.increment('shard.match.true')
    else
      statsd_client.increment('shard.match.false')
      mismatches << "Shard mismatch in Doorman shard #{doorman_shard_id} vs Classic #{current_account.shard_id} "
    end
    mismatches
  end

  def verify_authenticated_session
    revoke_session                          if session_invalid?
    revoke_device                           if device_manager.invalid_device?
    restrict_to_change_password             if authentication.password_expired? && !['update', 'create'].include?(params[:action])

    if logged_in?
      authentication.keepalive if renew_session?
      set_expires_at_in_headers
    end
  end

  module Expiration
    protected

    def authenticated_with_session?
      logged_in? && authentication.stored?
    end

    def set_expires_at_in_headers
      response.headers['X-Zendesk-User-Session-Expires-At'] = authentication.timeout.to_i unless authentication.stale?
    end

    def renew_session?
      if request.headers['HTTP_X_ZENDESK_RENEW_SESSION'] == 'true'
        statsd_client.increment('renew_session', tag: "renew_header")
        logger.info("[SESSION RENEW] renew_header")
        true
      end
    end

    def lotus_request?
      request.headers["X-Zendesk-Lotus-Version"].present?
    end

    def revoke_session(_options = {})
      logger.info('Revoking session')
      cancel_session
      clear_current_user
      @current_registered_user = nil
    end

    def revoke_device
      logger.info("Device token #{device_manager.token} matches no known device")
      revoke_session
      device_manager.revoke_device
      permanent_cookies.delete(Zendesk::Auth::Otp::Tracking::COOKIE_VALIDATION_KEY)
      access_denied
    end

    def session_invalid?
      return false if current_user.is_system_user?

      stale_session? || user_suspended?
    end

    def user_suspended?
      if current_user.suspended?
        logger.info('Suspended session: user is supended')
        true
      end
    end

    # Expired due to inactivity / timeout controlled by security policy
    # When the password expires due to the security policy,
    # the user still needs access to change their password.
    def stale_session?
      if authentication.stale?
        logger.info("Stale session: last updated #{authentication.updated_at}")
        true
      end
    end

    def restrict_to_change_password
      locale = TranslationLocale.find(current_user.locale_id || 1)
      flash[:error] = I18n.t('txt.access.login.password_expired', locale: locale)
      redirect_to('/password')
    end

    def cancel_session(_options = {})
      authentication.destroy
      clear_authentication_cache

      warden.logout
      expires_now

      # Auth mechanisms in zendesk_auth rely on the session_id being
      # up-to-date. When reset_session is called, it clears out
      # the session completely and it is not recreated until the
      # ActionDispatch::Session::CookieStore middleware is called
      # The length is defined in #generate_sid in
      # actionpack-3.2.18/lib/action_dispatch/middleware/session/abstract_store.rb
      reset_session
      session[:session_id] = SecureRandom.hex(16)
    end
  end
  include Expiration

  def clear_session_and_logout(options = {})
    cancel_session
    mark_unavailable_for_voice if options.fetch(:mark_unavailable_for_voice, true) && current_user_exists?

    flash[:notice] = nil
    flash[:error]  = nil
  end

  def log_session_info
    log_message = "  Session: #{shared_session[:id]}"
    log_message << " Account: #{current_account.id}" if current_account
    log_message <<
      if current_account && current_user
        " User: #{current_user.id || 'nil'}"
      else
        ' User: nil'
      end
    logger.info(log_message)
  end

  def mark_unavailable_for_voice
    condition = -> (_) do
      logger.debug('Retry mark_unavailable_for_voice due to NetworkError')
      true
    end

    Zendesk::Retrier.retry_on_error([ZendeskAPI::Error::NetworkError], 4, retry_if: condition) do
      return unless current_account.has_voice_enabled?
      return unless current_user.is_voice_agent?
      api_client.connection.post("/api/v2/channels/voice/availabilities/#{current_user.id}/logout.json")
    end
  rescue ZendeskAPI::Error::RecordNotFound
    ZendeskExceptions::Logger.record($!, location: self, message: "Trying to logout user for Voice", fingerprint: 'ff1f7ef7578e95ff95851ce4ba65e05dc8484e11')
  rescue ZendeskAPI::Error::NetworkError
    ZendeskExceptions::Logger.record($!, location: self, message: "Impossible to mark agent unavailable for Voice due to Voice Error", fingerprint: 'f3b7fc5e979746b6f2367817f7bfe86f85a3ee4b')
  end

  def api_client
    @api_client ||= Zendesk::InternalApi::Client.new(current_account.subdomain, internal_user_id: current_user.id)
  end

  def statsd_client
    @session_management_statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'session_management')
  end
end
