# encoding: ASCII-8BIT
require 'xsd/qname'

module MsDynamics; module Integration


# {http://173.201.38.202:8000/}SearchUserProfileByEmailAddress
#   inputXML - SOAP::SOAPString
class SearchUserProfileByEmailAddress
  attr_accessor :inputXML

  def initialize(inputXML = nil)
    @inputXML = inputXML
  end
end

# {http://173.201.38.202:8000/}SearchUserProfileByEmailAddressResponse
#   searchUserProfileByEmailAddressResult - SOAP::SOAPString
class SearchUserProfileByEmailAddressResponse
  attr_accessor :searchUserProfileByEmailAddressResult

  def initialize(searchUserProfileByEmailAddressResult = nil)
    @searchUserProfileByEmailAddressResult = searchUserProfileByEmailAddressResult
  end
end

# {http://173.201.38.202:8000/}EscalateZendeskTicketAsCrmCase
#   inputXML - SOAP::SOAPString
class EscalateZendeskTicketAsCrmCase
  attr_accessor :inputXML

  def initialize(inputXML = nil)
    @inputXML = inputXML
  end
end

# {http://173.201.38.202:8000/}EscalateZendeskTicketAsCrmCaseResponse
#   escalateZendeskTicketAsCrmCaseResult - SOAP::SOAPString
class EscalateZendeskTicketAsCrmCaseResponse
  attr_accessor :escalateZendeskTicketAsCrmCaseResult

  def initialize(escalateZendeskTicketAsCrmCaseResult = nil)
    @escalateZendeskTicketAsCrmCaseResult = escalateZendeskTicketAsCrmCaseResult
  end
end

# {http://173.201.38.202:8000/}ValidateUserToGetGUIDandSR
#   inputXML - SOAP::SOAPString
class ValidateUserToGetGUIDandSR
  attr_accessor :inputXML

  def initialize(inputXML = nil)
    @inputXML = inputXML
  end
end

# {http://173.201.38.202:8000/}ValidateUserToGetGUIDandSRResponse
#   validateUserToGetGUIDandSRResult - SOAP::SOAPString
class ValidateUserToGetGUIDandSRResponse
  attr_accessor :validateUserToGetGUIDandSRResult

  def initialize(validateUserToGetGUIDandSRResult = nil)
    @validateUserToGetGUIDandSRResult = validateUserToGetGUIDandSRResult
  end
end


end; end
