# encoding: ASCII-8BIT
require 'zendesk/ms_dynamics/integration/generated/ms_dynamics.rb'
require 'zendesk/ms_dynamics/integration/generated/ms_dynamics_mapping_registry.rb'
require 'soap/rpc/driver'

module MsDynamics::Integration

class MSCRMAdapterSoap < ::Zendesk::SoapBase
  DefaultEndpointUrl = "http://zendeskdynamics.cloudapp.net/MSCRMAdapter.asmx"

  Methods = [
    [ "http://173.201.38.202:8000/SearchUserProfileByEmailAddress",
      "searchUserProfileByEmailAddress",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://173.201.38.202:8000/", "SearchUserProfileByEmailAddress"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://173.201.38.202:8000/", "SearchUserProfileByEmailAddressResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://173.201.38.202:8000/EscalateZendeskTicketAsCrmCase",
      "escalateZendeskTicketAsCrmCase",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://173.201.38.202:8000/", "EscalateZendeskTicketAsCrmCase"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://173.201.38.202:8000/", "EscalateZendeskTicketAsCrmCaseResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://173.201.38.202:8000/ValidateUserToGetGUIDandSR",
      "validateUserToGetGUIDandSR",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://173.201.38.202:8000/", "ValidateUserToGetGUIDandSR"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://173.201.38.202:8000/", "ValidateUserToGetGUIDandSRResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ]
  ]
end

end
