# encoding: ASCII-8BIT
require 'zendesk/ms_dynamics/integration/generated/ms_dynamics.rb'
require 'soap/mapping'

module MsDynamics; module Integration

module DefaultMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new
  NsC_8000 = "http://173.201.38.202:8000/"

  def self.generate_params(type)
    { :class => "MsDynamics::Integration::#{type}".constantize,
      :schema_name => XSD::QName.new(NsC_8000, type) }
  end

  def self.schema_element_soap
    { :schema_element => [
        ["inputXML", "SOAP::SOAPString", [0, 1]]
      ] }
  end

  def self.schema_element_soap_xsd(name)
    { :schema_element => [
      ["#{name}Result", ["SOAP::SOAPString", XSD::QName.new(NsC_8000, "#{name}Result")], [0, 1]]
      ] }
  end

  LiteralRegistry.register(generate_params("SearchUserProfileByEmailAddress").merge(schema_element_soap))

  LiteralRegistry.register(generate_params("SearchUserProfileByEmailAddressResponse").merge(schema_element_soap_xsd("SearchUserProfileByEmailAddressResult")))

  LiteralRegistry.register(generate_params("EscalateZendeskTicketAsCrmCase").merge(schema_element_soap))

  LiteralRegistry.register(generate_params("EscalateZendeskTicketAsCrmCaseResponse").merge(schema_element_soap_xsd("escalateZendeskTicketAsCrmCaseResult")))

  LiteralRegistry.register(generate_params("ValidateUserToGetGUIDandSR").merge(schema_element_soap))

  LiteralRegistry.register(generate_params("ValidateUserToGetGUIDandSRResponse").merge(schema_element_soap_xsd("validateUserToGetGUIDandSRResult")))

end

end; end
