require 'zendesk/search/elasticsearch/search'

module Zendesk
  module Search
    Results.send(:include, Zendesk::Serialization::Search::ResultsSerialization)

    def self.search(user, user_query, options = {})
      options[:locale] = user.translation_locale.locale
      Zendesk::Search::Elasticsearch::Search.search(user, user_query, options)
    end
  end
end
