require 'zendesk/users/user_manager'

module Zendesk
  # These methods cannot be overwritten by new methods pasted into console
  module ConsoleHelpers
    EML_PATH_REGEX = /(?<account_id>\d+)\/\S+-\S+-\S+-\S+-\S+\.(eml|json)/.freeze
    S3_EMAIL_DOWNLOAD_DIRECTORY = 'inbound-mail'.freeze

    class << self
      attr_accessor :queries
    end

    def on_all_shards(&block)
      ActiveRecord::Base.on_all_shards(&block)
    end

    def pod_local_shards
      Zendesk::Configuration::PodConfig.local_shards
    end

    def on_pod_local_shards(&_block)
      pod_local_shards.each do |shard|
        ActiveRecord::Base.on_shard(shard) do
          yield(shard)
        end
      end
    end

    def overwrite_password(user, password)
      auth = Zendesk::AuthenticatedSession.new({}).tap do |a|
        a.current_user = user
        a.current_account = user.account
      end
      user_manager = Zendesk::UserManager.new(auth)
      user_manager.reset_password_for(user, to: password)
    end

    # make it possible to do requests like this:
    # app.get("https://support.zendesk-acceptance.com/api/v2/XXXX")
    def login_as_user(account, user)
      account = (account.is_a?(String) ? account.to_account : account)

      user.account = account if user.is_system_user?

      ApplicationController.class_eval do
        define_method(:current_account) do
          account
        end

        def verify_authenticity_token
        end

        def authenticate_user
        end

        define_method(:current_registered_user) { user }

        def check_subsystem_authorization
        end
      end

      Api::BaseController.class_eval do
        define_method(:current_account) { account }

        def verify_authenticity_token
        end

        def authenticate_user
        end

        def check_subsystem_authorization
        end

        define_method(:current_registered_user) { user }
      end
    end

    def login_as_admin(account)
      account = (account.is_a?(String) ? account.to_account : account)
      login_as_user(account, account.admins.first)
    end

    def app_get(url, user = nil)
      log_to
      account = Account.lookup_by_url(url)
      return nil unless account
      user ? login_as_user(account, user) : login_as_admin(account)
      app.get(url)
    end

    def log_user(event)
      # Consoles in production/staging will route through the same Data Sunrise
      # regardless, but `on_slave` unblocks debugging via tainting the master
      # configuration.
      ActiveRecord::Base.on_slave do
        args = [["SELECT ?, ?", "User ID #{event}", `whoami`.chomp]]
        args << "" if RAILS4

        sql = ActiveRecord::Base.send(:sanitize_sql, *args)
        ActiveRecord::Base.connection.execute(sql)
      end
    end
    module_function :log_user

    def log_to(stream = $stdout)
      if stream
        ActiveRecord::Base.logger.subscribe(stream)
        true
      else
        ActiveRecord::Base.logger.subscribers.slice!(0)
        false
      end
    end

    # count queries that are done
    # [[sql, time], [sql, time]]
    def query_counter
      unless Zendesk::ConsoleHelpers.queries
        Zendesk::ConsoleHelpers.queries = []
        ActiveRecord::ConnectionAdapters::AbstractAdapter.class_eval do
          def log_with_query_diet(query, *args, &block)
            result = nil
            time = Benchmark.realtime do
              result = log_without_query_diet(query, *args, &block)
            end
            Zendesk::ConsoleHelpers.queries << [query, time]
            result
          end
          alias_method :log_without_query_diet, :log
          alias_method :log, :log_with_query_diet
        end
      end

      Zendesk::ConsoleHelpers.queries
    end

    def count_objects
      old = Hash.new(0)
      cur = Hash.new(0)
      ObjectSpace.each_object { |o| old[o.class] += 1 }
      yield
      ObjectSpace.each_object { |o| cur[o.class] += 1 }
      Hash[cur.map { |k, v| [k, v - old[k]] }].reject { |_k, v| v == 0 }
    end

    def log_http
      ::Net::HTTP.class_eval do
        def initialize_with_logger(*args)
          initialize_without_logger(*args)
          set_debug_output($stderr)
        end

        alias_method :initialize_without_logger, :initialize
        alias_method :initialize, :initialize_with_logger
      end
    end

    def log_sql_caller
      ActiveRecord::ConnectionAdapters::Mysql2Adapter.class_eval do
        def execute_with_sql_caller(query, name = nil)
          puts "#{"-" * 40}\n#{query}\n#{caller.join("\n")}\n#{"-" * 40}"
          execute_without_sql_caller(query, name)
        end
        alias_method :execute_without_sql_caller, :execute
        alias_method :execute, :execute_with_sql_caller
      end
    end

    def app_version
      [GIT_HEAD_TAG, GIT_HEAD_SHA.to_s[0...7]].join(' : ')
    end
    module_function :app_version

    def search_usage(base_account, text)
      found = []
      ([base_account] + base_account.spoke_accounts).each do |account|
        account.posts.each do |p|
          content = p.body.to_s
          next unless content.include?(text)
          found << content
          puts "Found in account:#{account.id} post:#{p.id} in Post.find(#{p.id}).content"
        end
      end
      found
    end

    def eml_path(account_id, message_id, start_date = DateTime.now, end_date = nil)
      start_datetime = DateTime.parse(start_date.to_s)
      end_datetime = end_date.present? ? DateTime.parse(end_date.to_s) : start_datetime
      account = Account.find(account_id)
      account.shard
      ticket = account.tickets.
        includes(:events).
        where(
          events: {
            type: 'Audit'
          },
          tickets: {
            via_id: [ViaType.MAIL, ViaType.WEB_FORM],
            created_at: (start_datetime - 1.day)..(end_datetime + 1.day)
          }
        ).
        where('events.value_previous LIKE ?', "%#{message_id}%").
        first

      return nil unless ticket.present?

      comment = ticket.comments.find { |c| c.audit.value_previous.include?(message_id) }

      JSON.parse(comment.audit.value_previous)['system']['raw_email_identifier']
    rescue ActiveRecord::AdapterNotSpecified
      raise ArgumentError, 'Account data cannot be found on this pod'
    end

    def email_json(eml_path)
      Zendesk::Mailer::RawEmailAccess.new(eml_path).json
    end

    def email_eml(eml_path)
      Zendesk::Mailer::RawEmailAccess.new(eml_path).eml
    end

    # `eml_path` takes on the following format:
    # 1019579/b6242d5c-5cfd-4b56-bc38-2083fef572ef.eml
    # 1019579/b6242d5c-5cfd-4b56-bc38-2083fef572ef.json
    def s3_email_download_url(eml_path)
      match = EML_PATH_REGEX.match(eml_path)

      raise "eml_path is invalid" if match.nil?

      validate_pod_access(match[:account_id])
      raw_json_string = email_json(eml_path)
      eml_url = eml_url_from_json_string(raw_json_string, eml_path)
      retrieve_email_object_from_s3(eml_url, eml_path)
    end

    def process_email(message, account = nil)
      require 'zendesk/inbound_mail'

      Zendesk::Mail.logger = Logger.new($stdout)

      # load json
      message = message.audit if message.is_a?(Comment)
      message = message.raw_email.json if message.is_a?(Audit)
      message = message.force_encoding(Encoding::UTF_8).scrub
      json = JSON.load(message).with_indifferent_access

      # convert to mail
      mail = Zendesk::Mail::Serializers::InboundMessageSerializer.load(json)
      mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id

      # process
      Zendesk::InboundMail::TicketProcessingJob.work(
        mail: mail,
        account_id: account.try(:id) || json.fetch(:account_id),
        recipient: account.try(:reply_address) || json.fetch(:recipient),
        route_id: account.try(:route_id) || json.fetch(:route_id)
      )
    end

    def create_account(subdomain, owner_email)
      app.post("https://#{subdomain}.zendesk.com/api/v2/accounts.json",
        account: {
          name: "Z3N #{subdomain}",
          subdomain: subdomain,
          help_desk_size: "Small Team"
        },
        owner: {
          name: owner_email.split("@").first,
          email: owner_email
        },
        address: {
          phone: "+1-123-456-7890"
        })
    end

    # produce a flamegraph and print instructions to opening it
    # dumps into a static location so users can refresh in their browsers to update
    # default `:interval` sample every interval microseconds (default: 1000), use 100 or 10 for fast things
    def flamegraph(name: 'test', **options, &block)
      options[:raw] = true
      options[:mode] ||= :wall

      unless Rails.application.config.cache_classes
        raise "Enable config.cache_classes in config/environments/#{Rails.env}.rb"
      end

      file = "#{name}.js"
      time_taken = nil
      GC.disable
      old_log = Rails.logger.level
      Rails.logger.level = 1 # logs are not free, so simulate production :info
      dump = ActiveSupport::Deprecation.silence do
        StackProf.run(options) do
          time_taken = Benchmark.realtime(&block)
        end
      end
      Rails.logger.level = old_log
      GC.enable
      report = StackProf::Report.new(dump)
      File.open(file, 'w+') { |f| report.print_flamegraph(f, true) }
      spec = Gem::Specification.find_by_name("stackprof")
      puts "open in your browser: file://#{spec.gem_dir}/lib/stackprof/flamegraph/viewer.html?data=#{File.expand_path(file)}"
      time_taken
    end

    def clear_all_cache(confirmation = false)
      if Rails.env.production?
        unless confirmation
          puts 'Caches not cleared — please confirm action'
          return
        end
      end

      puts 'Clearing Rails.cache, Kasket.cache, Prop.cache'

      Kasket.cache.clear if Kasket.cache != Rails.cache
      Rails.cache.clear if Prop.cache != Rails.cache
      Prop.cache.clear
    end

    def pretty_print_rule(rule)
      puts "#{rule.title} - (#{rule.type}) #{rule.id}"
      puts "\tConditions:"
      %w[any all].each do |a|
        puts "\t\t#{a}:"
        rule.definition.send("conditions_#{a}").each do |cd|
          puts "\t\t - When %s %s %s" % [cd.source, cd.operator, cd.value]
        end
      end

      puts "\tActions:"
      rule.definition.actions.each do |a|
        puts "\t\t - Will set #{a.source} to #{a.value}"
      end
      nil
    end

    if defined?(Rails::Console) && !Rails.env.development?
      # Print DD trace link after app.get
      ActionDispatch::Integration::Session.prepend(Module.new do
        def process(*args)
          trace_id = nil
          method, url, = args
          Datadog.tracer.trace('console request', service: 'rails console', resource: "#{method.upcase} #{url}") do |span|
            span.context.sampling_priority = Datadog::Ext::Priority::USER_KEEP
            trace_id = span.trace_id

            super
          end
        ensure
          Rails.logger.info "Datadog Trace link: https://zendesk.datadoghq.com/apm/trace/#{trace_id}"
        end
      end)
    end

    private

    def base_url(store)
      return unless store.is_a?(RemoteFiles::FogStore)

      connection = store.connection
      scheme = connection.instance_variable_get(:@scheme)
      host = connection.instance_variable_get(:@host)
      bucket = store.directory.key

      "#{scheme}://#{host}/#{bucket}"
    end

    def current_pod
      @current_pod ||= Zendesk::Configuration.fetch(:pod_id)
    end

    def validate_pod_access(account_id)
      account = Account.find(account_id)

      raise "Account cannot be found" if account.nil?
      raise "Use this console helper on pod #{account.pod_id}" if account.pod_id != current_pod
    end

    def get_object_https_url(store, object_name, expires = DateTime.now + 1.week)
      store.connection.get_object_https_url(store.directory.key, object_name, expires)
    end

    def eml_url_from_json_string(json_string, eml_path)
      raise "Email json not found" if json_string.nil?

      json = Yajl::Parser.parse(json_string, check_utf8: false).with_indifferent_access
      # `eml_url` takes on the following format:
      # "https://s3.amazonaws.com/zendesk-use1-inbound-mail-production/2278207%2F27b80e83-012f-4888-9595-e45a0be994f0.eml"
      eml_url = json['eml_url']

      return eml_url unless eml_path.include?('.json')

      eml_url.gsub('.eml', '.json')
    end

    def retrieve_email_object_from_s3(eml_url, eml_path)
      puts "Retrieving #{eml_url}"

      RemoteFiles::CONFIGURATIONS.values.find do |config|
        next unless config.configured?

        config.stores.find do |store|
          next unless store.options[:directory].include?(S3_EMAIL_DOWNLOAD_DIRECTORY)

          begin
            base = base_url(store)

            next unless base.present?
            next unless eml_url.include?(base)
            next unless store.file_from_url(eml_url)

            return get_object_https_url(store, eml_path)
          rescue StandardError => error
            Rails.logger.error "#{error.class}: #{error.message}: #{error.backtrace}"
          end
        end
      end
    end
  end
end
