module Zendesk::TicketMetric
  class Periods < SimpleDelegator
    def initialize(ticket:, metric_events:, business_hours:)
      @ticket         = ticket
      @metric_events  = metric_events
      @business_hours = business_hours

      super(periods)
    end

    def calculation
      @calculation ||= Calculation.new(self)
    end

    protected

    attr_reader :ticket,
      :metric_events,
      :business_hours

    private

    def periods
      business_hours ? business_periods : calendar_periods
    end

    def calendar_periods
      @calendar_periods ||= begin
        transition_periods.select(&:active?).map(&:to_time_segment)
      end
    end

    def business_periods
      @business_periods ||= begin
        return [] if calendar_periods.none?

        return calendar_periods unless ticket.schedule.present?

        Enumerator.new do |yielder|
          ticket.
            schedule.
            periods.
            after(calendar_periods.first.start_time).
            timeline.
            until(calendar_periods.last.end_time).
            each do |active_period|
              calendar_periods.each do |calendar_period|
                business_period = active_period & calendar_period

                yielder << business_period unless business_period.disjoint?
              end
            end
        end
      end
    end

    def transition_periods
      @transition_periods ||= Transition.new(ticket, metric_events).periods
    end
  end
end
