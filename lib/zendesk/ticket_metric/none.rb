module Zendesk::TicketMetric
  class None
    def initialize(sla_metric)
      @sla_metric = sla_metric
    end

    def equivalent?(policy_metric)
      ::Sla::PolicyMetric::EQUIVALENCY_ATTRIBUTES.all? do |attribute|
        public_send(attribute) == policy_metric.public_send(attribute)
      end
    end

    def present?
      false
    end

    def metric_id
      sla_metric.id
    end

    def target
      nil
    end

    def business_hours
      nil
    end

    def to_audit
      nil
    end

    def policy
      nil
    end

    def sla_policy_id
      nil
    end

    protected

    attr_reader :sla_metric
  end
end
