module Zendesk::TicketMetric
  class Calculation
    def initialize(periods)
      @periods = periods
    end

    def elapsed(to: Time.now)
      timeline.
        until(to).
        map(&:duration).
        reduce(Biz::Duration.new(0), :+).
        in_minutes
    end

    def time(after:)
      timeline.
        for(Biz::Duration.minutes(after)).
        reject(&:empty?).
        last.
        try(:end_time) || periods.first.try(:start_time)
    end

    protected

    attr_reader :periods

    private

    def timeline
      @timeline ||= Biz::Timeline::Forward.new(periods)
    end
  end
end
