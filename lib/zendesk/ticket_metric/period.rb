module Zendesk::TicketMetric
  class Period
    include Equalizer.new(:start_time, :end_time, :state)

    def initialize(start_time, end_time, state)
      @start_time = start_time
      @end_time   = end_time
      @state      = state
    end

    def active?
      state == :activated
    end

    def to_time_segment
      Biz::TimeSegment.new(start_time, end_time)
    end

    protected

    attr_reader :start_time,
      :end_time,
      :state
  end
end
