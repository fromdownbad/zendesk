module Zendesk::TicketMetric
  module StateMachine
    class ReplyTime < Abstract
      TRANSITIONS = proc do
        event :advance, deactivated: :deactivated, if: -> {
          ticket_via_side_conversation =
            ticket.account.has_sla_side_conversation_reply_time? &&
              ticket.via_id == ViaType.SIDE_CONVERSATION

          context.new_ticket? &&
            ticket.comment_added? &&
            ticket.comment.public_reply? &&
            !ticket_via_side_conversation
        }

        event :advance, deactivated: :resumed, if: -> {
          ticket.comment_added? && ticket.working? && (
            ticket.comment.from_end_user? ||
              (
                ticket.account.has_sla_side_conversation_reply_time? &&
                  ticket.via_id == ViaType.SIDE_CONVERSATION
              ) || (
                context.new_ticket? &&
                  ticket.comment.is_private? &&
                  ticket.comment.author.is_light_agent?
              )
          )
        }

        event :advance, activated: :deactivated, if: -> {
          should_deactivate = context.status_changed? && ticket.finished? ||
            ticket.comment_added? && ticket.comment.public_reply?
          ticket_via_side_conversation =
            ticket.account.has_sla_side_conversation_reply_time? &&
              ticket.via_id == ViaType.SIDE_CONVERSATION

          return should_deactivate unless ticket_via_side_conversation

          should_deactivate && ticket.requester_id != ticket.comment&.author_id
        }
      end
    end
  end
end
