module Zendesk::TicketMetric
  module StateMachine
    class AgentWorkTime < Abstract
      TRANSITIONS = proc do
        event :advance, %i[deactivated paused] => :resumed, if: -> {
          context.status_changed? &&
            ticket.working? &&
            !ticket.pending? &&
            !ticket.on_hold?
        }

        event :advance, %i[activated paused] => :deactivated, if: -> {
          context.status_changed? && ticket.finished?
        }

        event :advance, %i[activated deactivated] => :paused, if: -> {
          context.status_changed? && (ticket.pending? || ticket.on_hold?)
        }
      end
    end
  end
end
