module Zendesk::TicketMetric
  module StateMachine
    class PausableUpdateTime < Abstract
      TRANSITIONS = proc do
        event :advance, %i[activated deactivated paused] => :initiated, if: -> {
          ticket.working? &&
            !ticket.pending? &&
            ticket.comment_added? &&
            ticket.comment.public_reply?
        }

        event :advance, activated: :paused, if: -> {
          context.status_changed? && ticket.pending?
        }

        event :advance, %i[activated paused] => :deactivated, if: -> {
          context.status_changed? && ticket.finished?
        }

        event :advance, paused: :resumed, if: -> {
          ticket.working? && !ticket.pending? && (
            context.status_changed? && !ticket.comment_added? ||
              ticket.comment_added? && !ticket.comment.public_reply?
          )
        }
      end
    end
  end
end
