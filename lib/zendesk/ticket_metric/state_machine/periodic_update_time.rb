module Zendesk::TicketMetric
  module StateMachine
    class PeriodicUpdateTime < Abstract
      TRANSITIONS = proc do
        event :advance, %i[activated deactivated] => :initiated, if: -> {
          ticket.working? &&
            ticket.comment_added? &&
            ticket.comment.public_reply?
        }

        event :advance, activated: :deactivated, if: -> {
          context.status_changed? && ticket.finished?
        }
      end
    end
  end
end
