module Zendesk::TicketMetric
  module StateMachine
    class ResolutionTime < Abstract
      TRANSITIONS = proc do
        event :advance, deactivated: :resumed, if: -> {
          context.status_changed? && ticket.working?
        }

        event :advance, activated: :deactivated, if: -> {
          context.status_changed? && ticket.finished?
        }
      end
    end
  end
end
