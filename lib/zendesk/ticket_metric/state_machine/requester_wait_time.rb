module Zendesk::TicketMetric
  module StateMachine
    class RequesterWaitTime < Abstract
      TRANSITIONS = proc do
        event :advance, %i[deactivated paused] => :resumed, if: -> {
          context.status_changed? && ticket.working? && !ticket.pending?
        }

        event :advance, %i[activated paused] => :deactivated, if: -> {
          context.status_changed? && ticket.finished?
        }

        event :advance, %i[activated deactivated] => :paused, if: -> {
          context.status_changed? && ticket.pending?
        }
      end
    end
  end
end
