module Zendesk::TicketMetric
  module StateMachine
    class Abstract
      def self.inherited(subclass)
        super

        subclass.class_eval do
          class << self
            def build(ticket)
              StateMachine.build(ticket, metric, &transitions)
            end

            private

            def metric
              name.demodulize.underscore.to_sym
            end

            def transitions
              const_get(:TRANSITIONS)
            end
          end
        end
      end
    end
  end
end
