module Zendesk::TicketMetric
  class Transition
    def initialize(ticket, metric_events)
      @ticket        = ticket
      @metric_events = metric_events
    end

    def last
      @last ||= metric_events.transition.to_a.last || None.new(ticket)
    end

    def periods
      [
        *metric_events.transition.each_cons(2).map do |start_event, end_event|
          Period.new(start_event.time, end_event.time, start_event.to_state)
        end,
        Period.new(last.time, Biz::Time.heat_death, last.to_state)
      ]
    end

    protected

    attr_reader :ticket,
      :metric_events

    class None
      def initialize(ticket)
        @ticket = ticket
      end

      def time
        ticket.created_at || Time.now
      end

      def to_state
        :deactivated
      end

      def to_state?(state)
        to_state == state
      end

      protected

      attr_reader :ticket
    end
  end
end
