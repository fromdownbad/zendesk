module Zendesk
  module TicketMetric
    module StateMachine
      ACTIVATED_STATES = %i[activated initiated resumed].freeze

      def self.all
        @all ||= Zendesk::TicketMetric.supported.map do |metric|
          "#{name}::#{metric.to_s.camelize}".constantize
        end
      end

      def self.build(ticket, metric, &transitions)
        context = Context.new(ticket, metric)
        action  = Action.new(ticket, context)

        FiniteMachine.new(ticket, alias_target: :ticket) do
          initial Transition.new(ticket, context.latest_events).last.to_state

          instance_eval(&transitions)

          on_before :advance do
            action.measure if context.new_ticket?

            cancel_event unless context.measured?
          end

          if machine.states.include?(:resumed)
            on_transition :resumed do
              action.activate
            end
          end

          if machine.states.include?(:initiated)
            on_transition :initiated do |event|
              action.fulfill if event.from == :activated

              action.activate
            end
          end

          on_transition :deactivated do
            context.new_ticket? ? action.instafulfill : action.fulfill
          end

          if machine.states.include?(:paused)
            on_transition :paused do
              action.pause
            end
          end

          if context.sla.supported?
            on_after :advance do
              action.clear_breaches

              next if is?(:deactivated)

              action.apply_sla if context.sla.changed?

              next if ACTIVATED_STATES.none? { |state| is?(state) }

              action.breach if context.sla.breachable?
            end
          end
        end.tap do |state_machine| # rubocop:disable Style/MultilineBlockChain
          state_machine.class_eval { define_method(:context) { context } }
        end
      end
    end
  end
end

require 'zendesk/ticket_metric/state_machine/abstract'
require 'zendesk/ticket_metric/state_machine/agent_work_time'
require 'zendesk/ticket_metric/state_machine/pausable_update_time'
require 'zendesk/ticket_metric/state_machine/periodic_update_time'
require 'zendesk/ticket_metric/state_machine/reply_time'
require 'zendesk/ticket_metric/state_machine/requester_wait_time'
require 'zendesk/ticket_metric/state_machine/resolution_time'
