module Zendesk::TicketMetric
  class Action
    def initialize(ticket, context)
      @ticket  = ticket
      @context = context
    end

    def measure
      TicketMetric::Measure.create(
        account: ticket.account,
        ticket:  ticket,
        metric:  context.metric,
        time:    ticket.created_at
      )
    end

    def activate
      context.events.activate.create(
        account:     ticket.account,
        instance_id: context.next_instance_id,
        time:        ticket.updated_at
      )
    end

    def pause
      context.events.pause.create(
        account:     ticket.account,
        instance_id: context.latest_instance_id,
        time:        ticket.updated_at
      )
    end

    def fulfill
      context.events.fulfill.create(
        account:     ticket.account,
        instance_id: context.latest_instance_id,
        time:        ticket.updated_at
      )

      TicketMetric::UpdateStatus.create(
        account:     ticket.account,
        ticket:      ticket,
        metric:      context.metric,
        instance_id: context.latest_instance_id,
        time:        ticket.updated_at
      )
    end

    def instafulfill
      context.events.fulfill.create(
        account:     ticket.account,
        instance_id: 1,
        time:        ticket.created_at
      )
    end

    def apply_sla
      SlaTargetChange.create(
        ticket:      ticket,
        audit:       ticket.audit,
        target_info: {
          initial_target:     context.sla.last_policy_metric.to_audit,
          final_target:       context.sla.policy_metric.to_audit,
          metric_id:          context.sla.policy_metric.metric_id,
          current_sla_policy: context.sla.policy_metric.policy
        }
      )

      TicketMetric::ApplySla.create(
        account:     ticket.account,
        ticket:      ticket,
        metric:      context.metric,
        instance_id: context.latest_instance_id,
        time:        ticket.updated_at
      )
    end

    def breach
      context.events.breach.create(
        account:     ticket.account,
        instance_id: context.latest_instance_id,
        time:        context.sla.breach_time
      )
    end

    def clear_breaches
      context.events.breach.after(ticket.updated_at).each(&:soft_delete)
    end

    protected

    attr_reader :ticket,
      :context
  end
end
