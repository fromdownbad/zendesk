module Zendesk::TicketMetric
  class Sla
    def initialize(ticket, context)
      @ticket  = ticket
      @context = context
    end

    def changed?
      !policy_metric.equivalent?(last_policy_metric)
    end

    def policy_metric
      ticket.sla_ticket_policy.try do |ticket_policy|
        ticket_policy.policy_metrics.where(metric_id: sla_metric.id).first
      end || none_metric
    end

    def last_policy_metric
      last_sla_application.try(:policy_metric) || none_metric
    end

    def breachable?
      policy_metric.present? && last_sla_application.present? && !active_breach?
    end

    def breach_time
      [
        periods.calculation.time(after: policy_metric.target),
        ticket.updated_at
      ].max
    end

    def supported?
      Zendesk::Sla::TicketMetric.supported_metric?(context.metric)
    end

    protected

    attr_reader :ticket,
      :context

    private

    def last_sla_application
      context.latest_events.apply_sla.last
    end

    def active_breach?
      context.latest_events.breach.after(last_sla_application.time).any?
    end

    def sla_metric
      Zendesk::Sla::TicketMetric.
        for_metric(context.metric).
        find { |metric| metric.valid_instance?(context.latest_instance_id) }
    end

    def periods
      Periods.new(
        ticket:         ticket,
        metric_events:  context.latest_events,
        business_hours: policy_metric.business_hours?
      )
    end

    def none_metric
      @none_metric ||= None.new(sla_metric)
    end
  end
end
