module Zendesk::TicketMetric
  class Context
    attr_reader :ticket,
      :metric

    def initialize(ticket, metric)
      @ticket = ticket
      @metric = metric.to_sym
    end

    def new_ticket?
      ticket.id_changed?
    end

    def status_changed?
      new_ticket? || ticket.status_id_changed?
    end

    def measured?
      Zendesk::TicketMetric.launch?(metric) || events.measure.any?
    end

    def latest_instance_id
      [1, events.latest_instance_id].max
    end

    def next_instance_id
      Zendesk::TicketMetric.recurring?(metric) ? events.next_instance_id : 1
    end

    def events
      @events ||= ticket.metric_events.send(metric)
    end

    def latest_events
      events.where(instance_id: latest_instance_id)
    end

    def sla
      @sla ||= Sla.new(ticket, self)
    end
  end
end
