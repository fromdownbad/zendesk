module Zendesk
  class RequestOriginDetails
    ZENDESK_MOBILE_CLIENTS = %w[
      mobile/android/apps/support
      mobile/ios/apps/support
    ].freeze
    LOTUS_SOURCE = 'lotus'.freeze

    ATTRIBUTES = %i[
      account
      lotus_version
      lotus_feature
      lotus_initial_user_id
      lotus_initial_user_role
      lotus_tab_id
      lotus_uuid
      lotus_expiry_refetch
      lotus_radar_client_socket
      lotus_radar_client_status
      app_id
      app_name
      referer
      request_source
      remote_ip
      pod_relay
      mobile_client
      mobile_integration
      mobile_integration_version
      mobile_client_version
      force_exception_locale
      rule_admin_version
    ].freeze

    attr_reader *ATTRIBUTES

    def initialize(env = {})
      @account        = env['zendesk.account'].presence
      @lotus_version  = env['HTTP_X_ZENDESK_LOTUS_VERSION'].presence
      @lotus_feature  = env['HTTP_X_ZENDESK_LOTUS_FEATURE'].presence
      @app_id         = env['HTTP_X_ZENDESK_APP_ID'].presence
      @app_name       = env['HTTP_X_ZENDESK_APP_NAME'].presence
      @pod_relay      = env['HTTP_X_ZENDESK_POD_RELAY'].presence
      @remote_ip      = env["action_dispatch.remote_ip"]&.to_s.presence
      @referer        = stripped_referer_path(env['HTTP_REFERER']).presence
      @request_source = source.presence
      @force_exception_locale = env["HTTP_X_FORCE_EXCEPTION_LOCALE"].presence

      # X-Zendesk-Lotus-Initial-User-Id
      @lotus_initial_user_id = env['HTTP_X_ZENDESK_LOTUS_INITIAL_USER_ID'].presence
      # X-Zendesk-Lotus-Initial-User-Role
      @lotus_initial_user_role = env['HTTP_X_ZENDESK_LOTUS_INITIAL_USER_ROLE'].presence
      # X-Zendesk-Lotus-Tab-Id
      @lotus_tab_id = env['HTTP_X_ZENDESK_LOTUS_TAB_ID'].presence
      # X-Zendesk-Lotus-Uuid
      @lotus_uuid = env['HTTP_X_ZENDESK_LOTUS_UUID'].presence
      # X-Zendesk-Lotus-Expiry-Refetch
      @lotus_expiry_refetch = env['HTTP_X_ZENDESK_LOTUS_EXPIRY_REFETCH'].presence

      # Radar
      @lotus_radar_client_socket = env['HTTP_X_ZENDESK_RADAR_SOCKET'].presence
      @lotus_radar_client_status = env['HTTP_X_ZENDESK_RADAR_STATUS'].presence

      # mobile integration
      @mobile_client               = env["HTTP_X_ZENDESK_CLIENT"].presence
      @mobile_integration          = env["HTTP_X_ZENDESK_INTEGRATION"].presence
      @mobile_integration_version  = env["HTTP_X_ZENDESK_INTEGRATION_VERSION"].presence
      @mobile_client_version       = env["HTTP_X_ZENDESK_CLIENT_VERSION"].presence
      # support-rule-admin
      @rule_admin_version          = env['HTTP_X_ZENDESK_RULE_ADMIN_VERSION'].presence
    end

    def to_h
      @to_hash ||= ATTRIBUTES.each_with_object({}) do |attr, hash|
        hash[attr] = public_send(attr)
      end
    end

    def zendesk_mobile_client?
      mobile_client.in? ZENDESK_MOBILE_CLIENTS
    end

    def lotus?
      request_source == LOTUS_SOURCE
    end

    def rule_admin?
      rule_admin_version.present?
    end

    private

    def source
      if app_id
        'app'
      elsif lotus_version
        'lotus'
      else
        'api'
      end
    end

    def stripped_referer_path(referer)
      return unless referer

      # Get the path from the URI and strip any leading numbers (typically
      # Zendesk account IDs)
      return unless path = URI.parse(referer).path

      path.gsub(/\/\d+(\W|$)/, '/')
    rescue URI::InvalidURIError
      # not a URI, return nil
    end
  end
end
