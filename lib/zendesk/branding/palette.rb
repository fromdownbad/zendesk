require 'color/palette/monocontrast'

# there are two versions of the hsl variables
# h = 0 .. 1      hue = 0 .. 360
# s = 0 .. 1      saturation = 0 .. 100
# l = 0 .. 1      luminosity = 0 .. 100

module Zendesk
  module Branding
    class Palette
      attr_reader :brand_color

      FONT_COLOR_THRESHOLD = 130
      TAB_HOVER_LUMINANCE_FACTOR_LOW_CONTRAST = 0.05
      TAB_HOVER_LUMINANCE_FACTOR_HIGH_CONTRAST = 0.1
      RED_LUMINANCE_FACTOR = 0.21
      GREEN_LUMINANCE_FACTOR = 0.3
      BLUE_LUMINANCE_FACTOR = 0.07
      LIGHTNESS_THRESHOLD = 0.94
      DARK_GRAY = '333333'.freeze
      WHITE = 'FFFFFF'.freeze

      def initialize(settings)
        @brand_color = settings.header_color
      end

      def secondary_color
        hsl = Color::RGB.by_hex(brand_color).to_hsl
        hsl_to_hex apply_secondary_color_rules(hsl)
      end

      def tab_hover_color
        hsl = hex_to_hsl(secondary_color)
        hsl_to_hex apply_hover_color_rules(hsl)
      end

      def contrast_color
        light_color?(secondary_color) ? DARK_GRAY : WHITE
      end

      def light_font?
        light_color?(secondary_color) ? false : true
      end

      def light_brand_color?
        hex_to_hsl(brand_color).l > LIGHTNESS_THRESHOLD
      end
      alias_method :light_header?, :light_brand_color?

      def navbar_bg_color_rgb
        if light_brand_color?
          '0, 0, 0'
        else
          '255, 255, 255'
        end
      end

      private

      def light_color?(hex_color)
        rgb = hex_to_rgb(hex_color)
        red = rgb[0]
        green = rgb[1]
        blue = rgb[2]
        brightness_squared = red**2 * RED_LUMINANCE_FACTOR +
                             green**2 * GREEN_LUMINANCE_FACTOR +
                             blue**2 * BLUE_LUMINANCE_FACTOR
        brightness = Math.sqrt(brightness_squared)
        brightness > FONT_COLOR_THRESHOLD
      end

      def hex_to_hsl(hex_color)
        Color::RGB.by_hex(hex_color).to_hsl
      end

      def hsl_to_hex(hsl)
        hsl.to_rgb.hex.upcase
      end

      def hex_to_rgb(hex)
        hex = hex.delete('#')
        if hex.length < 6
          hex = hex[0] * 2 + hex[1] * 2 + hex[2] * 2
        end
        hex.scan(/../).map(&:hex)
      end

      def apply_hover_color_rules(hsl)
        if (hsl.hue > 50 || hsl.hue < 190) && hsl.l > 0.6 && hsl.s > 0.6
          hsl.l -= TAB_HOVER_LUMINANCE_FACTOR_HIGH_CONTRAST
        elsif hsl.l < 0.2
          hsl.l += TAB_HOVER_LUMINANCE_FACTOR_LOW_CONTRAST
        else
          hsl.l -= TAB_HOVER_LUMINANCE_FACTOR_LOW_CONTRAST
        end
        hsl
      end

      def apply_secondary_color_rules(hsl)
        hue = hsl.hue
        s = hsl.s
        l = hsl.l

        if l < 0.05
          s = 0
          l += 0.3
        elsif s >= 0.4
          l = if l < 0.1
            if hue <= 50
              l + 0.15
            elsif hue < 190
              l + 0.1
            elsif hue < 210
              l + 0.20
            elsif hue < 280
              l + 0.30
            else
              l + 0.20
            end
          elsif l < 0.2
            if hue < 10
              l + 0.15
            elsif hue < 190
              l + 0.10
            elsif hue < 210
              l + 0.15
            elsif hue < 280
              l + 0.3
            else
              l + 0.2
            end
          elsif l < 0.45
            if hue < 190
              l + 0.1
            elsif hue < 210
              l + 0.15
            else
              l + 0.2
            end
          elsif l < 0.55
            if hue < 50
              l + 0.15
            elsif hue < 80
              l + 0.2
            elsif hue < 190
              l + 0.3
            elsif hue < 280
              l + 0.15
            else
              l + 0.2
            end
          elsif l < 0.65
            if hue < 50
              l + 0.15
            elsif hue < 80
              l - 0.15
            elsif hue < 190
              l + 0.25
            elsif hue < 280
              l + 0.15
            else
              l + 0.2
            end
          elsif l < 0.7
            if hue < 50
              l + 0.1
            elsif hue < 80
              l - 0.2
            elsif hue < 190
              l + 0.2
            else
              l + 0.1
            end
          elsif l < 0.8
            if hue < 50
              l + 0.1
            elsif hue < 80
              l - 0.2
            elsif hue < 190
              l + 0.15
            else
              l + 0.1
            end
          elsif l < 0.85
            if hue < 50
              l + 0.1
            elsif hue < 190
              l - 0.2
            else
              l + 0.1
            end
          else
            if hue < 50
              l - 0.1
            elsif hue < 190
              l - 0.2
            else
              l - 0.1
            end
          end
        else
          l = if l < 0.1
            l + 0.2
          elsif l < 0.4
            if hue < 210
              l + 0.15
            else
              l + 0.2
            end
          elsif l < 0.45
            l + 0.15
          elsif l < 0.85
            l + 0.1
          else
            l - 0.1
          end
        end
        hsl.hue = hue
        hsl.s = s
        hsl.l = l

        hsl
      end
    end
  end
end
