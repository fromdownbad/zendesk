require 'zendesk_search/client'

module Zendesk
  module Elasticsearch
    module ControllerSupport
      private

      def search_client
        @search_client ||= begin
          ZendeskSearch::Client.new(
            current_account.id,
            current_account.has_search_green?,
            current_account.has_ss_enable_search_service_hub?
          )
        end
      end

      def set_search_cache_control_header
        # prevent Elasticsearch overload from very frequent use more than one request per second (per client)
        # the cache key is the unique URL with params and we use client's cache not per account
        # set "Cache-control:max-age=5,private" very similar how we protected Occam service
        # more details https://zendesk.atlassian.net/wiki/spaces/CP/pages/481558838

        if current_account.has_search_cache_control_header?
          if lotus_internal_request?
            expires_in(5.seconds)
          else
            expires_in(10.seconds)
          end
        end
      end

      def check_search_short_circuit_limiter
        # checks for a Search-specific "blackhole" Arturo
        if current_account.has_search_short_circuit_limiter?
          response.headers['Retry-After'] = '120' # Include header for retry after 2 minutes
          render json: { message: "Too many requests. Please retry later." }, status: 429
        end
      end
    end
  end
end
