module Zendesk::Maintenance::Util
  module BatchClose
    class << self
      def close_for_account!(account, args)
        ActiveRecord::Base.on_shard(account.shard_id) do
          batch_close_tickets(account, args)
        end
      end

      private

      def log(message)
        message = "BatchClose P#{Process.pid} #{Time.now}: #{message}"
        Rails.logger.info(message)
      end

      def batch_close_tickets(account, args)
        max_records   = args[:max_records]   || 1_000_000
        solved_before = args[:solved_before] || 28.days.ago
        batch_size    = args[:batch_size]    || 200
        record_count  = 0

        conditions = ["status_id = ? AND solved_at < ?", StatusType.SOLVED, solved_before]

        account.tickets.where(conditions).find_in_batches(batch_size: batch_size) do |tickets|
          while ticket = tickets.pop
            record_count += 1 if brute_close_ticket(ticket)

            if record_count >= max_records
              log("Closed #{record_count} tickets which is >= #{max_records}, stopping")
              return record_count
            end
          end
        end

        record_count
      end

      def brute_close_ticket(ticket)
        log("Closing solved ticket ##{ticket.id}/#{ticket.account_id}. Updated #{ticket.updated_at}, solved #{ticket.solved_at}")

        # Update the ticket record without invoking triggers
        ticket.will_be_saved_by(User.system, via_id: ViaType.BATCH)
        ticket.audit.disable_triggers = true
        ticket.status_id = StatusType.CLOSED
        unless ticket.save
          Rails.logger.info("DailyTicketCloseOnShardJob: Failed to close ticket #{ticket.id} for account #{ticket.account_id} with errors #{ticket.errors&.full_messages&.join(', ')} ")
          statsd_client.increment('brute_close_ticket', tags: ["success:false"])
          return false
        end
        statsd_client.increment('brute_close_ticket', tags: ["success:true"])
        true
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'daily_ticket_close_on_shard_job')
      end
    end
  end
end
