module Zendesk::Maintenance::Util
  # Releases a subdomain for a deleted account by appending "-{timestamp}-deleted"
  # to account subdomain, route subdomains, and host-mapping (if any mapping)
  class SubdomainReleaser
    attr_reader :account

    def initialize(acct)
      @account = acct
    end

    def release_subdomain!
      if deleted_subdomain?(account.subdomain)
        Rails.logger.info("Account subdomain #{account.subdomain} is already renamed to -deleted suffix")
        return
      end

      rename_account_subdomain
      rename_account_host_mapping
    end

    def verify_subdomain_released!
      raise "Releasing subdomain failed for account #{account.id}: subdomain #{account.subdomain} wasn't renamed" unless deleted_subdomain?(account.subdomain)
      raise "Releasing subdomain failed for account #{account.id}: host-mapping #{account.host_mapping} wasn't renamed" if account.host_mapping && !deleted_subdomain?(account.host_mapping)

      account.routes.each do |route|
        raise "Releasing subdomain failed for account #{account.id}: route subdomain #{route.subdomain} wasn't renamed" unless deleted_subdomain?(route.subdomain)
        raise "Releasing subdomain failed for account #{account.id}: route host-mapping #{route.host_mapping} wasn't renamed" if route.host_mapping && !deleted_subdomain?(route.host_mapping)
      end
    end

    private

    def rename_account_subdomain
      mark_subdomain(append_deleted(account.subdomain))
      rename_route_subdomain(account)
    end

    def deleted_subdomain?(subdomain)
      subdomain =~ /.+-[0-9]+-deleted\z/
    end

    def rename_account_host_mapping
      mark_host_mapping(append_deleted(account.host_mapping)) unless account.host_mapping.nil?
      rename_route_host_mapping(account)
    end

    def rename_route_subdomain(account)
      account.routes.each do |route|
        route.update_column(:subdomain, append_deleted(route.subdomain))
        route.update_column(:updated_at, Time.now)
      end
    end

    def mark_subdomain(new_subdomain)
      account.update_column(:subdomain, new_subdomain)
      account.update_column(:updated_at, Time.now)
    end

    def mark_host_mapping(new_host_mapping)
      account.update_column(:host_mapping, new_host_mapping)
      account.update_column(:updated_at, Time.now)
    end

    def rename_route_host_mapping(account)
      account.routes.reject { |r| r.host_mapping.nil? }.each do |route|
        route.update_column(:host_mapping, append_deleted(route.host_mapping))
        route.update_column(:updated_at, Time.now)
      end
    end

    # Builds a new subdomain for a deleted account by appending "-{TS}-deleted" to the account subdomain;
    # if the subdomain is too long, cuts the subdomain down to keep total length to <= the DNS character limit.
    def append_deleted(subdomain)
      return nil unless subdomain
      return subdomain if deleted_subdomain?(subdomain)
      suffix = "-#{timestamp}-deleted"
      cutlen = DNS_CHARACTER_LIMIT - suffix.length
      subdomain[0, cutlen] + suffix
    end

    def timestamp
      @timestamp ||= Time.now.to_i
    end
  end
end
