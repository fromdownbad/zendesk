module Zendesk::Maintenance::Jobs
  class NotificationCheckerJob < PerShardJob
    class << self
      def work_on_shard(_shard)
        late = Resque::Durable::QueueAudit.
          where(job_klass: 'MailRenderingJob').
          where(completed_at: nil).
          where('created_at < ?', 20.minutes.ago.to_s(:db)).
          count(:all)

        if late > 20
          resque_warn("#{late} notifications more than 20 minutes old")
        end
      end

      def args_to_log(*_args)
        { job: name.demodulize, time: Time.now.to_s }
      end
    end
  end
end
