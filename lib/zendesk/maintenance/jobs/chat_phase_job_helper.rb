module Zendesk::Maintenance::Jobs
  module ChatPhaseJobHelper
    class ChatPhase3SyncError < StandardError
    end

    NUMBER_OF_ACCOUNTS_PER_QUERY = 1000

    def chat_products(starting_after)
      system_account = Account.find Account.system_account_id
      subdomain = Zendesk::Accounts::Client::DEFAULT_SUBDOMAIN
      Zendesk::Accounts::Client.new(system_account, subdomain).chat_products(NUMBER_OF_ACCOUNTS_PER_QUERY, starting_after)
    end

    def log_errors(problem_accounts, statsd_name, message)
      accounts = problem_accounts.to_set.to_a
      statsd_client.gauge(statsd_name, accounts.count)
      custom_error = ChatPhase3SyncError.new('Chat phase 3 sync error')
      ZendeskExceptions::Logger.record(custom_error, location: self, message: "#{message}: #{accounts}", fingerprint: '515506b150408358ef5f9836fe9a05b8a5900f42')
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['chat_phase_3'])
    end

    def chat_job_enabled?(chat_job_feature)
      feature = Arturo::Feature.find_feature(chat_job_feature)
      feature.try(:phase) == 'on' || feature.try(:deployment_percentage) == 100
    end

    def validate_with_block
      starting_after = 0
      loop do
        products = chat_products(starting_after)
        break if products.empty?
        starting_after = products.last.id

        products = products.select { |p| p.plan_settings["phase"] == 3 }
        yield(products) unless products.empty?
      end
    end
  end
end
