module Zendesk::Maintenance::Jobs
  class RemoveVoiceDataJob < BaseDataDeleteJob
    priority :account_deletion
    stage :before_db

    private

    def erase_for_canceled
      Rails.logger.warn("Deleting voice data for account: #{account.subdomain} has_voice_data_deletion: #{account.has_voice_data_deletion?}")
      return unless account.has_voice_data_deletion?
      return unless account.voice_sub_account.present?
      Zendesk::Voice::InternalApiClient.new(account.subdomain).account_deleted!
    end
  end
end
