module Zendesk::Maintenance::Jobs
  class AcmeCertificateRenewalJob < PerAccountJob
    class << self
      def fetch_accounts
        scope = Account.active.serviceable.shard_unlocked.
          joins(:certificates).
          where("certificates.state = 'active'").
          where('certificates.autoprovisioned = true').
          where('certificates.valid_until < ?', today_plus_early_renewal_days)

        scope.find_each { |account| yield account }
      end

      def today_plus_early_renewal_days
        Date.today + AcmeCertificate::EARLY_RENEWAL_DAYS
      end

      def work_on_account(account)
        if account.brand_host_mappings.empty?
          revoke_certificates!(account)
        else
          Rails.logger.info "AcmeCertificateRenewalJob requesting updated certificate for account ##{account.id} #{account.subdomain}"
          AcmeCertificateJobStatus.enqueue_with_status!(account.id)
        end
        pause_for_a_bit
      end

      def revoke_certificates!(account)
        account.certificates.active.each do |cert|
          Rails.logger.info "AcmeCertificateRenewalJob account ##{account.id} #{account.subdomain} revoking certificate #{cert.id} since account has no hostmappings"
          cert.revoke!
        end
      end

      def pause_for_a_bit
        sleep rand * 20
      end

      def args_to_log
        {}
      end
    end
  end
end
