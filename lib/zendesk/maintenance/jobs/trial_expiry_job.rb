module Zendesk::Maintenance::Jobs
  # Scans accounts and expire those that are about to expired
  class TrialExpiryJob < PodMaintenanceJob
    class InvalidTrialException < StandardError; end

    def execute
      accounts = Account.shard_unlocked.where("is_active = 1 AND accounts.id > 1 and sandbox_master_id IS NULL AND subscriptions.is_trial = 1").includes(:subscription, :pre_account_creation).references(:subscription).to_a
      accounts.reject! do |a|
        if a.pre_account_creation.present? && !a.pre_account_creation.bounded?
          # don't expire pre created accounts
          true
        else
          if a.subscription.trial_expires_on.nil?
            exception = InvalidTrialException.new("Missing trial expire date for account #{a.id}")
            ZendeskExceptions::Logger.record(exception, location: self, message: exception.message, fingerprint: '9ae55f61ffd807be9c3aaaccd79ef913ede36f20')
          end
          !a.subscription.past_expiration_date? # if expiration date has not passed, don't expire it
        end
      end
      Rails.logger.info("Processing trial expiry for #{accounts.size} accounts")
      expire_accounts(accounts) unless dryrun?
      Rails.logger.info("Processed #{accounts.size} expirations")
    end

    private

    def expire_accounts(accounts)
      accounts.each do |account|
        begin
          ActiveRecord::Base.on_shard(account.shard_id) do
            account.expire_trial!
            account.subscription.pricing_model_revision = ZBC::Zendesk::PricingModelRevision::PATAGONIA
            Zendesk::SupportAccounts::Product.retrieve(account).save(validate: false)
          end

          Rails.logger.info("Expired trial for account #{account.id}")
        rescue StandardError => exception
          ZendeskExceptions::Logger.record(
            exception,
            location:    self,
            message:     exception.message,
            fingerprint: '9095e5f2173a5c92fdbb80bbc3dbaa7d15648c01'
          )
        end
      end
    end
  end
end
