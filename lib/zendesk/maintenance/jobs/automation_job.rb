module Zendesk::Maintenance::Jobs
  class AutomationJob < PerShardJob
    priority :automations

    class << self
      def work(*args)
        return super(*args) if Arturo.feature_enabled_for_pod?(:shard_level_automation_job, Zendesk::Configuration.fetch(:pod_id))

        time = beginning_of_the_hour

        resque_log("Begining to enqueue for time:#{time}");
        available_accounts.shuffle.each do |account_id, shard|
          ActiveRecord::Base.on_shard(shard) do
            process(account_id, time)
          end
        end
        resque_log("Done with enqueues");
      rescue Exception => e # rubocop:disable Lint/RescueException
        resque_error("Unable to finish AutomationJob: #{e.inspect}")
        raise e
      end

      def available_accounts
        selected = []
        # Pick all accounts qualified quickly
        Account.where(shard_id: ActiveRecord::Base.shard_names).scoping do
          ActiveRecord::Base.on_all_shards do |shard|
            resque_log("On shard #{shard}, time = #{Time.now}")
            AccountSetting.where('name = ? AND value > ?', 'last_login', 14.days.ago).select(:id, :account_id).find_in_batches do |account_settings|
              account_ids = account_settings.map(&:account_id)

              # Further filter based on active, unlocked
              Account.shard_local.active.shard_unlocked.where(id: account_ids).select(:id, :lock_state).find_each do |account|
                if account.has_pause_automation_execution?
                  Rails.logger.info(
                    "`:pause_automation_execution` arturo enabled for " \
                    "account_id: #{account.id} on shard #{shard}, skipping " \
                    "automation processing."
                  )
                else
                  selected << [account.id, shard]
                end
              end
            end
            resque_log("Done selecting accounts with shard #{shard}, time = #{Time.now}")
          end
        end
        selected
      end

      def work_on_shard(shard_id)
        time = beginning_of_the_hour

        process_time = Benchmark.realtime do
          eligible_accounts_on_shard.shuffle.each do |account_id|
            process(account_id, time)
          end
        end

        statsd_client.timing('account_automation_enqueues', process_time.to_i, tags: ["shard_id: #{shard_id}"])
      end

      def process(account_id, time)
        AccountAutomationsJob.enqueue(account_id, time, false)
        one_percent_times do
          resque_log("Enqueued upto #{account_id}/for:#{time} at #{Time.now}")
        end
      end

      def one_percent_times
        yield if rand < 0.01
      end

      private

      def account_settings_conditions
        ['name = ? AND value > ?', 'last_login', 14.days.ago]
      end

      def beginning_of_the_hour
        Time.at(Time.now.to_i - (Time.now.to_i % 1.hour))
      end

      def eligible_accounts_on_shard
        selected = []

        AccountSetting.where(account_settings_conditions).select(:id, :account_id).find_in_batches do |account_settings|
          account_ids = account_settings.map(&:account_id)

          # Further filter based on active, unlocked
          Account.shard_local.active.shard_unlocked.where(id: account_ids).select(:id, :lock_state).find_each do |account|
            if account.has_pause_automation_execution?
              Rails.logger.info(
                "`:pause_automation_execution` arturo enabled for " \
                "account_id: #{account.id}, skipping " \
                "automation processing."
              )
            else
              selected << account.id
            end
          end
        end
        selected
      end

      def statsd_client
        Zendesk::StatsD.client(namespace: 'automation_job')
      end
    end
  end
end
