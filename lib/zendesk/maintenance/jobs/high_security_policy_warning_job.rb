module Zendesk::Maintenance::Jobs
  class HighSecurityPolicyWarningJob < PodMaintenanceJob
    def execute
      accounts_created_60_days_ago.each do |account|
        ActiveRecord::Base.on_shard(account.shard_id) do
          ActiveRecord::Base.cache do
            if role_security_policies(account).all? { |policy| policy.level?(:high) }
              logger.info("High security policy warning for account #{account.id} started delivery")
              SecurityPolicyMailer.deliver_high_security_policy_warning(account)
            end
          end
        end
      end
    end

    protected

    def accounts_created_60_days_ago
      Account.joins(:subscription).active.shard_unlocked.where("DATE(accounts.created_at) = ? AND accounts.is_serviceable = true", 60.days.ago.to_date.to_s(:db)).to_a
    end

    def role_security_policies(account)
      [account.agent_security_policy, account.end_user_security_policy]
    end
  end
end
