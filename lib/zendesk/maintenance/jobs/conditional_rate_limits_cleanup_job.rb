module Zendesk::Maintenance::Jobs
  # Query Account Text table for an account's conditional rate limits and delete the limits
  # that expired
  class ConditionalRateLimitsCleanupJob < PerAccountJob
    class << self
      def work_on_account(account)
        return unless account.texts.present? && account.conditional_rate_limits.present?
        cleanup_conditional_rate_limits!(account)
      end

      def args_to_log
        {}
      end

      private

      def cleanup_conditional_rate_limits!(account)
        account.conditional_rate_limits.each do |conditional_rate_limit|
          next unless expired_conditional_rate_limit?(conditional_rate_limit)
          options = {
            'subdomain' => account.subdomain,
            'prop_key' => conditional_rate_limit['prop_key'],
            'expires_on' => conditional_rate_limit['expires_on'],
            'owner_email' => conditional_rate_limit['owner_email'],
          }
          Rails.logger.append_attributes(
            conditional_rate_limit: {
              subdomain: options['subdomain'],
              prop_key: options['prop_key'],
              expires_on: options['expires_on']
            }
          )
          account.remove_conditional_rate_limit!(options['prop_key'])
          MonitorMailer.deliver_rate_limit_notification(account, options) unless options['owner_email'].nil?
        end
      end

      def expired_conditional_rate_limit?(conditional_rate_limit)
        return if conditional_rate_limit['expires_on'].nil?
        Time.parse(conditional_rate_limit['expires_on']) < Time.now
      end
    end
  end
end
