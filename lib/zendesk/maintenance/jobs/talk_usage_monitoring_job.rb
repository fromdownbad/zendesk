module Zendesk::Maintenance::Jobs
  class TalkUsageMonitoringJob < PerShardJob
    priority :low

    class << self
      def work_on_shard(shard_id)
        return unless scheduled_talk_usage_tracking?

        ActiveRecord::Base.on_shard(shard_id) do
          min_threshold = track_talk_usage_older_than_hour? ? 1.hour.ago : 1.day.ago

          stale_usages = ZBC::Voice::UnprocessedUsage.
            where('usage_type != "failed_recharge" AND created_at < ?', min_threshold).
            group(:account_id).
            select('account_id, MIN(id) as oldest_id, COUNT(*) as count, MIN(created_at) as oldest_created_at')

          stale_usages.each do |stale_usage|
            statsd_client.gauge(
              'stale_usages',
              stale_usage.count,
              tags: [
                "account_id:#{stale_usage.account_id}",
                "usage_older_than:#{older_than_label(stale_usage.oldest_created_at)}"
              ]
            )
            process_usage(stale_usage)
          end
        end
      rescue StandardError => e
        resque_error("Error during TalkUsageMonitoringJob execution: #{e.message}")
      end

      private

      def older_than_label(timestamp)
        if timestamp < 3.months.ago
          'quarter'
        elsif timestamp < 1.month.ago
          'month'
        elsif timestamp < 1.week.ago
          'week'
        elsif timestamp < 1.day.ago
          'day'
        elsif timestamp < 1.hour.ago
          'hour'
        end
      end

      def process_usage(stale_usage)
        ZBC::Voice::Jobs::ProcessUsageJob.enqueue(stale_usage.oldest_id, stale_usage.account_id)
      end

      def track_talk_usage_older_than_hour?
        arturo_enabled?(:track_talk_usage_older_than_hour)
      end

      def scheduled_talk_usage_tracking?
        arturo_enabled?(:scheduled_talk_usage_tracking)
      end

      def arturo_enabled?(arturo_name)
        feature = Arturo::Feature.find_feature(arturo_name)
        feature.try(:phase) == 'on' || feature.try(:deployment_percentage) == 100
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['voice_usage'])
      end
    end
  end
end
