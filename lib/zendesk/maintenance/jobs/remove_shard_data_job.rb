module Zendesk::Maintenance::Jobs
  class RemoveShardDataJob < BaseDataDeleteJob
    priority :account_deletion

    stage :db

    MINIMUM_BACKOFF  = 0.3.seconds
    DEFAULT_BATCH    = 1000
    REENQUEUE_PERIOD = 10.minutes
    REENQUEUE_THRESHOLD = 5.minutes

    private

    def erase_for_canceled
      validate_account(account.shard_id)
      delete_from_shard(account.shard_id)
      remove_spokes unless dry_run?
    end

    def verify_for_canceled
      ActiveRecord::Base.on_shard(account.shard_id) do
        tables = find_tables
        verify_shard_data(tables)
        verify_account_ids
      end
    end

    def erase_for_moved
      delete_from_shard(account_move_source_shard_id)
    end

    def permanent_tables
      %w[account_move_shard_changes]
    end

    def validate_account(shard_id)
      raise 'shard id cannot be nil, job will only remove data from sharded database' if shard_id.nil?
      raise 'can\'t delete data from non soft deleted accounts on the current shard' if account.deleted_at.nil? && account.shard_id == shard_id
    end

    def delete_from_shard(shard_id)
      ActiveRecord::Base.on_shard(shard_id) do
        tables = find_tables
        if dry_run?
          Rails.logger.error("#{job_id_for_log}: Not deleting shard data because in dry-run mode; tables: #{tables}")
          return
        end
        delete_shard_data(tables)
        delete_account_ids
      end
    end

    def delete_shard_data(tables)
      tables.each do |table|
        delete_table_data(table)
      end
    end

    attr_accessor :database_backoff

    # We're not high priority! Sleep and give the database a break if it's overloaded.
    def sleep_to_database_backoff
      @database_backoff ||= Zendesk::DatabaseBackoff.new(minimum: MINIMUM_BACKOFF)
      duration = @database_backoff.backoff_duration
      statsd_client.gauge('backoff_duration', duration, tags: ['when:on_sleep'])
      Rails.logger.info "#{job_id_for_log}: Database Backoff (on sleep): #{@database_backoff.to_hash}"
      if duration > REENQUEUE_THRESHOLD
        raise TemporaryExit, "Requested backoff #{duration} over threshold #{REENQUEUE_THRESHOLD}, temporarily exiting"
      else
        sleep(duration.remainder(1.0))
        duration.floor.times do |i|
          sleep(1.0)
          current_backoff = @database_backoff.backoff_duration
          statsd_client.gauge('backoff_duration', current_backoff, tags: ['when:mid_sleep'])
          Rails.logger.info "#{job_id_for_log}: Database Backoff (mid-sleep): #{@database_backoff.to_hash}"
          if current_backoff < i
            Rails.logger.info "#{job_id_for_log}: Breaking backoff after #{i} seconds"
            break
          end
        end
      end
    end

    def verify_shard_data(tables)
      tables.each do |table|
        raise KillSwitchEnabled if kill_switch_flipped?
        raise "RemoveShardDataJob failed for account #{account.id}: Sharded data still exists in table #{table}." if rows_exists?(table)
      end
    end

    # Special handling for account_ids table
    def delete_account_ids
      table = relation(:account_ids)
      statement = table.where(table[:id].eq(account.id)).compile_delete.to_sql
      connection.delete(statement)
    end

    # Special handling for account_ids table
    def verify_account_ids
      table = relation(:account_ids)
      query = table.where(table[:id].eq(account.id)).take(1).project(:id).take(1).to_sql
      raise "RemoveShardDataJob failed for account #{account.id}. Row still in account_ids table." unless connection.exec_query(query).rows.first.nil?
    end

    # does this table still have rows for this account.
    def rows_exists?(table)
      t = relation(table)
      statement = t.where(t[:account_id].eq(account.id)).project(t[:account_id]).take(1).to_sql
      r = connection.execute(statement)
      r.first.present?
    end

    def delete_table_data(table)
      Rails.logger.info "#{job_id_for_log}: Starting deletion from table #{table}"
      verify_table_parameter(table)

      ZendeskAPM.trace('delete_table_data') do |span|
        span.set_tag('table', table)

        loop do
          break if delete_one_batch(table) == 0
        end
      end
    end

    # returns the number of rows deleted.
    def delete_one_batch(table)
      raise KillSwitchEnabled if kill_switch_flipped?
      warn_maxwell
      rows_deleted = delete_rows(table)
      sleep_to_database_backoff
      rows_deleted
    end

    # returns the number of rows deleted.
    def delete_rows(table)
      statsd_client.time('delete_rows', tags: ["table:#{table}"]) do
        connection.delete("DELETE FROM #{table} WHERE account_id = #{account.id} LIMIT #{DEFAULT_BATCH}")
      end
    end

    # Insert/update a row to tell the processing maxwell instance that the deletes
    # it's about to witness are for an account which is no longer on this shard
    # (which may cause them to be ignored). This is performed at the start of each batch.
    def warn_maxwell
      statsd_client.time('warn_maxwell') do
        connection.insert(warn_maxwell_insert_stmt)
      end
    end

    def warn_maxwell_insert_stmt
      @warn_maxwell_stmt ||=
        "INSERT INTO account_move_shard_changes SET created_at = NOW(),              updated_at = NOW(),              account_id = #{account.id},      shard_id = #{maxwell_shard_id}, id = #{account.id}  \
         ON DUPLICATE KEY UPDATE                    created_at = VALUES(created_at), updated_at = VALUES(updated_at), account_id = VALUES(account_id), shard_id = VALUES(shard_id),    id = VALUES(id)"
    end

    def maxwell_shard_id
      job_audit.reason == 'canceled' ? -1 : account.shard_id.to_i
    end

    def remove_spokes
      Subscription.unscoped do
        Subscription.where(hub_account_id: account.id).update_all(hub_account_id: nil)
      end
    end

    def verify_table_parameter(table)
      if (/^[a-zA-Z0-9_]+$/ =~ table) != 0
        raise "Invalid table name specified for delete: '#{table}'"
      end
    end

    def find_tables
      connection.tables.select { |table| has_account_id?(table) && !permanent_tables.include?(table) }
    end

    def has_account_id?(table) # rubocop:disable Naming/PredicateName
      !connection.columns(table).select { |s| s.name == 'account_id' }.empty?
    end

    def relation(table)
      Arel::Table.new(table)
    end

    def connection
      ActiveRecord::Base.connection
    end

    # There is some ridiculous interdependencies here between the state in which BaseDataDeleteJob calls #erase_for_moved
    # and this completely non-intuitive check...
    def account_move_source_shard_id
      # validate the account is not on the current shard
      # validate the account WAS on this shard before
      # return the current shard
      raise "Do not clean up the accounts current shard." if account.shard_id == ActiveRecord::Base.current_shard_id
      ActiveRecord::Base.current_shard_id
    end
  end
end
