module Zendesk::Maintenance::Jobs
  class ChatPhaseMismatchJob < BaseJob
    extend ChatPhaseJobHelper

    class << self
      DATADOG_MISMATCH_INTEGRATIONS = 'zopim_integration.mismatch'.freeze

      def work
        return unless chat_job_enabled?(:chat_phase3_integration_mismatch_job)

        find_mismatched_zopim_integrations
      end

      private

      def find_mismatched_zopim_integrations
        mismatched_zopim_integrations = []
        validate_with_block do |products|
          mismatched_zopim_integrations += validate_zopim_integrations(products)
        end
        log_errors(mismatched_zopim_integrations, DATADOG_MISMATCH_INTEGRATIONS, 'Mismatched zopim integrations for chat phase 3') unless mismatched_zopim_integrations.empty?
        resque_log("ChatPhaseMismatchJob mismatched count: #{mismatched_zopim_integrations.count}")
      end

      def validate_zopim_integrations(products)
        all_zopim_ints = ActiveRecord::Base.on_all_shards { ZopimIntegration.pluck(:account_id) }.flatten!
        cp3_zopim_array = products.map(&:account_id)
        cp3_zopim_ints = cp3_zopim_array & all_zopim_ints

        # create a list of bad accounts by shard
        bad_integrations = [].tap do |bad_integration|
          ActiveRecord::Base.on_all_shards do |shard|
            bad_accounts = ZopimIntegration.where("phase != 3 AND account_id IN (?)", cp3_zopim_ints).pluck(:account_id).to_a
            bad_integration << {shard: shard, accounts: bad_accounts} unless bad_accounts.empty?
          end
        end

        # now go through each account and remove accounts where shards don't match (moved accounts)
        [].tap do |mismatched_accounts|
          bad_integrations.each do |accounts_on_shard|
            valid_accounts = Account.where("id in (?) AND shard_id = ? AND is_serviceable = 1", accounts_on_shard[:accounts], accounts_on_shard[:shard]).pluck(:id).to_a.flatten
            mismatched_accounts.concat(valid_accounts) unless valid_accounts.empty?
          end
        end.uniq
      rescue StandardError => e
        Rails.logger.error "validate_missing_zopim_subscription error: #{e.inspect}"
        []
      end
    end
  end
end
