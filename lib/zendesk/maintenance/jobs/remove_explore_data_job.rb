module Zendesk::Maintenance::Jobs
  class RemoveExploreDataJob < BaseDataDeleteJob
    priority :account_deletion
    stage :before_db

    private

    def erase_for_canceled
      Rails.logger.warn("Deleting explore data for account: #{account.subdomain}")
      Zendesk::Explore::InternalApiClient.new(account.subdomain).account_deleted!
    end
  end
end
