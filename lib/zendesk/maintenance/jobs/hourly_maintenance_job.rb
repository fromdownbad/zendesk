module Zendesk::Maintenance::Jobs
  class HourlyMaintenanceJob < Zendesk::Maintenance::Jobs::BaseJob
    ALL_JOBS = [
      Zendesk::Maintenance::Jobs::APIAccountSignupCleanupJob
    ].freeze

    class << self
      def work(dryrun = false, console = false, time = Time.now)
        ALL_JOBS.each do |job|
          execute_job(job, dryrun, console, time)
        end
      end

      private

      def execute_job(job, dryrun, console, time)
        log_state(job, 'starting')
        job.execute(dryrun, console, time)
        log_state(job, 'finishing')
      rescue StandardError => e
        ZendeskExceptions::Logger.record(e, location: self, message: "Error processing job #{job.name} at #{Time.now}", fingerprint: 'ccba9e115016e5b230f2a7297177011bcd40b9e9')
      end

      def log_state(job, state)
        Rails.logger.info("HourlyMaintenanceJob (pid: #{Process.pid}) #{state} job #{job.name} at #{Time.now}")
      end
    end
  end
end
