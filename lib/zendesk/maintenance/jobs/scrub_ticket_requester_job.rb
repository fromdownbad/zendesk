module Zendesk::Maintenance::Jobs
  class ScrubTicketRequesterJob < PerAccountJob
    class << self
      attr_reader :account
      SEARCH_TICKETS_FROM = ENV.fetch('TICKET_ANONYMIZE_SEARCH_TICKETS_FROM', 48).to_i.hours
      SEARCH_TICKETS_TO = ENV.fetch('TICKET_ANONYMIZE_SEARCH_TICKETS_TO', 12).to_i.hours

      def fetch_accounts
        arturo = Arturo::Feature.find_by_symbol(:ticket_metadata_deletion_at_close)
        accounts = ActiveRecord::Base.on_slave do
          Account.where(subdomain: arturo.external_beta_subdomains).active.serviceable.shard_unlocked
        end
        accounts.find_each do |account|
          yield account
        end
      end

      def work_on_account(account)
        return if account.has_ticket_user_scrubbing_killswitch?

        @account = account
        return unless account.has_delete_ticket_metadata_pii_enabled?
        Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.time('scrubbing_job_time', tags: ["account_id:#{account.id}"]) do
          tickets = ActiveRecord::Base.on_slave do
            Ticket.unscoped.includes(:cia_events).where(status_updated_at: (Time.now - SEARCH_TICKETS_FROM)...(Time.now - SEARCH_TICKETS_TO), status_id: StatusType.CLOSED, account_id: account.id)
          end
          tickets.find_each do |ticket|
            if ticket.cia_events.exists?(action: 'scrub_pii_ticket_close')
              Rails.logger.info "Ticket already anonymized. account_id: #{ticket.account.id}, ticket_id: #{ticket.id}"
              next
            end
            ticket_scrubber = Zendesk::TicketAnonymizer::TicketScrubber.new(ticket: ticket)
            CIA.audit actor: User.system do
              ticket_scrubber.scrub_ticket
            end
          end
        end
      rescue StandardError => error
        statsd_record_scrubbing_process_failed(error)
      end

      def args_to_log
        {}
      end

      private

      def statsd_record_scrubbing_process_failed(error)
        message = "Ticket scrubbing job failed  from account_id #{account.id} due #{error.message}"
        Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.event("ticket scrubbing job failed", message, alert_type: 'warning')
        Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.increment('failed_scrubbing_process', tags: ["error_type:#{error.class}", "account_id:#{account.id}"])
      end
    end
  end
end
