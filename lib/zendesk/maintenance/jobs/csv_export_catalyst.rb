module Zendesk::Maintenance::Jobs
  class CsvExportCatalyst < PerShardJob
    class << self
      def work_on_shard(*args)
        # make existing jobs not fail, delete after 2012-07-22
      end

      def self.args_to_log(*_args)
        { }
      end
    end
  end
end
