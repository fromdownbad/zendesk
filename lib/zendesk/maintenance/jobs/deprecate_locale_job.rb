module Zendesk::Maintenance::Jobs
  # Before deprecating a locale, make sure that the ID is a member of DEPRECATED_IDS
  # in app/models/translation_locale.rb
  #
  # Search the logs for the deprecation warning to make sure accounts are not using
  # the id you are about to deprecate.
  class DeprecateLocaleJob < PerShardJob
    attr_reader :old_locale, :new_locale, :dry_run

    def self.work_on_shard(_shard_id, old_locale_id, new_locale_id, dry_run, account_id = nil)
      deprecate_locale = new(old_locale_id, new_locale_id, dry_run)

      if account_id
        deprecate_locale.work_on_account(account_id)
      else
        deprecate_locale.work_on_all
      end
    end

    def initialize(old_locale_id, new_locale_id, dry_run = true)
      @old_locale = TranslationLocale.find(old_locale_id)
      @new_locale = TranslationLocale.find(new_locale_id)
      @dry_run = dry_run
    end

    def work_on_all
      log "Working on #{account_ids.count} accounts"
      account_ids.each { |id| work_on_account(id) }
    end

    def work_on_account(account_id)
      log "Working on account #{account_id}"
      account = Account.shard_local.shard_unlocked.find(account_id)

      if !@dry_run && account.translation_locale == @old_locale
        account.update_attribute(:translation_locale, @new_locale)
      end

      begin
        ActiveRecord::Base.transaction do
          replace_in_allowed_locales(account)
          replace_in_users(account)
          replace_in_tickets(account)
          replace_in_cms_variants(account)
          replace_in_forums(account)
          replace_in_nps_recipients(account)
          replace_in_rules(account)
          replace_in_reports(account)
        end
      rescue => e
        log_error e, "Account update failed id: #{account_id}\n#{e.message}"
      end

      log "Done with account #{account_id}"
    end

    def replace_in_allowed_locales(account)
      allowed_locales = account.allowed_translation_locales
      if !@dry_run && allowed_locales.include?(@old_locale)
        ActiveRecord::Base.on_shard(nil) { allowed_locales.delete(@old_locale) }
        allowed_locales << @new_locale unless allowed_locales.include?(@new_locale)
      end
    end

    def replace_in_users(account)
      users = account.users.where(locale_id_scope)
      to_update = users.count(:all)
      updated = users.update_all(locale_id: @new_locale.id) unless @dry_run
      raise "User update failed" unless to_update == updated || @dry_run
    end

    def replace_in_tickets(account)
      account.tickets.where(locale_id_scope).each do |ticket|
        original_generated = ticket.generated_timestamp
        ticket.update_column(:locale_id, @new_locale.id) unless @dry_run
        # reset the generated_timestamp so the ticket doesn't appear updated
        ticket.update_column(:generated_timestamp, original_generated) unless @dry_run
      end
    end

    def replace_in_cms_variants(account)
      variants = account.cms_variants.where(translation_locale_id_scope)
      to_update = variants.count(:all)
      updated = variants.update_all(translation_locale_id: @new_locale.id) unless @dry_run
      raise "Variant update failed" unless to_update == updated || @dry_run
    end

    def replace_in_forums(account)
      forums = account.forums.where(translation_locale_id_scope)
      to_update = forums.count(:all)
      updated = forums.update_all(translation_locale_id: @new_locale.id) unless @dry_run
      raise "Forum update failed" unless to_update == updated || @dry_run
    end

    def replace_in_nps_recipients(account)
      recipients = NpsRecipient.where(account_id: account.id).where(locale_id_scope)
      to_update = recipients.count(:all)
      updated = recipients.update_all(locale_id: @new_locale.id) unless @dry_run
      raise "NpsRecipient update failed" unless to_update == updated || @dry_run
    end

    def replace_in_rules(account)
      account.rules.where(RULE_SCOPE).each do |rule|
        change_definition_items(rule.definition)
        rule.save!(validate: false) unless @dry_run
      end
    end

    def replace_in_reports(account)
      account.reports.where(REPORT_SCOPE).each do |report|
        change_report_definition(report.definition)
        report.save!(validate: false) unless @dry_run
      end
    end

    private

    RULE_SCOPE = "definition like '%source: locale_id%'".freeze

    REPORT_SCOPE = "definition like '%locale_id%'".freeze

    def locale_id_scope
      { locale_id: @old_locale.id }
    end

    def translation_locale_id_scope
      { translation_locale_id: @old_locale.id }
    end

    # collect all of the account ids that have content using the old locale
    def account_ids
      ids = [
        Account.where(locale_id_scope).pluck(:id),
        @old_locale.allowed_accounts.pluck(:account_id),
        User.where(locale_id_scope).pluck(:account_id),
        ::Cms::Variant.where(translation_locale_id_scope).pluck(:account_id),
        Forum.where(translation_locale_id_scope).pluck(:account_id),
        Ticket.where(locale_id_scope).pluck(:account_id),
        NpsRecipient.where(locale_id_scope).pluck(:account_id),
        ::Rule.where(RULE_SCOPE).pluck(:account_id),
        Report.where(REPORT_SCOPE).pluck(:account_id)
      ]

      ids.flatten.uniq
    end

    public_class_method def self.log(str)
      resque_log(str)
    end

    def log(str)
      self.class.log(str)
    end

    def log_error(e, str)
      ZendeskExceptions::Logger.record(e, location: self, message: str, fingerprint: '91c72b558429bd2b410bca42ab8a0ce32b5dbddf')
      Rails.logger.error(str)
    end

    def change_definition_items(definition)
      Definition::DEFINITION_ITEM_SETS.each do |item|
        definition_items = definition.send(item)
        change_definition_item_locale(definition_items)
      end
    end

    def change_definition_item_locale(items)
      items.each do |item|
        next unless item.source == "locale_id"
        if item.value == @old_locale.id
          item.value = @new_locale.id unless @dry_run
        elsif item.value.is_a?(Array) && item.value[0] == @old_locale.id.to_s
          item.value[0] = @new_locale.id.to_s unless @dry_run
        end
      end
    end

    def change_report_definition(definition)
      definition.each do |def_item|
        change_report_definition_data(def_item)
      end
    end

    def change_report_definition_data(definition_item)
      data = definition_item[:data]
      data.each do |condition|
        if condition[0] == "locale_id" && condition[2] == @old_locale.id.to_s
          condition[2] = @new_locale.id.to_s unless @dry_run
        end
      end
    end
  end
end
