module Zendesk::Maintenance::Jobs
  class AccountRiskAssessmentJob < PerShardJob
    extend Fraud::FraudScoreActions
    include ZendeskBillingCore::Zuora::Session

    SAFE_ACCOUNT_AGE = 32

    QUERY_AGE_THRESHOLD = 90

    SETTING_CONDITION = ['created_at > ? AND name = ? AND value LIKE ?',
                         QUERY_AGE_THRESHOLD.days.ago, 'risk_assessment', '%risky%'].freeze

    RESELLER_CUSTOMER = 'Reseller Customer'.freeze

    UPDATE_ASSESSMENT_ACTION = {
      Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB_SAFE_AGE => :mark_risk_assessment_as_safe_age,
      Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB_SAFE_PAID => :mark_risk_assessment_as_safe_paid
    }.freeze

    class << self
      attr_reader :current_account

      def work_on_shard(shard_id)
        Rails.logger.info "Starting AccountRiskAssessmentJob for #{shard_id}"

        ActiveRecord::Base.on_shard(shard_id) do
          AccountSetting.where(SETTING_CONDITION).find_each do |setting|
            @current_account = setting.account
            next unless @current_account.is_serviceable

            if account_paid_invoice? || account_from_reseller?
              update_risk_assessment_to_safe(Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB_SAFE_PAID)
            elsif @current_account.created_at < SAFE_ACCOUNT_AGE.days.ago
              update_risk_assessment_to_safe(Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB_SAFE_AGE)
            end
          end
        end
      end

      private

      def update_risk_assessment_to_safe(source_event)
        if @current_account.has_fraud_score_job_risk_assessment_daily_job?
          FraudScoreJob.enqueue_account(@current_account, source_event)
        else
          perform_fraud_actions({UPDATE_ASSESSMENT_ACTION[source_event] => true}, source_event)
        end
      end

      def account_paid_invoice?
        return false unless zuora_accounts.size == 1 # multiple invoices may cause inconsistencies

        invoices = zuora_invoices(zuora_accounts.first)
        invoices.any? do |invoice|
          if @current_account && invoice.balance.to_i != 0
            Rails.logger.info "AccountRiskAssessmentJob unpaid invoice balance: #{invoice.balance.to_i} for #{@current_account.id}"
          end
          invoice.balance.to_i == 0
        end
      rescue => e
        Rails.logger.error "AccountRiskAssessmentJob invoice error #{e} for #{@current_account.id}"
        statsd_client.increment("invoice_error")
        false
      end

      def account_from_reseller?
        zuora_accounts.first.try(:account_type__c) == RESELLER_CUSTOMER
      rescue => e
        Rails.logger.error "AccountRiskAssessmentJob reseller error #{e} for #{@current_account.id}"
        statsd_client.increment("account_type_error")
        false
      end

      def zuora_invoices(zuora_account)
        session.invoice.find(AccountId: zuora_account.id)
      end

      def zuora_accounts
        session.account.find(zendesk_account_id__c: @current_account.id)
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['risk_assessment_job'])
      end
    end
  end
end
