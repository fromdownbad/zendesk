module Zendesk::Maintenance::Jobs
  class ClearInactiveDeviceTokensJob < PodMaintenanceJob
    def execute
      ['com.zendesk.agent', 'com.zendesk.inbox'].each do |mobile_app_identifier|
        mobile_app_config = Zendesk::Configuration.dig(:urban_airship, mobile_app_identifier)
        next unless mobile_app_config

        client = PushNotifications::UrbanAirshipClient.new(
          mobile_app_config["app_key"], mobile_app_config["master_secret"]
        )

        response = client.tokens_deactivated_since(2.days.ago)

        if response.respond_to?(:parsed_response) &&
            response.parsed_response.is_a?(Hash) &&
            response["error"] == "Unauthorized"
          # As UrbanAirShip is currently not used, incrementing DataDog counter
          # and returning early
          statsd_client.increment(
            "failure", tags: ["exception:mobile_sdk.urban_airship.unauthorized"]
          )
          return
        end

        inactive_tokens = response.map { |t| t["device_token"] }

        next unless inactive_tokens && !inactive_tokens.empty?
        ActiveRecord::Base.on_all_shards do
          PushNotifications::DeviceIdentifier.delete_all(token: inactive_tokens)
        end
      end
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["urban_airship"])
    end
  end
end
