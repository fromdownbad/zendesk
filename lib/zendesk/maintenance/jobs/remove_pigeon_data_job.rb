require 'pigeon_client'
module Zendesk::Maintenance::Jobs
  class RemovePigeonDataJob < BaseDataDeleteJob
    priority :account_deletion
    stage :before_db

    private

    def erase_for_moved
      return if pod_local_move? || dry_run?
      delete_account
    end

    def erase_for_canceled
      return if dry_run?
      delete_account
    end

    def delete_account
      loop do
        raise KillSwitchEnabled if kill_switch_flipped?
        response = client.delete_account(account.id)
        deleted_items = response.body['count']
        Rails.logger.debug("Deleted #{deleted_items} for account #{account.id}")
        break unless deleted_items > 0
      end
    end

    def client
      @client ||= ::Pigeon::Client.new(
        url: Zendesk::Configuration.dig!(:pigeon, :url),
        timeout: 60
      )
    end
  end
end
