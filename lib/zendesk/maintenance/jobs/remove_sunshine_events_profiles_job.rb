module Zendesk::Maintenance::Jobs
  # Sends DELETE request to Sunshine Account Config which takes care of account deletion
  # for Sunshine data. Achieves this by repeatedly invoking the DELETE endpoint until a
  # termination condition is received.
  class RemoveSunshineEventsProfilesJob < BaseDataDeleteJob
    WAIT_IN_SECONDS_FOR_RETRIES = 60.0

    # account deletion request statuses
    DELETION_COMPLETE_STATUS = "COMPLETED".freeze
    DELETION_FAILED_STATUS = "FAILED".freeze

    # account deletion reasons
    DELETION_REASON_CANCELED = "CANCELED".freeze

    class DataDeletionError < StandardError
    end

    priority :account_deletion

    stage :independent

    def erase_for_canceled
      delete_sunshine_events_and_profiles DELETION_REASON_CANCELED
    end

    def verify_for_canceled
      verify_sunshine_events_and_profiles_deletion
    end

    # Noop. Account moves is not implemented for Sunshine Events and Profiles yet, hence data should not be deleted
    def erase_for_moved
    end

    # Noop. Account moves is not implemented for Sunshine Events and Profiles yet, hence data should not be deleted
    def verify_for_moved
    end

    private

    def delete_sunshine_events_and_profiles(reason)
      raise KillSwitchEnabled if kill_switch_flipped?
      return false if dry_run?
      return false unless Arturo.feature_enabled_for?(:sunshine_event_and_profiles_account_deletion, account)

      stats_client.increment('started', tags: %W[account_id:#{account.id} audit_id:#{job_audit.id}])

      loop do
        status = client.delete_account reason

        case status
        when DELETION_COMPLETE_STATUS
          break
        when DELETION_FAILED_STATUS
          raise DataDeletionError
        else
          Kernel.sleep(WAIT_IN_SECONDS_FOR_RETRIES)
        end
      end

      stats_client.increment('completed', tags: %W[account_id:#{account.id} audit_id:#{job_audit.id}])

      @done = true
    end

    def verify_sunshine_events_and_profiles_deletion
      return unless Arturo.feature_enabled_for?(:sunshine_event_and_profiles_account_deletion, account)

      raise DataDeletionError unless @done
    end

    def client
      @client ||= Zendesk::Sunshine::AccountConfigClient.new(account: account)
    end

    def stats_client
      @stats_client ||= Zendesk::StatsD::Client.new(
        namespace: %w[jobs remove_sunshine_events_profiles_job]
      )
    end
  end
end
