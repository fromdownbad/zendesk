module Zendesk::Maintenance::Jobs
  class TicketDeletionJob < JobWithStatus
    include Zendesk::Maintenance::Jobs::ScrubJobHelper

    APM_SERVICE_NAME = "ticket_deletion_job".freeze

    priority :immediate

    def work
      current_account.on_shard do
        success = log_deletion_metrics(current_account, tickets_ids) do
          process_ticket_ids(current_account, tickets_ids)
        end

        completed(results: { success: success })
      end
    end

    private

    def log_deletion_metrics(account, tickets_ids)
      Rails.logger.info("Scrubbing tickets_ids: #{tickets_ids}, account:#{current_account.subdomain}")

      ZendeskAPM.trace(
        'ticket_deletion_job.work',
        service: Zendesk::Maintenance::Jobs::TicketDeletionJob::APM_SERVICE_NAME
      ) do |span|
        span.set_tag(Datadog::Ext::Analytics::TAG_ENABLED, true)
        span.set_tag("zendesk.account_id",              account.id)
        span.set_tag("zendesk.account_subdomain",       account.subdomain)
        span.set_tag("zendesk.subpoena",                account.has_prevent_deletion_if_churned?)
        span.set_tag("ticket_deletion_job.tickets_ids", tickets_ids)

        yield
      end
    end

    def tickets_ids
      options[:tickets_ids]
    end
  end
end
