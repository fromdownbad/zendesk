module Zendesk::Maintenance::Jobs
  class RemoveReferenceParticipantDataJob < BaseDataDeletionParticipantJob
    consul_service_name 'reference-participant'
    service_port '8080'

    private

    def erase_for_moved
      return super if !Rails.env.production? && Arturo.feature_enabled_for_pod?(:data_deletion_endpoints_ready, Zendesk::Configuration.fetch(:pod_id))
    end

    def erase_for_canceled
      return super if !Rails.env.production? && Arturo.feature_enabled_for_pod?(:data_deletion_endpoints_ready, Zendesk::Configuration.fetch(:pod_id))
    end
  end
end
