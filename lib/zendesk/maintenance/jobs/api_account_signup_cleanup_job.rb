module Zendesk::Maintenance::Jobs
  class APIAccountSignupCleanupJob < PodMaintenanceJob
    # This job looks for accounts that were created more than 24 hours ago and,
    # if the owner has a password set but has not verified the email address,
    # clears the password.
    def execute
      ActiveRecord::Base.on_all_shards do
        owner_ids = recent_account_owner_ids(
          [
            Zendesk::Accounts::Source::BLACKBERRY,
            Zendesk::Accounts::Source::IPHONE,
            Zendesk::Accounts::Source::IPAD,
            Zendesk::Accounts::Source::ANDROID
          ]
        )

        user_ids = ids_of_users_to_clean_up(owner_ids)
        if user_ids.any?
          execute_cleanup!(user_ids)
        else
          logger.info("No users to clean up.")
        end
      end
    end

    # @param [Array[Integer]] ids the IDs of the users whose passwords should be wiped
    def execute_cleanup!(ids)
      User.find(ids).each do |u|
        if dryrun?
          logger.info("Would have removed password for #{u} (##{u.id})")
        else
          u.update_attribute(:crypted_password, nil)
          logger.info("Removed password for #{u} (##{u.id})")
        end
      end
    end

    def recent_account_owner_ids(sources)
      accounts = Account.joins(:subscription).shard_local.shard_unlocked.where(is_active: true, created_at: 2.days.ago...1.day.ago).to_a
      accounts.select { |a| sources.include?(a.source) }.map(&:owner_id)
    end

    # @param [Array[String]] sources the account-creation sources to clean up
    # @return [Array] an Array[Integer] of IDs of users who have a password and have not verified
    #                 their email address and created their account in the last 24 hours and
    #                 who crated their account via one of +sources+
    def ids_of_users_to_clean_up(owner_ids)
      User.find_by_sql(["
        SELECT DISTINCT(ui.user_id) as id
          FROM user_identities ui, users u
         WHERE ui.user_id    in (?)
           AND ui.is_verified = FALSE
           AND u.is_active    = TRUE
           AND u.id           = ui.user_id
           AND u.crypted_password IS NOT NULL
           AND ui.type = 'UserEmailIdentity'
      ", owner_ids]).map(&:id)
    end
  end
end
