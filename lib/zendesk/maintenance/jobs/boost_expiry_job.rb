module Zendesk::Maintenance::Jobs
  # Scans accounts and sends boost expiry notification
  #
  # FIXME: this job needs to
  class BoostExpiryJob < BaseMaintenanceJob
    def execute
      ActiveRecord::Base.on_all_shards do
        send_boost_expiry_mails(5)
        send_boost_expiry_mails(1)
        inactivate_expired_boosts
        inactivate_expired_addon_boosts
      end
    end

    def send_boost_expiry_mails(days)
      features = FeatureBoost.where(["DATE_SUB(DATE(valid_until), INTERVAL ? DAY) = UTC_DATE()", days]).to_a
      accounts = Account.shard_local.where(id: features.map(&:account_id).uniq)

      accounts_already_processed = []

      accounts.each do |account|
        if account.subscription.plan_type >= account.feature_boost.boost_level
          account.feature_boost.is_active = false
          Zendesk::SupportAccounts::Product.retrieve(account).update_plan_settings!(account.feature_boost)
        else
          logger.info("Sending #{days} boost expiry notice to account #{account.subdomain}/#{account.id}")
          unless dryrun?
            BoostMailer.deliver_boost_expiry(account, days)
          end
          accounts_already_processed << account.id
        end
      end

      logger.info("Processed #{accounts.length} boost #{days} day expiry notices")

      # TODO: See discussion at https://zendesk.atlassian.net/browse/FEAT-113
      # send_addon_boost_expiry_mails(days, accounts_already_processed)
    end

    def send_addon_boost_expiry_mails(days, accounts_already_processed)
      addons      = SubscriptionFeatureAddon.boosted.where(["DATE_SUB(DATE(boost_expires_at), INTERVAL ? DAY) = UTC_DATE()", days]).to_a
      account_ids = addons.map(&:account_id).uniq - accounts_already_processed
      accounts    = Account.shard_local.where(id: account_ids)

      accounts.each do |account|
        logger.info("Sending #{days} add-on(s) boost expiry notice to account #{account.subdomain}/#{account.id}")
        BoostMailer.deliver_boost_expiry(account, days) unless dryrun?
      end

      logger.info("Processed #{accounts.length} add-on(s) boost #{days} day expiry notices")
    end

    def inactivate_expired_boosts
      accounts = []

      FeatureBoost.where('is_active = 1 AND DATE(valid_until) < UTC_DATE()').find_each_for_shard do |feature_boost|
        account = feature_boost.account
        logger.info("Inactivating boost for account #{account.subdomain}/#{account.id}")
        account.feature_boost.is_active = false
        Zendesk::SupportAccounts::Product.retrieve(account).update_plan_settings!(account.feature_boost)

        accounts << account
      end

      accounts.each do |account|
        normalize(account)
      end
    end

    def inactivate_expired_addon_boosts
      account_ids = Set.new

      SubscriptionFeatureAddon.boosted.expired.find_each_for_shard do |expired_addon|
        account_ids << expired_addon.account.id

        logger.info("Destroying boosted add-on (feature: #{expired_addon.name}) " \
          "for account #{expired_addon.account.subdomain}/#{expired_addon.account.id}")

        expired_addon.destroy unless dryrun?
      end

      logger.info("Processed #{account_ids.length} for destroying boosted add-ons")
    end

    def normalize(account)
      unless account.subscription.has_community_forums?
        if account.forums.exists?("display_type_id != #{ForumDisplayType::ARTICLES.id}")
          logger.info("Account #{account.subdomain}/#{account.id} will have forums display type changed")
        end
      end

      unless account.subscription.has_categorized_forums?
        if account.categories.any?
          logger.info("Account #{account.subdomain}/#{account.id} will have categories destroyed")
        end
      end

      unless account.subscription.has_host_mapping?
        if account.host_mapping.present?
          logger.info("Account #{account.subdomain}/#{account.id} will have host_mapping reset")
        end
      end

      unless account.has_time_zone_selection?
        logger.info("Account #{account.subdomain}/#{account.id} will enqueue TZ reset")
      end

      unless account.subscription.has_group_rules?
        rules = account.all_rules.where(owner_type: 'Group')
        if rules.any?
          logger.info("Account #{account.subdomain}/#{account.id} will change owner on #{rules.size} rules")
        end
      end

      unless account.subscription.has_customer_satisfaction?
        logger.info("Account #{account.subdomain}/#{account.id} will have customer satisfaction rating")
      end
    end
  end
end
