module Zendesk::Maintenance::Jobs
  class BacklogJob < PerAccountJob
    class << self
      def work_on_account(account)
        # don't need ticket_stats for expired accounts
        return unless account.is_active && account.is_serviceable

        time_stamp = Time.now.to_i
        time_stamp -= (time_stamp % 1.hour) # hour boundary

        # for Zendesk::Stats::TicketBacklog
        RollupBacklogJob.enqueue(account.id, 1.hour.to_i, Time.at(time_stamp).utc)
      end

      def args_to_log
        {}
      end
    end
  end
end
