module Zendesk::Maintenance::Jobs
  class RemoveCollaborationDataJob < BaseDataDeleteJob
    priority :account_deletion
    stage :before_db

    private

    def erase_for_moved
      return if disabled_via_arturo? || pod_local_move? || dry_run?
      delete_account
    end

    def erase_for_canceled
      return if disabled_via_arturo? || dry_run?
      delete_account
    end

    def delete_account
      path = "/accounts/#{account.id}"
      loop do
        raise KillSwitchEnabled if kill_switch_flipped?
        response = client.delete(path)
        path = response.body['next_page']
        deleted_items = response.body['records_processed']
        Rails.logger.debug("Deleted #{deleted_items} for account #{account.id}")
        break if response.body['end_of_stream']
        sleep response.headers['Backoff'].to_f
      end
    end

    def client
      @client ||= Kragle.new('kragle-collaboration-data') do |connection|
        connection.url_prefix = url_prefix
        connection.headers['X-Zendesk-Account-Id'] = account.id.to_s
      end
    end

    def url_prefix
      @url_prefix ||= ENV.fetch('COLLABORATION_DATA_URL') do
        "http://pod-#{ENV.fetch('ZENDESK_POD_ID')}.collaboration-data.service.consul:3000"
      end
    end

    def disabled_via_arturo?
      !Arturo.feature_enabled_for?(:exodus_collaboration_deletion, account)
    end
  end
end
