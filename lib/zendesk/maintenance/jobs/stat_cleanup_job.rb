module Zendesk::Maintenance::Jobs
  class StatCleanupJob < BaseJob
    class << self
      def work
        Zendesk::Stats::StatRollup.cleanup_all(Time.zone.now)
      end

      def args_to_log
        { job: name.demodulize, time: Time.now.to_s }
      end
    end
  end
end
