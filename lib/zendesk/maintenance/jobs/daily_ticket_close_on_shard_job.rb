module Zendesk::Maintenance::Jobs
  class DailyTicketCloseOnShardJob < PerShardJob
    BATCH_SIZE = 500
    MAX_RUNS = 50

    priority :low

    class << self
      def work_on_shard(shard_id)
        ActiveRecord::Base.on_shard(shard_id) do
          fetch_accounts(shard_id) do |account|
            max_records = account.settings.max_ticket_batch_close_count
            solve_before_date = account.settings.close_after_days.days.ago

            Time.use_zone(account.time_zone) do
              args = {
                max_records: max_records,
                batch_size: BATCH_SIZE,
                solved_before: solve_before_date
              }
              total = 0
              runs  = 0

              time = Benchmark.realtime do
                while runs < MAX_RUNS
                  count = Zendesk::Maintenance::Util::BatchClose.close_for_account!(account, args)
                  runs  += 1
                  total += count
                  resque_log("Closed #{count} tickets for #{account.subdomain}") if count > BATCH_SIZE

                  # Nothing more to come for
                  break if count != max_records
                end
              end

              resque_log("Closed #{total} tickets for #{account.subdomain} in #{"%.4s" % time}s") if time > 5 || total > max_records
            end
          end
        end
      rescue StandardError => e
        resque_error("Error during batch close: #{e.message}")
        ZendeskExceptions::Logger.record(e, location: self, message: "Error during batch close: #{e.message}", fingerprint: 'f620494e37429c49acba5bc5fa58ee75a31fc78a')
      end

      def fetch_accounts(shard_id)
        Account.joins(:subscription).active.serviceable.shard_unlocked.where(shard_id: shard_id).find_each do |account|
          yield account
        end
      end
    end
  end
end
