module Zendesk::Maintenance::Jobs
  class ApiActivityDeleteJob < PerShardJob
    BATCH_MINIMUM_SLEEP = 1.second
    BATCH_MAXIMUM_SLEEP = 5.minutes

    class << self
      def work_on_shard(shard_id)
        ActiveRecord::Base.on_shard(shard_id) do
          hours_to_store = 48
          current_time = Time.now
          previous_hour = current_time.to_i / 1.hour - 1
          delete_before_this_hour = Time.at((previous_hour - hours_to_store) * 1.hour)
          ApiActivityClient.select(:id).where("last_request_time < ?", delete_before_this_hour).find_in_batches do |clients_to_delete_ids|
            ApiActivityClient.where(id: clients_to_delete_ids).delete_all
            resque_log("ApiActivityClient.remove old clients: delete ids count: #{clients_to_delete_ids.count} on shard #{shard_id}")
            sleep([BATCH_MAXIMUM_SLEEP, database_backoff.backoff_duration].min)
          end
        end
      end

      def database_backoff
        @database_backoff ||= Zendesk::DatabaseBackoff.new(minimum: BATCH_MINIMUM_SLEEP)
      end
    end
  end
end
