class NoProductRecords < StandardError; end

class Zendesk::Maintenance::Jobs::AccountPruningJob < Zendesk::Maintenance::Jobs::BaseJob
  priority :account_deletion

  DEFAULT_ACCOUNT_RETENTION = 90.days
  TRIAL_ACCOUNT_RETENTION = 9.months
  HARD_DELETION_DELAY = 30.days
  MULTIPRODUCT_SOFT_DELETE_RETENTION = 180.days

  def self.work
    job = new
    job.cancel_expired_trials
    job.suspend_multiproduct_accounts_with_no_active_products if multiproduct_pruning_job_enabled?
    job.cancel_suspended_multiproduct_trial_accounts if multiproduct_pruning_job_enabled?
    job.soft_delete_cancelled_accounts
    job.soft_delete_cancelled_multiproduct_accounts if multiproduct_pruning_job_enabled?
    job.soft_delete_old_sandbox_accounts
    job.hard_delete_soft_deleted_accounts
  end

  def self.multiproduct_pruning_job_enabled?
    feature = Arturo::Feature.find_feature(:voltron_multiproduct_pruning_job)
    feature.try(:phase) == 'on' || feature.try(:deployment_percentage) == 100
  end

  attr_accessor :dry_run

  def initialize(dry_run: false)
    @dry_run = dry_run
  end

  # What about non-trial accounts? They can be cancelled by:
  #
  # 1. By letting payment lapse after the grace and suspension period
  # 2. Explicitly by the customer in the Admin UI
  # 3. Explicitly by a Zendesk employee in Monitor UI
  #
  # See: https://zendesk.atlassian.net/wiki/display/Prod/Dunning+Process+and+Suspended+Accounts

  # VOLT-1626: Skip Shell accounts (created via Account Service, aka Pravda) that do not have a Support product
  # These accounts will not have a subscription record, hence the inner joins to exclude these accounts from this job
  def cancel_expired_trials(retention = TRIAL_ACCOUNT_RETENTION)
    accounts = Account.joins(:subscription).pod_local.where(multiproduct: false).expired_for(retention - DEFAULT_ACCOUNT_RETENTION).to_a
    accounts.select! { |account| account.subscription.account_type == "trial" }
    accounts.reject!(&:has_prevent_deletion_if_churned?)

    accounts.each do |account|
      account.on_shard do
        begin
          if account.has_sell_skip_trial_cancellation? && account.sell_product.try(:active?)
            Rails.logger.warn "cancel_expired_trials skipping #{account.idsub} as it is not multiproduct and has active Sell"
            next
          else
            Rails.logger.info "cancel_expired_trials #{account.idsub}"
            account.cancel! unless dry_run
          end
        rescue StandardError => e
          Rails.logger.error "cancel_expired_trials #{account.idsub} failed: #{e.inspect}"
          ZendeskExceptions::Logger.record(e, location: self, message: "cancel_expired_trials #{account.idsub} failed", fingerprint: '131f1994686d02e66c94edee6b19d3d0ca9ec5f2')
        end
      end
    end
  end

  def suspend_multiproduct_accounts_with_no_active_products
    # Only suspend multiproduct trial accounts without active products. Subscribed accounts are suspended by billing.
    Account.pod_local.where(multiproduct: true, is_active: true, is_serviceable: true, billing_id: nil).find_in_batches do |accounts|
      accounts.each do |account|
        account.on_shard do
          begin
            next if account.active_products?
            Rails.logger.info "suspending multiproduct trial account #{account.idsub}"
            account.is_serviceable = false
            Zendesk::SupportAccounts::Product.retrieve(account).save(validate: false)
          rescue StandardError => e
            Rails.logger.error "suspending multiproduct trial account #{account.idsub} failed: #{e.inspect}"
            statsd_client.increment("execute.fail", tags: ["task:suspend_multiproduct_accounts_with_no_active_products", "exception:#{e.class.name.underscore}"])
            ZendeskExceptions::Logger.record(e, location: self, message: "suspend_multiproduct_accounts #{account.idsub} failed", fingerprint: '3169d01bb504b77693729c4ecf31aa')
          end
        end
      end
    end
  end

  def cancel_suspended_multiproduct_trial_accounts(retention = DEFAULT_ACCOUNT_RETENTION)
    # Only cancel suspended multiproduct trial accounts. Subscribed accounts are cancelled by billing.
    Account.pod_local.where(multiproduct: true, is_active: true, is_serviceable: false, billing_id: nil).find_in_batches do |accounts|
      accounts.reject!(&:has_prevent_deletion_if_churned?)

      accounts.each do |account|
        account.on_shard do
          begin
            next if inside_retention_period?(account.products, retention)

            Rails.logger.info "cancel suspended multiproduct account #{account.idsub}"
            account.cancel! unless dry_run
          rescue StandardError => e
            Rails.logger.error "cancel suspended multiproduct account #{account.idsub} failed: #{e.inspect}"
            statsd_client.increment("execute.fail", tags: ["task:cancel_suspended_multiproduct_trial_accounts", "exception:#{e.class.name.underscore}"])
            ZendeskExceptions::Logger.record(e, location: self, message: "cancel_multiproduct_account #{account.idsub} failed", fingerprint: '4d59e3627f6afdb36688d554a5b61d9bdad5e2')
          end
        end
      end
    end
  end

  def soft_delete_cancelled_accounts(retention = DEFAULT_ACCOUNT_RETENTION)
    accounts = Account.joins(:subscription).pod_local.exclude_sandbox.where(multiproduct: false).canceled_for(retention).to_a
    accounts.reject!(&:has_prevent_deletion_if_churned?)

    CIA.audit actor: User.system do
      accounts.each do |account|
        sandboxes(account).each do |sandbox|
          soft_delete_account(sandbox)
        end
        soft_delete_account(account)
      end
    end
  end

  def soft_delete_cancelled_multiproduct_accounts(retention = MULTIPRODUCT_SOFT_DELETE_RETENTION)
    Account.pod_local.where(multiproduct: true, is_active: false, is_serviceable: false).find_in_batches do |accounts|
      accounts.reject!(&:has_prevent_deletion_if_churned?)

      CIA.audit actor: User.system do
        accounts.each do |account|
          begin
            next if inside_retention_period?(account.products, retention)
          rescue NoProductRecords
            skip_account = timestamp_inside_retention_period?(account.updated_at, retention)
            msg_details = if skip_account
              "Skipping account '#{account.idsub}' due to account.updated_at #{account.updated_at} within retention period."
            else
              "Soft deleting account '#{account.idsub}' due to account.updated_at #{account.updated_at} outside of retention period."
            end
            Rails.logger.info "No products found for multiproduct account '#{account.idsub}' during soft_deletion. #{msg_details}"

            next if skip_account
          rescue StandardError => e
            Rails.logger.warn "Failure for multiproduct account '#{account.idsub}' during soft_deletion: #{e.message}"
            statsd_client.increment("execute.fail", tags: ["task:soft_delete_cancelled_multiproduct_accounts", "exception:#{e.class.name.underscore}"])
            next
          end

          sandboxes(account).each do |sandbox|
            soft_delete_account(sandbox)
          end

          if account.active_products?
            Rails.logger.warn "AccountPruningJob#soft_delete_cancelled_multiproduct_accounts: Account #{account.idsub} has active products: #{account.products}"
          end
          soft_delete_account(account)
        end
      end
    end
  end

  def soft_delete_old_sandbox_accounts(retention = DEFAULT_ACCOUNT_RETENTION)
    Account.pod_local.where(is_active: false, is_serviceable: false).
      where.not(sandbox_master_id: nil).
      where("updated_at < ? ", retention.ago).
      find_in_batches do |accounts|
      accounts.reject!(&:has_prevent_deletion_if_churned?)

      CIA.audit actor: User.system do
        accounts.each do |account|
          soft_delete_account(account)
        end
      end
    end
  end

  def soft_delete_account(account)
    Rails.logger.info "soft_delete_account #{account.idsub}"

    return if dry_run

    account.on_shard do
      # We're deleting it. We don't care if a new validation error was introduced.
      Zendesk::SupportAccounts::Product.retrieve(account).soft_delete!
    end
  rescue StandardError => e
    if account.multiproduct?
      statsd_client.increment("execute.fail", tags: ["task:soft_delete_cancelled_multiproduct_accounts", "exception:#{e.class.name.underscore}"])
    end
    Rails.logger.error "soft_delete_account #{account.idsub} failed: #{e.inspect}"
    ZendeskExceptions::Logger.record(e, location: self, message: "soft_delete_account #{account.idsub} failed", fingerprint: 'dba75f3939f0b2e59cb1e684dd1331da8e1d8860')
  end

  def sandboxes(account)
    # Account#sandbox no longer works when the sandbox has been soft deleted.
    # but Account#sandbox_master still works fine.
    Account.where(sandbox_master_id: account.id)
  end

  def hard_delete_soft_deleted_accounts(duration = HARD_DELETION_DELAY)
    accounts = Account.with_deleted do
      # Note: `deleted_for` must come _first_ due to its `with_deleted` blasting the scope
      Account.deleted_for(duration).waiting_hard_deletion.pod_local.includes(:data_deletion_audits).to_a
    end

    accounts.each do |account|
      hard_delete_account(account)
    end
  end

  def hard_delete_account(account)
    # DataDeletionAudit records are created during soft deletion with status = "waiting".
    # This will mark the deletion as ready to be started by the DataDeletion::Runner
    account.data_deletion_audits.waiting.for_cancellation.each do |audit|
      begin
        Rails.logger.info "hard_delete_account #{account.idsub}"
        audit.enqueue! unless dry_run
      rescue StandardError => e
        Rails.logger.error "hard_delete_account #{account.idsub} failed: #{e.inspect}"
        ZendeskExceptions::Logger.record(e, location: self, message: "hard_delete_account #{account.idsub} failed", fingerprint: '28432a60d729a696b28c2c5686511815360dbb85')
      end
    end
  end

  private

  def inside_retention_period?(products, retention)
    raise NoProductRecords if products.empty?
    timestamp_inside_retention_period?(products.map(&:state_updated_at).max, retention)
  end

  def timestamp_inside_retention_period?(timestamp, retention)
    timestamp >= retention.ago
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'account_pruning_job')
  end
end
