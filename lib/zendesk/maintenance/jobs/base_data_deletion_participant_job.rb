module Zendesk::Maintenance::Jobs
  class BaseDataDeletionParticipantJob < BaseDataDeleteJob
    MAX_RETRIES = 3
    InvalidServicePort = Class.new(StandardError)

    class << self
      attr_reader :service_name

      def inherited(subclass)
        subclass.instance_variable_set(:@queue, :account_deletion)
      end

      def job_name
        name.demodulize.underscore
      end

      def stage
        :independent
      end

      def consul_service_name(service_name)
        @service_name = service_name.to_s
      end

      def service_port(port)
        raise InvalidServicePort unless (1..65535).cover? port.to_i

        @service_port = port.to_s
      end

      def consul_service_address
        "http://pod-#{ENV.fetch('ZENDESK_POD_ID')}.#{@service_name}.service.consul:#{@service_port}"
      end
    end

    private

    def erase_for_moved
      return if skip_on_intrapod_move? && pod_local_move?
      delete_account(__method__)
    end

    def erase_for_canceled
      raise KillSwitchEnabled if kill_switch_flipped?
      delete_account(__method__)
    end

    def delete_account(method_name)
      Rails.logger.info("#{job_id_for_log} Starting #{method_name} for account: #{account.id} (#{account.subdomain}")
      loop do
        response = make_request(method_name)
        if response.body['done']
          Rails.logger.info("#{job_id_for_log} finished #{method_name} for account: #{account.id} (#{account.subdomain}")
          statsd_client.increment('participant_job_completed', tags: %W[job:#{self.class.job_name}])
          return
        end
        # backoff response is optional
        backoff = response.body['backoff'] || 0.0
        if backoff > 0.0
          Rails.logger.info("#{job_id_for_log} Sleeping for #{backoff} seconds")
          sleep backoff
        end
      end
    end

    def make_request(method_name)
      api_client(method_name).post do |req|
        req.headers['Content-Type'] = 'application/json'
      end
    end

    def api_client(method_name)
      retry_options = {
        max: MAX_RETRIES
      }

      # Using custom ADD client(clone of Kragle), but with different system user.
      @api_client ||= Zendesk::DataDeletion::ParticipantClient.client(self.class.service_name, retry_options: retry_options) do |connection|
        connection.url_prefix = url_endpoint(method_name)
      end
    end

    def url_endpoint(method_name)
      "#{self.class.consul_service_address}/z/#{method_name}?account_id=#{account.id}"
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'data_deletion')
    end

    # default behavior for all participant jobs is to skip deletion if move is intra pod
    def skip_on_intrapod_move?
      true
    end
  end
end
