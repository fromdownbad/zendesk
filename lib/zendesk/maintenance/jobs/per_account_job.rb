module Zendesk::Maintenance::Jobs
  class PerAccountJob < BaseJob
    class << self
      # Override if you want to generate accounts some other way
      def fetch_accounts
        # Default: skip non-serviceable (e.g. expired trial) accounts
        # Default: skip shell accounts that don't have Support product (Accounts created via Account Service aka Pravda
        # that do not have a subscription record).
        Account.joins(:subscription).active.serviceable.shard_unlocked.find_each do |account|
          yield account
        end
      end

      def work
        Account.where(shard_id: ActiveRecord::Base.shard_names).scoping do
          fetch_accounts do |account|
            account.on_shard do
              work_on_account(account)
            end
          end
        end
      end

      def work_on_account(_account)
        raise "Must be implemented by subclass"
      end

      def args_to_log
        raise "Must be implemented by subclass!"
      end
    end
  end
end
