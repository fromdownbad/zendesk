module Zendesk::Maintenance::Jobs
  class CertificateMaintenanceJob < PodMaintenanceJob
    def execute
      ActiveRecord::Base.on_all_shards do
        notify_about_certificate_expiration
        notify_about_certificate_replacement
        replace_with_autoprovisioned_certificate
      end
    end

    private

    def notify_about_certificate_expiration
      Certificate.customer_provisioned.expiring(1.month.from_now.utc.to_date).find_each_for_shard do |c|
        revoke_certificate?(c) ? revoke_certificate!(c) : deliver_one_month_warning(c)
      end
    end

    def deliver_one_month_warning(certificate)
      Rails.logger.info("Sent expiration warning for certificate: #{certificate.id} with expiration date #{certificate.valid_until}")
      SecurityMailer.deliver_certificate_expiration_warning(certificate)
    end

    def notify_about_certificate_replacement
      Certificate.customer_provisioned.expiring(Certificate::EXPIRE_WARNING_DAYS.from_now.utc.to_date).find_each_for_shard do |c|
        revoke_certificate?(c) ? revoke_certificate!(c) : deliver_replacement_warning(c)
      end
    end

    def deliver_replacement_warning(certificate)
      Rails.logger.info("Sent replacement warning for certificate: #{certificate.id} with expiration date #{certificate.valid_until}")
      SecurityMailer.deliver_certificate_replacement_warning(certificate)
    end

    def replace_with_autoprovisioned_certificate
      Certificate.customer_provisioned.expiring(Certificate::REPLACE_DAYS.from_now.utc.to_date).find_each_for_shard do |c|
        revoke_certificate?(c) ? revoke_certificate!(c) : replace_certificate(c)
      end
    end

    def replace_certificate(certificate)
      account = certificate.account
      AcmeCertificateJobStatus.enqueue_with_status!(account.id)
    end

    def revoke_certificate!(certificate)
      Rails.logger.info("Revoking certificate #{certificate.id} with expiration date #{certificate.valid_until}")
      certificate.revoke!
    end

    def revoke_certificate?(certificate)
      account = certificate.account
      account.brand_host_mappings.empty? || !account.is_serviceable? || !account.is_active?
    end
  end
end
