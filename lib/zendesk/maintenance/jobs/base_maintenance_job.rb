module Zendesk::Maintenance::Jobs
  # Helper parent class for maintenance jobs. Provides 'logger' and 'dryrun?' methods and a convenience
  # method for executing a job. Subclasses MUST implement the instance method execute. To implement:
  #
  # class MyJob < BaseMaintenanceJob
  #  def execute
  #    do_stuff unless dryrun?
  #    logger.info("Did stuff")
  #  end
  # end
  #
  # The output from running the above ends up in log/robot/zd_robot_my_job.log
  class MaintenanceJobException < RuntimeError; end

  class BaseMaintenanceJob
    attr_writer :completed

    def self.execute(dryrun = false, console_output = false, time = Time.now)
      instance   = new(dryrun, console_output, time)
      klass      = instance.class
      klass_name = klass.name

      instance.logger.info("Starting #{klass_name.demodulize} at #{Time.now}")

      if instance.respond_to?(:execute)
        begin
          CIA.amend_audit actor: User.system do
            instance.execute
          end
        rescue StandardError => e
          handle_error(e, instance, klass_name)
        end
      else
        instance.logger.fatal("#{instance.class.name.demodulize} does not respond to execute")
      end

      instance.logger.info("Finishing #{instance.class.name.demodulize} at #{Time.now}")
      if instance.completed && instance.notify?
        ::BackgroundJobMailer.deliver_completion_alert(::Account.find(1), klass_name, instance.recipients)
      end
      instance.logger.close
      nil
    end

    def self.handle_error(e, instance, klass_name)
      raise e if Rails.env == 'test'
      error = MaintenanceJobException.new(e.message)
      error.set_backtrace(e.backtrace)

      ZendeskExceptions::Logger.record(error, location: self, message: "Processing job #{klass_name} at #{Time.now}", fingerprint: '4e9682b507b7870be7c6ee82c67a6a8e56c560e3')

      instance.logger.error("Failed to execute #{klass_name.demodulize}: #{error.message}")
      ::BackgroundJobMailer.deliver_failure_alert(::Account.find(1), klass_name, instance.recipients) if instance.notify?
      instance.completed = false
      instance.logger.error(error.backtrace.join("\n"))
    end

    attr_reader :logger

    def initialize(dryrun, console_output, time = Time.now)
      @logger = MaintenanceLogger.new(dryrun ? '[DRYRUN]:' : '', self.class.name, console_output)
      @dryrun = dryrun
      @time   = time
    end

    def recipients
      []
    end

    def completed
      @completed || true
    end

    def notify?
      recipients.any? && Rails.env.production?
    end

    def record_recoverable_failure(continuation_message, error)
      ZendeskExceptions::Logger.record(error, location: self, message: "Processing job #{self.class.name} at #{Time.now}", fingerprint: 'c582585a97eaaf5e8a80a074530633451753c693')

      logger.error("Recoverable error executing #{self.class.name.demodulize}: #{error.message}")
      logger.error(continuation_message) unless continuation_message.nil? || continuation_message.empty?
      logger.error(error.backtrace.join('\n')) unless error.backtrace.nil?
    end

    def dryrun?
      @dryrun
    end

    attr_reader :time

    def execute_report(report)
      logger.info("\tExecuting #{report.name}...")
      duration = Benchmark.realtime do
        report.execute
      end
      logger.info("\t#{report.name} executed in #{duration}s")
    end

    def execute_reports_with_continuity(reports)
      reports.each do |report|
        begin
          execute_report(report)
        rescue StandardError => e
          record_recoverable_failure("#{report.name} failed; continuing with remaining reports", e)
        end
      end
    end
  end

  class MaintenanceLogger
    def initialize(prefix, name, console_output)
      @prefix  = prefix
      @console = console_output
      dir = "#{Rails.root}/log/robot"
      FileUtils.mkdir_p(dir) unless File.exist?(dir)
      @logger = ActiveSupport::Logger.new("#{dir}/zd_robot_#{name.demodulize.underscore}.log")
    end

    def debug(message)
      log(:debug, message)
    end

    def info(message)
      log(:info, message)
    end

    def warn(message)
      log(:warn, message)
    end

    def error(message)
      log(:error, message)
    end

    def fatal(message)
      log(:fatal, message)
    end

    def log(type, message)
      @logger.send(type, "#{@prefix} #{message}")
      puts "#{@prefix} #{message}" if @console
    end

    def flush
      @logger.flush
    end

    def close
      @logger.close
    end
  end
end
