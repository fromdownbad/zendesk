module Zendesk::Maintenance::Jobs
  class RemoveLegionDataJob < BaseDataDeleteJob
    priority :account_deletion
    stage :before_db

    private

    def erase_for_moved
      return if pod_local_move? || dry_run?
    end

    def erase_for_canceled
      return if dry_run?
    end
  end
end
