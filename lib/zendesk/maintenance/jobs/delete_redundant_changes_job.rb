module Zendesk::Maintenance::Jobs
  # Can be run like this:
  #  Zendesk::Maintenance::Jobs::DeleteRedundantChangesJob.new(false, false).delete(100, 200)
  class DeleteRedundantChangesJob < PodMaintenanceJob
    def execute
      ActiveRecord::Base.on_all_shards { delete }
    end

    def delete(from = 0, to = 1000000)
      return if dryrun?

      Change.connection.execute(
        ActiveRecord::Base.send(
          :sanitize_sql, [
            "DELETE FROM events  "\
            "WHERE id > ? "\
            "AND id <= ? "\
            "AND type = 'Change' "\
            "AND BINARY COALESCE(value, '') = COALESCE(value_previous, '')",
            from,
            to
          ], "events"
        )
      )
    end
  end
end
