module Zendesk::Maintenance::Jobs
  class RemoveSellDataJob < BaseDataDeleteJob
    priority :account_deletion
    stage :independent

    private

    def erase_for_canceled
      return if dry_run?
      return unless Arturo.feature_enabled_for?(:sell_data_deletion, account)

      statsd_client.increment('started')

      loop do
        raise KillSwitchEnabled if kill_switch_flipped?
        response = client.delete_account
        done = response.body['done']
        Rails.logger.debug("Data deletion finished for account #{account.id}") if done

        break if done

        backoff_duration = response.body['backoff'].to_f
        statsd_client.gauge('backoff_duration', backoff_duration)
        sleep backoff_duration
      end

      statsd_client.increment('completed')
      true
    end

    def client
      @client ||= Zendesk::Sell::InternalApiClient.new(account.subdomain, timeout: 30.seconds)
    end
  end
end
