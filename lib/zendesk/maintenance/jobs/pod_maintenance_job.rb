module Zendesk::Maintenance::Jobs
  class PodMaintenanceJob < BaseMaintenanceJob
    class << self
      def execute(*args)
        Account.where(shard_id: ActiveRecord::Base.shard_names).scoping do
          super
        end
      end
    end
  end
end
