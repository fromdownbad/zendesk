module Zendesk::Maintenance::Jobs
  # Delivers password expiration warnings
  class SecurityPolicyMailerJob < PodMaintenanceJob
    def self.fetch_accounts
      Account.joins(:subscription).active.shard_unlocked.find_each do |account|
        yield account
      end
    end

    def execute
      self.class.fetch_accounts do |account|
        deliver_expiring_password_warnings_for_account(account)
      end
    end

    protected

    def deliver_expiring_password_warnings_for_account(account)
      if account.is_serviceable?
        account.on_shard do
          ActiveRecord::Base.cache do
            logger.info("Expiration notifications for account #{account.id} started delivery")
            agents         = account.agents.includes(:settings).to_a
            deliveries     = deliver_expiring_password_warnings(agents)
            delivery_count = deliveries.count
            logger.info("Expiration notifications for account #{account.id} finished delivering #{delivery_count} warnings")
          end
        end
      end
    rescue StandardError => error
      message = <<-MESSAGE.squish
        Failed to deliver password expiration warning for account
        #{account.subdomain} with error #{error}
      MESSAGE
      logger.error(message)
    end

    def deliver_expiring_password_warnings(agents)
      agents.select do |agent|
        if Arturo.feature_enabled_for?(:no_expiring_password_warnings_if_suspended, agent.account)
          return false if agent.suspended?
        end
        agent_policy_expires_at = agent.password_expires_at.try(:beginning_of_day)

        if expiry_notification_dates.include?(agent_policy_expires_at)
          SecurityPolicyMailer.deliver_expiring_password_warning(agent)
          true
        else
          false
        end
      end
    end

    def expiry_notification_dates
      [3.days.from_now.beginning_of_day, Date.today.beginning_of_day]
    end
  end
end
