module Zendesk::Maintenance::Jobs
  class RemoveExportArtefactsJob < BaseDataDeleteJob
    priority :account_deletion
    stage :before_db

    private

    def erase_for_moved
      client = Zendesk::VoyagerClientBuilder.build(account)
      res = client.clean_artefacts(shard_id: shard_to_delete)

      return if res.status == 404

      raise res.humanized_errors unless res.successful?
    end
  end
end
