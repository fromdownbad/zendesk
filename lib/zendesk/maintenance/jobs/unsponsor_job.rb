module Zendesk::Maintenance::Jobs
  class UnsponsorJob < PodMaintenanceJob
    def execute
      logger.info("Processing trial expiry")

      subscriptions = Subscription.
        where(manual_discount: 100).
        where.not(payment_method_type: PaymentMethodType.MANUAL).
        where('manual_discount_expires_on <= ?', Date.today)

      subscriptions.find_each_for_shard do |subscription|
        logger.info("Processing expiry for #{subscription.account.id}/#{subscription.account.subdomain}")
        unless dryrun?
          ActiveRecord::Base.on_shard(subscription.account.shard_id) do
            subscription.managed_update = true
            subscription.update_attributes!(manual_discount: 0, manual_discount_expires_on: nil)
          end
        end
      end
    end
  end
end
