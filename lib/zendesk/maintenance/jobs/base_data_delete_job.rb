require 'active_record/comments'

module Zendesk::Maintenance::Jobs
  class TemporaryExit < StandardError; end
  class InvalidJobError < StandardError; end

  class BaseDataDeleteJob < BaseJob
    priority :account_deletion

    include Zendesk::DataDeletion::AccountDeletionKillSwitch

    def self.work(job_audit_id, options = {})
      new(job_audit_id, options).work
    end

    def self.stage(stage = nil)
      return @stage if stage.nil?
      @stage = stage.to_sym
    end

    attr_accessor :options

    def initialize(id, options = {})
      @id = id
      @options = options
    end

    def work
      return if kill_switch_flipped?

      ZendeskAPM.trace('work', service: job_audit.job.demodulize.underscore) do |span|
        span.set_tag('zendesk.account_id', account.id)
        span.set_tag('zendesk.subdomain', account.subdomain)

        with_query_comments { work_in_trace }
      end
    end

    def work_in_trace
      last_heartbeat = @options['last_heartbeat'] && Time.at(@options['last_heartbeat'])
      Zendesk::DataDeletion::JobHeartbeater.new(job_audit, last_heartbeat).with_poll do
        Rails.logger.append_attributes(
          zendesk: {
            subdomain: account.subdomain,
            account_id: account.id
          }
        )
        Rails.logger.info "#{job_id_for_log}: Starting"
        job_audit.start!

        account.verify_can_delete_for_cancellation if job_audit.for_cancellation?
        raise InvalidJobError, "Audit type mismatch #{job_audit.job} != #{self.class.name}" unless job_audit.job == self.class.name
        raise InvalidJobError, "Job is for a move and is executing on the account's current shard" if job_audit.for_shard_move? && shard_to_delete == account.shard_id
        raise InvalidJobError, "Job is executing on the wrong pod. Executing on pod #{Zendesk::Configuration.fetch(:pod_id)}, but trying to delete shard #{shard_to_delete} for pod #{pod(shard_to_delete)}" if Zendesk::Configuration.fetch(:pod_id) != pod(shard_to_delete)

        ActiveRecord::Base.on_shard(shard_to_delete) do
          erase_method = "erase_for_#{job_audit.reason}"
          ZendeskAPM.trace(erase_method) do
            Rails.logger.info "#{job_id_for_log}: Starting #{erase_method}"
            send(erase_method)
          end

          return if dry_run?

          verify_method = "verify_for_#{job_audit.reason}"
          on_retry_schedule do
            ZendeskAPM.trace(verify_method) do
              Rails.logger.info "#{job_id_for_log}: Starting #{verify_method}"
              send(verify_method)
            end
          end
        end

        job_audit.finish!
        # delete all failed jobs
        DataDeletionAuditJob.delete_failed_audit_jobs(job_audit.audit_id, job_audit.job, 0)
        record_deletion_time
        Rails.logger.info "#{job_id_for_log}: Completed."
      end
    rescue KillSwitchEnabled => e
      Rails.logger.warn("#{job_id_for_log}: #{e.inspect}")
    rescue TemporaryExit => e
      Rails.logger.warn("#{job_id_for_log}: #{e.inspect}")
    rescue RuntimeError => e
      Rails.logger.error("#{job_id_for_log}: #{e.inspect} : #{e.backtrace}")
      job_audit.fail!(e)
      handle_runtime_exception(e)
    rescue StandardError => e
      Rails.logger.error("#{job_id_for_log}: #{e.inspect} : #{e.backtrace}")
      job_audit.fail!(e)
      # Don't raise unnecessarily
      raise e unless kill_switch_flipped?
    end

    def job_audit
      # `on_master` because the record may not replicate across the WAN before
      # resque picks up the job, which would silently fail.
      @job_audit ||= DataDeletionAuditJob.on_master.find(@id)
    end

    def account
      Account.with_deleted do
        @account ||= Account.find(job_audit.account_id)
      end
    end

    def key_log
      @foreign_key_log ||= Zendesk::Logging::Instance.new(Rails.root.to_s + '/log/foreign_keys_deleted.log.json', Logger::INFO)
    end

    def stage
      self.class.stage
    end

    def dry_run?
      options.fetch(:dry_run, false)
    end

    def handle_runtime_exception(_exception)
      # does nothing by default; override to implement participant-specific (runtime) exception handling
    end

    def pod_local_move?
      pod(account.shard_id.to_i) == pod(ActiveRecord::Base.current_shard_id.to_i)
    end

    def on_retry_schedule(scheduled_retries = [1, 2, 5, 10])
      scheduled_retries.each do |waiting_time|
        begin
          yield; return
        rescue
          sleep waiting_time.minutes
        end
      end; yield
    end

    def method_missing(method_name, *arguments, &block)
      if /^(erase|verify)_for_/.match?(method_name.to_s)
        Rails.logger.info "#{method_name} not applicable for job #{self.class.name}, skipping and marking job completed."
        return true
      end
      super
    end

    # This will return the (pod-local) shard on which an ADD job should run.
    # Note that this method is based on the shard_id field in the data_deletion_audit_jobs table,
    # NOT the shard_id in the data_deletion_audit table.
    def shard_to_delete
      if job_audit.reason == 'canceled'
        account.shard_id
      elsif job_audit.reason == 'moved'
        unless job_audit.shard_id.present?
          raise "No shard_id set in audit for 'Move' deletion."
        end

        job_audit.shard_id
      else
        raise "Unknown 'reason' set in deletion audit, reason: '#{job_audit.reason}'"
      end
    end

    # Ex. pod(626) => "6"
    def pod(shard)
      Zendesk::Configuration::PodConfig.pod_id_for_shard(shard)
    end

    # Ex. region(6) => "us"
    def region(pod_id)
      Zendesk::Configuration.dig(:pods, pod_id, :region)
    end

    # We want to measure the total time it takes to complete a deletion job.
    # The deletion process begins when the deletion audit is first created.
    # The deletion audit is created when the account is soft-deleted or when it is done moving.
    # The namespace for this metric is specifically NOT linked to the job itself (job is an attribute)
    # to facilitate comparing deletion times across jobs
    def record_deletion_time
      return unless job_audit.completed_at && job_audit.audit.started_at

      job_duration_days = ((job_audit.completed_at - job_audit.audit.started_at) / 1.day).round

      statsd_client = Zendesk::StatsD::Client.new(namespace: 'data_deletion', tags: ["reason:#{job_audit.reason}"])

      Rails.logger.info("Account #{job_audit.account_id} is hard-deleted #{job_duration_days} days after deletion process begins.")

      statsd_client.distribution('deletion_time', job_duration_days, tags: ["job:#{job_audit.job}"])
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(
        namespace: ['account_data_deletion', job_audit.job.demodulize.underscore],
        tags: %W[reason:#{job_audit.reason}]
      )
    end

    # identifier to help find log messages related to specific audit and job
    def job_id_for_log
      @job_for_log ||= "#{job_audit.job} #{job_audit.id}"
    end

    # in development we do not eager load jobs -> so find them all
    def self.lazy_descendants
      @@descendants ||= begin
        files = Dir.glob(File.expand_path('lib/zendesk/maintenance/jobs/*.rb'))
        files.each { |f| require f }
        true
      end
      descendants
    end

    private

    def with_query_comments(&block)
      comment = {
        service: 'classic-resque',
        resource_name: self.class.to_s.demodulize,
        account_id: account.id,
        trace_id: trace_id,
        shard: shard_to_delete
      }

      ActiveRecord::Comments.comment(comment.to_json, &block)
    end

    def trace_id
      Datadog&.tracer&.active_span&.trace_id
    end
  end
end
