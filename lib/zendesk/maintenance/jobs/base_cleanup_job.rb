module Zendesk::Maintenance::Jobs
  class BaseCleanupJob < PerShardJob
    def self.work_on_shard(_shard_id, klass_name)
      log_info = "pid: #{$$}, shard_id: #{ActiveRecord::Base.current_shard_id}"
      resque_log("Starting #{klass_name} cleanup #{log_info} #{Time.now}")
      klass = klass_name.constantize
      klass.cleanup

      resque_log("Ending #{klass_name} #{log_info} #{Time.now}")
    end
  end
end
