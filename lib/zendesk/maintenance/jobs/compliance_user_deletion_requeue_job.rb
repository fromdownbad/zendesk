module Zendesk::Maintenance::Jobs
  class ComplianceUserDeletionRequeueJob < PerShardJob
    priority :medium

    # Periodically checks the compliance_deletion_statuses table for requests that have not completed and have been requested more than 7 days ago.
    # It republishes notices to particpants that have not completed deletion.
    class << self
      def work_on_shard(shard_id)
        catch_and_report_errors do
          ComplianceDeletionStatus.republish_incomplete_statuses!(shard_id: shard_id)
        end
      end

      private

      def catch_and_report_errors
        yield
      rescue StandardError => e
        message = "Failed to execute ComplianceUserDeletionRequeueJob: #{e.message}"
        resque_log(message)
        ZendeskExceptions::Logger.record(e,
          location: self,
          message: message,
          fingerprint: 'QBHTusl1FVq9nvoZWSeapG8fAOCNLJck5y2xdir0')
      end
    end
  end
end
