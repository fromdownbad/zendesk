module Zendesk::Maintenance::Jobs
  class VoicePartnerEditionTrialExpiryJob < PerShardJob
    class << self
      def work_on_shard(shard_id)
        ActiveRecord::Base.on_shard(shard_id) do
          Voice::PartnerEditionAccount.
            where(active: true).
            where(trial: true).
            where('trial_expires_at <= ?', Time.now).
            each do |partner_edition_account|
            account = partner_edition_account.account
            CIA.audit(actor: User.system) do
              partner_edition_account.deactivate!
            end
            Rails.logger.warn("Voice Partner Edition Account deactivated for account_id: #{account.id}")
          end
        end
      end
    end
  end
end
