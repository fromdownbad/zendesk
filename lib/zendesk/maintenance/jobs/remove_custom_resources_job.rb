module Zendesk::Maintenance::Jobs
  class RemoveCustomResourcesJob < BaseDataDeleteJob
    priority :account_deletion

    stage :independent

    SLEEP_DEFAULT = 10.0

    ROWS_DEFAULT = 1000

    ROWS_MAX = begin
      str = ENV.fetch('CUSTOM_RESOURCES_ACCT_DEL_ROWS_MAX', '').presence
      if str
        str.to_i
      else
        5000
      end
    end

    AURORA_CPU_SETPOINT = 50.0

    def erase_for_canceled
      raise KillSwitchEnabled if kill_switch_flipped?
      return if dry_run?

      unless account.has_custom_resources_account_deletion?
        statsd_client.increment('skip', tags:
          [
            "account_id:#{account.id}",
            "audit_id:#{job_audit.id}"
          ])

        # If the arturo is disabled, skip deleting form Custom Resources.
        # If this is the case, data have to be cleaned manually.
        # So, disable the arturo only in case of technical difficulties and when
        # we need to let other deletion jobs finish.
        @is_erasure_done = true
        return
      end

      duration = Benchmark.realtime do
        @is_erasure_done = erase_recursively(0, ROWS_DEFAULT)
      end

      statsd_client.histogram('recur_requests', duration, tags:
        [
          "account_id:#{account.id}",
          "audit_id:#{job_audit.id}",
          "is_done:#{@is_erasure_done}"
        ])
    end

    # Note, verify_for_canceled is expected to be called after erase_for_canceled returns.
    def verify_for_canceled
      raise "RemoveCustomResourcesJob did not complete for account #{account.id}" unless @is_erasure_done
    end

    # Noop. Custom Resources is a regional service
    def erase_for_moved
    end

    # Noop. Custom Resources is a regional service
    def verify_for_moved
    end

    private

    def custom_resources_url(num_rows)
      "/api/sunshine/private/account_deletion?" \
      "account_id=#{account.id}&subdomain=#{account.subdomain}&limit=#{num_rows}"
    end

    def response_is_done(resp_body)
      resp_body.try(:[], 'data').try(:[], 'done') == 'done'
    end

    # Returns [duration of sleep, number of rows to delete].
    # For now, always uses the default number of rows.
    # Sleep duration is nonzero iff cpu is busier than AURORA_CPU_SETPOINT.
    # In the futuer we could use PID controller for either or both.
    def next_sleep_and_rows(resp_body)
      curr_cpu = resp_body['data']['cpu']

      if curr_cpu.nil?
        # CPU not available -- could be non-Aurora dev env, or could be Aurora error.
        [SLEEP_DEFAULT, ROWS_DEFAULT]
      elsif curr_cpu > AURORA_CPU_SETPOINT
        [SLEEP_DEFAULT, ROWS_DEFAULT]
      else
        [0, ROWS_DEFAULT]
      end
    end

    # Returns whether erasure for the whole account is done
    def erase_recursively(dur_sleep, num_rows)
      sleep(dur_sleep) if dur_sleep > 0

      num_rows = [ROWS_DEFAULT, num_rows.to_i, ROWS_MAX].sort[1] # clamp

      resp = api_client.post(custom_resources_url(num_rows))

      response_is_done(resp.body) || begin
        next_sleep, next_num_rows = next_sleep_and_rows(resp.body)
        erase_recursively(next_sleep, next_num_rows)
      end
    end

    def api_client
      @api_client ||= begin
        kragle = Kragle.new('custom_resources', shared_cache: false)

        # Classic => Envoy Service => Envoy Sidecar => Custom Resources => Doorman.
        kragle.url_prefix = ENV['CUSTOM_RESOURCES_URL'] ||
          'http://custom-resources-proxy.service.consul:23860'

        # See https://github.com/zendesk/custom_resources/blob/master/doc/adr/0002-supporting-requests-that-have-not-been-enriched-by-doorman.md
        # for why this is necessary.
        kragle.headers['Host'] = "accounts.#{Zendesk::Configuration[:host]}"
        # This header is used in Envoy to match on the correct sharded service
        kragle.headers['X-Zendesk-Account-Id'] = account.id.to_s
        kragle
      end
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(
        namespace: ['jobs', 'remove_custom_resources_job']
      )
    end
  end
end
