module Zendesk::Maintenance::Jobs
  class ComplianceUserDeletionJob
    extend ZendeskJob::Resque::BaseJob

    priority :medium

    class << self
      def work
        # read from support GDPR SQS queue to determine if there is anyone to permanently delete in classic
        sqs_subscriber = Zendesk::PushNotifications::Gdpr::GdprSqsUserDeletionSubscriber.new
        Zendesk::PushNotifications::Gdpr::GdprUserDeletionSubscriber.new(sqs_subscriber).process_queues
      end

      def args_to_log
        {}
      end
    end
  end
end
