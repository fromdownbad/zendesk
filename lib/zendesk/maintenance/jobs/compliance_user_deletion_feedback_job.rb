module Zendesk::Maintenance::Jobs
  class ComplianceUserDeletionFeedbackJob
    extend ZendeskJob::Resque::BaseJob

    priority :medium

    class << self
      def work
        # Read from GDPR Feedback SQS queue to record responses from participants in db and determine if deletion request is finished
        sqs_subscriber = Zendesk::PushNotifications::Gdpr::GdprSqsFeedbackSubscriber.new
        Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.new(sqs_subscriber).process_queues
      end

      def args_to_log
        {}
      end
    end
  end
end
