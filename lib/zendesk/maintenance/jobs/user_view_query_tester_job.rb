module Zendesk::Maintenance::Jobs
  # This job automates Customer Lists queries to help decide if we should keep
  # the USE INDEX(`index_users_on_account_id_and_sample`) optimization for CL
  # sampling.
  #
  # For now we're focusing on queries which filter on `created_at` on accounts
  # with CL sampling enabled. However, this pattern can be applied to AB test
  # other queries in the future.
  #
  # The job works like so:
  #
  # 1. Every hour, loop through all accounts and find customer lists that filter
  #    on created_at
  # 2. For each CL, run the query with
  #    `USE INDEX(`index_users_on_account_id_and_sample`)` and run the query
  #    without the `USE INDEX`.
  # 3. Randomly (50%), perform 2 in reverse. Without `USE INDEX` first followed
  #    by with `USE INDEX`. This will help remove some bias.
  # 4. Record the select times and count times of each run along with relevant
  #    information (see screenshot) and send it as a summary report to
  #    customer-lists@zendesk.com
  class UserViewQueryTesterJob < PerAccountJob
    class << self
      attr_reader :tester

      def work_with_report(*args)
        @tester = Zendesk::UserViews::QueryTester.new

        work_without_report(*args)

        @tester.deliver_csv_report
      end
      alias_method :work_without_report, :work
      alias_method :work, :work_with_report

      def work_on_account(account)
        tester.test_account(account)
      end

      def args_to_log(*_args)
        { job: name.demodulize, time: Time.now.to_s }
      end
    end
  end
end
