module Zendesk::Maintenance::Jobs
  class PerShardJob < BaseJob
    include ZendeskJob::Resque::BaseJob

    class << self
      def work(*args)
        if args.last.is_a?(Hash) && (shard_id = args.last['work_shard_id'])
          args.pop

          Account.unscoped.where(shard_id: [shard_id]).scoping do
            ActiveRecord::Base.on_shard(shard_id) do
              resque_log "Starting Job: #{name.demodulize}, shard: #{shard_id}"
              shard_time = Benchmark.ms do
                work_on_shard(shard_id, *args)
              end
              resque_log "Job: #{name.demodulize}, completed: #{shard_time.round(1)}ms, shard: #{shard_id}"
            end
          end
        else
          args << {}
          ActiveRecord::Base.on_all_shards do |shard|
            resque_log "Enqueuing Job: #{name.demodulize} on shard: #{shard}"
            args.last['work_shard_id'] = shard
            enqueue(*args)
          end
        end
      end

      def work_on_shard(_shard, *_args)
        raise "'work_on_shard' must be implemented by subclass"
      end

      def args_to_log(*args)
        { args: args.map(&:to_s).join(" ") }
      end
    end
  end
end
