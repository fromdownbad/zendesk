module Zendesk::Maintenance::Jobs
  class DeletePasswordsJob < PerAccountJob
    class << self
      def fetch_accounts
        scope = Account.active.serviceable.shard_unlocked.
          joins(:role_settings).
          where("role_settings.updated_at BETWEEN ? AND ?", Time.now - 2.days, Time.now - 1.day)

        scope.find_each { |account| yield account }
      end

      def work_on_account(account)
        unless account.role_settings.agent_password_allowed?
          Rails.logger.info "DeletePasswordsJob removing agent passwords for ##{account.id} #{account.subdomain}"

          delete_passwords(account.agents)
        end

        unless account.role_settings.end_user_password_allowed?
          Rails.logger.info "DeletePasswordsJob removing end user passwords for ##{account.id} #{account.subdomain}"

          delete_passwords(account.end_users)
        end
      end

      def delete_passwords(users)
        users.update_all(salt: nil, crypted_password: nil)
      end

      def args_to_log
        {}
      end
    end
  end
end
