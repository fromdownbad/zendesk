require 'active_record/comments'

module Zendesk::Maintenance::Jobs
  # This job recalculates the tag rankings daily by replacing scores or creating tag_scores rows with the new numbers.
  #
  # JOB
  #   This Job is run once a day (view the schedule in config/resque-schedule.yml)
  #
  # FUNCTIONALITY
  #  1) Calculates how many times each tagging#tag_name (for users, orgs or tickets) have been used in the last 2 months.
  #  2) Uses a unique key defined on account_id and tag_name (uk_tag_scores_on_account_id_and_tag_name)
  #  to create a new row in tag_scores or replace an existing row with the new number.
  #  We do it with a query like this:
  #  REPLACE INTO tag_scores (account_id, tag_name, score, updated_at) VALUES (1, 'some_tag', 3, '2019-07-16 20:17:54.406537'), ...
  #  3) Deletes old tag_scores not updated in the last week.
  #
  # SEARCH TERMS: TagCloud, TagScore, tag_score, tag_scores, tagscore. tagcloud, Tagging, tag_name
  #
  # CREATE & UPDATING TagScore (outside of Background Jobs)
  #   There are places in the app we will manually call TagScore.update.
  #
  # NOTICE if the background job (ThesaurusResetJob) is changed you should ensure `TagScore.update`
  #   is also changed to mimic these changes
  #
  class ThesaurusResetJob < PerAccountJob
    APM_SERVICE_NAME = 'tag_scores_job'.freeze
    OLD_TAG_SCORES_LIMIT = 7.days
    PERIODIC_LOCK_STATE_CHECK = 30.seconds

    class << self
      def work_on_account(account)
        measure_account_population(account) { populate(account) }
      end

      def populate(account)
        periodically = Zendesk::Periodical.new(PERIODIC_LOCK_STATE_CHECK)
        account_taggings = []
        number_of_old_deleted_scores = 0
        locked_account = false

        query_time = Benchmark.ms { account_taggings = ranked_tags(account) }

        # We need to check current account lock_state in 2 different places:
        #   * Before starting the loop in case account_taggings is empty,
        #     because we also want to clean old scores in that case,
        #     but only if the account is not locked.
        #   * Inside the loop to do the periodic checks while we're updating the numbers.
        periodically.perform { locked_account = locked?(account) }

        update_time = Benchmark.ms do
          account_taggings.each_slice(1_000).each do |group|
            break if locked_account

            values = group.map do |tag_name, score|
              Tagging.send(:sanitize_sql, ["(?, ?, ?, ?)", account.id, tag_name, score, Time.now])
            end

            insert_or_update(values)

            # Check account lock_state every 30 seconds
            periodically.perform { locked_account = locked?(account) }
          end

          number_of_old_deleted_scores = clean_old_scores(account.id) unless locked_account
        end

        if locked_account
          resque_log("ThesaurusResetJob detected a locked account: #{account.id}")
        # log big accounts and 1% of general accounts
        elsif rand < 0.01 || account_taggings.size > 5000 || account.settings.tagging_amount_limit < 5000
          message = "ThesaurusResetJob finished on account: #{account.id} - "\
                    "Scores updated: #{account_taggings.size} - "\
                    "Old scores deleted: #{number_of_old_deleted_scores} - "\
                    "Query time (ms): #{query_time} - "\
                    "Update time (ms): #{update_time}"

          resque_log(message)
        end
      rescue StandardError => e
        resque_log("Error processing ThesaurusResetJob for account: #{account.id}")
        ZendeskExceptions::Logger.record(e, location: self, message: "Error processing ThesaurusResetJob for account: #{account.id}", fingerprint: '05f8797d207a530de77fab5d4760667b4c10087d')
        raise e
      end

      def insert_or_update(values)
        # Uses the primary key defined on account_id and tag_name,
        # to create a new row in tag_scores or replace an existing row
        # with the new score/updated_at values.
        Tagging.connection.execute <<~SQL
          INSERT INTO tag_scores
            (account_id, tag_name, score, updated_at)
          VALUES
            #{values.join(", ")}
          ON DUPLICATE KEY UPDATE
            `score`=VALUES(`score`),
            `updated_at`=VALUES(`updated_at`)
        SQL
      end

      # Deletes old tag_scores not updated in the last week.
      def clean_old_scores(account_id)
        TagScore.where(account_id: account_id).where("updated_at < ?", OLD_TAG_SCORES_LIMIT.ago.beginning_of_day).delete_all
      end

      # Gets a hash with tag names and the number of times they have been used
      # (in users, orgs or tickets) in the last 2 months,
      # sorted by that number in descending order.
      #
      # Query:
      #   SELECT COUNT(*) AS count_all, `taggings`.`tag_name` AS taggings_tag_name
      #   FROM `taggings` WHERE `taggings`.`account_id` = 1
      #   AND (created_at >= '2019-08-23 00:00:00.000000')
      #   GROUP BY `taggings`.`tag_name`
      #   ORDER BY COUNT(*) DESC
      #   LIMIT 20000
      #
      # Result:
      #   => { "tag1" => 34, "tag2" => 33, "tag3" => 5, "tag4" => 1 }
      def ranked_tags(account)
        tagging_created_at_limit = account.settings.tagging_created_at_limit.days
        tagging_amount_limit = account.settings.tagging_amount_limit
        ActiveRecord::Comments.comment(query_comment(account)) do
          Tagging.on_slave do
            Tagging.
              where(account_id: account.id).
              where(['created_at >= ?', tagging_created_at_limit.ago.beginning_of_day]).
              limit(tagging_amount_limit).
              group(:tag_name).
              order("COUNT(*) DESC").
              count
          end
        end
      end

      def query_comment(account)
        "ThesaurusResetJob maintenance job recalculating tag scores for account #{account.id} in shard #{account.shard_id}"
      end

      def locked?(account)
        Account.without_locking { Account.select(:lock_state).find(account.id).is_locked? }
      end

      def args_to_log
        {}
      end

      def measure_account_population(account)
        return yield unless account.has_measure_tag_scores_job?

        ZendeskAPM.trace(
          'populate',
          service: APM_SERVICE_NAME
        ) do |span|
          span.set_tag(::Datadog::Ext::Analytics::TAG_ENABLED, true)
          span.set_tag("zendesk.account_id", account.id)
          span.set_tag("zendesk.account_subdomain", account.subdomain)

          yield
        end
      end
    end
  end
end
