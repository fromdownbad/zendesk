module Zendesk::Maintenance::Jobs
  class UserContactInformationJob < BaseJob
    class << self
      def work
        # This job writes to the account database with high throughput.
        # The existing PerShardJob helper enqueues _concurrent_ jobs per-shard,
        # which would overload the account database and cause replication lag.
        # This helper will process will isntead walk the shards in serial.
        ActiveRecord::Base.on_all_shards do |shard_id|
          Account.without_locking do
            work_on_shard(shard_id)
          end
        end
      end

      def work_on_shard(shard)
        condition = ['id > ? AND shard_id = ?', 0, shard]
        Account.where(condition).find_each do |account|
          next if account.is_locked?
          user_contact_information = account.users.includes(:identities).where(users: {roles: Role::ADMIN.id})

          # prune users from contact list that are no longer admins
          old_user_contact_list = UserContactInformation.where(account_id: account.id).where.not(user_id: user_contact_information.pluck(:id))
          old_user_contact_list.destroy_all unless old_user_contact_list.empty?

          # Note that 'updated_at' is bumped when the admins login. We should
          # investigate a synchronization strategy that is less write intensive.
          last_updated_at = UserContactInformation.last_updated_date(account)
          user_contact_information = user_contact_information.where('users.updated_at > ?', last_updated_at) if last_updated_at

          update_user_contact_information(user_contact_information)
        end
      end

      def update_user_contact_information(users)
        users.each do |user|
          user_contact_id = UserContactInformation.get_user_contact_information_id(user.account_id, user.id)
          if user_contact_id.blank?
            UserContactInformation.create! do |user_contact_information|
              user_contact_information.account = user.account
              user_contact_information.user_id = user.id
              user_contact_information.name = user.name
              user_contact_information.email = user.email
              user_contact_information.phone = user.phone
              user_contact_information.role = user.roles
              user_contact_information.is_email_verified = user.is_verified
            end
          else
            UserContactInformation.where(id: user_contact_id).update_all(
              account_id: user.account_id,
              name: user.name,
              email: user.email,
              phone: user.phone,
              role: user.roles,
              is_email_verified: user.is_verified,
              updated_at: Time.now.utc
            )
          end
        end
      end
    end
  end
end
