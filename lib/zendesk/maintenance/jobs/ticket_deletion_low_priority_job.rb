module Zendesk::Maintenance::Jobs
  # TicketDeletion same code as TicketDeletionJob
  class TicketDeletionLowPriorityJob < JobWithStatus
    include Zendesk::Maintenance::Jobs::ScrubJobHelper

    priority :low

    def work
      current_account.on_shard do
        success = process_ticket_ids(current_account, tickets_ids)
        completed(results: { success: success })
      end
    end

    private

    def tickets_ids
      options[:tickets_ids]
    end
  end
end
