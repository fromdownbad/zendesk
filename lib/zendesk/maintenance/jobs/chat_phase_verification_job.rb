module Zendesk::Maintenance::Jobs
  class ChatPhaseVerificationJob < BaseJob
    extend ChatPhaseJobHelper

    class << self
      DATADOG_MISSING_SUBSCRIPTIONS = 'zopim_subscriptions.missing'.freeze
      DATADOG_EXTRA_SUBSCRIPTIONS = 'zopim_subscriptions.extra'.freeze

      def work
        return if Rails.env.production? && Zendesk::Configuration.fetch(:pod_id) != 5
        return unless chat_job_enabled?(:chat_phase3_verification_job)

        validate_zopim_subscriptions
        resque_log('ChatPhaseVerificationJob');
      end

      private

      def validate_zopim_subscriptions
        missing_zopim_subscriptions = []
        should_have_deleted_zopim_subscriptions = []
        validate_with_block do |products|
          missing_zopim_subscriptions += find_missing_zopim_subscriptions(products)
          should_have_deleted_zopim_subscriptions += find_undeleted_zopim_subscriptions(products)
        end
        log_errors(missing_zopim_subscriptions, DATADOG_MISSING_SUBSCRIPTIONS, 'Missing zopim subscriptions for chat phase 3') unless missing_zopim_subscriptions.empty?
        log_errors(should_have_deleted_zopim_subscriptions, DATADOG_EXTRA_SUBSCRIPTIONS, 'Should have deleted these chat phase 3 zopim subscriptions for accounts') unless should_have_deleted_zopim_subscriptions.empty?
      end

      def find_missing_zopim_subscriptions(products)
        cp3_account_ids = products.select { |p| p.plan_settings['phase'] == 3 && !p.cancelled? }.map(&:account_id)
        active_cp3_account_ids = Account.where("is_serviceable = 1 AND id IN (?)", cp3_account_ids).pluck(:id)
        with_zopim_subs = ZBC::Zopim::Subscription.where(account_id: active_cp3_account_ids).pluck(:account_id)
        active_cp3_account_ids - with_zopim_subs
      rescue StandardError => e
        Rails.logger.error "validate_missing_zopim_subscription error: #{e.inspect}"
        []
      end

      def find_undeleted_zopim_subscriptions(products)
        cancelled_cp3_account_ids = products.select(&:cancelled?).map(&:account_id)
        ZBC::Zopim::Subscription.where(account_id: cancelled_cp3_account_ids).
          joins("INNER JOIN accounts ON account_id = accounts.id AND accounts.is_serviceable = 1").pluck(:account_id)
      rescue StandardError => e
        Rails.logger.error "validate_undeleted_zopim_subscription error: #{e.inspect}"
        []
      end
    end
  end
end
