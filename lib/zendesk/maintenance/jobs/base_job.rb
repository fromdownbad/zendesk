module Zendesk
  module Maintenance
    module Jobs
      class BaseJob
        extend ZendeskJob::Resque::BaseJob

        class << self
          def inherited(subclass)
            subclass.class_eval do
              priority :maintenance
            end
          end

          def work
            raise "Must be implemented by subclass"
          end

          def args_to_log(*args)
            {args: args.map(&:to_s).join(" ")}
          end
        end
      end
    end
  end
end
