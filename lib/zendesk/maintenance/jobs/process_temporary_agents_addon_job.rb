module Zendesk::Maintenance::Jobs
  class ProcessTemporaryAgentsAddonJob < PerShardJob
    class << self
      def work_on_shard(shard_id)
        ActiveRecord::Base.on_shard(shard_id) do
          start_temporary_agents_addons
          expire_temporary_agents_addons
        end
      end

      private

      def start_temporary_agents_addons
        addons_to_start = SubscriptionFeatureAddon.active_plus_grace_period(SubscriptionFeatureAddon::TEMPORARY_ZENDESK_AGENTS_GRACE_PERIOD).
          starts_at_unprocessed

        addons_to_start.each do |addon|
          resque_log("Start temporary agent addon (ID: #{addon.id}) for: #{addon.account.name}/#{addon.account_id}")
          process_addon(addon, :started)
        end
      end

      def expire_temporary_agents_addons
        addons_to_expire = SubscriptionFeatureAddon.expired_addons(SubscriptionFeatureAddon::TEMPORARY_ZENDESK_AGENTS_GRACE_PERIOD).
          expires_at_unprocessed

        addons_to_expire.each do |addon|
          resque_log("Expire temporary agent addon (ID: #{addon.id}) for: #{addon.account.name}/#{addon.account_id}")
          process_addon(addon, :expired)
        end
      end

      def process_addon(addon, status)
        addon.account.subscription.save!
        addon.update_column(:status_id, SubscriptionFeatureAddon::STATUS[status])
        resque_log("Processed temporary agent addon (STATUS: #{status}(ID: #{addon.id}) for: #{addon.account.name}/#{addon.account_id}")
      rescue StandardError => e
        ZendeskExceptions::Logger.record(e, location: self, message: "Error in processing temporary agent addon job: #{e.message}", fingerprint: 'b1212792060fb09d3ee9e64435228e44f0780ded')
      end
    end
  end
end
