module Zendesk::Maintenance::Jobs
  class RemoveAttachmentsFromCloudJob < BaseDataDeleteJob
    class Waterline
      def initialize(redis:, key:, ttl:)
        @redis = redis
        @key = key
        @ttl = ttl
      end

      attr_reader :redis, :key, :ttl

      def get
        # If null, .to_i returns

        # Incrementing the key before returning since we are storing the last processed attachment in the waterline
        # and we dont want to process the same attachment twice

        redis.get(key).to_i + 1
      end

      def set(value)
        redis.set(key, value, ttl: ttl)
      end
    end

    priority :account_deletion

    stage :before_db

    def erase_for_canceled
      each_attachment do |store, attachment|
        # Must guard against old/broken store values like xfiles and cf
        next unless attachment.class.known_stores.include? store
        storage = attachment.send(:get_storage_delegator, store)
        begin
          storage.destroy_file unless dry_run?
          key_log.info("Attachment Deleted",
            account: account.id, subdomain: account.subdomain,
            shard: account.shard_id, record_type: "attachment",
            storage_keys: attachment.foreign_keys, dry_run: dry_run?)
        rescue StandardError => e
          if attachment.send(store).exist?
            Rails.logger.error("#{job_id_for_log} Failed to remove #{attachment.class} id:#{attachment.id} from store:#{store}: #{e.inspect}")
            raise e
          else
            Rails.logger.info("#{job_id_for_log} Ignoring error while removing non-existent #{attachment.class} id: #{attachment.id}")
          end
        end
      end
    end

    def verify_for_canceled
      each_attachment do |store, attachment|
        # See above.
        next unless attachment.class.known_stores.include? store
        if attachment.send(store).exist?
          raise "#{job_id_for_log} failed for account #{account.idsub}." \
            "Attachment Cloud Data still exists for #{store} store."
        end
      end
    end

    def each_attachment
      cloud_stored_classes.each do |klass|
        waterline = Waterline.new(redis: Resque.redis, key: "#{self.class.name}/#{klass.name}/#{job_audit.id}", ttl: 1.week)
        klass.where("account_id = ?", account.id).find_in_batches(start: waterline.get) do |batch|
          batch.each do |attachment|
            raise KillSwitchEnabled if kill_switch_flipped?
            attachment.stores.each do |store|
              if attachment.class.attachment_backends[store]
                yield store, attachment
              else
                Rails.logger.warn("Ignoring deletion for unsupported backend #{store.inspect}")
              end
            end
          end

          waterline.set(batch.last.id)
        end
      end
    end

    def erase_for_moved
      # We don't remove attachments as they are unsharded.
    end

    # not a top-level constant to avoid eager loading for test/dev
    def cloud_stored_classes
      [::Attachment, ::ExpirableAttachment, ::AccountLogo, ::BrandLogo, ::HeaderLogo, ::MobileLogo, ::Favicon, ::Photo, ::RuleAttachment, ::Voice::Upload, ::Zendesk::OAuth::Logo]
    end
  end
end
