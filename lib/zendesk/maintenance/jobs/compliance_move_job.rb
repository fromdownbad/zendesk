module Zendesk::Maintenance::Jobs
  class ComplianceMoveJob < BaseMaintenanceJob
    def initialize(dryrun, console_output, time = Time.now)
      super(dryrun, console_output, time)
    end

    def execute
      purge_old_moves
      create_compliance_moves unless pods.blank?
      delete_churned_compliance_moves
    end

    def pod_attributes
      @pod_attributes ||= PoddableClient::PodApi.new.get_pods
    end

    def pods
      @pods ||= pod_attributes&.map { |pod| [pod.pod_id_number, pod.tag_names] }.to_h
    end

    def purge_old_moves
      local_pod_id = Zendesk::Configuration.fetch(:pod_id)
      local_moves = ComplianceMove.where(src_pod_id: local_pod_id)
      local_moves.without_arsi.delete_all
    end

    def create_compliance_moves
      ActiveRecord::Base.on_all_shards do |s|
        logger.info("Creating ComplianceMove records on shard #{s}...")

        ComplianceMove::MOVE_TYPE_IDS.each do |location, _code|
          logger.info("Creating ComplianceMove for accounts with #{location.upcase} location")

          accounts_to_move[location].each do |account|
            next if is_churned?(account)
            next if ComplianceMove.find_by_account_id(account.id)

            logger.info("Creating ComplianceMove for account ##{account.id}")
            next if dryrun?

            begin
              ComplianceMove.create!(
                account: account,
                account_subdomain: account.subdomain,
                src_shard_id: account.shard_id,
                src_pod_id: account.pod_id,
                move_type: ComplianceMove::MOVE_TYPE_IDS[location],
                move_scheduled: false
              )

              logger.info("ComplianceMove saved for account ##{account.id}")
            rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotSaved => e
              ZendeskExceptions::Logger.record(
                e,
                location: self,
                message: "Error creating ComplianceMove for account ##{account.id}: #{e.message}",
                fingerprint: '1b8a8538ce29c74c746c5b3f35ed1802dde6c001'
              )
            end
          end
        end
      end
    end

    def delete_churned_compliance_moves
      Account.with_deleted do
        ComplianceMove.waiting.each do |compliance_move|
          logger.info("Found ComplianceMove for churned account ##{compliance_move.account.id}")
          unless dryrun?
            compliance_move.destroy if is_churned?(compliance_move.account)
          end
        end
      end
    end

    def is_churned?(account) # rubocop:disable Naming/PredicateName
      account.deleted_at.present? || account.subscription.churned_on.present?
    end

    def account_ids
      @account_ids ||= {
        us: us_data_center_ids,
        eu: eu_data_center_ids,
        de: germany_data_center_ids
      }
    end

    def accounts_to_move
      @accounts_to_move = {}

      account_ids.each do |location, ids|
        @accounts_to_move[location] = ids.map do |id|
          account = Account.with_deleted { Account.find_by_id(id) }
          account_location_correct?(account.pod_id, location) ? nil : account
        end.compact
      end

      @accounts_to_move
    end

    def account_location_correct?(pod_id, location)
      pods[pod_id] && pods[pod_id].include?(ComplianceMove::TAG_NAMES[location])
    end

    def eu_data_center_ids
      SubscriptionFeatureAddon.where('(name = ?)', 'eu_data_center').pluck(:account_id).uniq
    end

    def germany_data_center_ids
      SubscriptionFeatureAddon.where('(name = ?)', 'germany_data_center').pluck(:account_id).uniq
    end

    def us_data_center_ids
      SubscriptionFeatureAddon.where('(name = ?)', 'us_data_center').pluck(:account_id).uniq
    end
  end
end
