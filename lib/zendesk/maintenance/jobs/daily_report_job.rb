module Zendesk::Maintenance::Jobs
  class DailyReportJob < GlobalMaintenanceJob
    REPORTS = [
      Zendesk::Export::BillingReport,
      Zendesk::Export::BenchmarkingReport
    ].freeze

    def execute
      execute_reports_with_continuity Zendesk::Maintenance::Jobs::DailyReportJob::REPORTS
    end
  end
end
