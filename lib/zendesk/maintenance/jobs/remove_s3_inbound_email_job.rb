require 'aws-sdk-s3'

module Zendesk::Maintenance::Jobs
  class RemoveS3InboundEmailJob < BaseDataDeleteJob
    priority :account_deletion
    stage :independent

    private

    # called by superclass
    def erase_for_canceled
      delete_categories
    end

    # called by superclass
    def verify_for_canceled
      verify_categories
    end

    # called by superclass
    def erase_for_moved
      return true unless different_stores?
      delete_categories
    end

    # called by superclass
    def verify_for_moved
      return true unless different_stores?
      verify_categories
    end

    def delete_categories
      return true unless applicable_for_environment?
      categories.each { |category| delete_category(category) }
    end

    def verify_categories
      return true unless applicable_for_environment?
      categories.each { |category| verify_category(category) }
    end

    def verify_primary_stores(pod)
      # Keys inbound_email & inbound_email_attachments should have the same primary store
      unless categories.map { |category| primary_store(pod, category) }.uniq.one?
        raise(ArgumentError, "RemoveS3InboundEmailJob detected different primary stores across #{categories.inspect} categories on '#{pod}' pod for account #{account.idsub}")
      end
    end

    def applicable_for_environment?
      !Rails.env.development?
    end

    def pod_to_keep
      pod(account.shard_id)
    end

    def pod_to_delete
      pod(shard_to_delete)
    end

    def different_stores?
      # We may not need this verification:
      verify_primary_stores(pod_to_delete)
      verify_primary_stores(pod_to_keep)

      # We don't need to check pods or regions anymore, only the store.
      store_to_delete = primary_store(pod_to_delete, 'inbound_email')
      store_to_keep   = primary_store(pod_to_keep, 'inbound_email')

      store_to_delete != store_to_keep
    end

    def categories
      %w[inbound_email inbound_email_attachments]
    end

    def config
      REMOTE_FILES_CONFIG
    end

    def prefix
      "#{account.id}/"
    end

    def primary_store(pod, category)
      primary_store = if KUBERNETES
        config[category]&.detect { |(_store, conf)| conf['primary'] }&.first
      else
        Zendesk::Configuration.dig(:pods, pod, :aws_regions, "#{category}_store").try(:first)
      end

      raise(ArgumentError, "No primary store detected for '#{category}' category on '#{pod}' pod.") unless primary_store

      primary_store
    end

    def store_details(store, category)
      unless store_details = config.fetch(category, {}).fetch(store)
        raise(ArgumentError, "No stores connection detected for '#{category}' category, '#{store}' store.")
      end
      store_details
    end

    def connection(category)
      @connection ||= {}
      primary_store = primary_store(pod_to_delete, category)
      store = store_details(primary_store, category)

      @connection[category] ||= Aws::S3::Client.new(
        access_key_id:      store.fetch('aws_access_key_id'),
        secret_access_key:  store.fetch('aws_secret_access_key'),
        region:             store.fetch('region')
      )
    end

    def bucket_name(category)
      primary_store = primary_store(pod_to_delete, category)
      store = store_details(primary_store, category)
      store.fetch('directory')
    end

    def delete_category(category)
      bucket = bucket_name(category)

      options = {
        category: category,
        prefix: prefix,
        bucket: bucket,
        account: account.id,
        subdomain: account.subdomain,
        shard: account.shard_id,
        record_type: "s3_email",
        dry_run: dry_run?
      }

      key_log.info("S3 Email Deleting", options.merge(phase: 'deleting'))

      unless dry_run?
        connection(category).list_objects(bucket: bucket, prefix: prefix).each do |objects|
          keys = objects.contents.map { |o| {key: o.key} }
          connection(category).delete_objects(bucket: bucket, delete: {objects: keys}) unless keys.empty?
        end
      end

      key_log.info("S3 Email Deleted", options.merge(phase: 'deleted'))
    end

    def verify_category(category)
      if connection(category).list_objects(bucket: bucket_name(category), prefix: prefix).contents.any?
        raise "RemoveS3InboundEmailJob failed for account #{account.idsub}: All objects not deleted."
      end
    end
  end
end
