module Zendesk::Maintenance::Jobs
  class RemoveEmbeddingsAfterDeleteJob < BaseDataDeleteJob
    priority :account_deletion
    stage :before_db

    private

    def erase_for_moved
      return if same_region_move?

      Rails.logger.info("Deleting embeddings data due to account move for account: #{account.subdomain}")
      Zendesk::AnswerBotService::InternalApiClient.new(account).delete_embeddings
    end

    def same_region_move?
      destination_pod_region = pod_to_crystal_ball_region(pod(account.shard_id.to_i))
      source_pod_region = pod_to_crystal_ball_region(pod(ActiveRecord::Base.current_shard_id.to_i))

      source_pod_region == destination_pod_region
    end

    def pod_to_crystal_ball_region(pod)
      region(pod) == 'emea' ? 'eu' : 'us'
    end
  end
end
