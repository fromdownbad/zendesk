# Shards with issues can be found via this:
# ActiveRecord::Base.on_all_shards{|s| [s, SatisfactionRatingIntention.where("created_at < ?", 2.hour.ago).count(:all)] }.select {|s,v|v > 0}
module Zendesk::Maintenance::Jobs
  class SatisfactionRatingIntentionJob
    extend ZendeskJob::Resque::BaseJob

    priority :medium

    def self.work
      count = 0
      ActiveRecord::Base.on_all_shards do
        SatisfactionRatingIntention.where('created_at < ?', 30.minutes.ago).find_each(batch_size: 100) do |rating|
          begin
            Zendesk::DB::RaceGuard.cache_guarded("sat-rating-intention/#{rating.account_id}/#{rating.id}") do
              rating.rate!
              rating.destroy # creation of the rating should already destroy the intention, but make sure
              count += 1
            end
          rescue StandardError => e
            ZendeskExceptions::Logger.record(e, location: self, message: "Unable to rate satisfaction rating intention", reraise: !Rails.env.production?, fingerprint: '8d56b277a399687b181378625531dbc53110279e')
            Rails.logger.error("Deleting unrateable satisfaction rating intention #{rating.inspect}")
            rating.destroy # requester is suspended or other edge cases
          end
        end
      end
      SatisfactionRatingIntention.stats.count 'rated', count
    end

    def self.args_to_log(*args)
      { arguments: args }
    end
  end
end
