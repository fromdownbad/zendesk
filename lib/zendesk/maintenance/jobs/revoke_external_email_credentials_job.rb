module Zendesk::Maintenance::Jobs
  class RevokeExternalEmailCredentialsJob < BaseDataDeleteJob
    priority :account_deletion
    stage :before_db

    private

    def erase_for_canceled
      account.external_email_credentials.each do |credential|
        next unless credential.encrypted_value

        ::RevokeExternalEmailCredentialJob.work(
          credential.encrypted_value,
          credential.encryption_key_name,
          credential.encryption_cipher_name
        )
      end
    end

    def verify_for_canceled
      true
    end
  end
end
