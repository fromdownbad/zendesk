module Zendesk::Maintenance::Jobs
  class UnuploadedUsageCleanupJob < PerShardJob
    priority :medium

    private_class_method :new

    AlreadySweepingError = Class.new(StandardError)

    def self.work_on_shard(shard_id)
      new(shard_id).cleanup_usages_with_lock
    end

    def cleanup_usages_with_lock
      with_lock { clean_up_usages }
    end

    private

    attr_reader :shard_id

    def initialize(shard_id)
      @shard_id = shard_id
    end

    def clean_up_usages
      statsd_client.increment("sweeping")

      begin
        usages.each_key do |account_id|
          account = Account.find(account_id)
          next unless can_monitor?(account)

          cache_manager = subscription_name_cache_manager(account)

          if cache_manager.cache_contains_subscription_name?
            upload_usages(usages[account_id], cache_manager.subscription_name) if can_clean_up?(account)
          else
            cache_manager.refresh_cache

            if cache_manager.cache_contains_subscription_name?
              upload_usages(usages[account_id], cache_manager.subscription_name) if can_clean_up?(account)
            elsif cache_manager.cache_contains_missing_usage_subscription_error?
              setup_valid_usage_subscription(account_id)
            end
          end
        end
      rescue StandardError
        statsd_client.increment("sweeping_error")
      end
    end

    # Returns a collection of usages grouped by their corresponding accounts
    # from all shards in the pod the job is running in, like so:
    #
    # {
    #   account-1: [
    #     usage-1,
    #     usage-2,
    #     usage-3
    #   ],
    #   account-2: [
    #     usage-1,
    #     usage-2,
    #     usage-3,
    #     usage-4
    #   ],
    #   account-3: [
    #     usage-1
    #   ]
    # }
    #
    def usages
      statsd_client.increment("query_usages")

      begin
        with_timing("unuploaded_usage_query") do
          ZBC::Zuora::VoiceUsage.
            where('usage_type = "usage" AND zuora_reference_id IS NULL AND created_at < ?', DateTime.now - 3.hours).
            select(:account_id, :id).
            group_by(&:account_id)
        end
      rescue StandardError
        statsd_client.increment("query_usages_error")
      end
    end

    def upload_usages(account_usages, subscription_name)
      statsd_client.increment("upload_usages")

      begin
        account_usages.each do |usage|
          ZBC::Zuora::Jobs::VoiceUsageJob.enqueue(
            usage.account_id,
            usage.id,
            subscription_name
          )
        end
      rescue StandardError
        statsd_client.increment("upload_usages_error")
      end
    end

    def setup_valid_usage_subscription(account_id)
      Voice::UsageSubscriptionCorrectorJob.enqueue(account_id)
    end

    def subscription_name_cache_manager(account)
      ZBC::Zuora::Voice::UsageSubscriptionCacheManager.new(
        account,
        "cleanup_job_cache_events"
      )
    end

    def with_timing(tag)
      result = nil
      time   = Benchmark.realtime { result = yield }

      statsd_client.timing(tag, time * 1000)

      result
    end

    def with_lock
      if acquire_lock
        statsd_client.increment("acquire_lock", tags: ["shard_id:#{shard_id}"])
        Rails.logger.info("#{self.class}: Acquired lock on #{shard_id}")

        with_timing("cleanup_job") do
          begin
            yield
          ensure
            release_lock
          end
        end
      else
        statsd_client.increment("already_sweeping_error", tags: ["shard_id:#{shard_id}"])

        raise AlreadySweepingError, "Only one instance of #{self.class.name} can run at a time."
      end
    end

    def acquire_lock
      redis_client.set(lock_key, true, nx: true)
    end

    def release_lock
      redis_client.del(lock_key)

      statsd_client.increment("release_lock", tags: ["shard_id:#{shard_id}"])
      Rails.logger.info("#{self.class}: Released lock on #{shard_id}")
    end

    def lock_key
      "voice_usage_cleanup_job_status:shard_#{shard_id}"
    end

    def redis_client
      @redis_client ||= Zendesk::RedisStore.client
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(
        namespace: %w[voice_usage cleanup_job]
      )
    end

    def can_monitor?(account)
      Arturo.feature_enabled_for?(
        :billing_voice_unuploaded_usage_cleanup_job_monitoring,
        account
      )
    end

    def can_clean_up?(account)
      Arturo.feature_enabled_for?(
        :billing_voice_unuploaded_usage_cleanup_job,
        account
      )
    end
  end
end
