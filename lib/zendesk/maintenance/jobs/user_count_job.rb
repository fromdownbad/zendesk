module Zendesk::Maintenance::Jobs
  class UserCountJob < PerAccountJob
    class << self
      def work_on_account(account)
        # don't need user counts for expired accounts
        return unless account.is_active &&
          account.is_serviceable && account.id != Account.system_account_id
        account.settings.user_count = account.users.count(:all)
        unless account.save
          resque_log("#{name}: Failed for account #{account.subdomain} due to the following errors: #{account.errors.full_messages.join(', ')}")
        end
      end

      def args_to_log
        {}
      end
    end
  end
end
