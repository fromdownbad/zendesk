module Zendesk::Maintenance::Jobs
  class SelectTicketsForScrubbingJob < PerAccountJob
    class << self
      SOFT_DELETED_TICKET_RETENTION = 30.days

      def work_on_account(account)
        return unless account.has_ticket_scrubbing?

        return if kill_switch_enabled?(account)

        deleted = Ticket.with_deleted do
          account.tickets.deleted_and_not_scrubbed_before(SOFT_DELETED_TICKET_RETENTION.ago).limit(account.settings.max_tickets_to_scrub_per_hour).pluck(:id)
        end

        archived = []
        # If achiver is disabled while account is moved to another shard, skip tickets
        unless ["false", "disable_pending"].include?(account.settings.write_to_archive_v2_enabled)
          tickets_conditions = Ticket.unscoped.deleted_and_not_scrubbed_before(SOFT_DELETED_TICKET_RETENTION.ago).condition_sql
          archived = account.ticket_archive_stubs.where(tickets_conditions).limit(account.settings.max_tickets_to_scrub_per_hour).pluck(:id)
        end

        ticket_ids = deleted + archived

        return if ticket_ids.empty?

        resque_log("Enqueuing #{ticket_ids.size} ticket(s) for scrubbing for account #{account.id}/#{account.subdomain}")
        statsd_client.count('tickets_enqueued', ticket_ids.size)

        job_class(account).enqueue(
          account_id: account.id, user_id: User.system.id, tickets_ids: ticket_ids
        )
      end

      def args_to_log
        {}
      end

      private

      def job_class(account)
        if account.has_ticket_scrubbing_low_priority_job?
          Zendesk::Maintenance::Jobs::TicketDeletionLowPriorityJob
        else
          Zendesk::Maintenance::Jobs::TicketDeletionJob
        end
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[ticket_scrub select_tickets_for_scrubbing_job])
      end

      def kill_switch_enabled?(account)
        Arturo.feature_enabled_for_dbcluster?(
          :kill_switch_select_tickets_for_scrubbing_job,
          DB_SHARDS_TO_CLUSTERS[account.shard_id]
        )
      end
    end
  end
end
