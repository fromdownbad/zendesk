# The table is there to keep a list of *just the accounts that actively
# live on the shard*, in a table on the shard itself.  Can be useful to
# join against to avoid shard data for an account that no longer "lives"
# there -- ie has yet to be cleaned off the shard.
#
# It's only *eventually* consistent.  Updated in run time as accounts are
# updated, and also updated nightly from the creaking "robot" cronjob.
# https://github.com/zendesk/zendesk/blob/48/lib/zendesk/maintenance/robot.rb
module Zendesk::Maintenance::Jobs
  class ShardedSubscriptionJob < PodMaintenanceJob
    def execute
      logger.info('Updating sharded subscriptions')
      all_accounts = {}

      Account.joins(:subscription).shard_unlocked.find_each do |account|
        next unless account.id > 0
        ActiveRecord::Base.on_shard(account.shard_id) do
          sync_sharded_subscription_from_account_database(account)
          all_accounts[account.id] = account.shard_id
        end
      end

      ActiveRecord::Base.on_all_shards do
        ShardedSubscription.find_each do |sharded_subscription|
          delete_inactive_records_caused_by_shard_movement(sharded_subscription)
        end
      end

      logger.info('Finished updating sharded subscriptions')
    end

    private

    def sync_sharded_subscription_from_account_database(account)
      sharded_subscription = account.sharded_subscription || account.build_sharded_subscription
      sharded_subscription.sync_from_account_database

      if sharded_subscription.changed?
        logger.info("Syncing account #{account.id} to sharded_subscriptions")
        sharded_subscription.save!
      end
    rescue StandardError => error
      message = <<-MESSAGE.squish
        Failed to sync sharded subscription #{sharded_subscription} from account
        database #{account} with error #{error}
      MESSAGE
      logger.error(message)
    end

    def delete_inactive_records_caused_by_shard_movement(sharded_subscription)
      # NOTE This introduces an N+1, but there is a bug in kasket that causes an
      # error here for sharded_subscription.account.nil? if we use :include => :account
      # in the find_each call. See https://github.com/staugaard/kasket/issues/4
      account = sharded_subscription.account
      if account.nil? || !account.shard_local?
        message = <<-MESSAGE.squish
          Removing sharded_subscription for account
          #{sharded_subscription.account_id} from shard
          #{ActiveRecord::Base.current_shard_id} as it is not local to that
          shard.
        MESSAGE
        logger.info(message)
        sharded_subscription.delete
      end
    rescue Zendesk::Accounts::Locking::LockedException
      # exit and process next
    end
  end
end
