require 'aws-sdk-s3'

module Zendesk::Maintenance::Jobs
  module ScrubJobHelper
    def process_ticket_ids(account, tickets_ids)
      success = true
      succeeded = 0
      failed = 0

      tickets_ids.each do |ticket_id|
        ticket = Ticket.with_deleted do
          account.tickets.
            # Currently `with_deleted` blasts the whole scope, causing it to fetch
            # the soft-archived record from `tickets` instead of Riak.
            where('status_id != ?', StatusType.ARCHIVED).
            find_by_id(ticket_id)
        end

        destroyed = destroy(ticket)
        destroyed ? succeeded += 1 : failed += 1
        success = false unless destroyed
      end

      statsd_client.batch do |c|
        c.count('processed', succeeded, tags: ['succeeded:true']) if succeeded > 0
        c.count('processed', failed, tags: ['succeeded:false']) if failed > 0
      end

      success
    end

    private

    def destroy(ticket)
      return true if ticket.nil?
      # TicketArchiveStub is returned when Riak has lost the archived ticket
      return false if ticket.is_a?(TicketArchiveStub)

      begin
        # if they have prevent_deletion_if_churned this will appear to work but will not delete any data
        unless ticket.account.has_prevent_deletion_if_churned?
          delete_raw_emails(ticket)
          delete_attachments(ticket)
          statsd_client.time('zendesk.voice.soft_delete_recording.request') do
            delete_voice_recordings(ticket)
          end
        end
        Zendesk::Scrub.scrub_ticket_and_associations(ticket)
      rescue StandardError => error
        message = "Ticket scrub failed for ticket" \
          " ##{ticket.id} for acc:#{ticket.account_id} on shard:#{ActiveRecord::Base.current_shard_id}:" \
          " #{error.message}"
        type = error.class.name.underscore
        statsd_client.increment('destroy.error', tags: ["exception:#{type}"])
        ZendeskExceptions::Logger.record(error, location: self, message: message, reraise: !Rails.env.production?, fingerprint: '6d1bab2ce67843565d4fa17d3ea3db52f2e87e17')
        return false
      end

      true
    end

    def delete_attachments(ticket)
      if current_account.has_email_gdpr_remote_files?
        ticket.delete_attachments
      else
       # Directly using ticket_id on Attachment.where does not have the required index
        comment_ids = ticket.comments.pluck(:id)
        attachments = Attachment.where(source_type: "Comment", source_id: comment_ids)
        attachments.each(&:remove_from_remote!)
      end
    end

    def delete_raw_emails(ticket)
      if current_account.has_email_gdpr_remote_files?
        ticket.delete_raw_emails
      else
        audits = ticket.audits.where(via_id: ViaType.MAIL)
        audits.each do |audit|
          raw_email = audit.raw_email
          %i[json_identifier eml_identifier].each do |format|
            identifier = raw_email.send(format)
            next if identifier.blank?
            begin
              remote_file(identifier).delete_now!
            rescue Excon::Error::BadRequest => e
              statsd_client.increment('scrub_job_helper.cannot_delete_email')
              Rails.logger.error "Cant delete email for ticket_id #{ticket.id} on account_id #{ticket.account_id} error: #{e.inspect}"
            end
          end
        end
      end
    end

    def remote_file(identifier)
      RemoteFiles::File.new(identifier,
        configuration: Zendesk::Mail.raw_remote_files.name,
        stored_in: Zendesk::Mail.raw_remote_files.stores)
    end

    def delete_voice_recordings(ticket)
      return unless ticket.account.has_lotus_feature_voice_soft_delete_recordings?
      return unless ticket.voice_comments.exists?

      api_client(ticket.account.subdomain).voice_soft_delete_recordings(ticket.nice_id)

      Rails.logger.debug("Soft deleting voice recordings for ticket: #{ticket.nice_id}")
    end

    def api_client(subdomain)
      Zendesk::Voice::InternalApiClient.new(subdomain)
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: %w[ticket_scrub deletion_job])
    end
  end
end
