require 'fog/aws'
require 'hc_uploader_configuration'

module Zendesk::Maintenance::Jobs
  class RemoveHCAttachmentsJob < BaseDataDeleteJob
    priority :account_deletion

    stage :independent

    private

    def erase_for_canceled
      raise "can't delete data from non soft deleted accounts" if account.deleted_at.nil?

      log_opts = {
        account: account.id,
        subdomain: account.subdomain,
        shard: account.shard_id,
        record_type: "hc_attachment_prefix",
        bucket: bucket,
        dry_run: dry_run?
      }

      prefixes.each do |prefix|
        log_opts[:prefix] = prefix
        key_log.info("HC Attachments Prefix Deleting", log_opts.merge(phase: "deleting"))
        delete_for_prefix(store, prefix) unless dry_run?
        key_log.info("HC Attachments Prefix Deleted", log_opts.merge(phase: "deleted"))
      end
    end

    def verify_for_canceled
      prefixes.each do |prefix|
        verify_for_prefix(account, store, prefix)
      end
    end

    def erase_for_moved
      # We don't remove hc attachments for moved accounts.
    end

    def delete_for_prefix(store, prefix)
      marker = ''
      while objects = store.directories.get(bucket, prefix: prefix, marker: marker).files.all.presence
        objects.each(&:destroy)
        marker = objects.last.key
      end
    end

    def verify_for_prefix(account, store, prefix)
      if store.directories.get(bucket, prefix: prefix).files.all.any?
        raise "RemoveHCAttachmentsJob failed for account #{account.idsub}: All objects not deleted"
      end
    end

    def prefixes
      %W[
        article_attachment/file/#{account.id}/
        settings_assets/#{account.id}/
        theme_assets/#{account.id}/
      ]
    end

    def store
      if account.has_help_center_replicated_s3_bucket?
        Fog::Storage.new(hc_replicated_bucket_credentials)
      else
        Fog::Storage.new(hc_bucket_credentials)
      end
    end

    def bucket
      if account.has_help_center_replicated_s3_bucket?
        HCUploaderConfiguration.replicated_bucket_name
      else
        HCUploaderConfiguration.bucket_name
      end
    end

    def hc_bucket_credentials
      {
        provider: 'AWS',
        region: HCUploaderConfiguration.region,
        aws_access_key_id: HCUploaderConfiguration.aws_access_key_id,
        aws_secret_access_key: HCUploaderConfiguration.aws_secret_access_key
      }
    end

    def hc_replicated_bucket_credentials
      {
        provider: 'AWS',
        region: HCUploaderConfiguration.replicated_region,
        aws_access_key_id: HCUploaderConfiguration.replicated_aws_access_key_id,
        aws_secret_access_key: HCUploaderConfiguration.replicated_aws_secret_access_key
      }
    end
  end
end
