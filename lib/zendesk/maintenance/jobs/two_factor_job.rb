module Zendesk::Maintenance::Jobs
  class TwoFactorJob < PerAccountJob
    class << self
      def work_on_account(account)
        return unless account.valid?

        before_value = account.settings.two_factor_all_agents # returns nil if no setting value stored yet
        account.settings.two_factor_all_agents = account.agents.all?(&:otp_configured?)

        if before_value != account.settings.two_factor_all_agents?
          account.settings.save!
        else
          account.settings.find_by_name('two_factor_all_agents').touch_without_callbacks
        end
      end

      def args_to_log
        {}
      end
    end
  end
end
