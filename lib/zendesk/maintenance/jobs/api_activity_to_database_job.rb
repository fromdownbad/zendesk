module Zendesk::Maintenance::Jobs
  class ApiActivityToDatabaseJob < PerAccountJob
    CLIENT_KEY_INDEX = 0
    CLIENT_TIMESTAMP_INDEX = 1
    CLIENT_DISPLAY_NAME_INDEX = 2
    CLIENT_TYPE_INDEX = 3
    CLIENT_CLIENT_ID_INDEX = 4
    HOURS_TO_STORE = 48

    class << self
      def work_on_account(account)
        resque_log("ApiActivityToDatabaseJob.work_on_account account: #{account.id} arturo: #{account.has_api_activity_job?}");
        return unless account.has_api_activity_job?

        hours_to_store = HOURS_TO_STORE
        update_clients(account, hours_to_store)
      end

      def account_key(account_id, api_activity_version)
        "api-activity/#{api_activity_version}/#{account_id}/keys"
      end

      def update_clients(account, hours_to_store)
        current_time = Time.now
        resque_log("ApiActivityToDatabaseJob.update_clients at #{current_time} on account: #{account.id}");
        client_keys = account_key(account.id, ApiActivityClient::API_ACTIVITY_COUNT_KEY_VERSION)
        account_keys = Rails.cache.read(client_keys) || {}
        previous_hour = current_time.to_i / 1.hour - 1
        unless account_keys.empty?
          account_keys.each_pair do |_key, client_profile|
            update_client(account, hours_to_store, client_profile, previous_hour)
          end
        end
      end

      def update_client(account, hours_to_store, client_profile, previous_hour)
        client_key = client_profile[CLIENT_KEY_INDEX]
        client = account.api_activity_clients.find_by_client_key(client_key)
        if client.blank?
          client = ApiActivityClient.new do |new_client|
            new_client.account_id = account.id
            new_client.client_key = client_key
            new_client.display_name = client_profile[CLIENT_DISPLAY_NAME_INDEX]
            new_client.client_type = client_profile[CLIENT_TYPE_INDEX]
            new_client.client_id = client_profile[CLIENT_CLIENT_ID_INDEX]
            new_client.data = "{}"
          end
        end
        do_update = client.update_client(previous_hour, Time.at(client_profile[CLIENT_TIMESTAMP_INDEX]), hours_to_store)
        begin
          client.save! if do_update
        rescue StandardError => e
          resque_error("ApiActivityToDatabaseJob: Failed to update client for account #{account.subdomain} due to the following error: #{e.message}")
        end
      end

      def args_to_log
        { job: name.demodulize, time: Time.now.to_s }
      end
    end
  end
end
