require 'zendesk/revere/has_api_client'

# This job runs per-pod, once a day
module Zendesk::Maintenance::Jobs
  class RevereSynchronizationJob < BaseJob
    extend Zendesk::Revere::HasApiClient

    class << self
      # The only thing we need to do daily, is ask Revere if anyone unsubscribed
      # via the link in the email.
      # This job asks Revere for that list, and then lets revere know it can
      # finish deleting those users afterwards.

      def work
        Rails.logger.info("Starting Revere Daily Sync Maintenance Job")

        sync_unsubscribes

        sync_account_moves
      end

      # from Revere, we get any users that unsubscribed via the link in email.
      def sync_unsubscribes
        unsubscribed_users = revere_api_client.unsubscribed
        deleted_users = []

        unsubscribed_users.each do |revere_user|
          account =
            if revere_api_client.arturo_enabled?(:new_subscribers_params)
              Account.find_by(subdomain: revere_user.subdomain)
            else
              Account.find(revere_user.zendesk_account_id)
            end
          next unless account.pod_local?

          user =
            if revere_api_client.arturo_enabled?(:new_subscribers_params)
              account.users.find { |usr| usr.email == revere_user.alert_destination }
            else
              account.users.where(id: revere_user.zendesk_user_id).first
            end
          next unless user
          user.settings.revere_subscription = false
          user.settings.save!

          deleted_users.append(revere_user.id)
        end

        revere_api_client.delete_unsubscribed(deleted_users) if deleted_users.any?
      end

      # we update subscriptions after a move, in case their pod changed.
      def sync_account_moves
        # process the last 2 days in case we miss a day.
        AccountMove.to_local_pod.where(state: "done").where("updated_at > ?", 2.days.ago).each do |am|
          account = am.account

          # This should be, at most, a few accounts per pod, per day, with actual subscribers.
          # Finally, a single account will never have more than a few subscriber itself.

          account.on_shard do
            account.revere_subscribers.each do |user|
              # issue a subscription update to get the pod corrected.
              revere_api_client.update_subscription(user, user.settings.revere_subscription)
            end
          end
        end
      end
    end
  end
end
