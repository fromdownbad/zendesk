module Zendesk::Maintenance::Jobs
  class RemoveCertificatesJob < BaseDataDeleteJob
    priority :account_deletion

    stage :db

    private

    def erase_for_canceled
      ActiveRecord::Base.transaction do
        account.certificates.each do |certificate|
          certificate.certificate_ips.each do |ip|
            release_ip(ip) unless dry_run?
            key_log.info("certificate_ip released", account: account.id, subdomain: account.subdomain, shard: account.shard_id, record_type: "certificate_ip", id: ip.id, dry_run: dry_run?)
          end
          certificate.destroy unless dry_run?
          key_log.info("certificate", account: account.id, subdomain: account.subdomain, shard: account.shard_id, record_type: "certificate", id: certificate.id, dry_run: dry_run?)
        end
      end
    end

    def erase_for_moved
      # We don't erase certificates for moved accounts
    end

    def release_ip(ip)
      ip.detach_from_certificate!
    end

    def verify_for_canceled
      raise "RemoveCertificatesJob failed for account #{account.id}" unless account.certificates.count(:all) == 0
    end
  end
end
