module Zendesk::Maintenance::Jobs
  class RemoveGooddataIntegrationJob < BaseDataDeleteJob
    priority :account_deletion

    stage :before_db

    def work
      statsd_client.increment('count')
      super
    end

    def handle_runtime_exception(_exception)
      statsd_client.increment('failed')
    end

    private

    def erase_for_canceled
      return unless account.gooddata_integration

      begin
        remove_gooddata_integration
        remove_gooddata_users
      rescue GoodData::Error::NotFound
        # The project doesnt exists at GD api side anymore. We can ignore these
        Rails.logger.warn("Skipping. GoodData project #{account.gooddata_integration.project_id} for account #{account.id} does not exist.")
      end
    end

    def verify_for_canceled
      # Manually deleted GD integrations leave a record in the gd table w/project_id = "-1" (as a string)
      return if account.gooddata_integration.nil? || account.gooddata_integration.project_id == "-1"

      verify_project_removed
    end

    def integration_provisioning
      @integration_provisioning ||= Zendesk::Gooddata::IntegrationProvisioning.new(account)
    end

    def remove_gooddata_integration
      # Manually deleted GD integrations leave a record in the gd table w/project_id = "-1" (as a string)
      if account.gooddata_integration.project_id == "-1"
        Rails.logger.warn("#{job_id_for_log}: Account #{account.id}, #{account.subdomain} has a gooddata project_id of -1")
        statsd_client.increment('invalid_project_id')
        return
      end

      integration_provisioning.destroy_gooddata_integration

      key_log.info(
        [
          'Gooddata Project Deleted',
          {
            account: account.id,
            subdomain: account.subdomain,
            shard: account.shard_id,
            record_type: 'gooddata_integration',
            storage_keys: account.gooddata_integration.project_id
          }
        ]
      )
    end

    def remove_gooddata_users
      Zendesk::Gooddata::UserProvisioning.destroy_for_account(account)
    end

    def verify_project_removed
      begin
        response = Zendesk::Gooddata::Client.v2.project(id: account.gooddata_integration.project_id).find
        unless response[:project][:content][:state] == 'DELETED' || response[:project][:content][:state] == 'ARCHIVED'
          raise "GoodData project #{account.gooddata_integration.project_id} for account #{account.id} could not be destroyed."
        end
      rescue GoodData::Error::NotFound
        # The project doesnt exists at GD api side anymore. We can ignore these
        Rails.logger.info("GoodData project #{account.gooddata_integration.project_id} for account #{account.id} not found.")
      end

      true
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(
        namespace: ['jobs', 'remove_gooddata_integration_job']
      )
    end
  end
end
