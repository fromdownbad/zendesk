module Zendesk::Maintenance::Jobs
  class SuspendedTicketNotificationJob < PerShardJob
    class << self
      def work_on_shard(_shard)
        send_suspended_ticket_notifications
      end

      def send_suspended_ticket_notifications
        on_eligible_accounts do |notification, account|
          next unless account.shard_local?

          count = suspended_ticket_count(account, notification.last_sent_at)
          if count > 0
            tickets = recent_suspended_tickets(account, notification.last_sent_at)
            SuspendedTicketsMailer.deliver_suspended_notification(account, tickets, count)
          end

          update_last_sent_at(notification)

          # temporary logging
          if account.id == 73767
            resque_log("SuspendedTicketNotificationJob for account 73767 happened at #{Time.now}")
          end
        end
      end

      def on_eligible_accounts
        condition = ["frequency != 0 AND IFNULL(email_list, '') != '' AND DATE_ADD(last_sent_at, INTERVAL frequency MINUTE) <= ?", Time.now.utc]
        Account.without_locking do
          SuspendedTicketNotification.includes(:account).where(condition).find_each do |notification| # I need the find_each because of the batch_size and the order clause
            resque_log("SuspendedTicketNotification #{notification.id} for account #{notification.account_id}")
            next if !notification.account || notification.account.is_locked?
            yield(notification, notification.account)
          end
        end
      end

      def suspended_ticket_count(account, since)
        account.suspended_tickets.where("created_at >= ?", since).count(:all)
      end

      def recent_suspended_tickets(account, since)
        account.suspended_tickets.where("created_at >= ?", since).order("created_at DESC").limit(10).to_a
      end

      def update_last_sent_at(notification)
        # will be 1 > if workers go down for more than a frequency
        frequencies_behind = (Time.now.to_i - notification.last_sent_at.to_i) / notification.frequency.minutes
        additional_time = (notification.frequency.minutes * frequencies_behind)
        notification.update_attribute(:last_sent_at, (notification.last_sent_at + additional_time).floor(10.minutes))
      end
    end
  end
end
