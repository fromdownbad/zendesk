module Zendesk::Maintenance::Jobs
  class UnsubscribeTwitterJob < BaseDataDeleteJob
    priority :account_deletion
    stage :before_db

    private

    def erase_for_canceled
      return true unless Arturo.feature_enabled_for?(:channels_enable_twitter_events_endpoint, account)

      twitter_proxy_client = Channels::TwitterAPI::TwitterProxy::InternalAPIClient.new(account.id)
      MonitoredTwitterHandle.where(account_id: account.id).each do |twitter_handle|
        twitter_proxy_client.unsubscribe(twitter_handle.twitter_user_id)
      end

      true
    end

    def verify_for_canceled
      true
    end
  end
end
