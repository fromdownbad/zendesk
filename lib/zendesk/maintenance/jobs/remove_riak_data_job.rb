module Zendesk::Maintenance::Jobs
  class RemoveRiakDataJob < BaseDataDeleteJob
    priority :account_deletion

    stage :before_db

    private

    def erase_for_canceled
      do_erase(account.shard_id)
    end

    def verify_for_canceled
      do_verify(account.shard_id)
    end

    def erase_for_moved
      if pod_local_move? || dry_run?
        false
      else
        do_erase(ActiveRecord::Base.current_shard_id)
      end
    end

    def verify_for_moved
      if pod_local_move? || dry_run?
        false
      else
        do_verify(ActiveRecord::Base.current_shard_id)
      end
    end

    def do_erase(shard_id)
      ActiveRecord::Base.on_shard(shard_id) do
        account.ticket_archive_stubs.find_in_batches(batch_size: 2000) do |batch|
          raise KillSwitchEnabled if kill_switch_flipped?
          batch.each do |ticket_archive_stub|
            ticket_archive_stub.remove_from_archive unless dry_run?
            key_log.info('Riak Key Deleted',
              account:     account.id,
              subdomain:   account.subdomain,
              shard:       account.shard_id,
              record_type: 'ticket_archive_stub',
              archive_key: "#{ticket_archive_stub.id}/#{ticket_archive_stub.key_version}",
              dry_run:     dry_run?)
          end

          TicketArchiveStub.delete(batch.map(&:id)) unless dry_run?
        end
      end
    end

    def do_verify(shard_id)
      ActiveRecord::Base.on_shard(shard_id) do
        account.ticket_archive_stubs.find_in_batches(batch_size: 2000) do |batch|
          raise KillSwitchEnabled if kill_switch_flipped?
          batch.each do |ticket_archive_stub|
            raise "RemoveRiakDataJob failed for account #{account.idsub}: Archived tickets still in Riak." if ticket_archive_stub.exists_in_archive?
          end
        end
      end
    end
  end
end
