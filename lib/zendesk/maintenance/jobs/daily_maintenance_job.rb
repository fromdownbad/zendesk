module Zendesk::Maintenance::Jobs
  class DailyMaintenanceJob < Zendesk::Maintenance::Jobs::BaseJob
    # Billing Jobs
    BILLING_JOBS = [
      Zendesk::Maintenance::Jobs::UnsponsorJob,
      Zendesk::Maintenance::Jobs::TrialExpiryJob,
      Zendesk::Maintenance::Jobs::BoostExpiryJob,
      Zendesk::Maintenance::Jobs::DailyReportJob,
      Zendesk::Maintenance::Jobs::ZopimStatusSynchronizerJob
    ].freeze

    # Billing Dependent Jobs
    BILLING_DEPENDENT_JOBS = [
      Zendesk::Maintenance::Jobs::SecurityPolicyMailerJob,
      Zendesk::Maintenance::Jobs::HighSecurityPolicyWarningJob,
      Zendesk::Maintenance::Jobs::ShardedSubscriptionJob
    ].freeze

    # Other Jobs(Not billing dependent)
    OTHER_JOBS = [
      Zendesk::Maintenance::Jobs::ComplianceMoveJob,
      Zendesk::Maintenance::Jobs::ClearInactiveDeviceTokensJob,
      Zendesk::Maintenance::Jobs::CertificateMaintenanceJob
    ].freeze

    ALL_JOBS = (BILLING_JOBS + BILLING_DEPENDENT_JOBS + OTHER_JOBS).freeze

    class << self
      def work(dryrun = false, console = false, time = Time.now)
        ALL_JOBS.each do |job|
          execute_job(job, dryrun, console, time)
        end
      end

      private

      def execute_job(job, dryrun, console, time)
        log_state(job, 'starting')
        job.execute(dryrun, console, time)
        log_state(job, 'finishing')
      rescue StandardError => e
        ZendeskExceptions::Logger.record(e, location: self, message: "Error processing job #{job.name} at #{Time.now}", fingerprint: '8b61db0135ca69b89c3dc6f022575091fcb994b3')
      end

      def log_state(job, state)
        Rails.logger.info("DailyMaintenanceJob (pid: #{Process.pid}) #{state} job #{job.name} at #{Time.now}")
      end
    end
  end
end
