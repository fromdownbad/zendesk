module Zendesk::Maintenance::Jobs
  # Query Zopim reseller API for accounts in trial and update the status for
  # those who expired
  class ZopimStatusSynchronizerJob < BaseMaintenanceJob
    def execute
      expire_zopim_trials unless dryrun?
    end

    private

    def expire_zopim_trials
      duration = Benchmark.realtime do
        logger.info("Expiring #{expired_subscriptions.count} subscription(s)")
        expired_subscriptions.each do |sub|
          expire_zopim_trial!(sub)
        end
      end
      logger.info("#{self.class.name} in #{duration}s")
    end

    def expire_zopim_trial!(subscription)
      logger.info('Expiring Zopim subscription for account ' \
        "#{subscription.account.id}")
      subscription.account.on_shard { subscription.expire_trial! }
    rescue => e
      logger.warn("Error expiring subscription #{subscription.id}!")
      message = <<-TEXT.squish
        WARNING: Zendesk account #{subscription.account.id} has a Zopim
        subscription record association, but the sync failed, likely due to
        a discrepancy in the list of Zopim agents (no agent or no owner).
      TEXT
      ZendeskExceptions::Logger.record(e, location: self, message: message, fingerprint: '2b8f47e14a3de4625febd4b20b0c0113900cb0c8')
    end

    def expired_subscriptions
      @subscriptions_to_expire ||= trial_subscriptions.map do |subscription|
        subscription if should_expire?(subscription)
      end.compact
    end

    def should_expire?(subscription)
      return if subscription.account.nil? || !subscription.account.pod_local?
      return if phase_three?(subscription)
      over_extended?(subscription) || lite_in_django?(subscription)
    end

    def phase_three?(subscription)
      phase_three = subscription.account.on_shard { subscription.phase_three? }
      if phase_three
        logger.info('Skipping phase three account for subscription ' \
          "#{subscription.id}!")
      end
      phase_three
    end

    def over_extended?(subscription)
      (subscription.created_at + ZBC::Zopim::Subscription::TrialManagement::
        MAX_TRIAL_LENGTH_IN_DAYS.days) < Date.today
    end

    def trial_subscriptions
      @trial_subscriptions ||= ZBC::Zopim::Subscription.where(
        plan_type: ZBC::Zopim::PlanType::Trial.name
      )
    end

    def lite_in_django?(subscription)
      logger.info("Looking up Zopim for ID #{subscription.zopim_account_id}")
      zopim_account = reseller_data(subscription)
      if zopim_account.blank?
        message = <<-TEXT.squish
           WARNING: Zendesk account #{subscription.account.id} has a Zopim
           subscription record association but the Zopim Reseller API is unable
           to find that record.
        TEXT
        ZendeskExceptions::Logger.record(Exception.new, location: self, message: message, fingerprint: '8d587912ac127f5c6a028d9ab78d16f42e775e09')
        false
      else
        zopim_account.plan == ZBC::Zopim::PlanType::Lite.name
      end
    end

    def reseller_data(subscription)
      subscription.reseller_data
    rescue
      nil
    end
  end
end
