# rubocop:disable Naming/PredicateName
# rubocop:disable Naming/AccessorMethodName
require "date"

module Zendesk::Fom # Field Object Model
  class Field
    extend ActionView::Helpers::TagHelper, ActionView::Helpers::UrlHelper, TicketsHelper, UserInterfaceHelper, TimeDateHelper, ERB::Util
    include ERB::Util

    cattr_reader :subclasses # Made available for Fom unit test

    def self.for(ident)
      return nil if ident.nil?
      if match = @@subclasses.detect { |klass| !ident.is_a?(Integer) && klass.matcher.member?(ident.to_sym) }
        match # Class field
      elsif ident.is_a?(TicketField) || ident.to_s =~ /\A-?\d+\Z/
        instance = CustomField.new(ident) # Instance field
        instance.ticket_field ? instance : nil
      end
    end

    def self.value(ticket)
      ticket.send(vars[:db_field])
    end

    def self.is_blank?(ticket)
      value(ticket).blank?
    end

    def self.humanize_value(v)
      v
    end

    def self.value_for_csv_column(ticket)
      v = value(ticket)
      if v.is_a?(Time)
        v.in_time_zone.to_s(:csv)
      else
        v.blank? ? '-' : v.to_s
      end
    end

    def self.is_active?
      true
    end

    def self.event_to_s(_event, _options = {})
      ''
    end

    def self.can_be
      {}
    end

    def self.is_a_date?
      [CreatedField, UpdatedField, RequesterUpdatedField, AssigneeUpdatedField, AssignedField, SolvedField, DueDateField, ResolutionField].member?(self)
    end

    def self.label
      vars[:label] || I18n.t("ticket_fields.#{vars[:i18n_key]}.label")
    end

    def self.title
      vars[:title] || label
    end

    def self.csv_field_alias
      vars[:join_field_alias].split(' AS ') if csv_aliased?
    end

    def self.csv_aliased?
      !!vars[:join_field_alias]
    end

    def self.csv_join_field_alias
      if csv_aliased?
        column_name, aliased_name = csv_field_alias
        null_value = Ticket.connection.quote(value_for_csv_column(Ticket.new) || '')
        "IFNULL(#{column_name}, #{null_value}) AS #{aliased_name}"
      end
    end

    def self.csv_join
      vars[:join]
    end

    def self.sortable?(_current_user_is_agent)
      true
    end

    # private

    def self.matcher(matchers = nil)
      @matchers ||= matchers
    end

    def self.inherited(subclass)
      (@@subclasses ||= []) << subclass
    end

    def self.method_missing(method, *args)
      if vars.keys.index(method.to_sym)
        vars[method.to_sym]
      else
        super
      end
    end

    def self.order_by(desc = nil)
      (db_field || identifier) + (desc ? " DESC" : " ASC")
    end

    def self.setup(options)
      Field.set_options(options)
    end

    def self.set_options(options)
      {
        identifier: options[:identifier],
        db_field: options[:db_field],
        label: options[:label],
        title: options[:title],
        presentation_type: options[:presentation_type],
        join: options[:join],
        join_field_alias: options[:join_field_alias],
        i18n_key: options[:i18n_key] || options[:identifier]
      }
    end
  end

  #############################################################################
  #############################################################################
  #############################################################################

  class SatisfactionProbabilityField < Field
    matcher [:satisfaction_probability]

    def self.vars
      setup(
        identifier: 'satisfaction_probability',
        db_field: 'satisfaction_probability'
      )
    end
  end

  class TranslationLocaleField < Field
    matcher [:locale_id]

    def self.value(ticket)
      ticket.translation_locale.present? ? ticket.translation_locale.localized_name(display_in: I18n.translation_locale) : nil
    end

    def self.sortable?(_current_user_is_agent)
      true
    end

    def self.vars
      setup(identifier: 'locale_id', db_field: 'locale_id')
    end
  end

  class SubjectField < Field
    matcher [:description, :subject, :FieldSubject]

    def self.vars
      setup(
        identifier: 'description',
        db_field: 'subject',
        i18n_key: 'subject',
        presentation_type: 'Text'
      )
    end

    def self.value_for_csv_column(ticket)
      ticket.title(200)
    end

    def self.can_be
      {
        deactivated: true,
        activated: true,
        visible_in_portal_edited: true,
        editable_in_portal_edited: true,
        required_in_portal_edited: true,
        required_edited: true
      }
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        I18n.t('txt.lib.zendesk.fom.field.subject_set_to_new_subject', new_subject: event.value) unless event.value.blank?
      elsif !(event.value_previous.nil? && event.value == '')
        I18n.t('txt.lib.zendesk.fom.field.subject_changed', old_subject: event.value_previous, new_subject: event.value)
      end
    end
  end

  class DescriptionField < Field
    matcher [:FieldDescription]

    def self.vars
      setup(
        identifier: 'description_full',
        i18n_key: 'description',
        db_field: 'description',
        presentation_type: 'Multi-line text'
      )
    end

    def self.is_blank?(_ticket)
      false
    end

    def self.can_be
      {}
    end
  end

  class StatusField < Field
    matcher [:status, :status_id, :FieldStatus]

    def self.vars
      setup(
        identifier: 'status',
        db_field: 'status_id',
        presentation_type: 'Drop-down'
      )
    end

    def self.can_be
      {}
    end

    def self.value_for_csv_column(ticket)
      humanize_value(ticket.status_id) || '-'
    end

    def self.humanize_value(v)
      StatusType[v].localized_name
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        I18n.t('txt.lib.zendesk.fom.field.set_new_status', new_status: I18n.t("txt.admin.helpers.ticket_helper.status_#{StatusType.to_s(event.value.to_i).downcase}"))
      elsif StatusType.to_s(event.value_previous.to_i) == '-'
        I18n.t('txt.lib.zendesk.fom.field.status_changed', old_status: StatusType.to_s(event.value_previous.to_i), new_status: I18n.t("txt.admin.helpers.ticket_helper.status_#{StatusType.to_s(event.value.to_i).downcase}"))
      else
        I18n.t('txt.lib.zendesk.fom.field.status_changed', old_status: I18n.t("txt.admin.helpers.ticket_helper.status_#{StatusType.to_s(event.value_previous.to_i).downcase}"), new_status: I18n.t("txt.admin.helpers.ticket_helper.status_#{StatusType.to_s(event.value.to_i).downcase}"))
      end
    end
  end

  class TicketTypeField < Field
    matcher [:type, :ticket_type, :ticket_type_id, :FieldTicketType]

    def self.vars
      setup(
        identifier: 'type',
        db_field: 'ticket_type_id',
        title: I18n.t('txt.admin.lib.zendesk.form.field.ticket_type_label'),
        presentation_type: 'Drop-down'
      )
    end

    def self.humanize_value(v)
      TicketType[v].localized_name
    end

    def self.is_blank?(ticket)
      ticket.ticket_type?('-')
    end

    def self.value_for_csv_column(ticket)
      ticket.ticket_type || '-'
    end

    def self.can_be
      {
        deactivated: true,
        activated: true,
        visible_in_portal_edited: true,
        editable_in_portal_edited: true,
        required_in_portal_edited: true,
        required_edited: true
      }
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        event.value.to_i == 0 ? '' : I18n.t('txt.lib.zendesk.fom.field.set_new_type', new_type: I18n.t("type.ticket.#{TicketType.to_s(event.value.to_i).downcase}"))
      elsif TicketType.to_s(event.value_previous.to_i) == '-'
        I18n.t('txt.lib.zendesk.fom.field.type_changed', old_type: TicketType.to_s(event.value_previous.to_i), new_type: I18n.t("type.ticket.#{TicketType.to_s(event.value.to_i).downcase}"))
      else
        I18n.t('txt.lib.zendesk.fom.field.type_changed', old_type: I18n.t("type.ticket.#{TicketType.to_s(event.value_previous.to_i).downcase}"), new_type: I18n.t("type.ticket.#{TicketType.to_s(event.value.to_i).downcase}"))
      end
    end
  end

  class PriorityField < Field
    matcher [:priority, :priority_id, :FieldPriority]

    def self.vars
      setup(
        identifier: 'priority',
        db_field: 'priority_id',
        presentation_type: 'Drop-down'
      )
    end

    def self.value_for_csv_column(ticket)
      ticket.priority
    end

    def self.is_blank?(ticket)
      ticket.priority?('-')
    end

    def self.can_be
      {
        deactivated: true,
        activated: true,
        visible_in_portal_edited: true,
        editable_in_portal_edited: true,
        required_in_portal_edited: true,
        required_edited: true
      }
    end

    def self.humanize_value(v)
      PriorityType[v].localized_name
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        event.value.to_i == 0 ? '' : I18n.t('txt.lib.zendesk.fom.field.new_priority', new_priority: I18n.t("type.priority.#{PriorityType.to_s(event.value.to_i).downcase}"))
      else
        if PriorityType.to_s(event.value_previous.to_i) == '-'
          priorities = { old_priority: PriorityType.to_s(event.value_previous.to_i), new_priority: I18n.t("type.priority.#{PriorityType.to_s(event.value.to_i).downcase}") }
        else
          priorities = { old_priority: I18n.t("type.priority.#{PriorityType.to_s(event.value_previous.to_i).downcase}") }
          priorities[:new_priority] = if PriorityType.to_s(event.value.to_i) == '-'
            PriorityType.to_s(event.value.to_i)
          else
            I18n.t("type.priority.#{PriorityType.to_s(event.value.to_i).downcase}")
          end
        end
        I18n.t('txt.lib.zendesk.fom.field.priority_changed', priorities)
      end
    end
  end

  class GroupField < Field
    matcher [:group, :group_id, :FieldGroup]

    def self.vars
      setup(
        identifier: 'group',
        db_field: 'group_id',
        presentation_type: 'Drop-down',
        join: 'LEFT OUTER JOIN groups ON groups.id = tickets.group_id',
        join_field_alias: 'groups.name AS group_name'
      )
    end

    def self.order_by(desc = nil)
      'group_name' + (desc ? ' DESC' : ' ASC')
    end

    def self.value_for_csv_column(ticket)
      (ticket['group_name'] || '-')
    end

    def self.humanize_value(v)
      unless v.nil?
        group = Group.find_by_id(v)
        return group.name if group
      end
      ''
    end

    def self.can_be
      {visible_in_portal_edited: true}
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        event.value.blank? ? '' : I18n.t('txt.lib.zendesk.fom.field.new_group', new_group: Group.identity(event.value.to_i))
      else
        I18n.t('txt.lib.zendesk.fom.field.group_change', old_group: Group.identity(event.value_previous.to_i), new_group: Group.identity(event.value.to_i))
      end
    end
  end

  class AssigneeField < Field
    matcher [:assignee, :assignee_id, :FieldAssignee]

    def self.vars
      setup(
        identifier: 'assignee',
        db_field: 'assignee_id',
        title: I18n.t('txt.admin.lib.zendesk.form.field.assignee_label'),
        presentation_type: 'Drop-down',
        join: 'LEFT OUTER JOIN users as assignee_ ON assignee_.id = tickets.assignee_id',
        join_field_alias: 'assignee_.name AS assignee_name'
      )
    end

    def self.order_by(desc = nil)
      'assignee_name' + (desc ? ' DESC' : ' ASC')
    end

    def self.can_be
      {visible_in_portal_edited: true}
    end

    def self.value_for_csv_column(ticket)
      ticket['assignee_name']
    end

    def self.humanize_value(v)
      User.find(v).name
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        event.value.blank? ? '' : I18n.t('txt.lib.zendesk.fom.field.new_assigned', agent_name: User.identity(event.value.to_i))
      else
        I18n.t('txt.lib.zendesk.fom.field.assignee_change', old_agent_name: User.identity(event.value_previous.to_i), new_agent_name: User.identity(event.value.to_i))
      end
    end
  end

  class RequesterField < Field
    matcher [:requester, :requester_id]

    def self.vars
      setup(
        identifier: 'requester',
        db_field: 'requester_id',
        join: 'LEFT OUTER JOIN users as requester_ ON requester_.id = tickets.requester_id',
        join_field_alias: 'requester_.name AS req_name'
      )
    end

    def self.order_by(desc = nil)
      'req_name' + (desc ? ' DESC' : ' ASC')
    end

    def self.value_for_csv_column(ticket)
      ticket['req_name']
    end

    def self.humanize_value(v)
      User.find(v).name
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        I18n.t('txt.lib.zendesk.fom.field.new_requester', new_requester: User.find(event.value).name)
      else
        I18n.t('txt.lib.zendesk.fom.field.requester_change', old_requester: User.find(event.value_previous.to_i).name, new_requester: User.find(event.value.to_i).name)
      end
    end
  end

  class SubmitterField < Field
    matcher [:submitter, :submitter_id]

    def self.vars
      setup(
        identifier: 'submitter',
        db_field: 'submitter_id',
        join: 'LEFT OUTER JOIN users as submitter_ ON submitter_.id = tickets.submitter_id',
        join_field_alias: 'submitter_.name AS submitter_name'
      )
    end

    def self.value_for_csv_column(ticket)
      ticket['submitter_name'] || I18n.t("txt.admin.models.user.identification.deleted_label")
    end

    def self.humanize_value(v)
      User.find(v).name
    end
  end

  class ScoreField < Field
    matcher [:score]

    def self.vars
      setup(
        identifier: 'score',
        db_field: 'base_score'
      )
    end

    def self.value_for_csv_column(ticket)
      ticket.score
    end

    # def self.order_by(desc = nil)
    #  desc ? 'base_score DESC, id ASC' : 'base_score ASC, tickets.id DESC'
      # desc ? 'base_score DESC' : 'base_score ASC'
    # end
  end

  class SatisfactionRatingField < Field
    matcher [:satisfaction_score]

    def self.vars
      setup(identifier: 'satisfaction_score', db_field: 'satisfaction_score')
    end

    def self.event_to_s(event, _options = {})
      if event.value.blank?
        "-"
      elsif [nil, 0, ""].include?(event.value_previous)
        I18n.t("txt.lib.zendesk.fom.field.customer_satisfaction_feedback_#{SatisfactionType.to_s(event.value.to_i).downcase}")
      else
        previous_satisfaction_translated = I18n.t(SatisfactionType[event.value_previous.to_i].translation_key)
        new_satisfaction_translated = I18n.t(SatisfactionType[event.value.to_i].translation_key)
        I18n.t('txt.lib.zendesk.fom.field.customer_satisfaction_feedback_from_to', previous_rating: previous_satisfaction_translated, new_rating: new_satisfaction_translated)
      end
    end
  end

  class SatisfactionCommentField < Field
    matcher [:satisfaction_comment]

    def self.vars
      setup(identifier: 'satisfaction_comment', db_field: 'satisfaction_comment')
    end

    def self.event_to_s(event, _options = {})
      "#{I18n.t('txt.lib.zendesk.fom.field.satisfaction_comment')} #{event.value}"
    end
  end

  class SatisfactionReasonField < Field
    matcher [:satisfaction_reason_code]

    def self.vars
      setup(identifier: 'satisfaction_reason_code', db_field: 'satisfaction_reason_code')
    end

    def self.event_to_s(event, _options = {})
      "#{I18n.t('txt.lib.zendesk.fom.field.customer_satisfaction_reason_code_is')} #{event.value}"
    end
  end

  class NiceIdField < Field
    matcher [:nice_id]

    def self.vars
      setup(identifier: 'nice_id', db_field: 'nice_id')
    end

    def self.order_by(desc = nil)
      'tickets.nice_id' + (desc ? ' DESC' : ' ASC')
    end
  end

  class CreatedField < Field
    matcher [:created, :created_at]

    def self.vars
      setup(
        identifier: 'created',
        db_field: 'created_at',
        title: I18n.t('txt.admin.models.rules.rule_dictionary.request_date_label')
      )
    end

    def self.order_by(desc = nil)
      # Re-enabling order by created_at because those goddamn API users can set the created_at
      # date to effectively bring that out of sync with the id ordering.
      # 'tickets.created_at' + (desc ? ' DESC' : ' ASC')
      'tickets.created_at' + (desc ? ' DESC' : ' ASC')
    end
  end

  class UpdatedField < Field
    matcher [:updated, :updated_at]

    def self.vars
      setup(
        identifier: 'updated',
        db_field: 'updated_at',
        title: I18n.t('txt.admin.lib.zendesk.form.field.latest_update_label')
      )
    end

    def self.order_by(desc = nil)
      'tickets.updated_at' + (desc ? ' DESC' : ' ASC')
    end
  end

  class RequesterUpdatedField < Field
    matcher [:updated_requester, :requester_updated_at]

    def self.vars
      setup(
        identifier: 'updated_requester',
        db_field: 'requester_updated_at',
        title: I18n.t('txt.admin.lib.zendesk.form.field.latest_update_by_requester_label')
      )
    end
  end

  class AssigneeUpdatedField < Field
    matcher [:updated_assignee, :assignee_updated_at]

    def self.vars
      setup(
        identifier: 'updated_assignee',
        db_field: 'assignee_updated_at',
        title: I18n.t('txt.admin.lib.zendesk.form.field.latest_update_by_assignee_label')
      )
    end
  end

  class AssignedField < Field
    matcher [:assigned, :assigned_at]

    def self.vars
      setup(
        identifier: 'assigned',
        db_field: 'assigned_at',
        title: I18n.t('txt.admin.lib.zendesk.form.field.assigned_date_label')
      )
    end
  end

  class SolvedField < Field
    matcher [:solved, :solved_at]

    def self.sortable?(current_user_is_agent)
      current_user_is_agent
    end

    def self.vars
      setup(
        identifier: 'solved',
        db_field: 'solved_at',
        title: I18n.t('txt.admin.lib.zendesk.form.field.solved_date_label')
      )
    end
  end

  class ResolutionField < Field
    matcher [:resolution_time]

    def self.vars
      setup(
        identifier: 'resolution_time',
        db_field: 'resolution_time',
        title: I18n.t('txt.admin.lib.zendesk.form.field.resolution_time_label')
      )
    end
  end

  class DueDateField < Field
    matcher [:due_date]

    def self.vars
      setup(
        identifier: 'due_date',
        db_field: 'due_date'
      )
    end

    def self.event_to_s(event, _options = {})
      return "" if event.value.nil?
      new_due_date = Time.parse(event.value).to_format
      if event.value_previous.nil?
        I18n.t('txt.lib.zendesk.fom.field.new_due_date', new_due_date: new_due_date)
      else
        old_due_date = Time.parse(event.value_previous).to_format
        I18n.t('txt.lib.zendesk.fom.field.due_date_change', old_due_date: old_due_date, new_due_date: new_due_date)
      end
    end
  end

  class OrganizationField < Field
    matcher [:organization, :organisation, :organization_id]

    def self.value_for_csv_column(ticket)
      ticket['organization_name']
    end

    def self.vars
      setup(
        identifier: 'organization',
        db_field: 'organization_id',
        join: 'LEFT OUTER JOIN organizations ON organizations.id = tickets.organization_id',
        join_field_alias: 'organizations.name AS organization_name'
      )
    end

    def self.order_by(desc = nil)
      'organization_name' + (desc ? ' DESC' : ' ASC')
    end

    def self.event_to_s(event, _options = {})
      new_org = Organization.with_deleted { Organization.find_by_id(event.value.to_i) }
      if event.is_a?(Create) || event.value_previous.nil?
        event.value.blank? ? '' : I18n.t('txt.lib.zendesk.fom.field.new_organization', new_org: new_org.name)
      else
        old_org = Organization.with_deleted { Organization.find_by_id(event.value_previous.to_i) }

        if event.value.nil?
          I18n.t('txt.lib.zendesk.fom.field.organization_change', old_org: old_org.name, new_org: '-')
        else
          I18n.t('txt.lib.zendesk.fom.field.organization_change', old_org: old_org.name, new_org: new_org.name)
        end
      end
    end
  end

  class SlaNextBreachAtField < Field
    matcher [:sla_next_breach_at]

    def self.vars
      setup(
        identifier: 'sla_next_breach_at',
        db_field: 'sla_breach_status'
      )
    end

    def self.value(ticket)
      ticket.sla.next_breach_at
    end
  end

  class UpdatedByTypeField < Field
    matcher [:updated_by_type, :updated_by_type_id]

    def self.value(ticket)
      ticket.updated_by_type == 'Agent' ? 'Agent' : 'User'
    end

    def self.vars
      setup(
        identifier: 'updated_by_type',
        db_field: 'updated_by_type_id',
        title: I18n.t('txt.admin.lib.zendesk.form.field.latest_updater_type')
      )
    end
  end

  class LinkedField < Field
    matcher [:linked, :linked_id]

    def self.vars
      setup(
        identifier: 'linked',
        db_field: 'linked_id'
      )
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        event.value.blank? ? '' : I18n.t('txt.lib.zendesk.fom.field.linked_to_problem', ticket_id: Ticket.find(event.value.to_i).nice_id)
      else
        linked_previous_value =
          if !event.value_previous.blank? && link_previous = Ticket.find_by_id(event.value_previous.to_i)
            link_previous.nice_id
          else
            ''
          end
        linked_value =
          if !event.value.blank? && link_to = Ticket.find_by_id(event.value.to_i)
            link_to.nice_id
          else
            ''
          end
        I18n.t('txt.lib.zendesk.fom.field.problem_changed', old_problem: linked_previous_value, new_problem: linked_value)
      end
    end
  end

  class TagsField < Field
    matcher [:current_tags, :tags, :tag_list]

    def self.vars
      setup(
        identifier: 'current_tags',
        db_field: 'current_tags'
      )
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        event.value.blank? ? '' : I18n.t('txt.lib.zendesk.fom.field.new_tags', tags: event.value)
      else
        I18n.t('txt.lib.zendesk.fom.field.change_tags', old_tags: event.value_previous, new_tags: event.value)
      end
    end
  end

  class CollaboratorsField < Field
    matcher [:current_collaborators, :collaborations]

    def self.vars
      setup(
        identifier: 'current_collaborators',
        db_field: 'current_collaborators'
      )
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        event.value.blank? ? '' : I18n.t('txt.lib.zendesk.fom.field.new_cc', new_cc: event.value)
      else
        I18n.t('txt.lib.zendesk.fom.field.cc_change', old_cc: event.value_previous, new_cc: event.value)
      end
    end
  end

  class TicketFormField < Field
    matcher [:ticket_form, :ticket_form_id]

    def self.vars
      setup(
        identifier: 'ticket_form',
        db_field: 'ticket_form_id',
        join: 'LEFT OUTER JOIN ticket_forms ON ticket_forms.id = tickets.ticket_form_id',
        join_field_alias: 'ticket_forms.name AS ticket_form_name'
      )
    end

    def self.value_for_csv_column(ticket)
      (ticket['ticket_form_name'] || '-')
    end

    def self.humanize_value(id)
      name = TicketForm.find_by_id(id).try(:name) if id
      name || '-'
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create) || event.value_previous.nil?
        I18n.t('txt.lib.zendesk.fom.field.set_new_ticket_form', form_name: humanize_value(event.value))
      else
        I18n.t('txt.lib.zendesk.fom.field.ticket_form_changed', old_form: humanize_value(event.value_previous), new_form: humanize_value(event.value))
      end
    end
  end

  class TicketBrandField < Field
    matcher [:brand, :brand_id]

    def self.vars
      setup(
        identifier: 'brand',
        db_field: 'brand_id',
        join: 'LEFT OUTER JOIN brands ON brands.id = tickets.brand_id',
        join_field_alias: 'brands.name AS brand_name'
      )
    end

    def self.value_for_csv_column(ticket)
      (ticket['brand_name'] || '-')
    end

    def self.humanize_value(id)
      name = Brand.with_deleted { Brand.find_by_id(id) }.try(:name) if id
      name || '-'
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create) || event.value_previous.nil?
        I18n.t('txt.lib.zendesk.fom.field.set_new_brand', brand_name: humanize_value(event.value))
      else
        I18n.t(
          'txt.lib.zendesk.fom.field.ticket_brand_changed',
          old_brand_name: humanize_value(event.value_previous),
          new_brand_name: humanize_value(event.value)
        )
      end
    end
  end

  class ChannelField < Field
    matcher [:via, :via_id]

    def self.humanize_value(_id)
      name || '-'
    end

    def self.vars
      setup(identifier: 'via', db_field: 'via_id')
    end
  end

  class SlaPolicyField < Field
    matcher [:sla_policy]

    def self.vars
      setup(
        identifier: 'sla_policy',
        db_field: 'sla_ticket_policy',
        join: 'LEFT OUTER JOIN sla_ticket_policies ON sla_ticket_policies.ticket_id = tickets.id JOIN sla_policies ON sla_policies.id = sla_ticket_policies.sla_policy_id',
        join_field_alias: 'sla_policies.title AS sla_policy_title'
      )
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create)
        event.value.blank? ? '' : I18n.t('txt.lib.zendesk.fom.field.new_sla_policy', new_policy: event.value)
      elsif event.value_previous.present?
        I18n.t('txt.lib.zendesk.fom.field.sla_policy_change_from_to', old_policy: event.value_previous, new_policy: event.value)
      else
        I18n.t('txt.lib.zendesk.fom.field.sla_policy_change_to', old_policy: event.value_previous, new_policy: event.value)
      end
    end
  end

  class TicketPublicityField < Field
    matcher [:is_public]

    def self.vars
      setup(identifier: 'is_public', db_field: 'is_public')
    end

    def self.value_for_csv_column(ticket)
      (ticket['is_public'] || '-')
    end

    def self.humanize_value(value)
      %w[0 f].include?(value) ? I18n.t('txt.lib.zendesk.fom.field.publicity_no') : I18n.t('txt.lib.zendesk.fom.field.publicity_yes')
    end

    def self.event_to_s(event, _options = {})
      if event.is_a?(Create) || event.value_previous.nil?
        I18n.t('txt.lib.zendesk.fom.field.publicity_set', is_public_value: humanize_value(event.value))
      else
        I18n.t('txt.lib.zendesk.fom.field.publicity_change', old_is_public_value: humanize_value(event.value_previous), new_is_public_value: humanize_value(event.value))
      end
    end
  end

  class AttributesMatchField < Field
    matcher [:attributes_match]

    def self.vars
      # It seems like `db_field` is a required field, even though we don't use it
      setup(identifier: 'attributes_match', db_field: 'id')
    end

    def self.value(_ticket)
      # A dummy value, to be replaced with a value from Deco
      '✓'
    end
  end

  ########################
  ##### Custom fields
  ########################

  class CustomField < Field
    matcher [:custom]

    def initialize(ticket_field_ident)
      @ticket_field_ident = ticket_field_ident
    end

    def vars
      return unless ticket_field
      joins = [
        "field_#{ticket_field.id}_.value AS field_#{ticket_field.id}",
        "LEFT OUTER JOIN ticket_field_entries AS field_#{ticket_field.id}_ ON (field_#{ticket_field.id}_.ticket_id = tickets.id AND field_#{ticket_field.id}_.ticket_field_id = #{ticket_field.id})"
      ]

      setup(
        identifier: ticket_field.id.to_s,
        label: ticket_field.title,
        title: ticket_field.title,
        presentation_type: get_type,
        db_field: "field_#{ticket_field.id}",
        join_field_alias: joins[0],
        join: joins[1]
      )
    end

    def csv_join_field_alias
      column_name, aliased_name = vars[:join_field_alias].split(' AS ')

      if ticket_field.is_a?(FieldCheckbox)
        column_name = "ELT(#{column_name}+1, 'No', 'Yes')"
      elsif ticket_field.is_a?(FieldTagger)
        column_name = "field_#{ticket_field.id}_options.name"
      end
      null_value = Ticket.connection.quote(value_for_csv_column(Ticket.new) || '')
      "IF(LENGTH(#{column_name}), #{column_name}, #{null_value}) AS #{aliased_name}"
    end

    def csv_join
      if ticket_field.is_a?(FieldTagger)
        "#{vars[:join]} LEFT OUTER JOIN custom_field_options AS field_#{ticket_field.id}_options ON (field_#{ticket_field.id}_options.custom_field_id = #{ticket_field.id} AND field_#{ticket_field.id}_options.value = field_#{ticket_field.id}_.value)"
      else
        vars[:join]
      end
    end

    def order_by(desc = nil)
      db_field + (desc ? ' DESC' : ' ASC')
    end

    def can_be
      return {} unless ticket_field
      {
        deactivated: true,
        deleted: true,
        title_edited: true,
        collapsable_for_agents_edited: true,
        visible_in_portal_edited: true,
        editable_in_portal_edited: true,
        required_in_portal_edited: true,
        required_edited: true,
        activated: field_activatable?
      }
    end

    def event_to_s(event, options = {})
      field = TicketField.find_by_id(event.value_reference)
      return '' if !field || field.is_a?(FieldTagger)
      ticket_field_title = Zendesk::Liquid::DcContext.render(TicketField.find(event.value_reference).title, options[:current_account], options[:current_user])
      if event.is_a?(Create)
        unless event.value.blank?
          return I18n.t('txt.lib.zendesk.fom.field.ticket_field_set_to_new_value', ticket_field_name: ticket_field_title.titleize, new_value: field.humanize_value(event.value))
        end
      else
        from = field.humanize_value(event.value_previous)
        to = field.humanize_value(event.value)
        if from != to
          return I18n.t('txt.lib.zendesk.fom.field.changed_custom_field', title_of_custom_field: ticket_field_title.titleize, old_value_of_custom_field: from, new_value_of_custom_field: to)
        end
      end
      ''
    end

    def value(ticket)
      return I18n.t(NO_CHANGE) unless ticket
      return '' unless ticket_field

      entry =
        if cache = ticket.ticket_field_entries_by_ticket_field_id_cache
          cache[ticket_field.id]
        else
          # the to_a.find here is to handle both archived and unarchived tickets. On an archived ticket,
          # ticket_field_entries is just an array, not an active record assocation+collection
          ticket.ticket_field_entries.to_a.find { |f| f.ticket_field_id == ticket_field.id }
        end

      # if the ticket_field_entries_by_ticket_field_id_cache is present, it may have been populated from
      # two places. From TicketPresenter#model_json for a normal ticket request, or from
      # TicketFieldEntriesRawDataExporter#raw_ticket_field_entries. In the export case, we have "raw" ticket
      # fields which means they are represented as as Hash instead of a TicketFieldEntry.
      case entry
      when nil then ''
      when Hash then entry[:value]
      else entry.value
      end
    end

    def value_for_csv_column(ticket)
      return '' unless ticket_field
      humanize_value(ticket["field_#{ticket_field.id}"])
    end

    def humanize_value(v)
      return '' unless ticket_field
      if ticket_field.is_a?(FieldCheckbox)
        return v == '1' ? I18n.t('txt.helpers.rules_helpers.yes_custom_ticket_field') : '-'
      elsif ticket_field.is_a?(FieldMultiselect)
        v = v.to_s.split.map { |tag| ticket_field.account.field_taggers_cache[tag] }.join(", ")
      elsif ticket_field.is_a?(FieldTagger) && !v.blank?
        v = ticket_field.account.field_taggers_cache[v]
      end
      v.blank? ? '-' : v
    end

    def is_blank?(ticket)
      return false unless ticket_field
      value(ticket).blank?
    end

    def is_active?
      return false unless ticket_field
      ticket_field.is_active?
    end

    def is_a_date?
      false
    end

    def ticket_field
      @ticket_field ||=
        if @ticket_field_ident.is_a?(TicketField)
          @ticket_field_ident
        else
          TicketField.find_by_id(@ticket_field_ident.to_s)
        end
    end

    def sortable?(_current_user_is_agent)
      true
    end

    private

    def field_activatable?
      !ticket_field.is_a?(FieldPartialCreditCard) || ticket_field.account.has_pci_credit_card_custom_field?
    end

    def setup(options)
      Field.set_options(options)
    end

    def method_missing(method, *args)
      if vars.keys.index(method.to_sym)
        vars[method.to_sym]
      else
        super
      end
    end

    def get_type
      return 'Text' if ticket_field.is_a?(FieldText) || ticket_field.is_a?(FieldSubject)
      return 'Numeric' if ticket_field.is_a?(FieldInteger)
      return 'Decimal' if ticket_field.is_a?(FieldDecimal)
      return 'Checkbox' if ticket_field.is_a?(FieldCheckbox)
      return 'Multi-line text' if ticket_field.is_a?(FieldTextarea) || ticket_field.is_a?(FieldDescription)
      return 'Regular expression' if ticket_field.is_a?(FieldRegexp)
      return 'Date' if ticket_field.is_a?(FieldDate)
      return 'PartialCreditCard' if ticket_field.is_a?(FieldPartialCreditCard)
      return 'Multiselect' if ticket_field.is_a?(FieldMultiselect)
      'Drop-down'
    end
  end
end
# rubocop:enable Naming/AccessorMethodName
# rubocop:enable Naming/PredicateName
