require 'zendesk/idempotency/response_store'
require 'zendesk/idempotency/error_log'
require 'zendesk/idempotency/error_handling'

module Zendesk
  module Idempotency
    extend ActiveSupport::Concern

    class_methods do
      def idempotent_proxy(store: Zendesk::RedisStore, expiry: 15.minutes)
        idempotent_store = ResponseStore.new(backend: store, logger: ErrorLog, expiry: expiry)
        ->(_, block) do
          return block.call unless idempotency_key

          signature, *stored_response = idempotent_store.get(idempotency_cache_key)

          if signature
            raise ParameterMismatchError unless signature == idempotency_request_digest

            response.status, headers, response.body = stored_response
            if RAILS4
              response.headers = headers
            else
              headers.each { |key, value| response.set_header(key, value) }
            end
            request.env['zendesk.idempotency_hit'] = true
            response.headers['X-Idempotency-Lookup'] = 'hit'
            # reset ttl
            idempotent_store.touch(idempotency_cache_key)
          else
            signature = idempotency_request_digest
            block.call
            idempotent_store.set(idempotency_cache_key, response, signature: signature)
            request.env['zendesk.idempotency_hit'] = false
            response.headers['X-Idempotency-Lookup'] = 'miss'
          end
        end
      end
    end

    private

    def idempotency_request_digest
      Digest::SHA256.hexdigest(params.to_json)
    end

    def idempotency_key
      @idempotency_key ||= request.headers['Idempotency-Key'].presence
    end

    def idempotency_cache_key
      [
        'id_res',
        idempotency_cache_prefix,
        action_name,
        request.path,
        Digest::SHA1.hexdigest(idempotency_key.to_s)
      ].join ':'
    end
  end
end
