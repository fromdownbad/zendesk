require 'faraday_middleware'
require 'faraday/restrict_ip_addresses'

module Zendesk
  module Net
    module MultiSSL
      MAX_RETRIES = 3

      RETRY_EXCEPTIONS = [EOFError, SystemCallError, SocketError, Faraday::Error::TimeoutError, ::Net::OpenTimeout, Faraday::SSLError].freeze

      class << self
        # sslv3 does not work on some accounts on ruby 1.9 or curl (works fine on ruby 1.8)
        # so we fallback to sslv1 on ssl error
        # can be reproduced on account 152311 ticket 71 with:
        # log_to; a = Account.find 152311; shard a; t = a.tickets.find_by_nice_id 71; st = t.shared_tickets.first; st.retrieve_jira_ticket
        #
        # attempts to use SSLv3 (or last successful SSL version) when contacting a remote server
        # then falls back to the other SSL version if that fails with an OpenSSL failure
        # Poor SSLv2, nobody loves you...
        def get_json(url, options = {})
          return unless response = get(url, options)
          return if response.body.strip.starts_with?("<") # ignore html responses
          begin
            JSON.parse(response.body)
          rescue JSON::ParserError => e
            ZendeskExceptions::Logger.record(e, location: self, message: "Invalid json in remote response #{e.message}", fingerprint: '319ec5d7c5f532461c4348878ce9b681d202d3c9')
            nil
          end
        end

        def get(url, options = {})
          http_request(:get, url, options)
        end

        def post(url, options = {})
          http_request(:post, url, options)
        end

        def http_request(method, url, options)
          retry_count = 0
          cache_key = "ssl_version/#{URI.parse(url).host}"
          ssl_version = Rails.cache.read(cache_key) || 3

          begin
            http_request_with(ssl_version, method, url, options)
          rescue ::Net::HTTPBadResponse => e
            Rails.logger.warn("Bad response from upstream SSLv#{ssl_version}): #{e.message}")
            raise if options[:raise_errors]
            nil
          rescue OpenSSL::SSL::SSLError => e
            ZendeskExceptions::Logger.record(e, location: self, message: "SSL Error SSLv1: #{e.message}", fingerprint: 'a225977cc140c8b73327cf6b16d68ea86315be1e') if ssl_version == 1
            # use other ssl version next time
            next_ssl_version = (ssl_version == 1 ? 3 : 1)
            Rails.cache.write(cache_key, next_ssl_version)

            begin
              http_request_with(next_ssl_version, method, url, options)
            rescue *RETRY_EXCEPTIONS => e
              retry if retry_on_error?(retry_count += 1, e, ssl_version, options)
              handle_ssl_error(ssl_version, e, options)
            rescue StandardError => e
              handle_ssl_error(ssl_version, e, options)
            end
          rescue *RETRY_EXCEPTIONS => e
            retry if retry_on_error?(retry_count += 1, e, ssl_version, options)
            raise if options[:raise_errors]
            nil
          rescue Timeout::Error => e
            Rails.logger.warn("Timeout from upstream SSLv#{ssl_version}: #{e.message}")
            raise if options[:raise_errors]
            nil
          rescue Faraday::RestrictIPAddresses::AddressNotAllowed => e
            # no need to log, the safe_url? check should have already taken care of that
            raise if options[:raise_errors]
            nil
          rescue StandardError => e
            ZendeskExceptions::Logger.record(e, location: self, message: "Unqualified exception SSLv#{ssl_version}: #{e.message}", fingerprint: 'ebf1eeb80cfaefd10866010b0b364168a2638a7b')
            raise if options[:raise_errors]
            nil
          end
        end

        private

        def handle_ssl_error(ssl_version, e, options)
          Rails.logger.warn("No luck after SSLv#{ssl_version} either: #{e.message}")
          raise if options[:raise_errors]
          nil
        end

        def retry_on_error?(retry_count, e, ssl_version, options)
          raise e if non_errno_system_call_error?(e)
          if retry_count == MAX_RETRIES
            Rails.logger.warn("Multi SSL retries exhausted: #{MAX_RETRIES} timeouts in a row: #{e.message}")
            ZendeskExceptions::Logger.record(e, location: self, message: "Exception after #{MAX_RETRIES} retries SSLv#{ssl_version}: #{e.message}", fingerprint: '5286eaf7342ff70aa3476daa3b4a9cc3b5d339fb') unless options[:ignore_retry_notice]
            false
          else
            Rails.logger.warn("Multi SSL retry ##{retry_count}: #{e.class}: #{e.message}")
            sleep(0.5)
            true
          end
        end

        def http_request_with(ssl_version, method, url, options)
          ssl = (options[:ssl] || {}).dup
          ssl[:version] = "TLSv1" if ssl_version == 1
          ssl[:verify] = false if Rails.env.development?

          client = Faraday.new(headers: options[:headers], ssl: ssl) do |builder|
            # Do not convert POST/PUT redirects to GET
            # https://github.com/lostisland/faraday_middleware/pull/66
            builder.use FaradayMiddleware::FollowRedirects, standards_compliant: true
            builder.use Faraday::RestrictIPAddresses, allow_url: lambda { |u| Zendesk::Net::AddressUtil.safe_url?(u.to_s) }
            builder.adapter Faraday.default_adapter
          end
          client.options[:timeout] = options[:timeout]

          client.send(method, url) do |req|
            req.body = options[:body]
          end
        rescue Faraday::Error::ConnectionFailed => e
          raise e.instance_variable_get('@wrapped_exception') || e
        end

        def non_errno_system_call_error?(e)
          (e.class < SystemCallError) && !e.class.name.start_with?('Errno::')
        end
      end
    end
  end
end
