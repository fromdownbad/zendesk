module Zendesk
  module Net
    module CNAMEResolver
      def self.resolve?(name)
        Resolv::DNS.new.getresource(name, Resolv::DNS::Resource::IN::CNAME)
      rescue StandardError
      end
    end
  end
end
