require "zendesk_statsd"

module Zendesk
  module RedisDatadog
    class << self
      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["redis", "performance"])
      end

      def instrument
        # We can't use prepend Module here because of recursion loops in ddtrace auto instrumentation
        # https://github.com/DataDog/dd-trace-rb/blob/b7fce03a41d17e68c2ae1921e9a5a01cdd2078b6/lib/ddtrace/contrib/redis/patcher.rb#L70
        ::Redis::Client.class_eval do
          alias_method :call_without_zd_statsd, :call

          def call(command, &block)
            time = Time.now
            begin
              call_without_zd_statsd(command, &block)
            ensure
              delay_ms = (Time.now - time) * 1000
              command_name = command.is_a?(Array) ? command[0] : command
              Zendesk::RedisDatadog.statsd_client.timing('command', delay_ms, tags: ["command:#{command_name}"])
            end
          end
        end
      end
    end
  end
end
