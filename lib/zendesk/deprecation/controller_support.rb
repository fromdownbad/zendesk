module Zendesk
  # Note, you can only call one deprecation_* method per controller.
  # Each subsequent deprecate_* call will overwrite the previous ones.
  module Deprecation
    module ControllerSupport
      def self.included(base)
        base.extend(Filters)
      end

      protected

      module Filters
        def deprecate_controller(conditions = {})
          deprecate_actions([:all], conditions)
        end

        def deprecate_action(action, conditions = {})
          deprecate_actions([action], conditions)
        end

        def deprecate_actions(actions, conditions = {})
          options = {}.tap do |opts|
            if feature = conditions.fetch(:with_feature, nil)
              opts[:if] = proc do
                if current_account
                  Arturo.feature_enabled_for?(feature, current_account)
                else
                  Arturo.feature_enabled_for_pod?(feature, Zendesk::Configuration.fetch(:pod_id))
                end
              end
            end
            opts[:only] = actions unless actions.include?(:all)
          end

          before_action :prevent_use, options
        end
      end

      def prevent_use
        head :gone
      end
    end
  end
end
