module Zendesk
  module GooddataIntegrations
    class Migrator
      attr_reader :data_file, :records

      Record = Struct.new(:account_id, :project_id)

      def initialize(data_file)
        @data_file = data_file
        @records   = parse_records
      end

      def execute(options = {dry_run: true})
        number_created = 0
        number_on_pod = 0

        records.each do |record|
          account = Account.find(record.account_id)

          next unless account.pod_local?

          number_on_pod += 1

          account.on_shard do
            if account.gooddata_integration.present?
              puts "GoodData integration already exists for account #{account.id}."
            else
              gd_integration = account.build_gooddata_integration(
                account: account,
                project_id: record.project_id,
                status: 'COMPLETE',
                admin_id: nil,
                scheduled_at: '12am'
              )

              if options[:dry_run]
                puts "GoodData integration not valid for account #{account.id}: #{gd_integration.errors.inspect}." unless gd_integration.valid?
              else
                if gd_integration.save
                  puts "GoodData integration created for account #{account.id} with project ID #{record.project_id}."
                  number_created += 1
                else
                  puts "Failed to create GoodData integration for account #{account.id}."
                end
              end
            end
          end
        end

        puts "Migration complete: GoodData integrations created for #{number_created} out of #{number_on_pod} records on this pod. (#{records.count(:all)} total in migration)"
      end

      private

      def parse_records
        records = []

        lines = File.readlines(@data_file)
        lines.each do |line|
          record = Record.new
          record.account_id, record.project_id = line.split(/\s+/)
          records << record
        end

        records
      end
    end
  end
end
