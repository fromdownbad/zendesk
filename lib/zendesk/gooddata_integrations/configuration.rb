require 'zendesk/configuration'

module Zendesk::GooddataIntegrations
  class Configuration < Zendesk::Configuration
    SIGNER_KEY_PATH = ENV.fetch('GOODDATA_SIGNER_KEY_PATH')

    private_constant :SIGNER_KEY_PATH

    class << self
      def signer_private_key
        File.read(SIGNER_KEY_PATH)
      end

      private

      # This file is only read for dev/test, but zendesk_configuration uses this
      # method to derive the GOODDATA_ prefix for env vars.
      def config_file
        'gooddata.yml'
      end
    end
  end
end
