module Zendesk::InboundMail
  class AccountIDMessageLogger < Zendesk::Mail::MessageLogger
    protected

    def format_message(message)
      message = message.to_s
      lines = []

      message.each_line do |line|
        account_id = @msg.account ? @msg.account.id : "none"

        lines << "#{@msg.message_id} account_id: #{account_id}: #{line}"
      end

      lines.join
    end
  end
end
