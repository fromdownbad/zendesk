module Zendesk::Mail::Clients
  class MacMail < AppleMail
    class << self
      def forward_html_element(mail, header_text)
        Zendesk::Mail::Clients::Client.forward_html_element(mail, header_text)
      end
    end
  end
end
