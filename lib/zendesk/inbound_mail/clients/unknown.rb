module Zendesk::Mail::Clients
  class Unknown < Client
    self.quote_pattern = /^>\ ?/m

    # ----- Original Message -----</B></P>
    # From: 6a3ee6c9481@example.com
    # Date: Tuesday, January 5, 2010 6:01 pm
    # Subject: Re: Re: Re: Radio City Christmas Spectacular Refund (ticket #85435) (ticket #91775) (ticket #92105)
    #

    # -------- Mensaje original --------
    # De: Jose Lopez <jose@example.com>
    # Fecha: 03/10/2014 12:32 a.m. (GMT-06:00)
    # A: Juan Perez <juan@example.com>
    # CC: other@example.com
    # Asunto: Re: Restaurante
    #

    # -----Mensaje original-----
    # De: Mario [mailto:mario@example.com]
    # Enviado el: Miercoles, 09 de Febrero de 2005 01:01 p.m.
    # Para: foo@example.gov; mario@example.net
    # Asunto: PROPUESTA CURSO FLASH, MARIO ZAIZAR

    # Starts with "From:" or "Original Message" and ends at "Subject:" header.
    replying.body = /^(?:(?:#{DATE_KEYS}:[^\n]*\n)?#{FROM_KEYS}:|----- #{ORIGINAL_MSG_KEYS}).*?(#{SUBJECT_KEYS}:.*?)\n?(#{IMPORTANCE_KEYS}:.*?)?$/m
    replying.limit = 375
  end
end
