module Zendesk::Mail::Clients
  class AolMail < Client
    replying.body = /----- Reply message -----.*?Date:.*?$/m
    replying.limit = 375

    def self.forward_html_element(mail, _)
      parse_html(mail.content.html).css("div.aolReplacedBody > div").last
    end
  end
end
