# this is also defined in the zendesk_mail gem

module Zendesk::Mail::Clients
  class Gmail < Client
    GMAIL_SIGNATURE_DELIMITER = "-- ".freeze
    GMAIL_SIGNATURE_DATA_ATTRIBUTE_VALUE = 'gmail_signature'.freeze

    replying.body = /^(:?#{TIME_SOURCE}|#{DATE_SOURCE})/
    replying.limit = 175

    def self.prefer_html?(_)
      true
    end

    def self.forward_html_element(mail, header_text)
      element = super(mail, header_text)

      return if element.nil?

      updated_element = remove_signature_and_delimiter(element)

      updated_element.each do |el|
        el.children = remove_signature_and_delimiter(el.children)
      end

      updated_element
    end

    def self.remove_signature_and_delimiter(element)
      node_set = Nokogiri::XML::NodeSet.new(element.document)
      node_blacklist = []

      element.each do |node|
        node_class = node.attr('class')

        next if node_blacklist.include?(node)
        next if node.attr('data-smartmail') == GMAIL_SIGNATURE_DATA_ATTRIBUTE_VALUE

        if node_class && node_class.include?('gmail_quote')
          node_set.push(node)
          next_node = node.next_sibling

          while next_node
            node_blacklist.push(next_node)
            next_node = next_node.next_sibling
          end
        elsif node.content == GMAIL_SIGNATURE_DELIMITER
          node_blacklist.push(node.next) if node.next && node.next.name == 'br'
        else
          node_set.push(node)
        end
      end

      node_set
    end

    private_class_method :remove_signature_and_delimiter
  end
end
