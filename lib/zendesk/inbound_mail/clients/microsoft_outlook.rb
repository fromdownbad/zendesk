module Zendesk::Mail::Clients
  class MicrosoftOutlook < Client
    def self.forward_html_element(mail, _)
      parsed_html = parse_html(mail.content.html)

      element = last_rply_fwd_msg_div(parsed_html)

      return last_ltr_or_rtl_section(parsed_html) if element.nil?

      section = next_sibling_div(element)

      return last_ltr_or_rtl_section(parsed_html) if section.nil?

      section
    end
  end
end
