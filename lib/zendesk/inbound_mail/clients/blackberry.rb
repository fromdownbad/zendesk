module Zendesk::Mail::Clients
  class Blackberry < Client
    replying.body = /^-----Original Message-----$/m
    replying.limit = 50
  end
end
