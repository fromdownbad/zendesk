module Zendesk::Mail::Clients
  class Iphone < AppleMail
    # NOTE: This strips away some original whitespace. Oops.
    self.quote_pattern = /^>\ ?/m
  end
end
