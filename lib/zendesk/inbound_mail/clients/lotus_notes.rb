module Zendesk::Mail::Clients
  class LotusNotes < Client
    self.quote_pattern = /^>\ ?/m
  end
end
