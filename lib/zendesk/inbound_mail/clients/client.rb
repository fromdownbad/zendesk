require 'zendesk/inbound_mail/html_document'

module Zendesk::Mail::Clients
  class Client
    MAX_NOKOGIRI_TREE_DEPTH = -1 # Removes limit on tree depth

    include Zendesk::InboundMail::LocalizedHeaders

    class << self
      attr_accessor :quote_pattern

      def basename
        name.demodulize.snake_case
      end

      def replying
        @replying ||= Zendesk::InboundMail::Replier.new
      end

      def inline_attachments?
        false
      end

      def prefer_html?(_mail)
        false
      end

      def autolinking?(_mail)
        false
      end

      def forward_html_body(mail, header_text)
        if (forward_body = forward_html_element(mail, header_text))
          forward_body.to_html
        else
          mail.logger.info "Can't extract the forward's HTML body"
          nil
        end
      rescue NoMethodError => e
        return forward_body.to_xhtml unless forward_body.nil?

        mail.logger.info "Can't extract the forward's HTML body: #{e.message}"
        nil
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [:inbound_mail, :clients])
      end

      protected

      def parse_html(html)
        Nokogiri::HTML5.parse(html, max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH)
      rescue SystemStackError
        mail.logger.warn('SystemStackError occurred in parse_html')
        Nokogiri::HTML5::Document.new
      end

      def forward_html_element(mail, header_text)
        doc = parse_html(mail.content.html)

        # Get the html body to forward
        if (generic_forward_body = generic_forward_html_body(mail, doc, header_text))
          mail.logger.info "Forward's HTML body detected using the generic finder!"
          return generic_forward_body
        else
          mail.logger.warn "Can't detect the Forward's HTML body using the generic finder!"
        end

        # Get the first line of the forward header (plain-text version)
        unless (start_of_header_text = header_text.split("\n").first)
          debug_log(mail, "Can't detect the first line of the forward header")
          return
        end

        # Get the HTML element that includes that first line of the header
        forward_header = doc.css("div > text()").find do |el|
          el.text.include?(start_of_header_text)
        end

        unless forward_header && forward_header.parent
          debug_log(mail, "Can't detect the forward header or it's parent using: #{start_of_header_text}")
          return
        end

        unless (first_div_after_header = forward_header.parent.css('div').first)
          debug_log(mail, "Can't detect the first div after the forward header!")
          return
        end

        Zendesk::InboundMail::HTMLDocument.remove_preceding_elements(first_div_after_header)

        doc
      end

      def generic_forward_html_body(mail, doc, header_text)
        body = doc.at_css('body')
        styles = doc.css('styles')

        unless header_text
          debug_log mail, "Can't find the forward body without a plain text header"
          return
        end
        lines = header_text.split("\n")

        # This will break on `first` returning `nil`. However that is
        # existing behavior.
        header_start = Regexp.quote(lines.first.split(/:/).first)
        header_start&.gsub!("\\ ", "[[:space:]]") if mail.account.has_email_escape_forward_header_spaces_fix?

        header_end = Regexp.quote(lines.last.split(/:/).first)
        header_end&.gsub!("\\ ", "[[:space:]]") if mail.account.has_email_escape_forward_header_spaces_fix?

        first_header_key = doc.search('//text()').detect { |el| el.text.match(header_start) }
        unless first_header_key
          debug_log mail, "Can't find where the forward header starts: '#{header_start}'"
          return
        end

        last_header_key = if mail.account.has_email_find_last_header_key_after_first?
          doc.search('//text()').detect { |el| (el <=> first_header_key) > 0 && el.text.match(header_end) }
        else
          doc.search('//text()').detect { |el| el.text.match(header_end) }
        end
        unless last_header_key
          debug_log mail, "Can't find where the forward header ends: '#{header_end}'"
          return
        end

        if (last_header_element = last_header_element(last_header_key, lines.last))
          container = Zendesk::InboundMail::HTMLDocument.remove_self_and_preceding_elements(last_header_element)
          container = Zendesk::InboundMail::HTMLDocument.remove_if_empty(container)
          Zendesk::InboundMail::HTMLDocument.remove_surrounding_containers(container)
          Zendesk::InboundMail::HTMLDocument.remove_leading_brs(body)

          styles + body.children
        else
          debug_log mail, "Couldn't find a valid last header element"
          nil
        end
      end

      def last_header_element(element, last_header_text)
        loop do
          element_with_text = Zendesk::InboundMail::HTMLDocument.next_element_with_text(element)

          # If the words found in the element match the last line of header text
          if (words = element_with_text.text.scan(/[^\W\d][\w'-]*(?<=\w)/)) && words.all? { |word| last_header_text.include? word }
            # Don't let the words in the last line get double-matched
            words.each { |word| last_header_text.sub!(word, "") }
            element = element_with_text
          else
            break
          end
        end
        element
      end

      def last_ltr_or_rtl_section(fragment)
        Zendesk::InboundMail::HTMLDocument.last_ltr_or_rtl_section(fragment)
      end

      def last_rply_fwd_msg_div(fragment)
        Zendesk::InboundMail::HTMLDocument.last_rply_fwd_msg_div(fragment)
      end

      def next_sibling_div(fragment)
        Zendesk::InboundMail::HTMLDocument.next_sibling_div(fragment)
      end

      private

      def debug_log(mail, message)
        return unless mail.account.has_email_debugging_mode?
        mail.logger.info "[DEBUG] #{message}"
      end
    end
  end
end
