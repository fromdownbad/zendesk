module Zendesk::Mail::Clients
  class Zimbra < Client
    # ----- "Walt Whitman via RT" <support@example.com> wrote:
    replying.body = /^-----.*? wrote:$/m
    replying.limit = 175

    def self.prefer_html?(_)
      true
    end
  end
end
