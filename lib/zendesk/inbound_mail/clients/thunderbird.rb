module Zendesk::Mail::Clients
  class Thunderbird < Client
    replying.body = /^On.*? wrote:$|^\n.{1,20} wrote, On.{3,20}\n/m
    replying.limit = 160

    def self.forward_html_element(mail, _)
      parse_html(mail.content.html).css("div.moz-forward-container > div")
    end
  end
end
