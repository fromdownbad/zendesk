module Zendesk::Mail::Clients
  class FacebookMail < Client
    # Facebook sends messages in the text/plain part rather than actual content.
    def self.prefer_html?(_)
      true
    end
  end
end
