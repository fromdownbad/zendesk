module Zendesk::Mail::Clients
  class Yahoo < Client
    # ______________________________
    # From: bob example <bob@example.com>
    # To: Yahoo User <yahoouser@yahoo.com>
    # Sent: Thursday, July 25, 2013 6:08 PM
    # Subject: Hi Yahoo User

    # ______________________________
    # De: Mario Zaizar <zaizarmario@yahoo.com>
    # Para: mariozaizar@outlook.com
    # Enviado: miercoles, 13 de julio de 2016 4:27 p.m.
    # Asunto: Test #1 Yahoo sends an email to Outlook

    replying.body = /(?:(?:_+\n) ?#{FROM_KEYS}:[^\n]*\n)#{TO_KEYS}:.*?\n#{SENT_KEYS}:.*?(\n#{SUBJECT_KEYS}:.*?\n)/m
    replying.limit = 300

    def self.forward_html_element(mail, _)
      yahoo_message_container = parse_html(mail.content.html).css("div.y_msg_container").first
      yahoo_message_container && last_ltr_or_rtl_section(yahoo_message_container)
    end
  end
end
