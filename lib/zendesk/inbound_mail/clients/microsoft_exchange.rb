require 'zendesk_mail/clients/microsoft_exchange'

module Zendesk::Mail::Clients
  class MicrosoftExchange < Client
    #   _____
    #
    # From: OutlookUser.com Support Center [mailto:support@outlookuser.zendesk.com]
    # Sent: January 5, 2010 2:25 PM
    # To: Melanie
    # Subject: [OutlookUser.com Support Center] Re: Table Lamp Bases
    # Importance: High

    # From: OutlookUser Support [mailto:notifications-support@outlookuser.zendesk.com]=20
    # Sent: Wednesday, January 06, 2010 9:54 AM
    # To: Outlook User
    # Subject: [OutlookUser Business Systems] Re: Rolling report to Excel

    # Von: Angela Beaumel =
    # [mailto:notifications-support@outlookuser.zendesk.com]=20
    # Gesendet: Samstag, 23. Juni 2012 07:30
    # An: Angela B=C3=A4umel
    # Betreff: [Outlook User] Betreff: Test for international Mail-Headers

    replying.body = /(_____)*\s+?#{FROM_KEYS}:.*?(?:#{SUBJECT_KEYS}):.*?\n?(#{IMPORTANCE_KEYS}:.*?)?$/m
    replying.limit = 375

    class << self
      def prefer_html?(_)
        true
      end

      def autolinking?(_)
        true
      end

      protected

      def forward_html_element(mail, header_text)
        doc = parse_html(mail.content.html)

        generic_approach = generic_forward_html_body(mail, doc.dup, header_text)

        old_approach = word_section(doc, mail) || exchange_section(doc, mail) || last_ltr_or_rtl_section(doc)

        return old_approach unless generic_approach

        if generic_approach.to_s != old_approach.to_s
          mail.logger.info "MicrosoftExchange detected forward html differently than the old way"
          mail.logger.info "generic_approach: #{generic_approach.to_s[0..2000]}"
          mail.logger.info "old_approach: #{old_approach.to_s[0..2000]}"
        end

        generic_approach
      end

      def word_section(doc, mail = nil)
        mail.logger.info "Trying to find forward body by looking for 'div.WordSection1' element" if mail
        doc.css("div.WordSection1").last.tap do |body|
          mail.logger.info "Found 'div.Section1' element" if body && mail
        end
      end

      def exchange_section(doc, mail = nil)
        mail.logger.info "Trying to find forward body by looking for 'div.Section1' element" if mail
        section = doc.css("div.Section1").last

        if section
          mail.logger.info "Found 'div.Section1' element" if mail
          section.children.each do |child|
            child.remove # Remove any children before forward header
            if child.name == "div" # Stop at forward header (From, To, Subject...)
              break
            end
          end

          section.children
        end
      end
    end
  end
end
