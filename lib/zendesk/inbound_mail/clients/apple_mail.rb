module Zendesk::Mail::Clients
  class AppleMail < Client
    # NOTE: This strips away some original whitespace. Oops.
    self.quote_pattern = /^>\ ?/m

    def self.inline_attachments?
      true
    end

    def self.forward_html_element(mail, _)
      parse_html(mail.content.html).css("blockquote > div").last
    end
  end
end
