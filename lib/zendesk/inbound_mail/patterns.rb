module Zendesk::InboundMail::Patterns
  ZENDESK_CONFIG_HOST     = Regexp.escape(Zendesk::Configuration.fetch(:host))
  ZENDESK_APP_ADDRESS     = /^(?:.+@[a-zA-Z0-9\-]+\.#{ZENDESK_CONFIG_HOST}|support@zendesk\.com)$/
  ZENDESK_DOMAIN_ADDRESS  = /^(?:.+@(?:\S*\.)?(?:#{ZENDESK_CONFIG_HOST}|zendesk\.com)(?:>)?)$/
  ZENDESK_MESSAGE_ID      = /\A<?[a-zA-Z0-9]{8,10}_?(\h+_\h+_sprut)?@#{ZENDESK_CONFIG_HOST}>?\z/
  ENVELOPE_DOMAIN_PATTERN = /@([^>]*)/
  OUT_HOST_PATTERN        = /[\w\d]+\.pod\d+\.[a-z]+\d+\.zdsys(test)?\.com/
end
