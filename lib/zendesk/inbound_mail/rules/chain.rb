module Zendesk::InboundMail::Rules
  class Chain
    def initialize(*rules)
      @rules = rules
    end

    def execute(state, *args)
      result = rules.map do |rule|
        rule.execute(state, *args)
      end

      result.include? true
    end

    private

    attr_reader :rules
  end
end
