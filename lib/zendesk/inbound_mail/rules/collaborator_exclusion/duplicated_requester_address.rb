module Zendesk::InboundMail::Rules::CollaboratorExclusion
  class DuplicatedRequesterAddress < Zendesk::InboundMail::Rules::Base
    def execute(state, *args)
      address = args.first[:address]
      ticket = state.ticket

      if duplicate_of_requester_address?(state, address, ticket)
        log("Collaborator address: #{address} is a duplicate of the requester address")
        true
      end
    end

    private

    def duplicate_of_requester_address?(state, address, ticket)
      # compares all requester emails to address stripped of Zendesk plus ID
      # or original address if no plus ID is present
      address_without_plus_id = Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(address)
      if (found = ticket_author(ticket).try(:emails_include?, address_without_plus_id))
        state.requester_is_on_email = true
      end
      found
    end

    def ticket_author_email(ticket)
      if ticket.respond_to?(:requester)
        ticket.try(:requester).try(:email)
      else
        ticket.author.try(:email)
      end
    end

    def ticket_author(ticket)
      if ticket.respond_to?(:requester)
        ticket.try(:requester)
      else
        ticket.author
      end
    end
  end
end
