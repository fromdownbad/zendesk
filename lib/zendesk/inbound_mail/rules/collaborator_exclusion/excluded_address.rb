module Zendesk::InboundMail::Rules::CollaboratorExclusion
  class ExcludedAddress < Zendesk::InboundMail::Rules::Base
    def execute(_state, *args)
      address = args.first[:address]
      excluded = args.first[:excluded]
      if excluded.include?(address)
        log("Already received email, skipping: #{address}")
        true
      end
    end
  end
end
