module Zendesk::InboundMail::Rules::CollaboratorExclusion
  class InvalidDomain < Zendesk::InboundMail::Rules::Base
    def execute(_state, *args)
      address = args.first[:address]

      if UserEmailIdentity.domain_invalid_address?(address)
        log("Invalid @domain.invalid email address, skipping: #{address}")
        true
      end
    end
  end
end
