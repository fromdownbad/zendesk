module Zendesk::InboundMail::Rules::CollaboratorExclusion
  class BadReplyAddressAndTicketReplyBug < Zendesk::InboundMail::Rules::Base
    def execute(state, *args)
      address = args.first[:address]
      account = state.account
      ticket = state.ticket

      if bad_reply_address?(address, account.subdomain) && ticket_possibly_affected_by_reply_to_bug?(ticket)
        log("Bad reply address introduced by a bug, skipping: #{address}")
        true
      end
    end

    private

    def bad_reply_address?(address, account_subdomain)
      Zendesk::InboundMail::BAD_REPLY_ADDRESSES[account_subdomain] == address
    end

    def ticket_possibly_affected_by_reply_to_bug?(ticket)
      return false if ticket.new_record?
      ticket.created_at < DateTime.parse('November 5 2013')
    end
  end
end
