module Zendesk::InboundMail::Rules
  class Base
    def initialize(logger)
      @logger = logger
    end

    def execute
      raise NotImplementedError
    end

    private

    attr_reader :logger

    def log(message)
      logger.log(message)
    end
  end
end
