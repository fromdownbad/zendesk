module Zendesk
  module InboundMail
    class RspamdTrain
      RSPAMD_BAYES_BACKEND            = 'bayes'.freeze
      RSPAMD_FUZZY_BACKEND            = 'fuzzy'.freeze

      RSPAMD_BAYES_CLASSIFIER_COMMON  = 'common'.freeze
      RSPAMD_BAYES_CLASSIFIER_PERUSER = 'peruser'.freeze

      RSPAMD_FUZZY_DENIED_FLAG        = '11'.freeze
      RSPAMD_FUZZY_WHITE_FLAG         = '11'.freeze
      RSPAMD_FUZZY_WEIGHT             = '10'.freeze
      RSPAMD_FUZZY_URI                = 'fuzzyadd'.freeze

      RSPAMD_BAYES_SPAM_URI           = 'learnspam'.freeze
      RSPAMD_BAYES_HAM_URI            = 'learnham'.freeze

      def initialize(raw_email:, classification:, recipient:, classifier: RSPAMD_BAYES_CLASSIFIER_COMMON, backend: RSPAMD_BAYES_BACKEND)
        @raw_email           = raw_email
        @classification      = classification
        @recipient           = recipient
        @classifier          = classifier
        @backend             = backend
        @rspamd_api_endpoint = ENV['RSPAMD_API_ENDPOINT']
        @rspamd_password     = ENV['RSPAMD_PASSWORD']
      end

      def call
        conn = Faraday.new(url: @rspamd_api_endpoint)

        resp = conn.post do |req|
          req.url rspamd_training_resource

          req.headers['Password']   = @rspamd_password
          req.headers['Rcpt']       = @recipient
          req.headers['Classifier'] = @classifier         if bayes?
          req.headers['Flag']       = fuzzy_flag          if fuzzy?
          req.headers['Weight']     = RSPAMD_FUZZY_WEIGHT if fuzzy?

          req.body = @raw_email.eml
        end

        increment_success_metric           if resp.status == 200
        increment_not_enough_tokens_metric if resp.status == 204
        increment_already_trained_metric   if resp.status == 208
        increment_failed_metric            if resp.status == 404

        resp
      rescue Faraday::ClientError => e
        increment_error_metric
        Rails.logger.warn(e.message)
      end

      private

      def fuzzy?
        @backend == RSPAMD_FUZZY_BACKEND
      end

      def bayes?
        @backend == RSPAMD_BAYES_BACKEND
      end

      def ham?
        @classification == Zendesk::InboundMail::RspamdSpamScore::HAM
      end

      def spam?
        @classification == Zendesk::InboundMail::RspamdSpamScore::SPAM
      end

      def increment_success_metric
        increment_metric("#{@backend}.#{@classifier}.success")
      end

      def increment_not_enough_tokens_metric
        increment_metric("#{@backend}.#{@classifier}.not_enough_tokens")
      end

      def increment_already_trained_metric
        increment_metric("#{@backend}.#{@classifier}.already_trained")
      end

      def increment_failed_metric
        increment_metric("#{@backend}.#{@classifier}.failed")
      end

      def increment_error_metric
        increment_metric('error')
      end

      def increment_metric(metric)
        statsd_client.increment("ham.#{metric}")  if ham?
        statsd_client.increment("spam.#{metric}") if spam?
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'rspamd.training')
      end

      def fuzzy_flag
        return RSPAMD_FUZZY_WHITE_FLAG  if ham?
        return RSPAMD_FUZZY_DENIED_FLAG if spam?
      end

      def rspamd_training_resource
        return RSPAMD_FUZZY_URI      if fuzzy?
        return RSPAMD_BAYES_HAM_URI  if ham? && bayes?
        return RSPAMD_BAYES_SPAM_URI if spam? && bayes?
      end
    end
  end
end
