module Zendesk::InboundMail::Truncation
  module SuspendedTicket
    include Zendesk::InboundMail::Truncation::Base

    def truncate_from_name_to_limit
      super if truncation_needed?('from_name')
    end

    def truncate_from_mail_to_limit
      super if truncation_needed?('from_mail')
    end

    def truncate_message_id_to_limit
      super if truncation_needed?('message_id')
    end
  end
end
