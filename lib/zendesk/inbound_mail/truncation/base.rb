module Zendesk::InboundMail::Truncation
  module Base
    def statsd_client
      Zendesk::InboundMail::StatsD.client
    end

    def truncation_needed?(attribute)
      value = read_attribute(attribute)
      column = self.class.columns_hash.fetch(attribute)
      limit = Zendesk::Extensions::TruncateAttributesToLimit.limit(column)
      if (size = value.to_s.size) > limit
        model = self.class.to_s
        statsd_client.increment(:truncated_attribute, tags: ["model:#{model}", "attr:#{attribute}"])
        statsd_client.histogram(:truncated_attribute_length, size, tags: ["model:#{model}", "attr:#{attribute}"])
        true
      end
    end
  end
end
