module Zendesk::InboundMail::Truncation
  module InboundEmail
    include Zendesk::InboundMail::Truncation::Base

    def truncate_message_id_to_limit
      super if truncation_needed?('message_id')
    end

    def truncate_from_to_limit
      super if truncation_needed?('from')
    end
  end
end
