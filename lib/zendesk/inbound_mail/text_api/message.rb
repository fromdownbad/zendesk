module Zendesk::InboundMail
  module TextApi
    # Represents a message containing zero or more API tags.
    #
    class Message
      # Gets the String original text.
      attr_reader :text

      # Gets the Array of String names of tags that were not recognized by
      #   the resolver.
      attr_reader :unknown_tags

      # Gets the Array of tags whose values were found invalid by the resolver.
      #   Each tag is an Array with two elements, the String tag name and the
      #   String value.
      attr_reader :invalid_values

      # Gets the Hash of properties extracted from the text.
      attr_reader :properties

      # Gets the String text that has been stripped of API tags.
      def stripped_text
        parse_result.stripped_text
      end

      def stripped_html
        if tags.any? && @html.present?
          parse_result.stripped_html
        else
          @html
        end
      end

      def known_tags
        parse_result.known_tags
      end

      # Creates a new Message.
      #
      # text - the String original text.
      #
      # Options
      #
      # :parser   - the Parser used to extract tags from text (optional).
      # :resolver - the Resolver used to resolve the tag values (optional).
      #
      def initialize(text:, html: nil, options: {})
        @text           = text
        @html           = html
        @parser         = options.fetch(:parser, Parser.new)
        @resolver       = options.fetch(:resolver, Resolver.new)
        @unknown_tags   = []
        @invalid_values = []
        @properties     = {}

        build_properties_hash
      end

      private

      def tags
        parse_result.tags
      end

      def parse_result
        @parse_result ||= @parser.parse(@text, @html)
      end

      def build_properties_hash
        tags.each { |name, value| resolve_property(name, value) }
      end

      def resolve_property(name, value)
        property, resolved_value = @resolver.resolve(name, value)
        @properties[property] = resolved_value
      rescue Resolver::UnknownTag
        @unknown_tags << name
      rescue Resolver::InvalidValue
        @invalid_values << [name, value]
      end
    end
  end
end
