module Zendesk::InboundMail
  module TextApi
    # Internal: Resolves tags based on their name and text value.
    # New tags must also be declared in the text api treetop grammar.
    class Resolver
      UnknownTag   = Class.new(StandardError)
      InvalidValue = Class.new(StandardError)

      class << self
        def can_resolve?(name)
          tags.include?(name)
        end

        private

        def define_tag(name, &block)
          tags[name] = block || proc { |value| value }
        end

        def tags
          @@tags ||= {}.with_indifferent_access
        end

        # Some webmail clients still insert psuedo HTML tags in plain text mail.
        # Focusing on specific values since the plain text cleanup also strips away newlines.
        def resolve_user(tag, value)
          value = Zendesk::MtcMpqMigration::Content::HTMLPlainText.convert(value).strip

          # ZD915802: allow me to have "foo@bar.com (mailto:foo@bar.com)" as the value
          # because some mail clients auto-link the email address.
          value &&= value.scan(/[^\s]+/).first

          # When the value is numeric, it will be matched to user IDs; If isn't,
          # then gets tested as an email address. Otherwise, this tag/value will
          # be ignored.
          if value =~ /^(\d+)$/
            [:"#{tag}_id", Integer($1)]
          elsif sanitized_value = Zendesk::Mail::Address.sanitize(value)
            # EM-2975: It's a common typo to leave addresses with an ending `&gt;`
            # E.g. `foo@bar.com>`. Sanitize cleanups the address as `foo@bar.com`
            [:"#{tag}_email", sanitized_value]
          else
            raise InvalidValue
          end
        end
      end

      # Resolves a tag.
      #
      # name  - the String or Symbol name of the tag.
      # value - the String value of the tag
      #
      # Examples
      #
      #   resolver = TextApiResolver.new
      #   resolver.resolve("public", "yes")
      #   #=> [:is_public, true]
      #
      # Returns an Array holding the resolved Symbol property name and the resolved value.
      # Raises UnknownTag if the tag name cannot be resolved.
      def resolve(name, value)
        raise UnknownTag unless @@tags.key?(name)
        @@tags.fetch(name).call(value)
      end

      # Status types
      define_tag(:open) { [:status_id, StatusType.find('open')] }
      define_tag(:pending) { [:status_id, StatusType.find('pending')] }
      define_tag(:solved) { [:status_id, StatusType.find('solved')] }
      define_tag(:archived) { [:status_id, StatusType.find('solved')] }
      define_tag(:done) { [:status_id, StatusType.find('solved')] }
      define_tag(:hold) { [:status_id, StatusType.find('hold')] }
      define_tag(:"on-hold") { [:status_id, StatusType.find('hold')] }

      # Priority types
      define_tag(:low) { [:priority_id, PriorityType.find('low')] }
      define_tag(:normal) { [:priority_id, PriorityType.find('normal')] }
      define_tag(:urgent) { [:priority_id, PriorityType.find('urgent')] }
      define_tag(:high) { [:priority_id, PriorityType.find('high')] }

      # Ticket types
      define_tag(:problem) { [:ticket_type_id, TicketType.find('problem')] }
      define_tag(:question) { [:ticket_type_id, TicketType.find('question')] }
      define_tag(:incident) { [:ticket_type_id, TicketType.find('incident')] }
      define_tag(:task) { [:ticket_type_id, TicketType.find('task')] }

      # Public short tag
      define_tag(:note) { [:is_public, false] }

      define_tag(:public) { |value| [:is_public, %w[false no].exclude?(value.to_s.downcase)] }

      define_tag(:assignee) { |value| resolve_user(:assignee, value) }
      define_tag(:assign) { |value| resolve_user(:assignee, value) }
      define_tag(:requester) { |value| resolve_user(:requester, value) }

      define_tag(:group) do |value|
        if value =~ /^(\d+)$/
          [:group_id, Integer($1)]
        else
          [:group_name, value]
        end
      end

      define_tag(:status) do |value|
        if status_id = StatusType.find(value.to_s.downcase)
          [:status_id, status_id]
        else
          raise InvalidValue
        end
      end

      define_tag(:priority) do |value|
        if priority_id = PriorityType.find(value.to_s.downcase)
          [:priority_id, priority_id]
        else
          raise InvalidValue
        end
      end

      define_tag(:type) do |value|
        if type_id = TicketType.find(value.to_s.downcase)
          [:ticket_type_id, type_id]
        else
          raise InvalidValue
        end
      end

      define_tag(:tags) { |value| [:set_tags, value.to_s.downcase] }
      define_tag(:tag) { |value| [:set_tags, value.to_s.downcase] }
    end
  end
end
