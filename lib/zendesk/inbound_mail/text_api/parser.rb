module Zendesk::InboundMail
  module TextApi
    # Internal: Parses a string for tags comforming to the text API syntax.
    #
    # Tags are written as an "#" followed by the tag name, whitespace, and
    # the tag value, e.g. "#public yes". Tags must be written on their own
    # lines, and must begin at the start of the line.
    #
    class Parser
      VALID_USER_TAGS               = %w[assignee assign requester].freeze

      VALID_USER_TAGS_REGEXP        = Regexp.union(VALID_USER_TAGS.map { |tag| Regexp.new("##{tag}", Regexp::IGNORECASE) })
      VALID_WORD_VALUE_TAGS_REGEXP  = /public|assignee|assign|requester|group|status|priority|type|tags|tag/i
      VALID_SINGLE_WORD_TAGS_REGEXP = /note|public|open|pending|solved|archived|done|hold|on-hold|low|normal|urgent|high|tags|tag|problem|question|incident|task/i

      USER_HASHTAGS_REGEXP          = /^(#{Regexp.union(VALID_USER_TAGS_REGEXP)})\s*$/i
      WORD_VALUE_TAG_REGEXP         = /^#(#{Regexp.union(VALID_WORD_VALUE_TAGS_REGEXP)})\s+(.+)$/i
      SINGLE_WORD_TAG_REGEXP        = /^#(#{Regexp.union(VALID_SINGLE_WORD_TAGS_REGEXP)})\s*/i

      # Internal: Represents the result of parsing a text.
      #
      # Examples
      #
      #   text = <<-EOS
      #   #public true
      #   #awesome true
      #   #note
      #
      #   Hi there!
      #   EOS
      #
      #   parser = Parser.new
      #   result = parser.parse(text)
      #
      #   result.tags
      #   #=> [["public", "true"], ["awesome", "true"], ["note]]
      #
      #   result.stripped_text
      #   #=> "Hi there!"
      #
      class Result
        # Gets the Array of tags that were parsed from the text.
        attr_reader :tags, :raw_tags

        # Creates a new Result.
        #
        # text - the String text that was parsed.
        # tags - the Array of tags that were parsed from the text.
        #
        def initialize(text, html, tags, raw_tags)
          @text = text
          @html = html
          @tags = tags
          @raw_tags = raw_tags
        end

        def stripped_text
          stripped_text = ""
          @text.each_line do |line|
            stripped_text += strip_known_tags_from(line)
          end

          stripped_text.lstrip # Strip blank lines from the beginning of the text
        end

        def stripped_html
          doc = Nokogiri::HTML5.parse(@html, max_tree_depth: Zendesk::InboundMail::HTMLDocument::MAX_NOKOGIRI_TREE_DEPTH)
          doc.search('//text()').each do |node|
            node_content = node.content.to_str
            next unless node_content.gsub!(known_tags_regex, '')

            remove_user_tag_value_node_if_present(node)

            node.content = node_content
            remove_blank_nodes(node) if node.content.blank?
          end
          doc.content.blank? ? nil : doc.to_s
        end

        # Build lines of known tags from @tags
        #
        # @tags
        # #=> [["public", "true"], ["problem"]]
        #
        # Returns
        # #=> ["#public true", "#problem"]
        #
        def known_tags
          @known_tags ||= @raw_tags.map { |t| "##{t.join(' ')}" }
        end

        private

        # Matches a line to known_tags
        #
        # Returns true if line matches one of the known tags
        #
        def known_tags_in(line)
          known_tags.include?(line.strip)
        end

        def known_tags_regex
          # known_tags = ["#problem", "#assignee john@doe.com"] will become /#problem|#assignee john@doe\.com|#assignee/
          Regexp.union(known_tags + (VALID_USER_TAGS & tags.map(&:first)).map { |tag| "##{tag}" })
        end

        # Returns the line stripped for known tags
        #
        def strip_known_tags_from(line)
          if known_tags_in(line)
            line = line.gsub(WORD_VALUE_TAG_REGEXP, '').gsub(SINGLE_WORD_TAG_REGEXP, '')
          end
          line
        end

        def get_tag_value(name)
          found_tag = tags.find { |tag| tag.length == 2 && tag[0] == name.delete("#") }
          found_tag[1] if found_tag
        end

        def remove_user_tag_value_node_if_present(node)
          if (user_tag = node.content[USER_HASHTAGS_REGEXP, 1])
            user_tag_value = get_tag_value(user_tag)
            next_node = node.next
            next_node.remove if next_node && next_node.content == user_tag_value
          end
        end

        def remove_blank_nodes(node)
          # try to remove both the current topmost blank node and any immediately following
          while node && node_is_blank?(node)
            node = topmost_blank_node(node)
            next_node = node.next
            node.remove
            node = next_node
          end
        end

        def topmost_blank_node(node)
          while node.ancestors.any? && node_is_blank?(node.parent)
            node = node.parent
          end
          node
        end

        def node_is_blank?(node)
          node && node.content.blank? && node.css('img').empty?
        end
      end

      # Parses the text, returning a list of tags.
      #
      # text - the String text that should be parsed.
      # html - the HTML representation of the text string, which will also have tags stripped
      #
      # Returns the Result result, containing the parsed tags and the stripped text.
      #
      def parse(text, html)
        Result.new(text, html, *parse_tags(text))
      end

      private

      def logger
        Rails.logger
      end

      def parse_tags(text)
        tags = []
        raw_tags = []
        text.each_line do |line|
          case line.strip
          when WORD_VALUE_TAG_REGEXP
            if Zendesk::InboundMail::TextApi::Resolver.can_resolve?($1.downcase)
              tags << [$1.downcase, $2]
              raw_tags << [$1, $2]
            end
          when SINGLE_WORD_TAG_REGEXP
            if Zendesk::InboundMail::TextApi::Resolver.can_resolve?($1.downcase)
              tags << [$1.downcase]
              raw_tags << [$1]
            end
          when /^\s*$/
            next
          else
            break
          end
        end

        [tags, raw_tags]
      end
    end
  end
end
