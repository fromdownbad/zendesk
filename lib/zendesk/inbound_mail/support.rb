module Zendesk
  module InboundMail
    module Support
      def self.parse_json(json)
        Yajl::Parser.parse(json, check_utf8: false).with_indifferent_access
      end
    end
  end
end
