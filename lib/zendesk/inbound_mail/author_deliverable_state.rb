module Zendesk::InboundMail
  class AuthorDeliverableState
    attr_accessor :state,
      :new_deliverable_state,
      :from_address,
      :from_name,
      :account,
      :logger

    attr_writer :identity

    delegate :log, to: :logger

    def self.mark(state:, new_deliverable_state:, logger:, author: nil)
      new(state: state, new_deliverable_state: new_deliverable_state, logger: logger, author: author).send(:mark)
    end

    private

    def initialize(state:, new_deliverable_state:, logger:, author:)
      @new_deliverable_state = DeliverableStateType[new_deliverable_state]
      @from_address = state.from.try(:address)
      @from_name    = state.from.try(:name).try(:strip)
      @account      = state.account
      @author       = author
      @logger       = logger
    end

    def mark
      if identity
        log("Marking #{identity.value} as #{new_deliverable_state.name}")
        identity.public_send("mark_as_#{new_deliverable_state.name.downcase}".to_sym)
      else
        log("Could not find identity for #{from_address}")
      end
    end

    def identity
      return unless author
      @identity ||= author.identities.detect { |i| i.is_a?(UserEmailIdentity) && i.value == from_address }
    end

    def author
      @author ||= find_or_create_author
    end

    def find_or_create_author
      return if from_address.blank?

      user = account.user_identities.email.where(value: from_address).first.try(:user)
      unless user
        name = from_name.try(:size).to_i < 2 ? "Unknown #{from_name}".strip : from_name
        user = User.create(account: account, name: name, email: from_address)
      end

      user
    end
  end
end
