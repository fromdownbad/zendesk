module Zendesk::InboundMail
  class TicketProcessingJob
    TICKET_PROCESSING_TIMEOUT = 5.minutes.to_i

    def self.work(attributes)
      new(attributes).work
    end

    # This method provides a hook for config/initializers/newrelic.rb to add a singlton method to every processor class
    # and should not be used anywhere else.- cwippern - 2017/07/27
    def self.all_processors
      Dir["#{File.dirname(__FILE__)}/processors/*.rb"].each { |file| load file }
      processors_module = Zendesk::InboundMail::Processors
      processors_module.constants.map { |symbol| processors_module.const_get(symbol) }.select { |c| c.is_a?(Class) }
    end

    attr_reader :mail, :account_id, :recipient

    def initialize(attributes)
      @mail       = attributes.fetch(:mail)
      @account_id = attributes.fetch(:account_id)
      @route_id   = attributes.fetch(:route_id)
      @recipient  = attributes.fetch(:recipient)
    end

    def work
      raise Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout.new, 'ContentProcessor timed out' if raise_content_processor_timeout?
      account.on_shard { convert_mail_to_ticket }
      state
    end

    def convert_mail_to_ticket
      Timeout.timeout(TICKET_PROCESSING_TIMEOUT, Zendesk::InboundMail::ProcessingTimeout) do
        ActiveRecord::Base.connection.cache do
          Account.on_slave.connection.cache do
            ensure_state
            processing_chain.execute(state, mail)
          end
        end
      end
    end

    def state
      @state ||= Zendesk::InboundMail::ProcessingState.new.tap do |state|
        state.account     = account
        state.brand_id    = brand_id
        state.recipient   = recipient
        state.suspension  = mail.processing_state.suspension
        state.from        = mail.processing_state.from
        state.from_header = mail.processing_state.from_header if account.has_email_mtc_add_from_header_state_variable?
      end
    end

    def account
      Account.find(account_id)
    end

    def brand_id
      Route.where(id: @route_id).first.try(:active_brand).try(:id)
    end

    def ensure_state
      unless account_id == state.account.id
        mail.logger.error("ProcessingState's account#id: #{state.account.id} does not match TicketProcessingJob's account_id: #{account_id}")
        exit(1)
      end
    end

    private

    def raise_content_processor_timeout?
      state.suspension == SuspensionType.CONTENT_PROCESSOR_TIMED_OUT
    end

    def processing_chain
      @processing_chain ||= Chain.new(*processors)
    end

    # Be careful when changing MTC processor order.
    # The validating processor must come before the user processor.
    def processors
      @processors ||= begin
        [
          Processors::BlockingProcessor,
          Processors::ForwardingVerificationProcessor,
          Processors::NPSSurveyAutoReplyIgnoreProcessor,
          Processors::GmailForwardingValidationProcessor,
          Processors::YahooForwardingValidationProcessor,
          Processors::MessageIdProcessor,
          Processors::SenderAuthenticationProcessor,
          Processors::WhitelistProcessor,
          Processors::BlacklistProcessor,
          Processors::LoopProcessor,
          Processors::TicketFinderProcessor,
          Processors::GoogleVerificationBlockingProcessor,
          Processors::ContentProcessor,
          Processors::ReplyToProcessor,
          Processors::ForwardingProcessor,
          Processors::BounceProcessor,
          Processors::ValidatingProcessor,
          Processors::UserProcessor,
          Processors::TextApiProcessor,
          Processors::RecipientProcessor,
          Processors::CloudmarkProcessor,
          Processors::RspamdProcessor,
          Processors::SpamProcessor,
          Processors::MalwareProcessor,
          Processors::SyntaxErrorProcessor,
          Processors::PropertiesProcessor,
          Processors::LanguageTextProcessor,
          Processors::LanguageHeaderProcessor,
          Processors::FraudSignalsProcessor,
          Processors::ExcessRecipientsProcessor,
          Processors::TicketCreatorProcessor,
          Processors::AttachmentProcessor,
          Processors::CollaboratorProcessor,
          Processors::ATSDProcessor,
          Processors::CommitProcessor,
          Processors::IdentifierProcessor,
          Processors::StatsProcessor,
          Processors::ReplyParserInstrumentationProcessor
        ]
      end
    end
  end
end
