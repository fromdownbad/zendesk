module Zendesk::InboundMail
  module TicketProcessingWorkerShared
    MTC_RSS_METRIC = "rss_bytes".freeze
    MTC_RSS_ABORT_METRIC = "rss_aborts".freeze

    # Process aborts when its RSS exceeds this limit (after one GC attempt)
    MTC_RSS_LIMIT = ENV.fetch('MTC_RSS_LIMIT', 2.gigabytes).to_i

    def json
      @json ||= Zendesk::InboundMail::Support.parse_json(json_to_parse)
    end

    def json_to_parse
      # Must be defined by including class!
      raise NotImplementedError, "Ticket processing worker's json_to_parse method is not implemented"
    end

    def message
      @message ||= Zendesk::Mail::Serializers::InboundMessageSerializer.load(json)
      # AccountIDMessageLogger uses @message's account.id, so do not change
      # the order in which the @message.account and @message.logger are set
      @message.account = account
      # Overrides the default Zendesk::Mail::InboundMessage @logger instance
      # variable with Zendesk::Mail::AccountIDMessageLogger, which adds
      # the account_id to every line of the MTC logs
      @message.logger = Zendesk::InboundMail::AccountIDMessageLogger.new(@message)

      @message
    end

    def account_id
      @account_id ||= json.fetch(:account_id)
    end

    def account
      @account ||= Account.find(account_id)
    end

    def recipient
      @recipient ||= json.fetch(:recipient)
    end

    def logger
      message.logger
    end

    def work
      tags = []
      wait_time = time_since_json_creation

      if wait_time > InboundEmail::FAILURE_DURATION
        tags << "retry"
      else
        statsd_client.increment(:queued)
        statsd_client.histogram(:wait_before_processing, wait_time)
      end

      if record_mtc_processing_duration_for_event_slo?
        Zendesk::Mail.logger.progname = message.message_id.to_s.tr('<>', '')
        logger.info("Processing JSON mail #{Socket.gethostname}:#{item.path}")

        instrument(tags) do
          ActiveRecord::Base.on_shard(account.shard_id) { run_job }
        end

        logger.info("Processing JSON done")
      else
        # Remove the entire code in the else block once :email_mtc_instrument_processing_duration_event_slo arturo is GA'ed in production
        statsd_client.time(:processing, tags: tags) do
          Zendesk::Mail.logger.progname = message.message_id.to_s.tr('<>', '')
          logger.info("Processing JSON mail #{Socket.gethostname}:#{item.path}")

          ActiveRecord::Base.on_shard(account.shard_id) { run_job }

          logger.info("Processing JSON done")
        end
      end

      process_health_check!
    ensure
      Zendesk::Mail.logger.progname = nil
      statsd_client.histogram(:total, time_since_json_creation, tags: tags)
    end

    def create_unprocessable(suspension)
      Unprocessable.create(account, message, suspension)
    end

    def run_job
      Zendesk::InboundMail::TicketProcessingJob.work(
        mail:       message,
        account_id: account_id,
        route_id:   json[:route_id],
        recipient:  recipient
      )
    end

    private

    def time_since_json_creation
      Time.now - json_create_time.to_datetime
    rescue ArgumentError => e
      ZendeskExceptions::Logger.record(
        e,
        location: self,
        message: "Can't report Email's processing time since json creation!",
        reraise: !Rails.env.production?,
        fingerprint: '53b7c5c138f82e7c13d39b066c05458b7a42685b'
      )
      0
    end

    def json_create_time
      json[:headers][:date][0]
    rescue StandardError
      item.created_at
    end

    def statsd_client
      Zendesk::InboundMail::StatsD.client
    end

    def record_mtc_processing_duration_for_event_slo?
      account.has_email_mtc_instrument_processing_duration_event_slo?
    end

    def instrument(tags)
      execution_starts = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      yield
    ensure
      execution_duration = (Process.clock_gettime(Process::CLOCK_MONOTONIC) - execution_starts)

      statsd_client.timing(:processing, (execution_duration * 1000).round, tags: tags)
      statsd_client.sli_histogram_decremental(:json_processed, duration: (execution_duration * 1000).round, tags: tags)
    end

    def process_health_check!
      return if !account.has_email_mtc_memory_gc_logging? && !account.has_email_mtc_memory_auto_exit?

      # Implement our own memory limits.
      # The work hosts are shared, have limited memory. If we happen to process a large email or
      # ticket, do our best to return the memory. Ruby's GC isn't great at returning memory to the
      # OS, so if we can't, we exit, and get replaced.
      rss = ZendeskJob::RssReader.rss
      statsd_client.gauge(MTC_RSS_METRIC, rss)
      logger.info("MTC health check - current rss: #{rss}")

      if account.has_email_mtc_memory_auto_exit? && rss > MTC_RSS_LIMIT
        statsd_client.increment(MTC_RSS_ABORT_METRIC)
        rss_warning = "RSS is at #{rss}, over the limit of #{MTC_RSS_LIMIT}, aborting"
        logger.warn(rss_warning)
        raise MemoryLimitError, rss_warning
      end
    end
  end
end
