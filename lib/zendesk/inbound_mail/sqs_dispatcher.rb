require 'fog/aws'

module Zendesk::InboundMail
  class SQSMessage
    attr_accessor :message

    def initialize(sqs_message)
      @message = sqs_message&.data&.[](:body)&.[]("Message")&.[](0)
    end

    def body
      return {} unless message
      return @body if @body
      body_string = message&.[]("Body")
      @body = body_string ? Zendesk::InboundMail::Support.parse_json(body_string) : {}
    end

    def receipt_handle
      return unless message
      @receipt_handle ||= message&.[]("ReceiptHandle")
    end

    def receive_count
      return unless message
      return @receive_count if @receive_count

      unless (@receive_count = message&.[]("Attributes")&.[]("ApproximateReceiveCount"))
        Zendesk::InboundMail::StatsD.client.increment(:sqs_no_receive_count)

        # Default to 1 if a count is not provided (this shouldn't happen, thus the ^ metric)
        @receive_count = 1
      end

      @receive_count
    end

    def records
      return [] unless body.key?("Records")
      @records ||= body["Records"]
    end

    def self.record_path(record)
      record&.[](:s3)&.[](:object)&.[](:key)
    end

    def self.record_event_time(record)
      record&.[](:eventTime)
    end
  end

  class SQSDispatcher
    attr_reader :running

    SUPPORT_MAIL_PROCESSING_STATSD_NAMESPACE = "support_mail_processing".freeze

    def initialize(options = {})
      logger.info("Using SQS Dispatcher")

      @failure_handler     = QueueFailureHandler.new
      @stop_when_done      = !!options[:stop_when_done]
      @stop_when_signalled = options.fetch(:stop_when_signalled, true)
      @sleep               = options[:sleep] || 1
      @options             = options

      aws_access_key = ENV.fetch('EMAIL_AWS_SQS_ACCESS_KEY', nil)
      aws_secret_access_key = ENV.fetch('EMAIL_AWS_SQS_SECRET_KEY', nil)

      if aws_access_key && aws_secret_access_key
        options = {
          aws_access_key_id: aws_access_key,
          aws_secret_access_key: aws_secret_access_key,
          region: 'us-west-2'
        }

        # EMAIL_AWS_SESSION_TOKEN is only needed in the sandbox/local dev env
        aws_session_token = ENV.fetch('EMAIL_AWS_SESSION_TOKEN', nil)
        options[:aws_session_token] = aws_session_token if aws_session_token.present?

        @sqs = Fog::AWS::SQS.new(options)
      end

      @sqs_queue_url = ENV.fetch('MPQ_MTC_SQS_QUEUE_URL', nil)

      if !aws_access_key || !aws_secret_access_key || !@sqs_queue_url
        logger.info("Env vars configured for MTC SQS access: " \
          "Access key: #{aws_access_key.present?}, " \
          "Secret access key: #{aws_secret_access_key.present?}, " \
          "Session token: #{aws_session_token.present?}" \
          "Queue URL: #{@sqs_queue_url.present?}")
      end

      on_done do |item, worker|
        handle_done(item, worker)
      end

      on_failure(Zendesk::InboundMail::SQSMaxReceivesError, Zendesk::InboundMail::SyntaxError, Zendesk::MtcMpqMigration::Errors::ProcessingError) do |item, worker, exception|
        handle_permanent_failure(item, worker, exception)
      end

      # Handle all exceptions with retries for now
      on_failure(Exception) do |item, worker, exception|
        handle_unexpected_failure(item, worker, exception)
      end
    end

    def on_failure(*exceptions)
      @failure_handler.add_rule(*exceptions, &Proc.new)
    end

    def on_done
      @failure_handler.add_rule(nil, &Proc.new)
    end

    def handle_done(item, _worker)
      Zendesk::InboundMail::StatsD.client.increment(:processed, tags: ['result:success'])
      @sqs.delete_message(@sqs_queue_url, item.receipt_handle)
    end

    def handle_permanent_failure(item, worker, exception)
      DispatchFailureHandler.log(item.path, exception)

      Zendesk::InboundMail::StatsD.client.increment(:processed, tags: ['result:failed'])

      exception_message = 'permanently failed'

      if exception.is_a?(Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout)
        Zendesk::InboundMail::StatsD.client.increment(:processing_timeout, tags: ['type:content_processor'])
      end

      exception_is_processing_error = exception.is_a?(Zendesk::MtcMpqMigration::Errors::ProcessingError)
      suspension = exception_is_processing_error ? exception.suspension : SuspensionType.UNPROCESSABLE_EMAIL

      if create_unprocessable(worker, item, suspension)
        exception_message = exception_is_processing_error ? exception.message : 'unprocessable'
        logger.info("Created unprocessable suspended ticket")
      else
        Zendesk::InboundMail::StatsD.client.increment(:unprocessable_not_created, tags: ["source:sqs_dispatcher"])
        logger.info("Could not create unprocessable suspended ticket")
      end

      # For failures that are not due to hitting the max number of retries, we want to delete the SQS message here
      # so we don't continually retry them. TODO: we may want to change this to move the message to the DLQ instead.
      unless exception.is_a?(Zendesk::InboundMail::SQSMaxReceivesError)
        DispatchFailureHandler.notify(item.path, exception, exception_message, metadata(worker))
        @sqs.delete_message(@sqs_queue_url, item.receipt_handle)
      end

      worker.message.delete_remote_files
    end

    def handle_unexpected_failure(item, worker, exception)
      exception_message = 'failed and will retry'
      DispatchFailureHandler.log(item.path, exception)
      DispatchFailureHandler.notify(item.path, exception, exception_message, metadata(worker))

      Zendesk::InboundMail::StatsD.client.increment(:retries)
      Zendesk::InboundMail::StatsD.client.increment(:error)

      support_mail_processing_statsd_client.increment(:mpq_mtc_sqs_queue_size)

      if exception.is_a?(Zendesk::InboundMail::ProcessingTimeout)
        Zendesk::InboundMail::StatsD.client.increment(:processing_timeout, tags: ['type:generic'])
        exit(1)
      end
      # Do not delete the SQS message - this will allow it to retry if there are still retries left
      # for this email
    end

    def start
      unless @sqs_queue_url
        logger.info("MPQ_MTC_SQS_QUEUE_URL is not configured; aborting")
        return
      end

      trap_stop_signals if @stop_when_signalled
      @running = true

      while @running
        begin
          logger.info("Polling SQS")
          queue_item = @sqs.receive_message(@sqs_queue_url)
          next unless queue_item

          sqs_message = SQSMessage.new(queue_item)

          # TODO: Revisit how we record metrics for current SQS MPQ-MTC queue
          # sizeif we switch to using a combination of long polling + accepting
          # > 1 message at a time from SQS in the future. Note that if we need
          # to decrement by the total number of email JSONs received from SQS
          # all at once here, `#decrement` should be able to take a `:by`
          # parameter:
          # https://www.rubydoc.info/gems/dogstatsd-ruby/2.1.0/Datadog%2FStatsd:decrement
          #
          # Do not decrement the :mpq_mtc_sqs_queue_size if no actual
          # email messages will be processed below
          support_mail_processing_statsd_client.decrement(:mpq_mtc_sqs_queue_size) if sqs_message.records.any?

          sqs_message.records.each do |record|
            path = SQSMessage.record_path(record)
            next unless path

            remote_file = instrument_s3_json_retrieval do
              remote_file_retrieve(path)
            end

            event_time = SQSMessage.record_event_time(record)
            receipt_handle = sqs_message.receipt_handle
            receive_count = sqs_message.receive_count

            sqs_item = SQSItem.new(remote_file.content, event_time, path, receipt_handle, receive_count)
            delegate_work(sqs_item)
          end
        rescue StandardError => e
          # Exceptions occurring within the worker/processors should be handled by the regular
          # failure handler
          support_mail_processing_statsd_client.increment(:mpq_mtc_sqs_queue_size)
          exception_logger.record(e, message: 'Error receiving SQS message')
        end

        if @stop_when_done
          stop
        else
          sleep(@sleep) if @sleep
        end
      end
    end

    def stop
      @running = false
    end

    def trap_stop_signals
      Signal.trap('TERM') { stop }
      Signal.trap('INT')  { stop }
    end

    def delegate_work(item)
      worker = worker_class.new(item)
      worker.report_to(self)
      worker.perform
    end

    def work_done(item, worker, exception = nil)
      if handler = @failure_handler.handle(exception)
        handler.call(item, worker, exception)
      else
        raise exception if exception
      end
    end

    def remote_file_retrieve(identifier)
      RemoteFiles::File.new(
        identifier,
        configuration: Zendesk::Mail.raw_remote_files.name,
        stored_in:     Zendesk::Mail.raw_remote_files.stores
      ).tap(&:retrieve!)
    end

    def exception_logger
      ZendeskExceptions::Logger
    end

    def create_unprocessable(worker, _item, suspension)
      worker.create_unprocessable(suspension)
    rescue StandardError, Zendesk::Accounts::Locking::LockedException => e
      exception_logger.record(e, message: 'Error creating unprocessable')
      false
    end

    def worker_class
      @worker_class ||= Zendesk::InboundMail::SQSTicketProcessingWorker
    end

    private

    def metadata(worker)
      {}.tap do |m|
        message_id = worker&.message&.message_id
        m[:message_id] = message_id if message_id.present?
      end
    end

    def logger
      @logger ||= Zendesk::Mail.logger
    end

    def support_mail_processing_statsd_client
      @support_mail_processing_statsd_client ||= Zendesk::StatsD::Client.new(
        namespace: SUPPORT_MAIL_PROCESSING_STATSD_NAMESPACE
      )
    end

    def instrument_s3_json_retrieval
      execution_starts = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      yield
    ensure
      execution_duration = (Process.clock_gettime(Process::CLOCK_MONOTONIC) - execution_starts)
      Zendesk::InboundMail::StatsD.client.histogram(
        :s3_json_retrieval_time,
        execution_duration
      )
    end
  end
end
