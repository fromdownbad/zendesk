module Zendesk
  module InboundMail
    class Replier
      attr_accessor :body, :limit

      # Helps to identify From:, Sent:, To:, Subject:, Importance: from reply.
      # Returns [start, end] of match or nil
      def match(content)
        return unless body
        return unless match = content.match(body)
        start_of_header, end_of_header = match.offset(0)

        # Prevents overly greedy reply header matching
        return if (end_of_header - start_of_header) > limit

        [start_of_header, end_of_header]
      end
    end
  end
end
