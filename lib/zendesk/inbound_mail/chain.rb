module Zendesk
  module InboundMail
    class Chain
      delegate :to_a, :size, :member?, to: :processors

      def initialize(*processors)
        @processors = processors
        @statsd_client = Zendesk::InboundMail::StatsD.client
      end

      def execute(state, mail)
        processors.each do |processor|
          processor_name = processor.name.demodulize
          mail.logger.info("------------ #{processor_name} started ------------")
          execution_starts = Time.now

          # E.g. zendesk.mail_ticket_creator.processors{ processor_name: bounce_processor }
          @statsd_client.time(:processors, tags: ["processor_name:#{processor_name.underscore}"]) do
            processor.execute(state, mail)
          end

          execution_duration = (Time.now - execution_starts)
          mail.logger.info("#{processor_name} finished in #{execution_duration}s")
        end

        true
      rescue Halt => e
        FailureHandler::Rejection.handle(state, mail, e)
        false
      rescue StandardError => e
        mail.logger.warn "Unexpected exception: #{e.class} #{e.message}:"
        e.backtrace.take(100).each { |trace| mail.logger.warn " * #{trace}" }
        raise
      end

      def processors
        @processors ||= []
      end
    end
  end
end
