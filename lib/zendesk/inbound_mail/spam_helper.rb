# frozen_string_literal: true

module Zendesk
  module InboundMail
    module SpamHelper
      SOURCE_HEADER = 'X-Zendesk-Source'
      SOURCE_HEADER_VALUE_FOR_MAIL_FETCHER = '1'

      # Replaces SpamProcessor::SPAM_SCORE_HEADER
      RSPAMD_PRIMARY_SCORE_HEADER = 'X-Spam-Zendesk-Score'
      RSPAMD_EXPERIMENTAL_SCORE_HEADER = 'X-Spam-Zendesk-Score-Exp'

      # Replaces SpamProcessor::HARD_THRESHOLD
      RSPAMD_REJECT_THRESHOLD = 20.0

      # Replaces `SpamProcessor::HARD_THRESHOLD * SpamProcessor::SOFT_THRESHOLD`
      RSPAMD_SUSPEND_THRESHOLD = 8.0

      MAX_LOGGED_MAIL_SUBJECT_LENGTH = 500
      MAX_LOGGED_MAIL_BODY_LENGTH = 1000

      def from_mail_fetcher?
        mail.headers[SOURCE_HEADER]&.include?(SOURCE_HEADER_VALUE_FOR_MAIL_FETCHER)
      end

      def cloudmark_score
        @cloudmark_score ||= Zendesk::InboundMail::CloudmarkSpamScore.new(mail).value
      end

      # Replaces SpamProcessor#spam_score
      def rspamd_score
        mail.headers[RSPAMD_PRIMARY_SCORE_HEADER]&.first.to_f || 0.0
      end

      def rspamd_experimental_score
        mail.headers[RSPAMD_EXPERIMENTAL_SCORE_HEADER]&.first.to_f || 0.0
      end

      # Replaces SpamProcessor#obviously_spam?
      def rspamd_reject?
        # Never hard reject anything below the standard RSPAMD_REJECT_THRESHOLD,
        # no matter what the value of the multiplier is.
        threshold = if multiplier < 1.0
          RSPAMD_REJECT_THRESHOLD
        else
          multiplier * RSPAMD_REJECT_THRESHOLD
        end

        rspamd_score > threshold
      end

      # Replaces SpamProcessor#probably_spam?
      def rspamd_suspend?
        rspamd_score > multiplier * RSPAMD_SUSPEND_THRESHOLD
      end

      def rspamd_experimental_reject?
        # Never hard reject anything below the standard RSPAMD_REJECT_THRESHOLD,
        # no matter what the value of the multiplier is.
        threshold = if multiplier < 1.0
          RSPAMD_REJECT_THRESHOLD
        else
          multiplier * RSPAMD_REJECT_THRESHOLD
        end

        rspamd_experimental_score > threshold
      end

      def rspamd_experimental_suspend?
        rspamd_experimental_score > multiplier * RSPAMD_SUSPEND_THRESHOLD
      end

      def record_whitelist_stats
        statsd_client.increment("#{self.class.name.demodulize.underscore}.potential_spam_whitelisted", tags: @whitelist_logging_tags)
      end

      def rspamd_status
        @rspamd_status ||= account.has_email_rspamd? ? 'enabled' : 'disabled'
      end

      def truncated_mail_subject
        mail.subject.to_s.slice(0, MAX_LOGGED_MAIL_SUBJECT_LENGTH)
      end

      def truncated_mail_body
        mail.body.to_s.slice(0, MAX_LOGGED_MAIL_BODY_LENGTH)
      end

      def mail_recipients
        mail.recipients.join(', ')
      end

      def whitelisted_by
        @whitelisted_by ||= begin
          if state.account_whitelisted?
            :account
          elsif state.organization_whitelisted?
            :organization
          elsif state.zopim_whitelisted?
            :zopim
          end
        end
      end

      def whitelist_tags(result)
        ["whitelist:#{whitelisted_by}", "result:#{result}"]
      end

      def multiplier
        return @multiplier if @multiplier

        @multiplier = 1.0

        if state.whitelisted?
          log("Account is whitelisted")
          @multiplier *= 2.0
        end

        if submitter && submitter.is_verified?
          log("Submitter is verified")
          @multiplier *= 2

          if submitter.is_agent?
            log("Submitter is an Agent")
            @multiplier *= 2
          end
        end

        if state.authorized?
          if state.ticket && state.ticket.updated_at >= 6.months.ago
            log("Is Authorized: ticket recently updated")
            @multiplier += 2
          else
            log("Is Authorized: ticket old and cannot be trusted to bypass filter")
          end
        end

        if account.spam_threshold_multiplier != 1
          log("Custom: applying account threshold multiplier of #{account.spam_threshold_multiplier}")
          @multiplier *= account.spam_threshold_multiplier
        end

        @multiplier
      end

      def submitter
        state.submitter || state.author
      end

      def suspend(message, reason = SuspensionType.SPAM)
        super
      end

      def reject!(message)
        state.suspension = SuspensionType.SPAM
        super
      end
    end
  end
end
