require 'adrian'

module Zendesk::InboundMail
  class Dispatcher < Adrian::Dispatcher
    RETRY_QUEUE_MAX_AGE = 2.hours.freeze
    def initialize(config)
      config[:sleep] = config[:sleep_time] || 1

      super(config)

      root = Pathname.new(config.fetch(:json_queue_path))

      @new_queue = Adrian::DirectoryQueue.create(
        path: root + 'new',
        logger: Zendesk::Mail.logger
      )

      @backlog_queue = Adrian::DirectoryQueue.create(
        path: root + 'backlog',
        logger: Zendesk::Mail.logger
      )

      @retry_queue = Adrian::DirectoryQueue.create(
        path: root + 'retry',
        delay: InboundEmail::FAILURE_DURATION,
        max_age: RETRY_QUEUE_MAX_AGE,
        logger: Zendesk::Mail.logger
      )

      failed_queue = Adrian::DirectoryQueue.create(
        path: root + 'failed',
        logger: Zendesk::Mail.logger
      )

      done_queue = Adrian::DirectoryQueue.create(
        path: root + 'done',
        logger: Zendesk::Mail.logger
      )

      @queue = Adrian::CompositeQueue.new(@new_queue, @backlog_queue, @retry_queue)

      on_failure(Adrian::Queue::ItemTooOldError, Zendesk::InboundMail::SyntaxError, Zendesk::MtcMpqMigration::Errors::ProcessingError) do |item, worker, exception|
        DispatchFailureHandler.log(item.path, exception)

        Zendesk::InboundMail::StatsD.client.increment(:processed, tags: ['result:failed'])

        exception_message = 'permanently failed'

        if exception.is_a?(Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout)
          Zendesk::InboundMail::StatsD.client.increment(:processing_timeout, tags: ['type:content_processor'])
        end

        exception_is_processing_error = exception.is_a?(Zendesk::MtcMpqMigration::Errors::ProcessingError)
        suspension = exception_is_processing_error ? exception.suspension : SuspensionType.UNPROCESSABLE_EMAIL

        if create_unprocessable(item, suspension)
          exception_message = exception_is_processing_error ? exception.message : 'unprocessable'
          done_queue.push(item)
        else
          if Arturo::Feature.find_feature(:email_dispatcher_unprocessable_failed).try(:phase) == "on"
            Zendesk::InboundMail::StatsD.client.increment(:unprocessable_not_created, tags: ["source:adrian_dispatcher"])
          end
          failed_queue.push(item)
        end

        DispatchFailureHandler.notify(item.path, exception, exception_message, metadata(worker)) unless exception.is_a?(Adrian::Queue::ItemTooOldError)

        worker_for(item).message.delete_remote_files
      end

      on_failure(SignalException) do |item, _worker, _exception|
        DispatchFailureHandler.warn("Received SIGTERM while working on #{item.path}")

        Zendesk::InboundMail::StatsD.client.increment(:sigterm)
        Zendesk::InboundMail::StatsD.client.increment(:retries)
        Zendesk::InboundMail::StatsD.client.increment(:error)

        @retry_queue.push(item)

        exit(0)
      end

      on_failure(MemoryLimitError) do |item, _worker|
        # only raised after a mail has successfully processed but memory usage is
        # too high afterwards, so complete the last one and force an exit
        Zendesk::InboundMail::StatsD.client.increment(:processed, tags: ['result:success'])
        done_queue.push(item)
        exit(1)
      end

      on_failure(Exception) do |item, worker, exception|
        exception_message = 'failed and will retry'
        DispatchFailureHandler.log(item.path, exception)
        DispatchFailureHandler.notify(item.path, exception, exception_message, metadata(worker))

        Zendesk::InboundMail::StatsD.client.increment(:retries)
        Zendesk::InboundMail::StatsD.client.increment(:error)

        @retry_queue.push(item)

        if exception.is_a?(Zendesk::InboundMail::ProcessingTimeout)
          Zendesk::InboundMail::StatsD.client.increment(:processing_timeout, tags: ['type:generic'])
          exit(1)
        end
      end

      on_done do |item, _worker|
        Zendesk::InboundMail::StatsD.client.increment(:processed, tags: ['result:success'])
        done_queue.push(item)
      end
    end

    def create_unprocessable(item, suspension)
      worker_for(item).create_unprocessable(suspension)
    rescue StandardError, Zendesk::Accounts::Locking::LockedException => e
      exception_logger.record(e, message: 'Error creating unprocessable')
      false
    end

    def worker_class
      @worker_class ||= Zendesk::InboundMail::AdrianTicketProcessingWorker
    end

    def worker_for(item)
      worker_class.new(item)
    end

    def start
      super(@queue, worker_class)
    rescue StandardError => e
      exception_logger.record(e, message: 'Error dispatching work')
      raise
    end

    def exception_logger
      ZendeskExceptions::Logger
    end

    private

    def metadata(worker)
      {}.tap do |m|
        message_id = worker&.message&.message_id
        m[:message_id] = message_id if message_id.present?
      end
    end
  end
end
