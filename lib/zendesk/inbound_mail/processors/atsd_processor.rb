module Zendesk::InboundMail::Processors
  # Executes Fraud::SpamDetector code for new tickets only
  class ATSDProcessor < BaseProcessor
    delegate :ticket, to: :state

    def execute
      return unless account.has_email_atsd_processor? && account.has_spam_detector_inbound_spam?
      return if state.suspended? || state.ticket.persisted? || state.closed_ticket

      if detector.reject_ticket?
        log "mtc_ticket_model_rejected_ticket due to indicators: #{detector.spam_indicators}"
        statsd_client.increment(:fraud_spam_detection, tags: ['action:rejected'])
        reject!('Email detected as spam/fraud by ATSD with high confidence')
      elsif detector.suspend_ticket?
        raw_email_identifier = ticket.original_raw_email_identifier
        recipient = state.recipient

        log "mtc_ticket_model_suspended_ticket due to indicators: #{detector.spam_indicators}"
        statsd_client.increment(:fraud_spam_detection, tags: ['action:suspended'])
        suspend('Email detected as potential spam/fraud by ATSD', SuspensionType.MALICIOUS_PATTERN_DETECTED)
        state.ticket = TicketCreatorProcessor.new(state, mail).suspend_ticket
        Zendesk::InboundMail::Ticketing::MailTicket.new(state, mail).add_metadata(state.ticket)
        CollaboratorProcessor.new(state, mail).preserve_collaborators

        if account.has_email_train_rspamd_in_mtc? && raw_email_identifier
          RspamdFeedbackJob.report_spam(
            user: User.system,
            identifier: raw_email_identifier,
            recipient: recipient,
            classifier: Zendesk::InboundMail::RspamdTrain::RSPAMD_BAYES_CLASSIFIER_COMMON
          )

          log("Training Rspamd based on suspension in ATSDProcessor. Sender: #{state.from.address}")
          statsd_client.increment('mtc_train_rspamd', tags: ['processor:atsd_processor'])
        end
      end
    rescue StandardError => error
      raise error if error.is_a? Zendesk::InboundMail::HardMailRejectException
      warn "Error running Fraud::SpamDetector: #{error.class} #{error.message}: #{error.backtrace}"
    end

    private

    def detector
      @detector ||= Fraud::SpamDetector.new(state.ticket, false)
    end
  end
end
