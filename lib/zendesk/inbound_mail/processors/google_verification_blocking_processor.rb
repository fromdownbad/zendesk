module Zendesk::InboundMail::Processors
  # GoogleVerificationBlockingProcessor suspends email that is attempting to
  # exploit issue described in Z1#4356968. This exploit only affects ticket
  # replies, therefore we only need to run this processor when a ticket exists.
  class GoogleVerificationBlockingProcessor < BaseProcessor
    GOOGLE_NO_REPLY = 'noreply@google.com'.freeze
    GOOGLE_MESSAGE_ID = /^.+@google\.com>$/.freeze

    def execute
      return if state.suspended?
      return if no_existing_ticket_found?
      return unless google_verification_email?
      suspend('Suspending Google verification email', SuspensionType.FROM_NOREPLY)
    end

    private

    def no_existing_ticket_found?
      !state.ticket && !state.closed_ticket
    end

    def google_verification_email?
      return false unless [mail.reply_to&.address, mail.from&.address].include?(GOOGLE_NO_REPLY)
      return false unless mail.message_id =~ GOOGLE_MESSAGE_ID

      html.text.blank? || html.text =~ google_exploit_pattern
    end

    def google_exploit_pattern
      # Encoded ID, followed by account email, followed by verification code,
      # because we expect our Encoded ID to be at the end of HTML content
      /\[#{Zendesk::MtcMpqMigration::ZendeskMail::TicketIdentification::ENCODED_ID_V2_PATTERN}\].+@(#{account_domains_regex}){1}.+\d{6}/
    end

    def account_domains_regex
      "(#{account_domains.map { |domain| Regexp.escape(domain) }.join("|")})"
    end

    def account_domains
      account.recipient_addresses.map(&:domain).uniq
    end

    def html
      html_part = mail.parts.detect { |part| !part.attachment? && part.content_type == 'text/html' }
      # Note: "ArgumentError Exception: Document tree depth limit exceeded"
      # only occurs with Nokogiri::HTML5.parse
      @html ||= Nokogiri::HTML.parse(html_part&.body)
    end
  end
end
