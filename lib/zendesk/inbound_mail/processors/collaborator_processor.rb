module Zendesk::InboundMail
  module Processors
    class CollaboratorProcessor < BaseProcessor
      def execute
        if state.suspended?
          preserve_collaborators if state.ticket.legacy_or_modern_collaboration_enabled?
          return
        end

        # Save all recipients. Will be used later in notification.rb for filtering
        # out cc notification recipients already FROM, REPLY-TO, TO, CC on the email
        # Ex. "from@example.com reply-to@example.com to@example.com cc1@example.com"
        recipients_for_latest_mail = mail.all_parties
        log("Saving recipients_for_latest_mail: #{recipients_for_latest_mail}.")
        state.ticket.recipients_for_latest_mail = recipients_for_latest_mail

        # Save (TO, CC) recipients to prevent double email notifications from ActionExecutor#suppressed_recipients.
        # Ex. [to@example.com, cc1@example.com, cc2@example.com]
        to_and_ccs_for_latest_mail = mail.direct_recipients
        log("Saving to_and_ccs_for_latest_mail: #{to_and_ccs_for_latest_mail}.")
        state.ticket.to_and_ccs_for_latest_mail = to_and_ccs_for_latest_mail

        if possible_agent_reply_to_spoof?
          log("Skipping collaboration update, email flagged as 'possible reply-to agent spoofed'")
          statsd_client.increment('reply_to_processor.collaborator_processor_skipped')
          return
        end

        if account.has_follower_and_email_cc_collaborations_enabled?
          log("Author is a light agent, only followers can be updated") if author_is_light_agent?
        elsif state.author.try(:is_light_agent?) && state.ticket.persisted? && !state.ticket.requested_by?(state.author)
          log("Skipping collaboration update, light agents can only update collaborators on new tickets or as the current requester")
          return
        end

        if apply_private_comment_security_fix?
          if suspend_for_netsuite_ccs? && current_user_is_netsuite?
            suspend("Suspending collaborator update due to invalid submitter", SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE)
            state.ticket = TicketCreatorProcessor.new(state, mail).suspend_ticket
          end

          log("Skipping collaboration update, email flagged as 'other user update' because end user author <#{state.author.try(:email)}> not part of the conversation")
          return
        end

        debug_log("Ticket state before building collaborators: #{state.ticket.inspect}")
        saved_new_users = Ticketing::MailCollaboration.build_all(
          state.ticket,
          collaborator_addresses: collaborator_addresses,
          requester_is_on_email: state.requester_is_on_email,
          logger: mail_logger
        )

        log("Memoize newly created collaborators on processing state: #{saved_new_users}")
        state.add_saved_new_users(saved_new_users)

        debug_log("Ticket state after building collaborators: #{state.ticket.inspect}")
      rescue ArgumentError => e
        # Glorious ticket suspension hackery
        log("Suspending collaborator update due to errors: #{e.class}: #{e.message}.")

        state.suspension = SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE
        state.ticket = TicketCreatorProcessor.new(state, mail).suspend_ticket
      end

      def preserve_collaborators
        if author_can_add_collaborators?
          # TODO: CCs - Handle persisting :mail_ccs and :followers, based on an account
          # setting agent_ccs_become_followers <-- proposed name
          state.ticket.metadata[:ccs] = collaborator_addresses.map(&:to_hash)
          log("Ticket is suspended. Storing the following CCs as the SuspendedTicket's metadata[:ccs]: " \
            "#{state.ticket.metadata[:ccs].map { |cc| cc[:address] }.join(", ")}")
        else
          log("Skipping collaboration update, account only allows agents to update collaborators")
        end
      end

      private

      def apply_private_comment_security_fix?
        account.has_email_make_other_user_comments_private? &&
          state.flags.include?(EventFlagType.OTHER_USER_UPDATE)
      end

      def suspend_for_netsuite_ccs?
        state.ticket.current_user && !state.ticket.new_record? && !account.has_allow_netsuite_ccs?
      end

      def current_user_is_netsuite?
        collaborator_addresses.any? do |mail_address|
          downcased_address = mail_address.address.downcase
          (downcased_address =~ /@.*?\.netsuite\.com\Z/) && state.ticket.current_user.emails_include?(downcased_address)
        end
      end

      def possible_agent_reply_to_spoof?
        account.has_email_reply_to_vulnerability_collaborator_processor? &&
          state.flags.include?(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF)
      end

      # Author can add collaborators if is an Agent OR when the restriction has
      # been disabled with #is_collaborators_addable_only_by_agents? (in capabilities.rb)
      def author_can_add_collaborators?
        state.author.try(:is_agent?) || !account.is_collaborators_addable_only_by_agents?
      end

      def author_is_light_agent?
        author = state.author
        author.present? && (author.is_agent? && !author.can?(:publicly, Comment))
      end

      def collaborator_addresses
        @collaborator_addresses ||= valid_collaborator_addresses
      end

      def valid_collaborator_addresses
        excluded = original_recipients

        exclusion_rules = [
          Zendesk::InboundMail::Rules::CollaboratorExclusion::ExcludedAddress.new(mail_logger),
          Zendesk::InboundMail::Rules::CollaboratorExclusion::BadReplyAddressAndTicketReplyBug.new(mail_logger),
          Zendesk::InboundMail::Rules::CollaboratorExclusion::InvalidDomain.new(mail_logger),
          Zendesk::InboundMail::Rules::CollaboratorExclusion::DuplicatedRequesterAddress.new(mail_logger)
        ]
        chain = Zendesk::InboundMail::Rules::Chain.new(*exclusion_rules)

        mail.parties_with_sender_plus_id_removed.reject do |party|
          address = party.address
          chain.execute(state, address: address, excluded: excluded)
        end
      end

      def original_recipients
        recipients = []
        recipients << state.recipient
        recipients << state.original_recipient_address

        if state.agent_forwarded && state.submitter
          submitter_ids = state.submitter.identities.map(&:value).map(&:downcase)
          recipients.concat submitter_ids unless mail.cc.to_a.detect { |cc| submitter_ids.include?(cc.address.downcase) }
        end

        log("Envelope-To: #{mail.headers["Envelope-To"].inspect}")
        ["Delivered-To", "X-Forwarded-For", "X-BeenThere"].each do |header|
          next unless addresses = mail.headers[header].presence
          pre_delivered = addresses.map(&:address)
          log("Removing recipient as pre-delivered #{header}: #{pre_delivered.inspect}")
          recipients.concat pre_delivered
        end

        recipients.compact!
        recipients.map!(&:downcase)
        recipients.uniq!
        recipients
      end

      def suspended_ticket_collaboration_enabled?
        if account.has_follower_and_email_cc_collaborations_enabled?
          account.has_comment_email_ccs_allowed_enabled? || account.has_ticket_followers_allowed_enabled?
        else
          account.is_collaboration_enabled?
        end
      end
    end
  end
end
