module Zendesk::InboundMail::Processors
  class ReplyParserInstrumentationProcessor < BaseProcessor
    delegate :ticket, to: :state

    def execute
      instrument_reply_parsing if should_instrument_reply_parsing?
    end

    private

    def should_instrument_reply_parsing?
      return false if Zendesk::MtcMpqMigration::Content::CLIENTS_EXEMPT_FROM_DELIMITER_TRUNCATION.include?(mail.client)
      return false if account.has_no_mail_delimiter_enabled?

      account.has_email_instrument_delimiter? && ticket.present?
    end

    def instrument_reply_parsing
      Zendesk::ReplyParserInstrumentation.new(
        mail,
        mail.client,
        account.locale_id,
        account.mail_delimiter,
        compare_html: account.settings.rich_content_in_emails?
      ).instrument
    end
  end
end
