module Zendesk::InboundMail::Processors
  # Performs email sender verification such as DKIM and SPF checks
  class SenderAuthenticationProcessor < BaseProcessor
    def execute
      if account.has_email_sender_auth_report_metrics_v2?
        report_metrics_v2
      else
        report_metrics
      end

      return if logging_only?
      return unless processor_enabled?

      state.sender_auth_passed = authentication_result.pass?
      state.sender_auth_header_results = authentication_result.to_a

      audit_email   if authentication_result.audit?
      flag_email    if authentication_result.flag?
      reject_email  if authentication_result.reject?
      suspend_email if authentication_result.suspend?

      if account.has_email_mtc_sender_auth_dmarc_logging? || account.has_email_mtc_sender_auth_dmarc?
        if !state.suspended? && header_reader.dmarc_fail_and_reject?
          log("Mail was not already suspended for sender auth but dmarc=fail with a policy to reject or quarantine, sender #{state.agent ? 'is' : 'is not'} an agent")
          statsd_client.increment('sender_auth_processor.detect_dmarc_fail', tags: ["from:#{state.agent ? 'agent' : 'enduser'}"])

          if account.has_email_mtc_sender_auth_dmarc? && state.agent
            # Suspending for now since it's safer
            suspend("Failed DMARC authentication", SuspensionType.FAILED_DMARC_AUTHENTICATION)

            statsd_client.increment('sender_auth_processor.suspend_dmarc_fail')
          end
        end
      end
    end

    private

    def header_reader
      @header_reader ||= Zendesk::InboundMail::SenderAuthentication::HeaderReaderFactory.create(mail)
    end

    def authentication_result
      @authentication_result ||= Zendesk::InboundMail::SenderAuthentication::Processor.call(header_reader, mail)
    end

    def reject_email
      reject!(spoofing_zendesk_log_message)
    end

    def suspend_email
      suspend(suspending_log_message, SuspensionType.FAILED_EMAIL_AUTHENTICATION)
    end

    def flag_email
      state.add_flag(EventFlagType.POTENTIAL_MESSAGE_SPOOFING, message: { userEmail: state.from.address })
    end

    def audit_email
      log("SPF Identifier Alignment not compliant. From/Reply-To does not match Envelope-From/Return-Path. #{state.sender_auth_header_results.to_sentence}")
      statsd_client.increment('spf_identifier_alignment_non_compliant')
    end

    def logging_only?
      if account.has_email_sender_authentication_logging? && !account.has_email_sender_authentication?
        log('Account has email_sender_authentication_logging Arturo enabled')
        true
      else
        false
      end
    end

    def processor_enabled?
      if account.has_email_sender_authentication?
        log('Account has email_sender_authentication_processing Arturo and setting enabled')
        true
      elsif authentication_result.sender_rule == :disreputable
        log("#{mail.sender.domain} is subject to mandatory sender authentication due to sender rule: 'disreputable'")
        true
      else
        log('Skipping SenderAuthenticationProcessor')
        false
      end
    end

    def suspending_log_message
      "FAILED_EMAIL_AUTHENTICATION: #{state.sender_auth_header_results.to_sentence}"
    end

    def spoofing_zendesk_log_message
      message = "Email claims to be from Zendesk, but does not match known Zendesk IPs. Rejecting! "
      message += "Return-Path, Envelope-From or From: #{mail.smtp_envelope_from}, "
      message += "Recipient addresses: #{recipient_support_addresses.join(' ')}, "
      message += "Auth Header: #{header_reader.header || 'None'}, "
      message += "Auth Header Value: #{header_reader.values.join(' ')}"
      message
    end

    def recipient_support_addresses
      recipient_addresses = [mail.to, mail.cc, mail.bcc].flatten
      wildcard_domains = account.settings.accept_wildcard_emails ? account.routes.map(&:default_host) : []
      recipient_addresses.select { |addr| account.recipient_emails.include?(addr.address) || wildcard_domains.include?(addr.domain) }.uniq.map(&:address)
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [:mail_ticket_creator, :sender_authentication])
    end

    def report_metrics
      header = header_reader.header || 'missing_auth_header'.freeze

      log(
        "Outcome: #{authentication_result.action}, " \
        "account: #{account.id}, " \
        "from: #{mail.sender.domain}, " \
        "whitelisted: #{state.whitelisted?}, " \
        "flagged: #{authentication_result.flag?}, " \
        "dry_run: #{logging_only?}, " \
        "auth_header: #{header}, " \
        "rule: #{authentication_result.sender_rule}, " \
        "auth_results: #{authentication_result.to_a.to_sentence}"
      )

      statsd_tags = %W[
        action:#{authentication_result.action}
        origin:#{authentication_result.origin}
        SPF:#{authentication_result.spf_status}
        DKIM:#{authentication_result.dkim_status}
        dry_run:#{logging_only?}
      ]

      statsd_client.increment(header.underscore, tags: statsd_tags)
    end

    # Like #report_metrics, but adds information about DMARC as well as
    # if the user is an agent or not
    def report_metrics_v2
      header = header_reader.header || 'missing_auth_header'.freeze
      sender_role = state.agent ? 'agent' : 'end_user'
      dmarc_results = [header_reader.dmarc, header_reader.dmarc_policy]
      auth_results_with_dmarc = authentication_result.to_a + dmarc_results

      log(
        "Outcome: #{authentication_result.action}, " \
        "account: #{account.id}, " \
        "from: #{mail.sender.domain}, " \
        "whitelisted: #{state.whitelisted?}, " \
        "flagged: #{authentication_result.flag?}, " \
        "dry_run: #{logging_only?}, " \
        "auth_header: #{header}, " \
        "rule: #{authentication_result.sender_rule}, " \
        "sender_role: #{sender_role}, " \
        "auth_results: #{auth_results_with_dmarc.to_sentence}" \
      )

      statsd_tags = %W[
        action:#{authentication_result.action}
        origin:#{authentication_result.origin}
        SPF:#{authentication_result.spf_status}
        DKIM:#{authentication_result.dkim_status}
        dry_run:#{logging_only?}
        DMARC:#{header_reader.dmarc}
        DMARC_policy:#{header_reader.dmarc_policy}
        sender_role:#{sender_role}
      ]

      statsd_client.increment(header.underscore.to_s, tags: statsd_tags)
    end
  end
end
