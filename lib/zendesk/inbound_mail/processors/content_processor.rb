require 'nokogumbo'

module Zendesk::InboundMail::Processors
  # Extracts the body of the message.
  #   Preconditions:  account
  #   Postconditions: state.plain_content
  class ContentProcessor < BaseProcessor
    delegate :content, to: :mail
    delegate :ticket, to: :state

    def execute
      if updating_existing_ticket? && !suspected_forward?
        state.plain_content = if mail.processing_state.plain_content&.present?
          mail.logger.info('Setting plain text content using parsed plain text reply')

          if account.has_no_mail_delimiter_enabled? && !account.settings.rich_content_in_emails?
            if mail.processing_state.plain_content != mail.processing_state.quoted_plain_content
              state.add_flag(EventFlagType.REPLY_PARSER_TRUNCATED)
            else
              mail.logger.info('Skipping REPLY_PARSER_TRUNCATED since plain text reply and quoted content match')
            end
          end

          mail.processing_state.plain_content
        elsif mail.attachments.present? && account.has_email_attachment_only_reply_fix?
          mail.logger.info("Attachments present but no content in parsed reply - Setting plain text content to placeholder")
          statsd_client.increment(:placeholder_content_fallback, tags: %w[type:attachment_only_plain_reply location:content_processor])
          ::I18n.t("txt.email.content_processor.no_content")
        elsif mail.processing_state.full_plain_content&.present?
          mail.logger.info('No content in parsed reply - Setting plain text content using full plain text message')
          statsd_client.increment(:full_content_fallback, tags: %w[type:no_plain_reply location:content_processor])

          mail.processing_state.full_plain_content
        else
          ""
        end

        if account.settings.rich_content_in_emails?
          state.html_content = if Zendesk::InboundMail::HTMLDocument.content_present?(mail.processing_state.html_content)
            mail.logger.info('Setting HTML content using parsed HTML reply')

            if account.has_no_mail_delimiter_enabled?
              if mail.processing_state.html_content != mail.processing_state.quoted_html_content
                state.add_flag(EventFlagType.REPLY_PARSER_TRUNCATED)
              else
                mail.logger.info('Skipping REPLY_PARSER_TRUNCATED since HTML reply and quoted content match')
              end
            end

            mail.processing_state.html_content
          elsif mail.attachments.present? && account.has_email_attachment_only_reply_fix?
            mail.logger.info("Attachments present but no content in parsed reply - Setting HTML content to placeholder")
            statsd_client.increment(:placeholder_content_fallback, tags: %w[type:attachment_only_html_reply location:content_processor])
            ::I18n.t("txt.email.content_processor.no_content")
          elsif Zendesk::InboundMail::HTMLDocument.content_present?(mail.processing_state.full_html_content)
            mail.logger.info('No content in parsed reply - Setting HTML content using full HTML message')
            statsd_client.increment(:full_content_fallback, tags: %w[type:no_html_reply location:content_processor])

            mail.processing_state.full_html_content
          elsif mail.processing_state.plain_content&.present? && (mail.processing_state.plain_content != mail.processing_state.quoted_plain_content)
            state.add_flag(EventFlagType.REPLY_PARSER_TRUNCATED)
            nil
          end
        end
      else
        mail.logger.info("Using full content - #{suspected_forward? ? 'suspected forward' : 'new ticket'}")
        statsd_client.increment(:full_content_fallback, tags: %w[type:suspected_forward location:content_processor]) if suspected_forward?

        state.plain_content = mail.processing_state.full_plain_content || ""

        if account.settings.rich_content_in_emails?
          mail.logger.info('Setting HTML content using full HTML content')
          state.html_content = mail.processing_state.full_html_content
        end
      end

      state
    rescue Zendesk::InboundMail::ProcessingTimeout => exception
      mail.logger.error("Intercepted ProcessingTimeout. #{exception.class.name}: #{exception.message}")
      raise Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout.new, 'ContentProcessor timed out'
    end

    private

    # Makes the assumption that ticket creation processor hasn't run yet, thus any ticket in the state is an existing ticket.
    def updating_existing_ticket?
      ticket.present? || state.closed_ticket
    end

    def suspected_forward?
      @suspected_forward ||= mail.suspected_forward?
    end
  end
end
