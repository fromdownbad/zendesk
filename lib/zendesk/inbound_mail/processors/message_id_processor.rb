module Zendesk::InboundMail::Processors
  # Detects repeated emails with same message id
  class MessageIdProcessor < BaseProcessor
    NULL_STRINGS = %w[<null> <$null>].freeze

    def execute
      handle_message_id_null_strings if account.has_email_replace_message_id_null_string?

      if inbound_email = account.inbound_emails.where(message_id: mail.message_id).first
        unless inbound_email.failed?
          log_message = rejection_message("original created: #{inbound_email.created_at}")
          reject_for_duplicated_mail!(log_message)
        end
      else
        catch_race_condition_errors do
          debug_log("Creating inbound email for <#{state.from.address}>")

          inbound_email = account.inbound_emails.create! do |i|
            i.from = account.has_email_set_inbound_email_from_address? ? mail.from.address : state.from.address
            i.message_id = mail.message_id
            i.in_reply_to = mail.in_reply_to if mail.in_reply_to =~ /\A<.{5,100}>\Z/
            i.references = mail.references
          end

          debug_log("Inbound email created: #{inbound_email.inspect}")
        end
      end
      state.inbound_email = inbound_email
    end

    private

    def reject_for_duplicated_mail!(message)
      # Do not hard delete files because a duplicate message already exists:
      raise Zendesk::InboundMail::DuplicateMailRejectException, message
    end

    def catch_race_condition_errors
      yield
    rescue ActiveRecord::StatementInvalid => e
      if e.message.include?("Duplicate entry")
        log_message = rejection_message("original created: #{Time.now} (race condition)")
        reject_for_duplicated_mail!(log_message)
      else
        raise e
      end
    end

    def rejection_message(message = nil)
      "Mail soft rejected due to duplicate message ids: #{mail.message_id}. #{message}"
    end

    def handle_message_id_null_strings
      if NULL_STRINGS.include? mail.message_id
        mail.message_id = mail.class.generate_message_id
        state.message_id = mail.message_id
        log "Message ID was null, updating it to #{mail.message_id}"
      end
    end
  end
end
