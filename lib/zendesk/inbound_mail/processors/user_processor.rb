module Zendesk::InboundMail::Processors
  # Finds or instantiates a new user and sets it on state.
  class UserProcessor < BaseProcessor
    MAX_EMAIL_ADDRESS_LENGTH = 255

    def execute
      reject_if_from_address_too_long

      if state.author = author
        if possible_email_takeover?
          log("Author ##{author.id} found, but matching an address that hasn't been verified: '#{state.from.address}'")
        else
          log("Author ##{author.try(:id)} found!")
        end
      else
        log("No author found!")
      end

      if possible_agent_reply_to_spoof? && account.has_email_reply_to_vulnerability_user_processor?
        log("Account is not open but anonymous user tried to impersonate as an agent and added a comment")
      end

      if !author && !state.suspended?
        suspend_if_system_user || suspend_if_unknown_author || suspend_if_signup_required
      end

      state.author ||= build_author unless state.suspended?

      process_ticket_sharing_partner
      process_machine_user
      suspend_if_suspended_author
    end

    private

    def possible_agent_reply_to_spoof?
      state.original_author.nil? && state.flags.include?(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) && !account.is_open?
    end

    def reject_if_from_address_too_long
      if (address = state.from.address) && (address.length > MAX_EMAIL_ADDRESS_LENGTH)
        statsd_client.increment('user_processor.reject_from_address_too_long')
        reject!('Sender email address is over the limit of characters allowed')
      end
    end

    def author
      return @author if @author
      log("Looking up the author with '#{state.from.address}' address")
      @author = account.find_user_by_email(state.from.address)
    end

    def possible_email_takeover?
      return false unless account.has_email_takeover_vulnerability_stats?

      # Logic to avoid doing extra SQL queries
      email_identities = author.identities.email.pluck(:value, :is_verified)
      total = email_identities.size
      total_verified = email_identities.count(&:second)
      total_unverified = total - total_verified

      log("Total identities found: #{total}, verified: #{total_verified}, unverified: #{total_unverified}")
      return false if total == 1 # There is no takeover if there is only one identity

      from_is_verified = email_identities.any? { |identity_map| identity_map.first == state.from.address && identity_map.second }
      log("The author's address '#{state.from.address}' is #{from_is_verified ? 'verified' : 'unverified'}")
      return false if from_is_verified # There is no takeover if the from is already verified

      from_is_default = state.from.address.casecmp?(author.email)
      log("The author's address '#{state.from.address}' #{from_is_default ? 'is' : 'is not'} the default email identity")
      return false if from_is_default # There is no takeover if from is already the default identity

      warn("Possible email takeover vulnerability! Mail from '#{state.from.address}' assigned to User ##{author.id} using '#{author.email}' as the default identity.")
      tags = ["total:#{total}", "total_verified:#{total_verified}", "total_unverified:#{total_unverified}"]
      statsd_client.increment('user_processor.email_takeover_vulnerability.likely', tags: tags)

      true
    end

    def author_suspended?
      state.author.try(:suspended?) || (state.author.try(:organization) && state.author.organization.suspended?)
    end

    def process_ticket_sharing_partner
      return unless mail.from_zendesk?
      if sharing_agreement_with_account?(mail.zendesk_from_account_id)
        mark_author_as_ticket_sharing_partner
        reject!("Mail sent from a Zendesk with which this account has a sharing agreement, hard reject")
      end
    end

    def process_machine_user
      return if mail.from_collaboration_app?

      if mail.from_zendesk? || mail.from_external_machine?
        mark_author_as_machine
        state.add_flag(EventFlagType.MACHINE_GENERATED)
      end
    end

    def mark_author_as_machine
      # state.author is nil if the state is suspended and the from address is not already a User
      return unless state.author
      log("Marking author as a machine")
      Zendesk::InboundMail::AuthorDeliverableState.mark(
        state: state,
        new_deliverable_state: DeliverableStateType.MACHINE,
        logger: mail_logger,
        author: state.author
      )
    end

    def mark_author_as_ticket_sharing_partner
      # state.author is nil if the state is suspended and the from address is not already a User
      author = persisted_author
      author.identities.detect { |identity| identity.value == state.from.address }.tap do |identity|
        if identity
          identity.mark_as_ticket_sharing_partner
        else
          log("Could not find identity for ticket sharing partner #{state.from.address}")
        end
      end
    end

    def suspend_if_suspended_author
      if author_suspended?
        update_suspension(SuspensionType.SPAM, "Suspending as spam due to author being suspended")
      end
    end

    def suspend_if_system_user
      regex = /(daemon|system administrator|postmaster).*@/i
      if state.from.address&.match?(regex)
        update_suspension(SuspensionType.SYSTEM_USER, "Suspended as from header matches #{regex.inspect}")
      end

      state.suspended?
    end

    def suspend_if_unknown_author
      unless account.is_open?
        update_suspension(SuspensionType.UNKNOWN_AUTHOR, "Suspended as author is unknown and account is not open.")
      end

      state.suspended?
    end

    def suspend_if_signup_required
      if account.is_signup_required? && !state.agent_forwarded
        # The suspended ticket save handles sending an email to the user when it gets committed
        update_suspension(SuspensionType.SIGNUP_REQUIRED)
      end

      state.suspended?
    end

    def update_suspension(cause, log_message = nil)
      state.suspension = cause

      log_message ||= "Suspending due to: #{SuspensionType.to_s(state.suspension)}"
      log(log_message)
    end

    # Create the user, don't send them a welcome email as signup is not required. Their ticket will get
    # created and if they want to login, they will get prompted to verify first (is_verified default false)
    def build_author
      log("Building new user with <#{state.from.address}> address")

      name = state.from.name.strip
      name_length = name.length

      if name_length < 2
        name = "Unknown #{name}".strip
      elsif name_length > limit = Zendesk::Extensions::TruncateAttributesToLimit.limit(User.columns_hash.fetch('name'))
        name = name.slice(0, limit)
        log("Truncating requester name since its length #{name_length} exceeds limit #{limit}.")
        state.add_flag(EventFlagType.REQUESTER_NAME_TRUNCATED)
      end

      new_user = User.new.tap do |user|
        user.account = account
        user.email   = state.from.address
        user.name    = name
        user.skip_email_identity_blacklist_validation = true
      end

      debug_log("Built user: #{new_user.inspect}")
      new_user
    end

    def persisted_author
      persisted_author = state.author || build_author
      adapter = Zendesk::InboundMail::Adapters::AuthorAdapter.new(processing_state: state,
                                                                  account: account,
                                                                  mail_logger: mail_logger,
                                                                  statsd_client: statsd_client)
      adapter.save_with_instrumentation!
      persisted_author
    end

    def sharing_agreement_with_account?(obfuscated_remote_id)
      return false if obfuscated_remote_id.blank?

      account.sharing_agreements.accepted.any? do |agreement|
        Zendesk::Mail.obfuscate_id(agreement.remote_account.try(:id)) == obfuscated_remote_id
      end
    end
  end
end
