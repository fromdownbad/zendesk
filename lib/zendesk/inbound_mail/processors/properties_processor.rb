module Zendesk::InboundMail::Processors
  # Parses out the email properties as a key/value hash and sets them on state. The processor
  # converts the keys from human friendly format to a more stringent format, e.g. it turns
  #   {{requester:someone@example.com group:23}}
  # into
  #   { requester_email: "someone@example.com", group_id: 23 }
  class PropertiesProcessor < BaseProcessor
    # These are the properties that are allowed as part of the email API. This hash maps each of these
    # to a function that resolves the value into an appropriate identifier within Zendesk
    ACCEPTED_PROPERTIES = {
      requester: :resolve_user,
      assignee: :resolve_named_user,
      group: :resolve_group,
      priority: :resolve_state,
      ticket_type: :resolve_state,
      status: :resolve_state,
      tags: :resolve_tags,
      public: :resolve_access,
      requester_name: :resolve_requester_name,
      requester_phone: :resolve_requester_phone
    }.freeze

    STATE_RESOLVERS = {
      status: StatusType,
      ticket_type: TicketType,
      priority: PriorityType
    }.freeze

    def execute
      subject_properties = parse_mail

      if state.properties.nil?
        log("Parsing properties from email subject")
        state.properties = subject_properties
      elsif subject_properties
        log("Parsing properties from email subject and state")
        state.properties = subject_properties.merge(state.properties)
      end

      new_subject = mail.subject.to_s.gsub(/\{\{.+\}\}/, '')
      log "Cleaning properties from subject '#{mail.subject}': '#{new_subject}'"
      mail.subject = new_subject
    end

    private

    def parse_mail(subject = nil)
      subject ||= mail.subject.to_s
      match = subject.match(/\{\{(.+?)\}\}/)
      return unless match

      log("Parsing: '#{subject}' subject")

      props  = match[1]
      result = {}

      ACCEPTED_PROPERTIES.keys.each do |key|
        # Match properties being set in quotes or without quotes
        if match = props.match(/#{key}:"([^"]*)"/i) || props.match(/#{key}:\s*(\S+)/i)
          send(ACCEPTED_PROPERTIES[key], result, key, match[1]) if match[1].present?
        end
      end

      log("Result: #{result.inspect}")

      result.any? ? result : nil
    end

    def resolve_named_user(result, key, value)
      resolve_user(result, key, value, true)
    end

    def resolve_user(result, key, value, named = false)
      if /^\d+$/.match?(value)
        result["#{key}_id".to_sym]    = value
      elsif EMAIL_PATTERN.match?(value.downcase)
        result["#{key}_email".to_sym] = value.downcase
      elsif named && value.present?
        # This is an undocumented feature that allows assignee lookups by name in the email API. It used
        # to be documented but has been removed as people really should go by the email address instead.
        # Since the number of assignees is low, we're leaving it in for a "soft exit" for now.
        result["#{key}_name".to_sym] = value.downcase
      end
      result
    end

    def resolve_group(result, _key, value)
      if /^\d+$/.match?(value)
        result[:group_id]   = value
      else
        result[:group_name] = value
      end
      result
    end

    def resolve_state(result, key, value)
      resolver = STATE_RESOLVERS[key]
      if resolver && resolver == StatusType && value.casecmp('on-hold').zero?
        value = 'hold'
      end
      if resolver && state = resolver.find(value.downcase)
        result["#{key}_id".to_sym] = state
      end
      result
    end

    def resolve_tags(result, _key, value)
      if value.present?
        result[:set_tags] = value.downcase
      end
      result
    end

    def resolve_access(result, _key, value)
      if value.present?
        result[:is_public] = !value.to_s.casecmp('false').zero?
      end
      result
    end

    def resolve_requester_name(result, _key, value)
      if value.present?
        result[:requester_name] = value
      end
      result
    end

    def resolve_requester_phone(result, _key, value)
      if value.present?
        result[:requester_phone] = Voice::Core::NumberSupport::E164Number.internal(value)
      end
      result
    end
  end
end
