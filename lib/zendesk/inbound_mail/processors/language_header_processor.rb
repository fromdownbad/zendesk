require 'set'

module Zendesk::InboundMail::Processors
  # Extracts language header from mail and sets the corresponding locale if it matches one of the
  # account available languages and if the author is a new user. If no header is present, the language
  # is detected programatically and matched.
  class LanguageHeaderProcessor < BaseProcessor
    # Language header examples:
    # Accept-Language: en-GB, en-AU, en-US
    # X-Accept-Language: fr, en

    NORWEGIAN_LOCALES = Set['nn', 'nb']

    def execute
      return if !state.author || state.author.locale_id
      unless language_header
        log("No Accept-Language headers detected")
        return
      end

      log("Language header detected: #{language_header}")

      if locale = detect_locale_from_header
        log("Setting user #{state.author.name} language to #{locale}")
        author.translation_locale = locale
      end
    end

    protected

    def detect_locale_from_header
      languages = language_header.tr(" ", "").split(",").map { |l| l.split("-")[0] }.uniq
      languages.detect do |language|
        locale = account.available_languages.detect { |tl| language == tl.locale.split("-")[0] }

        # Special case for Norway, we need to also recognize 'nb-NO', 'nn-NO' as Norwegian
        if !locale && NORWEGIAN_LOCALES.include?(language)
          locale = account.available_languages.detect { |tl| tl.name == 'Norsk' }
        end

        return locale if locale
      end
    end

    def language_header
      (mail.headers["Accept-Language"] || mail.headers["X-Accept-Language"]).try(:first)
    end

    def author
      state.author
    end
  end
end
