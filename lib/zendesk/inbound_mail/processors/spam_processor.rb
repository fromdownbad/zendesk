module Zendesk::InboundMail::Processors
  # Checks if the email is spam and if so, suspends the email by placing
  # it in the 'ticket bin'.
  class SpamProcessor < BaseProcessor
    SPAM_SCORE_HEADER = 'X-Spam-Zendesk-Score'.freeze

    SOURCE_HEADER = 'X-Zendesk-Source'.freeze
    SOURCE_IS_MAIL_FETCHER = '1'.freeze

    HARD_THRESHOLD = 20.0
    SOFT_THRESHOLD = 0.4

    def execute
      # Kill switch! use with caution.
      if account.has_email_skip_spam_processor?
        warn("WARNING! Account '#{account.subdomain}' has the Spam Processor disabled (via email_skip_spam_processor Arturo)")
        statsd_client.increment('spam_processor.disabled')
        return
      end

      if from_mail_fetcher?
        log("Skipping SpamProcessor for emails fetched from Mail Fetcher (Gmail Connector)")
        statsd_client.increment('spam_processor.skip_mail_fetcher_email')
        return
      end

      if filter_with_cloudmark
        if rspamd_obviously_spam?
          statsd_client.increment(:cloudmark_overruled, tags: ["spam_suspension:#{state.suspension == SuspensionType.SPAM}"])

          if state.suspension != SuspensionType.SPAM
            log("Cloudmark overruled due to Rspamd score of #{spam_score} > multiplier (#{multiplier}) * HARD_THRESHOLD (#{HARD_THRESHOLD})")
          end

          suspend("Cloudmark overruled: suspending due to high Rspamd score of #{spam_score}")
        end

        return
      end

      if rspamd_obviously_spam?
        reject!(
          "Mail hard rejected due to spam score of #{spam_score} > " \
          "multiplier (#{multiplier}) * HARD_THRESHOLD (#{HARD_THRESHOLD})"
        )
      end

      if rspamd_probably_spam?
        suspend(
          "Suspended due to spam score of #{spam_score} > " \
          "multiplier (#{multiplier}) * spam_threshold (#{HARD_THRESHOLD}) * soft_threshold (#{SOFT_THRESHOLD})"
        )
      end

      log("Spam multiplier is: #{multiplier}")
    end

    private

    def rspamd_status
      @rspamd_status ||= account.has_email_rspamd? ? 'enabled' : 'disabled'
    end

    def truncated_mail_subject
      mail.subject.to_s.slice(0, 500)
    end

    def truncated_mail_body
      mail.body.to_s.slice(0, 1000)
    end

    def mail_recipients
      mail.recipients.join(', ')
    end

    def whitelisted_by
      if state.account_whitelisted?
        :account
      elsif state.organization_whitelisted?
        :organization
      elsif state.zopim_whitelisted?
        :zopim
      end
    end

    def whitelist_tags(result)
      ["whitelist:#{whitelisted_by}", "result:#{result}"]
    end

    def from_mail_fetcher?
      mail.headers[SOURCE_HEADER]&.include?(SOURCE_IS_MAIL_FETCHER)
    end

    def cloudmark_score
      @cloudmark_score ||= Zendesk::InboundMail::CloudmarkSpamScore.new(mail).value
    end

    def spam_score
      mail.headers[SPAM_SCORE_HEADER]&.first || 0
    end

    alias_method :rspamd_score, :spam_score

    def multiplier
      return @multiplier if @multiplier

      @multiplier = 1.0

      if state.whitelisted?
        log("The mail is whitelisted. Multiplying the spam multiplier by 2")
        @multiplier *= 2.0
      end

      if submitter && submitter.is_verified?
        log("The submitter has at least one verified identity. Multiplying the spam multiplier by 2")
        @multiplier *= 2

        if submitter.is_agent?
          log("The submitter is an Agent. Multiplying the spam multiplier by 2")
          @multiplier *= 2
        end
      end

      if state.authorized?
        if state.ticket && state.ticket.updated_at >= 6.months.ago
          log("The mail is authorized, and the ticket is considered recent. Incrementing the spam multiplier by 2")
          @multiplier += 2
        else
          log("The mail is authorized, but the ticket is older than 6 months. Can't increment the spam multiplier")
        end
      end

      if account.spam_threshold_multiplier != 1
        log("The account has a custom spam threshold. Multiplying the spam multiplier by #{account.spam_threshold_multiplier}")
        @multiplier *= account.spam_threshold_multiplier
      end

      if check_ignore_multiplier? && ignore_multiplier?
        if account.has_email_ignore_spam_multiplier?
          statsd_client.increment('spam_processor.ignore_multiplier')
          log("Resetting multiplier to 1.0: " + ignore_multiplier_log_message)
          @multiplier = 1.0
        else
          statsd_client.increment('spam_processor.ignore_multiplier_log_only')
          log("LOG-ONLY: " + ignore_multiplier_log_message)
        end
      end

      @multiplier
    end

    def check_ignore_multiplier?
      account.has_email_ignore_spam_multiplier? || account.has_email_ignore_spam_multiplier_logging?
    end

    def ignore_multiplier?
      cloudmark_and_rspamd_both_detect_spam_without_multiplier? && state.ticket
    end

    def cloudmark_and_rspamd_both_detect_spam_without_multiplier?
      cloudmark_score = Zendesk::InboundMail::CloudmarkSpamScore.new(mail)

      cloudmark_score.value && cloudmark_score.obviously_spam? && (spam_score.to_f > HARD_THRESHOLD * SOFT_THRESHOLD)
    end

    def ignore_multiplier_log_message
      mail_body = state.plain_content&.slice(0, 500)
      mail_body_without_newlines = mail_body&.gsub(/\s+/, ' ')

      [
        "Cloudmark and Rspamd both detect spam, and mail is in reply to an existing ticket.",
        "Cloudmark: #{cloudmark_score}. Rspamd: #{spam_score}. Original multiplier: #{@multiplier}.",
        "Ticket Requester ID: #{state.ticket.requester.id}; Ticket requester email: #{state.ticket.requester.email}",
        "Ticket ID: #{state.ticket.id}. Ticket Subject: '#{state.ticket.subject}'. ",
        "Mail Subject: '#{mail.subject&.slice(0, 100)}'. Mail Body: '#{mail_body_without_newlines}'.",
        "X-Rspamd-Authentication-Results: '#{mail.headers['X-Rspamd-Authentication-Results']&.first}'.",
        "X-Rspamd-Status: '#{mail.headers['X-Rspamd-Status']&.first}'."
      ].join(" ")
    end

    protected

    def submitter
      state.submitter || state.author
    end

    def rspamd_obviously_spam?
      # Never hard reject anything below the HARD_THRESHOLD, no matter
      # what the value of the multiplier is.
      threshold = if multiplier < 1
        HARD_THRESHOLD
      else
        multiplier * HARD_THRESHOLD
      end

      spam_score.to_f > threshold
    end

    def rspamd_probably_spam?
      spam_score.to_f > multiplier * (HARD_THRESHOLD * SOFT_THRESHOLD)
    end

    def filter_with_cloudmark
      enabled = true

      if account.has_email_rspamd?
        warn("Skipping Cloudmark because '#{account.subdomain}' has Rspamd enabled (via email_rspamd Arturo)")
        statsd_client.increment('spam_processor.rspamd.enabled')
        enabled = false
      end

      cloudmark_score = Zendesk::InboundMail::CloudmarkSpamScore.new(mail)
      if cloudmark_score.value.nil?
        log("Cloudmark: missing score, skipping")
        unless account.has_email_rspamd?
          log("Falling back on Rspamd, even though '#{account.subdomain}' does not have Rspamd enabled (via email_rspamd Arturo)")
        end
        statsd_client.increment(:cloudmark, tags: ["result:missing_score", "enabled:#{enabled}"])
        return false
      end

      if cloudmark_score.obviously_spam?
        if multiplier > 1
          log("Cloudmark: Skipping suspension for spam score: #{cloudmark_score.value}, threshold: #{cloudmark_score.threshold}, multiplier: #{multiplier}")
          statsd_client.increment(:cloudmark, tags: ["result:skipped_by_multiplier", "multiplier:#{multiplier}"])
        else
          statsd_client.increment(:cloudmark, tags: ["result:obviously_spam", "enabled:#{enabled}", "value:#{cloudmark_score.value}", "threshold:#{cloudmark_score.threshold}"])

          message = "Cloudmark: suspended due to spam score: #{cloudmark_score.value}"
          if enabled
            log("Cloudmark: Suspending for spam score: #{cloudmark_score.value}, threshold: #{cloudmark_score.threshold}, multiplier: #{multiplier}")
            suspend(message)
          end
        end
      elsif cloudmark_score.probably_spam?
        log("Cloudmark: probably spam, but allowing. Spam score: (#{cloudmark_score.value}), Multiplier: (#{multiplier})")
        statsd_client.increment(:cloudmark, tags: ["result:probably_spam", "enabled:#{enabled}", "value:#{cloudmark_score.value}"])
      end

      if !enabled && message
        log("#{message} DRY_RUN")
      end

      enabled
    end

    def suspend(message, reason = SuspensionType.SPAM)
      super
    end

    def reject!(message)
      state.suspension = SuspensionType.SPAM
      super
    end
  end
end
