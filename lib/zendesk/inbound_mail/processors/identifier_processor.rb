module Zendesk::InboundMail::Processors
  # Associates the email message id to the now saved ticket
  # Updates the email category
  class IdentifierProcessor < BaseProcessor
    delegate :comment, :ticket, to: :state

    def execute
      unless state.suspended?
        state.inbound_email.ticket_id = ticket.id
        state.inbound_email.comment = comment

        log("Related email to ticket #{ticket.class.name}/#{ticket.id} (nice_id: #{ticket.nice_id})")
        log("Related email to comment #{comment.id}") if comment
      end

      state.inbound_email.mark_by_system(state.suspension || InboundEmail.categories[:ham])
      state.inbound_email.save!
    end
  end
end
