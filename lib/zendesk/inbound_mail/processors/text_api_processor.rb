module Zendesk::InboundMail::Processors
  # Internal: Allows updating a Ticket using the text based API.
  #
  # The text API consists of simple key-value declarations, using the format `@tagname value`, where `tagname` refers
  # to a recognized tag, and value is a string, the meaning of which varies based on the tag.
  #
  # The tags should be listed at the top of the mail, and will be stripped from the comment left on the Ticket.
  #
  # Note that we only strip API commands from the HTML comment if we've already found them in the plain text version.
  # If they exist in the HTML but not plain text, they will not get executed/stripped
  #
  # The currently recognized tags are:
  #
  # `#public`    - sets the visibility of the comment (yes, no, true, false).
  # `#status`    - sets the status of the ticket (open, pending, solved).
  # `#priority`  - sets the priority of the ticket (low, normal, high, urgent).
  # `#type`      - sets the type of the ticket (incident, problem, task, question).
  # `#tags`      - adds the tags to the ticket.
  # `#tag`       - same as `@tags`.
  # `#assignee`  - assigns the ticket to a User specified by either a user id or
  #                an email address.
  # `#requester` - sets the requester to a User specified by either a user id or
  #                an email address.
  # `#group`     - assigns the ticket to a Group specified by either a group id
  #                or a group name.
  # `#open`      - sets the status to open
  # `#pending`   - sets the status to pending
  # `#solved`    - sets the status to solved
  #
  # `#low`       - sets the priority to low
  # `#normal`    - sets the priority to normal
  # `#high`      - sets the priority to high
  # `#urgent`    - sets the priority to urgent
  #
  #
  # `#problem`   - sets the ticket type to problem
  # `#question`  - sets the ticket type to question
  # `#incident`  - sets the ticket type to incident
  # `#task`      - sets the ticket type to task
  #
  # `#note`      - sets is_public to false
  # `#id`        - sets the ticket (handled in TicketFinderProcessor+TicketIdentification)
  #
  class TextApiProcessor < BaseProcessor
    def execute
      return unless agent_author_or_comment?

      if agent_spoofed?
        log('Discontinuing TextApiProcessor as agent spoof detected.')
        statsd_client.increment('reply_to_processor.text_api_processor_skipped')
        return
      end

      if account.has_email_sender_authentication? && !state.sender_auth_passed?
        log("Sender authentication failed - ignoring text API commands")
        return
      end

      content = state.agent_comment || state.plain_content

      if content.bytesize > 1.megabytes
        log("Content is too large to parse (over 1 megabyte), skipping text api")
        return
      end

      unless /#\w/.match?(content)
        log("No tags detected")
        return
      end

      if state.agent_comment
        @message = Zendesk::InboundMail::TextApi::Message.new(text: content, options: { parser: parser })

        state.properties    = @message.properties
        state.agent_comment = @message.stripped_text
        mutated_content = state.agent_comment
      else
        @message = Zendesk::InboundMail::TextApi::Message.new(text: content,
                                                              html: state.html_content,
                                                              options: { parser: parser })

        state.properties    = @message.properties
        state.plain_content = @message.stripped_text
        state.html_content  = @message.stripped_html if account.settings.rich_content_in_emails?
        mutated_content = state.plain_content
      end

      if mutated_content.blank?
        log("Email following text api commands is blank") if no_content_logging_enabled?

        if hard_reject_no_content_enabled? && mail.attachments.blank?
          reject!("Email following text api commands is blank and no attachments are present")
        else
          is_public_if_blank_value = account.settings.rich_content_in_emails? ? false : "false"
          @message.properties[:is_public] = is_public_if_blank_value if mail.attachments.blank?
        end
      end

      # Remove when removing :email_log_agent_sender_with_nonmatching_from_and_reply_to logging
      state.email_containing_text_api = true if account.has_email_log_agent_sender_with_nonmatching_from_and_reply_to?

      log("Email contains TextAPI commands and Sender Authentication status is #{state.sender_auth_status}")
      log("Sender auth header results: #{state.sender_auth_header_results.to_sentence}")

      log_tags_used
      log_unknown_tags
      log_invalid_values
      log_api_properties
      record_metrics
    end

    private

    def enabled?
      agent_author_or_comment? && !agent_spoofed?
    end

    def agent_author_or_comment?
      return true if state.agent_comment

      return true if state.author && state.author.is_agent?

      # ProcessingState#agent is a smell and should be refactored. See method for full comment.
      return true if state.suspended? && state.agent

      false
    end

    def agent_spoofed?
      account.has_email_reply_to_vulnerability_text_api_processor? && state.flags.include?(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF)
    end

    def parser
      Zendesk::InboundMail::TextApi::Parser.new
    end

    def hard_reject_no_content_enabled?
      account.has_email_hard_reject_text_api_with_no_content?
    end

    def no_content_logging_enabled?
      account.has_email_hard_reject_text_api_with_no_content? || account.has_email_log_text_api_with_no_content?
    end

    def log_tags_used
      return if @message.properties.blank?
      log("Using tags #{@message.properties.keys.to_sentence}")
    end

    def log_api_properties
      log("TextAPI properties: #{@message.properties}")
    end

    def log_unknown_tags
      @message.unknown_tags.each do |tag|
        log("Unknown tag '#{tag}'")
      end
    end

    def log_invalid_values
      @message.invalid_values.each do |tag, value|
        log("Invalid value #{value.inspect} for tag '#{tag}'")
      end
    end

    def record_metrics
      return if @message.known_tags.blank?

      @message.known_tags.each do |tag|
        command = tag.split.first.delete('#')
        statsd_client.increment("text_api_processor.processed", tags: ["command:#{command}"])
      end

      statsd_client.increment("text_api_processor.emails_containing_text_api")
    end
  end
end
