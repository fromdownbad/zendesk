require 'zendesk/utils/mime_types' # do not change this without testing eager load works with a test-deploy to master
require 'zendesk/inbound_mail/html_document'

module Zendesk::InboundMail::Processors
  # Uploads attachments for the mail and links them to the ticket on state
  # Precondition: state.ticket
  class AttachmentProcessor < BaseProcessor
    MAX_NOKOGIRI_TREE_DEPTH = -1 # Removes limit on tree depth

    def execute
      if attachments_allowed?
        build_attachments
      else
        log("Skipping. Author is not allowed to add attachments.")
        mail.delete_remote_attachments
      end

      remove_broken_inline_images_in_comment_html_body unless enabled?
      report_reply_to_processor_metrics
    end

    private

    def existing_ticket?
      state.ticket && !state.ticket.new_record?
    end

    def html_from_parts
      @html_from_parts ||= [mail].concat(mail.parts).detect { |part| part.mime_type == Mime[:html] && !part.attachment? }&.body&.to_s
    end

    # Returns an array of modified image src values
    def replace_image_src_in_body(body, content_id_map)
      body.css('img').map do |image|
        next unless content_id = image['src'].to_s[/^cid:(.*)/, 1]
        next unless attachment = content_id_map[content_id]
        attachment.inline = inline?(attachment)

        if account.has_email_enable_mtc_cid_resolution?
          image['src'] = attachment.url
        else
          image['src'] = attachment.url unless account.has_no_mail_delimiter_enabled?
        end
      end
    end

    def build_attachments
      container = if state.ticket.is_a?(SuspendedTicket)
        state.ticket
      else
        state.ticket.comment
      end

      log("Found #{mail.attachments.size} attachments")

      content_id_map = mail.attachments.each_with_object({}) do |attachment, id_map|
        content_type = attachment.content_type || guess_content_type(attachment)
        attachment_model = container.attachments.build(
          is_public: false,
          account: account,
          author: author,
          content_type: content_type,
          filename: attachment.file_name
        )

        if account.send_real_attachments? && account.settings.rich_content_in_emails?
          attachment_model.inline = if state.ticket.is_a?(Ticket) && state.ticket.comment.rich? && inline?(attachment_model)
            !!attachment.headers['inline'].try(:first)
          else
            false
          end
        end

        # record content ids for later use in cid resolution
        content_id = attachment.headers['content-id'].try(:first)
        id_map[content_id] = attachment_model

        if attachment.remote_url.present?
          log("Creating attachment model from remote file #{attachment.remote_url}")
          attachment_model.copy_from_cloud(attachment.remote_url)
        else
          log("Creating attachment model from local data")
          attachment_model.set_temp_data(attachment.body)
        end
      end

      ensure_valid_attachments(container)
      resolve_cid_images(content_id_map)
      ignore_quoted_attachments(container) if account.send_real_attachments?
    end

    def author
      # ZD#1101134 don't create user if suspended ticket
      # Similar logic to MailTicket#to_suspended_ticket
      state.author if !state.ticket.is_a?(SuspendedTicket) || state.author.try(:persisted?)
    end

    def raw_email
      @raw_email ||= Zendesk::Mailer::RawEmailAccess.new(
        "#{account.id}/#{mail.id}.eml",
        attachments: mail.try(:attachments).presence,
        tiff_prevent_inline: account.has_tiff_prevent_inline?
      )
    end

    def resolve_cid_images(content_id_map)
      return unless account.settings.rich_content_in_emails?

      if account.has_email_enable_mtc_cid_resolution? && !state.ticket.is_a?(Ticket)
        return unless html_from_parts

        body = Nokogiri::HTML5.fragment(html_from_parts, max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH)
        modified = replace_image_src_in_body(body, content_id_map)

        return if modified.none?

        raw_email.resolve_json_cid_images!(content_id_map, mail.logger)
        json = Yajl::Parser.parse(raw_email.json, check_utf8: false).with_indifferent_access

        if state.agent_forwarded
          modifications, body = raw_email.resolve_cid_images(state.ticket.content, content_id_map)
          unless modifications.flatten.compact.empty?
            log("Setting state.ticket.content with cid resolved html")
            state.ticket.content = body
          end
        else
          log("Setting state.ticket.content for #{existing_ticket? ? "existing" : "new"} ticket from processing_state")
          state.ticket.content = if existing_ticket?
            json.dig('processing_state', 'content', 'html_reply') || json.dig('processing_state', 'content', 'plain_reply') || NO_CONTENT.dup
          else
            json.dig('processing_state', 'content', 'html_full') || json.dig('processing_state', 'content', 'plain_full') || NO_CONTENT.dup
          end
        end

        return
      end

      return unless state.ticket.is_a?(Ticket)
      return unless comment = state.ticket.comment

      raw_body = if account.has_no_mail_delimiter_enabled?
        html_from_parts
      else
        comment.raw_html_body.presence
      end

      return unless raw_body

      body = Nokogiri::HTML5.fragment(raw_body, max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH)
      modified = replace_image_src_in_body(body, content_id_map)

      return if modified.none?

      comment.html_body = if account.has_email_enable_mtc_cid_resolution?
        raw_email.resolve_json_cid_images!(content_id_map, mail.logger)
        json = Yajl::Parser.parse(raw_email.json, check_utf8: false).with_indifferent_access

        html_reply = json.dig('processing_state', 'content', 'html_reply')
        html_full = json.dig('processing_state', 'content', 'html_full')

        state.html_content = if state.agent_forwarded
          modifications, body = raw_email.resolve_cid_images(state.html_content, content_id_map)
          if modifications.flatten.compact.empty?
            log("Not setting state.html_content with cid resolved html")
            state.html_content
          else
            log("Setting state.html_content with cid resolved html")
            body
          end
        elsif existing_ticket?
          if Zendesk::InboundMail::HTMLDocument.content_present?(html_reply)
            log("Setting html_content for existing ticket to html_reply")
            html_reply
          elsif Zendesk::InboundMail::HTMLDocument.content_present?(html_full)
            log("Setting html_content for existing ticket to html_full")
            html_full
          end
        else
          if Zendesk::InboundMail::HTMLDocument.content_present?(html_full)
            log("Setting html_content for new ticket to html_full")
            html_full
          end
        end

        mail_comment_attributes = Zendesk::InboundMail::Ticketing::MailComment.new(state, mail).attributes
        updated_comment = Comment.new({ticket: state.ticket, account: account}.merge(mail_comment_attributes))
        updated_comment.html_body
      else
        body.to_html
      end
    rescue SystemStackError
      nil
    end

    def guess_content_type(attachment)
      Zendesk::Utils::MimeTypes.determine_mime_type(attachment.file_name, attachment.content_type)
    end

    def attachments_allowed?
      enabled? && (account.is_attaching_enabled? || state.author.try(:is_agent?))
    end

    def enabled?
      !account.has_email_reply_to_vulnerability_attachment_processor? ||
          state.original_author&.is_light_agent? ||
          !state.flags.include?(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) || account.is_attaching_enabled?
    end

    def remove_broken_inline_images_in_comment_html_body
      return unless account.settings.rich_content_in_emails?
      return unless html_from_parts
      return unless state.ticket.is_a?(Ticket)
      return unless comment = state.ticket.comment

      body = Nokogiri::HTML5.fragment(html_from_parts, max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH)
      body.css('img').each do |image|
        content_id = image['src'].to_s[/^cid:(.*)/, 1]
        image.remove if content_id.present?
      end

      comment.html_body = body.to_html
    end

    def ensure_valid_attachments(model)
      bad_attachments = []
      model.attachments.each do |attachment|
        begin
          unless attachment.valid?
            log("Discarding attachment due to: #{attachment.errors.full_messages.join(', ')}")
            unless attachment.valid_size?
              filename = CGI.escapeHTML(attachment.filename)
              if model.is_a?(Comment)
                model.ticket.add_translatable_error_event(
                  key: "ticket.comment.flagged_reason_attachment_too_big",
                  file: filename, account_limit: account_attachment_size_limit
                )
              end
              state.add_flag(EventFlagType.ATTACHMENT_TOO_BIG, message: { file: filename, account_limit: account_attachment_size_limit })
            end
            bad_attachments << attachment
          end
        rescue StandardError => e
          log("Failed to validate attachment attachment: #{e.message}")
          ZendeskExceptions::Logger.record(e, location: self, message: "Failed to validate attachment: #{e.message}", fingerprint: '5aba13f573bc296338c2d347c42acb3828296422')
          bad_attachments << attachment
        end
      end
      model.attachments -= bad_attachments
    end

    def ignore_quoted_attachments(model)
      return unless state.ticket.is_a?(Ticket)
      return unless comment = state.ticket.comment
      return unless comment.rich?
      return unless html_body = comment.html_body.presence

      body = Nokogiri::HTML5.fragment(html_body, max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH)

      ignored_attachments = []

      dom_attachments = body.css('img').map { |image| image['src'] }

      model.attachments.each do |attachment|
        next unless attachment.inline
        next if dom_attachments.include?(attachment.url)

        ignored_attachments << attachment
      end

      model.attachments -= ignored_attachments
    rescue SystemStackError
      nil
    end

    def account_attachment_size_limit
      @account_attachment_size_limit ||= account.max_attachment_megabytes.to_s
    end

    def inline?(attachment_or_model)
      !account.has_tiff_prevent_inline? || attachment_or_model.content_type != Mime::TIFF
    end

    def report_reply_to_processor_metrics
      statsd_client.increment('reply_to_processor.attachment_processor_skipped') unless enabled?
    end
  end
end
