require_relative '../spam_helper'

# WARNING: As of https://github.com/zendesk/zendesk/pull/45929, RspamdProcessor,
# CloudmarkProcessor, and SpamHelper no longer maintain parity with the
# legacy SpamProcessor. Do NOT roll this processor out unless you have a
# very good reason to do so.
module Zendesk::InboundMail::Processors
  # Checks if the email is spam using results from Rspamd.
  # If the email is extremely spammy, hard rejects the email.
  # If the email is somewhat spammy, suspends the email.
  class RspamdProcessor < BaseProcessor
    include Zendesk::InboundMail::SpamHelper

    def execute
      return unless account.has_email_new_spam_processors?

      # Preserves the original SpamProcessor behavior of falling back on
      # Rspamd if Cloudmark score header is missing, even if Rspamd is not
      # enabled for the account
      return unless account.has_email_rspamd? || cloudmark_score_missing?

      if account.has_email_skip_spam_processor?
        warn("WARNING! Account '#{account.subdomain}' has the Spam Processor disabled (via email_skip_spam_processor Arturo)")
        statsd_client.increment('rspamd_processor.disabled')
        return
      end

      if from_mail_fetcher?
        log("Skipping SpamProcessor for emails fetched from Mail Fetcher (Gmail Connector)")
        statsd_client.increment('rspamd_processor.skip_mail_fetcher_email')
        return
      end

      if rspamd_reject?
        reject!(
          "Mail hard rejected due to spam score of #{rspamd_score} > " \
          "multiplier (#{multiplier}) * RSPAMD_REJECT_THRESHOLD (#{RSPAMD_REJECT_THRESHOLD})"
        )
      end

      if rspamd_suspend?
        suspend(
          "Suspended due to spam score of #{rspamd_score} > " \
          "multiplier (#{multiplier}) RSPAMD_SUSPEND_THRESHOLD (#{RSPAMD_SUSPEND_THRESHOLD})"
        )
      end

      log("Spam multiplier is: #{multiplier}")
    end

    private

    def cloudmark_score_missing?
      Zendesk::InboundMail::CloudmarkSpamScore.new(mail).value.nil?
    end
  end
end
