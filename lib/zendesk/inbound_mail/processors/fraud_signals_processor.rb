module Zendesk::InboundMail::Processors
  class FraudSignalsProcessor < BaseProcessor
    include Fraud::FraudSafelist

    HIGH_RSPAMD_SCORE_THRESHOLD = 13.0
    LOCALHOST_UNKNOWN = "from localhost (unknown".freeze
    RSPAMD_SCORE_PATTERN = /(Yes|No)\,\sscore\=(?<score>\d{1,2}\.\d{1,2})/.freeze
    RULES = [
      :suspend_if_spammy_received_header,
      :suspend_if_regruhosting_and_x_php_originating_script,
      :suspend_if_high_rspamd_and_from_localhost_unknown
    ].freeze

    def execute
      RULES.each do |rule|
        if state.suspended?
          if account.has_email_fraud_signals_processor_scan_signup_suspended_emails? && state.suspension == SuspensionType.SIGNUP_REQUIRED
            log("Executing_FraudSignalsProcessor rules on email already suspended due to SIGNUP_REQUIRED")
          else
            log("Skipping_FraudSignalsProcessor rules on email for suspended email")
            return
          end
        end
        execute_rule(rule)
      end
    end

    private

    def execute_rule(rule)
      send(rule)

      if account.has_email_train_rspamd_in_mtc? && state.suspension == SuspensionType.MALICIOUS_PATTERN_DETECTED
        train_rspamd(rule.to_s)
      end
    end

    def suspend_if_spammy_received_header
      return unless check_received_headers_for_spammy_pattern_matches_enabled?
      return if spammy_received_header_matches.empty?

      if account.has_email_fraud_signals_processor_received_headers?
        statsd_client.increment(
          :fraud_signals_processor_suspend,
          tags: spammy_received_header_pattern_datadog_tags
        )
        suspend("Spam detected via Received headers: #{spammy_received_header_matches}")
      else
        log("Received header fraud signals detected in log-only mode: #{spammy_received_header_matches}")
      end
    end

    def suspend_if_regruhosting_and_x_php_originating_script
      return unless account.has_email_fraud_signals_processor_regruhosting_php_mailer?

      if mail.headers["X-PHP-Originating-Script"].present? && regruhosting_in_received_headers?
        statsd_client.increment(
          :fraud_signals_processor_suspend,
          tags: ["reason:regruhosting_and_x_php_originating_script"]
        )
        suspend("Spam detected by regruhosting.ru in Received header and presence of X-PHP-Originating-Script header. Subject was: '#{mail.subject}'")
      end
    end

    def suspend_if_high_rspamd_and_from_localhost_unknown
      return unless check_for_high_rspamd_and_from_localhost_unknown?

      if high_rspamd_score? && sent_from_localhost_unknown?
        if account.has_email_fraud_signals_processor_high_rspamd_localhost_unknown?
          statsd_client.increment(
            :fraud_signals_processor_suspend,
            tags: ["reason:high_rspamd_locahost_unknown"]
          )
          suspend("Spam detected by combination of 'from localhost (unknown' in bottom two Received header(s) and high Rspamd score. Rspamd score: #{rspamd_score};  #{bottom_two_received_headers}")
        else
          log("High rspamd score and 'from localhost (unknown' detected in log-only mode: Rspamd score: #{rspamd_score}; Bottom two Received headers: #{bottom_two_received_headers}")
        end
      end
    end

    def check_received_headers_for_spammy_pattern_matches_enabled?
      account.has_email_fraud_signals_processor_received_headers? ||
        account.has_email_fraud_signals_processor_received_headers_log_only?
    end

    def spammy_received_header_matches
      @spammy_received_header_matches ||= begin
        spammy_matches = Set.new

        received_headers.each do |received_header|
          received_header.scan(spammy_received_header_pattern).each do |match|
            if safe_ip_address?(parse_out_ip_from_regex(match), mail_logger)
              next
            end
            spammy_matches.add(match)
          end
        end

        spammy_matches.to_a
      end
    rescue RegexpError => e
      log("FraudSignalsProcessor Received header regex error! #{e.class} #{e.message}")
      statsd_client.increment(
        :fraud_signals_processor_received_header_error,
        tags: ["class:#{e.class}"]
      )
      []
    rescue StandardError => e
      log("FraudSignalsProcessor Received header pattern matching errored for non-RegexpError reasons! #{e.class} #{e.message}")
      statsd_client.increment(
        :fraud_signals_processor_received_header_error,
        tags: ["class:#{e.class}"]
      )
      []
    end

    def received_headers
      @received_headers ||= mail.headers["Received"] || []
    end

    def spammy_received_header_pattern
      Regexp.union(spammy_received_header_patterns)
    end

    def spammy_received_header_patterns
      return [] unless arturo_feature = Arturo::Feature.find_feature(:orca_fraud_signals_processor_received_header_patterns)
      arturo_feature.external_beta_subdomains.map { |arturo_value| Regexp.new(arturo_value, Regexp::IGNORECASE) }
    end

    def spammy_received_header_pattern_datadog_tags
      pattern_tags = spammy_received_header_matches.map do |pattern|
        "spammy_pattern:#{pattern}"
      end.uniq

      ["reason:spammy_received_header_pattern"] + pattern_tags
    end

    def regruhosting_in_received_headers?
      received_headers.any? do |received_header|
        received_header.include?("regruhosting.ru")
      end
    end

    def check_for_high_rspamd_and_from_localhost_unknown?
      account.has_email_fraud_signals_processor_high_rspamd_localhost_unknown? ||
        account.has_email_fraud_signals_processor_high_rspamd_localhost_unknown_log_only?
    end

    def high_rspamd_score?
      return false unless rspamd_score

      rspamd_score >= HIGH_RSPAMD_SCORE_THRESHOLD
    end

    def rspamd_score
      return @rspamd_score if defined?(@rspamd_score)

      @rspamd_score = begin
        if rspamd_header = mail.headers["X-Rspamd-Status"].try(:first)
          rspamd_header.match(RSPAMD_SCORE_PATTERN).try(:[], :score).try(:to_f)
        end
      end
    end

    def sent_from_localhost_unknown?
      bottom_two_received_headers.any? do |received_header|
        received_header.include?(LOCALHOST_UNKNOWN)
      end
    end

    def bottom_two_received_headers
      received_headers.last(2)
    end

    def parse_out_ip_from_regex(regex_match)
      # IP regex patterns tend to contain [] or (), e.g. '\[10\.210\.80\.219\]'
      regex_match.tr('[]()', '')
    end

    def suspend(message, reason = SuspensionType.MALICIOUS_PATTERN_DETECTED)
      add_suspension_tracer_tag
      super
    end

    def add_suspension_tracer_tag
      return unless account.has_orca_classic_fraud_signals_span?
      current_root_span = Datadog.tracer.active_root_span
      current_root_span.set_tag('zendesk.mail_ticket_creator.fraud_signals_processor.suspend', account.subdomain) unless current_root_span.nil?
    end

    def train_rspamd(rule)
      if raw_email_identifier_for_rspamd_training
        RspamdFeedbackJob.report_spam(
          user: User.system,
          identifier: raw_email_identifier_for_rspamd_training,
          recipient: state.recipient,
          classifier: Zendesk::InboundMail::RspamdTrain::RSPAMD_BAYES_CLASSIFIER_COMMON
        )

        log("Training Rspamd based on suspension in FraudSignalsProcessor. Sender: #{state.from.address}")
        statsd_client.increment("mtc_train_rspamd", tags: ["processor:fraud_signals_processor", "rule:#{rule}"])
      end
    end

    def raw_email_identifier_for_rspamd_training
      @raw_email_identifier_for_rspamd_training ||= begin
        if mail.eml_url.present? && remote_file = Zendesk::Mail.raw_remote_files.file_from_url(mail.eml_url)
          remote_file.identifier
        end
      end
    end
  end
end
