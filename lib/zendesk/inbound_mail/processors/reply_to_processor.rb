module Zendesk::InboundMail::Processors
  # The processor has various checks in place to identify agent spoofing,
  # i.e. when the reply-to and from email addresses don't match and the
  # reply-to is of an agent.
  # state.from_header: is the preserved from header
  # state.from       : is the computed sender details,
  #                    viz either the sender or the reply-to address
  #                    if that has been provided
  class ReplyToProcessor < BaseProcessor
    def execute
      return unless processor_enabled?

      state.original_author = original_sender

      if proceed_with_end_user_spoof_identification?
        state.add_flag(EventFlagType.REPLY_TO_END_USER_POSSIBLE_SPOOF)
        return
      end

      if proceed_with_agent_spoof_identification?
        warn("Email contains a non matching from(#{original_address}) and " \
             "reply-to(#{resolved_address}) addresses, and the reply-to has an agent role.")

        state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) unless authorized_original_sender?

        report_metrics
        report_matching_from_and_reply_to_after_plus_id_removal
      end
    end

    private

    def processor_enabled?
      if account.has_email_reply_to_vulnerability_check?
        log('Account has email_reply_to_vulnerability_check Arturo enabled')
        true
      else
        log('Skipping ReplyToProcessor')
        false
      end
    end

    def proceed_with_agent_spoof_identification?
      different_originator_fields? && state.agent && !(from_whitelisted? && from_header_whitelisted?)
    end

    def proceed_with_end_user_spoof_identification?
      return false unless account.has_email_reply_to_vulnerability_end_user_impersonation?

      different_originator_fields? && computed_sender_end_user? && !allowed_original_sender?
    end

    def allowed_original_sender?
      original_sender.present? && original_sender_agent? || original_sender_is_support?
    end

    def report_matching_from_and_reply_to_after_plus_id_removal
      if original_sanitized_address == resolved_address
        log("Note, email #{original_address} and #{resolved_address} are same after removing plus ids")
        statsd_client.increment('reply_to_processor.reply_to_and_from_match_after_plus_id_removal')
      end
    end

    def original_address
      state.from_header&.address
    end

    def original_sanitized_address
      @original_sanitized_address ||= remove_plus_id(original_address)
    end

    def resolved_address
      state.from.address
    end

    def different_originator_fields?
      !original_address.nil? && original_address != resolved_address
    end

    def authorized_original_sender?
      original_sender.present? && original_sender_agent? && !original_sender_light_agent? ||
          original_sender_is_support?
    end

    def original_sender_agent?
      original_sender.is_agent?
    end

    def original_sender_light_agent?
      original_sender.is_light_agent?
    end

    def original_sender_is_support?
      from_account_address?(original_address)
    end

    def original_sender
      return @original_sender if @original_sender
      @original_sender = find_user_by_email(original_address)
    end

    def computed_sender
      return @computed_sender if @computed_sender
      @computed_sender = find_user_by_email(resolved_address)
    end

    def computed_sender_end_user?
      computed_sender.present? && computed_sender.is_end_user?
    end

    def from_whitelisted?
      state.whitelisted?
    end

    def from_header_whitelisted?
      state.from_header_whitelisted?
    end

    def report_metrics
      flagged = state.flags.include?(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF)

      log(
        "Outcome: email #{flagged ? 'is' : 'is not'} flagged, " \
        "new ticket: #{state.ticket.blank? && state.closed_ticket.blank?}, " \
        "from: #{original_address}, " \
        "from_whitelisted: #{from_header_whitelisted?}, " \
        "reply_to: #{resolved_address}, " \
        "whitelisted: #{from_whitelisted?}, " \
        "from_user_role: #{original_sender.present? ? original_sender.role_name : 'unknown'}, " \
        "from_an_light_agent: #{original_sender.present? ? original_sender_light_agent? : false}, " \
        "from_an_support_address: #{original_sender_is_support?}" \
      )

      statsd_client.increment('reply_to_processor.reply_to_and_from_header_variance', tags: ["flagged:#{flagged}"])
    end

    def remove_plus_id(address)
      return unless address

      return address unless address.include?('+')

      "#{address.split('+')[0]}@#{address.split('@')[1]}"
    end

    def find_user_by_email(address)
      account.find_user_by_email(address)
    end
  end
end
