require_relative "base_processor"
require_relative "../loggers/agent_access"
require_relative "../loggers/user_abilities"

module Zendesk::InboundMail::Processors
  # Creates or updates tickets and suspended tickets
  class CommitProcessor < BaseProcessor
    delegate :ticket,
      :author,
      :suspended?,
      :suspended_ticket?,
      :followup_ticket?,
      :comment?,
      :submitter,
      :flagged?,
      :suspension,
      :properties,
      :mark_new_ticket,
      to: :state

    def execute
      # Ensures that Ticket validations logs are routed to MTC logger.
      ticket.application_logger = mail_logger

      log("Ticket: #{ticket.inspect}")
      log("Author: #{author.inspect}")

      if suspended_ticket?
        create_suspended_ticket!
      else
        create_or_update_ticket!
      end

      expire_view_caches
    end

    private

    def update_ticket_tags?(applied_properties)
      account.has_tickets_inherit_requester_tags? &&
        applied_properties.key?(:requester) &&
        ticket.changed.include?("current_tags")
    end

    def create_suspended_ticket!
      ticket.add_flags(state.flags, state.flags_options) if flagged?
      ticket.save!

      log("Suspended ticket ##{ticket.id} created (score: #{SuspensionType[ticket.cause].score})#{recipient_nice_id_message}")
    end

    def recipient_nice_id_message
      state.via_recipient_nice_id ? " (via recipient nice_id)" : ""
    end

    def create_or_update_ticket!
      ticket.current_user = author

      if flagged?
        log("Flagging audit, flags: #{state.flags.to_a}")
        ticket.add_flags!(state.flags, state.flags_options)
      end

      unless ticket.valid?
        if account.has_email_check_invalid_group?
          mail.logger.error("Ticket is invalid: #{commit_error_details}")
        end

        reasons = []
        if author.new_record? && !author.valid?
          mail.logger.error("Invalid author: #{author.errors.inspect}")
          reasons << "author"
        end

        if ticket.errors.messages[:organization_id]
          if ticket.requester && ticket.requester.organization_memberships
            mail.logger.error("requester orgs: #{ticket.requester.organization_memberships.inspect}")
            reasons << "requester_orgs"
          else
            mail.logger.error("bad/missing requester or memberships?: #{ticket.requester.inspect}, #{ticket.requester.try(:organization_memberships).inspect}")
            reasons << "requester_or_memberships"
          end
        end

        if account.has_email_check_invalid_group? && !ticket.group.try(:valid?)
          mail.logger.error("Invalid group ##{ticket.group_id}: #{ticket.group.inspect}")
          reasons << "group"
        end

        # The reasons Array should never be modified again after this final step
        reasons << "unknown" if reasons.empty?

        if ticket.requester.try(:invalid?)
          Zendesk::InboundMail::Loggers::AgentAccess.new(mail_logger, ticket).log_edit_access
          mail.logger.error("Requester is invalid: #{ticket.requester.errors.inspect}")
        end

        mail.logger.error("Ticket is invalid: #{ticket.errors.inspect}; #{ticket.audit.inspect}; #{ticket.audit.events.inspect}")
        statsd_client.increment("ticket.invalid", tags: reasons.map { |reason| "detail:#{reason}" })

        raise ActiveRecord::RecordInvalid, ticket
      end

      ticket.set_nice_id

      Ticket.transaction do
        # Persist any author user updates first to avoid race conditions
        adapter = Zendesk::InboundMail::Adapters::AuthorAdapter.new(processing_state: state,
                                                                    account: account,
                                                                    mail_logger: mail_logger,
                                                                    statsd_client: statsd_client,
                                                                    save_attrs: [:locale_id])
        adapter.save_with_instrumentation!

        if applied_properties = apply_api_properties_to_ticket
          log("Staged TextAPI properties and values: #{applied_properties.inspect}")
        end

        if ticket.new_record? && !state.closed_ticket
          mark_new_ticket
          set_requester_as_author
        end

        if applied_properties && update_ticket_tags?(applied_properties)
          ticket.restore_attributes([:current_tags])
          log("Re-applying TextAPI properties to update ticket tags")
          applied_properties = apply_api_properties_to_ticket
        end

        commit_ticket!

        if applied_properties
          properties_not_applied = applied_properties.keys.each_with_object([]) do |key, properties|
            if key.to_s == "set_tags"
              applied_tags = applied_properties[key]&.split&.sort&.uniq
              ticket_tags = ticket.send(key)&.split&.sort&.uniq || []
              properties << key if applied_tags != ticket_tags
            else
              properties << key if applied_properties[key] != ticket.send(key)
            end
          end

          log("TextAPI properties not applied: #{properties_not_applied.inspect}") if properties_not_applied.present?
        end

        update_attachment_authorship
        add_agent_comment
      end

      log("Ticket ##{ticket.id} (nice_id: '#{ticket.nice_id}', encoded_id: '#{ticket.encoded_id}') committed#{recipient_nice_id_message}")
    rescue StandardError => e
      # Clean up newly saved Users
      log("Handling error in create_or_update_ticket!, removing saved new users: #{state.saved_new_users}")
      clean_new_users!

      raise e
    ensure
      # Clean up ticket logger
      ticket.application_logger = nil
    end

    def apply_api_properties_to_ticket
      user = submitter || author

      if properties.present? && user.is_agent? && !user.is_light_agent?
        log("About to apply TextAPI properties to ticket: #{properties.inspect}")

        applied_properties, applied = ticket.apply_properties(properties)

        # All mail API commands were applied
        if applied
          log("Tentatively applied TextAPI properties to ticket: #{properties.inspect}")
          return applied_properties
        # A subset of API commands were applied. For example, #tags succeeded, but #assignee failed
        elsif applied_properties.present?
          log("Tentatively applied TextAPI properties to ticket: #{applied_properties.inspect}")
          return applied_properties
        # All mail API commands were not applied
        else
          log("Could not apply TextAPI properties to ticket: #{properties.inspect}")
        end
      end

      false
    end

    def set_requester_as_author
      if account.has_email_takeover_vulnerability_stats?
        log("Setting the requester ##{ticket.requester.try(:id)} '#{ticket.requester.try(:email)}' as the comment author")
      else
        log("Setting the requester as the author")
      end

      ticket.comment.author = ticket.requester
    end

    def commit_ticket!
      log("Committing ticket with title: '#{ticket.title}'")
      ticket.audit.suspension_type_id = suspension

      if mail.redacted?
        log("Building redaction event and tag for ticket ##{ticket.id}")
        ticket.comment.build_redaction_event
        ticket.add_redaction_tag!
      end

      ticket.save!
      log("Ticket ##{ticket.id} saved successfully!")
    rescue MiniMagick::Error => e
      warn("Ignored attachment thumbnailing failure, due to #{e.class}: #{e.message}")
    rescue StandardError => e
      statsd_client.increment("ticket.error", tags: ["detail:#{e.class}", "source:commit_ticket"])

      warn("Failed to commit ticket ##{ticket.id} due to #{e.class}: #{e.message} - #{commit_error_details}")
      raise e
    end

    def commit_error_details
      message = []
      message << "Message-Id: #{mail.message_id}"
      message << "Inspect: #{ticket.inspect}"
      message << "Group: #{ticket.send(:group_invalid?)} - #{ticket.group_id} - #{ticket.group.inspect}"
      message << "Triggers fired: #{ticket.events.select { |event| event.via_id == ViaType.RULE }.map(&:via_reference_id).join(", ")}"
      message.join("\n")
    end

    def update_attachment_authorship
      if (comment = ticket.comments.last) && comment.attachments.any?
        ids = comment.attachments.map(&:id)
        Attachment.where("id IN (?) OR parent_id IN (?)", ids, ids).update_all(author_id: comment.author_id)

        log("Attachment references updated (#{ids})")
      end
    rescue StandardError => e
      ZendeskExceptions::Logger.record(e, location: self, message: "Attachment update failed: #{e.message}", fingerprint: 'be7c5004663ec0317015641de278f892ffc12da0')
    end

    def add_agent_comment
      return unless state.agent_comment

      unless state.ticket.can_be_updated?
        warn("Closed ticket prevents adding agent comment.")
        return
      end

      if state.submitter
        unless state.submitter.can?(:add_comment, state.ticket)
          Zendesk::InboundMail::Loggers::UserAbilities.new(mail_logger, ticket, state.submitter).log_user_can_add_comment
          warn("Not adding agent comment for agent forwarded ticket; ticket submitter doesn't have permission to add " \
                 "agent comments to this ticket: #{state.submitter.inspect}")
          return
        end
        state.ticket = Zendesk::InboundMail::Ticketing::MailTicket.create_agent_comment_on_ticket(
          account, ticket, state.agent_comment, state.submitter, properties.try(:[], :is_public), mail_logger
        )
      else
        warn("Not adding agent comment for agent forwarded ticket; state.submitter is nil; current state.author: " \
               "#{state.author.inspect}, state.from: #{state.from.try(:address)}, original sender: #{mail.sender}")
      end
    end

    def clean_new_users!
      state.saved_new_users.each do |user|
        next unless user

        # default to self as is done in Users::Roles #ensure_current_user_for_non_agents
        user.current_user = user
        user.delete!
      end
    end

    def expire_view_caches
      log("Updating version #{account.scoped_cache_key(:views)}")
      account.expire_scoped_cache_key(:views)
      log("Updated version #{account.scoped_cache_key(:views)}")
    end
  end
end
