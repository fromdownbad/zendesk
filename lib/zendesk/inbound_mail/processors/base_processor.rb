module Zendesk::InboundMail
  module Processors
    class BaseProcessor
      def self.execute(state, mail)
        new(state, mail).execute
      end

      attr_reader :state, :mail
      delegate :log, :warn, :error, to: :mail_logger

      def initialize(state, mail)
        @state = state
        @mail  = mail
      end

      def execute
        raise NotImplementedError
      end

      private

      def account
        @account ||= state.account
      end

      def suspend(message, reason)
        warn(message, "suspending")
        state.suspension = reason
      end

      def reject!(message)
        warn(message, "rejecting")
        raise Zendesk::InboundMail::HardMailRejectException, message
      end

      def mail_logger
        @mail_logger ||= Zendesk::InboundMail::MailLogger.new(mail.logger, location: self.class)
      end

      def statsd_client
        Zendesk::InboundMail::StatsD.client
      end

      def debugging_mode?
        @debugging_mode ||= account.try(:has_email_debugging_mode?)
      end

      def debug_log(message)
        log("[DEBUG] #{message}") if debugging_mode?
      end

      def in_zendesk_subdomain?(address)
        !!(/^.+@#{Regexp.escape(account.subdomain)}\.#{::Zendesk::InboundMail::Patterns::ZENDESK_CONFIG_HOST}/ =~ address)
      end

      def wildcard_support_address?(address)
        account.settings.accept_wildcard_emails && in_zendesk_subdomain?(address)
      end

      def from_account_address?(address)
        account.recipient_email?(address) ||
        wildcard_support_address?(address) ||
        account.default_hosts.include?(email_domain(address))
      end

      def email_domain(address)
        address.to_s.split('@', 2).last
      end
    end
  end
end
