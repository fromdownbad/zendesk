module Zendesk::InboundMail::Processors
  # ForwardingVerificationProcessor detects and handles Zendesk's forwarding verification email.
  #
  # When someone adds a support address that is a custom address (basically anything that is
  # not *@subdomain.zendesk.com), we send an email to verify that they control that email address
  # and that forwarding is setup properly. In this email we send, we include a forwarding
  # verification code.
  #
  # If we receive an email containing a forwarding verification code and mail.sent_at matches
  # the time we sent the forwarding verification email, we mark the email address as verified
  # and reject the incoming email (we don't want a ticket generated)
  class ForwardingVerificationProcessor < BaseProcessor
    def execute
      readable_parts(mail).each do |body|
        if result = RecipientAddresses::ForwardingStatus.verify_forwarding_code(account, body)
          reject!("verification test email: #{result}")
        end
      end
    end

    private

    def readable_parts(mail)
      [mail].concat(mail.parts).map(&:body).map(&:to_s).select(&:valid_encoding?) # binary parts are not encoded in utf-8
    end
  end
end
