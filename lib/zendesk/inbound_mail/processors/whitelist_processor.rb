module Zendesk::InboundMail::Processors
  # Has the ability to explicitly whitelist an email. Most likely by inspecting the sender
  # address.
  class WhitelistProcessor < BaseProcessor
    def execute
      return if state.suspended?

      if account.has_email_sender_authentication? && state.sender_auth_failed?
        log("WhitelistProcessor skipped due to failed sender auth")
        return
      end

      if address_whitelisted? || mail_subject_whitelisted?
        state.whitelisted = true
      end

      if !!perform_whitelist_check_on_from_header?
        state.from_header_whitelisted = from_header_whitelisted? || mail_from_zopim?
      end

      log("Whitelist alignment info: #{[address_whitelisted?].concat(state.sender_auth_header_results).join(',')}")
    end

    private

    def address_whitelisted?
      @address_whitelisted ||= sender_whitelisted? || mail_from_zopim?
    end

    def from_header_whitelisted?
      address = state.from_header&.address
      log("WhitelistProcessor: Using From Header address '#{address}' for whitelisting...")

      if account.whitelists?(address)
        log("WhitelistProcessor: From Header Address '#{address}' is whitelisted by Account#whitelists?")
        return true
      end

      false
    end

    def perform_whitelist_check_on_from_header?
      return false unless account.has_email_mtc_add_from_header_state_variable?

      (state.from_header&.address != state.from&.address) && state.agent
    end

    def sender_whitelisted?
      if state.agent
        log("WhitelistProcessor: Address '#{state.from.address}' is whitelisted as agent.")
        return true
      end

      addresses = [state.from.address, mail.reply_to&.address, mail.from&.address].compact.uniq
      log("WhitelistProcessor: Using '#{addresses}' addresses for whitelisting...")

      if !!whitelisted_address = addresses.find { |address| account.whitelists?(address) }
        log("WhitelistProcessor: Address '#{whitelisted_address}' is whitelisted by Account#whitelists?")
        return true
      end

      false
    end

    def mail_subject_whitelisted?
      account.domain_whitelist.to_s.tokenize.select { |r| r.index(':') }.each do |whitelist|
        next unless whitelist =~ /^subject:("[^"]*"|\S+)/i
        if mail.subject.to_s.downcase.index($1.delete('"').downcase)
          log("#{state.from.address} whitelisted due to '#{whitelist}'")
          return true
        end
      end
      false
    end

    def mail_from_zopim?
      if mail.from_zopim?
        log("#{mail.from.address} whitelisted as Zopim")
        true
      else
        false
      end
    end
  end
end
