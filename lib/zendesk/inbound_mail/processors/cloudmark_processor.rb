require_relative '../spam_helper'

# WARNING: As of https://github.com/zendesk/zendesk/pull/45929, RspamdProcessor,
# CloudmarkProcessor, and SpamHelper no longer maintain parity with the
# legacy SpamProcessor. Do NOT roll this processor out unless you have a
# very good reason to do so.
module Zendesk::InboundMail::Processors
  # Checks if the email is spam using results from Cloudmark.
  # If the email is extremely spammy, suspends the email.
  # If Rspamd deems the email extremely spammy, hard rejects
  # the email, regardless of Cloudmark feedback.
  class CloudmarkProcessor < BaseProcessor
    include Zendesk::InboundMail::SpamHelper

    def execute
      return unless account.has_email_new_spam_processors?

      if account.has_email_skip_spam_processor?
        warn("WARNING! Account '#{account.subdomain}' has the CloudmarkProcessor disabled (via email_skip_spam_processor Arturo)")
        statsd_client.increment('cloudmark_processor.disabled')
        return
      end

      if from_mail_fetcher?
        log("Skipping SpamProcessor for emails fetched from Mail Fetcher (Gmail Connector)")
        statsd_client.increment('cloudmark_processor.skip_mail_fetcher_email')
        return
      end

      if filter_with_cloudmark
        if rspamd_reject?
          statsd_client.increment(:cloudmark_overruled, tags: ["spam_suspension:#{state.suspension == SuspensionType.SPAM}"])

          if state.suspension != SuspensionType.SPAM
            log("Cloudmark overruled due to Rspamd score of #{rspamd_score} > multiplier (#{multiplier}) * RSPAMD_REJECT_THRESHOLD (#{RSPAMD_REJECT_THRESHOLD})")
          end

          suspend("Cloudmark overruled: suspending due to high Rspamd score of #{rspamd_score}")
        end
      end
    end

    private

    def filter_with_cloudmark
      enabled = true

      if account.has_email_rspamd?
        warn("Skipping Cloudmark because '#{account.subdomain}' has Rspamd enabled (via email_rspamd Arturo)")
        statsd_client.increment('spam_processor.rspamd.enabled')
        enabled = false
      end

      cloudmark_score = Zendesk::InboundMail::CloudmarkSpamScore.new(mail)
      if cloudmark_score.value.nil?
        log("Cloudmark: missing score, skipping")
        statsd_client.increment(:cloudmark, tags: ["result:missing_score", "enabled:#{enabled}"])
        return false
      end

      if cloudmark_score.obviously_spam?
        if multiplier > 1
          log("Cloudmark: Skipping suspension for spam score: #{cloudmark_score.value}, threshold: #{cloudmark_score.threshold}, multiplier: #{multiplier}")
          statsd_client.increment(:cloudmark, tags: ["result:skipped_by_multiplier", "multiplier:#{multiplier}"])
        else
          statsd_client.increment(:cloudmark, tags: ["result:obviously_spam", "enabled:#{enabled}", "value:#{cloudmark_score.value}", "threshold:#{cloudmark_score.threshold}"])

          message = "Cloudmark: suspended due to spam score: #{cloudmark_score.value}"
          if enabled
            log("Cloudmark: Suspending for spam score: #{cloudmark_score.value}, threshold: #{cloudmark_score.threshold}, multiplier: #{multiplier}")
            suspend(message)
          end
        end
      elsif cloudmark_score.probably_spam?
        log("Cloudmark: probably spam, but allowing. Spam score: (#{cloudmark_score.value}), Multiplier: (#{multiplier})")
        statsd_client.increment(:cloudmark, tags: ["result:probably_spam", "enabled:#{enabled}", "value:#{cloudmark_score.value}"])
      end

      if !enabled && message
        log("#{message} DRY_RUN")
      end

      enabled
    end
  end
end
