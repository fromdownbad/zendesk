module Zendesk::InboundMail::Processors
  # Records post-commit/end-of-processor chain stats
  class StatsProcessor < BaseProcessor
    IN_HOST_PATTERN = /from\sin\d+\.pod\d+\.[a-z]+\d+\.(?:zdsys|zdsystest)\.com/.freeze
    MF_HOST_PATTERN = /by\swork\d+\.pod\d+\.[a-z]+\d+\.(?:zdsys|zdsystest)\.com\s\(Gmail Connector\)/.freeze
    RECEIVED_PATTERN = /(#{IN_HOST_PATTERN}|#{MF_HOST_PATTERN}).*;\s+(.*)$/m.freeze
    X_ZENDESK_SOURCE = 'X-Zendesk-Source'.freeze

    delegate :ticket,
      :suspended?,
      :suspended_ticket?,
      :followup_ticket?,
      :comment?,
      to: :state

    def execute
      record_agent_sender_with_nonmatching_from_and_reply_to_stats
      record_deferred_stats
      record_post_commit_stats
      record_total_mail_processing_time
    rescue StandardError => error
      warn("Unexpected error while recording end of processing stats; #{error.class} #{error.message}: #{error.backtrace}")
    end

    private

    def record_deferred_stats
      state.deferred_stats.each { |stat, tags| statsd_client.increment(stat, tags) }
    end

    def record_post_commit_stats
      if suspended_ticket?
        statsd_client.increment("ticket.commit", tags: ["type:suspended"])
        statsd_client.increment("ticket.suspended", tags: ["type:#{SuspensionType[ticket.cause]}"])
      elsif followup_ticket?
        statsd_client.increment("ticket.commit", tags: %w[type:followup type:comment])
      elsif comment?
        statsd_client.increment("ticket.commit", tags: ["type:comment"])
      else
        statsd_client.increment("ticket.commit", tags: ["type:new"])
      end
    end

    def detect_received_at
      mail.headers["received"].to_a.map do |received_header|
        received_header[RECEIVED_PATTERN, 2]&.to_datetime
      end.compact.min
    end

    def record_total_mail_processing_time
      tags = suspended_ticket? ? ['type:suspension'] : ['type:ticket_or_comment']

      if mail_received_at = detect_received_at
        processing_time = Time.now - mail_received_at
        tags += mail.headers.key?(X_ZENDESK_SOURCE) ? ['source:gmail'] : ['source:postfix']
        statsd_client.histogram(:total_inbound_time, processing_time, tags: tags)
        statsd_client.sli_histogram_decremental(:total_inbound_email_processed, duration: (processing_time * 1000).round, tags: tags) if record_total_inbound_latency_for_event_based_slo?
      else
        statsd_client.increment("total_inbound_time.failed", tags: tags)
        warn("Could not determine when mail was received #{mail.headers['received'].inspect}")
      end
    end

    # Remove when removing :email_log_agent_sender_with_nonmatching_from_and_reply_to logging
    def record_agent_sender_with_nonmatching_from_and_reply_to_stats
      return unless account.has_email_log_agent_sender_with_nonmatching_from_and_reply_to?
      return unless state.author&.is_agent? || state.submitter&.is_agent?

      from_address = remove_plus_id(mail.from&.address)
      reply_to_address = remove_plus_id(mail.headers["reply-to"]&.first&.address)

      if from_address && reply_to_address && from_address != reply_to_address
        from_user_role_name = from_address_role(from_address)

        log(
          "Author detected as agent, and From and Reply-To headers have different address values. " \
          "New Ticket: #{state.new_ticket?}, " \
          "From Header Address: #{from_address}, " \
          "From Address User role: #{from_user_role_name}, " \
          "Reply-To Header Address: #{reply_to_address}, " \
          "Suspended: #{state.suspended?}, " \
          "SuspensionType: #{state.suspended? ? SuspensionType.to_s(state.suspension) : 'N/A'}, " \
          "Anonymous users can submit support requests: #{account.is_open?}, "\
          "List of agent privilege actions performed in the email - " \
          "Add/Modify CCs: #{ccs_modified?} when only agents can add CCs setting is #{account.is_collaborators_addable_only_by_agents? ? 'ON' : 'OFF'}, " \
          "Add Attachments: #{!mail.attachments.blank?} when end users are not allowed to attach files setting is #{account.is_attaching_enabled? ? 'ON' : 'OFF'}, " \
          "Mail API: #{state.email_containing_text_api}, " \
          "Email Forwarding: #{state.agent_forwarded}"
        )

        tags = ["subdomain:#{account.subdomain}", "new_ticket:#{state.new_ticket?}", "from_user_role:#{from_user_role_name}"]

        statsd_client.increment('user_processor.agent_sender_with_nonmatching_from_and_reply_to', tags: tags)
      end
    end

    # Remove when removing :email_log_agent_sender_with_nonmatching_from_and_reply_to logging
    def from_address_role(address)
      from_user = account.find_user_by_email(address)
      role_name = if !from_user.nil?
        from_user.role_name
      elsif from_account_address?(address)
        'Support address'
      else
        'N/A'
      end
      role_name
    end

    # Remove when removing :email_log_agent_sender_with_nonmatching_from_and_reply_to logging
    def ccs_modified?
      email_cc_audit = if account.has_follower_and_email_cc_collaborations_enabled?
        ticket.audits&.last&.events&.select { |e| e.type == EmailCcChange.name }
      else
        ticket.audits&.last&.events&.select { |e| e.value_reference == 'current_collaborators' }
      end

      return false if email_cc_audit.nil?
      !email_cc_audit.empty?
    end

    # Remove when removing :email_log_agent_sender_with_nonmatching_from_and_reply_to logging
    def remove_plus_id(address)
      return unless address

      Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(address)
    end

    def record_total_inbound_latency_for_event_based_slo?
      account.has_email_inbound_pipeline_record_event_based_slo?
    end
  end
end
