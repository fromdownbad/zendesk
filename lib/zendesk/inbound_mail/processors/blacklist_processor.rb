module Zendesk::InboundMail::Processors
  # Has the ability to explicitly blacklist an email by inspecting the sender
  # address and matching this against account settings
  class BlacklistProcessor < BaseProcessor
    def execute
      unless account.is_open?
        log "Abort: The account isn't open."
        return
      end

      [:from, :reply_to].each do |header|
        filter(header)
      end
    end

    private

    def filter(header)
      address = mail.send(header)

      if address.nil? || address.address.nil?
        log "Skip: Can't find any address in the '#{header}' header"
        return
      end

      log "The mail #{state.whitelisted? ? 'was' : 'was NOT'} marked as safe (allowlisted)"

      if !state.whitelisted? && account.blacklists?(address.address)
        log "The '#{address.address}' address is included in the account's domain blocklist, suspending"
        suspend("#{header}: #{address} blacklisted")
        return
      else
        log "The '#{address.address}' address is NOT included in the account's domain blocklist"
      end

      if domain_blacklist.suspend?(address)
        suspend("#{header}: #{address} blacklisted due to explicit block in '#{account.domain_blacklist}'")
      elsif domain_blacklist.reject?(address)
        reject!("#{header}: #{address} rejected due to 'reject:#{address}' in account domain blacklist")
      end
    end

    def suspend(message, reason = SuspensionType.BLOCKLISTED)
      super
    end

    def domain_blacklist
      @domain_blacklist ||= DomainBlacklist.new(account.domain_blacklist.to_s)
    end

    class DomainBlacklist
      def initialize(domain_blacklist)
        @domain_blacklist = domain_blacklist
      end

      def suspend?(address)
        actions = actions_for(address)
        actions.include?(:suspend) || actions.include?(:block)
      end

      def reject?(address)
        actions_for(address).include?(:reject)
      end

      protected

      def actions_for(address)
        return [] unless address && address.address && address.domain

        matching_entries = entries.select do |entry|
          entry[:pattern].casecmp(address.address) == 0 || entry[:pattern].casecmp(address.domain) == 0
        end

        matching_entries.map { |entry| entry[:action] }.uniq
      end

      def entries
        return @entries if @entries

        @entries = @domain_blacklist.tokenize.map do |entry|
          if match = entry.match(/(block|suspend|reject):([^\s]+)/i)
            { action: match[1].to_sym, pattern: match[2] }
          end
        end
        @entries.compact!
        @entries
      end
    end
  end
end
