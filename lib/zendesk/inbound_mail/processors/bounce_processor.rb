module Zendesk::InboundMail::Processors
  class BounceProcessor < BaseProcessor
    MAX_LOGGED_MESSAGE_LENGTH = 5000

    def execute
      if detected_as_bounce? || delivery_status_msg_attached?
        log "Email detected as a bounce via #{bounce_via}"
        type = "soft (unrecognized)"

        if account.has_email_log_welcome_email_bounce? && response_to_zendesk_welcome_mail?
          statsd_client.increment('welcome_email_bounce')
        end

        if undeliverable_recipient && status_code_match_data
          type = bounce_type

          if account.has_email_switch_bounce_processing_logging_mode?
            @target_action = "skipped"
            warn "Update identity and targets deliverability is disabled!"
          elsif !reject_only_bounce?
            update_identity_deliverability!(undeliverable_recipient)

            if account.has_email_update_all_targets_on_bounce?
              log "Arturo enabled: 'email_update_all_targets_on_bounce'"
              update_email_targets_deliverability!(undeliverable_recipient)
            else
              update_target_deliverability!(undeliverable_recipient)
            end
          end

          log_bounce_stats
        else
          log "No Undeliverable Recipient detected!" unless undeliverable_recipient
          log "No Bounce Status Code detected!" unless status_code_match_data
        end

        if account.has_email_switch_bounce_processing_debugging_mode?
          log "Arturo enabled: 'email_switch_bounce_processing_debugging_mode'"
          log_bounce_information
        end

        reject!("Rejecting: Account #{account.id}: This email is a #{type} bounce notification detected via #{bounce_via}")
      else
        log "Email is not a bounce notification"
      end
    end

    private

    delegate :subject, :from, :attachments, to: :mail

    def identity_for_email(email, create = true)
      identity = account.user_email_identities.where(value: email).first

      if !identity && create
        log "Creating User and UserEmailIdentity for: #{email}"
        name = Zendesk::Mail::Address.parse(email).try(:name) || User::UNKNOWN_NAME
        identity = User.create(account: account, email: email, name: name).try(:identities).try(:first)
        log "Unable to create UserEmailIdentity for email #{email} when processing email bounce" unless identity
      end

      identity
    end

    def update_identity_deliverability!(email)
      return unless (identity = identity_for_email(email, create_user?))

      if soft_bounce?
        identity.increment_undeliverable_count!
        log "Incremented undeliverable_count to #{identity.undeliverable_count} for undeliverable recipient #{email}"
      else # hard bounce
        identity.mark_deliverable_state(DeliverableStateType.UNDELIVERABLE) if identity.deliverable?
        log "Marked recipient #{email} as undeliverable"
      end
    end

    def create_user?
      if account.has_email_create_undeliverable_user_for_open_accounts_only?
        response_to_zendesk_mail? && account.is_open?
      else
        response_to_zendesk_mail?
      end
    end

    # Remove after email_update_all_targets_on_bounce Arturo gets GA'd
    def update_target_deliverability!(email)
      @target_action = "no_target"
      target = EmailTarget.where(account_id: account.id).where("data LIKE ?", "%email: #{email}%").first
      return unless target

      unless target.is_active
        @target_action = "inactive_target"
        return
      end

      if soft_bounce?
        @target_action = "failure_count_incremented"
        target.message_failed
      else # hard bounce
        target.deactivate(send_notification: true)
      end

      # Capture deactivation for a hard bounce, or a soft bounce that reached the MAX_UNDELIVERABLE_COUNT
      @target_action = "deactivated" unless target.is_active
    end

    def update_email_targets_deliverability!(email)
      targets = account.targets.active.email.with_email(email)

      log "Found #{targets.size} active email targets with IDs: #{targets.map(&:id)} for #{account.subdomain}"
      @target_action = :no_target && return unless targets.any?

      if soft_bounce?
        targets.map(&:message_failed)
        @target_action = :failure_count_incremented
      else
        targets.each { |target| target.deactivate(send_notification: true) }
        @target_action = :deactivated
      end

      log "#{targets.size} email targets: '#{@target_action}'"
      statsd_client.count('bounce.targets', targets.size, tags: ["action:#{@target_action}"])

      targets
    end

    def log_bounce_stats
      # Named capture group to pull just the status code (e.g. 5.2.0) out of the status string.
      # Important that the `:` chars in the STATUS_PATTERNS do not leak into the statsd tag
      statsd_tags = [
        "bounce_via:#{bounce_via}",
        "bounce_type:#{bounce_type}",
        "status_code:#{status_code_match_data[:code]}",
        "target_deliverability_action:#{@target_action || 'no_target'}",
        "reject_only:#{reject_only_bounce? ? 'yes' : 'no'}"
      ]

      # Remove `target_deliverability_action` after email_update_all_targets_on_bounce Arturo gets GA'd
      statsd_client.increment(:bounce, tags: statsd_tags)
    end

    # Remove BounceProcessor#log_bounce_information when Bounce v2.0 research is complete
    def log_bounce_information
      log "[DEBUG] BounceProcessor: " \
        "message_id: #{mail.message_id}, " \
        "account: #{account.subdomain}, " \
        "bounce_attachments: #{delivery_status_messages.size}, " \
        "bounce_via: #{bounce_via}, " \
        "bounce_type: #{bounce_type}, " \
        "bounce_subject: #{mail.subject}, " \
        "bounce_sender: #{state.from.try(:address)}, " \
        "#{bounce_data_from_email_body}" \
        "#{bounce_data_from_attachment}"
    rescue StandardError => exception
      log "[DEBUG] BounceProcessor: #{exception.class} #{exception.message}"
    end

    def bounce_data_from_email_body
      if (email_body = state.plain_content)
        email_body_bounce_recipient = email_body[/#{Zendesk::InboundMail::BouncePatterns::FAILED_RECIPIENT}/, 2]
        email_body_bounce_status_code = email_body.match(Zendesk::InboundMail::BouncePatterns::STATUS).try(:[], :code)
        email_body_fulltext = email_body[0...MAX_LOGGED_MESSAGE_LENGTH]

        "email_body_bounce_recipient: #{email_body_bounce_recipient}, " \
        "email_body_bounce_status_code: #{email_body_bounce_status_code}, " \
        "email_body_fulltext: #{email_body_fulltext} ,"
      else
        "email_body_bounce_recipient: , email_body_bounce_status_code: , email_body_fulltext: , "
      end
    end

    def bounce_data_from_attachment
      if (attachment = delivery_status_messages.first.body if delivery_status_messages.any?)
        attachment_bounce_recipient = attachment[/#{Zendesk::InboundMail::BouncePatterns::FAILED_RECIPIENT}/, 2]
        attachment_bounce_status_code = attachment.match(Zendesk::InboundMail::BouncePatterns::STATUS).try(:[], :code)
        attachment_fulltext = attachment[0...MAX_LOGGED_MESSAGE_LENGTH]

        "attachment_bounce_recipient: #{attachment_bounce_recipient}, " \
        "attachment_bounce_status_code: #{attachment_bounce_status_code}, " \
        "attachment_fulltext: #{attachment_fulltext} [END_DEBUG]"
      else
        "attachment_bounce_recipient: , attachment_bounce_status_code: , attachment_fulltext:  [END_DEBUG]"
      end
    end

    def detected_as_bounce?
      subject_undeliverable? && from_undeliverable?
    end

    def subject_undeliverable?
      unless subject.present?
        log "Can't find any email subject: '#{subject}'"
        return false
      end

      return @subject if @subject

      @subject = subject[/#{Zendesk::InboundMail::BouncePatterns::SUBJECT_UNDELIVERABLE}/i].present?
      log "Email subject #{@subject ? 'does' : 'does not'} appear like a bounce notification: '#{subject}'"

      @subject
    end

    def from_undeliverable?
      address = from.try(:address).try(:to_s)
      return false unless address.present?
      return @from if @from

      @from = address[/#{Zendesk::InboundMail::BouncePatterns::FROM_UNDELIVERABLE}/i].present?
      log "From address #{@from ? 'is' : 'is not'} listed as a common bounce address: '#{address}'"

      @from
    end

    def delivery_status_msg_attached?
      log "Found #{delivery_status_messages.size} delivery status attachments"
      delivery_status_messages.any?
    end

    def undeliverable_recipient
      return @undeliverable_recipient if @undeliverable_recipient

      @undeliverable_recipient = bounce_body[/#{Zendesk::InboundMail::BouncePatterns::FAILED_RECIPIENT}/, 2]
      log "Bounce's original recipient: '#{@undeliverable_recipient}'" if @undeliverable_recipient

      @undeliverable_recipient
    end

    def hard_bounce?
      # All 5.*.* are considered hard bounces, except 5.2.* which indicates full mailbox
      status_code_class == '5' && status_code_subjects != '2'
    end

    def soft_bounce?
      !hard_bounce? # On the assumption that there *IS* a bounce of some sort
    end

    def reject_only_bounce?
      # These are bounces (hard or soft) where we have decided to prevent the incoming mail from creating
      # a ticket but are not updating the UserIdentity deliverability
      return @reject_only_bounce if @reject_only_bounce
      return false unless subject.present?

      @reject_only_bounce = subject[/#{Zendesk::InboundMail::BouncePatterns::SUBJECT_REJECT_ONLY}/i].present?.tap do |reject_only|
        log("Subject is reject-only: '#{subject}'") if reject_only
      end
    end

    def status_code_class
      status_code[0]
    end

    def status_code_subjects
      status_code[1]
    end

    def status_code_match_data
      return @matchdata if @matchdata

      @matchdata = bounce_body.match(Zendesk::InboundMail::BouncePatterns::STATUS)
      log "Bounce Status code via #{bounce_via}: '#{@matchdata[:code]}' (#{bounce_type})" if @matchdata

      @matchdata
    end

    def status_code
      return @status_code if @status_code

      @status_code = if status_code_match_data
        # Returns the Status Code values as [class, subject]
        [status_code_match_data[:class], status_code_match_data[:subjects]]
      else
        ["0", "0"]
      end
    end

    def delivery_status_messages
      return @delivery_status_messages if @delivery_status_messages

      # Returns all attachments with `Content-Type: message/delivery-status`
      @delivery_status_messages = attachments.select do |attachment|
        debug_log " - Checking if '#{attachment.file_name}' attachment (#{attachment.content_type}) is a delivery status message"
        next unless attachment.content_type
        !!attachment.content_type.match(Zendesk::InboundMail::BouncePatterns::DSN_ATTACHMENT)
      end

      log "Found #{@delivery_status_messages.size} (message/delivery-status) attachments: #{delivery_status_messages.map(&:file_name).inspect}"
      @delivery_status_messages
    end

    def description
      return @description if @description

      @description = if delivery_status_messages.any?
        delivery_status_messages.first.body
      else
        state.plain_content
      end

      @description
    end

    def response_to_zendesk_mail?
      found_zendesk_header = (state.plain_content =~ Zendesk::InboundMail::BouncePatterns::ZENDESK_MAILER).present?
      log "Bounce is a response to a Zendesk-sent email. Ticket ##{state.ticket.try(:nice_id)}" if found_zendesk_header
      found_zendesk_header
    end

    def response_to_zendesk_welcome_mail?
      found_zendesk_header = if response_to_zendesk_welcome_mail_in_body?
        true
      elsif response_to_zendesk_welcome_mail_in_original_email?
        true
      else
        response_to_zendesk_welcome_email_in_delivery_status_notification?
      end

      log "Bounce is a response to a Zendesk welcome email" if found_zendesk_header

      found_zendesk_header
    end

    def response_to_zendesk_welcome_mail_in_body?
      (state.plain_content =~ Zendesk::InboundMail::BouncePatterns::ZENDESK_WELCOME_MAIL).present?
    end

    def response_to_zendesk_welcome_email_in_delivery_status_notification?
      delivery_status_messages.any? do |delivery_status_message|
        (delivery_status_message.body =~ Zendesk::InboundMail::BouncePatterns::ZENDESK_WELCOME_MAIL).present?
      end
    end

    def response_to_zendesk_welcome_mail_in_original_email?
      attachments.any? do |attachment|
        # Attachments with content-type "message/rfc822" are complete emails
        # When such an attachment is included in a bounce notification
        # emails, it is often, if not always, a copy of the original email
        # that bounced.
        attachment.content_type == "message/rfc822" &&
          attachment.body =~ Zendesk::InboundMail::BouncePatterns::ZENDESK_WELCOME_MAIL
      end
    end

    def bounce_type
      hard_bounce? ? :hard : :soft
    end

    def bounce_via
      return @bounce_via if @bounce_via

      @bounce_via =
        if !detected_as_bounce? && delivery_status_msg_attached?
          :attachment
        else
          :email_body
        end

      @bounce_via
    end

    def bounce_body
      return @bounce_body if @bounce_body

      @bounce_body =  case bounce_via
                      when :email_body then state.plain_content
                      when :attachment then delivery_status_messages.first.body
      end

      debug_log "Bounce's body detected via #{bounce_via}: '#{@bounce_body}'"
      @bounce_body
    end
  end
end
