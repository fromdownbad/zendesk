module Zendesk::InboundMail::Processors
  # Checks if the email is spam or from automated systems and if so, suspends the email by placing
  # it in the 'ticket bin'.
  #
  # Avoid auto-responders http://partmaps.org/era/mail/autoresponder-faq.html
  # http://www.hackvalue.nl/en/article/19/the_art_of_autoresponders_part_2
  # http://www.emaildiscussions.com/showthread.php?t=50560
  class ValidatingProcessor < BaseProcessor
    def execute
      if blank_mail?
        suspend("no content", SuspensionType.BLANK)
      end

      if to_account_no_reply_address?
        suspend("sent to account noreply address", SuspensionType.TO_NOREPLY)
      end

      filter(Zendesk::InboundMail::ValidationRule.hard_rules)

      if state.whitelisted?
        log("Not checking mail against soft rules because it is whitelisted!")
        return
      end

      filter(soft_rules)
      suspend_bulk_mail
    end

    private

    def soft_rules
      @soft_rules ||= Zendesk::InboundMail::ValidationRule.soft_rules
    end

    def suspend_bulk_mail
      if mail.bulk?
        if mail.from_google_apps_group?
          log("Passing G Suite group mail")
        elsif mail.from_permitted_bulk_mailer?
          log("Passing suspected bulk mail")
        else
          suspend("bulk headers (precedence or email list) (may whitelist)", SuspensionType.AUTO_MAILER)
        end
      end
    end

    def blank_mail?
      mail.subject.blank? && mail.body.blank? && mail.parts.all?(&:blank?)
    end

    def to_account_no_reply_address?
      state.recipient == Zendesk::Mailer::AccountAddress.new(account: account).noreply_email
    end

    def filter(rules)
      filtered_rules = []

      rules.each do |rule|
        next unless rule.deny?(mail, account)

        # NOTE: The suspension message shows the header name and offending value. E.g "Header mailer: Autoresponder"
        message = "Header #{rule.header}: #{rule.value(mail)}"

        if rule.header == 'Auto-Submitted' && rule.value(mail) =~ /auto-generated/i && from_outlook_dot_com?
          log("Ignore rule for Outlook.com: #{message}")
          next
        end

        message << " (may whitelist)" if soft_rules.include?(rule)

        filtered_rules << rule

        log("Suspending ticket with message: #{message}")
        suspend(message, rule.error)
      end

      filtered_rules
    end

    def from_outlook_dot_com?
      mail.message_id.ends_with?(".outlook.com>", ".exchangelabs.com>")
    end

    def suspend(message, reason)
      return if state.suspension
      log("Suspended due to: #{message}")
      state.suspension = reason
    end
  end
end
