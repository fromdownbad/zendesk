module Zendesk::InboundMail::Processors
  # Resolves what address this email originally got sent to in case it was not Zendesk
  class RecipientProcessor < BaseProcessor
    def execute
      if address = original_recipient_address
        if sanitized = Zendesk::Mail::Address.sanitize(address)
          if sent_to_google_group_alias? && sanitized == account.default_recipient_address.try(:email)
            log("Original recipient address set to '#{matched_xbeen_there}' instead of '#{sanitized}' for account #{account.subdomain}")
            state.original_recipient_address = matched_xbeen_there
          else
            log("Original recipient address set to '#{sanitized}'")
            state.original_recipient_address = sanitized
          end
        else
          log("Ignoring invalid original recipient address: #{address}")
        end
      end
    end

    private

    def original_recipient_address
      recipient_from_account_recipients || recipient_used_to_find_account
    end

    def recipient_from_account_recipients
      log("Checking for account recipient addresses: #{mail.recipients.inspect}")

      if recipient_address = Zendesk::InboundMail::RecipientsHelper.find_recipient_address(mail, account)
        log("Using original recipient address from account recipients.")
        state.brand_id = recipient_address.active_brand.id
        recipient_address.ticket_received!
        recipient_address.email
      end
    end

    def recipient_used_to_find_account
      unless state.recipient.to_s.end_with?(Zendesk::Configuration.fetch(:host))
        log("Using original recipient address from account lookup.")
        state.recipient
      end
    end

    def sent_to_google_group_alias?
      account.has_google_group_alias_x_been_there? && matched_xbeen_there
    end

    def matched_xbeen_there
      x_been_there.detect { |email| account.recipient_addresses.by_email(email).exists? }
    end

    def x_been_there
      been_there_values = mail.headers['X-BeenThere'].presence || []
      been_there_values.map(&:address)
    end
  end
end
