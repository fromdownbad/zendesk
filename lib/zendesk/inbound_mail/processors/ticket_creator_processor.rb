module Zendesk::InboundMail::Processors
  # Creates the ticket and sets it on state.
  class TicketCreatorProcessor < BaseProcessor
    def execute
      ticket = state.closed_ticket || state.ticket

      unless state.suspended?
        @author_persisted = state.author.try(:persisted?)
        save_author
      end

      if suspend_ticket_from_unverified_account_owner?
        ensure_verified_owner if ticket.nil? && !state.suspended?
      end

      ensure_valid_operation(ticket) if ticket && !state.suspended?
      ensure_ticket
      ensure_valid_ticket_group_for_restricted_agent
      ensure_valid_ticket_group

      if state.ticket.is_a?(Ticket)
        options = { via_id: state.ticket.current_via_id || ViaType.MAIL }
        state.ticket.via_reference_id = state.ticket.source_links.first.source_id if options[:via_id] == ViaType.CLOSED_TICKET
        state.ticket.will_be_saved_by(state.author, options)
        propagate_created_at(state.ticket)
        state.comment = state.ticket.comment
      end

      Zendesk::InboundMail::Ticketing::MailTicket.new(state, mail).add_metadata(state.ticket)
    end

    # Used in CollaboratorProcessor and ATSDProcessor
    def suspend_ticket
      if account.has_email_suspend_ticket_logs?
        log("Creating a suspended ticket. #{suspension_type_details}")
      else
        log("Creating suspended ticket")
      end
      suspended = Zendesk::InboundMail::Ticketing::MailTicket.new(state, mail).to_suspended_ticket

      if state.locale_set_by_detection?
        suspended.properties ||= {}
        suspended.properties[:locale_id] = state.locale_detected
      end

      suspended
    end

    private

    def ensure_valid_ticket_group
      return unless account.has_email_replace_invalid_group?

      if !state.suspended_ticket? && !state.ticket.group.try(:valid?)
        default_group = account.default_group # OR state.submitter.default_group_id

        if default_group.present? && default_group.valid? && default_group.is_active?
          log("Default group ##{default_group.id}: #{default_group.inspect}")
          log("Replacing ticket's invalid group ##{state.ticket.group_id} with the account's default group ##{default_group.id}")
          state.ticket.group_id = default_group.id
        end
      end
    end

    def ensure_valid_ticket_group_for_restricted_agent
      if !state.suspended_ticket? && group_restricted_agent_forwarded_new_message?
        log("Changing ticket's group_id to ##{state.submitter.default_group_id} because group restricted agent forwarded a new message")
        state.ticket.group_id = state.submitter.default_group_id
      end
    end

    def group_restricted_agent_forwarded_new_message?
      state.agent_forwarded && group_restricted_submitter? && !state.closed_ticket
    end

    def group_restricted_submitter?
      restricted = state.group_restricted_submitter?
      log("Submitter has restriction id of RoleRestrictionType.GROUPS") if restricted
      restricted
    end

    def propagate_created_at(ticket)
      if ticket.created_at_changed?
        ticket.audit.created_at = ticket.created_at
        ticket.comment.created_at = ticket.created_at
      end
    end

    def ensure_ticket
      if state.suspended?
        state.ticket = suspend_ticket
      elsif state.ticket
        add_comment
      else
        build_ticket
      end

      log("Email subject: '#{mail.subject}'")
      log("Ticket title: '#{state.ticket.title}'")
      state.ticket
    end

    def build_ticket
      state.ticket = Zendesk::InboundMail::Ticketing::MailTicket.new(state, mail).to_ticket

      if state.closed_ticket && !ticket_anonymized?
        if state.account.has_email_followup_ticket_html_comment_fix? && state.ticket&.comment&.html_body
          build_followup(state.ticket.comment.html_body)
        else
          build_followup(state.ticket.description)
        end
      end

      if state.locale_set_by_detection?
        state.ticket.translation_locale = state.author.translation_locale
      end
      log(
        "Creating#{state.closed_ticket ? ' followup' : ''} ticket with " \
        "#{state.ticket.comment.is_public ? 'public' : 'private/internal'} comment"
      )

      log("Disabling triggers (x-zendesk-disable-triggers header detected)") if state.ticket.disable_triggers
    end

    def build_followup(mail_description)
      followup = Zendesk::InboundMail::Ticketing::Followup.new(state, mail, mail_description)

      # The author of this email has to be either CC'ed, requester or assignee on the old ticket
      # This means that the author must exist at this point in time.
      @author_persisted = state.author.persisted? if @author_persisted.nil?
      if @author_persisted
        state.ticket.requester = followup.original_requester
        copy_followup_collaborators(followup)
      end

      state.ticket.comment         = followup.add_comment
      state.ticket.original_via_id = state.ticket.via_id
      state.ticket.via_id          = followup.via_id
      state.ticket.additional_tags = followup.additional_tags
      state.ticket.description     = followup.description
      state.ticket.recipient       = followup.original_recipient

      # 2015.10.21 Ticket#set_followup_source seems to primarily take care of source links, ticket fields, and
      # current tags - let's use that for consistency with other followup creation paths (and because it fixes
      # ZD#1244568)

      state.ticket.via_followup_source_id = followup.closed_ticket.nice_id
      state.ticket.set_followup_source_ticket(followup.closed_ticket)
    end

    def ticket_anonymized?
      return unless state.account.has_skip_anonymized_ticket_followup?

      state.closed_ticket.requester.settings.scrubbed_ticket_anonymous_user
    end

    def copy_followup_collaborators(followup)
      followup_log_message = ["Ticket is a follow up to #{followup.closed_ticket.id}. Requester set to #{state.ticket.requester.id}"]
      if account.has_follower_and_email_cc_collaborations_enabled? && !followup.legacy_ccs_present?
        if followup.follower_ids.present?
          state.ticket.set_followers = state.ticket.user_ids_to_collaborators_hashes(followup.follower_ids)
          followup_log_message << "followers to #{followup.follower_ids.inspect}"
        end
        if followup.email_cc_ids.present?
          state.ticket.set_email_ccs = state.ticket.user_ids_to_collaborators_hashes(followup.email_cc_ids)
          followup_log_message << "email_ccs to #{followup.email_cc_ids.inspect}"
        end
      else
        state.ticket.set_collaborators = followup.all_collaborator_ids
        followup_log_message << "collaborators to #{followup.all_collaborator_ids.inspect}"
      end
      log followup_log_message.join(", ")
    end

    def add_comment(_state = state, _mail = mail)
      mail_comment_attributes = Zendesk::InboundMail::Ticketing::MailComment.new(state, mail).attributes

      log(
        "Updating ticket #{state.ticket.id} with#{state.flagged? ? ' flagged' : ''} " \
        "#{mail_comment_attributes[:is_public] ? 'public' : 'private/internal'} comment"
      )

      state.ticket.add_comment(mail_comment_attributes.except(:original_plain_body))
      if account.has_email_exclude_inline_attachments_fix? && mail_comment_attributes[:html_body].present?
        state.ticket.comment.format = "rich"
      end
      # :original_plain_body will not get set by the above mass assignment as it is not a DB column (not attr_accessible), so set it directly
      state.ticket.comment.original_plain_body = mail_comment_attributes[:original_plain_body]
      state.ticket.current_via_id = ViaType.Mail
    end

    def ensure_valid_operation(ticket)
      if !account.is_serviceable?
        state.suspension = SuspensionType.UNSERVICEABLE_ACCOUNT
        trial_limit = ' due to exceeding user trial limit' if account.abusive?
        log("Unable to update ticket #{ticket.id} because the account #{account.id} is unserviceable#{trial_limit}")
        return false

      elsif !state.submitter.can?(:edit, ticket)
        if state.authorized? && state.submitter.can?(:add_collaborator, ticket)
          log("Submitter #{state.submitter.id} unable to edit ticket, but authorized to add collaborators.")

          if account.has_ticket_followers_allowed_enabled? && state.submitter.can?(:add_follower, ticket)
            log("Submitter #{state.submitter.id} unable to edit ticket, but authorized to add followers.")
          end

          if account.has_comment_email_ccs_allowed_enabled? && state.submitter.can?(:add_email_cc, ticket)
            log("Submitter #{state.submitter.id} unable to edit ticket, but authorized to add email CCs.")
          end
        elsif state.submitter.can?(:only_create_ticket_comments, ticket)
          log("Submitter #{state.submitter.id} unable to edit ticket, but authorized to only_create_ticket_comments.")
        end

        if state.authorized?
          log("Submitter #{state.submitter.id} unable to edit ticket, but flagging since ticket was identified by encoded ID.")
          state.add_flag(EventFlagType.OTHER_USER_UPDATE)
        else
          log("Submitter #{state.submitter.id} unable to edit ticket, so suspending.")
          state.suspension = SuspensionType.OTHER_USER_UPDATE
        end

        can_or_not = (state.submitter.can?(:view, ticket) ? "can" : "can not")
        log("Attempt to update ticket #{ticket.id} by user #{state.submitter.id}, who #{can_or_not} view ticket.")
      end

      true
    end

    def save_author
      adapter = Zendesk::InboundMail::Adapters::AuthorAdapter.new(processing_state: state,
                                                                  account: account,
                                                                  mail_logger: mail_logger,
                                                                  statsd_client: statsd_client,
                                                                  save_attrs: [:locale_id])
      adapter.save_with_instrumentation!
    rescue ActiveRecord::RecordInvalid, ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation => exception
      log("Can't save the author #{state.author.name}! #{exception.class}: #{exception.message}")
      repair_author
    end

    def repair_author
      # Attempt to repair invalid author if possible
      author = state.author
      log("Trying to repair Author because: #{author.errors.full_messages.join(', ')}")

      # Try to fix up collaboration with an existing user that has the same email address
      user_identity_value = author.identities.first.try(:value)
      existing_user = User.uncached { account.find_user_by_email(user_identity_value) }

      if existing_user.try(:valid?)
        log("Found a valid user with #{user_identity_value} email address: #{existing_user.inspect}")
      else
        log("Couldn't find a valid user with #{user_identity_value} email address!")
        return
      end

      log("Saving Author's locale ID: #{state.locale_detected}")
      existing_user.locale_id = state.locale_detected

      log("Replacing Author ##{state.author.try(:id)} with: ##{existing_user.try(:id)} user")
      state.author = existing_user
    end

    def ensure_verified_owner
      if account.has_email_suspend_ticket_from_unverified_account_owner?
        unless account.owner.is_verified?
          state.suspension = SuspensionType.ACCOUNT_OWNER_EMAIL_UNVERIFIED
          log("Suspending ticket because the account owner #{account.owner.id} is unverified")
          return false
        end
      end

      true
    end

    def suspend_ticket_from_unverified_account_owner?
      return false unless account.has_email_suspend_ticket_from_unverified_account_owner?

      rollout_date = Account::SUSPEND_TICKET_FROM_UNVERIFIED_ACCOUNT_OWNER_ROLLOUT_DATE
      account.created_at > rollout_date
    end

    def suspension_type_details
      return unless state.suspension
      "Suspension type ##{state.suspension} #{SuspensionType[state.suspension]&.name&.upcase}"
    end
  end
end
