module Zendesk::InboundMail::Processors
  # Finds the ticket that should be updated (if it is an update).
  class TicketFinderProcessor < BaseProcessor
    MIN_SUBJECT_SIZE = 3

    delegate :agent, to: :state

    def execute
      state.ticket = ticket_for_update

      throttle if state.from && state.ticket && !state.suspended?

      state.ticket = handle_closed_ticket if state.ticket && state.ticket.closed?

      state
    end

    private

    # There's a closed ticket on state, figure out what to update and modify content
    def handle_closed_ticket
      # The `latest` scope doesn't work with archived ticket events
      audit = if state.ticket.archived?
        state.ticket.audits.last
      else
        state.ticket.audits.latest
      end

      if audit.via?(:merge) && current = Ticket.find_by_id(audit.via_reference_id)
        log("Ticket #{state.ticket.id} merged into #{current.id}: updating that instead")
        return current unless current.closed?
      end

      state.closed_ticket = state.ticket

      nil
    end

    def throttle
      if account.has_email_ignore_rate_limits?
        log("Bypassing the rate limit on ticket updates by specific user")
        return
      end

      throttle_key = "#{state.from.address}:#{state.ticket.id}"

      if Prop.throttle(:email_sender_ticket_updates, throttle_key)
        count = Prop.query(:email_sender_ticket_updates, throttle_key)

        warn("suspended due to loop. Sender cannot update the same ticket more than 10 times an hour. (#{count})")
        if throttle_enabled?
          state.suspension = SuspensionType.LOOP
        end
      end
    end

    def throttle_enabled?
      # Check inbound_mail/rate_limit.rb for more details about limits.
      Rails.env.production?
    end

    def ticket_for_update
      ticket   = find_ticket_by_email_token
      ticket ||= find_by_message_ids([mail.in_reply_to].concat(mail.references))
      ticket ||= find_ticket_by_body_or_subject_encoded_id
      ticket ||= find_ticket_by_recipient_encoded_id
      ticket ||= find_ticket_by_recipient_nice_id
      ticket ||= find_ticket_by_body_nice_id

      if ticket
        log("#{ticket.closed? ? 'Closed t' : 'T'}icket id: #{ticket.id} nice: #{ticket.nice_id} (found by: #{@location} #{@version})")

        if ticket.closed? && ticket.requester.suspended?
          warn("Original ticket author suspended: #{ticket.requester.id} #{ticket.requester.email}, attempting to create new ticket")
          ticket = nil # create a new ticket, original author is suspended
        end

        record_stats
      elsif @ignored_recipient_encoded_id_matched_ticket
        log_ignored_recipient_encoded_id_matched_ticket_data
        statsd_client.increment("ticket.ignore_recipient_encoded_id_matched_ticket", tags: ["sender_role:#{sender_role_tag_value}"])
      else
        log("Could not find a ticket")
      end

      ticket
    end

    def find_ticket_by_email_token
      log("Looking up by email encoded_id from header message_ids: #{mail.message_ids.inspect}")

      # E.g. PXTB06MF12_5464fd9da933d_180833fce15465be0819e7_sprut@zendesk-test.com or PXTB06MF12@zendesk-test.com
      alphabet = Zendesk::MtcMpqMigration::ZendeskMail::TicketIdentification::ENCODED_ID_ALPHABET
      pattern = /#{alphabet}{10,}/

      encoded_ids = mail.message_ids.map { |id| id[/^<(#{pattern})(_.*)?@#{Regexp.escape(Zendesk::Configuration.fetch(:host))}>/, 1] }.compact.uniq
      encoded_ids.detect do |encoded_id|
        unless ticket = account.tickets.find_by_encoded_id(encoded_id, logger: mail.logger)
          log("Can't find any Ticket with encoded_id: '#{encoded_id}' on '#{account.subdomain}' account")
          next
        end

        @location = :message_ids
        @version  = :encoded_id

        state.authorize!
        return ticket
      end
    end

    def find_ticket_by_recipient_encoded_id
      if encoded_id = mail.recipients_encoded_id(account)
        log("Found potential encoded ID match in recipient headers: #{encoded_id}")
      else
        log("No encoded IDs found in recipient headers")
        return
      end

      if ticket = account.tickets.find_by_encoded_id(encoded_id, logger: mail.logger)
        log("Ticket with encoded ID #{encoded_id} found: Ticket ##{ticket.id}")
      else
        log("No ticket found with encoded ID #{encoded_id}")
        return
      end

      if !agent
        log("Not matching inbound mail to Ticket ##{ticket.id} using recipient header encoded ID, because sender <#{state.from.address}> is not an agent")
        @ignored_recipient_encoded_id_matched_ticket = ticket
        return
      elsif agent.can?(:edit, ticket)
        log("Matching inbound mail to Ticket ##{ticket.id} using recipient header encoded ID, because sender <#{state.from.address}> (#{sender_role_tag_value}) has permission to edit the ticket")
      elsif agent.is_light_agent? && agent.can?(:view, ticket)
        log("Matching inbound mail to Ticket ##{ticket.id} using recipient header encoded ID, because sender <#{state.from.address}> (light_agent) has permission to view the ticket")
      else
        log "Found Ticket ##{ticket.id} for account '#{account.subdomain}' via recipient encoded ID, but sender <#{state.from.address}> (#{sender_role_tag_value}) cannot edit the ticket, so looking for other ticket identifiers in the email"
        @ignored_recipient_encoded_id_matched_ticket = ticket
        return
      end

      @location = :recipients
      @version  = :encoded_id

      state.id_location = @location
      state.authorize!
      ticket
    end

    def find_ticket_by_body_or_subject_encoded_id
      mail.body_and_subject_encoded_ids.detect do |encoded_id|
        log("Looking up by #{encoded_id.location} (encoded)")

        next unless ticket = account.tickets.find_by_encoded_id(encoded_id.value, logger: mail.logger)
        @location = encoded_id.location
        @version  = :encoded_id

        state.id_location = @location
        state.authorize!
        return ticket
      end
    end

    def find_ticket_by_recipient_nice_id
      log("Looking up by nice_id on the recipients list")

      unless nice_id = mail.recipients_nice_id(account)
        log("No nice_id found on the recipients list")
        return
      end

      unless ticket = account.tickets.find_by_nice_id(nice_id)
        log("Can't find any Ticket with nice_id: '#{nice_id}' on '#{account.subdomain}' account")
        return
      end

      state.via_recipient_nice_id = true
      @location = :recipients
      @version  = :nice_id

      if account.has_email_suspend_recipient_nice_id_forgery?
        log("Checking for user recipient nice id forgery - email_suspend_recipient_nice_id_forgery enabled")
        suspend_user_nice_id_forgery(ticket)
      end

      suspend_agent_nice_id_forgery(ticket)

      ticket
    end

    def find_ticket_by_body_nice_id
      log("Looking up by nice_id in the body")

      # Agents can easily be spoofed, and hackers can comment/use text-api on every ticket
      if agent
        log("Stopped search since Agents cannot thread comments by nice_id alone (insecure)")
        return
      end

      unless nice_id = mail.body_nice_id
        log("No nice_id found in the body")
        return
      end

      unless ticket = account.tickets.find_by_nice_id(nice_id)
        log("Can't find any Ticket with nice_id: '#{nice_id}' on '#{account.subdomain}' account")
        return
      end

      @location = :body
      @version  = :nice_id
      ticket
    end

    # Resolve ticket from Reply-To or References header. Only works for ticket updates where people were CC'd
    # on mails that resulted in ticket updates. E.g. person A sends email to support and person B.
    # Person B does a "Reply All" to this email.
    def find_by_message_ids(message_ids)
      log("Looking up by message_ids")

      if message_ids.blank?
        log("No message_ids found")
        return
      end

      message_ids = message_ids.reject(&:blank?)
      message_ids.map! { |message_id| message_id.try(:downcase) }
      emails = account.inbound_emails.where(message_id: message_ids).to_a.sort_by { |e| message_ids.index(e.message_id.try(:downcase)) }

      # We send out in_reply_to with encoded_id@email.com and have message id encoded_id_xxx@email.com
      emails.concat(from_and_in_reply_to_emails) if mail.in_reply_to.present?

      emails.detect do |email|
        log("Looking up by message_id #{email.message_id}")

        unless email.ticket_id && ticket = account.tickets.find_by_id(email.ticket_id)
          log("Can't find any Ticket with ID: '#{email.ticket_id}' on '#{account.subdomain}' account")
          next
        end

        if mail.in_reply_to.present? && !mail.from_zendesk? && from_and_in_reply_to_emails.include?(email)
          log("The mail is not from Zendesk and references emails with message_ids: #{from_and_in_reply_to_emails.map(&:message_id)}")
          log("New email threaded: '#{email.message_id}'")
        end

        @location = :references
        @version  = :inbound_email

        state.authorize!
        return ticket
      end
    end

    def from_and_in_reply_to_emails
      ActiveRecord::Base.on_slave do
        @from_and_in_reply_to_emails ||= account.inbound_emails.where(
          from:        mail.from.address,
          in_reply_to: mail.in_reply_to
        )
      end
    end

    def suspend_user_nice_id_forgery(ticket)
      return if mail.smtp_envelope_from.downcase.include? mail.sender.address.downcase

      state.suspension = SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE
      log("Suspending ticket lookup by nice id ##{ticket.nice_id} due to possible user forgery")
    end

    # From can easily be spoofed, so  we cannot trust emails that come in via nice_id + agent
    # since it could be abused to get access to/modify all tickets in an account
    def suspend_agent_nice_id_forgery(ticket)
      return unless agent

      state.suspension = SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE
      log("Suspending ticket lookup by nice id ##{ticket.nice_id} due to possible agent forgery")
    end

    def record_stats
      state.record_deferred_stat("ticket.threading", tags: %W[method:#{@version} location:#{@location}])
    end

    def sender_role_tag_value
      if !agent
        "end_user"
      elsif agent.is_light_agent?
        "light_agent"
      else
        "agent"
      end
    end

    def log_ignored_recipient_encoded_id_matched_ticket_data
      log(
        "Ignoring ticket found via recipient encoded ID. No other connection to ticket found in the email, and email sender cannot comment based on recipient encoded ID alone: " \
        "Account: #{account.subdomain}\; " \
        "Ticket ID: #{@ignored_recipient_encoded_id_matched_ticket.id}\; " \
        "Ticket nice ID: #{@ignored_recipient_encoded_id_matched_ticket.nice_id}\; " \
        "Ticket encoded ID: #{@ignored_recipient_encoded_id_matched_ticket.encoded_id}\; " \
        "Sender: #{state.from.name} <#{state.from.address}>\; " \
        "Sender is agent: #{!!agent}\; " \
        "Sender is light agent: #{agent ? agent.is_light_agent? : false}\; " \
        "To header addresses: #{mail.to.map(&:address).join(", ")}\; " \
        "CC and BCC header addresses: #{[mail.cc, mail.bcc].flatten.map(&:address).join(", ")}\; " \
        "Email subject: #{mail.subject}"
      )
    rescue StandardError => exception
      log "Error while logging ignored ticket found via recipient encoded ID: #{exception.class} #{exception.message}"
    end
  end
end
