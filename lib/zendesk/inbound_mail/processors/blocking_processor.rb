require 'set'

module Zendesk::InboundMail::Processors
  # BlockingProcessor rejects if they match a reason to block.
  #
  # Current reasons to block are:
  # * email sent to addresses in BLOCK_RECIPIENT_LIST (currently just 'noreply@twitter.zendesk.com'.)
  # * email sent to non-support addresses, if account doesn't accept wildcard emails.
  class BlockingProcessor < BaseProcessor
    BLOCK_RECIPIENT_LIST = Set['noreply@twitter.zendesk.com']

    def execute
      if BLOCK_RECIPIENT_LIST.member?(state.recipient)
        reject!("Block recipient list #{state.recipient} hard reject")
      end

      if blocked_wildcard_usage?
        reject!("Block recipient #{state.recipient}: wildcard is disabled, not in recipient_addresses, hard reject")
      end
    end

    private

    # All normally incoming emails have @host_name addresses,
    # only emails coming in through mail_fetcher have a non @hostname address
    def blocked_wildcard_usage?
      return false unless state.recipient

      recipient = without_plus_tag(state.recipient)
      domain = recipient.split("@").last

      !account.settings.accept_wildcard_emails &&
        account.routes.map(&:default_host).include?(domain) &&
        !account.recipient_emails.include?(recipient) &&
        recipient != Zendesk::Mailer::AccountAddress.new(account: account).noreply_email
    end

    def without_plus_tag(email)
      email.sub(/\+.*@/, "@")
    end
  end
end
