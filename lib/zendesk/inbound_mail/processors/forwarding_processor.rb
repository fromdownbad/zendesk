module Zendesk::InboundMail::Processors
  # Attempts to extract original mail from agent/support-address forwarded mails to new tickets.
  #
  # Preconditions:  state.ticket, account
  # Postconditions: state.submitter, state.from, mail.subject, state.plain_content
  class ForwardingProcessor < BaseProcessor
    def execute
      parse_forward

      # The from address could be a support address (if the forwarded email was
      # also from a support address), so we should suspend in that case
      suspend_if_sender_is_account_address
    end

    private

    def forward
      @forward ||= Zendesk::InboundMail::Forward.from_mail(mail, state.plain_content)
    end

    def parse_forward
      unless forwarding_enabled?
        log("Skipping #{forward_initiator}-forwarded email, forwarding is not applicable")
        return
      end

      log("Parsing #{forward_initiator}-forwarded email from #{mail.client} client.")

      if forward.valid?
        if state.agent
          if account.has_email_disable_light_agent_forwarding?
            unless state.agent.try(:can?, :publicly, Comment)
              suspend("Forwarded email is from an agent with no public comment access — suspending.", SuspensionType.LIGHT_AGENT_FORWARDED)
              return
            end
          end

          state.agent_forwarded = true

          if state.flags.include?(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) && account.has_email_reply_to_vulnerability_forwarding_processor?
            log("Forwarded email is from an agent who was specified in the reply to, proceeding but could be a possible spoof")
          end

          unless account.has_email_disable_light_agent_forwarding?
            unless state.agent.try(:can?, :publicly, Comment)
              log("Forwarded email is from an agent with no public comment access")
              statsd_client.increment("forwarding_processor.private_comment_agent")
            end
          end

          state.submitter       = state.agent
          state.agent_comment   = forward.comment
        end

        state.from          = forward.from unless from_account_address?(forward.from.address)
        mail.subject        = forward.subject
        state.plain_content = forward.body

        if account.settings.rich_content_in_emails?
          log("Getting HTML body from forwarded message")
          html_body = forward.html_body

          if html_body.blank?
            warn("Failing back to Plain-Text on the forward's message")
          else
            state.html_content = html_body
          end
        end

        log("Parsed forwarded message correctly")
        true
      else
        log("Failed to parse agent forwarded mail: #{forward.errors.inspect}")
        false
      end
    end

    def forwarding_enabled?
      log("Email forwarding detection is #{account.settings.agent_forwardable_emails? ? 'enabled' : 'disabled'} for '#{account.subdomain}' account")
      log("The sender #{state.agent ? 'is' : 'is not'} an Agent.")
      log("The sender #{from_account_address?(state.from.address) ? 'is' : 'is not'} a Support address")

      (
        (state.agent && account.settings.agent_forwardable_emails?) ||
        from_account_address?(state.from.address)
      ) && forwarding_new_ticket?
    end

    def suspend_if_sender_is_account_address
      return unless from_account_address?(state.from.address)

      warn("Sender is a support address", 'suspending')
      warn("Potential Loop detected from #{state.from.address.downcase}", nil)
      state.suspension = SuspensionType.FROM_SUPPORT_ADDRESS
    end

    def forwarding_new_ticket?
      log("The email #{forward ? 'was' : 'was not'} detected as a Forward")
      new_ticket? && forward
    end

    def new_ticket?
      log("The email is related to Ticket/#{state.ticket.id} (nice_id: #{state.ticket.nice_id})") unless state.ticket.blank?
      log("The email is related to Ticket/#{state.closed_ticket.id} (Closed)") unless state.closed_ticket.blank?

      state.ticket.blank? && state.closed_ticket.blank?
    end

    def forward_initiator
      state.agent ? :agent : :recipient
    end
  end
end
