module Zendesk::InboundMail::Processors
  class SyntaxErrorProcessor < BaseProcessor
    def execute
      if mail.has_errors? && !state.suspended?
        raise Zendesk::InboundMail::SyntaxError, mail.error_messages.to_s
      end
    end
  end
end
