module Zendesk::InboundMail::Processors
  # GmailForwardingValidationProcessor handles forwarding verification mail from Gmail.
  #
  # When a Zendesk account admin is setting up a custom support address (ex. support@uber.com),
  # they need to configure their email server to forward all emails coming to their custom support
  # address onto their direct support address (ex. support@uber.zendesk.com).
  #
  # When Gmail and Google Apps users try to configure their accounts to forward all messages to
  # their direct support address, Google will first send an email containing a verification code
  # to the address you're trying to forward all your mail to. You have to enter the verification
  # code in this email to verify that you control the second address. It's their way of
  # preventing abuse.
  #
  # When this processor detects that the mail is a Gmail forwarding verification mail, we create a
  # ForwardingVerificationToken for the current account with Gmail's confirmation code stored in
  # the 'value' field. This ForwardingVerificationToken is used in the UI to display the
  # confirmation code to the user during the onboarding process.
  #
  # The processor will also mark the state as whitelisted if it detects that the mail is a
  # Gmail forwarding verification mail.
  class GmailForwardingValidationProcessor < BaseProcessor
    GMAIL_FORWARDING_VERIFICATION_ADDRESS = 'forwarding-noreply@google.com'.freeze
    VERIFICATION_NUMBER_PATTERN = /\(\D{0,4}(\d{8,12})\D{0,4}\)/ # check for 8-12 digits in case the current length changes (9)

    def execute
      subject = mail.subject.to_s
      forwarding_from_address = subject[FIND_EMAIL_PATTERN]
      gmail_verification_code = subject[VERIFICATION_NUMBER_PATTERN, 1]

      if state.from.address  == GMAIL_FORWARDING_VERIFICATION_ADDRESS && forwarding_from_address && gmail_verification_code
        debug_log("creating forwarding_verification_token")
        token = account.forwarding_verification_tokens.create!(
          to_email: mail.recipients.first,
          from_email: forwarding_from_address,
          email_provider: ForwardingVerificationToken::GMAIL,
          value: gmail_verification_code
        )
        debug_log("forwarding_verification_token created: #{token}")

        log('Received forwarding verification email from Gmail. Change from address and allow creation of ticket')
        state.from = Zendesk::Mail::Address.new(name: 'Zendesk Support', address: 'noreply@zendesk.com')
        state.whitelisted = true
      end
    end
  end
end
