module Zendesk::InboundMail::Processors
  # NPSSurveyAutoReplyIgnoreProcessor rejects mail if it's in response to a Net Promoter Score
  # survey email and it fails any of the hard rules. In the hard rules, we have a number of checks
  # to detect Auto Reply/OOO emails so we're really just rejecting NPS auto reply emails.
  class NPSSurveyAutoReplyIgnoreProcessor < BaseProcessor
    NPS_EMAIL_SUFFIX_PATTERN = /\+nps\d+@/

    def execute
      return unless to_account_nps_address?
      if Zendesk::InboundMail::ValidationRule.hard_rules.any? { |rule| rule.deny?(mail) }
        reject!("nps survey auto reply")
      end
    end

    private

    def nps_address_pattern(account)
      /#{account.reply_address.gsub('.', '\.').split('@').join("\\+nps(\\d+)@")}/
    end

    def to_account_nps_address?
      state.recipient =~ NPS_EMAIL_SUFFIX_PATTERN &&
        state.recipient.sub(NPS_EMAIL_SUFFIX_PATTERN, "@") == account.reply_address
    end
  end
end
