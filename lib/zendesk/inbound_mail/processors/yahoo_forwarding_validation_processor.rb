module Zendesk::InboundMail::Processors
  class YahooForwardingValidationProcessor < BaseProcessor
    YAHOO_FROM_ADDRESS = "no-reply@cc.yahoo-inc.com".freeze

    # Match links of type (old verification links)
    # https://edit.yahoo.com/commchannel/verify?.intl=us&p=vOv7O86oOAYfisU7Si.NCPFP.PbGB_b5oeuTh2SdDeuNJSVcdPF87VxNpRiQhUBccoAym1uovm5f83OWnf3rnx_VoCzQenPz.L4YKjTfSaYy9tpPGVhOtFhfbPTG3Egr1k29ouc3&.partner=
    CONFIRMATION_LINK_PATTERN = /https:\/\/edit\.yahoo\.com\/commchannel\/verify\?\.intl=\w+&p=\S+&\.partner=/

    # Match links of type (new verification links)
    # https://login.yahoo.com/account/action/verify?t=6EUMWhajIE9LbaruDGfTQ.NqYAnsvRC7SW0K8gMGGdkZ0QkrKy61ZYnHKM.wGBDHEev7lzWCV1s5ZlIO_c01VitDzjHPz7BlnxkNtHs8M3bpmA--&.done=https://login.yahoo.com/account/action/verify?t=6EUMWhajIE9LbaruDGfTQ.NqYAnsvRC7SW0K8gMGGdkZ0QkrKy61ZYnHKM.wGBDHEev7lzWCV1s5ZlIO_c01VitDzjHPz7BlnxkNtHs8M3bpmA--
    NEW_CONFIRMATION_LINK_PATTERN = /https:\/\/login\.yahoo\.com\/account\/action\/verify\?t=\w+\S+&\.done=https:\/\/login\.yahoo\.com\/account\/action\/verify\?t=\w+\S+/

    def log_and_increment_statsd(log_message, statsd_message)
      log(log_message)
      statsd_client.increment(:received_yahoo_verification_email, tags: [statsd_message])
    end

    def confirmation_link
      if Zendesk::MtcMpqMigration::Content::MIME.new(@mail).body.match(NEW_CONFIRMATION_LINK_PATTERN)
        "new"
      elsif Zendesk::MtcMpqMigration::Content::MIME.new(mail).body.match(CONFIRMATION_LINK_PATTERN)
        "old"
      else
        "other"
      end
    end

    def execute
      return unless state.from.address == YAHOO_FROM_ADDRESS

      log_and_increment_statsd("Received Yahoo forwarding verification message with confirmation link that matches the #{confirmation_link} regex", "confirmation_link:#{confirmation_link}")

      if account.has_email_yahoo_forwarding_validation_processor_new_regex?
        return if confirmation_link == "old"
      else
        return if confirmation_link == "new"
      end

      state.from = Zendesk::Mail::Address.new(name: "Zendesk Support", address: "noreply@zendesk.com")
      state.whitelisted = true

      log_and_increment_statsd("Changed `from` address of Yahoo forwarding verification message and allowed creation of ticket", "whitelisted:#{confirmation_link}")
    end
  end
end
