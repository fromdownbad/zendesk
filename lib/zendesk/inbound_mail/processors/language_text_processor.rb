require 'cld'

module Zendesk::InboundMail::Processors
  class LanguageTextProcessor < BaseProcessor
    CLD_UNKNOWN_LANGUAGE = 'un'.freeze # Unknown

    # https://support.zendesk.com/rosetta/interface
    CLD_CODE_TO_LOCALES = {
      'iw'      => 'he',    # Hebrew
      'nb'      => 'no',    # Norwegian
      'nn'      => 'no',    # Norwegian
      'zh-Hant' => 'zh-tw'  # Chinese (Traditional)
    }.freeze

    def execute
      if !state.author
        warn("No author detected, aborting language detection")
        return
      elsif state.author.persisted? && (locale_id = state.author.locale_id)
        log("Using author's locale: '#{locale_id}'")
        return
      end

      to_detect = extract_text_to_detect(state)
      language = CLD.detect_language(to_detect)
      log("Language detection result: #{language&.inspect} for '#{to_detect.truncate(200)}' (Sample size: #{to_detect.size})")

      override_cdl_locale_codes(language)

      if language[:reliable]
        log("Reliable detection found, setting language to '#{language[:code]}' code")
        set_cld_language(language[:code])
      elsif language[:code] != CLD_UNKNOWN_LANGUAGE # Z1-4817223
        log("Unreliable detection found (but allowed), setting language to '#{language[:code]}' code")
        set_cld_language(language[:code])
      else
        log("Unreliable detection found, aborting language detection")
      end
    end

    private

    # [Z1-1355942] Do a little extra work to remove URLs from the text before trying to detect language
    # lots of URLs can break detection since they are usually English characters.
    def extract_text_to_detect(state)
      log("Removing URLs from the text to avoid misinterpretation of English characters")
      to_detect = state.plain_content.split("[[classifier_delimiter]]")[0].to_s
      to_detect.gsub(ZendeskText::Regexps::VALID_URL) { |m| m.sub($3, '') }.strip # $3 is the full URL
    end

    def set_cld_language(detected_locale) # rubocop:disable Naming/AccessorMethodName
      # Presumably if language[:reliable] is true then language[:code] is non-nil
      locale = account.available_languages.find { |tl| tl.locale && tl.locale.casecmp(detected_locale).zero? }

      locale ||= prefer_default_locale(account.available_languages.find_all do |tl|
        detected_locale.split("-")[0].try(:casecmp, tl.locale.split("-")[0]).try(:zero?)
      end)

      unless locale
        log("Can't find '#{detected_locale}' locale in the account's available languages list")
        return
      end

      log("Setting user (#{state.from.address}) language to '#{locale}' locale")
      state.author.translation_locale = locale if state.author
      state.locale_detected = locale.id
    end

    def prefer_default_locale(locales)
      locales.find { |tl| tl == account.translation_locale } || locales.first
    end

    def override_cdl_locale_codes(language)
      if locale_from_cld = CLD_CODE_TO_LOCALES[language[:code]]
        log("Overriding locale from '#{language[:code]}' to '#{locale_from_cld}'")
        language[:code] = locale_from_cld
      end
    end
  end
end
