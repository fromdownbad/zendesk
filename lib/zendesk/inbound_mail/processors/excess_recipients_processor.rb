module Zendesk::InboundMail::Processors
  # Suspends new incoming email tickets where the number of Tos and CCs exceeds the account configured threshold.
  # This processor can move to MPQ once the rest of the processors ahead of it in the processing chain are moved over.
  class ExcessRecipientsProcessor < BaseProcessor
    def execute
      # Return if logging or normal Arturo is not turned on
      return if !account.has_email_ticket_email_ccs_suspension_threshold? || !account.has_email_ticket_email_ccs_suspension_threshold_logging?

      if mail.recipients.count > account.ticket_email_ccs_suspension_threshold

        # Log message if number of mail recipients exceed the account configured amount
        message = "Amount of CCs (#{mail.recipients.count}) on this ticket exceeds the account-configured limit of #{account.ticket_email_ccs_suspension_threshold}."
        log(message)

        # Return if ticket is already suspended, ticket author is an agent or if the account only has logging Arturo enabled
        return if state.suspended? || state.author.is_agent? || !account.has_email_ticket_email_ccs_suspension_threshold?

        if state.whitelisted?
          log("Whitelisted — not suspending due to exceeding configured recipients threshold")
        elsif !new_ticket?
          log("Updating existing ticket — not suspending due to exceeding configured recipients threshold")
        else
          suspend(message, SuspensionType.NUMBER_RECIPIENTS_EXCEEDS_THRESHOLD)
        end
      end
    end

    private

    def new_ticket?
      state.ticket.blank? && state.closed_ticket.blank?
    end
  end
end
