module Zendesk::InboundMail::MailParsingQueue
  # With email_no_delimiter roll-out the JSON produced by MPQ includes a `processing_state` key:
  #   {
  #     "id": "4f7e58ef-ac35-4fe1-8075-235755b37ea1",
  #     "message-id": ["<5cf19e34c97e4_92ade4fc165f84816@mail-parsing-queue.mail>"],
  #     ...
  #     "processing_state": {
  #       "from": {
  #         "name": "Mikkel",
  #         "address": "mikkel@example.com"
  #        },
  #       "content": {
  #         "html_content": "<html>truncated</html>",
  #         "full_html_content": "<html>full content</html>",
  #         "plain_content": "truncated",
  #         "full_plain_content": "full content"
  #       },
  #       "suspension": 24
  #     }
  #   }

  # Provides an way to extract MPQ state information directly from InboundMessage.
  class ProcessingState
    attr_accessor :metadata

    def initialize(processing_state)
      @metadata = processing_state.with_indifferent_access
    end

    # These methods should mirror MTC and MPQ ProcessingState Struct objects
    def from
      @from ||= if (from = metadata.fetch(:from, nil))
        Zendesk::Mail::Address.new.tap do |address|
          address.name = from[:name]
          address.address = from[:address]
          address.original_name = nil
        end
      end
    end

    def from_header
      @from_header ||= if (from_header = metadata.fetch(:from_header, nil))
        Zendesk::Mail::Address.new.tap do |address|
          address.name = from_header[:name]
          address.address = from_header[:address]
          address.original_name = nil
        end
      end
    end

    def suspension
      @suspension ||= metadata.fetch(:suspension, nil)
    end

    def html_content
      @html_content ||= metadata.dig(:content, :html_reply)
    end

    def plain_content
      @plain_content ||= metadata.dig(:content, :plain_reply)
    end

    def full_html_content
      @full_html_content ||= metadata.dig(:content, :html_full)
    end

    def full_plain_content
      @full_plain_content ||= metadata.dig(:content, :plain_full)
    end

    def quoted_html_content
      @quoted_html_content ||= metadata.dig(:content, :html_quoted)
    end

    def quoted_plain_content
      @quoted_plain_content ||= metadata.dig(:content, :plain_quoted)
    end
  end

  # Example usage:
  # mail = Zendesk::Mail::Serializers::InboundMessageSerializer.load(message_json)
  #
  # Can then be used the same as the MTC or MPQ ProcessingState Struct object:
  # mail.processing_state.from # returns Zendesk::Mail::ZendeskAddress
  def processing_state
    processing_state = params.fetch(:processing_state, {})
    @processing_state ||= Zendesk::InboundMail::MailParsingQueue::ProcessingState.new(processing_state)
  end
end
