module Zendesk::InboundMail
  class MailLogger
    attr_accessor :logger

    def initialize(logger, location: nil)
      @logger = logger
      @location = location || self
    end

    def log(message)
      logger.info(message)
    end

    alias_method :info, :log

    def warn(message, suffix = "warning")
      message = [message, suffix].compact.join(' -- ')
      logger.warn(message)
    end

    def error(message)
      ZendeskExceptions::Logger.record(StandardError.new(message), location: @location, message: message, reraise: !Rails.env.production?, fingerprint: message)
    end
  end
end
