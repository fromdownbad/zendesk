module Zendesk::InboundMail::BouncePatterns
  # Note that "Delivery Status Notification (Delay)" is not a bounce.
  UNDELIVERABLE_SUBJECTS = [
    /\AUndeliverable:/i,
    /\AUndelivered Mail/i,
    /\AMail Returned/i,
    /Delivery Failure/i,
    /failure notice/i,
    /Returned mail/i,
    /Undeliverable mail/i,
    /Mail delivery failed/i,
    /Delivery Status Notification \(Failure\)/i,
    /\ANDN:/,
  ].freeze

  SUBJECT_UNDELIVERABLE = Regexp.union(UNDELIVERABLE_SUBJECTS)

  REJECT_ONLY_SUBJECTS = [
    /Ip frequency limited/i
  ].freeze

  SUBJECT_REJECT_ONLY = Regexp.union(REJECT_ONLY_SUBJECTS)

  FROM_UNDELIVERABLE = /^(mailer-daemon|postmaster|failurenotice|internet|noreply|gateway|post\.office|sold\.noreply)@/i

  BOUNCED_EMAIL_ADDRESS = /\s*(#{FIND_EMAIL_PATTERN})\s*/

  # Example: `Final-Recipient: rfc822; recipient@gmail.com`
  FAILED_RECIPIENT_PATTERNS = [
    /An error occurred while trying to deliver the mail to the following recipients:/,
    /Final-Recipient: rfc822;/,
    /Delivery to the following recipient has been delayed:/,
    /Delivery to the following recipients failed\./,
    /Delivery to the following recipient failed permanently:/,
    /Unable to deliver message to the following recipients, due to being unable to connect successfully to the destination mail server\./,
    /The following address failed:\s*</,
    /Diagnostic information for administrators:\s*Generating server:\s*.+\s*/,
    /Recipient Address:\s*/,
    /The mail system\s*</
  ].freeze

  FAILED_RECIPIENT = /(#{Regexp.union(FAILED_RECIPIENT_PATTERNS)})#{BOUNCED_EMAIL_ADDRESS}/

  # IETF documentation of "Delivery Status Notification" file: https://tools.ietf.org/html/rfc3464
  # Full DSN file (Content-Type: message/delivery-status)
  #
  #   Reporting-MTA: dns; googlemail.com
  #   Arrival-Date: Mon, 20 Mar 2017 15:36:50 -0700 (PDT)
  #   X-Original-Message-ID: <CACGNpndTPhN+yA7=H2QjN2EDWM1xvR3cq9BducOxY+1afje2nA@mail.gmail.com>
  #
  #   Final-Recipient: rfc822; recipient@gmail.com
  #   Action: failed
  #   Status: 5.1.1
  #   Remote-MTA: dns; gmail-smtp-in.l.google.com. (2607:f8b0:400c:c0c::1b, the
  #    server for the domain gmail.com.)
  #   Diagnostic-Code: smtp; 550-5.1.1 The email account that you tried to reach does not exist. Please try
  #    550-5.1.1 double-checking the recipient's email address for typos or
  #    550-5.1.1 unnecessary spaces. Learn more at
  #    550 5.1.1  https://support.google.com/mail/?p=NoSuchUser t30si2927556uat.28 - gsmtp
  #   Last-Attempt-Date: Mon, 20 Mar 2017 15:36:51 -0700 (PDT)
  DSN_ATTACHMENT = /message\/delivery-status/i

  # DSN action. Example: `Action: failed`
  DSN_ACTION = /Action:/i

  # IETF documentation of status code structure and values: https://tools.ietf.org/html/rfc3463
  # Example: `Status: 5.1.1`
  STATUS_CODE = /(?<class>[245])\.(?<subjects>[0-7]{1,3})\.[0-8]{1,3}/

  # Example1:
  #   450-4.2.1 The user you are trying to contact is receiving mail at a rate that
  #   450-4.2.1 prevents additional messages from being delivered. Please resend your
  #   450-4.2.1 message at a later time. If the user is able to receive mail at that
  #   450-4.2.1 time, your message will be delivered. For more information, please
  #   450-4.2.1 visit
  #   450 4.2.1  https://support.google.com/mail/?p=ReceivingRate e64si1652721ybi.316 - gsmtp

  # Example (from email body)
  #   The response from the remote server was:
  #   550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces. Learn more at https://support.google.com/mail/?p=NoSuchUser t30si2927556uat.28 - gsmtp

  # Example (from DSN_ATTACHMENT)
  #   Diagnostic-Code: smtp; 550-5.1.1 The email account that you tried to reach does not exist. Please try
  #    550-5.1.1 double-checking the recipient's email address for typos or
  #    550-5.1.1 unnecessary spaces. Learn more at
  #    550 5.1.1  https://support.google.com/mail/?p=NoSuchUser t30si2927556uat.28 - gsmtp
  STATUS_TEXT = [
    /\d{3}.+[245]\.[0-7]{1,3}\.[0-8]{1,3}.*$/,
    /Diagnostic-Code:.*/i
  ].freeze

  # Gets the Status Code "real meaning"
  BOUNCE_DESCRIPTION = Regexp.union(STATUS_TEXT)

  STATUS_PATTERNS = [
    /Status:/,
    /The error that the other server returned was:\s*\d{3}/,
    /Remote Server\s*returned '\d{3}/,
    /The following address failed:\s*<#{FIND_EMAIL_PATTERN}>:\s*\d{3}/,
    /Diagnostic information for administrators:\s*Generating server:\s*.+\s*#{FIND_EMAIL_PATTERN}\s*#\d{3}-?/,
    /said: \d{3}-/
  ].freeze

  STATUS = /(#{Regexp.union(STATUS_PATTERNS)})\s*(?<code>#{STATUS_CODE})/

  ZENDESK_MAILER = /X-Mailer:\s*Zendesk Mailer/

  ZENDESK_WELCOME_MAIL = /X-Delivery-Context:\s*welcome\-owner\-[0-9]+/
end
