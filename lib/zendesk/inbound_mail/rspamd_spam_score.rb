module Zendesk
  module InboundMail
    class RspamdSpamScore
      HEADER_KEY = 'X-Rspamd-Status'.freeze
      REGEX = /^(Yes|No),/
      HAM = 'ham'.freeze
      SPAM = 'spam'.freeze

      attr_reader :mail

      def initialize(mail)
        @mail = mail
      end

      def ham?
        @ham ||= (value == 'No' || value.nil?)
      end

      def spam?
        @spam ||= (value == 'Yes')
      end

      def classification
        @classification ||= ham? ? HAM : SPAM
      end

      protected

      def value
        @value ||= header_value =~ REGEX && Regexp.last_match(1)
      end

      def header_value
        mail.headers[HEADER_KEY].try(:first)
      end
    end
  end
end
