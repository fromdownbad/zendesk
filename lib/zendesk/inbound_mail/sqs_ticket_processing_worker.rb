module Zendesk::InboundMail
  class SQSTicketProcessingWorker
    include TicketProcessingWorkerShared

    MAX_RECEIVES = 8 # should be equal to the "Maximum Receives" setting for the redrive policy

    attr_reader :item

    def initialize(item)
      @item = item
    end

    def report_to(boss)
      @boss = boss
    end

    def perform
      # Bail if we've retried this email the max number of times already
      Zendesk::Mail.logger.info("SQS message receive count: #{item.receive_count}")
      if item.receive_count >= MAX_RECEIVES
        work_done(SQSMaxReceivesError.new)
        return
      end

      exception = nil

      begin
        work
      rescue Exception => e # rubocop:disable Lint/RescueException
        exception = e
      end

      work_done(exception)
    end

    def json_to_parse
      item.value
    end

    def work_done(exception)
      @boss.work_done(item, self, exception) if @boss
    end
  end
end
