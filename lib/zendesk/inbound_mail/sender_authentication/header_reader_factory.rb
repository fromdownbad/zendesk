module Zendesk::InboundMail::SenderAuthentication
  class HeaderReaderFactory
    SOURCE_HEADER = 'X-Zendesk-Source'.freeze

    def self.create(mail)
      new(mail).create
    end

    def initialize(mail)
      @mail = mail
    end

    def create
      header_reader.new(@mail)
    end

    private

    def header_reader
      if authentication_results_header_usable?
        HeaderReaders::AuthenticationResults
      elsif rspamd_authentication_results_header_usable?
        HeaderReaders::XRspamdAuthenticationResults
      else
        HeaderReaders::MissingAuthentication
      end
    end

    def authentication_results_header_usable?
      source_mailfetcher? && @mail.headers[HeaderReaders::AuthenticationResults::HEADER].present?
    end

    def rspamd_authentication_results_header_usable?
      @mail.headers[HeaderReaders::XRspamdAuthenticationResults::HEADER].present?
    end

    def source_mailfetcher?
      if @mail.headers[SOURCE_HEADER].present? && @mail.headers[SOURCE_HEADER].include?('1')
        @mail.logger.info('Mail imported by Mail Fetcher')
        true
      else
        false
      end
    end
  end
end
