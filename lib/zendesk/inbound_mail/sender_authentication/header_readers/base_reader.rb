module Zendesk::InboundMail::SenderAuthentication::HeaderReaders
  class BaseReader
    FROM_HEADER = 'From'.freeze
    REPLY_TO_HEADER = 'Reply-To'.freeze
    DKIM_SIGNATURE_HEADER = 'Dkim-Signature'.freeze

    def initialize(mail)
      @mail = mail
    end

    def header
      defined?(self.class::HEADER) ? self.class::HEADER : nil
    end

    def values
      @values ||= @mail.headers[header] || []
    end

    def spf_alignment
      envelope_from_domain == from_domain ? :aligned : :unaligned
    end

    def dkim_alignment
      return :dkim_none unless dkim_signatures.present?
      dkim_matches_header_from ? :aligned : :unaligned
    end

    def dmarc_alignment
      if [dkim_alignment, spf_alignment].include? :aligned
        :aligned
      else
        :unaligned
      end
    end

    def dmarc_fail_and_reject?
      false
    end

    def dmarc
      :dmarc_missing
    end

    def dmarc_policy
      :dmarc_policy_missing
    end

    def skip?
      false
    end

    private

    def envelope_from_domain
      @envelope_from_domain ||= begin
        domain = @mail.smtp_envelope_from.match(Zendesk::InboundMail::Patterns::ENVELOPE_DOMAIN_PATTERN)
        domain[1] if domain
      end
    end

    def from_domain
      @from_domain ||= begin
        address = @mail.headers[FROM_HEADER] || @mail.headers[REPLY_TO_HEADER]
        address.try(:first).try(:domain)
      end
    end

    def dkim_signatures
      @dkim_signatures = @mail.headers[DKIM_SIGNATURE_HEADER] || []
    end

    def dkim_matches_header_from
      @dkim_matches_header_from ||= begin
        dkim_signatures.any? do |signature|
          signature["d=#{from_domain}\;"]
        end
      end
    end

    def values_containing(pattern)
      values.flat_map { |value| value.scan(pattern).flatten }.uniq.compact
    end
  end
end
