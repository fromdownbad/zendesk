module Zendesk::InboundMail::SenderAuthentication::HeaderReaders
  class AuthenticationResults < BaseReader
    HEADER = "Authentication-Results".freeze
    DKIM_VALUE_PATTERN = /mx.google.com; .*dkim=(\w*)/
    SPF_VALUE_PATTERN = /mx.google.com; .*spf=(\w*)/
    DKIM_NONE_VALUES = %w[none temperror permerror].freeze
    DKIM_VALID_VALUES = %w[pass].freeze
    SPF_NONE_VALUES = %w[none neutral softfail temperror permerror].freeze
    SPF_VALID_VALUES = %w[pass].freeze

    def dkim_status
      return :dkim_none unless dkim_present?
      return :dkim_pass if dkim_valid?
      :dkim_fail
    end

    def spf_status
      return :spf_none unless spf_present?
      return :spf_pass if spf_passed?
      :spf_fail
    end

    private

    def dkim_present?
      @dkim_present ||= dkim_values.any? && (DKIM_NONE_VALUES & dkim_values).empty?
    end

    def dkim_valid?
      @dkim_valid ||= dkim_present? && (DKIM_VALID_VALUES & dkim_values).present?
    end

    def spf_present?
      @spf_present ||= spf_values.any? && (SPF_NONE_VALUES & spf_values).empty?
    end

    def spf_passed?
      @spf_passed ||= spf_present? && (SPF_VALID_VALUES & spf_values).present?
    end

    def dkim_values
      @dkim_values ||= values_containing(DKIM_VALUE_PATTERN)
    end

    def spf_values
      @spf_values ||= values_containing(SPF_VALUE_PATTERN)
    end
  end
end
