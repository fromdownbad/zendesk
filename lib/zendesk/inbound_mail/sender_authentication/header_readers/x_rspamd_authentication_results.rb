module Zendesk::InboundMail::SenderAuthentication::HeaderReaders
  class XRspamdAuthenticationResults < BaseReader
    HEADER                  = "X-Rspamd-Authentication-Results".freeze
    RSPAMD_HEADER           = "X-Rspamd-Status".freeze
    DKIM_VALUE_PATTERN      = /[\.\w]+; .*dkim=(\w*)/
    SPF_VALUE_PATTERN       = /[\.\w]+; .*spf=(\w*)/
    DMARC_VALUE_PATTERN     = /[\.\w]+; .*dmarc=(\w*)/
    # Set DMARC_POLICY_PATTERN equal to the value of DMARC_POLICY_PATTERN_V2
    # when removing the :email_mtc_sender_auth_dmarc_v2 Arturo
    DMARC_POLICY_PATTERN    = /[\.\w]+; .*dmarc=\w*.+\(policy=(\w*)\);/
    DMARC_POLICY_PATTERN_V2 = /[\.\w]+; .*dmarc=\w*.+\(policy=(\w*)\)/
    DMARC_REJECT_VALUES     = %w[quarantine reject].freeze
    RSPAMD_REPLY_PATTERN    = /;\sREPLY\([0-9\.\-]+\)\[\]/
    DELIVERED_TO_HEADER     = 'Delivered-To'.freeze
    RECIPIENT_HEADERS       = %w[To Cc].freeze
    DKIM_NONE_VALUES        = %w[none temperror permerror].freeze
    DKIM_VALID_VALUES       = %w[pass].freeze
    SPF_NONE_VALUES         = %w[none neutral softfail temperror permerror].freeze
    SPF_ERROR_VALUES        = %w[temperror permerror].freeze
    SPF_VALID_VALUES        = %w[pass].freeze
    FAILED_VALUES           = %w[fail].freeze

    def dkim_status
      return :dkim_none unless dkim_present?
      return :dkim_pass if dkim_valid?
      return :dkim_forwarded if forwarded?
      :dkim_fail
    end

    def spf_status
      return :spf_none if spf_error?
      return :spf_none unless spf_present?
      return :spf_pass if spf_passed?
      return :spf_forwarded if forwarded?
      :spf_fail
    end

    def skip?
      rspamd_reply_present?
    end

    def dmarc_fail_and_reject?
      return false unless dmarc_values
      return false unless (dmarc_values & FAILED_VALUES).present?

      (dmarc_policy_values & DMARC_REJECT_VALUES).present?
    end

    def dmarc
      if !dmarc_values.empty?
        "dmarc_#{dmarc_values.first}".to_sym
      else
        :dmarc_missing
      end
    end

    def dmarc_policy
      if !dmarc_policy_values.empty?
        "dmarc_policy_#{dmarc_policy_values.first}".to_sym
      else
        :dmarc_policy_missing
      end
    end

    private

    def dkim_present?
      @dkim_present ||= dkim_values.any? && (DKIM_NONE_VALUES & dkim_values).empty?
    end

    def dkim_valid?
      @dkim_valid ||= dkim_present? && (DKIM_VALID_VALUES & dkim_values).present?
    end

    def rspamd_reply_present?
      @rspamd_reply_present ||= x_rspamd_status.grep(RSPAMD_REPLY_PATTERN).any?
    end

    def spf_present?
      @spf_present ||= spf_values.any? && (SPF_NONE_VALUES & spf_values).empty?
    end

    def spf_error?
      @spf_error ||= begin
        errors = SPF_ERROR_VALUES & spf_values
        errors.each { |error| @mail.statsd_client.increment(:spf_error, tags: ["error:#{error}"]) }
        errors.any?
      end
    end

    def spf_passed?
      @spf_passed ||= spf_present? && (SPF_VALID_VALUES & spf_values).present?
    end

    def dkim_values
      @dkim_values ||= values_containing(DKIM_VALUE_PATTERN)
    end

    def spf_values
      @spf_values ||= values_containing(SPF_VALUE_PATTERN)
    end

    def dmarc_values
      @dmarc_values ||= values_containing(DMARC_VALUE_PATTERN)
    end

    def dmarc_policy_values
      # Replace the dmarc_policy_pattern Arturo with DMARC_POLICY_PATTERN when
      # removing the :email_mtc_sender_auth_dmarc_v2 Arturo
      @dmarc_policy_values ||= values_containing(dmarc_policy_pattern)
    end

    # Remove this method entirely when removing the
    # :email_mtc_sender_auth_dmarc_v2 Arturo
    def dmarc_policy_pattern
      @dmarc_policy_pattern ||= if @mail.account.has_email_mtc_sender_auth_dmarc_v2?
        DMARC_POLICY_PATTERN_V2
      else
        DMARC_POLICY_PATTERN
      end
    end

    def forwarded?
      @forwarded ||= if @mail.account.has_email_sender_auth_no_recipient_fix?
        recipients.present? && (delivered_to - recipients).present?
      else
        (delivered_to - recipients).present?
      end
    end

    def recipients
      @mail.headers.values_at(*RECIPIENT_HEADERS).flatten.compact
    end

    def delivered_to
      @mail.headers.values_at(DELIVERED_TO_HEADER).flatten.compact
    end

    def x_rspamd_status
      @mail.headers.values_at(RSPAMD_HEADER).flatten.compact
    end
  end
end
