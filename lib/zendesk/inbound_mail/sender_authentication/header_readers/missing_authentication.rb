module Zendesk::InboundMail::SenderAuthentication::HeaderReaders
  class MissingAuthentication < BaseReader
    def dkim_status
      :dkim_none
    end

    def spf_status
      :spf_none
    end
  end
end
