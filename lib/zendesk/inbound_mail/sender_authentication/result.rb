module Zendesk::InboundMail::SenderAuthentication
  class Result
    attr_accessor :action, :origin
    attr_reader :dkim_status, :spf_alignment, :spf_status, :dmarc_status, :sender_rule

    def initialize(result, query, sender_rule)
      @action, @meta = result
      @spf_status, @dkim_status, @spf_alignment = query
      @sender_rule = sender_rule.to_sym
    end

    def pass?
      action == :pass
    end

    def flag?
      @meta == :flag
    end

    def audit?
      @meta == :audit
    end

    def suspend?
      action == :suspend
    end

    def reject?
      action == :reject
    end

    def to_a
      @to_a ||= [origin, spf_status, dkim_status, spf_alignment]
    end
  end
end
