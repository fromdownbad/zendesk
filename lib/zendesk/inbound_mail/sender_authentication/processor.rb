module Zendesk::InboundMail::SenderAuthentication
  class Processor
    OUT_HOST_PATTERN = /out\d+\.pod\d+\.[a-z]+\d+\.zdsys(test)?\.com/

    def self.call(header_reader, mail)
      new(header_reader, mail).run
    end

    def initialize(header_reader, mail)
      @header_reader = header_reader
      @mail = mail
    end

    def run
      result = authentication_matrix.lookup
      if @mail.account.has_email_sender_auth_skip_rspamd_reply?
        result.action = :pass if skip_authentication?
      end
      result.action = :pass if result.reject? && mail_from_zendesk?
      result.origin = origin
      result
    end

    private

    def authentication_matrix
      @authentication_matrix ||= Matrix.new(@header_reader, @mail.sender.domain, @mail.account)
    end

    def origin
      zendesk_origin? ? :from_zendesk : :not_from_zendesk
    end

    def zendesk_origin?
      @mail.from_zendesk_address? || mail_from_zendesk?
    end

    # This is only used in the case where the email appears from zendesk.com (based on smtp_envelope_from) but it
    # still failed both SPF and DKIM. We'll look at the Message ID and Received headers and trust that the email
    # is from Zendesk if the message ID matches our known outbound patterns, and we find the zendesk domains and
    # hosts as expected in the earliest of the Received headers.
    def mail_from_zendesk?
      return false unless mail_has_zendesk_message_id?
      return false unless received_headers_usable?
      received_headers_contain_zendesk_out_host?
    end

    def mail_has_zendesk_message_id?
      return true if @mail.zendesk_message_id?
      @mail.logger.info("Message-ID does not match known outbound Zendesk message ID patterns")
      false
    end

    def received_headers_usable?
      received_headers.present? && received_headers.size > 1
    end

    def received_headers
      @received_headers ||= @mail.headers["Received"]
    end

    # Earliest or next earliest Received header must contain an out host such as "out8.pod5.iad1.zdsys.com"
    def received_headers_contain_zendesk_out_host?
      contains_out_host = contains_out_host?(received_headers.last) || contains_out_host?(received_headers[-2])
      @mail.logger.info("SPF/DKIM failed but headers indicate Zendesk-sent email - will not reject") if contains_out_host
      contains_out_host
    end

    def skip_authentication?
      if @header_reader.skip?
        @mail.logger.info('Ignore X-Rspamd-Authentication-Results - Rspamd REPLY symbol present')
        true
      else
        false
      end
    end

    def contains_out_host?(header)
      !!(header =~ Zendesk::InboundMail::Patterns::OUT_HOST_PATTERN)
    end
  end
end
