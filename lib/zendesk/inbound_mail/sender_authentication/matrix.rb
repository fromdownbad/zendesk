module Zendesk::InboundMail::SenderAuthentication
  class Matrix
    def self.senders
      @senders ||= YAML.load_file(Rails.root.join('config/sender_authentication/senders.yml')).with_indifferent_access
    end

    def initialize(header_reader, sender_domain, account)
      @header_reader = header_reader
      @sender_domain = sender_domain
      @account = account
    end

    def lookup
      Result.new(result, query, sender_rule)
    end

    def matrix
      @matrix ||= YAML.load_file(Rails.root.join("config/sender_authentication/#{@header_reader.class.name.demodulize.underscore}.yml")).with_indifferent_access
    end

    private

    def result
      matrix.fetch(sender_rule).fetch(query)
    end

    def query
      @query ||= [@header_reader.spf_status, @header_reader.dkim_status, @header_reader.spf_alignment]
    end

    def sender_rule
      return :default if sender_rules.nil?

      if sender_rule_enabled?
        sender_rules[@header_reader.class.name.demodulize.underscore] || :default
      else
        :default
      end
    end

    def sender_rules
      @sender_rules ||= self.class.senders.select do |host, _|
        @sender_domain.match?(/(\.|^)#{Regexp.quote(host)}$/)
      end.values.first
    end

    def sender_rule_enabled?
      return true unless sender_rules.key?('_arturo')
      @account.arturo_enabled? sender_rules['_arturo']
    end
  end
end
