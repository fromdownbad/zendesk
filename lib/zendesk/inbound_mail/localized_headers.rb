module Zendesk::InboundMail::LocalizedHeaders
  FROM_KEYS = begin
    /(?:From              # English
       |Feladó            # Hungarian - ZD#4462131
       |Fra               # Norwegian, Danish
       |Frá               # Icelandic
       |Från              # Swedish
       |Von               # German
       |De                # French, Portuguese, Spanish
       |Da                # Italian
       |Od                # Czech, Polish
       |寄件人             # Traditional Chinese
       |发件人             # Simplified Chinese
       |מאת               # Hebrew
       |Lähettäjä         # Finnish - ZD#482513
       |(?-x:От кого)     # Russian - ZD#552849
       |Van               # Dutch
       |送信者             # Japanese
       |差出人             # Japanese
       |Kimden            # Turkish
       |Dari              # Indonesian
       |จาก               # Thai
       |(?-x:보낸 사람)     # Korean
       |द्वारा               # Hindi
    )/xi # /xi White-space and case insensitive.
  end

  # ISO 639 codes — http://xml.coverpages.org/iso639a.html
  # en-U   (English)
  # fr     (Français)
  # ru     (Русский, Russian)
  # fi     (Suomi, Finnish)
  # pl     (Polski, Polish)
  # hu     (Hungarian)
  # it     (Italiano)
  # de     (Deutsch)
  # de-at  (Deutsch, Austria)
  # es     (Español)
  # es-419 (Español, Latinoamérica)
  # es-es  (Español, España)
  # pt     (Português, Portugal)
  # pt-BR  (Português, Brasil)
  # ja     (日本語, Japanese)
  # ko     (한국어,  Korean)
  # zh-TW  (繁體中文, Traditional Chinese)
  # zh-CN  (简体中文, Simplified Chinese)
  # nl     (Nederlands, Dutch)
  # tr     (Türkçe)
  # da     (Dansk)
  # no     (Norsk)
  # sv     (Svenska)
  # he     (עִבְרִית, Hebrew)
  # id     (Indonesian)
  # th     (Thai, ไทย)
  # hi     (हिंदी, Hindi)
  # cs     (Čeština, Czech)

  # SUPPORTED_MAIL_LOCALES and V2 patterns are not currently being used - being preserved here so we don't lose all the i18n work.
  SUPPORTED_MAIL_LOCALES = %w[en-US fr ru fi pl hu it de de-at es es-419 es-es pt pt-BR ja ko zh-TW zh-CN nl tr da no sv he id th hi cs].freeze

  TO_KEYS_V2           = /(?:#{SUPPORTED_MAIL_LOCALES.map { |locale| I18n.t('inbound_mail.localized_headers.to', locale: locale) }.join('|')})/xi
  FROM_KEYS_V2         = /(?:#{SUPPORTED_MAIL_LOCALES.map { |locale| I18n.t('inbound_mail.localized_headers.from', locale: locale) }.join('|')})/xi
  DATE_KEYS_V2         = /(?:#{SUPPORTED_MAIL_LOCALES.map { |locale| I18n.t('inbound_mail.localized_headers.date', locale: locale) }.join('|')})/xi
  SENT_KEYS_V2         = /(?:#{SUPPORTED_MAIL_LOCALES.map { |locale| I18n.t('inbound_mail.localized_headers.sent', locale: locale) }.join('|')})/xi
  SUBJECT_KEYS_V2      = /(?:#{SUPPORTED_MAIL_LOCALES.map { |locale| I18n.t('inbound_mail.localized_headers.subject', locale: locale) }.join('|')})/xi
  IMPORTANCE_KEYS_V2   = /(?:#{SUPPORTED_MAIL_LOCALES.map { |locale| I18n.t('inbound_mail.localized_headers.importance', locale: locale) }.join('|')})/xi
  ORIGINAL_MSG_KEYS_V2 = /(?:#{SUPPORTED_MAIL_LOCALES.map { |locale| I18n.t('inbound_mail.localized_headers.original_message', locale: locale) }.join('|')})/xi

  ORIGINAL_MSG_KEYS = begin
    /(?:Original\sMessage # English
      |Mensaje\sOriginal  # Spanish
    )/xi
  end

  DATE_KEYS = begin
    /(?:Date             # English
      |Fecha             # Spanish
    )/xi
  end

  SENT_KEYS = begin
    /(?:Sent             # English
      |Gesendet          # German
      |Enviado           # Spanish
    )/xi
  end

  TO_KEYS = begin
    /(?:To                # English
      |An                 # German
      |Para               # Spanish
      |A                  # Spanish (Mexico)
    )/xi
  end

  SUBJECT_KEYS = begin
    /(?:Subject           # English
      |Betreff            # German
      |Ämne               # Swedish
      |Asunto             # Spanish
    )/xi
  end

  IMPORTANCE_KEYS = begin
    /(?:Importance        # English
    )/xi
  end

  # English and Chinese (https://en.wikipedia.org/wiki/Chinese_punctuation) colons
  COLON = /[:：]/
end
