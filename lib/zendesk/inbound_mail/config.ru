require_relative '../../../config/environment'

ENV['UNICORN_WORKERS'] = '1'

use ZendeskHealthCheck::Endpoint::Diagnostic
run Zendesk::InboundMail::Website.new
