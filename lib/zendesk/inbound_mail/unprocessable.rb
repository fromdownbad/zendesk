module Zendesk
  module InboundMail
    class Unprocessable
      def self.create(account, mail, suspension = SuspensionType.UNPROCESSABLE_EMAIL)
        new(account, mail, suspension).create
      end

      delegate :logger, to: :mail

      attr_reader :account, :mail

      def initialize(account, mail, suspension)
        @account = account
        @mail = mail
        @suspension = suspension
      end

      def create
        unless account
          logger.warn("Ignoring unprocessable for unidentified account. Email: #{mail.id}")
          return
        end

        account.on_shard do
          record_exceptions do
            suspended.save!
            logger.warn("Registered unprocessable email: #{mail.id}")
            mark_email_record_as_unprocessable
            suspended
          end
        end
      end

      protected

      def record_exceptions
        yield
      rescue StandardError => e
        logger.error("Failed to register unprocessable email #{e.class.name}: #{e.message}\n#{e.backtrace.join("\n")}")
        if suspended.attachments.any?(&:invalid?)
          original_email = suspended.attachments.first
          logger.error("Unprocessable email size: #{original_email.size}; Errors: #{original_email.errors.inspect}")
          Zendesk::InboundMail::StatsD.client.increment(:unprocessable_email_over_size_limit)
          Zendesk::InboundMail::StatsD.client.histogram(:unprocessable_email_size, original_email.size)
        else
          ZendeskExceptions::Logger.record(e, location: self, message: "Failed while registering unprocessable email #{mail.message_id}", fingerprint: '0983c404ce9d3e9342db39bad1f16af5a83d78e9')
          raise
        end
      end

      def mark_email_record_as_unprocessable
        email_record && email_record.mark_by_system!(suspension)
      end

      def email_record
        @email_record ||= account.inbound_emails.where(message_id: mail.message_id).first
      end

      def suspended
        localized_subject = ::I18n.t("txt.suspended_ticket.subject_#{suspension_name}")
        localized_content = ::I18n.t("txt.suspended_ticket.content_#{suspension_name}")

        @suspended ||= account.suspended_tickets.new do |s|
          s.account = account
          s.cause = suspension
          s.subject = localized_subject || "Email processing error."
          s.content = localized_content || "Zendesk could not process the attached email. We are investigating."
          s.from_mail = mail.reply_to.address
          s.from_name = mail.reply_to.name || Zendesk::InboundMail::Ticketing::MailTicket::UNKNOWN
          s.message_id = mail.message_id
          s.properties = {}

          if mail.eml_url
            attach_remote_eml_file(s)
          elsif account.settings.credit_card_redaction?
            mail.logger.info("Cannot attach eml, credit card redaction enabled (eml was redacted)")
          else
            mail.logger.warn("Cannot attach eml, eml_url missing for unknown reason")
          end
        end
      end

      def suspension
        @suspension ||= SuspensionType.UNPROCESSABLE_EMAIL
      end

      def suspension_name
        SuspensionType[suspension].name
      end

      private

      def attach_remote_eml_file(suspended_ticket)
        attachment_model = suspended_ticket.attachments.build(
          account: account,
          content_type: Mime[:eml].to_s,
          filename: "unprocessable-email-#{Time.now.strftime('%Y-%m-%d-%H%M')}.eml",
          source: suspended_ticket
        )
        mail.logger.info("Creating raw mail attachment model from remote file #{mail.eml_url}")
        attachment_model.copy_from_cloud(mail.eml_url)
      end
    end
  end
end
