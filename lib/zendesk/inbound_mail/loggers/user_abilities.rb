module Zendesk::InboundMail::Loggers
  class UserAbilities
    attr_reader :logger, :ticket, :user

    delegate :log, to: :logger

    def initialize(logger, ticket, user)
      @logger = logger
      @ticket = ticket
      @user = user
    end

    def log_user_can_add_comment
      can_add_comment = edit?
      log("User #{can_add_comment ? 'can' : 'cannot'} add comments to this ticket.")
    end

    private

    def edit?
      if ticket.new_record?
        log('User can edit ticket because it is a new record')
        true
      elsif user.is_agent?
        # Agents may edit any ticket that they can see.
        can_view = view?
        log("User is agent and #{can_view ? 'can' : 'cannot'} view the ticket")
        can_view
      elsif user.foreign?
        # perhaps should become: ticket.shared_tickets.
        log('User is foreign?')
        true
      elsif ticket.requester_id == user.id
        # Non-agents may edit tickets that they have requested.
        log('User is the requester')
        true
      elsif ticket.organization && ticket.organization.is_shared_comments? && user.organizations.include?(ticket.organization)
        # May edit if belongs to same org and org has shared_comments enabled?
        log("Ticket is in the user's organization")
        true
      elsif ticket.is_collaborator?(user)
        # Followers and CCs are cool too.
        log('User is a collaborator')
        true
      end
    end

    def view?
      if user.is_admin?
        # Admins may view all tickets
        log('User is admin')
        true
      elsif ticket.requester_id == user.id || ticket.requester_id_was == user.id
        # Requesters may always view their tickets.
        log('User is or was the requester')
        true
      elsif user.is_agent? && user.agent_restriction?(:none)
        log('User is an unrestricted agent')
        true
      elsif ticket.is_collaborator?(user)
        # Users may always view tickets that they are following or already CC'd on.
        log('User is a collaborator.')
        true
      elsif user.is_agent? && user.agent_restriction?(:assigned)
        is_or_was_assigned = ticket.assignee_id && ((user.id == ticket.assignee_id) || (user.id == ticket.assignee_id_was))
        log("Restricted agent is restricted to assigned tickets and the ticket #{is_or_was_assigned ? 'is or was' : 'is not'} assigned to the user.")
        is_or_was_assigned
      elsif user.is_agent? && user.agent_restriction?(:groups)
        if (user_in_ticket_group = user.in_group?(ticket.group_id))
          log('Restricted agent in group that ticket is assigned to.')
        end
        if (user_was_in_group_can_assign_to_any = user.abilities.defer?(:assign_to_any_group, ticket) &&
          user.in_group?(ticket.group_id_was))
          log('Restricted agent was in group and can :assign_to_any_group.')
        end
        user_in_ticket_group || user_was_in_group_can_assign_to_any
      elsif user.is_agent? && user.agent_restriction?(:organization)
        user_in_ticket_org = ticket.organization_id && user.has_organizations?(ticket.organization_id)
        log("Restricted agent #{user_in_ticket_org ? 'is' : 'is not'} a part of the organization the ticket is assigned to.")
        user_in_ticket_org
      else
        can_view_ticket_org = ticket.organization && user.abilities.defer?(:view, ticket.organization)
        log("User #{can_view_ticket_org ? 'can' : 'cannot'} view tickets from organization this ticket is assigned to")
        can_view_ticket_org
      end
    end
  end
end
