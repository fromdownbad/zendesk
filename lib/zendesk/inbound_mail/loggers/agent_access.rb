module Zendesk::InboundMail::Loggers
  class AgentAccess
    attr_reader :logger, :ticket

    delegate :log, :warn, to: :logger

    def initialize(logger, ticket)
      @logger = logger
      @ticket = ticket
    end

    def log_edit_access
      author = ticket.comment.author
      return unless author.is_agent?

      # When a comment is flagged with OTHER_USER_UPDATE, REGISTERED_USER_LOGGED_OUT, AGENT_NOT_IN_GROUP, or
      # AGENT_ASSIGNED_ONLY, only a small subset of ticket attributes may be changed, if these attributes are the only
      # ones being changed and one of the above flags are present, edit access will be granted.
      flagged_attribute_whitelist = Access::Validations::TicketValidator::FLAGGED_ATTRIBUTE_WHITELIST
      all_changed_attributes_allowed = ticket.changed.all? { |change| flagged_attribute_whitelist.include?(change) }
      flagged_update_access = ticket.flags_allow_comment? && all_changed_attributes_allowed
      if flagged_update_access
        log("Flagged update access granted")
      elsif ticket.flags_allow_comment?
        log("Flagged update access denied, disallowed changed attributes: #{ticket.changed - flagged_attribute_whitelist}")
      else
        log("Flagged update access denied, ticket is not flagged with an AuditFlagType that allows comments: #{ticket.try(:audit).try(:flags)}")
      end

      # Light agents can modify a larger subset of ticket attributes, logic in allowed_light_agent_attributes_changed?
      restricted_agent = author.is_agent? && !author.can?(:edit, @ticket)
      restricted_agent_edit_access = restricted_agent && allowed_attributes_changed?
      log("restricted agent edit access #{restricted_agent_edit_access ? 'granted' : 'denied'}#{restricted_agent ? '' : ' (no restrictions)'}")

      if ticket.attributes_changed? && !(author.can?(:edit, ticket) || restricted_agent_edit_access || flagged_update_access)
        warn("Saving ticket/comment will fail because the author can't edit the ticket/make public comments and " \
             "doesn't have restricted agent edit access or flagged update access.")
      else
        log("Saving update from #{restricted_agent ? 'restricted ' : ''}agent should be successful.")
      end
    end

    def allowed_attributes_changed?
      changes = ticket.changed - Access::Validations::TicketValidator::RESTRICTED_AGENT_ATTRIBUTE_WHITELIST
      unless changes.empty?
        log("Disallowed light agent changes to: #{changes.inspect}")
        return false
      end

      # ZD#358649 lotus is sorting ccs alphabetically
      if ticket.changed.include?("current_collaborators")
        ccs_were  = normalize_collaborators(ticket.current_collaborators_was)
        ccs_are   = normalize_collaborators(ticket.current_collaborators)
        ccs_equal = ccs_were == ccs_are
        log("Disallowed light agent changes to collaborators from: #{ccs_were.inspect} to: #{ccs_are.inspect}") unless ccs_equal
        return ccs_equal
      end

      # light agents can only change the requester if they are the current requester
      if ticket.changed.include?("requester_id")
        original_requester = ticket.delta_changes["requester_id"].first
        requesters_equal = original_requester == ticket.comment.author.id
        log("Disallowed light agent changes to: #{ticket.changed.inspect}") unless requesters_equal
        return requesters_equal
      end

      false
    end

    private

    def normalize_collaborators(value)
      value.to_s.scan(/<([^>]*)>/).sort
    end
  end
end
