require 'adrian'

module Zendesk::InboundMail
  class AdrianTicketProcessingWorker < Adrian::Worker
    include TicketProcessingWorkerShared

    def json_to_parse
      File.read(item.path)
    end
  end
end
