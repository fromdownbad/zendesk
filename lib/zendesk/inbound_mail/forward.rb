module Zendesk
  module InboundMail
    # Detects forwards and provides the original subject, content and sender.
    # Supported forward patterns are detailed in forward_header.rb
    class Forward
      # Fwd: Help!
      # => Help!
      SUBJECT_PATTERN = begin
        /^               # Must begin with
          (?:\[|\s)*     # Optional square bracket or spaces
          (?:FWD|FW      # The word Fwd or Fw, or their equivalents
           |Doorst       # Doorsturen (Dutch)
           |ENC          # Encaminhado (Portugese)
           |FS           # Framsenda (Icelandic)
           |I            # Inoltro (Italian)
           |İLT          # İlet (Turkish)
           |PD           # Podaj dalej (Polish)
           |RV           # Reenviar (Spanish)
           |Továbbítás   # Továbbítás (Hungarian)
           |TR           # Transfert (French)
           |TRS          # Terusan (Indonesian)
           |VB           # Vidarebefordrat (Swedish)
           |VL           # Välitetty (Finnish)
           |VS           # Videresendt (Danish, Norwegian)
           |WG           # Weitergeleitet (German)
           |YML          # Ymlaen (Welsh)
           |ΠΡΘ          # Προωθημένο (Greek)
           |НА           # HA (Russian)
           |הועבר         # הועבר (Hebrew)
           |إعادة\ توجيه  # إعادة توجيه (Arabic)
           |轉寄          # 轉寄 (Chinese - Traditional)
           |转发          # 转发 (Chinese - Simplified)
          )
          (?:[:：]\s*|:?\s+) # Followed by either a colon or whitespace
          (.*)           # Capture original subject
        /xi # Case insensitive
      end

      attr_reader :mail
      attr_accessor :plain_content
      attr_writer :from, :subject, :body

      class << self
        # Three possible outcomes:
        #   1. Suspected good forward. Return it.
        #   2. Not parseable as a forward, record errors accessible by Forward#valid?
        #   3. Not a forward at all. Return nil.
        def from_mail(mail, plain_content)
          forward = new(mail, plain_content)
          return nil unless forward.suspected_forward?
          forward
        end

        def suspected_forward?(mail)
          mail.suspected_forward?
        end
      end

      def initialize(mail, plain_content)
        @mail = mail
        @plain_content = plain_content
      end

      def valid?
        errors.blank?
      end

      def errors
        errors = []
        errors << :no_subject      if subject.nil?
        errors << :no_from_address if from.blank?
        errors << :no_content      if from.present? && body.blank? && mail.attachments.blank?
        errors
      rescue ProcessingTimeout
        errors << :processing_timeout
      ensure
        errors
      end

      # Retrieves first occurence of original sender address from message body.
      # Example:
      #
      # message =
      #   Note to self...
      #   --- Begin Forwarded Message ---
      #   From: The mentor <mentor@example.com>
      #   ...
      #
      # from
      # => 'mentor@example.com'
      def from
        # Don't parse the forward headers multiple times.
        return @from if defined?(@from)

        @from ||= header.from_address && Zendesk::Mail::Address.parse(header.from_address)
        log("Forward's FROM address is: #{@from}") if @from

        @from
      end

      def suspected_forward?
        !!subject
      end

      def subject
        mail.forward_subject
      end

      def body
        comment_and_body.last
      end

      def html_body
        unless mail.content.try(:html)
          log("Can't find the HTML content from the email")
          return
        end

        html_body = mail.client.forward_html_body(mail, header_text)
        log("Forward's HTML body is empty") unless html_body
        html_body
      end

      def comment
        comment_and_body.first
      end

      private

      def comment_and_body
        @comment_and_body ||= begin
          unquote!
          if header_text
            remove_content_above_forward_header!
            remove_forward_header!
            message.strip!
          end
          [@comment, message]
        end
      end

      def message
        @message ||= plain_content.dup
      end

      def header_text
        @forward_header ||= begin
          header_text_value = header.to_s.strip

          if header_text_value
            # Only log certain amount just in case email is huge
            log("Forward's header (Plain-Text): #{header_text_value[0..5000]}")
          end

          header_text_value
        end
      end

      def header
        @header ||= ForwardHeader.new(message, mail)
      end

      def remove_content_above_forward_header!
        return unless forward_index = message.index(header_text)
        @comment = message.slice!(0, forward_index).strip.presence
      end

      def unquote!
        message.gsub!(mail.client.quote_pattern, '') if mail.client.quote_pattern.present?
      end

      def remove_forward_header!
        message.sub!(header_text, '')
      end

      def log(message)
        mail.logger.info(message)
      end
    end
  end
end
