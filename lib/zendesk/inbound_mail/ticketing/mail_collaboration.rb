require_relative "./mail_collaboration_update_rules"
require_relative "./mail_collaboration_update_types"

module Zendesk::InboundMail::Ticketing
  class MailCollaboration
    def self.build_all(ticket, collaborator_addresses: [], requester_is_on_email: false, logger: Rails.logger, recovering_suspended_ticket: false)
      # The current_user is set in TicketCreatorProcessor when Ticket#will_be_saved_by is called
      submitter = ticket.new_record? ? ticket.submitter || ticket.current_user : ticket.current_user

      update_type = mail_collaboration_update_type(
        ticket,
        submitter,
        requester_is_on_email,
        logger
      )

      if recovering_suspended_ticket && resetting_email_ccs?(update_type)
        update_type = AddNewAsEmailCcsUpdateType.new
      end

      logger.info("Update type is #{update_type.class.name}")

      email_cc_count = (resetting = resetting_email_ccs?(update_type)) ? 0 : ticket.email_ccs.size
      logger.info("Current number EMAIL_CC collaborators (#{resetting ? 'resetting' : 'adding'}): #{email_cc_count}")

      return [] if update_type.is_a?(NoopUpdateType)
      return [] unless submitter_has_update_permission?(ticket, submitter, update_type, logger)

      if update_type.adds_only_author?
        add_author_for_update_type(ticket, submitter, update_type, email_cc_count)

        if update_type.edits_email_ccs?
          add_new_agent_ccs_as_followers(ticket, logger)
        end

        return []
      end

      defer_collaboration_creation = update_type.defers_collaboration_creation?
      collaborator_ids = defer_collaboration_creation ? { can_comment_publicly: [], can_comment_privately: [] } : nil
      account = ticket.account
      settings = {
        has_email_ccs_light_agents_v2: account.has_email_ccs_light_agents_v2?,
        has_light_agent_email_ccs_allowed_enabled: account.has_light_agent_email_ccs_allowed_enabled?
      }

      is_ccs_limit_exceeded = false
      saved_new_users = collaborator_addresses.each_with_object([]) do |collaborator_address, new_users|
        collaboration = MailCollaboration.new(
          account,
          ticket,
          collaborator_address,
          update_type,
          email_cc_count,
          new_users,
          logger
        )

        collaboration.replace_matching_user(ticket.current_user)
        added = collaboration.add_or_skip(ticket, collaborator_ids, settings)

        if attempting_to_add_as_new_email_cc?(update_type, collaboration.user, settings)
          if added
            email_cc_count += 1
          elsif account.has_follower_and_email_cc_collaborations_enabled? && collaboration.errors.include?(:exceeds_limit)
            is_ccs_limit_exceeded = true
          end
        end
      end

      if is_ccs_limit_exceeded
        ticket.add_flags!(
          [EventFlagType.EMAIL_CCS_EXCEEDED_LIMIT],
          EventFlagType.EMAIL_CCS_EXCEEDED_LIMIT => {
            message: { max_ccs_limit: Ticket::MAX_COLLABORATORS_V2 }
          }
        )
      end

      if defer_collaboration_creation
        update_collaborations(ticket, update_type, collaborator_ids, saved_new_users, logger)
      end

      saved_new_users
    end

    def self.submitter_has_update_permission?(ticket, submitter, update_type, logger)
      if update_type.is_a?(LegacyCcsUpdateType)
        return submitter.can?(:add_collaborator, ticket)
      end

      submitter_can_add_email_cc = submitter.can?(:add_email_cc, ticket)
      submitter_can_add_follower = submitter.can?(:add_follower, ticket)
      email_ccs = update_type.edits_email_ccs? && submitter_can_add_email_cc
      followers = update_type.edits_followers? && submitter_can_add_follower
      (email_ccs || followers).tap do |submitter_has_permissions|
        logger.info(
          "Submitter #{submitter_has_permissions ? 'has' : 'does not have'} permission to update "\
          "collaborations and appropriate settings are enabled. 'update_type' is #{update_type.class.name} "\
          "'submitter_can_add_email_cc' is #{submitter_can_add_email_cc} and 'submitter_can_add_follower' is #{submitter_can_add_follower}."
        )
      end
    end

    def self.resetting_email_ccs?(update_type)
      update_type.is_a?(ResetEmailCcsUpdateType)
    end

    def self.attempting_to_add_as_new_email_cc?(update_type, user, settings)
      return false if user.nil? || update_type.is_a?(LegacyCcsUpdateType)

      adding_follower = update_type.is_a?(AddNewAsLegacyUpdateType) && user.is_agent?

      if settings[:has_email_ccs_light_agents_v2] && settings[:has_light_agent_email_ccs_allowed_enabled]
        update_type.edits_email_ccs? && !adding_follower
      else
        light_agent = !user.can?(:publicly, Comment)
        update_type.edits_email_ccs? && !(adding_follower || light_agent)
      end
    end

    def self.mail_collaboration_rules(ticket)
      if ticket.account.has_email_collaboration_by_light_agents_fixed?
        [
          LegacyCcs,
          FollowersAndEmailCcsSettingsDisabled,
          NewTicket,
          SubmitterIsLightAgent,
          AgentIsCcAndFollowerOrAssigneeWithRequesterOnEmail,
          AgentIsCcAndFollowerOrAssigneeWithRequesterNotOnEmail,
          FollowerOrAssigneeWithPrivateComment,
          FollowerOrAssigneeWithOnlyFollowersAllowed,
          FollowerOrAssigneeWithOnlyEmailCcsAllowed,
          FollowerOrAssigneeWithFollowersAndCcsAllowed,
          EmailCcOrRequesterAndRequesterIsOnEmail,
          EmailCcOrRequesterAndSubmitterIsAgent,
          EmailCcOrRequesterNoop,
          AssignedGroupAgent,
          SubmitterIsAgentAndRequesterIsOnEmail,
          SubmitterIsAgentAndFollowersIsEnabled,
          SubmitterIsAgentAndEmailCcsIsEnabled,
          SubmitterIsEndUser,
          Noop
        ]
      else
        [
          LegacyCcs,
          FollowersAndEmailCcsSettingsDisabled,
          NewTicket,
          AgentIsCcAndFollowerOrAssigneeWithRequesterOnEmail,
          AgentIsCcAndFollowerOrAssigneeWithRequesterNotOnEmail,
          FollowerOrAssigneeWithPrivateComment,
          FollowerOrAssigneeWithOnlyFollowersAllowed,
          FollowerOrAssigneeWithOnlyEmailCcsAllowed,
          FollowerOrAssigneeWithFollowersAndCcsAllowed,
          EmailCcOrRequesterAndRequesterIsOnEmail,
          EmailCcOrRequesterAndSubmitterIsAgent,
          EmailCcOrRequesterNoop,
          AssignedGroupAgent,
          SubmitterIsAgentAndRequesterIsOnEmail,
          SubmitterIsAgentAndFollowersIsEnabled,
          SubmitterIsAgentAndEmailCcsIsEnabled,
          SubmitterIsLightAgent,
          SubmitterIsEndUser,
          Noop
        ]
      end
    end

    def self.mail_collaboration_update_type(ticket, submitter, requester_is_on_email, logger)
      found_rule = mail_collaboration_rules(ticket).each do |rule_class|
        rule = rule_class.new(ticket, submitter, requester_is_on_email)

        if rule.conditions_are_met?
          logger.info(rule.to_s)
          break rule
        end
      end

      found_rule ||= Noop.new(ticket, submitter, requester_is_on_email)
      found_rule.update_type
    end

    def self.set_send_verify_email(ticket, saved_new_users)
      return if saved_new_users.empty?
      saved_new_users.each do |new_user|
        collaboration = ticket.collaborations.detect { |c| c.user_id == new_user.id }
        next unless collaboration.try(:user)

        # must be done on the collaboration's reference to the User since
        # send_verify_email is a virtual attribute
        collaboration.user.send_verify_email = true
      end
    end

    def self.add_author_for_update_type(ticket, submitter, update_type, email_cc_count = 0)
      collaborator_type = if update_type.is_a?(AddAuthorAsEmailCcUpdateType)
        if email_cc_count >= Ticket::MAX_COLLABORATORS_V2
          Rails.logger.warn "Unable to add author as an email CC, maximum of #{Ticket::MAX_COLLABORATORS_V2} email CCs reached"
          return
        end

        CollaboratorType.EMAIL_CC
      end

      collaborator_type ||= CollaboratorType.FOLLOWER if update_type.is_a?(AddAuthorAsFollowerUpdateType)
      return unless collaborator_type

      ticket.additional_collaborators_with_type([submitter.id], collaborator_type)
    end

    def self.update_collaborations(ticket, update_type, collaborator_ids, saved_new_users, logger)
      public_commenter_ids = collaborator_ids[:can_comment_publicly] || []
      private_commenter_ids = collaborator_ids[:can_comment_privately] || []
      all_collaborator_ids = public_commenter_ids + private_commenter_ids

      if update_type.is_a?(ResetEmailCcsUpdateType)
        ticket.set_collaborators_with_type(public_commenter_ids, CollaboratorType.EMAIL_CC)
        ticket.additional_collaborators_with_type(private_commenter_ids, CollaboratorType.FOLLOWER) unless private_commenter_ids.empty?

      elsif update_type.is_a?(AddNewAsLegacyUpdateType)
        ticket.additional_collaborators_with_type(all_collaborator_ids, CollaboratorType.LEGACY_CC)

      elsif [AddNewAsEmailCcsUpdateType, AddNewEndUsersUpdateType, AddNewAgentsAsEmailCcsUpdateType, AddNonGroupAgentsAsEmailCcsUpdateType].include?(update_type.class)
        ticket.additional_collaborators_with_type(public_commenter_ids, CollaboratorType.EMAIL_CC) unless public_commenter_ids.empty?
        ticket.additional_collaborators_with_type(private_commenter_ids, CollaboratorType.FOLLOWER) unless private_commenter_ids.empty?

      elsif [AddNewAgentsAsFollowersUpdateType, AddNonGroupAgentsAsFollowersUpdateType].include?(update_type.class)
        ticket.additional_collaborators_with_type(all_collaborator_ids, CollaboratorType.FOLLOWER)
      end

      # Set send_verify_email on the collaboration users now that the collaborations
      # have been created - this needs to happen after collaboration creation since it
      # is a virtual attribute on the User
      set_send_verify_email(ticket, saved_new_users)

      # If the setting that automatically makes agent email CCs followers is enabled,
      # add those follower collaborations now
      add_new_agent_ccs_as_followers(ticket, logger)
    end

    def self.add_new_agent_ccs_as_followers(ticket, logger)
      return unless ticket.account.has_agent_email_ccs_become_followers_enabled?

      new_followers = new_non_follower_agent_email_ccs(ticket)
      logger.info("Agent email CCs being added as followers as well: #{new_followers.map(&:email)}")
      new_followers.each do |new_follower|
        ticket.collaborations.build(
          ticket: ticket,
          user: new_follower,
          collaborator_type: CollaboratorType.FOLLOWER
        )
      end
    end

    def self.new_non_follower_agent_email_ccs(ticket)
      # MTC never deletes followers so persisted followers will never be in the ticket's @collaborations_for_removal,
      # so closed_ticket.collaborations or ticket.collaborations is okay to use here.
      excluded_agent_ids = if (closed_ticket = ticket.followup_source) && ticket.new_record?
        follower_ids_from_closed = closed_ticket.collaborations.followers.map(&:user_id)

        agent_email_cc_ids_from_closed = closed_ticket.collaborations.email_ccs.select do |email_cc|
          email_cc.user.is_agent?
        end.map(&:user_id)

        follower_ids_from_closed + agent_email_cc_ids_from_closed
      else
        ticket.collaborations.map do |collaboration|
          if collaboration.persisted? && collaboration.collaborator_type == CollaboratorType.FOLLOWER
            collaboration.user_id
          end
        end.compact
      end

      ticket.collaborations.select do |collaboration|
        collaboration.new_record? &&
          collaboration.collaborator_type == CollaboratorType.EMAIL_CC &&
          collaboration.user.is_agent? &&
          !excluded_agent_ids.include?(collaboration.user_id)
      end.map(&:user)
    end

    attr_reader :account, :ticket, :mail_address, :logger
    attr_writer :user

    def initialize(account, ticket, mail_address, update_type, email_cc_count, new_users, logger)
      @account = account
      @ticket = ticket
      @mail_address = mail_address
      @new_users = [] if new_users
      @update_type = update_type
      @email_cc_count = email_cc_count
      @saved_new_users = new_users
      @logger = logger
      @debugging_mode = account.has_email_debugging_mode?
    end

    def user
      @user ||= account.find_user_by_email(mail_address.address) || build_user
    end

    def collaborator_info
      user.id ? "#{user.id} #{user.identities.map(&:id)}" : "new"
    end

    def downcased_address
      @downcased_address ||= mail_address.address.downcase
    end

    def valid?
      errors.none?
    end

    def errors
      return @errors if @errors

      @errors = []
      @errors << :invalid_user                unless user_valid?
      @errors << :already_a_collaborator      if already_a_collaborator?
      @errors << :user_not_allowed            unless user_allowed?
      @errors << :collaboration_not_allowed   unless account_allows_collaboration?
      @errors << :collaborator_is_assignee    if collaborator_is_assignee?
      @errors << :collaborator_is_requester   if collaborator_is_requester?
      @errors << :excluded_due_to_group       if excluded_due_to_group?
      @errors << :exceeds_limit               if exceeds_limit?
      @errors << :potential_netsuite_cc_loop  if potential_netsuite_cc_loop?
      @errors << :user_role_not_allowed       unless user_role_allowed?
      @errors
    end

    def build
      ticket.collaborations.build(user: user, ticket: ticket)

      if !@account.has_email_fix_current_collaborators_mail_collaboration? || @update_type.is_a?(LegacyCcsUpdateType)
        ticket.current_collaborators = (!ticket.current_collaborators.blank? ? ticket.current_collaborators + ', ' : '') + user.name
      end
    end

    def add_or_skip(ticket, collaborator_ids, settings)
      defer_collaboration_creation = @update_type.defers_collaboration_creation? && collaborator_ids

      if (is_valid = valid?)
        if @new_users.try(:include?, user)
          # Attempt to save the user if a new_users array was provided
          # (which will be used to delete the new users if the ticket save fails)
          logger.info("Attempting to save newly built user for collaborator #{downcased_address}")

          begin
            UserIdentity.uncached { user.save! }
            @saved_new_users << user
          rescue ActiveRecord::RecordInvalid, ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation => e
            logger.info("Rescuing expected exception from attempt to save collaborator #{downcased_address}: #{e.message}")
            repair_user
            is_valid = valid?
          rescue StandardError => e
            logger.info("Rescuing other exception from attempt to save collaborator #{downcased_address}: #{e.message}")
            is_valid = false
          end
        end

        if is_valid && !defer_collaboration_creation
          logger.info("Adding collaborator #{downcased_address}, #{collaborator_info}")
          build
        end
      end

      if is_valid && defer_collaboration_creation
        user_update_privacy = if settings[:has_email_ccs_light_agents_v2] && settings[:has_light_agent_email_ccs_allowed_enabled]
          :can_comment_publicly
        else
          user.can?(:publicly, Comment) ? :can_comment_publicly : :can_comment_privately
        end
        collaborator_ids[user_update_privacy] ||= []
        collaborator_ids[user_update_privacy] << user.id
      else
        logger.info("Skipping collaborator #{downcased_address} due to collaboration errors: #{errors.inspect}") unless is_valid

        if user == ticket.current_user && !ticket.new_record? && errors.include?(:potential_netsuite_cc_loop)
          raise ArgumentError, 'Submitter is invalid collaborator'
        end
      end

      is_valid
    end

    def replace_matching_user(other_user)
      if account.has_follower_and_email_cc_collaborations_enabled?
        if other_user && other_user.emails_include?(downcased_address)
          logger.info("Email author is: #{downcased_address} (matched by all email identities)")
          self.user = other_user # Prevents attempting to save new records twice
        end
      else
        if other_user.try(:email).try(:downcase) == downcased_address
          logger.info("Email author is: #{downcased_address} (matched by default email identity)")
          self.user = other_user # Prevents attempting to save new records twice
        end
      end
    end

    protected

    def existing_end_user_collaborators
      ticket.collaborations.select do |collaboration|
        collaboration.user && collaboration.user.is_end_user?
      end
    end

    def build_user
      logger.info 'Building user' if @debugging_mode
      user_name = mail_address.name || mail_address.friendly
      user_name = "Unknown #{user_name}".strip if user_name.size < 2

      new_user = User.new(
        account: account,
        name: user_name,
        email: downcased_address,
        send_verify_email: account.is_signup_required?
      )

      logger.info "User built: #{new_user.inspect}" if @debugging_mode
      @new_users << new_user if @new_users
      new_user
    end

    def exceeds_limit?
      if @update_type.is_a?(LegacyCcsUpdateType)
        user.is_end_user? && existing_end_user_collaborators.size >= account.settings.collaborators_maximum
      else
        @email_cc_count >= Ticket::MAX_COLLABORATORS_V2
      end
    end

    def user_allowed?
      !user.new_record? || account.is_open?
    end

    def account_allows_collaboration?
      account.allows_collaborator?(downcased_address) && account.allows?(downcased_address)
    end

    def potential_netsuite_cc_loop?
      !account.has_allow_netsuite_ccs? && downcased_address =~ /@.*?\.netsuite\.com\Z/
    end

    def collaborator_is_assignee?
      if account.has_follower_and_email_cc_collaborations_enabled?
        ticket.assignee.try(:emails_include?, downcased_address)
      else
        ticket.assignee && ticket.assignee.identities.map(&:value).compact.map(&:downcase).include?(downcased_address)
      end
    end

    def collaborator_is_requester?
      if account.has_follower_and_email_cc_collaborations_enabled?
        ticket.requester.try(:emails_include?, downcased_address)
      else
        ticket.requester && ticket.requester.identities.map(&:value).compact.map(&:downcase).include?(downcased_address)
      end
    end

    def already_a_collaborator?
      return false if @update_type.is_a?(ResetEmailCcsUpdateType)
      collaborators = ticket.collaborators.flat_map { |user| user.identities.email.map(&:value) }.compact.map(&:downcase)
      collaborators.include?(downcased_address)
    end

    def user_valid?
      return true if user.valid?

      if @debugging_mode
        logger.info("Collaboration user is invalid with errors: #{user.errors.full_messages.join(', ')}")
      end

      false
    end

    def user_role_allowed?
      return true unless @update_type.restricts_by_user_role?

      if @update_type.adds_only_agents?
        user.is_agent?
      else
        user.is_end_user?
      end
    end

    def excluded_due_to_group?
      return false unless account.has_follower_and_email_cc_collaborations_enabled?
      return false unless [AddNonGroupAgentsAsFollowersUpdateType, AddNonGroupAgentsAsEmailCcsUpdateType].include?(@update_type.class)
      !ticket.assignee && ticket.group_includes?(user)
    end

    def repair_user
      # Attempt to repair invalid user if possible.
      user_identity_value = user.identities.first.try(:value)
      collaborator_name = user_identity_value || user.name

      # Log the existing errors.
      logger.info("Collaborator #{collaborator_name} is invalid with errors: #{user.errors.full_messages.join(', ')}")

      # Search for an existing user with the same email address.
      existing_user = User.uncached { account.find_user_by_email(user_identity_value) }

      # Abort if the existing user is also invalid.
      unless existing_user.try(:valid?)
        logger.info("Could not find existing valid User for collaborator #{user_identity_value}")
        return
      end

      # Replace the invalid user with the existing one.
      logger.info("Found existing valid User for collaborator #{user_identity_value}: #{existing_user.inspect}")
      @user = existing_user
    end
  end
end
