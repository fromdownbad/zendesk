module Zendesk
  module InboundMail
    module Ticketing
      class Followup
        attr_reader :state, :mail, :mail_description

        delegate :account, :author, :ticket, :closed_ticket, to: :state

        def initialize(state, mail, mail_description)
          @state = state
          @mail  = mail
          @mail_description = mail_description
        end

        def source_link_attributes
          { source: closed_ticket, target: state.ticket, link_type: TicketLink::FOLLOWUP }
        end

        def via_id
          ViaType.CLOSED_TICKET
        end

        def additional_tags
          closed_ticket.current_tags
        end

        def description
          followup_title = ::I18n.t(
            'ticket.followup.comment_with_title',
            source_ticket_nice_id: closed_ticket.nice_id,
            source_ticket_title: closed_ticket.title,
            locale: author.translation_locale
          )

          if rich?
            "#{followup_title}<br /><br />#{mail_description}"
          else
            "#{followup_title}\n\n#{mail_description}"
          end
        end

        def original_requester
          closed_ticket.requester
        end

        def original_recipient
          closed_ticket.recipient
        end

        def follower_ids
          return @follower_ids if defined? @follower_ids
          @follower_ids = []
          if account.has_ticket_followers_allowed_enabled?
            @follower_ids += closed_ticket.collaborations.followers.map(&:user_id)
            @follower_ids.delete(state.ticket.requester.id)
            @follower_ids.uniq!
          end
          @follower_ids
        end

        def email_cc_ids
          return @email_cc_ids if defined? @email_cc_ids
          @email_cc_ids = []
          if account.has_comment_email_ccs_allowed_enabled?
            @email_cc_ids = [original_requester.id, closed_ticket.collaborations.email_ccs.map(&:user_id)].flatten
            @email_cc_ids.delete(state.ticket.requester.id)
            @email_cc_ids.uniq!
          end
          @email_cc_ids
        end

        def legacy_ccs_present?
          closed_ticket.collaborations.legacy_ccs.any?
        end

        def all_collaborator_ids
          collaborator_ids = [original_requester.id, closed_ticket.collaborations.map(&:user_id)].flatten.uniq
          collaborator_ids.delete(state.ticket.requester.id)
          collaborator_ids.uniq!
          collaborator_ids
        end

        def add_comment
          is_public = if apply_private_comment_security_fix?
            false
          else
            Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail)
          end

          Zendesk::InboundMail::Ticketing::CommentPrivacy.add_flags!(state, mail)

          comment_options = {
            author: author,
            body: description,
            is_public: is_public
          }
          comment_options[:format] = "rich" if rich?
          state.ticket.add_comment(comment_options)
        end

        def apply_private_comment_security_fix?
          account.has_email_make_other_user_comments_private? &&
            state.flags.include?(EventFlagType.OTHER_USER_UPDATE)
        end

        def ticket_fields
          closed_ticket.ticket_field_entries.map { |tfe| {ticket_field_id: tfe.ticket_field_id, value: tfe.value} }
        end

        private

        def rich?
          @rich ||= state.account.has_email_followup_ticket_html_comment_fix? && state.ticket&.comment&.html_body
        end
      end
    end
  end
end
