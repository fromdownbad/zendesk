module Zendesk
  module InboundMail
    module Ticketing
      class CommentPrivacy
        SUPPORTED_EVENT_TYPES = [Notification.to_s, OrganizationActivity.to_s, Cc.to_s, FollowerNotification.to_s].freeze

        attr_reader :state, :mail

        delegate :account, :author, to: :state
        delegate :logger, to: :mail

        def initialize(state, mail)
          @state = state
          @mail  = mail
        end

        def self.add_flags!(state, mail)
          new(state, mail).add_flags!
        end

        def self.comment_public?(state, mail)
          new(state, mail).comment_public?
        end

        def self.parent_comments_public?(state, mail)
          new(state, mail).parent_comments_public?
        end

        def add_flags!
          if account.has_email_private_comment_requester_missing_flag? &&
            account.has_comment_email_ccs_allowed_enabled? &&
            state.author_is_email_cc? &&
            requester_not_found?
            state.add_flag(EventFlagType.PRIVATE_COMMENT_REQUESTER_MISSING)
          end
        end

        # See Zendesk::CommentPublicity for more references
        def comment_public?
          if account.has_comment_email_ccs_allowed_enabled? && state.author_is_email_cc?
            logger.info "Comment email ccs enabled & author is an email cc"
            if account.has_ccs_requester_excluded_public_comments_enabled? && only_support_recipient?
              logger.info "CCs requester excluded public comments enabled & only support recipient present"
            elsif requester_not_found?
              logger.info "Original requester was cut out, make this comment private/internal"
              return false
            end
          end

          unless author.is_agent?
            logger.info "Any end user comments should be public"
            return true
          end

          if account.has_email_allow_light_agent_to_add_private_comment?
            unless author.can?(:publicly, Comment)
              logger.info "Author can't publicly comment, make this comment private/internal. Arturo email_allow_light_agent_to_add_private_comment is enabled"
              return false
            end
          end

          # This is the "agent as end user" case; the agent should be treated
          # as if they're an end-user when they're making tickets via Help Center.
          if ticket.agent_as_end_user_for?(author) &&
              Arturo.feature_enabled_for?(:agent_as_end_user, ticket.account)
            logger.info "Comments made by agent on their own tickets from HelpCenter should be public"
            return true
          end

          # Remove the below unless block once the arturo :email_allow_light_agent_to_add_private_comment
          # is GA'ed in production
          unless account.has_email_allow_light_agent_to_add_private_comment?
            unless author.can?(:publicly, Comment)
              logger.info "Author can't publicly comment, make this comment private/internal. Arturo email_allow_light_agent_to_add_private_comment is disabled"
              return false
            end
          end

          is_public = state.properties ? state.properties[:is_public] : nil
          unless is_public.nil?
            logger.info "Text API can override Comment privacy, make this comment #{is_public ? 'public' : 'private/internal'}"
            return is_public
          end

          # Make the comment private if any of the following conditions are NOT true
          unless ticket.entry.nil? && account_has_public_agent_email_comments? && ticket.is_public?
            logger.info "Ticket entry is nil, make this comment private/internal" unless ticket.entry.nil?
            logger.info "Account disabled public Agent comments, make this comment private/internal" unless account_has_public_agent_email_comments?
            logger.info "Ticket is private, make this comment private/internal" unless ticket.is_public?
            return false
          end

          if use_parent_comment_privacy?
            logger.info "When an agent is replying, privacy of the originating comment is used (unless it isn't present)"

            if parent_comments_private? && requester_not_on_email?
              logger.info "Parent comment wasn't public, make this comment private/internal"
              return false
            end
          end

          logger.info "Make this comment public"
          true
        end

        def parent_comments_public?
          message_ids = [mail.in_reply_to].concat(mail.references).compact.uniq
          return true if message_ids.empty?

          # If mail references any inbound email that created a private comment, return false
          comments = Comment.arel_table

          return false if account.inbound_emails.
            where(message_id: message_ids).
            joins(:comment).
            where(comments[:account_id].eq(account.id)).
            where(comments[:is_public].eq(false)).
            any?

          # If mail 'In-Reply-To' message ID references an OutboundEmail notification for a private comment, return false
          notification = find_outbound_notification_by_message_ids([mail.in_reply_to])
          return false unless notification_comment_public?(notification)

          # If mail 'References' header contains message IDs associated to outbound notifications, return false
          # if latest associated OutboundEmail notification was for a private comment
          notification = find_outbound_notification_by_message_ids(mail.references - [mail.in_reply_to])
          return false unless notification_comment_public?(notification)

          true
        end

        private

        def ticket
          @ticket ||= (state.closed_ticket || state.ticket)
        end

        def requester_not_on_email?
          requester_not_found?
        end

        def parent_comments_private?
          !parent_comments_public?
        end

        def requester_not_found?
          addresses = mail.direct_recipients.map { |address| Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(address) }.uniq
          recipient_ids = ticket.account.user_identities.email.where(value: addresses).pluck(:user_id)
          !recipient_ids.include?(ticket.requester_id) && !ticket.requested_by?(state.author)
        end

        def only_support_recipient?
          mail.recipients.size == 1 && Zendesk::InboundMail::RecipientsHelper.find_recipient_address(mail, account)
        end

        def account_has_public_agent_email_comments?
          ticket.account.is_email_comment_public_by_default
        end

        def use_parent_comment_privacy?
          account.has_comment_email_ccs_allowed_enabled? || account.has_ticket_followers_allowed_enabled?
        end

        def find_outbound_notification_by_message_ids(message_ids)
          notification_id = OutboundEmail.for_ticket(ticket).
            where(message_id: message_ids).
            order("created_at DESC").
            pluck(:notification_id).
            first

          return unless notification_id

          ticket.events.where(id: notification_id, type: SUPPORTED_EVENT_TYPES).first
        end

        def notification_comment_public?(notification)
          return true if notification.nil? || notification.audit.nil? || notification.audit.comment.nil?
          notification.audit.comment.is_public?
        end
      end
    end
  end
end
