module Zendesk
  module InboundMail
    module Ticketing
      class MailComment
        attr_reader :state, :mail

        delegate :account, :ticket, :author, to: :state

        def initialize(state, mail)
          @state = state
          @mail  = mail
        end

        def attributes
          # TODO: Create a setting for third party comment privacy
          comment_is_public = if apply_private_comment_security_fix?
            false
          else
            Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail)
          end

          Zendesk::InboundMail::Ticketing::CommentPrivacy.add_flags!(state, mail)

          attributes = {
            is_public: comment_is_public,
            channel_back: channel_back?(comment_is_public),
          }

          if (html = state.html_content)
            attributes[:html_body] = html
            attributes[:original_plain_body] = state.plain_content.dup.presence || NO_CONTENT.dup
          else
            attributes[:body] = state.plain_content.dup.presence || NO_CONTENT.dup
          end

          attributes
        end

        private

        def apply_private_comment_security_fix?
          account.has_email_make_other_user_comments_private? &&
            state.flags.include?(EventFlagType.OTHER_USER_UPDATE)
        end

        def channel_back?(comment_is_public)
          return unless author.is_agent?
          (ticket.via_facebook? && comment_is_public).inspect
        end
      end
    end
  end
end
