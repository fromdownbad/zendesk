module Zendesk::InboundMail::Ticketing
  class MailCollaborationUpdateRule
    attr_accessor :ticket, :submitter, :requester_is_on_email
    delegate :account, to: :ticket

    def initialize(ticket, submitter, requester_is_on_email)
      self.ticket = ticket
      self.submitter = submitter
      self.requester_is_on_email = requester_is_on_email
    end

    def conditions_are_met?
      false
    end

    def update_type
      update_type_class.new
    end

    def to_s
      "MailCollaborationUpdateRule: #{log_message}"
    end

    protected

    def update_type_class
      NoopUpdateType
    end

    def log_message
      "base rule"
    end

    def followers_allowed?
      @followers_allowed ||= account.has_ticket_followers_allowed_enabled?
    end

    def email_ccs_allowed?
      @email_ccs_allowed ||= account.has_comment_email_ccs_allowed_enabled?
    end

    def submitter_is_agent?
      submitter.is_agent? && submitter.can?(:publicly, Comment)
    end

    def submitter_is_light_agent?
      submitter.is_agent? && !submitter.can?(:publicly, Comment)
    end

    private

    def submitter_is_assignee?
      submitter == ticket.assignee
    end

    def submitter_is_in_group?
      ticket.group_includes?(submitter)
    end

    def add_new_agents_update_type_class
      if followers_allowed?
        AddNewAgentsAsFollowersUpdateType
      elsif email_ccs_allowed?
        AddNewAgentsAsEmailCcsUpdateType
      else
        NoopUpdateType
      end
    end
  end

  class LegacyCcs < MailCollaborationUpdateRule
    def conditions_are_met?
      !account.has_follower_and_email_cc_collaborations_enabled?
    end

    protected

    def update_type_class
      LegacyCcsUpdateType
    end

    def log_message
      "CCs/Followers setting is disabled, adding CCs as legacy collaborations"
    end
  end

  class FollowersAndEmailCcsSettingsDisabled < MailCollaborationUpdateRule
    def conditions_are_met?
      !followers_allowed? && !email_ccs_allowed?
    end

    protected

    def update_type_class
      NoopUpdateType
    end

    def log_message
      "individual CCs and Followers settings are disabled, so no collaborations can be added"
    end
  end

  class NewTicket < MailCollaborationUpdateRule
    def conditions_are_met?
      ticket.new_record? && ticket.followup_source.nil?
    end

    protected

    def update_type_class
      ResetEmailCcsUpdateType
    end

    def log_message
      "ticket is new"
    end
  end

  class AgentIsCcAndFollowerOrAssignee < MailCollaborationUpdateRule
    def conditions_are_met?
      submitter_is_agent_participant_and_email_cc?
    end

    protected

    def submitter_is_agent_participant_and_email_cc?
      return false if submitter.is_end_user?
      requester_or_email_cc?(ticket, submitter) && follower_or_assignee?(ticket, submitter)
    end

    def log_message
      "submitter is an agent already participating in the ticket but is also an email CC"
    end

    private

    def requester_or_email_cc?(ticket, submitter)
      ticket.requested_by?(submitter) || ticket.is_email_cc?(submitter)
    end

    def follower_or_assignee?(ticket, submitter)
      ticket.is_follower?(submitter) || submitter_is_assignee?
    end
  end

  class AgentIsCcAndFollowerOrAssigneeWithRequesterOnEmail < AgentIsCcAndFollowerOrAssignee
    def conditions_are_met?
      super && requester_is_on_email
    end

    protected

    def update_type_class
      ResetEmailCcsUpdateType
    end

    def log_message
      "#{super}, and requester is on the email"
    end
  end

  class AgentIsCcAndFollowerOrAssigneeWithRequesterNotOnEmail < AgentIsCcAndFollowerOrAssignee
    def conditions_are_met?
      super && !requester_is_on_email
    end

    protected

    def update_type_class
      add_new_agents_update_type_class
    end

    def log_message
      "#{super}, and requester is not on the email"
    end
  end

  class FollowerOrAssignee < MailCollaborationUpdateRule
    def conditions_are_met?
      ticket.is_follower?(submitter) || submitter_is_assignee?
    end

    protected

    def log_message
      "submitter is a follower or the assignee"
    end
  end

  class FollowerOrAssigneeWithPrivateComment < FollowerOrAssignee
    def conditions_are_met?
      super && !ticket.comment.is_public
    end

    protected

    def update_type_class
      add_new_agents_update_type_class
    end

    def log_message
      "#{super}, and comment is private"
    end
  end

  class FollowerOrAssigneeWithOnlyFollowersAllowed < FollowerOrAssignee
    def conditions_are_met?
      super && followers_allowed? && !email_ccs_allowed?
    end

    protected

    def update_type_class
      AddNewAgentsAsFollowersUpdateType
    end

    def log_message
      "#{super}, adding all new users but email CCs are not allowed"
    end
  end

  class FollowerOrAssigneeWithOnlyEmailCcsAllowed < FollowerOrAssignee
    def conditions_are_met?
      super && !followers_allowed? && email_ccs_allowed?
    end

    protected

    def update_type_class
      AddNewAsEmailCcsUpdateType
    end

    def log_message
      "#{super}, adding all new users as email CCs"
    end
  end

  class FollowerOrAssigneeWithFollowersAndCcsAllowed < FollowerOrAssignee
    def conditions_are_met?
      super && followers_allowed? && email_ccs_allowed?
    end

    protected

    def update_type_class
      AddNewAsLegacyUpdateType
    end

    def log_message
      "#{super}, adding all new users and both followers and email CCs are allowed"
    end
  end

  class EmailCcOrRequester < MailCollaborationUpdateRule
    def conditions_are_met?
      ticket.is_email_cc?(submitter) || ticket.requested_by?(submitter)
    end

    protected

    def log_message
      log_requester_tracking_mismatch = ticket.requested_by?(submitter) != requester_is_on_email
      "#{log_requester_tracking_mismatch ? 'WARNING: there is a mismatch detecting that the requester is on the email: ' : ''}"\
        "submitter is an email CC or requester"
    end
  end

  class EmailCcOrRequesterAndRequesterIsOnEmail < EmailCcOrRequester
    def conditions_are_met?
      super && requester_is_on_email && email_ccs_allowed?
    end

    protected

    def update_type_class
      ResetEmailCcsUpdateType
    end

    def log_message
      "#{super}, and requester is on email"
    end
  end

  class EmailCcOrRequesterAndSubmitterIsAgent < EmailCcOrRequester
    def conditions_are_met?
      super && submitter_is_agent?
    end

    protected

    def update_type_class
      add_new_agents_update_type_class
    end

    def log_message
      "#{super}, and submitter is an agent"
    end
  end

  class EmailCcOrRequesterNoop < EmailCcOrRequester
    protected

    def log_message
      "#{super}, requester is not on email and submitter is not an agent, so skipping collaboration update"
    end
  end

  class AssignedGroupAgent < MailCollaborationUpdateRule
    def conditions_are_met?
      submitter_is_in_group?
    end

    protected

    def update_type_class
      add_non_group_agents_update_type
    end

    def log_message
      "submitter is a member of the assigned group"
    end

    private

    def add_non_group_agents_update_type
      if followers_allowed?
        AddNonGroupAgentsAsFollowersUpdateType
      elsif email_ccs_allowed?
        AddNonGroupAgentsAsEmailCcsUpdateType
      else
        NoopUpdateType
      end
    end
  end

  class SubmitterIsAgent < MailCollaborationUpdateRule
    def conditions_are_met?
      submitter_is_agent?
    end

    protected

    def log_message
      "submitter is an agent not already participating"
    end
  end

  class SubmitterIsAgentAndRequesterIsOnEmail < SubmitterIsAgent
    def conditions_are_met?
      super && requester_is_on_email
    end

    protected

    def update_type_class
      AddAuthorAsEmailCcUpdateType
    end

    def log_message
      "#{super}, and requester is on email"
    end
  end

  class SubmitterIsAgentAndFollowersIsEnabled < SubmitterIsAgent
    def conditions_are_met?
      super && followers_allowed?
    end

    protected

    def update_type_class
      AddAuthorAsFollowerUpdateType
    end

    def log_message
      "#{super}, and followers are enabled"
    end
  end

  class SubmitterIsAgentAndEmailCcsIsEnabled < SubmitterIsAgent
    def conditions_are_met?
      super && email_ccs_allowed?
    end

    protected

    def update_type_class
      AddAuthorAsEmailCcUpdateType
    end

    def log_message
      "#{super}, and email CCs are enabled"
    end
  end

  class SubmitterIsEndUser < MailCollaborationUpdateRule
    def conditions_are_met?
      submitter.is_end_user?
    end

    protected

    def update_type_class
      if account.has_email_make_other_user_comments_private?
        NoopUpdateType
      else
        AddAuthorAsEmailCcUpdateType
      end
    end

    def log_message
      "submitter is an end user not already participating"
    end
  end

  class SubmitterIsLightAgent < MailCollaborationUpdateRule
    def conditions_are_met?
      submitter_is_light_agent?
    end

    protected

    def update_type_class
      if followers_allowed?
        AddNewAgentsAsFollowersUpdateType
      else
        NoopUpdateType
      end
    end

    def log_message
      "submitter is a light agent and can only add private comments and followers"
    end
  end

  class Noop < MailCollaborationUpdateRule
    def conditions_are_met?
      Zendesk::InboundMail::StatsD.client.increment("email_ccs_update_type_not_found")
      true
    end

    protected

    def update_type_class
      NoopUpdateType
    end

    def log_message
      "follower_and_email_cc_collaborations is enabled but no update type found. Submitter: #{submitter.inspect}"
    end
  end
end
