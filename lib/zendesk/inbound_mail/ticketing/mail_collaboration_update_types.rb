module Zendesk::InboundMail::Ticketing
  class MailCollaborationUpdateType
    attr_reader :adds_only_agents,
      :adds_only_author,
      :adds_only_end_users,
      :defers_collaboration_creation,
      :edits_email_ccs,
      :edits_followers

    alias :adds_only_agents? :adds_only_agents
    alias :adds_only_author? :adds_only_author
    alias :adds_only_end_users? :adds_only_end_users
    alias :defers_collaboration_creation? :defers_collaboration_creation
    alias :edits_email_ccs? :edits_email_ccs
    alias :edits_followers? :edits_followers

    def initialize
      @adds_only_agents = false
      @adds_only_author = false
      @adds_only_end_users = false
      @defers_collaboration_creation = false
      @edits_email_ccs = false
      @edits_followers = false
    end

    def restricts_by_user_role?
      @adds_only_agents || @adds_only_end_users
    end
  end

  class LegacyCcsUpdateType < MailCollaborationUpdateType
  end

  class NoopUpdateType < MailCollaborationUpdateType
  end

  class AddNewAsLegacyUpdateType < MailCollaborationUpdateType
    def initialize
      super
      @defers_collaboration_creation = true
      @edits_email_ccs = true
      @edits_followers = true
    end
  end

  class AddNewAgentsUpdateType < MailCollaborationUpdateType
    def initialize
      super
      @defers_collaboration_creation = true
      @adds_only_agents = true
    end
  end

  class AddNewAgentsAsEmailCcsUpdateType < AddNewAgentsUpdateType
    def initialize
      super
      @edits_email_ccs = true
    end
  end

  class AddNewAgentsAsFollowersUpdateType < AddNewAgentsUpdateType
    def initialize
      super
      @edits_followers = true
    end
  end

  class AddNewAsEmailCcsUpdateType < MailCollaborationUpdateType
    def initialize
      super
      @defers_collaboration_creation = true
      @edits_email_ccs = true
    end
  end

  class ResetEmailCcsUpdateType < MailCollaborationUpdateType
    def initialize
      super
      @defers_collaboration_creation = true
      @edits_email_ccs = true
    end
  end

  class AddAuthorAsFollowerUpdateType < MailCollaborationUpdateType
    def initialize
      super
      @edits_followers = true
      @adds_only_author = true
    end
  end

  class AddAuthorAsEmailCcUpdateType < MailCollaborationUpdateType
    def initialize
      super
      @edits_email_ccs = true
      @adds_only_author = true
    end
  end

  class AddNewEndUsersUpdateType < MailCollaborationUpdateType
    def initialize
      super
      @defers_collaboration_creation = true
      @edits_email_ccs = true
      @adds_only_end_users = true
    end
  end

  class AddNonGroupAgentsAsFollowersUpdateType < MailCollaborationUpdateType
    def initialize
      super
      @defers_collaboration_creation = true
      @edits_followers = true
      @adds_only_agents = true
    end
  end

  class AddNonGroupAgentsAsEmailCcsUpdateType < MailCollaborationUpdateType
    def initialize
      super
      @defers_collaboration_creation = true
      @edits_email_ccs = true
      @adds_only_agents = true
    end
  end
end
