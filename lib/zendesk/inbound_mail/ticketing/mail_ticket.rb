require 'uri'

module Zendesk
  module InboundMail
    module Ticketing
      class MailTicket
        IP_ADDRESS_PATTERN = /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/
        CONNECT_CAMPAIGN_TAGS = %w[connect_email_campaign].freeze
        UNKNOWN = 'Unknown'.freeze
        AUDIT_METADATA_MAIL_CLIENT_SIZE_LIMIT = 512

        attr_reader :state, :mail

        def initialize(state, mail)
          @state = state
          @mail  = mail
        end

        def to_ticket
          ticket = Ticket.new do |t|
            t.via_id       = ViaType.MAIL
            t.status_id    = StatusType.NEW
            t.subject      = use_subject? ? mail.subject : ''
            t.account      = state.account
            t.submitter    = state.submitter
            t.requester    = state.author
            t.recipient    = remove_plus_id(state.recipient)
            t.description  = description(state.plain_content)
            if html = state.html_content
              t.comment.html_body = description(html)
              t.comment.original_plain_body = state.plain_content
            end
            t.comment.is_public = false if set_comment_to_private?
            t.original_recipient_address = state.original_recipient_address
            t.disable_triggers = true if disable_triggers?
            t.brand_id = state.brand_id
            t.created_at = created_at
          end

          if mark_as_unverified?
            ticket.mark_as_unverified(identity)
            log_marked_as_unverified
          end

          ticket
        end

        def to_suspended_ticket
          suspended_ticket_recipient = if state.account.has_email_suspended_ticket_ignore_nil_recipient?
            remove_plus_id(state.recipient) if state.recipient
          else
            remove_plus_id(state.recipient)
          end

          suspended_ticket = state.account.suspended_tickets.new do |s|
            s.subject    = mail.subject
            s.from_name  = state.from.name || UNKNOWN
            s.from_mail  = state.from.address
            s.author     = state.author unless state.author.try(:new_record?)
            s.recipient  = suspended_ticket_recipient
            s.cause      = state.suspension
            s.content    = state.html_content.presence || state.plain_content.dup.presence || NO_CONTENT.dup
            s.ticket_id  = (state.closed_ticket || state.ticket).try(:nice_id)
            s.message_id = mail.message_id
            s.properties = suspended_ticket_properties
            s.original_recipient_address = state.original_recipient_address
          end

          suspended_ticket
        end

        def add_metadata(ticket)
          if ticket.is_a?(Ticket)
            ticket.audit.metadata[:system].merge!(metadata)
          elsif ticket.is_a?(SuspendedTicket)
            ticket.metadata = metadata
          end
        end

        def self.create_agent_comment_on_ticket(account, ticket, agent_comment, submitter, public, logger = Ticket.logger)
          ticket = account.tickets.find(ticket.id) # get rid of all state

          if flag_agent_not_in_group?(ticket, submitter)
            logger.info("flagging ticket due to AGENT_NOT_IN_GROUP")
            ticket.add_flags!([EventFlagType.AGENT_NOT_IN_GROUP])
          elsif flag_agent_assigned_only?(ticket, submitter)
            logger.info("flagging ticket due to AGENT_ASSIGNED_ONLY")
            ticket.add_flags!([EventFlagType.AGENT_ASSIGNED_ONLY])
          end

          ticket.will_be_saved_by(submitter)
          ticket.add_comment(body: agent_comment, is_public: public, author: submitter)
          ticket.save!
          ticket
        end

        def self.flag_agent_not_in_group?(ticket, submitter)
          submitter.agent_restriction?(:groups) && !ticket.group.try(:includes_user_with_id?, submitter.try(:id))
        end

        def self.flag_agent_assigned_only?(ticket, submitter)
          submitter.agent_restriction?(:assigned) && ticket.assignee_id != submitter.try(:id)
        end

        private

        def set_comment_to_private?
          return false unless state.account.has_email_light_agent_forwarding_first_comment_private?

          state.agent_forwarded && state.submitter.present? && !state.submitter.can?(:publicly, Comment)
        end

        def description(content)
          content = content.dup.presence || NO_CONTENT.dup
          if state.account.field_subject.try(:is_active?) || mail.subject.blank?
            content
          else
            "#{mail.subject}: #{content}".strip
          end
        end

        def suspended_ticket_properties
          properties = (state.properties || {}).merge(disable_triggers: disable_triggers?)
          properties[:redacted] = true if mail.redacted?
          properties[:brand_id] = state.brand_id if state.brand_id
          if state.agent_comment
            properties[:agent_comment] = {
              value: state.agent_comment,
              submitter_id: state.submitter.id,
              is_public: state.properties.try(:[], :is_public)
            }
          end
          properties[:from_zendesk] = true if mail.from_zendesk?
          properties[:html] = true if state.html_content

          properties
        end

        def use_subject?
          !mail.subject.blank? && state.account.field_subject.try(:is_active?)
        end

        # NB: Please consider the maximum allowed size of Audit metadata
        # when adding new fields to the metadata below.
        def metadata
          data = {}
          data[:message_id] = mail.message_id
          data[:client]     = mail_client if mail.mailer.present?

          if ip = ip_address
            data[:ip_address] = ip
          end

          if mail.eml_url.present? && remote_file = Zendesk::Mail.raw_remote_files.file_from_url(mail.eml_url)
            data[:raw_email_identifier] = remote_file.identifier
          end

          if mail.json_url.present? && remote_file = Zendesk::Mail.raw_remote_files.file_from_url(mail.json_url)
            data[:json_email_identifier] = remote_file.identifier
          end

          data[:machine_generated] = true if mail.from_zendesk?

          data
        end

        def ip_address
          return unless received = mail.headers["Received"].try(:last)
          received[IP_ADDRESS_PATTERN, 0]
        end

        def mail_client
          mail.mailer.first(AUDIT_METADATA_MAIL_CLIENT_SIZE_LIMIT)
        end

        def disable_triggers?
          mail.headers["X-Zendesk-Disable-Triggers"] == ["true"]
        end

        def created_at
          return unless time = mail.headers["X-Zendesk-Created-At"].try(:first)
          Time.at(time.to_i)
        end

        def remove_plus_id(address)
          Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(address)
        end

        def mark_as_unverified?
          identity && !identity.is_verified?
        end

        def identity
          @identity ||= state.account.user_identities.find_by_email(state.from.address)
        end

        def log_marked_as_unverified
          mail.logger.info("Ticket was marked as unverified! from address:'#{identity.value}', account:#{identity.account_id}, identity:#{identity.id}, message_id: #{mail.message_id}")
          statsd_client.increment('ticket.marked.unverified')
        end

        def statsd_client
          @statsd_client ||= Zendesk::StatsD::Client.new(namespace: :mail_ticket)
        end
      end
    end
  end
end
