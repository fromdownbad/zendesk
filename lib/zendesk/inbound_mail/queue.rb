module Zendesk
  module InboundMail
    module Queue
      autoload :Conveyor,     'zendesk/mail/queue/conveyor'
      autoload :Directory,    'zendesk/mail/queue/directory'
      autoload :EmailFile,    'zendesk/mail/queue/email_file'
      autoload :ErrorHandler, 'zendesk/mail/queue/error_handler'
      autoload :Worker,       'zendesk/mail/queue/worker'
    end
  end
end
