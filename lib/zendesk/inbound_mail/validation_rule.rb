require 'zendesk/outgoing_mail/mailer'

module Zendesk
  module InboundMail
    class ValidationRule
      attr_accessor :header, :pattern, :error

      def initialize(options)
        @pattern  = options.fetch(:pattern)
        @header   = options.fetch(:header)
        @error    = options.fetch(:error)
        @unless   = options[:unless]
        @if       = options[:if]
      end

      def deny?(mail, account = nil)
        return false if @unless && @unless.call(account, mail)
        return false if @if && !@if.call(account, mail)
        value(mail) =~ pattern
      end

      def value(mail)
        if mail.respond_to?(header)
          mail.send(header).try(:to_s)
        elsif mail.headers.key?(header)
          mail.headers[header].map(&:to_s).join(" ")
        end
      end

      # Returns the header name and the matched pattern (for internal use only).
      #
      # E.g. "Header mailer matches (?-mix:Kayako|MailChimp|Benchmail Agent|Clang|Autoresponder) pattern"
      def to_s
        "Header #{@header} matches #{@pattern} pattern"
      end

      class << self
        # Returns a list of hard validation rules.
        #
        # Hard rules will always be enforced; whitelisting does not alter the behavior.
        def hard_rules
          @hard_rules ||= Set.new
        end

        # Returns a list of soft validation rules.
        #
        # Soft rules can be circumvented by whitelisting.
        def soft_rules
          @soft_rules ||= Set.new
        end

        def automation_rules
          soft_rules + hard_rules
        end

        # Defines a hard validation rule.
        def hard_rule(header, options = {})
          add_rule(hard_rules, header, options)
        end

        # Defines a soft validation rule.
        def soft_rule(header, options = {})
          add_rule(soft_rules, header, options)
        end

        private

        def add_rule(rule_set, header, options = {})
          rule_set << ValidationRule.new(options.merge(header: header))
        end
      end

      AUTO_VACATION_SUBJECTS = Regexp.new([
        '^OOO$',
        'Out[ -]of[ -](the[ -])?Office(?!365)',
        'Auto Response',
        '^Email received',
        'Auto[- ]?Reply:?',
        'Autosvar',
        'Automatische Antwort',
        'Automatisk svar',
        'Ikke til stede',
        'Holiday Message',
        'Aus[êe]ncia Tempor[áa]ria',
        'Mensagem de aus[êe]ncia',
        '^Fora da empresa',
        '^Tak for din henvendelse',
        'niet aanwezig',
        '\bfrånvaro', # zd2636587
        'Automatic reply',
        'Op vakantie',
        '^Samodejni odgovor',
        'Automatisch antwoord',
        'Réponse (automatique )?en cas d\'absence',
        'Réponse automatique',
        'Puhkusel',
        'Kontorist väljas',
        'Automaattinen vastaus',
        '^On Sabbatical',
        '^Vacation Response', # Z1-4916170
        # zd542832
        '自動応答',
        '自动答复',
        '自動回覆',
        '자동 회신',
        'Risposta automatica',
        'Resposta automática',
        'Автоответ',
        'Respuesta automática',
        'Odpowiedź automatyczna',
        'Otomatik Yanıt',
        'Absence du bureau',
        # end zd542832
        '自动回复',                      # zd919070
        'Автоматический ответ',      # zd1479261
        'Poissa[:\s]',               # zd606442
        'Fuera de (la[ -])?Oficina', # zd629451 #zd1317552
        'Automaatne vastus',         # zd714654
        'Automatyczna odpowiedź',    # zd910713
        'Automatyczna odpowiedz',    # zd925655
      ].join('|'), 'i')

      AUTO_VACATION_I18N_SUBJECTS = Regexp.new([
        'træffes ikke',
        'Poza biurem',
        'Absence du bureau',
        'Fraværende'
      ].join('|'), 'i')

      ZENDESK_AUTOMATED_MAIL_DELIVERY_CONTEXTS = Regexp.new([
        'verify-email-',
        'new-password-',
        'device-',
        'user-profile-crypted-password-changed-'
      ].join('|'))

      hard_rule :from,                pattern: /system administrator|postmaster@|automaticresponse@/i, error: SuspensionType.SYSTEM_USER
      hard_rule :subject,             pattern: AUTO_VACATION_SUBJECTS, error: SuspensionType.AUTO_VACATION
      hard_rule 'X-Twitteremailtype', pattern: /is_following/, error: SuspensionType.AUTO_MAILER

      # Undeliverable patterns are now separated out for use by the BounceProcessor if enabled.
      hard_rule :subject, pattern: /#{BouncePatterns::SUBJECT_UNDELIVERABLE}/i,   error: SuspensionType.AUTO_DELIVERY_FAILURE
      hard_rule :subject, pattern: /^(NO RESPONSE REQUIRED|Automatic Response)/i, error: SuspensionType.AUTO_DELIVERY_FAILURE
      hard_rule :subject, pattern: /Autoresponse|Automatic reply/i,               error: SuspensionType.AUTO_DELIVERY_FAILURE
      hard_rule :subject, pattern: /^AUTO:/,                                      error: SuspensionType.AUTO_DELIVERY_FAILURE

      hard_rule 'Precedence',               pattern: /junk/i,                     error: SuspensionType.AUTO_MAILER
      hard_rule 'X-Autoreply',              pattern: /./,                         error: SuspensionType.AUTO_MAILER
      hard_rule 'X-Autorespond',            pattern: /./,                         error: SuspensionType.AUTO_MAILER
      hard_rule 'X-Autoresponder',          pattern: /./,                         error: SuspensionType.AUTO_MAILER
      hard_rule 'X-NetSuite',               pattern: /./,                         error: SuspensionType.AUTO_MAILER
      hard_rule 'X-Tender',                 pattern: /./,                         error: SuspensionType.AUTO_MAILER
      hard_rule :subject,                   pattern: AUTO_VACATION_I18N_SUBJECTS, error: SuspensionType.AUTO_VACATION

      # [ZD-356193]
      hard_rule 'X-Auto-Response-Suppress',
        pattern: /./,
        error: SuspensionType.AUTO_MAILER,
        if: ->(account, _) { account && account.has_email_suspend_mail_from_auto_response? }

      # [ZD-1957364] - keeping the Arturo that controls this as a per-account lever in case it's needed. It
      # can't be fully rolled out because there are legitimate emails that come in with it.
      hard_rule 'X-Intercom-Bin',
        pattern: /notification/i,
        error: SuspensionType.AUTO_MAILER,
        if: ->(account, _) { account && account.has_email_suspend_mail_from_intercom? }

      # [ZD-3407706]
      soft_rule :from, pattern: /regist_campaign-P030@tmail.benesse.ne.jp/i, error: SuspensionType.BLOCKLISTED

      soft_rule :from, pattern: /daemon|westernunionresponse|system administrator|no-reply@/i, error: SuspensionType.SYSTEM_USER
      soft_rule :from, pattern: /postmaster@|noreply@|wowcall@wowcall.com|bandwidth@pixiepoppins.org/i, error: SuspensionType.SYSTEM_USER
      soft_rule :from, pattern: /Auto[- ]?Reply:?|helpdesk@textcu.be/i, error: SuspensionType.SYSTEM_USER
      soft_rule :from,
        pattern: /welcome@freedcamp.com/i,
        error: SuspensionType.SYSTEM_USER,
        if: ->(account, _) { account && account.has_email_suspend_mail_from_welcome_freedcamp? }
      soft_rule :from,
        pattern: /mailer@doodle.com/i,
        error: SuspensionType.SYSTEM_USER,
        if: ->(account, _) { account && account.has_email_suspend_mail_from_mailer_doodle? }

      # rubocop:disable Layout/ExtraSpacing

      # From an automated ticketing system:
      soft_rule 'RT-Ticket',           pattern: /[a-z]/i, error: SuspensionType.AUTO_MAILER
      soft_rule :mailer,
        pattern: /Kayako|MailChimp|Benchmail Agent|Clang|Autoresponder/,
        error: SuspensionType.AUTO_MAILER
      soft_rule 'X-Mailing-Software',  pattern: /Just-Eat Mailing-System/, error: SuspensionType.AUTO_MAILER
      soft_rule 'Auto-Submitted',
        pattern: /^(?!no)./i,
        error: SuspensionType.AUTO_MAILER,
        unless: ->(_, mail) { mail.from_zendesk? }
      soft_rule :message_id,           pattern: /cmWinServer/, error: SuspensionType.AUTO_MAILER
      soft_rule 'X-EviteMessageID',    pattern: /./, error: SuspensionType.AUTO_MAILER
      soft_rule 'Delivered-To',        pattern: /Autoresponder/, error: SuspensionType.AUTO_MAILER
      soft_rule :from,                 pattern: /@iobox/i, error: SuspensionType.BLOCKLISTED
      soft_rule 'X-Amazon-Auto-Reply',
        pattern: /true/i,
        error: SuspensionType.AUTO_DELIVERY_FAILURE,
        if: ->(account, _) { account && account.has_reject_bounced_mail_from_amazon? }
      soft_rule Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT,
        pattern: ZENDESK_AUTOMATED_MAIL_DELIVERY_CONTEXTS,
        error: SuspensionType.FROM_ZENDESK_ACCOUNT,
        if: ->(account, mail) { account && account.has_suspend_automated_inbound_emails_from_zendesks? && mail.from_zendesk? }
      # rubocop:enable Layout/ExtraSpacing
    end
  end
end
