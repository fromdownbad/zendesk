module Zendesk
  module InboundMail
    class CloudmarkSpamScore
      HEADER_KEY         = 'X-CMAE-Score'.freeze
      PROBABLE_THRESHOLD = 90.0
      OBVIOUS_THRESHOLD  = 99.0

      # Remove this and replace the value of OBVIOUS_THRESHOLD when Arturo is GA'd
      OBVIOUS_THRESHOLD_V2 = 95.0

      attr_reader :mail

      def initialize(mail)
        @mail = mail
      end

      def probably_spam?
        value >= PROBABLE_THRESHOLD
      end

      def obviously_spam?
        value >= threshold
      end

      def value
        header_value.to_f if header_value.present?
      end

      def threshold
        if mail.account&.has_email_update_cloudmark_obvious_spam_threshold?
          mail.logger.info("CloudmarkSpamScore using OBVIOUS_THRESHOLD_V2 to determine obviously_spam")
          OBVIOUS_THRESHOLD_V2
        else
          OBVIOUS_THRESHOLD
        end
      end

      protected

      def header_value
        mail.headers[HEADER_KEY].try(:first)
      end
    end
  end
end
