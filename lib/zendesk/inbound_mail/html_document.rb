module Zendesk
  module InboundMail
    class HTMLDocument
      MAX_NOKOGIRI_TREE_DEPTH = -1 # Removes limit on tree depth

      class << self
        def next_element_with_text(element)
          return if element.nil?
          original_text = element.text.strip
          loop do
            break if element.name == 'body'
            element = element.try(:next) || element.parent
            break if element.text.present? && original_text != element.text.strip
          end
          element
        end

        def ancestor_with_equivalent_text(element)
          loop do
            if element && element.respond_to?(:parent) && element.parent.text.strip == element.text.strip
              element = element.parent
            else
              break
            end
          end
          element
        end

        def remove_self_and_preceding_elements(element)
          return unless element && element.respond_to?(:parent)
          remove_preceding_elements(element)
          parent = element.parent
          element.remove
          parent
        end

        def remove_preceding_elements(element)
          while element && element.name != 'body' && element.respond_to?(:parent) && (parent = element.parent)
            parent.children.slice(0, parent.children.index(element)).each &:remove
            element = parent
          end
        end

        def remove_if_empty(element)
          return if element.nil? || element.name == 'body'
          if element.css('img').empty? && (element.text.try(:blank?) || element.text.try(:match, /\A[[:space:]]*\z/).present?)
            parent = element.parent
            element.remove
            parent
          else
            element
          end
        end

        def remove_self_and_preceding_elements_old(element)
          loop { element.previous_sibling ? element.previous_sibling.remove : break }

          parent_element = element.parent
          element.remove
          parent_element
        end

        def remove_surrounding_containers(element)
          return if element.nil? || element.name == 'body' || !element.respond_to?(:parent)
          parent = element.parent
          if %w[blockquote div p].include? element.name.downcase
            element.replace(element.children)
          end
          remove_surrounding_containers(parent)
        end

        def remove_leading_brs(body)
          brs = []
          text_element = body.search('//text()').detect { |el| el.text.present? && el.text !~ /\A[[:space:]]*\z/ }
          body.traverse do |element|
            break if element == text_element || element.name == 'img'
            brs << element if element.name == 'br'
          end
          brs.each &:remove
        end

        def html_up_to(body, target)
          elements = depth_first_list(body)
          elements[(elements.index(target)..-1)].map &:remove
          body
        end

        def depth_first_list(element, visited = [])
          visited << element
          if (children = element.children)
            children.each do |child|
              depth_first_list(child, visited)
            end
          elsif element.next
            depth_first_list(element.next, visited)
          end
          visited
        end

        def content_present?(html)
          doc = Nokogiri::HTML5.parse(html, max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH)
          !doc.css('body').text.gsub(/[[:space:]]/, '').empty? || !doc.css('img').empty?
        end

        # DEPRECATED bad way to blindly pick a section to return from a mail document
        def last_ltr_or_rtl_section(fragment)
          return unless fragment
          fragment.css("div[dir='ltr'], div[dir='rtl']").last
        end

        def last_rply_fwd_msg_div(fragment)
          return unless fragment
          fragment.css("div#divRplyFwdMsg[dir='ltr'], div#divRplyFwdMsg[dir='rtl']").last
        end

        def next_sibling_div(fragment)
          return unless fragment

          # next_element returns the next Nokogiri::XML::Element sibling node
          while fragment = fragment.next_element
            break if fragment.name == 'div'
          end

          fragment
        end
      end
    end
  end
end
