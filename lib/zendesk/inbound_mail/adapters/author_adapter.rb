module Zendesk
  module InboundMail
    module Adapters
      # Responsible for handling Author persistence logic including error handling and reporting
      class AuthorAdapter
        def initialize(processing_state:, account:, mail_logger:, statsd_client:, save_attrs: [])
          @state = processing_state
          @account = account
          @logger = mail_logger
          @statsd_client = statsd_client
          @save_attrs = save_attrs.select { |attr| whitelisted_save_attrs.include?(attr) }
        end

        # Attempt to persist the Author with instrumentation to handle errors and record any pertinent information
        def save_with_instrumentation!
          with_error_handling { save }
        end

        private

        attr_reader :state, :account, :logger, :statsd_client, :save_attrs

        delegate :log, to: :logger
        delegate :author, :add_saved_new_users, :message_id, to: :state

        def save
          log("Starting save_author_changes!")

          log("ProcessingState author: #{author_data}")

          if author.present? && (author.new_record? || save_attrs_changed?)
            prepare_for_save
            save_author
          elsif author.present? && author.changed?
            log("Author was changed but not directly updated: #{author.changes.inspect}")
          else
            log("Not saving the author!")
          end
        end

        def whitelisted_save_attrs
          [:locale_id].freeze
        end

        def author_data
          {
            'exists?' => author.try(:present?),
            'changed?' => author.try(:changed?),
            'new?' => author.try(:new_record?),
            'locale changed?' => author.try(:locale_id_changed?)
          }
        end

        def save_attrs_changed?
          save_attrs.any? do |attr|
            author.public_send("#{attr}_changed?")
          end
        end

        def prepare_for_save
          log("Preparing to save the author: #{author.inspect}")
          log("Querying for existing UserEmailIdentity")
          email_identity = UserEmailIdentity.where(account_id: account.id, value: author.try(:email)).first

          log("#{email_identity ? 'Did' : 'Did not'} find existing UserEmailIdentity #{email_identity}")
          log("Expect User save to #{email_identity ? 'fail' : 'not fail'}")
        end

        def save_author
          was_new_record = author.new_record?
          author.save!

          if was_new_record
            log("Adding author #{author.inspect} to ProcessingState's saved_new_users")
            add_saved_new_users(author)
          end

          log("Author saved: #{author.inspect}")
        end

        def with_error_handling
          yield
        rescue StandardError => exception
          log("There was an error saving the author #{author.name}! #{exception.class}: #{exception.message}")
          record_metrics(exception)
          add_active_record_errors(author, [exception.message])
          raise exception
        end

        def record_metrics(exception)
          tags = ["detail:#{exception.class}", "source:save_author_changes"]

          if exception.is_a?(ActiveRecord::RecordInvalid)
            type = exception.message.end_with?("is already being used by another user") ? "duplicate_user" : "other"
            tags << "invalid_reason:#{type}"
          end

          statsd_client.increment("ticket.error", tags: tags)
        end

        def add_active_record_errors(author, errors)
          author.errors[:base].concat(errors)
        end
      end
    end
  end
end
