module Zendesk::InboundMail
  class RateLimit
    ACCOUNT_WHITELIST_LIMIT = 200
    ACCOUNT_WHITELIST_LIMIT_MULTIPLIER = 10
    ACCOUNT_LOOP_THRESHOLD_CEILING = 400
    DURATION = 1.hour

    SENDER_EXCEPTIONS = {
      'missedchat@livemercial.zendesk.com' => 120,
      'shiftmanager@jam.co.uk'             => 120,
      'sistema@smshosting.it'              => 120,
      'orders@dietchef.co.uk'              => 120,
      'info@dietchef.co.uk'                => 120,
      'transcoder@project-unicorn.com'     => 120,
      'mtl-scanner01@sanimax.com'          => 120,
      'qc-scanner01@sanimax.com'           => 120,
      'mailman@liquidcompass.net'          => 120,
      'inscription.employeur@jobboom.com'  => 120,
      'jobboompostinginterface@canoe.ca'   => 120,
      'mailbox@my.simulscribe.com'         => 120,
      'voicemail@ifbyphone.com'            => 120,
      'katrin.hagen@groupon.de'            => 120,
      'poloy6300@gmail.com'                => 120,
      'operacao@netmovies.com.br'          => 120,
      'service@ccavenue.com'               => 120,
      'orders@ccavenue.com'                => 120,
      'plimus@dudamobile.com'              => 120,
      'sales@plimus.com'                   => 120,
      'support@plimus.com'                 => 120,
      'vendors@plimus.com'                 => 120,
      'roger.capote@porky.com'             => 120,
      'ebay@ebay.de'                       => 1_000, # ZD: 301983
      'fba-ship-confirm@amazon.com'        => 120,
      'dferry@gilt.com'                    => 150,   # ZD: 266912 for account 80487
      'automated@airbnb.com'               => 200,   # 1st ZD: 266526 => 100; 2nd ZD: 333083 => 200 for account 31570
      'notification@unteamworks.org'       => 200,
      'no-reply@olark.com'                 => 120,   # ZD: 293394
      'groupon@optimo.it'                  => 150,
      'griffincustomerservices@gmail.com'  => 120,
      'femapemicfdprod@mapfre.com.mx'      => 120,   # ZD: 945468 for account 472413
      'hello@delighted.com'                => 120,   # ZD: 894501 for account 547534
      'dmca.support@redbubble.com'         => 2_500, # ZD: 909015 for Redbubble (account 558695)
      'auftragvestel@vangerow.de'          => 800,   # ZD: 394276 for account 191264
      'support@nationalcrimecheck.com.au'  => 400,   # ZD: 1264064 for account 34989
      'ticketschedule@wework.com'          => 200,   # ZD: 3008292 for account 533533
      'donotreply@niagarawater.com'        => 1_000, # JIRA: EM-2263 for account 1470967
      'motor@kleinschmidt.com'             => 1_000, # JIRA: EM-2263 for account 1470967
      'motorcs@kleinschmidt.com'           => 1_000, # JIRA: EM-2263 for account 1470967
      'srvi2prod1@anheuser-busch.com'      => 1_000, # JIRA: EM-2263 for account 1470967
      'kleinschmidt_pm@kleinschmidt.com'   => 1_000, # JIRA: EM-2263 for account 1470967
      'noreply@tms.blujaysolutions.net'    => 1_000, # JIRA: EM-2263 for account 1470967
      'motorkli@kleinschmidt.com'          => 1_000, # JIRA: EM-3360 for account 1470967
      'motorcskli@kleinschmidt.com'        => 1_000, # JIRA: EM-3367 for account 1470967
      'workdayhr@pfizer.com'               => 1_000, # JIRA: EM-2792 for account 682888, see comment thread.
      'noreply@shopee.com'                 => 500,   # JIRA: EM-2956 for account 2165099
      'noreply@support.lazada.co.th'       => 500,   # JIRA: EM-2956 for account 2165099
      'reportingservices@15below.com'      => 400,   # JIRA: EM-2996 for account 3957552
      'sending@geldfuerflug.de'            => 400,   # JIRA: EM-2996 for account 3957552
      'customersupport@commercehub.com'    => 500,   # JIRA: EM-4105 for account 1979552
      'priceassurance@medline.com'         => 400    # JIRA: EM-4293 for account 9040347
    }.freeze

    # These should probably have only name@subdomain.zendesk.com style addresses, however, the Pinterest variants may still be needed
    RECIPIENT_EXCEPTIONS = {
      'automated@poco-kundendienst-b2b.zendesk.com' => 100,     # EM-2973 (account 1919391)
      'copyright@pinterest.com' => 100,                         # ZD: 3085985 for Pinterest (account 139822)
      'copyright@pinterest.zendesk.com' => 100,                 #
      'KD.b2b@poco.de' => 100,                                  # EM-2973 (account 1919391)
      'marketingcoordinators@guildmortgage.zendesk.com' => 200, #
      'sfhelp@guildmortgage.zendesk.com' => 200,                #
      'support@z3n-pod998-email.zendesk-staging.com' => 30,     # Staging
      'support@z3n-pod999-email.zendesk-staging.com' => 30,     # Staging
      'trademark@pinterest.com' => 100,                         #
      'trademark@pinterest.zendesk.com' => 100,                 #
      'website@guildmortgage.zendesk.com' => 200                # ZD: 3096962 for Guild Mortgage (account 1147644)
    }.freeze

    def initialize(account, sender, recipient, logger = Rails.logger)
      @account   = account
      @sender    = sender
      @recipient = Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(recipient)
      @logger = account.has_email_log_inbound_mail_rate_limit? ? logger : Rails.logger
    end

    def exceeded?
      return false if account.has_email_ignore_rate_limits?
      recently_sent_email_count > limit
    end

    def greatly_exceeded?
      return false if account.has_email_ignore_rate_limits?

      if account.has_email_log_inbound_mail_rate_limit?
        percent_exceeded = ((recently_sent_email_count / limit.to_f) * 100).round

        if percent_exceeded >= 100
          # round percent to nearest hundred so we don't have too many buckets (i.e. 100/200/300)
          rounded_percent_exceeded = 100 * (percent_exceeded / 100.0).round
          statsd_client.histogram(:percent_exceeded, rounded_percent_exceeded)
          logger.info("Inbound mail rate limit exceeded: account: #{account.id}, percent_exceeded: #{rounded_percent_exceeded}")
        elsif percent_exceeded > 85 && percent_exceeded < 100
          statsd_client.histogram(:rate_limit_utilization, percent_exceeded)
          logger.info("Inbound mail rate limit nearly exhausted: account: #{account.id}, rate_limit_utilization_percentage: #{percent_exceeded}")
        end
      end

      recently_sent_email_count > (2 * limit)
    end

    def message(greatly)
      "count #{recently_sent_email_count} > threshold #{(greatly ? 2 : 1) * limit} mails in the last #{DURATION.inspect}."
    end

    private

    attr_reader :account, :sender, :recipient, :logger

    def account_sender_rate_limit
      @account_sender_rate_limit ||= InboundMailRateLimit.find_by(
        account_id: account.id,
        email: sender,
        is_sender: true,
        is_active: true
      )
    end

    def account_recipient_rate_limit
      @account_recipient_rate_limit ||= InboundMailRateLimit.find_by(
        account_id: account.id,
        email: recipient,
        is_sender: false,
        is_active: true
      )
    end

    def limit
      @limit ||= begin
        global_limit = begin
          global_limits = []
          global_limits << account.loop_threshold
          global_limits << SENDER_EXCEPTIONS[sender] if SENDER_EXCEPTIONS.key?(sender)
          global_limits << RECIPIENT_EXCEPTIONS[recipient] if RECIPIENT_EXCEPTIONS.key?(recipient)

          if account.has_email_account_whitelist_multiplier?
            multiplied_threshold = [account.loop_threshold * ACCOUNT_WHITELIST_LIMIT_MULTIPLIER, ACCOUNT_LOOP_THRESHOLD_CEILING].min
            global_limits += [multiplied_threshold, ACCOUNT_WHITELIST_LIMIT] if sender_on_account_whitelist? || Rails.env.development?
          else
            global_limits << ACCOUNT_WHITELIST_LIMIT if sender_on_account_whitelist? || Rails.env.development?
          end

          global_limits.max
        end

        return global_limit unless account.has_email_inbound_mail_rate_limits?

        sender_rate_limit = account_sender_rate_limit.present? ? account_sender_rate_limit.rate_limit : 0
        recipient_rate_limit = account_recipient_rate_limit.present? ? account_recipient_rate_limit.rate_limit : 0
        account_limits = [sender_rate_limit, recipient_rate_limit].max

        return account_limits if account_limits > 0

        global_limit
      end
    end

    def recently_sent_email_count
      @recently_sent_email_count ||= account.inbound_emails.
        where(from: sender).
        where('created_at > ?', DURATION.ago.utc).
        from("#{account.inbound_emails.table_name} force index (index_inbound_emails_on_created_at_and_account_id_and_from)").
        count
    end

    def sender_on_account_whitelist?
      account.domain_whitelist.to_s.downcase.include?(sender)
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'inbound_mail_rate_limit')
    end
  end
end
