module Zendesk::MtcMpqMigration::ZendeskMail::ProcessorChainSupport
  EXTERNAL_MACHINE_IDENTITIES = [:intercom_notification?].freeze

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: :processor_chain_support)
  end

  def redacted?
    [self, parts].flatten.any? { |part| part.headers.key?('X-Zendesk-Redacted') }
  end

  def from_auto_mailer?
    Zendesk::InboundMail::ValidationRule.automation_rules.any? { |rule| rule.deny?(self) } || bulk?
  end

  def client
    @client ||= Zendesk::InboundMail::Client.find_by_mail(self).tap do |client|
      logger.info("Client: #{client.inspect}") if client
    end
  end

  # All parties related to this email who can potentially get CC'ed
  def parties
    # Include all REPLY_TO:, FROM:, TO: and CC: addresses
    addresses = []
    addresses << reply_to if reply_to.present?
    addresses << from     if reply_to.blank? && from.present?
    addresses += to       if to.present?
    addresses += cc       if cc.present?
    addresses.select { |addr| addr.address.present? }.uniq
  end

  # Identical to ProcessorChainSupport#parties except that it removes the
  # plus ID from the reply_to or from address, if applicable. This prevents
  # a longtime Zendesk-to-Zendesk unprocessable email bug documented in
  # JIRA tickets EM-979 and EM-1734.
  # ProcessorChainSupport#parties should still remain in the code, because
  # it is used to set state.ticket.recipients_for_latest_mail, which sends
  # notification emails in notification.rb. Plus IDs should not be stripped
  # here, because they are sometimes used for ticket threading.
  def parties_with_sender_plus_id_removed
    # Include all REPLY_TO:, FROM:, TO: and CC: addresses
    # If REPLY_TO or FROM (whichever is used) is a Zendesk address and has
    # a plus ID (e.g. support+id123456@zendesk-test.com), strip out the plus ID
    addresses = []
    addresses << address_without_address_plus_id(reply_to) if reply_to.present?
    addresses << address_without_address_plus_id(from)     if reply_to.blank? && from.present?
    addresses += to                                        if to.present?
    addresses += cc                                        if cc.present?
    addresses.select { |addr| addr.address.present? }.uniq
  end

  def address_without_address_plus_id(address)
    fixed_address = address.dup
    fixed_address.address = Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(fixed_address.address)
    fixed_address
  end

  def all_parties
    # Ex. "from@example.com reply-to@example.com to@example.com cc1@example.com"
    parties.map(&:address).sort.join(" ")
  end

  def direct_recipients
    # Include all TO: and CC: addresses
    recipients = []
    recipients += to if to.present?
    recipients += cc if cc.present?
    recipients.uniq!

    # Ex. [to@example.com, cc1@example.com, cc2@example.com]
    recipients.map(&:address).map(&:downcase).compact.uniq
  end

  def original_recipient
    ["X-Forwarded-For", "X-Envelope-To", "X-Orig-To", "Resent-From", "X-Yahoo-Forwarded"].each do |header|
      next unless addresses = headers[header].presence
      address = addresses[0]
      logger.info("Recipient #{address} found by header #{header}")
      return address
    end

    nil
  end

  # Imitates mail.smtp_envelope_from but uses the json to return the value
  # See https://github.com/zendesk/mail/blob/master/lib/mail/message.rb#L1055
  def smtp_envelope_from
    headers['Return-Path'].try(:first) || headers['Envelope-From'].try(:first) || from.try(:address)
  end

  def bulk?
    precedence_bulk? || email_list_bulk?
  end

  def precedence_bulk?
    headers.key?("Precedence") && headers["Precedence"].join =~ /junk|bulk|list|auto_reply/i
  end

  def email_list_bulk?
    headers.key?("List-Unsubscribe")
  end

  def from_google_apps_group?
    headers.key?("X-Google-Expanded") || headers.key?("X-pstn-nxpr") ||
    headers.key?("X-Google-Group-Id") || (headers["List-Help"] || []).join.index("google.com")
  end

  def from_permitted_bulk_mailer?
    headers["message-id"].join("\n") =~ /mandrillapp.com>?$/i
  end

  def from_zopim?
    from.present? && from.address == 'noreply@zopim.com'
  end

  def delete_remote_attachments
    urls = attachments.map(&:remote_url).compact

    urls.each do |remote_url|
      remote_file = Zendesk::Mail.attachment_remote_files.file_from_url(remote_url)
      handle_remote_file_deletion(remote_file, remote_url)
    end
  end

  def delete_remote_files
    urls = [remote_url, eml_url, json_url].compact

    urls.each do |remote_url|
      remote_file = Zendesk::Mail.raw_remote_files.file_from_url(remote_url)
      handle_remote_file_deletion(remote_file, remote_url)
    end

    delete_remote_attachments
  end

  def handle_remote_file_deletion(remote_file, remote_url)
    return unless remote_file

    logger.warn("Account object not defined, cannot check Arturo settings, falling back to default remote file delete behavior") unless account

    if account.try(:has_email_switch_off_redis_deletions?)
      logger.info("Arturo :email_switch_off_redis_deletions is enabled. Did not attempt to delete file at #{remote_url}")
      statsd_client.increment(:skip_deletion, tags: ["reason:file_deletions_disabled"])
    else
      delete_remote_file(remote_file, remote_url)
    end
  end

  def delete_remote_file(remote_file, remote_url)
    logger.info("Deleting remote file at #{remote_url}")
    remote_file.delete
    logger.info("Deleted remote file at #{remote_url}")
  rescue StandardError => exception
    logger.info("Rescuing #{exception.class}: #{exception.message}")
    logger.info("Remote file delete failed due to Redis issue: #{remote_url}")
    statsd_client.increment(
      :skip_deletion,
      tags: ["reason:exception_raised", "error_type:#{exception.class}"]
    )
  end

  def from_external_machine?
    EXTERNAL_MACHINE_IDENTITIES.any? { |identity| send(identity) }
  end

  def intercom_notification?
    reply_to && reply_to.address =~ /^(n|notifications)\+.*@(.*\.)?intercom-mail\.com$/ && headers["X-Intercom-Bin"].try(:first) =~ /^(notification|proven_users)$/i
  end

  def from_zendesk_address?
    !!(::Zendesk::InboundMail::Patterns::ZENDESK_DOMAIN_ADDRESS =~ reply_to_or_from_address)
  end

  def from_collaboration_app?
    from_zendesk? && message_id =~ /^<c11n\+[^\+]+\+[^\+]+@.*zendesk.+>/
  end

  def zendesk_message_id?
    !!(message_id =~ ::Zendesk::InboundMail::Patterns::ZENDESK_MESSAGE_ID)
  end

  def readable_parts
    [self].concat(parts).map(&:body).map(&:to_s).select(&:valid_encoding?) # binary parts are not encoded in utf-8
  end
end
