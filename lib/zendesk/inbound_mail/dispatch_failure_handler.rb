module Zendesk::InboundMail
  class DispatchFailureHandler
    class << self
      def notify(path, exception, message = nil, metadata = {})
        error_message = "processing mail at #{path}"
        error_message = "#{message}: #{error_message}" if message.present?
        error_message = "#{exception}: #{error_message}"

        ZendeskExceptions::Logger.record(exception, location: self, message: error_message, fingerprint: exception.to_s, metadata: metadata)

        statsd_client.increment("processing_error.error", tags: ["detail:#{exception.class}"])
      end

      def warn(message)
        logger.warn(message)
      end

      def log(path, exception)
        logger.info("Unhandled exception while Processing mail at #{path}, #{exception.class.name}: #{exception.message} \n#{exception.backtrace&.join("\n")}")
      end

      private

      def logger
        Zendesk::Mail.logger
      end

      def statsd_client
        Zendesk::InboundMail::StatsD.client
      end
    end
  end
end
