require 'cld'
require 'stringio'

module Zendesk::InboundMail
  # A forward header is expected to have a group of headers, one of which is a From address.
  # The header group must be separated from the rest of the message by empty lines or delimiters.
  #
  #### A typical forward:
  #
  # I'm forwarding a message...
  #
  # ---- Begin forwarded message   # => header.upper_delimiter / header.begin
  #                                # => header.upper_empty_line
  # From: hello@example.com        # => header.start_of_from_header / header.from_address
  # To: somebody@example.com
  # Subject: hi
  #                                # => header.end_of_lower_empty_line / header.end
  # Hello...
  #
  #### A minimal forward:
  ##
  ## This is simply the From marker surrounded by empty lines.
  ## The upper empty line isn't necessary since it's the start of the message.
  #
  #                                # => header.begin
  # From: hello@example.com        # => header.start_of_from_header / header.from_address
  #                                # => header.end_of_lower_empty_line / header.end
  # Original message...
  #
  #
  #### Limitations
  ##
  ## The "From:" key and delimiter are used as anchoring points.
  ## If either appears above the forwarded message, we'll fail to extract correctly:
  #
  #                                # => header.begin
  # I'm forwarding a message.
  # It's "From: the past"          # => header.start_of_from_header
  #                                # => header.end_of_lower_empty_line / header.end
  # From: hello@example.com
  # To: somebody@example.com
  # Subject: hi
  #
  # Hello...
  #
  class ForwardHeader
    include LocalizedHeaders

    attr_reader :text, :mail
    delegate :account, to: :mail

    # From: John Doe <<john.doe@example.com>> asdf
    # => mentor@example.com
    LOOSE_EMAIL_SOURCE = FIND_EMAIL_PATTERN.source
    # From: John\nDoe <<john.doe@example.com>> asdf
    # => "John Doe", "john.doe@example.com"
    USER_CAPTURE_GROUP = /(.*?\n?.*?)(#{LOOSE_EMAIL_SOURCE})/

    FROM_PATTERN        = /(?:#{FROM_KEYS}[[:space:]]?#{COLON}|On.*,)#{USER_CAPTURE_GROUP}/ix
    FROM_HEADER_PATTERN = /^\W?\s*+(#{FROM_KEYS}[[:space:]]?|On.*?#{LOOSE_EMAIL_SOURCE}.*?wrote)#{COLON}/i

    FROM_HEADER_PATTERN_PREFIX = /^\W?\s*+/
    FROM_HEADER_PATTERN_SUFFIX = /(#{FROM_KEYS}[[:space:]]?|On.*?#{LOOSE_EMAIL_SOURCE}.*?wrote)#{COLON}/i

    FROM_HEADER_PATTERN_V2 = /#{FROM_HEADER_PATTERN_PREFIX}.*#{FROM_HEADER_PATTERN_SUFFIX}/i

    # Patterns to identify the requester from the Forward on-wrote or headers section.
    ON_WROTE_PATTERN = /^\W?\s*+(On.*,[[:space:]]?#{USER_CAPTURE_GROUP}.*?wrote#{COLON})/i

    # Cleanup patterns due to the requester patterns being greedy
    TO_PATTERN     = /To\s*?#{COLON}\s*?(.*?)[\n\r]/i            # To: somebody@example.com
    CC_PATTERN     = /Cc\s*?#{COLON}\s*?(.*?)[\n\r]/i            # Cc: somebody@example.com
    MAILTO_PATTERN = /\[?\s*mailto\s*#{COLON}\s*\[?\s*(.*?)\]/i  # [mailto:somebody@example.com] or mailto:[somebody@example.com]

    # TODO: Does "Begin forwarded message" and "Forward" need to be i18n-ized?
    # /
    #  ^\s?(----+\s#{FROM_KEYS}|                 # ----\nFrom: Me
    #       ----.*?\n?.*?----|                   # ---- Original Message ----, ----------------
    #       Begin\sforwarded\smessage:|          # Begin forwarded message:
    #       ----.*?Forward|                      # ------ Forwarded Message
    #       ________________________________)    # Underscores via Outlook
    # /ix
    UPPER_DELIMITER = /^\s?(----+\s#{FROM_KEYS}|----.*?\n?.*?----|Begin\sforwarded\smessage#{COLON}|----.*?Forward|________________________________)/ix

    # /
    #  ((?:\n|\r\n)  # New line
    #   \s*(?:>\s)?  # Optionally quoted
    #  ){2}          # Two in a row
    # /x
    EMPTY_LINE = /((?:\n|\r\n)\s*(?:>\s)?){2}/x

    # Optional quote/single space followed by a header key and value
    HEADER_LINE = /^>? ?((#{FROM_KEYS}|([[:alnum:]\-\*]+))#{COLON}.*?)$/

    def initialize(text, mail)
      @text = text
      @mail = mail
    end

    def beginning
      upper_delimiter ? upper_delimiter : [upper_empty_line, start_of_from_header].compact.sort.first
    end

    def ending
      [end_of_from_line, end_of_header_group, end_of_lower_empty_line].compact.sort.last
    end

    def to_s
      if start_of_from_header.nil? || beginning.nil? || ending.nil?
        log "Can't find the forward's headers"
        return ''
      end

      text.slice(beginning...ending)
    end

    # Removes `mailto:`, `Cc:`, and `To:` from the text and searches the `From:` section.
    # Returns the from address as "Customer Name <address@example.com>" format if matched.
    def from_address
      return @from_address if defined?(@from_address)

      if (match = cleaned_forward_headers.match(FROM_PATTERN))
        @from_address = %("#{match[1].gsub(/\s+/, " ").gsub(/[<>"']/, "").strip}" <#{match[2]}>)
      else
        log "Can't find the forward's from address"
        nil
      end
    end

    def headers_language
      @headers_language ||= CLD.detect_language(cleaned_forward_headers)
    end

    protected

    def cleaned_forward_headers
      @cleaned_forward_headers ||= to_s.gsub(cleanup_pattern, "\\1")
    end

    def cleanup_pattern
      if account.has_email_cleanup_to_headers_from_forward_header?
        debug_log "Removing MAILTO, CC and TO patterns from the forward headers"
        Regexp.union(MAILTO_PATTERN, CC_PATTERN, TO_PATTERN)
      else
        debug_log "Removing MAILTO, and CC patterns from the forward headers"
        Regexp.union(MAILTO_PATTERN, CC_PATTERN)
      end
    end

    def start_of_from_header
      return @start_of_from_header if defined?(@start_of_from_header)
      @start_of_from_header = identify_from_header_start_index
    end

    def identify_from_header_start_index
      debug_log "Searching for from headers using FROM_HEADER_PATTERN"

      if (matched_from_header = text.match(FROM_HEADER_PATTERN))
        matched_from_header.begin(1)
      elsif account.has_email_yahoo_forward_header_parsing? && (matched_from_header = text.match(FROM_HEADER_PATTERN_V2))
        matched_from_header.begin(1)
      else
        log "Can't find the forward from's header"
        nil
      end
    end

    def upper_delimiter
      debug_log "Using UPPER_DELIMITER to find upper delimiter"
      text.rindex(UPPER_DELIMITER, start_of_from_header)
    end

    def upper_empty_line
      text.rindex(EMPTY_LINE, start_of_from_header)
    end

    def end_of_from_line
      end_of("\n", start_of_from_header)
    end

    def end_of_lower_empty_line
      end_of(EMPTY_LINE, start_of_from_header)
    end

    def end_of_header_group
      return nil if header_group.blank?
      text.index(header_group) + header_group.size
    end

    def header_group
      headers = []

      debug_log "Using HEADER_LINE to find upper header group"
      text[start_of_from_header..text.size].each_line do |line|
        break if line !~ HEADER_LINE
        headers << line
      end

      headers.join
    end

    def end_of(pattern, offset)
      if (match_data = text.match(pattern, offset))
        match_data.end(0)
      end
    end

    def log(message)
      mail.logger.try :info, message
    end

    def debug_log(message)
      return unless account.has_email_debugging_mode?
      log "[DEBUG] #{message}"
    end
  end
end
