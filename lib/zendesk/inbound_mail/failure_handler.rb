module Zendesk
  module InboundMail
    module FailureHandler
      module Handling
        def self.included(base)
          base.extend(ClassMethods)
        end

        module ClassMethods
          def handle(state, mail, exception)
            new(state, mail, exception).handle
          end
        end

        delegate :account, :inbound_email, to: :state
        delegate :logger, to: :mail

        attr_reader :state, :mail, :exception

        def initialize(state, mail, exception)
          @state     = state
          @mail      = mail
          @exception = exception
          @statsd_client = Zendesk::InboundMail::StatsD.client
        end

        def handle
          raise 'Need to define custom handling logic'
        end
      end

      class Rejection
        include Handling

        def handle
          logger.warn("#{rejection_type} reject: #{exception.message}")
          @statsd_client.increment("ticket.rejected", tags: ["type:#{rejection_type}"])
          state.rejected!

          return unless account

          account.on_shard do
            if duplicate_mail_reject_error?
              logger.info("Not deleting remote files for duplicate #{mail.message_id}, ticket_id: #{existing_ticket_id || 'nil'}")
            else
              logger.info("Deleting remote files for #{mail.message_id}")
              mail.delete_remote_files
            end
            handle_inbound_email if inbound_email
          end
        end

        private

        def rejection_type
          duplicate_mail_reject_error? ? :soft : :hard
        end

        def duplicate_mail_reject_error?
          exception.is_a?(Zendesk::InboundMail::DuplicateMailRejectException)
        end

        def existing_ticket_id
          existing_inbound_email.try(:ticket_id)
        end

        def existing_inbound_email
          @existing_inbound_email ||= account.inbound_emails.where(message_id: mail.message_id).first
        end

        def handle_inbound_email
          if state.suspension && uncategorized?
            inbound_email.mark_by_system(state.suspension)
            @statsd_client.increment(:marked_by_system, tags: ["suspension_type:#{SuspensionType[state.suspension].name}"])
          end

          inbound_email.reject
          inbound_email.save!
        end

        def uncategorized?
          inbound_email.category_id.blank?
        end
      end
    end
  end
end
