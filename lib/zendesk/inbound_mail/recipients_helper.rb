module Zendesk::InboundMail::RecipientsHelper
  def self.latest_email_user_ids(ticket, latest_audit_recipients = nil)
    value = (latest_audit_recipients ? latest_audit_recipients : ticket.to_and_ccs_for_latest_mail)
    ticket.account.user_identities.email.where(value: value).pluck(:user_id).uniq
  end

  # This method returns the support address that is part of the recipients in the incoming email
  def self.find_recipient_address(mail, account)
    recipients = mail.recipients.map { |address| Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(address) }

    account.recipient_addresses.by_email(recipients).min_by do |ra|
      [
        ra.email == account.backup_email_address ? 1 : 0,
        ra.default_host? ? 1 : 0,
        recipients.index(ra.email)
      ]
    end
  end
end
