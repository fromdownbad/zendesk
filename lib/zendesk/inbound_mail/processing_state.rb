module Zendesk::InboundMail
  class ProcessingState < Struct.new( # rubocop:disable Style/StructInheritance
    :account,
    :agent_comment,
    :agent_forwarded,
    :author,
    :brand_id,
    :closed_ticket,
    :comment,
    :connect_campaign_recipient_id,
    :deferred_stats,
    :email_containing_text_api,
    :flags,
    :from,
    :from_header,
    :from_header_whitelisted,
    :full_html_content,
    :full_plain_content,
    :html_content,
    :id_location,
    :inbound_email,
    :locale_detected,
    :message_id,
    :original_recipient_address,
    :original_author,
    :path,
    :plain_content,
    :properties,
    :recipient,
    :requester_is_on_email,
    :saved_new_users,
    :sender_auth_header_results,
    :sender_auth_passed,
    :submitter,
    :suspension,
    :ticket,
    :via_recipient_nice_id,
    :whitelisted
  )

    def initialize(account = nil)
      self.whitelisted = false
      self.from_header_whitelisted = false
      self.account = account
      self.agent_forwarded = false
      self.email_containing_text_api = false
      self.saved_new_users = []
      self.sender_auth_header_results = []
      self.sender_auth_passed = false
      self.requester_is_on_email = false
      @new_ticket = false
    end

    def submitter
      self[:submitter] || author
    end

    def whitelisted?
      whitelisted
    end

    def from_header_whitelisted?
      from_header_whitelisted
    end

    def sender_auth_passed?
      sender_auth_passed
    end

    def sender_auth_failed?
      !sender_auth_passed
    end

    def sender_auth_status
      sender_auth_passed ? :PASSED : :FAILED
    end

    def prefix
      message_id.to_s
    end

    def suspended?
      suspended_ticket? || suspension.present?
    end

    def flagged?
      flags.any?
    end

    # Valid after TicketCreatorProcessor has run
    def suspended_ticket?
      ticket.is_a?(SuspendedTicket)
    end

    # Valid after TicketFinderProcessor has run
    def followup_ticket?
      closed_ticket || ticket && ticket.try(:via_id) == ViaType.CLOSED_TICKET
    end

    # Valid after TicketFinderProcessor has run
    def new_ticket?
      !followup_ticket? && ticket && (ticket.new_record? || @new_ticket)
    end

    # Valid after TicketFinderProcessor has run
    def comment?
      !followup_ticket? && !new_ticket?
    end

    def mark_new_ticket
      @new_ticket = true
    end

    def locale_set_by_detection?
      locale_detected.present?
    end

    def rejected!
      @rejected = true
    end

    def rejected?
      !!@rejected
    end

    def authorize!
      @authorized = true
    end

    def authorized?
      !!@authorized
    end

    def existing_ticket?
      ticket && !ticket.new_record?
    end

    def from=(*args)
      @agent = nil
      super
    end

    # This is a less than ideal method for ProcessingState. It mixes behavior into this class that is very similar to
    # that of Processors (UserProcessor?). It also adds complexity to other methods in ProcessingState (e.g. #from=).
    # Ideally this method should be removed all together and its behavior relocated to a Processor or other appropriate
    # domain object.
    def agent
      return @agent unless @agent.nil?
      @agent = (from && account.find_user_by_email(from.address, scope: account.agents)) || false
    end

    def group_restricted_submitter?
      submitter.is_agent? && submitter.agent_restriction?(:groups)
    end

    def add_flag(flag, options = {})
      flags.add(flag)
      flags_options.merge!(flag => options)
    end

    def flags
      @flags ||= AuditFlags.new
    end

    def flags_options
      @flags_options ||= {}
    end

    # if we manipulate the plain text content we cannot use the html
    def plain_content=(c)
      return if c == plain_content
      super(c.freeze)

      self.html_content = nil
    end

    def deferred_stats
      @deferred_stats ||= {}
    end

    def record_deferred_stat(key, stat)
      deferred_stats[key] = stat
    end

    def add_saved_new_users(users)
      saved_new_users << users
      saved_new_users.flatten!
      saved_new_users.compact!
    end

    def author_is_email_cc?
      return false if !(ticket || closed_ticket) || !author
      return @author_is_email_cc unless @author_is_email_cc.nil? # memoize false
      @author_is_email_cc = (closed_ticket || ticket).is_email_cc?(author)
    end

    def author_is_follower?
      return false if !ticket || !author
      return @author_is_follower unless @author_is_follower.nil? # memoize false
      @author_is_follower ||= ticket.is_follower?(author)
    end
  end
end
