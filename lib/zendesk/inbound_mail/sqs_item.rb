module Zendesk::InboundMail
  class SQSItem
    attr_reader :created_at, :path, :receipt_handle, :receive_count, :value
    attr_accessor :logger

    def initialize(value, created_at, path, receipt_handle, receive_count)
      @value = value
      @created_at = created_at
      @path = path
      @receipt_handle = receipt_handle
      @receive_count = receive_count
    end

    def name
      File.basename(path)
    end

    def ==(other)
      other.respond_to?(:name) &&
        name == other.name
    end
  end
end
