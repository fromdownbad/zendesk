module Zendesk
  module InboundMail
    class Website
      def call(*)
        [404, { 'Content-Type' => 'application/json' }, [{ message: 'Not found' }.to_json]]
      end
    end
  end
end
