require 'active_record/comments'

module Zendesk
  module UserViews
    class Executer
      attr_reader :builder
      attr_reader :total_entries_builder
      attr_reader :view
      attr_reader :entity
      attr_reader :params
      attr_reader :user, :subsystem
      attr_reader :count_time, :select_time

      def self.users_for_view(view, current_user, params = {})
        new(view, current_user, params).find_users
      end

      def initialize(view, current_user, params = {})
        @view                  = view
        @params                = params
        @builder               = Zendesk::UserViews::QueryBuilder.new(view, params)
        @total_entries_builder = Zendesk::UserViews::QueryBuilder.new(view, params)
        @entity                = view.entity
        @user                  = current_user
        @subsystem             = params[:subsystem_name]
      end

      def find_users
        ActiveRecord::Comments.comment(comment) do
          entities_sql      = builder.query
          total_entries_sql = total_entries_builder.query_total_entries
          total_entries     = -1
          records           = nil

          find_with_benchmark do
            @count_time = Benchmark.realtime do
              total_entries = entity.count_by_sql(total_entries_sql)
            end
            statsd_client.timing('count', (@count_time * 1000).to_i, tags: total_entries_builder.statsd_tags(false))

            @select_time = Benchmark.realtime do
              records = WillPaginate::Collection.create(builder.page, builder.per_page, total_entries) do |pager|
                pager.replace(entity.find_by_sql(entities_sql))
              end
            end
            statsd_client.timing('select', (@select_time * 1000).to_i, tags: total_entries_builder.statsd_tags(true))

            records
          end

          records
        end
      end

      private

      def comment
        "user_rule:#{view.to_param}, user:#{user.to_param}, shard:#{view.account.shard_id}, subsystem:#{subsystem}"
      end

      def find_with_benchmark
        records = []
        elapsed_time = Benchmark.realtime do
          records = yield
        end
        logger.info(%(Find#{view.view_type}: #{view.class}, #{view.id}, #{view.account_id}, #{records.length}, #{elapsed_time}, #{view.definition.conditions_all.length}, #{view.definition.conditions_any.length})) unless Rails.env.production?
      end

      def logger
        Rails.logger
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["api", "v2", "user_views"])
      end
    end
  end
end
