module Zendesk
  module UserViews
    class DefinitionValidator < Validator
      def validate
        unless view.definition.is_a?(OpenStruct)
          view.definition = nil
          add_error("invalid_conditions_you_must_select_condition")
          return
        end

        if view.definition.conditions_all.empty?
          add_error("all_condition_missing", error: "BlankValue")
          return
        end
      end
    end
  end
end
