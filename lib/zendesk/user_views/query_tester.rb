# See Zendesk::Maintenance::Jobs::UserViewQueryTesterJob for more information
class Zendesk::UserViews::QueryTester
  attr_reader :results

  def initialize
    @results = []
  end

  def test_account(account)
    test_account_user_views(account) if test_account?(account)
  end

  def test_account?(account)
    account.has_user_views_query_tester? && account.has_user_views_sampling_enabled?
  end

  def test_account_user_views(account)
    account.user_views.find_each do |user_view|
      next unless test_user_view?(user_view)

      test_user_view(user_view)
    end
  end

  def test_user_view?(user_view)
    user_view.definition.conditions_all.any? { |c| c.source == 'created_at' }
  end

  def test_user_view(user_view)
    with       = Zendesk::UserViews::Executer.new(user_view, User.system, {})
    without    = Zendesk::UserViews::Executer.new(user_view, User.system, skip_use_index: true)
    with_first = with_first?

    if with_first
      with_results = with.find_users
      without_results = without.find_users
    else
      without_results = without.find_users
      with_results = with.find_users
    end

    @results << [
      user_view.account.subdomain,
      user_view.account.shard_id,
      user_view.id,
      user_view.title,
      ActiveRecord::Base.connection_config[:host],
      with.select_time,
      with.count_time,
      with_results.total_entries,
      without.select_time,
      without.count_time,
      without_results.total_entries,
      with_first,
      Time.now.to_s
    ]
  end

  def with_first?
    rand(2) == 0
  end

  def generate_csv
    CSV.generate do |csv|
      csv << [
        "Account",
        "Shard",
        "User View",
        "Title",
        "DB",
        "USE INDEX: Select (s)",
        "USE INDEX: Count (s)",
        "Count",
        "No USE INDEX: Select (s)",
        "No USE INDEX: Count (s)",
        "Count",
        "USE INDEX First?",
        "Time"
      ]
      @results.each do |result|
        csv << result
      end
    end
  end

  def deliver_csv_report
    return if @results.empty?

    subdomain, = @results.first
    MonitorMailer.deliver_user_view_query_test_report(Account.find_by_subdomain(subdomain), generate_csv)
  end
end
