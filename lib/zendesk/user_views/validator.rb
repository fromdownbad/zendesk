module Zendesk
  module UserViews
    class Validator
      attr_reader :account, :view

      def initialize(view)
        @account = view.account
        @view = view
      end

      def validate
        # Validate the definition first
        DefinitionValidator.new(view).validate

        # If the definition is invalid, there is no use validating the rest
        return false unless valid?

        [:conditions_all, :conditions_any].each do |type|
          view.definition.send(type).each do |condition|
            ConditionValidator.validate(view, condition)
          end
        end

        ExecutionValidator.new(view).validate

        valid?
      end

      def valid?
        !view.errors.present?
      end

      private

      def add_error(error, params = {})
        view.errors.add(:base, Api.error("txt.models.rules.rule.#{error}", params))
      end
    end
  end
end
