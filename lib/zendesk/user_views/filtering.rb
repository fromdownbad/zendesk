module Zendesk
  module UserViews
    class Filtering
      attr_reader :condition, :table_alias, :source_custom_field, :source

      delegate :view, :definition, to: :condition
      delegate :operator, :value, to: :definition

      def initialize(condition, table_alias)
        @condition = condition
        @table_alias = table_alias
        @source_custom_field = view.account.find_user_custom_field_by_source(definition.source)
        @source = UserView.custom_field_key(definition.source)
      end

      def collapsible?
        false
      end

      protected

      def users
        @users ||= User.arel_table
      end
    end

    class CustomFieldFiltering < Filtering
      def apply(query)
        if condition.invert_operator?
          condition.invert_operator_if_necessary

          query.
            outer_join(cf_values_table_alias).
            on(join_conditions).
            where(cf_values_table_alias[:owner_id].eq(nil))
        else
          query.
            join(cf_values_table_alias).
            on(join_conditions)
        end
      end

      def any_join(query)
        condition.invert_operator_if_necessary

        query.
          outer_join(cf_values_table_alias).
          on(join_conditions)
      end

      def where
        if condition.invert_operator?
          cf_values_table_alias[:owner_id].eq(nil)
        else
          cf_values_table_alias[:owner_id].eq(users[:id])
        end
      end

      def collapsible?
        true
      end

      protected

      def join_conditions
        @join_conditions ||= begin
          where_conditions.
            and(AccountIdCondition.for(cf_values_table_alias, view.account_id)).
            and(cf_values_table_alias[:cf_field_id].eq(source_custom_field.id)).
            and(cf_values_table_alias[:owner_id].eq(users[:id]))
        end
      end

      def where_conditions
        condition.where(cf_values_table_alias)
      end

      private

      def cf_values
        @cf_values ||= ::CustomField::Value.arel_table
      end

      def cf_values_table_alias
        @cf_values_table_alias ||= cf_values.alias(table_alias)
      end
    end

    class CurrentTagsFiltering < Filtering
      def apply(query)
        if operator.to_sym == :includes
          query.
            join(taggings_table_alias).
            on(join_conditions)
        else
          any_join(query).where(where)
        end
      end

      def any_join(query)
        query.
          outer_join(taggings_table_alias).
          on(join_conditions)
      end

      def where
        if operator.to_sym == :includes
          taggings_table_alias[:taggable_id].not_eq(nil)
        else
          taggings_table_alias[:taggable_id].eq(nil)
        end
      end

      private

      def join_conditions
        @join_conditions ||= begin
          taggings_table_alias[:taggable_type].eq(view.view_type).
            and(taggings_table_alias[:tag_name].in(tag_array)).
            and(AccountIdCondition.for(taggings_table_alias, view.account_id)).
            and(taggings_table_alias[:taggable_id].eq(users[:id]))
        end
      end

      def tag_array
        @tag_array ||= condition.value_predicate
      end

      def taggings
        @taggings ||= Tagging.arel_table
      end

      def taggings_table_alias
        @taggings_table_alias ||= taggings.alias(table_alias)
      end
    end

    class OrganizationFiltering < Filtering
      def apply(query)
        if value.nil?
          case operator.to_sym
          when :is
            definition.operator = 'is_not_present'
          when :is_not
            definition.operator = 'is_present'
          end
        end

        if condition.negative?
          if operator.to_sym == :is_not_present
            query.
              where(users[:organization_id].eq(nil))
          else
            query.
              outer_join(organization_memberships).
              on(organization_memberships[:user_id].eq(users[:id]).and(organization_memberships[:organization_id].eq(value))).
              where(organization_memberships[:organization_id].eq(nil))
          end
        else
          if operator.to_sym == :is_present
            query.
              where(users[:organization_id].not_eq(nil))
          else
            query.
              join(organization_memberships).
              on(organization_memberships[:user_id].eq(users[:id])).
              where(organization_memberships[:organization_id].eq(value))
          end
        end
      end

      private

      def organization_memberships
        @organization_memberships ||= OrganizationMembership.arel_table.alias(table_alias)
      end
    end

    class UserAttributeFiltering < Filtering
      def apply(query)
        query.where(where)
      end

      def any_join(query)
      end

      def where
        condition.where
      end
    end

    class LanguageFiltering < UserAttributeFiltering
      def where
        if condition.negative?
          if value == view.account.locale_id
            # This is checking if user's locale_id is not account's locale.
            # Because users where locale_id is nil default to account's locale, they should be excluded.
            condition.where.and(users[:locale_id].not_eq(nil))
          else
            condition.where
          end
        else
          condition.where.or(users[:locale_id].eq(nil))
        end
      end

      private

      def users
        @users ||= User.arel_table
      end
    end
  end
end
