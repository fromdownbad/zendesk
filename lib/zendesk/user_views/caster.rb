module Zendesk
  module UserViews
    class Caster
      attr_reader :type, :operator, :value, :custom_field, :source, :account

      def initialize(account, type, operator, value, source, custom_field = nil)
        @type = type
        @operator = operator
        @value = value
        @source = source
        @custom_field = custom_field
        @account = account
      end

      def cast_value
        if is_present_condition?
          if is_date_comparison? && @source != "last_login"
            return false
          else
            return value
          end
        end

        result = if value.to_s.blank?
          false
        elsif is_date_comparison?
          cast_date_value
        elsif custom_field.present? && is_castable?
          custom_field.cast_value_for_db(value)
        elsif is_castable?
          if type == "Dropdown"
            cast_system_dropdown
          else
            "::CustomField::#{type}".constantize.new.cast_value_for_db(value)
          end
        elsif type == "Tags"
          cast_tags
        end
        result || false
      end

      def cast_tags
        return false unless value && value.is_a?(Array) && value.map(&:class).uniq == [String]
        value
      end

      def cast_system_dropdown
        return cast_role if source == "roles"
        return false unless id = Integer(value) rescue nil
        if source == "organization_id"
          account.organizations.find_by_id(id) && id
        elsif source == "locale_id"
          account.available_languages.map(&:id).include?(id) && id
        end
      end

      def cast_role
        standard_role = cast_standard_role
        return standard_role if standard_role
        cast_enterprise_role
      end

      def cast_standard_role
        return false unless ["agent", "end-user", "admin"].include?(value)
        case value
        when "agent"
          Role::AGENT.id
        when "admin"
          Role::ADMIN.id
        else
          Role::END_USER.id
        end
      end

      def cast_enterprise_role
        match = value.match(/^permission_set_id:(\d+)$/)
        match && match[1].to_i
      end

      def cast_date_value
        if [:within_next_n_days, :within_previous_n_days].include?(operator.to_sym)
          Integer(value) rescue false
        else
          casted_date = ::CustomField::Date.new.cast_value_for_db(value)
          if [:created_at, :last_login].include?(source.to_sym)
            casted_time = Time.parse(casted_date) rescue false
            if casted_time && [:greater_than, :less_than_or_equal_to].include?(operator.to_sym)
              casted_time.end_of_day
            else
              casted_time
            end
          else
            casted_date
          end
        end
      end

      def is_castable? # rubocop:disable Naming/PredicateName
        %w[Checkbox Dropdown Decimal Integer Text Textarea Regexp].include?(type)
      end

      def is_present_condition? # rubocop:disable Naming/PredicateName
        [:is, :is_not, :is_present, :is_not_present].include?(operator.try(:to_sym)) && value.to_s.blank?
      end

      def is_date_comparison? # rubocop:disable Naming/PredicateName
        ['Date', :datetime].include?(type) || (custom_field && custom_field.type == "Date")
      end
    end
  end
end
