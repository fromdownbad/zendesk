module Zendesk
  module UserViews
    class Ordering
      attr_reader :view

      def initialize(ordering, type, view)
        @key = ordering[:id].to_s
        @order = ordering[:order]
        @type = type
        @view = view
      end

      def reusable?
        false
      end

      protected

      def users
        @users ||= User.arel_table
      end

      def cf_values
        @cf_values ||= ::CustomField::Value.arel_table
      end

      def ordering
        (@order.to_s == 'asc') ? :asc : :desc
      end
    end

    # TODO: This is starting to look a lot like CustomFieldFiltering
    class CustomFieldOrdering < Ordering
      def initialize(ordering, type, view)
        super
        @key = UserView.custom_field_key(ordering[:id])
        @source_custom_field = view.account.find_user_custom_field_by_key(@key)
      end

      def apply(query)
        query.
          outer_join(cf_values_table_alias).
          on(join_conditions)

        query.order(order_expression)
      end

      def join_conditions
        AccountIdCondition.for(cf_values_table_alias, view.account_id).
          and(cf_values_table_alias[:cf_field_id].eq(source_custom_field.id)).
          and(cf_values_table_alias[:owner_id].eq(users[:id]))
      end

      def source_custom_field
        view.account.find_user_custom_field_by_key(@key)
      end

      def table_alias
        "#{@type}_by_custom_field_value"
      end

      def order_expression
        cf_values_table_alias[:value].send(ordering)
      end

      def reusable?
        true
      end

      def reuse(query, applied_table_alias)
        query.order(cf_values.alias(applied_table_alias)[:value].send(ordering))
      end

      protected

      def cf_values_table_alias
        @cf_values_table_alias ||= cf_values.alias(table_alias)
      end
    end

    class DropdownChoiceOrdering < CustomFieldOrdering
      def apply(query)
        super

        query.
          outer_join(cf_dropdown_choices).
          on(cf_dropdown_choices[:id].eq(cf_values_table_alias[:value]))
      end

      def order_expression
        cf_dropdown_choices[:value].send(ordering)
      end

      def reusable?
        true
      end

      def reuse(query, applied_table_alias)
        query.
          join(cf_dropdown_choices).
          on(cf_dropdown_choices[:id].eq(cf_values.alias(applied_table_alias)[:value]))

        query.order(order_expression)
      end

      private

      def cf_dropdown_choices
        @cf_dropdown_choices ||= ::CustomField::DropdownChoice.arel_table.alias("#{@type}_custom_field_dropdown_choices")
      end
    end

    class OrganizationOrdering < Ordering
      def apply(query)
        query.
          outer_join(organizations).
          on(organizations[:id].eq(users[:organization_id]))

        query.order(order_expression)
      end

      def table_alias
        "#{@type}_by_organization"
      end

      def order_expression
        organizations[:name].send(ordering)
      end

      def reusable?
        true
      end

      def reuse(query, _applied_table_alias)
        query.
          join(organizations).
          on(organizations[:id].eq(users[:organization_id]))

        query.order(order_expression)
      end

      private

      def organizations
        @organizations ||= ::Organization.arel_table.alias(table_alias)
      end
    end

    class RolesOrdering < Ordering
      def apply(query)
        query.
          outer_join(permission_sets).
          on(permission_sets[:id].eq(users[:permission_set_id]))

        # First sort by role: end-user, admin, agent
        query.order(users[:roles].send(ordering))
        # Only for agents, sort by permission set name
        query.order("CASE WHEN `users`.`roles` = #{Role::AGENT.id} THEN `#{table_alias}`.`name` ELSE NULL END #{@order}")
      end

      def table_alias
        "#{@type}_by_roles"
      end

      def reusable?
        false
      end

      private

      def permission_sets
        @permission_sets ||= PermissionSet.arel_table.alias(table_alias)
      end
    end

    class TimeZoneOrdering < Ordering
      def apply(query)
        query.order("IFNULL(users.#{@key}, '#{account_time_zone}') #{@order}")
      end

      def reusable?
        false
      end

      private

      def account_time_zone
        view.account.time_zone
      end
    end

    class UserAttributeOrdering < Ordering
      def apply(query)
        order  = "users.#{@key} #{@order}"
        offset = Time.use_zone(view.account.time_zone) { Time.zone }.formatted_offset(true)
        order  = "CAST(CONVERT_TZ(`users`.`#{@key}`, 'GMT', '#{offset}') AS DATE) #{@order}" if ["created_at", "last_login"].include?(@key) && @type == "group"
        query.order(order)
      end
    end
  end
end
