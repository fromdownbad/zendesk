module Zendesk
  module UserViews
    class ConditionValidator < Validator
      attr_reader :view, :condition, :account

      delegate :value, to: :condition

      def initialize(view, condition)
        @view = view
        @condition = condition
        @account = view.account
      end

      def source
        condition.source.to_s
      end

      def operator
        condition.operator.to_s
      end

      def self.validate(view, condition)
        source = condition.source.to_s
        klass = if view.user_attribute_definitions.keys.include?(source)
          UserAttributeConditionValidator
        else
          if field = view.field_from_source(source)
            if field.is_system?
              SystemFieldConditionValidator
            else
              CustomFieldConditionValidator
            end
          else
            InvalidSourceValidator
          end
        end

        klass.new(view, condition).validate
      end

      class InvalidSourceValidator < ConditionValidator
        # Since Zendesk::UserViews::ConditionsValidator.validate has already determined that the condition
        # has an invalid source, #validate simply adds an error to the UserView
        def validate
          add_error("invalid_source", condition: "#{source} #{operator} #{value}", source: source, error: "InvalidSource")
        end
      end

      class ValidSourceValidator < ConditionValidator
        def validate
          validate_operator
          validate_value
        end

        def validate_operator
          return unless valid?

          possible_operators = operators
          unless account.has_user_views_negative_operators_enabled?
            possible_operators -= %w[is_not is_not_present not_includes]
          end

          unless possible_operators.include?(operator)
            add_error("invalid_operator", condition: "#{title} #{operator} #{value}", operator: operator, error: "InvalidOperator")
          end
        end

        def validate_value
          return unless valid?

          caster = Zendesk::UserViews::Caster.new(account, operator_data[:type].capitalize, operator, value, source, field)
          if caster.cast_value == false
            add_error("invalid_value", condition: "#{title} #{operator_data[:title]} #{value}", value: value, error: "InvalidValue")
          end
        end

        def field
          @field ||= view.field_from_source(source)
        end

        def operator_data
          @operator_data ||= UserView.operator_data(operator, type)
        end
      end

      class UserAttributeConditionValidator < ValidSourceValidator
        def operators
          view.user_attribute_definitions[source]["operators"]
        end

        def type
          view.user_attribute_definitions[source]["type"]
        end

        def title
          UserView.user_attribute_title(source)
        end
      end

      class CustomFieldConditionValidator < ValidSourceValidator
        def operators
          UserView::CUSTOM_FIELD_OPERATORS[field.type]
        end

        def type
          field.type
        end

        def title
          field.title
        end
      end

      class SystemFieldConditionValidator < CustomFieldConditionValidator
        def title
          ::I18n.t(field.title)
        end
      end
    end
  end
end
