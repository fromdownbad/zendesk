module Zendesk
  module UserViews
    class Condition
      attr_reader :view, :definition, :operator

      delegate :source, :value, to: :definition

      def initialize(view, definition)
        @view = view
        @definition = definition
        @operator = definition.operator
      end

      def value_predicate
        Caster.new(view.account, field_type, operator, value, source, custom_field).cast_value
      end

      def is(field, db_value)
        if field_type == "Date" && [:created_at, :last_login].include?(source.to_sym)
          field.gteq(db_value).and(field.lteq(db_value.end_of_day))
        else
          field.eq(db_value)
        end
      end

      def is_not(field, db_value) # rubocop:disable Naming/PredicateName
        if field_type == "Date" && [:created_at, :last_login].include?(source.to_sym) && !db_value.nil?
          field.lt(db_value).or(field.gt(db_value.end_of_day)).or(field.eq(nil))
        else
          result = field.not_eq(db_value)
          if db_value.nil? || invert_operator?
            result
          else
            result.or(field.eq(nil))
          end
        end
      end

      def where
        field = field_predicate
        db_value = value_predicate

        expr =
          case operator.to_sym
          when :is
            is(field, db_value)
          when :is_not
            is_not(field, db_value)
          when :greater_than
            field.gt(db_value)
          when :greater_than_or_equal_to
            field.gteq(db_value)
          when :less_than
            field.lt(db_value)
          when :less_than_or_equal_to
            field.lteq(db_value)
          when :includes
            field.in(db_value)
          when :not_includes
            field.not_in(db_value)
          when :within_next_n_days
            field.gteq(Time.now).and(field.lteq(db_value.to_i.days.from_now.to_s(:db)))
          when :within_previous_n_days
            field.lteq(Time.now).and(field.gteq(db_value.to_i.days.ago.to_s(:db)))
          when :is_present
            field.not_eq(nil)
          when :is_not_present
            field.eq(nil)
          else
            raise "invalid operator"
          end

        # Add any additional constraints, e.g. add an AND clause
        add_secondary_constraints(expr)
      end

      def invert_operator_if_necessary
      end

      def invert_operator?
        false
      end

      def add_secondary_constraints(expr)
        # noop in superclass
        expr
      end

      def negative?
        is_nil? || is_not? || is_not_present?
      end

      private

      def custom_field
        nil
      end

      def is_nil? # rubocop:disable Naming/PredicateName
        definition.operator.to_sym == :is && value.nil?
      end

      def is_not? # rubocop:disable Naming/PredicateName
        definition.operator.to_sym == :is_not && !value.nil?
      end

      def is_not_present? # rubocop:disable Naming/PredicateName
        definition.operator.to_sym == :is_not_present
      end
    end

    class CustomFieldCondition < Condition
      attr_accessor :table_alias

      def where(table_alias)
        self.table_alias = table_alias
        super()
      end

      def invert_operator_if_necessary
        if is_nil? || is_not_present?
          @operator = 'is_not'
        elsif is_not?
          @operator = 'is'
        end
      end

      def invert_operator?
        is_nil? || is_not? || is_not_present?
      end

      private

      def field_type
        @field_type ||= custom_field.type
      end

      def custom_field
        @custom_field ||= view.field_from_source(source)
      end

      def field_predicate
        table_alias[:value]
      end
    end

    class CustomFieldConditions
      attr_reader :view, :definition

      def initialize(conditions)
        @conditions = conditions
        @view = conditions.first.view
        @definition = conditions.first.definition
      end

      def invert_operator?
        @conditions.all?(&:invert_operator?)
      end

      def negative?
        @conditions.all?(&:negative?)
      end

      def invert_operator_if_necessary
        @conditions.each(&:invert_operator_if_necessary)
      end

      def where(table_alias)
        invert_operator = invert_operator?

        @conditions.inject(nil) do |result, current|
          where = current.where(table_alias)
          if result.nil? # first
            where
          elsif invert_operator
            result.or(where)
          else
            result.and(where)
          end
        end
      end
    end

    class UserAttributeCondition < Condition
      private

      def field_type
        @field_type ||= view.user_attribute_definitions[source]["type"].capitalize
      end

      def field_predicate
        if source == 'roles' && value.try(:match, /^permission_set_id:\d+$/)
          users[:permission_set_id]
        else
          users[source.to_sym]
        end
      end

      def add_secondary_constraints(expr)
        return expr if source != 'roles' ||
          !view.account.enterprise_roles_in_user_views_enabled? ||
          !value.try(:match, /^permission_set_id:\d+$/)

        if is_not?
          expr.or(users[source.to_sym].not_eq(Role::AGENT.id))
        else
          expr.and(users[source.to_sym].eq(Role::AGENT.id))
        end
      end

      def users
        @users ||= User.arel_table
      end
    end
  end
end
