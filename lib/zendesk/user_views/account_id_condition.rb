module Zendesk
  module UserViews
    class AccountIdCondition
      attr_reader :who
      attr_reader :account_id

      def self.for(who, account_id)
        new(who, account_id).apply
      end

      def initialize(who, account_id)
        @who = who
        @account_id = account_id
      end

      def apply
        who[:account_id].eq(account_id)
      end
    end
  end
end
