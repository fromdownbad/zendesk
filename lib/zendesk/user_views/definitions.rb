require 'role'

class Zendesk::UserViews::Definitions
  include Zendesk::CustomField::FieldHelper
  attr_reader :account, :user

  def initialize(account, user)
    @account = account
    @user    = user
  end

  def build
    system_definitions + custom_definitions
  end

  def operator_as_json(key, type)
    result = UserView.operator_data(key, type).merge(key: key)
    result[:label] = I18n.t("txt.admin.models.rules.rule_dictionary.days") if UserView::SPECIAL_CASE_TYPES.keys.include?(key)
    result
  end

  def definition_as_json(title, key, type, operators, dropdown_options)
    groupable = type == 'system' ? UserView.valid_execution_attributes_by_type(account)['group'].include?(key) : type != 'Textarea'
    sortable  = type == 'system' ? UserView.valid_execution_attributes_by_type(account)['sort'].include?(key) : true
    unless account.has_user_views_negative_operators_enabled?
      operators = operators.select { |o| ["is_not", "is_not_present", "not_includes"].exclude?(o[:key]) }
    end
    definition = { title: title, key: key, type: type, groupable: groupable, operators: operators, sortable: sortable }
    definition[:custom_field_options] = dropdown_options if dropdown_options
    definition
  end

  def system_definitions
    UserView.valid_execution_attributes(account).map do |attribute|
      display_operators = if (definition = UserView.user_attribute_definitions(account)[attribute])
        definition["operators"].map do |operator|
          operator_as_json(operator, definition["type"])
        end
      else
        []
      end
      dropdown_options = roles_dropdown_options if attribute == "roles"
      title = UserView.user_attribute_title(attribute)
      definition_as_json(title, attribute, "system", display_operators, dropdown_options)
    end
  end

  def roles_dropdown_options
    res = [
      { name: Role::END_USER.display_name, value: "end-user" },
      { name: Role::AGENT.display_name,    value: "agent" },
      { name: Role::ADMIN.display_name,    value: "admin" }
    ]
    if account.enterprise_roles_in_user_views_enabled?
      account.permission_sets.each do |permission_set|
        res << { name: permission_set.name, value: "permission_set_id:#{permission_set.id}" }
      end
    end
    res
  end

  def custom_definitions
    custom_fields = account.user_custom_fields.includes(:dropdown_choices).to_a
    custom_fields.map do |field|
      display_operators = UserView::CUSTOM_FIELD_OPERATORS[field.type].map do |operator|
        operator_as_json(operator, field.type.downcase)
      end
      dropdown_options = if field.custom_field_options.present?
        active_options = field.custom_field_options.select(&:active?)
        active_options.map do |option|
          name = render_dynamic_content(option.name)
          {
            id: option.id,
            name: name,
            value: option.value
          }
        end
      end
      title = render_dc_or_i18n(field, field.title)
      definition_as_json(title, "custom_fields.#{field['key']}", field.type.downcase, display_operators, dropdown_options)
    end
  end

  def render_dynamic_content(text)
    Zendesk::Liquid::DcContext.render(text, account, user, 'text/plain', nil, user.dc_cache)
  end
end
