require 'zendesk/user_views/filtering'
require 'zendesk/user_views/ordering'
require 'zendesk/user_views/condition'

module Zendesk
  module UserViews
    class QueryBuilder
      attr_reader :view
      attr_reader :params
      attr_reader :has_tagging_conditions
      attr_reader :skip_sampling
      attr_reader :skip_use_index

      DEFAULT_SORTING = {
        id: :created_at,
        order: :asc
      }.freeze

      def initialize(view, params)
        @view = view
        @params = params
        @has_tagging_conditions = false
        @skip_sampling = params[:sampling] == false
        @skip_use_index = params[:skip_use_index] == true
      end

      def query
        initialize_query
        parse_params_sorting
        apply_all_conditions
        apply_any_conditions
        apply_grouping_and_sorting
        pagination
        to_sql
      end

      def query_total_entries
        initialize_query
        apply_all_conditions
        apply_any_conditions
        to_count_sql
      end

      def page
        params[:page].try(:to_i) || 1
      end

      def per_page
        params[:per_page].try(:to_i) || view.per_page || 15
      end

      # Returns an array of tags describing the current query, suitable
      # for passing to statsd
      def statsd_tags(include_sort)
        tags = [
          "shard:#{view.account.shard_id}",
          "zendesk_version:#{GIT_HEAD_TAG}"
        ]

        if include_sort

          # TODO: Calling parse_params_sorting is generally not needed, since the caller
          # has probably already called query.  However, we don't know this for sure.
          parse_params_sorting
          tags.concat [
            statsd_output_info(view.output.sort, 'order'),
            statsd_output_info(view.output.group, 'group')
          ]
        end

        if defined?(ActiveRecord::ConnectionAdapters::MysqlFlexmasterAdapter) && ActiveRecord::Base.connection.is_a?(ActiveRecord::ConnectionAdapters::MysqlFlexmasterAdapter)
          tags << "db_host:#{ActiveRecord::Base.connection.current_host}"
        end

        tags
      end

      def default_where_condition
        users[:account_id].eq(view.account.id).and(users[:is_active].eq(1))
      end

      private

      def get_custom_field(source)
        custom_fields[UserView.custom_field_key(source)]
      end

      def custom_fields
        @custom_fields ||= view.custom_fields_by_key
      end

      def to_sql
        @query.project(projection).to_sql
      end

      def projection
        "`#{view.table}`.*"
      end

      def to_count_sql
        count = @has_tagging_conditions ? "count(distinct `users`.`id`)" : "count(*)"
        @query.project(count).to_sql
      end

      def initialize_query
        @query = users.where(users_where_condition).
          outer_join(user_settings).
          on(
            user_settings[:user_id].eq(users[:id]).
            and(user_settings[:account_id].eq(users[:account_id])).
            and(user_settings[:name].eq('suspended'))
          ).
          where(
            user_settings[:value].not_eq('true').
            or(user_settings[:value].eq(nil))
          )

        if sampling_query_with_use_index?
          @query.from('`users` USE INDEX(`index_users_on_account_id_and_sample`)')
        end
      end

      def tag_conditions?
        view.definition.conditions_all.map(&:source).include?("current_tags") &&
          view.account.has_user_views_tag_queries_without_forcing_index?
      end

      def users
        @users ||= User.arel_table
      end

      def user_settings
        @user_settings ||= UserSetting.arel_table
      end

      def user_sample
        @user_sample ||= users.alias('user_sample')
      end

      def parse_params_sorting
        return unless params_has_sorting?
        view.output.sort       ||= {}
        view.output.sort[:id]    = params[:sort_by] if execution_validator.validate_output('sort', params[:sort_by])
        view.output.sort[:order] = params[:sort_order] if execution_validator.validate_sort_order(params[:sort_order])
      end

      def apply_all_conditions
        @apply_all_conditions ||= collapse_filterings(filtering_conditions(view.definition.conditions_all, 'all')).each do |filtering|
          @has_tagging_conditions = true if filtering.is_a?(Zendesk::UserViews::CurrentTagsFiltering)
          filtering.apply(@query)
        end
      end

      def apply_any_conditions
        where_conditions = []

        filtering_conditions(view.definition.conditions_any, 'any').each do |filtering|
          @has_tagging_conditions = true if filtering.is_a?(Zendesk::UserViews::CurrentTagsFiltering)
          filtering.any_join(@query)
          where_conditions << filtering.where
        end

        where_condition = where_conditions.inject { |condition, who| condition.or(who) }
        @query.where(where_condition) if where_condition
      end

      def users_where_condition
        if query_with_sampling?
          default_where_condition.and(users[:sample].lteq(sample_threshold))
        else
          default_where_condition
        end
      end

      def sampling_query_with_use_index?
        query_with_sampling? && !tag_conditions? && !skip_use_index
      end

      def query_with_sampling?
        view.account.has_user_views_sampling_enabled? && !skip_sampling
      end

      def sample_threshold
        (UserView::SAMPLING_RANGE * UserView::SAMPLING_THRESHOLD) / user_count
      end

      def user_count
        view.account.settings.user_count
      end

      def collapse_filterings(filterings)
        processed = []
        collapsible = {}

        filterings.each do |filtering|
          if filtering.collapsible?
            collapsible[filtering.source] ||= []
            collapsible[filtering.source] << filtering
          else
            processed << filtering
          end
        end

        collapsible.each do |_source, collapsible_filterings|
          if collapsible_filterings.length > 1
            conditions = collapsible_filterings.map(&:condition)
            table_alias = collapsible_filterings.first.table_alias
            filtering_with = collapsible_filterings.first.class
            processed << filtering_with.new(CustomFieldConditions.new(conditions), table_alias)
          else
            processed << collapsible_filterings.first
          end
        end

        processed
      end

      def filtering_conditions(conditions, type)
        conditions.each_with_index.map do |definition, index|
          table_alias = "#{type}_#{index}"

          if get_custom_field(definition.source)
            condition_klass = CustomFieldCondition
            filtering_with = CustomFieldFiltering
          else
            condition_klass = UserAttributeCondition
            filtering_with = if definition.source == 'current_tags'
              CurrentTagsFiltering
            elsif definition.source == 'locale_id' && view.account.has_user_views_use_default_language?
              LanguageFiltering
            elsif definition.source == 'organization_id' && view.account.has_multiple_organizations_enabled?
              OrganizationFiltering
            else
              UserAttributeFiltering
            end
          end

          condition = condition_klass.new(view, definition)
          filtering_with.new(condition, table_alias)
        end
      end

      def ensure_valid_sorting(output)
        output.sort       ||= {}
        output.sort[:id]    = DEFAULT_SORTING[:id] unless output.sort[:id]
        output.sort[:order] = DEFAULT_SORTING[:order] unless output.sort[:order]
      end

      def apply_grouping_and_sorting
        @query.group(users[:id]) if @has_tagging_conditions

        ensure_valid_sorting(view.output)

        if params_has_sorting?
          apply_ordering(view.output.sort, 'params_sort')
          return
        end

        if group_only?
          apply_ordering(view.output.group, 'group_only')
        else
          apply_ordering(view.output.group, 'group') if view.has_grouping?
          apply_ordering(view.output.sort, 'sort') if view.has_sorting?
        end
      end

      def apply_ordering(ordering, type)
        same_field_as_ordering = @apply_all_conditions.find { |condition| condition.definition.source == ordering[:id].to_s }

        ordering_with = if custom_field = get_custom_field(ordering[:id])
          if custom_field.type == 'Dropdown'
            DropdownChoiceOrdering
          else
            CustomFieldOrdering
          end
        elsif ordering[:id] == 'organization_id'
          OrganizationOrdering
        elsif ordering[:id] == 'roles' && view.account.enterprise_roles_in_user_views_enabled?
          RolesOrdering
        elsif ordering[:id] == 'time_zone'
          TimeZoneOrdering
        else
          UserAttributeOrdering
        end

        orderer = ordering_with.new(ordering, type, view)

        negative = @apply_all_conditions.any? { |filtering| filtering.condition.negative? }

        if same_field_as_ordering && orderer.reusable? && !negative
          orderer.reuse(@query, same_field_as_ordering.table_alias)
        else
          orderer.apply(@query)
        end
      end

      def pagination
        skip = (page - 1) * per_page
        @query.skip(skip).take(per_page)
      end

      def params_has_sorting?
        params[:sort_by].present? && params[:sort_order].present?
      end

      def group_only?
        view.has_grouping? && view.has_sorting? &&
        view.output.group[:id] == view.output.sort[:id] &&
        !["created_at", "last_login"].include?(view.output.group[:id])
      end

      def execution_validator
        @execution_validator ||= Zendesk::UserViews::ExecutionValidator.new(view)
      end

      # Converts output info (i.e. view.output.sort or view.output.group)
      # into a string suitable for use as a tag with statsd
      def statsd_output_info(output_info, name)
        if output_info
          field = statsd_output_info_name(output_info[:id])
          "#{name}:#{field}:#{output_info[:order]}"
        else
          "#{name}:none"
        end
      end

      # Converts the id of an output info item into a name for use
      # in a statsd tag
      def statsd_output_info_name(id)
        # If the name is from a custom field, we don't actually care about the name.
        # Instead, we want to let people know it's custom, and what the type is, like
        # "cf_integer".
        if UserView.custom_field?(id)
          id = "cf_#{get_custom_field(id)[:type].downcase}"
        end

        id
      end
    end
  end
end
