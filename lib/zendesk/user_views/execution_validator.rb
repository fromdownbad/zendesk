module Zendesk
  module UserViews
    class ExecutionValidator < Validator
      def validate
        view.output.columns.each do |item|
          add_error("invalid_columns", field: item, error: "InvalidColumns") unless validate_output("columns", item)
        end

        ["group", "sort"].each do |type|
          next unless type_value = view.output.send(type)
          unless validate_output(type, type_value[:id]) && validate_sort_order(type_value[:order])
            add_error("invalid_#{type}", field: type_value[:id], order: type_value[:order], error: "InvalidValue")
          end
        end
      end

      def validate_output(type, item)
        item = item.to_s
        UserView.valid_execution_attributes_by_type(account)[type].include?(item) || account.find_user_custom_field_by_source(item)
      end

      def validate_sort_order(sort_order)
        ["asc", "desc"].include?(sort_order.to_s.downcase)
      end
    end
  end
end
