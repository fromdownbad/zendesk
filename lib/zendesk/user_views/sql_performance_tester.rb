# Multiple times: run each query ten times, pick median (duration)
# Zendesk::UserViews::SqlPerformanceTester.new(account, sorting: true, run_multiple_times: true)
# Zendesk::UserViews::SqlPerformanceTester.new(account, result_size: 20000, sampling: false)
# Recommended running the query killer (script/rule_watch), queries taking over 10sec will be killed and 10 will be saved as duration.
class Zendesk::UserViews::SqlPerformanceTester
  def initialize(account, options = {})
    @account      = account
    @result_size  = options.fetch(:result_size, 15)
    @sorts        = options.fetch(:sorting, false)
    @query_method = options[:run_multiple_times] ? method(:run_query_multiple_times) : method(:run_query)
    @sampling     = options[:sampling] != false
  end

  def run
    time_started = Time.now
    puts "Running: #{time_started}"
    @account.on_shard do
      file = Tempfile.new("customer-lists-sql-#{@account.subdomain}.csv")
      CSV.open(file.path, "wb") do |csv|
        csv << ["key", "operator", "sort", "group", "duration", "count", "query"]
        write_csv(csv)
      end
      puts "\n\n#{file.path}"
    end
    time_finished(time_started)
  end

  def time_finished(time_started)
    time = (Time.now - time_started)
    st = 'seconds'
    if time > 60
      time /= 60
      st = 'minutes'
    end
    if time > 60
      time /= 60
      st = 'hours'
    end
    puts "Took #{time} #{st} to finish."
  end

  def get_definitions # rubocop:disable Naming/AccessorMethodName
    definitions = Zendesk::UserViews::Definitions.new(@account, @account.agents.first).build
    @possible_executions = {}
    @possible_executions['sort'] = definitions.select { |definition| definition[:sortable] }
    @possible_executions['group'] = definitions.select { |definition| definition[:groupable] }
    definitions
  end

  def write_csv(csv)
    get_definitions.each do |definition|
      definition[:operators].each do |operator|
        csv << @query_method.call(operator, definition)
        next unless @sorts
        @possible_executions.each do |type, value|
          value.each do |item|
            csv << @query_method.call(operator, definition, type => { "id" => item[:key], "order" => "asc"})
          end
        end
      end
    end
  end

  def run_query_multiple_times(operator, definition, execution = {})
    run_query(operator, definition, execution, warm_cache: true) # warm mysql cache
    sleep 0.5
    output    = run_query(operator, definition, execution)
    duration  = [output[4]]
    9.times.each { duration << run_query(operator, definition, execution)[4]; puts "duration: #{duration}" }
    output[4] = duration.sort[4]
    puts "\n\n=== #{output[0]} ===\n#{output.slice(1, output.size)}\n\n==="
    output
  end

  def build_query(operator, definition, execution)
    params        = generate_params(operator, definition, execution)
    view          = Zendesk::Rules::UserViewInitializer.new(@account, params, :user_view).user_view
    query_builder = Zendesk::UserViews::QueryBuilder.new(view, params)
    query_builder.query + " /* user_rule:, user:#{@account.owner.id}, shard:#{@account.shard_id} */"
  end

  def run_query(operator, definition, execution = {}, _options = {})
    query = build_query(operator, definition, execution)
    t = Time.now
    begin
      UserView.connection.execute(query)
      duration = Time.now - t
      count = UserView.connection.select_value(query.sub(/SELECT  `users`\.\*/, "SELECT count(*)"))
    rescue ZendeskDatabaseSupport::MappedDatabaseExceptions::LostConnection
      duration = 10 # It means 10+
      count = '-'
    end
    output = generate_output(definition, operator, duration, count, query, execution)
    puts "\n\n=== #{output[0]} ===\n#{output.slice(1, output.size)}\n\nCount: #{output[5]}\n\n==="
    output
  end

  def generate_output(definition, operator, duration, count, query, execution = {})
    if execution.keys.first == "sort"
      sort = execution["sort"]["id"]
    elsif execution.keys.first == "group"
      group = execution["group"]["id"]
    end
    output = [definition[:key], operator[:key], sort, group, duration, count, query]
    output
  end

  def generate_params(operator, definition, execution = {})
    HashWithIndifferentAccess.new("user_view" => {
        "title" => "abc",
        "all" => [{
          "field" => definition[:key],
          "operator" => operator[:key],
          "value" => get_value(operator[:type], definition)
        }],
        "execution" => execution
      },
                                  "sampling" => @sampling,
                                  "per_page" => @result_size)
  end

  def get_value(type, definition)
    case type
    when "dropdown"
      if options = definition[:custom_field_options]
        options.first[:value]
      elsif definition[:key] == "organization_id"
        @account.organizations.first.id
      elsif definition[:key] == "locale_id"
        ENGLISH_BY_ZENDESK.id
      end
    when "text", "textarea", "regexp"
      "a"
    when "tags"
      tags = Tagging.select('tag_name').where(
        taggable_type: "User", account_id: @account.id
      ).group(:tag_name).order('count(*) desc').limit(2)
      tags = tags[0..1].map(&:tag_name)
      tags = ["pt_1", "pt_2"] if tags.empty?
      tags
    when "date"
      "2013-06-28"
    when "integer"
      "300"
    when "decimal"
      "300.0"
    when "checkbox"
      true
    end
  end
end
