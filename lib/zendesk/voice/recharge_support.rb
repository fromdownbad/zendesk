module Zendesk
  module Voice
    module RechargeSupport
      private

      def recharge_settings
        ::Voice::RechargeSettings.find_by_account_id!(current_account.id)
      end

      def require_no_recharge_settings
        return if current_account.voice_recharge_settings.blank?
        render status: :not_found, json: { domain: 'Voice', error: 'Recharge settings already exist' }
      end

      def require_recharge_settings
        return if current_account.voice_recharge_settings.present?
        render status: :not_found, json: { domain: 'Voice', error: 'Recharge settings do not exist' }
      end

      def require_zuora_account
        return if current_account.zuora_subscription.present?
        render status: :not_found, json: { domain: 'Voice', error: 'Recharge not supported on trial' }
      end

      def zuora_account_id
        @zuora_account_id ||= current_account.zuora_subscription.zuora_account_id
      end

      def update_recharge_settings(params)
        client = current_account.zuora_subscription.client
        client.update_recharge_settings(params)
        synchronizer = ZendeskBillingCore::Zuora::Synchronizer.new(zuora_account_id)
        !!synchronizer.synchronize_voice_recharge_settings!
      end

      def create_params
        ActionController::Parameters.new(enabled: true).merge(params.require(:recharge_settings))
      end
    end
  end
end
