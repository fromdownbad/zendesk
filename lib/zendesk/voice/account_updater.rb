module Zendesk::Voice
  class AccountUpdater
    attr_reader :account, :log_string, :balance, :reference
    def initialize(account_id, balance, reference)
      @account = Account.find(account_id.to_i)
      @balance = balance.to_i
      @reference = reference
      @log_string = "test"
    end

    def voice_usage_builder
      current = current_balance
      adjustment = calculate_balance(current)
      voice_usage = @account.zuora_voice_usages.new(
        usage_type: "noop",
        voice_reference_id: reference,
        units: adjustment
      )
      @log_string = log(voice_usage, current, adjustment)
      voice_usage
    end

    def save!
      voice_usage = voice_usage_builder
      if Arturo.feature_enabled_for?(:serialize_voice_usage, @account)
        ZendeskBillingCore::Voice::UnprocessedUsage.create!(
          account_id:         @account.id,
          usage_type:         voice_usage.usage_type,
          voice_reference_id: voice_usage.voice_reference_id,
          units:              voice_usage.units
        )
      else
        voice_usage.save!
      end
    end

    def current_balance
      last_zuora_voice_usage = ZendeskBillingCore::Zuora::VoiceUsage.where(account_id: @account.id).order('id DESC').first
      @current_balance = last_zuora_voice_usage.balance unless last_zuora_voice_usage.nil?
    end

    def calculate_balance(current_balance)
      @calculate_balance = current_balance.nil? ? nil : (@balance * 1000 - current_balance)
    end

    private

    def log(voice_usage, current, adjustment)
      "#{@account.name}(#{@account.id}) Beginning Balance: #{current.nil? ? "NONE" : current} Adustment: #{adjustment.nil? ? 'Not adjusted' : adjustment / 1000} Final balance: #{@balance}
      VOICE USAGE #{voice_usage.reload.inspect} \n"
    end
  end
end
