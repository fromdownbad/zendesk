module Zendesk::Voice
  class BulkCsvUpdater
    # this class is used to fix credit balance manually in production.
    # this is a temporary fix till prepaid credit is implemented.
    def initialize(csv_path)
      @csv_path = csv_path
      @export_path = File.join(Rails.root, "log")
    end

    def parse_csv
      CSV.foreach(@csv_path) do |row|
        account_id = row[0]
        balance = row[1]
        reference = row[2]
        update_credit(account_id, balance, reference)
      end
    end

    def update_credit(account_id, balance, reference)
      updater = account_updater(account_id, balance, reference)
      account = updater.account
      ActiveRecord::Base.on_shard(account.shard_id) do
        updater.save!
        puts "Adjusted credit for account_id #{account_id}"
        log(updater.log_string)
      end
    end

    def account_updater(account_id, balance, reference)
      Zendesk::Voice::AccountUpdater.new(account_id, balance, reference)
    end

    def log(log_data)
      File.open(File.join(@export_path, "voice_credit_import.log"), "a") { |file| file.puts log_data }
    end
  end
end
