module Zendesk
  module Voice
    module ControllerSupport
      def self.included(base)
        base.before_action :restrict_access_to_voice_cti, unless: :internal_request?
        base.before_action :find_or_create_partner_edition_account, if: :voice_partner_edition_request?
      end

      protected

      def restrict_access_to_voice_cti
        unless current_account.has_cti_integrations?
          Rails.logger.info "[CTI] Restricted from CTI due to small plan"

          head :forbidden
          return
        end

        return if current_account.voice_partner_edition_account.blank?

        unless current_account.voice_partner_edition_account.active?
          Rails.logger.info "[CTI] Restricted from CTI due to in-active partner edition account"

          head :forbidden
          return
        end
      end

      def capture_cti_usage(action:, display_user_id: nil, ticket_id: nil)
        return if internal_request?

        cti_usage = ::Voice::CtiUsage.new
        cti_usage.account_id = current_account.id
        cti_usage.user_id = current_user.id
        cti_usage.action = action
        cti_usage.display_user_id = display_user_id
        cti_usage.app_id = request.headers['X-Zendesk-App-Id']
        cti_usage.event = params[:event]
        cti_usage.ticket_id = ticket_id

        if cti_usage.save
          Rails.logger.debug "[CTI] Event was registered. user: '#{current_user.id}', action: '#{action}'"
        else
          Rails.logger.error "[CTI] Could not register the CTI usage. user: '#{current_user.id}', error: '#{cti_usage.errors.full_messages}', action:'#{action}'"
        end
      end

      def voice_partner_edition_request?
        !internal_request?
      end

      def internal_request?
        request.signed_internal_request?
      end

      def find_or_create_partner_edition_account
        ::Voice::PartnerEditionAccount.find_or_create_by_account(current_account)
      end
    end
  end
end
