require 'zendesk/internal_api/client'
require 'faraday_middleware/stale_response'
require 'faraday-http-cache'

module Zendesk::Voice
  class InternalApiClient
    REQUEST_TIMEOUT = 3.seconds.freeze

    RESCUABLE_ERRORS = [Faraday::TimeoutError, Kragle::ClientError].freeze
    NETWORK_ERRORS = [Faraday::Error::ConnectionFailed, ZendeskAPI::Error::NetworkError, Kragle::ServerError].freeze

    def initialize(subdomain, timeout: REQUEST_TIMEOUT)
      @account = Account.with_deleted { Account.find_by_subdomain!(subdomain) }
      @connection = @account.has_voice_vip? ? KragleConnection.build_for_voice(@account) : KragleConnection.build_for_account(@account, "voice")
      @timeout = timeout
      # This is required by Classic only.
      # https://zendesk.slack.com/files/ciaran/F1MF7L15L/Investigation_into_Faraday__ParsingError
      @connection.builder.insert_after Kragle::Middleware::Response::ContentType, FaradayMiddleware::Gzip
    end

    def create_sub_account
      connection.post("/api/v2/channels/voice/internal/sub_accounts.json")
    end

    def voice_subscription_details
      voice_account_url = '/api/v2/channels/voice/voice_account.json'
      get(voice_account_url)
    rescue Faraday::ConnectionFailed => ex
      Rails.logger.info("Error: Can't fetch Voice Account for #{@account.id} : #{ex.message}")
      nil
    rescue *NETWORK_ERRORS => ex
      ZendeskExceptions::Logger.record(ex, location: self, message: "Can't fetch Voice Account for #{@account.id} : #{ex.message}", fingerprint: '959d9c1fb38c0f909455e85ea79dc74f87db6aaf')
      nil
    end

    def update_sub_account(params = {}, request_headers = {})
      put("/api/v2/channels/voice/internal/sub_accounts.json", sub_account: params) do |req|
        request_headers.each do |key, value|
          req.headers[key.to_s] = value.to_s unless value.blank?
        end
      end
    end

    def suspend_sub_account
      put("/api/v2/channels/voice/internal/sub_accounts/suspend.json")
    end

    def get_sub_account # rubocop:disable Naming/AccessorMethodName
      # Configure stale cache middleware
      unless connection.builder.handlers.include?(FaradayMiddleware::StaleResponse)
        connection.builder.insert_after Kragle::Middleware::Instrumentation,
          FaradayMiddleware::StaleResponse, Rails.cache,
          'sub_account.stale_response_cache'
      end

      get('/api/v2/channels/voice/sub_account.json?include=features')
    end

    def get_voice_feature(feature)
      # Configure stale cache middleware
      unless connection.builder.handlers.include?(FaradayMiddleware::StaleResponse)
        connection.builder.insert_after Kragle::Middleware::Instrumentation,
          FaradayMiddleware::StaleResponse, Rails.cache,
          'check_feature.stale_response_cache'
      end

      get("/api/v2/channels/voice/internal/features/check/#{feature}")
    end

    def fetch_groups_phone_numbers
      get('/api/v2/channels/voice/groups_phone_numbers.json')
    end

    def create_sdk_embeddable(app_id:, snapcall_button_id:, group_id:, phone_number_id:, enabled: false)
      params = {
        talk_embeddable: {
          phone_number_id: phone_number_id,
          app_id: app_id,
          group_id: group_id,
          enabled: enabled,
          capability: MobileSdkApp::TALK_CAPABILITY_CLICK_TO_CALL,
          snapcall_button_id: snapcall_button_id
        }
      }

      post('/api/v2/channels/voice/talk_embeddables/sdk.json', params)
    rescue
      Rails.logger.error 'Could not create SDK Talk embeddable'
      raise
    end

    def update_sdk_embeddable(id, **params)
      params = {
        talk_embeddable: params
      }

      put("/api/v2/channels/voice/talk_embeddables/sdk/#{id}.json", params)
    end

    def delete_sdk_embeddable(id)
      delete("/api/v2/channels/voice/talk_embeddables/sdk/#{id}.json")
    end

    def voice_soft_delete_recordings(nice_ticket_id)
      condition = -> (_) do
        Rails.logger.debug("Retry soft delete recordings due to NetworkError")
        sleep 3
        true
      end
      Zendesk::Retrier.retry_on_error NETWORK_ERRORS, 3, retry_if: condition do
        delete("/api/v2/channels/voice/recordings/destroy_by_ticket_nice_id/#{nice_ticket_id}")
      end
    end

    def voice_subscription_created(plan_type)
      put('/api/v2/channels/voice/internal/sub_accounts/subscription_created.json',
        billing_subscription: { plan_type: plan_type })
    end

    def voice_subscription_updated(changes)
      put('/api/v2/channels/voice/internal/sub_accounts/subscription_updated.json',
        billing_subscription: changes)
    end

    def voice_fraud_report(account, fraud_report)
      return unless account.voice_enabled?
      put('/api/v2/channels/voice/internal/voice_account/fraud_report.json', fraud_report: fraud_report)
    end

    def voice_fraud_score(account, risky)
      return unless account.voice_enabled?

      put('/api/v2/channels/voice/internal/voice_account/fraud_score.json', fraud_score: { risky: risky })
    end

    def voice_create_new_trial(account)
      return if account.voice_enabled?
      post('/api/v2/channels/voice/internal/trials.json')
    rescue
      Rails.logger.error "Could not create trial for account #{account}"
      raise
    end

    def create_trial
      post('/api/v2/channels/voice/internal/suite_trials.json')
    end

    def voice_subscription_activated
      put('/api/v2/channels/voice/internal/sub_accounts/activate.json')
    end

    def remove_from_routing(group_id)
      put("/api/v2/channels/voice/internal/groups/#{group_id}/remove_from_routing.json")
    end

    def recent_active_agents(limit: 0)
      get('/api/v2/channels/voice/internal/sub_accounts/recent_active_agents.json', limit: limit)
    end

    def voice_seat_lost(user_id)
      condition = -> (_) do
        Rails.logger.debug("Retry mark_as_unavailable_on_voice due to NetworkError")
        sleep 3
        true
      end
      Zendesk::Retrier.retry_on_error NETWORK_ERRORS, 3, retry_if: condition do
        post("/api/v2/channels/voice/availabilities/#{user_id}/voice_seat_lost.json")
      end
    rescue Kragle::ResourceNotFound, ZendeskAPI::Error::RecordNotFound
      Rails.logger.info("Voice responded with 404 when trying to mark user unavailable (#{"/api/v2/channels/voice/availabilities/#{user_id}/voice_seat_lost.json"})")
    rescue *NETWORK_ERRORS
      ZendeskExceptions::Logger.record($!, location: self, message: "Impossible to mark agent unavailable for Voice due to Voice Error", fingerprint: '5d0be6b709e731501d467a8867be7c15e0b5d9b3')
    end

    def account_deleted!
      delete('/api/v2/channels/voice/internal/sub_accounts.json', account_id: @account.id)
    end

    %i[get post put patch delete].each do |verb|
      define_method(verb) do |url, params = {}, &block|
        make_request(verb, url, params, &block)
      end
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["internal_api_client"], tags: ["internal_api_client:voice"])
    end

    private

    attr_reader :connection, :timeout

    def make_request(verb, url, params = {})
      connection.send(verb, url, params) do |req|
        req.options.timeout = timeout
        yield(req) if block_given?
      end
    rescue *RESCUABLE_ERRORS => ex
      statsd_client.increment("error", tags: ["method:#{verb}", "url:#{url}", "error:#{ex.class}"])
      nil
    end
  end
end
