# The default range for moment data is from 2000-01-01 to 25 years from the current time.
require 'numeric_to_base60'

module Zendesk::MomentData
  DEFAULT_START_TIME = 946713600 # '2000-01-01'
  MINUTE = 60.0

  def moment_packed
    data = build_moment_data

    abbr_map = data[:abbr_offset_pairs].map(&:first).join(" ")
    offset_map = encode60_array(data[:abbr_offset_pairs].map(&:last)).join(" ")
    abbr_offset_index = data[:abbr_offset_indices].join("")
    timestamp_diffs = encode60_array(data[:timestamp_diffs]).join(" ")

    abbr_map = tzinfo.current_period.abbreviation if abbr_map.empty?
    offset_map = encode60(tzinfo.current_period.utc_total_offset / -MINUTE) if offset_map.empty?
    abbr_offset_index = "0" if abbr_offset_index.empty?

    [tzinfo.name, abbr_map, offset_map, abbr_offset_index, timestamp_diffs].join("|")
  end

  private

  def build_moment_data
    data = {
      abbr_offset_pairs: [],
      abbr_offset_indices: [],
      last_timestamp: 0,
      timestamp_diffs: [],
    }

    transitions = TZInfo::Timezone.get(tzinfo.name).transitions_up_to(25.years.from_now, DEFAULT_START_TIME)

    transitions.each do |transition, _index|
      data = add_moment_abbr_offset_index(transition.previous_offset, data)
      data = add_moment_timestamp_diff(transition, data)
      data[:last_transition] = transition
    end

    last_transition = data[:last_transition] || tzinfo.current_period
    data = add_moment_abbr_offset_index(last_transition.offset, data)

    data
  end

  def add_moment_abbr_offset_index(offset, data)
    offset_in_minutes = offset.utc_total_offset / -MINUTE
    abbr_offset_pair = [offset.abbreviation, offset_in_minutes]
    pair_index = data[:abbr_offset_pairs].index(abbr_offset_pair)

    if pair_index
      data[:abbr_offset_indices] << encode60(pair_index)
    else
      data[:abbr_offset_pairs] << abbr_offset_pair
      data[:abbr_offset_indices] << data[:abbr_offset_pairs].length - 1
    end
    data
  end

  def add_moment_timestamp_diff(transition, data)
    timestamp = transition.at.to_i / MINUTE
    data[:timestamp_diffs] << timestamp - data[:last_timestamp]
    data[:last_timestamp] = timestamp
    data
  end

  def encode60(number)
    NumericToBase60.encode60(number, 1)
  end

  def encode60_array(array)
    array.map { |element| encode60(element) }
  end
end
