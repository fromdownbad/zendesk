require 'iso8601'

module Zendesk
  module CustomParameterTypes
    class ISO8601DateTimeConstraint < StrongerParameters::Constraint
      def value(v)
        ISO8601::DateTime.new(v.to_s).to_time.utc
      rescue ISO8601::Errors::UnknownPattern
        StrongerParameters::InvalidValue.new(v, "It must be an acceptable ISO 8601 date format")
      rescue ISO8601::Errors::RangeError
        StrongerParameters::InvalidValue.new(v, "It is not in a valid range")
      end
    end

    def ids
      StrongerParameters::RegexpConstraint.new(/\A\s*(?:(?:\d+|suspended|deleted)(?:\s*,\s*|\s*,?\s*\z))+/)
    end

    def id
      raise "use bigid"
    end

    def date_time
      ISO8601DateTimeConstraint.new
    end
  end
end
