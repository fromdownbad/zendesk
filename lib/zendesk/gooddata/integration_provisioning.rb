require 'forwardable'

module Zendesk
  module Gooddata
    class IntegrationProvisioning
      extend Forwardable

      INTEGRATION_VERSION = 2
      PROJECT_TITLE       = '%s Insights'.freeze
      CONNECTOR           = 'zendesk4'.freeze
      TEMPLATE_NAME       = 'ZendeskAnalytics'.freeze
      TEMPLATE_VERSION    = 33
      API_DOMAIN          = '%s.zendesk.com'.freeze
      ZOPIM_API_DOMAIN    = 'www.zopim.com'.freeze

      PROJECT_ROLE_MAPPING = {
        'adminRole' => :admin_role_id,
        'zendeskEditorRole' => :editor_role_id,
        'readOnlyUserRole' => :read_only_user_role_id
      }.freeze

      ATTEMPT_INTERVAL = 2.seconds
      ATTEMPT_LIMIT    = 150

      def self.destroy_for_account(account)
        gooddata_integration = account.gooddata_integration

        return unless gooddata_integration.present?

        gooddata_integration.soft_delete

        Zendesk::Gooddata::UserProvisioning.destroy_for_account(account)
        GooddataIntegrationsDestroyJob.enqueue(
          account.id,
          gooddata_integration.id
        )
      end

      def self.integration_template_version(gooddata_project_id)
        Zendesk::Gooddata::Client.v2.project(id: gooddata_project_id).integration(connector: CONNECTOR).find do |response|
          response[:integration][:projectTemplate].scan(/\d+$/).first.to_i
        end
      end

      attr_reader :account, :admin, :gooddata_client

      def initialize(account, options = {})
        @account         = account
        @admin           = options[:admin]
        @gooddata_client = Zendesk::Gooddata::Client.v2
        @gooddata_integration = options[:gooddata_integration]
      end

      def gooddata_integration
        @gooddata_integration || account.gooddata_integration
      end

      def perform
        create_gooddata_integration unless gooddata_integration.present?

        if gooddata_integration.pending?
          confirm_gooddata_project_enabled && create_gooddata_connector
        end

        configure_gooddata_connector    if gooddata_integration.created?
        cache_gooddata_project_role_ids if gooddata_integration.configured?

        if gooddata_integration.complete?
          load_color_palette_to_project(gooddata_integration.project_id)
          set_project_time_zone
          provision_users
        end
      end

      def re_enable
        gooddata_re_enable
        enable_gooddata_integration
        provision_users
        kick_off_incremental_reload
        gooddata_re_enable_complete
      end

      def create_gooddata_integration
        if account.gooddata_integration.present?
          fail "An integration already exists for account #{account.id}."
        end

        gooddata_integration = account.create_gooddata_integration(
          version: INTEGRATION_VERSION,
          project_id: -1,
          admin_id: admin.id
        )

        gooddata_project_id = gooddata_client.project.create(
          title: project_title,
          template_name: TEMPLATE_NAME,
          template_version: TEMPLATE_VERSION
        ) { |response| response[:uri].split('/').last }

        gooddata_integration.project_id = gooddata_project_id
        gooddata_integration.save!

        true
      end

      def create_gooddata_connector
        fail_due_to_improper_state! unless gooddata_integration.pending?

        gooddata_project.integration(connector: CONNECTOR).create(
          template_name: TEMPLATE_NAME,
          template_version: TEMPLATE_VERSION
        )

        gooddata_integration.integration_created!

        true
      end

      def configure_gooddata_connector
        fail_due_to_improper_state! unless gooddata_integration.created?

        set_project_api_domains

        gooddata_integration.integration_configured!

        true
      end

      def cache_gooddata_project_role_ids
        fail_due_to_improper_state! unless gooddata_integration.configured?

        assign_role_attributes

        gooddata_integration.finished!

        true
      end

      def upgrade_gooddata_integration(v2_project_id)
        previous_integration = gooddata_integration

        GooddataIntegration.transaction do
          new_integration_fields = {
            project_id: v2_project_id,
            version: INTEGRATION_VERSION,
            status: :configured
          }

          if previous_integration.present?
            new_integration_fields[:scheduled_at] = previous_integration.scheduled_at
            previous_integration.soft_delete
          end

          account.reload.create_gooddata_integration(new_integration_fields)
        end

        cache_gooddata_project_role_ids
        migrate_users_to_new_project(previous_integration.project_id, v2_project_id) if previous_integration.present?

        true
      end

      def destroy_gooddata_integration
        if gooddata_project_enabled?
          project_template_version = self.class.integration_template_version(gooddata_integration.project_id)

          gooddata_project.integration(connector: CONNECTOR).update(
            active: false,
            template_name: TEMPLATE_NAME,
            template_version: project_template_version
          )

          gooddata_project.delete
        end

        true
      end

      def enable_gooddata_integration
        project_template_version = self.class.integration_template_version(gooddata_integration.project_id)

        gooddata_project.integration(connector: CONNECTOR).update(
          active: true,
          template_name: TEMPLATE_NAME,
          template_version: project_template_version
        )

        true
      end

      def gooddata_re_enable
        gooddata_integration.re_enable!
      end

      def gooddata_re_enable_complete
        gooddata_integration.re_enable_complete!
      end

      def set_project_time_zone
        gooddata_project.configure_time_zone(time_zone: formatted_time_zone)
      end

      def set_project_title
        gooddata_project.set_title(title: project_title)
      end

      def set_project_api_domains
        gooddata_project.integration(connector: CONNECTOR).configure(
          api_domain: api_domain,
          zopim_api_domain: ZOPIM_API_DOMAIN,
          zopim_subdomain: account.subdomain
        )
      end

      def kick_off_full_reload
        response = gooddata_project.integration(connector: CONNECTOR).synchronize(incremental: false)
        gooddata_client.get(response[:uri])
      end

      def kick_off_incremental_reload
        response = gooddata_project.integration(connector: CONNECTOR).synchronize(incremental: true)
        gooddata_client.get(response[:uri])
      end

      def assign_role_attributes
        [:admin_role_id,
         :editor_role_id,
         :read_only_user_role_id].each do |role_attribute|
          gooddata_integration.send(
            "#{role_attribute}=",
            project_role_mapping.fetch(role_attribute)
          )
        end
      end

      private

      def provision_users
        if admin
          Zendesk::Gooddata::UserProvisioning.new(account).create_gooddata_user(admin)
        end

        Zendesk::Gooddata::UserProvisioning.create_all_for_account(account)
      end

      def migrate_users_to_new_project(old_project_id, new_project_id)
        GooddataUser.where(gooddata_project_id: old_project_id).each do |gooddata_user|
          gooddata_user.gooddata_project_id = new_project_id
          gooddata_user.save

          Zendesk::Gooddata::UserProvisioning.sync_for_user(gooddata_user.user)
        end
      end

      def confirm_gooddata_project_enabled
        attempts = 0

        until gooddata_project_enabled?
          sleep ATTEMPT_INTERVAL

          attempts += 1

          fail ProjectNotEnabled if attempts >= ATTEMPT_LIMIT
        end

        true
      end

      def gooddata_project_enabled?
        gooddata_project.find do |response|
          Rails.logger.info "IntegrationProvisioning#gooddata_project_enabled?: #{response[:project][:content]}"
          response[:project][:content][:state] == 'ENABLED'
        end
      end

      def project_role_mapping
        return @project_role_mapping if @project_role_mapping

        @project_role_mapping = project_role_ids.each_with_object({}) do |role_id, role_mapping|
          gooddata_project.role(id: role_id) do |response|
            gooddata_role = response[:projectRole][:meta][:identifier]

            if PROJECT_ROLE_MAPPING.keys.include?(gooddata_role)
              role_mapping.store(PROJECT_ROLE_MAPPING[gooddata_role], role_id)
            end
          end
        end
      end

      def project_role_ids
        gooddata_project.roles do |response|
          response[:projectRoles][:roles].map { |uri| uri.split('/').last }
        end
      end

      def formatted_time_zone
        ActiveSupport::TimeZone[account.time_zone].tzinfo.identifier
      end

      def api_domain
        API_DOMAIN % account.subdomain
      end

      def project_title
        PROJECT_TITLE % account.subdomain
      end

      def fail_due_to_improper_state!
        fail "The GoodData integration for account #{account.id} " \
          'is not in the proper state.'
      end

      def gooddata_project
        @gooddata_project ||=
          gooddata_client.project(id: gooddata_integration.project_id)
      end

      def color_palette_json
        JSON(File.read("#{Rails.root}/config/gooddata_color_palette.json"))
      end

      def load_color_palette_to_project(_project_id)
        gooddata_project.configure_style_settings(
          settings: color_palette_json
        )
      end

      ProjectNotEnabled = Class.new(StandardError)
    end
  end
end
