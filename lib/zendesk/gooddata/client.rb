module Zendesk
  module Gooddata
    class Client
      class << self
        def v2
          @v2_gooddata_client ||= create_client
        end

        private

        def create_client
          client = GoodData.client do |gd|
            config = Zendesk::GooddataIntegrations::Configuration

            gd.api_endpoint       = config.fetch(:api_endpoint)
            gd.login              = config.fetch(:login)
            gd.password           = config.fetch(:password)
            gd.auth_token         = config.fetch(:auth_token)
            gd.organization       = config.fetch(:organization)
            gd.sso_provider       = config.fetch(:sso_provider)
            gd.signer_email       = config.fetch(:signer_email)
            gd.signer_private_key = config.signer_private_key
          end

          ddtrace = Faraday::Middleware.load_middleware(:ddtrace)

          if ddtrace.present?
            client.send(:connection).send(:connection).builder.
              insert(0, ddtrace, service_name: 'classic-faraday-gooddata')
          end

          client
        end
      end
    end
  end
end
