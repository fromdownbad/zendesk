require 'forwardable'
require 'securerandom'

module Zendesk
  module Gooddata
    class UserProvisioning
      extend Forwardable

      def self.create_for_user(zendesk_user)
        if zendesk_user.account.gooddata_integration.present?
          GooddataUserCreateJob.enqueue(
            zendesk_user.account_id,
            zendesk_user.id
          )
        else
          Rails.logger.info "Account #{zendesk_user.account.id} does not have a GoodData integration."
        end
      end

      def self.sync_for_user(zendesk_user)
        gooddata_user = GooddataUser.for_user(zendesk_user)
        gooddata_integration = zendesk_user.account.gooddata_integration

        if gooddata_user.present?
          GooddataUserSyncJob.enqueue(
            zendesk_user.account_id,
            gooddata_user.id
          )
        else
          GooddataUserCreateJob.enqueue(
            zendesk_user.account_id,
            zendesk_user.id
          ) if gooddata_integration.present?
        end
      end

      def self.destroy_for_account(account)
        gooddata_users = GooddataUser.users_for_account(account)
        gooddata_users.each do |gooddata_user|
          GooddataUserDestroyJob.enqueue(
            account.id,
            gooddata_user.id
          )
        end
      end

      def self.destroy_for_user(zendesk_user)
        gooddata_user = GooddataUser.for_user(zendesk_user)

        return unless gooddata_user.present?

        GooddataUserDestroyJob.enqueue(
          zendesk_user.account_id,
          gooddata_user.id
        )
      end

      def self.create_all_for_account(account)
        account.agents.each do |agent|
          Zendesk::Gooddata::UserProvisioning.create_for_user(agent)
        end
      end

      def self.sync_all_for_account(account)
        account.agents.each do |agent|
          Zendesk::Gooddata::UserProvisioning.sync_for_user(agent)
        end
      end

      # Helper method used for debugging and manual account correction
      def self.reprovision_all_users(account)
        account.agents.each do |agent|
          Zendesk::Gooddata::UserProvisioning.reprovision_user(agent)
        end
      end

      def self.reprovision_user(zendesk_user)
        if should_destroy_user?(zendesk_user)
          Zendesk::Gooddata::UserProvisioning.destroy_for_user(zendesk_user)
          # waiting for the user destroy job to finish
          wait_for(15) { GooddataUser.for_user(zendesk_user).nil? }
        end

        Zendesk::Gooddata::UserProvisioning.sync_for_user(zendesk_user)
      end

      def self.should_destroy_user?(zendesk_user)
        gooddata_user = GooddataUser.for_user(zendesk_user)

        gooddata_user.present? &&
          (login_deleted?(gooddata_user) || project_mismatched?(zendesk_user, gooddata_user))
      end

      def self.wait_for(sleep_interval)
        sleepy_time = 0

        until yield
          sleep(sleep_interval)
          sleepy_time += sleep_interval
          raise Timeout::Error if sleepy_time >= 120
        end
      end

      def self.login_deleted?(gooddata_user)
        client = Zendesk::Gooddata::Client.v2
        response = client.get("/gdc/account/profile/#{gooddata_user.gooddata_user_id}")
        response[:accountSetting][:login].start_with?('deleted-')
      rescue GoodData::Error::Forbidden, GoodData::Error::NotFound
        true
      end

      def self.project_mismatched?(zendesk_user, gooddata_user)
        zendesk_user.account.gooddata_integration.project_id != gooddata_user.gooddata_project_id
      end

      attr_reader :account, :gooddata_client

      def initialize(account)
        @account         = account
        @gooddata_client = Zendesk::Gooddata::Client.v2
      end

      delegate [:gooddata_integration] => :account

      def create_gooddata_user(zendesk_user)
        return false if GooddataUser.for_user(zendesk_user).present?

        unless gooddata_integration.present?
          Rails.logger.info "Account #{account.id} does not have a GoodData integration."
          return false
        end

        delete_existing_gooddata_user_if_present(zendesk_user)

        gooddata_user = GooddataUser.create! do |u|
          u.account_id = account.id
          u.user_id = zendesk_user.id
          u.gooddata_project_id = gooddata_integration.project_id
          u.gooddata_user_id = create_user_in_gooddata(zendesk_user)
        end

        unless gooddata_user.gooddata_role?(:none)
          gooddata_project.add_user(
            user_id: gooddata_user.gooddata_user_id,
            role_id: gooddata_integration.role_id_for(
              gooddata_user.gooddata_role
            )
          )

          zendesk_user.settings.show_insights_onboarding = true
          zendesk_user.settings.save!
        end

        true
      end

      def sync_gooddata_user(gooddata_user)
        unless gooddata_integration.present?
          fail "Account #{account.id} does not have a GoodData integration."
        end

        if gooddata_user.user.is_agent? &&
          gooddata_user.user.is_active? &&
          !gooddata_user.gooddata_role?(:none)

          gooddata_project.update_user(
            user_id: gooddata_user.gooddata_user_id,
            role_id: gooddata_integration.role_id_for(
              gooddata_user.gooddata_role
            )
          )

          gooddata_client.user(id: gooddata_user.gooddata_user_id).update(
            first_name: gooddata_user.first_name,
            last_name: gooddata_user.last_name,
            email: gooddata_user.email
          )
        else
          destroy_gooddata_user(gooddata_user)
        end

        true
      end

      def destroy_gooddata_user(gooddata_user)
        unless UserProvisioning.login_deleted?(gooddata_user)
          gooddata_client.user(id: gooddata_user.gooddata_user_id).delete
        end

        unless gooddata_user.destroy
          fail "GoodData user #{gooddata_user.id} could not be destroyed."
        end

        true
      end

      private

      def delete_existing_gooddata_user_if_present(zendesk_user)
        existing_user_id = find_existing_user_in_gooddata(zendesk_user)
        gooddata_client.user(id: existing_user_id).delete if existing_user_id.present?
      end

      def find_existing_user_in_gooddata(zendesk_user)
        gooddata_client.user.find_by_login(
          login: GooddataUser.gooddata_login_for_user(zendesk_user)
        ) do |response|
          return unless (user = response[:accountSettings][:items].first)

          user[:accountSetting][:links][:self].split('/').last
        end
      end

      def create_user_in_gooddata(zendesk_user)
        gooddata_client.user.create(
          login: GooddataUser.gooddata_login_for_user(zendesk_user),
          password: SecureRandom.hex(50),
          first_name: zendesk_user.first_name,
          last_name: zendesk_user.last_name,
          email: zendesk_user.email
        ) { |response| response.fetch(:uri).split('/').last }
      end

      def gooddata_project
        @gooddata_project ||=
          gooddata_client.project(id: gooddata_integration.project_id)
      end
    end
  end
end
