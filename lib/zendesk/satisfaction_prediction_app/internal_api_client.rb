module Zendesk::SatisfactionPredictionApp
  class InternalApiClient
    APP_NAME = 'satisfaction-prediction-app'.freeze
    AVAILABLE_ENDPOINT = "/api/v2/satisfaction_prediction/private/prediction/account/%{account_id}/available".freeze

    def initialize(account)
      @account = account
    end

    def fetch_satisfaction_prediction_available
      endpoint = AVAILABLE_ENDPOINT % { account_id: account.id }
      connection.get(endpoint).body["available"]
    end

    def url_prefix
      "http://pod-#{ENV.fetch('ZENDESK_POD_ID')}.satisfaction-prediction-app.service.consul:3000"
    end

    private

    attr_reader :account

    def connection
      connection = Kragle.new(APP_NAME, shared_cache: false)
      connection.url_prefix = url_prefix
      connection
    end
  end
end
