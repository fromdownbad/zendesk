module Zendesk::SupportAccounts
  class Role::System < Role
    LIGHT_AGENT_ROLE_NAME = 'light_agent'.freeze
    AGENT_ROLE_NAME = 'agent'.freeze
    ADMIN_ROLE_NAME = 'admin'.freeze

    # rubocop:disable Lint/UnusedMethodArgument
    def sync!(context: nil, permissions_changed: [], enabled: nil)
      staff_service_client.toggle_role!(
        name: @role_name,
        context: context,
        assignable: enabled.nil? ? assignable? : enabled
      )
      sync_guide_light_agent(context, enabled) if @role_name == LIGHT_AGENT_ROLE_NAME
    rescue Kragle::ResourceNotFound => e
      # This should only ever occur the first time a system role is synced
      case @role_name
      when LIGHT_AGENT_ROLE_NAME
        initialize_light_agent(context)
      when AGENT_ROLE_NAME, ADMIN_ROLE_NAME
        sync_role_templates(context)
      else
        raise e
      end
    end
    # rubocop:enable Lint/UnusedMethodArgument

    private

    def sync_guide_light_agent(context, enabled)
      return unless account.has_ocp_guide_light_agent_role_sync?
      staff_service_client.toggle_role!(
        name: @role_name,
        context: context,
        product: Zendesk::Accounts::Client::GUIDE_PRODUCT,
        assignable: enabled.nil? ? assignable? : enabled
      )
    rescue Kragle::ResourceNotFound
      staff_service_client.create_role!(
        title: "txt.default.roles.guide.light_agent.name",
        description: "txt.default.roles.guide.light_agent.description",
        name: @role_name,
        max_agents_countable: false,
        is_system_role: true,
        is_active: true,
        assignable: enabled.nil? ? assignable? : enabled,
        product: 'guide',
        context: context
      )
    end

    def assignable?
      case @role_name
      when LIGHT_AGENT_ROLE_NAME
        light_agent_assignable?
      when AGENT_ROLE_NAME
        agent_assignable?
      when ADMIN_ROLE_NAME
        admin_assignable?
      else
        raise "#{@role_name} is not a recognised system role"
      end
    end

    def admin_assignable?
      true
    end

    def agent_assignable?
      return true if account.has_ocp_agent_assignable_on_enterprise?

      !account.has_permission_sets?
    end

    def light_agent_assignable?
      permission_set.present?
    end

    def initialize_light_agent(context)
      return if permission_set.nil?
      fix_light_agent_description if permission_set.description.blank?

      staff_service_client.create_role!(
        title: permission_set.name,
        description: permission_set.description || light_agent_description,
        name: @role_name,
        max_agents_countable: false,
        is_system_role: true,
        is_active: true,
        assignable: assignable?,
        context: context
      )
      sync_guide_light_agent(context, assignable?)
    end

    def light_agent_description
      I18n.t('txt.default.roles.light_agent.description')
    end

    def fix_light_agent_description
      ::I18n.locale = account.translation_locale
      permission_set.update_columns(description: light_agent_description)
    end

    def sync_role_templates(context)
      staff_service_client.create_role!(
        title: "txt.default.roles.support.#{@role_name}.name",
        description: "txt.default.roles.support.#{@role_name}.description",
        name: @role_name,
        max_agents_countable: true,
        is_system_role: true,
        is_active: true,
        assignable: assignable?,
        context: context
      )
    end
  end
end
