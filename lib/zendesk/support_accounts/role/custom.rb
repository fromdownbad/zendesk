module Zendesk::SupportAccounts
  class Role::Custom < Role
    def sync!(context: nil, permissions_changed: [], enabled: nil)
      @permissions_changed = permissions_changed
      assignable = enabled.nil? ? account.has_permission_sets? : enabled
      permission_set.nil? ? delete_role(context) : upsert_role(context, assignable)
      guide_synchronization.perform! if guide_synchronization.perform?
      explore_synchronization.perform! if explore_synchronization.perform?
    end

    private

    def upsert_role(context, assignable)
      staff_service_client.update_or_create_role!(
        title: permission_set.name,
        description: permission_set.description.present? ? permission_set.description : permission_set.name,
        name: permission_set.pravda_role_name,
        max_agents_countable: true,
        context: context,
        assignable: assignable
      )
    end

    def delete_role(context)
      staff_service_client.delete_role!(name: @role_name, context: context)
    rescue Kragle::ResourceNotFound
      Rails.logger.warn("Role #{@role_name} was already deleted for account: #{account.id}")
    end

    def guide_synchronization
      Zendesk::SupportAccounts::Internal::GuideRoleEntitlementsSynchronization.new(account, permission_set, @permissions_changed)
    end

    def explore_synchronization
      Zendesk::SupportAccounts::Internal::ExploreRoleEntitlementsSynchronization.new(account, permission_set, @permissions_changed)
    end
  end
end
