module Zendesk::SupportAccounts
  class Role::NonSupport < Role
    def sync!(context: nil, permissions_changed: []); end
  end
end
