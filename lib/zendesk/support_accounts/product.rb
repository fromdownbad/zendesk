module Zendesk::SupportAccounts
  class Product
    ACCOUNT_CHANGE_COLUMNS = ['is_active', 'is_serviceable', 'sandbox_master_id', 'deleted_at'].freeze
    SUBSCRIPTION_CHANGE_COLUMNS = ['plan_type', 'max_agents', 'trial_expires_on', 'is_trial', 'churned_on', 'payment_method_type'].freeze
    FEATURE_BOOST_CHANGE_COLUMNS = ['is_active', 'valid_until', 'boost_level'].freeze

    private_class_method :new

    def self.retrieve(account)
      if account.pre_created_account? || account.id == SYSTEM_ACCOUNT_ID || account.subscription.nil?
        NilProduct.new(account: account, subscription: account.subscription)
      else
        new(account)
      end
    end

    def initialize(account)
      @account = account
      @subscription = account.subscription
      @feature_boost = account.feature_boost
    end

    def save!
      account.save!
      subscription.save!
      feature_boost.save! if feature_boost
      Omnichannel::AccountProductSyncJob.enqueue(account_id: account.id) if product_changed?
    end

    def save(validate: true)
      account.save(validate: validate)
      subscription.save(validate: validate)
      feature_boost.save(validate: validate) if feature_boost
      Omnichannel::AccountProductSyncJob.enqueue(account_id: account.id) if product_changed?
    end

    def update_plan_settings!(model)
      model.save!
      Omnichannel::AccountProductPlanSettingsSyncJob.enqueue(account_id: model.account_id)
    end

    def soft_delete!
      account.soft_delete!(validate: false)
      Omnichannel::AccountProductSyncJob.enqueue(account_id: account.id)
    end

    def restore!
      raise 'Cannot restore a partially deleted account' unless account.data_deletion_audits.for_cancellation.all? { |audit| audit.waiting? || audit.canceled? }
      account.data_deletion_audits.for_cancellation.waiting.each(&:cancel!)

      ::Account.with_deleted { account.soft_undelete! }

      subscription.canceled_on = Time.now
      save!
    end

    def to_h
      result = {
        name: name,
        state: state,
        trial_expires_at: trial_expires_on,
        plan_settings: plan_settings
      }
      unless state_updated_at.nil?
        result[:state_updated_at] = state_updated_at
      end
      result
    end

    def plan_settings
      {
        plan_type: plan_type,
        max_agents: max_agents,
        light_agents: light_agents?,
        suite: suite,
        side_conversations: side_conversations,
        boosted_plan_type: boosted_plan_type,
        # NOTE: Classic calls it `boost_valid_until`, but Pravda calls it `boost_expires_at`
        boost_expires_at: boost_valid_until
      }
    end

    private

    attr_reader :account, :subscription, :feature_boost

    def product_changed?
      (account.previous_changes.keys & ACCOUNT_CHANGE_COLUMNS).any? ||
        (subscription.previous_changes.keys & SUBSCRIPTION_CHANGE_COLUMNS).any? ||
        (feature_boost && (feature_boost.previous_changes.keys & FEATURE_BOOST_CHANGE_COLUMNS).any?)
    end

    def name
      Zendesk::Accounts::Client::SUPPORT_PRODUCT
    end

    def state
      if !account.is_active? || account.deleted_at.present?
        :cancelled
      elsif subscription.zuora_managed?
        :subscribed
      elsif free?(account, subscription)
        :free
      elsif account.brands.empty?
        :not_started
      elsif expired_trial?(account, subscription)
        :expired
      elsif expired_sponsored?(account, subscription)
        :expired
      elsif subscription.is_trial?
        :trial
      else
        :unknown
      end
    end

    def trial_expires_on
      subscription.trial_expires_on.try(:to_datetime).try(:iso8601)
    end

    def state_updated_at
      if state == :expired && subscription.trial_expires_on.present?
        subscription.trial_expires_on.to_datetime.iso8601
      elsif state == :cancelled && subscription.churned_on.present?
        subscription.churned_on.to_datetime.iso8601
      end
    end

    def plan_type
      subscription.plan_type
    end

    def max_agents
      subscription.max_agents
    end

    def light_agents?
      subscription.has_light_agents?
    end

    def suite
      if subscription.zuora_managed?
        account.settings.suite_subscription?
      else
        account.settings.suite_trial?
      end
    end

    def side_conversations
      subscription.has_ticket_threads?
    end

    def expired_trial?(account, subscription)
      (account.multiproduct || !account.is_serviceable?) &&
        !subscription.is_trial &&
        subscription.trial_expires_on.present? &&
        subscription.trial_expires_on.to_date <= Date.today
    end

    def expired_sponsored?(account, subscription)
      !account.is_serviceable? && subscription.trial_expires_on.nil?
    end

    # Following completion of https://zendesk.atlassian.net/browse/VOLT-743:
    # Free state should not be used for integration partner accounts or employee personal accounts.
    # All existing non-canary, sponsored accounts should be moved into Zuora - resulting in a 'subscribed' state.
    def free?(account, subscription)
      account.is_sandbox? ||
        subscription.is_spoke? ||
        subscription.is_sponsored? ||
        subscription.payment_method_type == PaymentMethodType.Manual ||
        legacy_sponsored_account?(account, subscription)
    end

    def legacy_sponsored_account?(account, subscription)
      !account.multiproduct &&
        account.is_serviceable? &&
        !subscription.is_trial? &&
        !subscription.zuora_managed? &&
        subscription.payment_method_type == PaymentMethodType.Credit_Card
    end

    def boosted_plan_type
      feature_boost.boost_level if feature_boost_relevant?
    end

    def boost_valid_until
      feature_boost.valid_until if feature_boost_relevant?
    end

    def feature_boost_relevant?
      feature_boost &&
        feature_boost.is_active &&
        feature_boost.valid_until > Time.now &&
        feature_boost.boost_level > plan_type
    end
  end
end
