module Zendesk::SupportAccounts
  class Account
    def initialize(record:)
      @record = record
    end

    def id
      record.id
    end

    def entitlements_sync_enabled?
      existing_subscription? && not_pre_created_account?
    end

    def role_sync_enabled?
      existing_subscription? && not_pre_created_account?
    end

    private

    attr_reader :record

    def existing_subscription?
      record.subscription.present?
    end

    def not_pre_created_account?
      !record.pre_created_account?
    end
  end
end
