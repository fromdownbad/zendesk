module Zendesk::SupportAccounts
  class NilProduct
    def initialize(account:, subscription:)
      @account = account
      @subscription = subscription
    end

    def soft_delete!
      account.soft_delete!(validate: false)
    end

    def save!
      account.save!
      subscription.save! if subscription.present?
    end

    def save(validate: true)
      account.save(validate: validate)
      subscription.save(validate: validate) if subscription.present?
    end

    def update_plan_settings!(model)
      model.save!
    end

    def present?
      false
    end

    def blank?
      true
    end

    def nil?
      true
    end

    private

    attr_reader :account, :subscription
  end
end
