module Zendesk::SupportAccounts
  class Role
    attr_reader :account, :role_name

    CUSTOM_ROLE_PREFIX = 'custom'.freeze

    def initialize(account:, permission_set_id: nil, role_name: nil)
      @account = account
      @permission_set_id = permission_set_id
      @role_name = role_name
    end

    def self.for_account(account_id)
      account = ::Account.find account_id
      non_default_roles = account.permission_sets.map do |ps|
        RoleFactory.retrieve(
          account: account, permission_set_id: ps.id, role_name: ps.pravda_role_name
        )
      end
      non_default_roles + default_for(account_id)
    end

    def self.default_for(account_id)
      account = ::Account.find account_id
      [System::ADMIN_ROLE_NAME, System::AGENT_ROLE_NAME].map do |role_name|
        RoleFactory.retrieve(account: account, permission_set_id: nil, role_name: role_name)
      end
    end

    def sync!(*)
      raise 'Must be implemented by sublcass'
    end

    private

    def permission_set
      @permission_set ||= account.permission_sets.find_by(id: @permission_set_id)
    end

    def staff_service_client
      @staff_service_client ||= Zendesk::StaffClient.new(
        account,
        retry_options: {
          max: 3, # retry up to 3 times
          interval: 1, # 1st retry after 1 second
          backoff_factor: 1.5, # wait time until 2nd retry: 1.5 sec, 3rd retry: 2.25 sec
          exceptions: Zendesk::StaffClient::RETRYABLE_ERRORS,
          methods: Faraday::Request::Retry::IDEMPOTENT_METHODS + [:patch]
        }
      )
    end
  end
end
