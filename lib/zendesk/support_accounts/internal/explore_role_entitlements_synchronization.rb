module Zendesk
  module SupportAccounts
    module Internal
      class ExploreRoleEntitlementsSynchronization
        include Zendesk::SupportUsers::Internal::AuditHeaders

        def initialize(account, permission_set, permissions_changed)
          @account = account
          @permission_set = permission_set
          @permissions_changed = permissions_changed
        end

        def perform?
          permission_set.present? && explore_permissions_changed?
        end

        def perform!
          permission_set.users.each do |user|
            Omnichannel::ExploreEntitlementSyncJob.enqueue(account_id: account.id, user_id: user.id, audit_headers: audit_headers)
          end
        end

        private

        attr_reader :account, :permission_set

        def explore_permissions_changed?
          @permissions_changed.any? { |permission| PermissionExploreEntitlementsChangesObserver::EXPLORE_PERMISSIONS.include?(permission) }
        end
      end
    end
  end
end
