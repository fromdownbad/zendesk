module Zendesk::SupportAccounts
  class RoleFactory
    class << self
      def retrieve(account:, permission_set_id:, role_name:)
        role_type(role_name).new(account: account, permission_set_id: permission_set_id, role_name: role_name)
      end

      private

      def role_type(role_name)
        return Zendesk::SupportAccounts::Role::NonSupport if role_name.nil?
        role_name.start_with?('custom') ? Zendesk::SupportAccounts::Role::Custom : Zendesk::SupportAccounts::Role::System
      end
    end
  end
end
