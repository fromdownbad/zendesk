require "gravatar-ultimate"

module Zendesk
  class Gravatar
    DEFAULTS = {
      r: "g", default: "https://assets.zendesk.com/images/2016/default-avatar-80.png", ssl: true, size: 80
    }.freeze

    def self.url(user, options = {})
      if user.email.present? && user.account.settings.have_gravatars_enabled?
        ::Gravatar.new(user.email).image_url(options.reverse_merge(DEFAULTS))
      end
    end
  end
end
