module Zendesk
  module CloudflareRateLimiting
    # See https://techmenu.zende.sk/adrs/generic-cloudflare-rate-limit

    EDGE_RATE_LIMIT_HEADER_NAME = 'ZENDESK-EP'.freeze
    STRONG_HEADER = 5 # Challenge (Captcha) - 50 requests/min
    MILD_HEADER = 10 # Block - 200 requests/min
    NEW_ACCOUNT_TICKET_CREATION_HEADER = 21
    TRIAL_ACCOUNT_TICKET_CREATION_HEADER = 22

    private

    def rate_limit_header(limit_type, simulate: false)
      response.headers[EDGE_RATE_LIMIT_HEADER_NAME] =
        case limit_type
        when :mild
          simulate ? -MILD_HEADER : MILD_HEADER
        when :strong
          simulate ? -STRONG_HEADER : STRONG_HEADER
        when :new_account_ticket_creation
          simulate ? -NEW_ACCOUNT_TICKET_CREATION_HEADER : NEW_ACCOUNT_TICKET_CREATION_HEADER
        when :trial_account_ticket_creation
          simulate ? -TRIAL_ACCOUNT_TICKET_CREATION_HEADER : TRIAL_ACCOUNT_TICKET_CREATION_HEADER
        else
          ''
        end
    end
  end
end
