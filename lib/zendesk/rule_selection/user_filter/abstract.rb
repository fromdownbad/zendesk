module Zendesk::RuleSelection
  module UserFilter
    class Abstract
      def initialize(context)
        @context = context
      end

      def to_arel
        [account(:arel), group(:arel), personal(:arel)].compact.reduce(:or)
      end

      def to_elasticsearch
        filters = [account(:es), group(:es), personal(:es)].compact

        return nil if filters.empty?

        "(#{filters.join(OR_JOIN)})"
      end

      protected

      attr_reader :context

      private

      def account(backend)
        send("account_#{backend}")
      end

      def account_arel
        nil
      end

      def account_es
        nil
      end

      def group(backend)
        return unless context.user.account.subscription.has_group_rules?

        send("group_#{backend}")
      end

      def group_arel
        nil
      end

      def group_es
        nil
      end

      def personal(backend)
        return unless context.user.account.subscription.has_personal_rules?

        send("personal_#{backend}")
      end

      def personal_arel
        nil
      end

      def personal_es
        nil
      end
    end
  end
end
