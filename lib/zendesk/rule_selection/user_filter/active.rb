module Zendesk::RuleSelection
  module UserFilter
    class Active < Abstract
      def self.valid?(*)
        true
      end

      private

      def account_arel
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(context.user.account_id)).
          and(context.table[:is_active].eq(true))
      end

      def account_es
        'is_active:true'
      end
    end
  end
end
