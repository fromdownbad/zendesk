module Zendesk::RuleSelection
  module UserFilter
    class Shared < Abstract
      def self.valid?(context)
        context.user.can?(:manage_shared, context.type)
      end

      private

      def account_arel
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(context.user.account_id))
      end

      def group_arel
        context.table[:owner_type].eq('Group')
      end
    end
  end
end
