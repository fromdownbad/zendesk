module Zendesk::RuleSelection
  module UserFilter
    class Global < Abstract
      def self.valid?(context)
        context.user.can?(:manage_shared, context.type)
      end

      private

      def account_arel
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(context.user.account_id))
      end

      def account_es
        'owner_type:Account'
      end

      def group_arel
        context.table[:owner_type].eq('Group')
      end

      def group_es
        'owner_type:Group'
      end

      def personal_arel
        context.table[:owner_type].eq('User').
          and(context.table[:owner_id].eq(context.user.id))
      end

      def personal_es
        "(owner_type:User AND owner_id:#{context.user.id})"
      end
    end
  end
end
