module Zendesk::RuleSelection
  module UserFilter
    class Group < Abstract
      def self.valid?(context)
        context.user.can?(:manage_group, context.type)
      end

      private

      def account_arel
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(context.user.account_id)).
          and(context.table[:is_active].eq(true))
      end

      def account_es
        '(owner_type:Account AND is_active:true)'
      end

      def group_arel
        return nil unless group_ids.present?

        if context.modern_groups?
          if context.user.account.has_multiple_group_views_reads?
            base_group_arel
          else
            base_group_arel.and(context.table[:owner_id].in([*group_ids, Rule.group_owner_id]))
          end
        else
          context.table[:owner_type].eq('Group').
            and(context.table[:owner_id].in(group_ids))
        end
      end

      def base_group_arel
        context.table[:id].eq(context.group_table[context.foreign_key]).
          and(context.group_table[:group_id].in(group_ids)).
          and(context.table[:owner_type].eq('Group'))
      end

      def group_es
        return nil unless group_ids.present?

        "(owner_type:Group AND #{owner_field}:(#{group_ids.join(OR_JOIN)}))"
      end

      def personal_arel
        context.table[:owner_type].eq('User').
          and(context.table[:owner_id].eq(context.user.id))
      end

      def personal_es
        "(owner_type:User AND owner_id:#{context.user.id})"
      end

      def owner_field
        context.modern_groups? ? 'group_ids' : 'owner_id'
      end

      def group_ids
        context.user.group_ids
      end
    end
  end
end
