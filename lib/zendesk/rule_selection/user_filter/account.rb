module Zendesk::RuleSelection
  module UserFilter
    class Account < Abstract
      def self.valid?(context)
        context.user.can?(:edit, context.type)
      end

      private

      def account_arel
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(context.user.account_id))
      end
    end
  end
end
