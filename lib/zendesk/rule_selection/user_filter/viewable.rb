module Zendesk::RuleSelection
  module UserFilter
    class Viewable < Abstract
      private

      def account_arel
        context.table[:owner_type].eq('Account').
          and(context.table[:owner_id].eq(context.user.account_id)).
          and(context.table[:is_active].eq(true))
      end

      def group_arel
        if context.modern_groups?
          if context.user.account.has_multiple_group_views_reads?
            base_group_arel
          else
            base_group_arel.and(
              context.table[:owner_id].in(
                [*context.user.group_ids, Rule.group_owner_id]
              )
            )
          end
        else
          context.table[:owner_type].eq('Group').
            and(context.table[:owner_id].in(context.user.group_ids)).
            and(context.table[:is_active].eq(true))
        end
      end

      def base_group_arel
        context.table[:id].eq(context.group_table[context.foreign_key]).
          and(context.group_table[:group_id].in(context.user.group_ids)).
          and(context.table[:owner_type].eq('Group')).
          and(context.table[:is_active].eq(true))
      end

      def personal_arel
        context.table[:owner_type].eq('User').
          and(context.table[:owner_id].eq(context.user.id)).
          and(context.table[:is_active].eq(true))
      end
    end
  end
end
