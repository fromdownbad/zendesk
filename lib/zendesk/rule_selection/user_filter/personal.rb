module Zendesk::RuleSelection
  module UserFilter
    class Personal < Abstract
      def self.valid?(context)
        context.user.can?(:manage_personal, context.type)
      end

      private

      def personal_arel
        context.table[:owner_type].eq('User').
          and(context.table[:owner_id].eq(context.user.id))
      end
    end
  end
end
