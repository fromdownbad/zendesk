module Zendesk::RuleSelection
  module OptionFilter
    def self.for_context(context)
      all(context).
        select { |filter_scope| filter_scope.valid?(context) }.
        map    { |filter_scope| filter_scope.new(context) }
    end

    def self.all(context)
      context.
        option_filters.
        map { |filter| "#{name}::#{filter.to_s.camelize}".constantize }
    end

    private_class_method :all
  end
end

require 'zendesk/rule_selection/option_filter/access'
require 'zendesk/rule_selection/option_filter/active'
require 'zendesk/rule_selection/option_filter/category'
require 'zendesk/rule_selection/option_filter/group'
require 'zendesk/rule_selection/option_filter/rules_category_id'
