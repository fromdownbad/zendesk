module Zendesk::RuleSelection
  class Filter
    def self.for_context(context)
      new(
        context,
        user_filter:    UserFilter.for_context(context),
        option_filters: OptionFilter.for_context(context)
      )
    end

    def initialize(context, user_filter:, option_filters: [])
      @context        = context
      @user_filter    = user_filter
      @option_filters = option_filters
    end

    def to_arel
      all_filters.map(&:to_arel).compact.reduce(:and)
    end

    def to_elasticsearch
      all_filters.map(&:to_elasticsearch).compact.join(AND_JOIN)
    end

    protected

    attr_reader :context,
      :option_filters,
      :user_filter

    private

    def all_filters
      [user_filter, *option_filters]
    end
  end
end
