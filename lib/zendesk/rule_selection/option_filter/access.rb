module Zendesk::RuleSelection
  module OptionFilter
    class Access
      def self.valid?(context)
        context.options.access?
      end

      def initialize(context)
        @context = context
      end

      def to_arel
        context.table[:owner_type].in(access_types)
      end

      def to_elasticsearch
        "(owner_type:(#{access_types.join(OR_JOIN)}))"
      end

      protected

      attr_reader :context

      private

      def access_types
        Array(context.options.access)
      end
    end
  end
end
