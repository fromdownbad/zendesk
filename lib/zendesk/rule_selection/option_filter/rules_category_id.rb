module Zendesk::RuleSelection
  module OptionFilter
    class RulesCategoryId
      def self.valid?(context)
        context.options.rules_category_id?
      end

      def initialize(context)
        @context = context
      end

      def to_arel
        context.table[:rules_category_id].eq(context.options.rules_category_id)
      end

      # TODO: talk to Torch to get correct ES query
      def to_elasticsearch
        ""
      end

      protected

      attr_reader :context
    end
  end
end
