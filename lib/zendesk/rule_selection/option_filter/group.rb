module Zendesk::RuleSelection
  module OptionFilter
    class Group
      def self.valid?(context)
        context.options.group?
      end

      def initialize(context)
        @context = context
      end

      def to_arel
        if context.modern_groups?
          if context.user.account.has_multiple_group_views_reads?
            base_arel
          else
            base_arel.and(
                context.table[:owner_id].in(
                  [context.options.group_id, Rule.group_owner_id]
                )
              )
          end
        else
          context.table[:owner_type].eq('Group').
            and(context.table[:owner_id].eq(context.options.group_id))
        end
      end

      def to_elasticsearch
        "(owner_type:Group AND #{owner_field}:#{context.options.group_id})"
      end

      protected

      attr_reader :context

      private

      def owner_field
        context.modern_groups? ? 'group_ids' : 'owner_id'
      end

      def base_arel
        context.table[:id].eq(context.group_table[context.foreign_key]).
          and(context.group_table[:group_id].eq(context.options.group_id)).
          and(context.table[:owner_type].eq('Group'))
      end
    end
  end
end
