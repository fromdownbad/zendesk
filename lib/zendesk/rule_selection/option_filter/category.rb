module Zendesk::RuleSelection
  module OptionFilter
    class Category
      def self.valid?(context)
        context.options.category?
      end

      def initialize(context)
        @context = context
      end

      def to_arel
        context.table[:title].matches("#{context.options.category}::%").
          or(context.table[:title].eq(context.options.category))
      end

      def to_elasticsearch
        "category:\"#{context.options.category}\""
      end

      protected

      attr_reader :context
    end
  end
end
