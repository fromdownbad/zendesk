module Zendesk::RuleSelection
  module OptionFilter
    class Active
      def self.valid?(context)
        context.options.active?
      end

      def initialize(context)
        @context = context
      end

      def to_arel
        context.table[:is_active].eq(context.options.active)
      end

      def to_elasticsearch
        "is_active:#{context.options.active}"
      end

      protected

      attr_reader :context
    end
  end
end
