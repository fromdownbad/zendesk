module Zendesk::RuleSelection
  module UserFilter
    EDIT = %i[account active].freeze
    ROLE = %i[global group limited].freeze

    private_constant :EDIT,
      :ROLE

    def self.edit
      EDIT.map(&method(:to_class))
    end

    def self.role
      ROLE.map(&method(:to_class))
    end

    def self.for_context(context)
      context.
        user_filters.
        find { |filter| filter.valid?(context) }.
        new(context)
    end

    def self.for_reorder(context)
      context.
        reorder_filters.
        map(&method(:to_class)).
        select { |filter| filter.valid?(context) }.
        map    { |filter| filter.new(context) }
    end

    def self.to_class(filter)
      "#{name}::#{filter.to_s.camelize}".constantize
    end

    private_class_method :to_class
  end
end

require 'zendesk/rule_selection/user_filter/abstract'
require 'zendesk/rule_selection/user_filter/account'
require 'zendesk/rule_selection/user_filter/active'
require 'zendesk/rule_selection/user_filter/global'
require 'zendesk/rule_selection/user_filter/group'
require 'zendesk/rule_selection/user_filter/limited'
require 'zendesk/rule_selection/user_filter/personal'
require 'zendesk/rule_selection/user_filter/shared'
require 'zendesk/rule_selection/user_filter/viewable'
