module Zendesk::RuleSelection
  module Sort
    ORDER = %w[asc desc].freeze

    private_constant :ORDER

    def self.order
      ORDER.to_enum
    end

    def self.for_context(context)
      valid_sort(context, context.sorts)
    end

    def self.for_search(context)
      valid_sort(context, context.search_sorts)
    end

    def self.valid_sort(context, sorts)
      sorts.
        map  { |sort| "#{name}::#{sort.to_s.camelize}".constantize }.
        find { |sort| sort.valid?(context) }.
        new(context)
    end

    private_class_method :valid_sort
  end
end

require 'zendesk/rule_selection/sort/abstract'
require 'zendesk/rule_selection/sort/alphabetical'
require 'zendesk/rule_selection/sort/default'
require 'zendesk/rule_selection/sort/owner_position'
require 'zendesk/rule_selection/sort/position'
require 'zendesk/rule_selection/sort/relevance'
require 'zendesk/rule_selection/sort/timestamp'
require 'zendesk/rule_selection/sort/usage'
