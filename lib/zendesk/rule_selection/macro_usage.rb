module Zendesk::RuleSelection
  class MacroUsage
    LEGACY_MACRO_USAGE_KEY = 'rule:macro:usage:for-user:%{user_id}:' \
      'last-7-days:%{date}'.freeze

    MACRO_USAGE_KEY = 'rule:macro:usage:for-account:%{account_id}' \
      ':for-user:%{user_id}:last-7-days:%{date}'.freeze

    HISTORY_DAYS = (0..7).freeze

    def initialize(user)
      @user    = user
      @account = user.account
    end

    def most_used(count)
      key =
        if account.has_rule_usage_read_from_rules_cluster?
          macro_usage_key(today)
        else
          legacy_macro_usage_key(today)
        end

      redis.zrevrange(key, 0, count.pred).map(&:to_i)
    end

    def record(macro_id)
      redis.pipelined { record_usage(macro_id) }
    end

    def record_many(macro_ids)
      redis.pipelined { macro_ids.each(&method(:record_usage)) }
    end

    protected

    attr_reader :today,
      :user,
      :account

    private

    def record_usage(macro_id)
      record_dates.each do |date|
        redis.zincrby(legacy_macro_usage_key(date), 1, macro_id)
        redis.expireat(
          legacy_macro_usage_key(date),
          date.next_day.to_time.utc.to_i
        )

        redis.zincrby(macro_usage_key(date), 1, macro_id)
        redis.expireat(macro_usage_key(date), date.next_day.to_time.utc.to_i)
      end
    end

    def legacy_macro_usage_key(date)
      format(LEGACY_MACRO_USAGE_KEY, user_id: user.id, date: date)
    end

    def macro_usage_key(date)
      format(
        MACRO_USAGE_KEY,
        user_id: user.id,
        account_id: user.account.id,
        date: date
      )
    end

    def record_dates
      HISTORY_DAYS.map { |day| today + day }
    end

    def redis
      Zendesk::RedisStore.client
    end

    def today # rubocop:disable Lint/DuplicateMethods
      Time.now.utc.to_date
    end
  end
end
