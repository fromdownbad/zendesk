module Zendesk::RuleSelection
  module Context
    ACCESS_MAPPING = {
      'account'  => 'Account',
      'personal' => 'User',
      'shared'   => %w[Account Group]
    }.freeze

    private_constant :ACCESS_MAPPING

    def self.access_mapping
      ACCESS_MAPPING
    end
  end
end

require 'zendesk/rule_selection/context/automation'
require 'zendesk/rule_selection/context/macro'
require 'zendesk/rule_selection/context/trigger'
require 'zendesk/rule_selection/context/view'
