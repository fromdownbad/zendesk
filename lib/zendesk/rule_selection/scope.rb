module Zendesk::RuleSelection
  SORT_METHODS = %i[sorted unsorted].freeze

  class Scope
    def self.for_context(context)
      new(context, Filter.for_context(context)).sorted
    end

    def self.for_search(context)
      new(context, Filter.for_context(context)).unsorted
    end

    def self.for_reorder(context, ordering: :sorted)
      raise 'Invalid sorting option provided' unless SORT_METHODS.include?(ordering)

      UserFilter.for_reorder(context).map do |filter|
        new(context, Filter.new(context, user_filter: filter)).public_send(ordering)
      end
    end

    def self.viewable(context)
      new(
        context,
        Filter.new(
          context,
          user_filter:    UserFilter::Viewable.new(context),
          option_filters: OptionFilter.for_context(context)
        )
      ).sorted
    end

    def initialize(context, filter)
      @context = context
      @filter  = filter
    end

    def sorted
      Zendesk::RuleSelection::Sort.for_context(context).order(unsorted)
    end

    def unsorted
      context.rules.where(filter.to_arel)
    end

    protected

    attr_reader :context,
      :filter
  end
end
