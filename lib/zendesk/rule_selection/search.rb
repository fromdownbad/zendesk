module Zendesk::RuleSelection
  class Search
    def self.for_context(context)
      new(context, Filter.for_context(context)).result
    end

    def initialize(context, filter)
      @context = context
      @filter  = filter
    end

    def result
      Result.new(
        paginate(
          Zendesk::RuleSelection::Scope.for_search(context).with_ordered_ids(
            response['results'].map { |result| result['id'] }
          ),
          response['count']
        ),
        response['results'].each_with_object({}) do |result, highlights|
          highlights.store(result['id'], result['_highlights'])
        end
      )
    end

    protected

    attr_reader :context,
      :filter

    private

    def response
      @response ||= begin
        client.search(
          Zendesk::RuleSelection::Sort.
            for_search(context).
            to_elasticsearch(context.options.query),
          account_id:  context.user.account_id,
          endpoint:    'rule',
          from:        context.options.offset,
          fq:          filter.to_elasticsearch,
          hl:          true,
          incremental: true,
          size:        context.options.per_page,
          type:        context.type.to_s.downcase
        )
      end
    end

    def paginate(results, count)
      WillPaginate::Collection.create(
        context.options.page,
        context.options.per_page,
        count
      ) do |pager|
        pager.replace(results)
      end
    end

    def client
      ZendeskSearch::Client.new(
        context.user.account_id,
        context.user.account.has_search_green?,
        context.user.account.has_ss_enable_search_service_hub?
      )
    end

    Result = Struct.new(:rules, :highlights)

    private_constant :Result
  end
end

require 'zendesk/rule_selection/search/triggers'
