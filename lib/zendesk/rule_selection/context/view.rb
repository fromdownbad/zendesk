module Zendesk::RuleSelection
  module Context
    class View
      OPTION_FILTERS = %i[access active group].freeze

      REORDER_FILTERS = %i[personal shared].freeze

      SORTS = %i[
        user_setting
        alphabetical
        owner_position
        timestamp
        default
      ].freeze

      SEARCH_SORTS = %i[alphabetical owner_position timestamp relevance].freeze

      private_constant :OPTION_FILTERS,
        :REORDER_FILTERS,
        :SORTS,
        :SEARCH_SORTS

      def initialize(user, options = {})
        @user    = user
        @options = Context::Options.new(options)
      end

      attr_reader :options,
        :user

      def default_sort
        :owner_position
      end

      def foreign_key
        :view_id
      end

      def group_table
        GroupView.arel_table
      end

      def modern_groups?
        user.account.has_multiple_group_views_reads?
      end

      def option_filters
        OPTION_FILTERS.to_enum
      end

      def reorder_filters
        REORDER_FILTERS.to_enum
      end

      def rules
        return user.account.all_views unless modern_groups?

        user.
          account.
          all_views.
          includes(:groups_views).
          references(:groups_views)
      end

      def sorts
        SORTS.to_enum
      end

      def search_sorts
        SEARCH_SORTS.to_enum
      end

      def table
        ::View.arel_table
      end

      def type
        ::View
      end

      def user_filters
        UserFilter.role
      end

      def user_setting_order
        @user_setting_order ||= user.shared_views_order
      end

      def user_setting_sort?
        user.account.has_user_level_shared_view_ordering?
      end
    end
  end
end
