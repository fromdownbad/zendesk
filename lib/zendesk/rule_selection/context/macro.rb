module Zendesk::RuleSelection
  module Context
    class Macro
      OPTION_FILTERS = %i[access active category group].freeze

      REORDER_FILTERS = %i[personal shared].freeze

      SORTS = %i[alphabetical owner_position timestamp usage default].freeze

      SEARCH_SORTS = %i[alphabetical owner_position timestamp relevance].freeze

      private_constant :OPTION_FILTERS,
        :REORDER_FILTERS,
        :SORTS,
        :SEARCH_SORTS

      def initialize(user, options = {})
        @user    = user
        @options = Context::Options.new(options)
      end

      attr_reader :options,
        :user

      def default_sort
        alphabetical_sort? ? :alphabetical : :owner_position
      end

      def foreign_key
        :macro_id
      end

      def group_table
        GroupMacro.arel_table
      end

      def modern_groups?
        true
      end

      def option_filters
        OPTION_FILTERS.to_enum
      end

      def reorder_filters
        REORDER_FILTERS.to_enum
      end

      def rules
        user.account.all_macros.includes(:groups_macros).
          references(:groups_macros)
      end

      def sorts
        SORTS.to_enum
      end

      def search_sorts
        SEARCH_SORTS.to_enum
      end

      def table
        ::Macro.arel_table
      end

      def type
        ::Macro
      end

      def user_filters
        UserFilter.role
      end

      def user_setting_sort?
        false
      end

      private

      def alphabetical_sort?
        user.account.settings.macro_order == 'alphabetical'
      end
    end
  end
end
