module Zendesk::RuleSelection
  module Context
    class Options
      def initialize(options = {})
        @options = options
      end

      def access
        Context.access_mapping[options[:access].to_s.downcase]
      end

      def access?
        access.present?
      end

      def active
        options[:active]
      end

      def active?
        !active.nil?
      end

      def category
        options[:category]
      end

      def category?
        !category.nil?
      end

      def group_id
        options[:group_id]
      end

      def group?
        group_id.present?
      end

      def offset
        options[:per_page] * (options[:page] - 1)
      end

      def page
        options[:page]
      end

      def per_page
        options[:per_page]
      end

      def query
        options[:query]
      end

      def sort_by
        options[:sort_by]
      end

      def rules_category_id
        options[:category_id]
      end

      def rules_category_id?
        rules_category_id.present?
      end

      def categories_enabled?
        options[:trigger_categories_api]
      end

      def filter
        return nil unless options[:filter]
        @filter ||= begin
          transformed_filter = JSON.parse(CGI.unescape(options[:filter]))
          transformed_filter = transformed_filter['json'] if transformed_filter.key? 'json'
          ['conditions', 'actions'].each do |item_type|
            transformed_filter[item_type]&.each do |item|
              transformed_item = DefinitionItem.build(item.with_indifferent_access)
              item['field'] = transformed_item.source
              item['operator'] = transformed_item.operator if item_type != 'action' && transformed_item.operator

              if item.key?('value')
                item['value'] = transformed_item.value.first if transformed_item.value
                item['value'] = '__NULL__' if item['value'].blank?
              end
            end
          end

          transformed_filter
        end
      end

      def sort_by?(sort)
        sort_by == sort.to_s
      end

      def sort_order
        Sort.order.find { |order| order == options[:sort_order].try(:downcase) }
      end

      protected

      attr_reader :options
    end
  end
end
