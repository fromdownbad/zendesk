module Zendesk::RuleSelection
  module Context
    class Automation
      OPTION_FILTERS = %i[active].freeze

      REORDER_FILTERS = %i[account].freeze

      SORTS = %i[alphabetical position timestamp usage default].freeze

      SEARCH_SORTS = %i[alphabetical position timestamp relevance].freeze

      private_constant :OPTION_FILTERS,
        :REORDER_FILTERS,
        :SORTS,
        :SEARCH_SORTS

      def initialize(user, options = {})
        @user    = user
        @options = Context::Options.new(options)
      end

      attr_reader :options,
        :user

      def default_sort
        :position
      end

      def option_filters
        OPTION_FILTERS.to_enum
      end

      def reorder_filters
        REORDER_FILTERS.to_enum
      end

      def rules
        user.account.all_automations
      end

      def sorts
        SORTS.to_enum
      end

      def search_sorts
        SEARCH_SORTS.to_enum
      end

      def table
        ::Automation.arel_table
      end

      def type
        ::Automation
      end

      def user_filters
        UserFilter.edit
      end

      def user_setting_sort?
        false
      end
    end
  end
end
