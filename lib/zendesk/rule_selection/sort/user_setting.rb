module Zendesk::RuleSelection
  module Sort
    class UserSetting < Abstract
      def self.valid?(context)
        context.options.sort_by?(:user_setting) &&
          context.user_setting_sort? &&
          context.user_setting_order.any?
      end

      private

      def elasticsearch_ordering
        fail 'this sort is not supported for search'
      end

      def new_elasticsearch_ordering
        fail 'this sort is not supported for search'
      end

      def ordering
        context.type.send(
          :sanitize_sql_array,
          [
            'FIELD(rules.id, ?) DESC, position ASC, title ASC',
            context.user_setting_order.reverse
          ]
        )
      end
    end
  end
end
