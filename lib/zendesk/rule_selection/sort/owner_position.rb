module Zendesk::RuleSelection
  module Sort
    class OwnerPosition < Abstract
      def self.valid?(context)
        context.options.sort_by?(:position)
      end

      def new_endpoint_order_param
        {sort: "#{'-' if sort_order == 'desc'}owner_filter,#{'-' if sort_order == 'desc'}position"}
      end

      private

      def default_order
        'asc'
      end

      def elasticsearch_ordering
        "order_by:owner_filter sort:#{sort_order} " \
          "order_by:position sort:#{sort_order}"
      end

      def new_elasticsearch_ordering
        "sort:#{'-' if sort_order == 'desc'}owner_filter,#{'-' if sort_order == 'desc'}position"
      end

      def ordering
        "IF(rules.owner_type = 'User', 1, 0) #{sort_order}, " \
          "position #{sort_order}, title #{sort_order}"
      end
    end
  end
end
