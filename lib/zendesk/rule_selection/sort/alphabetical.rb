module Zendesk::RuleSelection
  module Sort
    class Alphabetical < Abstract
      def self.valid?(context)
        context.options.sort_by?(:alphabetical)
      end

      def new_endpoint_order_param
        {sort: "#{'-' if sort_order == 'desc'}title"}
      end

      private

      def default_order
        'asc'
      end

      def elasticsearch_ordering
        "order_by:title sort:#{sort_order}"
      end

      def new_elasticsearch_ordering
        "sort:#{'-' if sort_order == 'desc'}title"
      end

      def ordering
        "title #{sort_order}"
      end
    end
  end
end
