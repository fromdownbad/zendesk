module Zendesk::RuleSelection
  module Sort
    class Abstract
      def initialize(context)
        @context = context
      end

      def order(scope)
        scope.order(ordering)
      end

      def to_elasticsearch(query)
        if context.is_a?(Zendesk::RuleSelection::Context::Trigger) &&
          Arturo.feature_enabled_for?(:new_trigger_search, context.user.account) &&
          context.options.filter

          [query, new_elasticsearch_ordering].compact.join(' ')
        else
          [query, elasticsearch_ordering].compact.join(' ')
        end
      end

      def new_endpoint_order_param
        {}
      end

      protected

      attr_reader :context

      private

      def default_order
        fail 'must define in subclass'
      end

      def elasticsearch_ordering
        fail 'must define in subclass'
      end

      def new_elasticsearch_ordering
        fail 'must define in subclass'
      end

      def ordering
        fail 'must define in subclass'
      end

      def sort_order
        context.options.sort_order || default_order
      end

      def sort_order?(order)
        sort_order == order.to_s
      end
    end
  end
end
