module Zendesk::RuleSelection
  module Sort
    class Position < Abstract
      def self.valid?(context)
        context.options.sort_by?(:position)
      end

      def order(scope)
        if use_categories?
          scope.joins(:rule_category).order(ordering)
        else
          super
        end
      end

      def new_endpoint_order_param
        if use_categories?
          {sort: "#{sort_symbol}category_position,#{sort_symbol}position,#{sort_symbol}title"}
        else
          {sort: "#{sort_symbol}position,#{sort_symbol}title"}
        end
      end

      private

      def sort_symbol
        sort_order == 'desc' ? '-' : ''
      end

      def default_order
        'asc'
      end

      def elasticsearch_ordering
        if use_categories?
          "order_by:category_position,position,title sort:#{sort_order}"
        else
          "order_by:position,title sort:#{sort_order}"
        end
      end

      def new_elasticsearch_ordering
        if use_categories?
          "sort:#{sort_symbol}category_position,#{sort_symbol}position,#{sort_symbol}title"
        else
          "sort:#{sort_symbol}position,#{sort_symbol}title"
        end
      end

      def ordering
        if use_categories?
          rules_categories_position = "rules_categories.position #{sort_order}"
          rules_position = "rules.position #{sort_order}"
          title = "rules.title #{sort_order}"

          "#{rules_categories_position}, #{rules_position}, #{title}"
        else
          "position #{sort_order}, title #{sort_order}"
        end
      end

      def use_categories?
        account = context.user.account

        context.type == ::Trigger &&
          account.has_trigger_categories_api_enabled? &&
          account.trigger_categories.any?
      end
    end
  end
end
