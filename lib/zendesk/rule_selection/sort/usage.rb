module Zendesk::RuleSelection
  module Sort
    class Usage < Abstract
      MAPPING = {
        'usage_1h'  => :hourly,
        'usage_24h' => :daily,
        'usage_7d'  => :weekly,
        'usage_30d' => :monthly
      }.freeze

      private_constant :MAPPING

      def self.valid?(context)
        MAPPING.keys.any? { |usage| context.options.sort_by?(usage) }
      end

      def order(scope)
        rules(scope).
          sort_by { |rule| rule.usage.public_send(timeframe) }.
          tap do |rules|
            rules.reverse! if sort_order?(:desc)
          end
      end

      private

      def rules(scope)
        Zendesk::RuleSelection::ExecutionCounter.new(
          account: context.user.account,
          type:    context.type.to_s.downcase.to_sym
        ).rules_with_execution_counts(timeframe, scope)
      end

      def timeframe
        MAPPING[context.options.sort_by]
      end

      def default_order
        'desc'
      end
    end
  end
end
