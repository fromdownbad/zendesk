module Zendesk::RuleSelection
  module Sort
    class Timestamp < Abstract
      COLUMNS = %i[created_at updated_at].freeze

      private_constant :COLUMNS

      def self.valid?(context)
        COLUMNS.any? { |column| context.options.sort_by?(column) }
      end

      def new_endpoint_order_param
        {sort: "#{'-' if sort_order == 'desc'}#{context.options.sort_by}"}
      end

      private

      def default_order
        'desc'
      end

      def elasticsearch_ordering
        "order_by:#{context.options.sort_by} sort:#{sort_order}"
      end

      def new_elasticsearch_ordering
        "sort:#{'-' if sort_order == 'desc'}#{context.options.sort_by}"
      end

      def ordering
        "rules.#{context.options.sort_by} #{sort_order}"
      end
    end
  end
end
