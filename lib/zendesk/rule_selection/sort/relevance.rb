module Zendesk::RuleSelection
  module Sort
    class Relevance < Abstract
      def self.valid?(*)
        true
      end

      private

      def elasticsearch_ordering
        nil
      end

      def new_elasticsearch_ordering
        nil
      end
    end
  end
end
