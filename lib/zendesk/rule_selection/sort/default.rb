module Zendesk::RuleSelection
  module Sort
    class Default < SimpleDelegator
      def self.valid?(*)
        true
      end

      def initialize(context)
        super(default_sort(context))
      end

      private

      def default_sort(context)
        "#{Sort.name}::#{context.default_sort.to_s.camelize}".
          constantize.
          new(context)
      end
    end
  end
end
