module Zendesk::RuleSelection
  class ExecutionCounter
    def initialize(account:, type:)
      @account = account
      @type    = type
    end

    def all_execution_counts(timeframe)
      rule_usage.all_execution_counts(timeframe: timeframe, type: type)
    end

    def execution_count(rule, timeframe)
      rule_usage.execution_count(
        rule_id:   rule.id,
        timeframe: timeframe,
        type:      type
      )
    end

    def rules_with_execution_counts(timeframe, scope)
      counts = execution_counts(timeframe, scope)

      scope.to_a.each do |rule|
        rule.usage.public_send("#{timeframe}=", counts[rule.id].value.to_i)
      end
    end

    protected

    attr_reader :account,
      :type

    private

    def execution_counts(timeframe, scope = [])
      rule_usage.execution_counts(
        rule_ids:  scope.to_a.map(&:id),
        timeframe: timeframe,
        type:      type
      )
    end

    def rule_usage
      @rule_usage ||= RuleUsage.new(account)
    end
  end
end
