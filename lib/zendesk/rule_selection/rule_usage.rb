module Zendesk::RuleSelection
  class RuleUsage
    BASE_KEY = 'rule:%{type}:usage:for-account:%{account}:%{timeframe}'.freeze

    MONTHLY_KEY = ':%s'.freeze
    WEEKLY_KEY  = ':%s'.freeze
    DAILY_KEY   = ':%s:%0.2d'.freeze
    HOURLY_KEY  = ':%s:%0.2d:%0.2d'.freeze

    EPOCH = Date.new(2016)

    MONTHLY_STEP = 3
    DAILY_STEP   = 4
    HOURLY_STEP  = 15

    def initialize(account)
      @account = account
    end

    def all_execution_counts(timeframe:, type:)
      redis.zrevrangebyscore(
        send("#{timeframe}_key", current_time, type),
        '+inf',
        0,
        with_scores: true
      ).each_with_object({}) do |execution, counts|
        counts[execution.first.to_i] = execution.last.to_i
      end
    end

    def execution_counts(rule_ids:, type:, timeframe:)
      {}.tap do |hash|
        redis.pipelined do
          rule_ids.each_with_object(hash) do |rule_id, counts|
            counts[rule_id] = execution_count(
              rule_id:   rule_id,
              timeframe: timeframe,
              type:      type
            )
          end
        end
      end
    end

    def execution_count(rule_id:, timeframe:, type:)
      redis.zscore(send("#{timeframe}_key", current_time, type), rule_id) || 0
    end

    def most_used(timeframe:, type:, number: 5)
      redis.zrevrange(
        send("#{timeframe}_key", current_time, type),
        0,
        number.pred,
        with_scores: true
      ).map { |item| {id: item[0].to_i, count: item[1].to_i} }
    end

    def record(rule_id:, type:)
      redis.pipelined do
        all_records(type).each do |record|
          record_usage(record.key, rule_id)

          set_expiration(record.key, record.expire_at)
        end
      end
    end

    def record_many(rule_ids:, type:)
      redis.pipelined do
        all_records(type).each do |record|
          rule_ids.each { |rule_id| record_usage(record.key, rule_id) }

          set_expiration(record.key, record.expire_at)
        end
      end
    end

    protected

    attr_reader :account

    private

    def record_usage(key, rule_id)
      redis.zincrby(key, 1, rule_id)
    end

    def set_expiration(key, expire_at)
      redis.expireat(key, expire_at.to_i)
    end

    def all_records(type)
      [
        *monthly_records(type),
        *weekly_records(type),
        *daily_records(type),
        *hourly_records(type)
      ]
    end

    def monthly_records(type)
      (0..30).step(MONTHLY_STEP).map do |day|
        Record.new(
          monthly_key(current_day + day.days, type),
          (current_day + (day + MONTHLY_STEP).days).utc.to_i
        )
      end
    end

    def monthly_key(time, type)
      epoch_days   = time.to_date - EPOCH
      nearest_step = EPOCH + epoch_days - epoch_days % MONTHLY_STEP

      "#{base_key(:monthly, type)}#{format(MONTHLY_KEY, nearest_step)}"
    end

    def weekly_records(type)
      (0...7).map do |day|
        Record.new(
          weekly_key(current_day + day.days, type),
          (current_day + (day + 1).days).utc.to_i
        )
      end
    end

    def weekly_key(time, type)
      "#{base_key(:weekly, type)}#{format(WEEKLY_KEY, time.to_date)}"
    end

    def daily_records(type)
      (0..24).step(DAILY_STEP).map do |hour|
        Record.new(
          daily_key(current_hour + hour.hours, type),
          (current_hour + (hour + DAILY_STEP).hours).utc.to_i
        )
      end
    end

    def daily_key(time, type)
      nearest_step = Time.new(
        time.year,
        time.month,
        time.mday,
        time.hour - time.hour % DAILY_STEP
      )

      "#{base_key(:daily, type)}" \
        "#{format(DAILY_KEY, nearest_step.to_date, nearest_step.hour)}"
    end

    def hourly_records(type)
      (0..60).step(HOURLY_STEP).map do |minute|
        Record.new(
          hourly_key(current_time + minute.minutes, type),
          (current_time + (minute + HOURLY_STEP).minutes).utc.to_i
        )
      end
    end

    def hourly_key(time, type)
      nearest_step = Time.new(
        time.year,
        time.month,
        time.mday,
        time.hour,
        time.min - time.min % HOURLY_STEP
      )

      "#{base_key(:hourly, type)}" \
        "#{format(
          HOURLY_KEY,
          nearest_step.to_date,
          nearest_step.hour,
          nearest_step.min
        )}"
    end

    def base_key(timeframe, type)
      format(BASE_KEY, account: account.id, timeframe: timeframe, type: type)
    end

    def current_time
      @current_time ||= Time.now.utc
    end

    def current_day
      @current_day ||= current_time.beginning_of_day
    end

    def current_hour
      @current_hour ||= current_time.beginning_of_hour
    end

    def redis
      Zendesk::RedisStore.client
    end

    Record = Struct.new(:key, :expire_at)

    private_constant :Record
  end
end
