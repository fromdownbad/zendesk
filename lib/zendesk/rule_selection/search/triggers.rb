class Zendesk::RuleSelection::Search::Triggers < Zendesk::RuleSelection::Search
  private

  def response
    @response ||= begin
      order_param = Zendesk::RuleSelection::Sort.
        for_search(context).new_endpoint_order_param
      client.search(
        '',
        {
          account_id:  context.user.account_id,
          endpoint:    'trigger',
          filter:      { json: structured_filter(context.options.filter) },
          from:        context.options.offset,
          hl:          true,
          incremental: true,
          size:        context.options.per_page
        }.merge(order_param)
      )
    end
  end

  def structured_filter(query_filter)
    query_filter["is_active"] = context.options.active.to_s unless context.options.active.nil?
    structured_filter_arr = []
    query_filter.each do |k, v|
      if ['actions', 'conditions'].include? k
        query_filter[k].each do |h|
          structured_filter_arr << { k => { '$eq' => structured_value(h.with_indifferent_access) } }
        end
      else
        structured_filter_arr << { k => { '$eq' => v } }
      end
    end
    JSON.generate(filter: { '$and' => structured_filter_arr })
  end

  def structured_value(value_hash)
    value_as_a_string = value_hash[:field]
    value_as_a_string += " #{value_hash[:operator]}" if value_hash[:operator]
    value_as_a_string += " #{value_hash[:value]}" if value_hash[:value]
    value_as_a_string
  end
end
