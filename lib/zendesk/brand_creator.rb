module Zendesk
  class BrandCreator
    def initialize(account, route = nil)
      if account.has_brand_creator_read_from_master? && account.multiproduct?
        read_account_from_master(account.id, route)
      else
        @account = account
        @route = route || account.routes.build
      end
    end

    def create!
      Brand.transaction do
        create_brand!
        update_account!
      end
    end

    def brand
      @brand ||= begin
        brand = account.brands.build
        brand.route = route

        brand
      end
    end

    private

    attr_reader :account, :route

    def read_account_from_master(id, route)
      # the brand creator might run before the account is replicated to the slave
      Account.on_master do
        @account = find_account(id)
        # route will be nil when account has no route, this can happen when db replication is not finished
        # in that case we read route from account from master db
        @route = route || @account.route || @account.routes.build
      end
    end

    def find_account(id)
      Account.find(id)
    end

    def create_brand!
      brand.attributes = default_brand_options
      if account.route_id
        brand.save!
      else
        route.save!
      end
    end

    def default_brand_options
      {
        name: account.name,
        subdomain: account.subdomain,
        host_mapping: account.host_mapping,
        signature_template: account.default_brand.try(:signature_template) || Brand::DEFAULT_SIGNATURE_TEMPLATE
      }.with_indifferent_access
    end

    def update_account!
      unless account.route_id
        account.route = route
        account.update_column(:route_id, route.id)
      end
      account.default_brand = brand
      account.settings.save!
    end
  end
end
