module Zendesk::Facebook
  def self.config
    Zendesk::Facebook::Config
  end

  def self.configure
    yield config
  end

  def self.logfile
    Zendesk::Facebook.config.logfile
  end

  def self.open_logfile
    @logfile ||= File.open(logfile, 'a')
  end

  def self.logger
    @logger ||= Zendesk::Facebook::Logger.new(open_logfile)
  end
end
