module Zendesk
  class RateControl
    attr_reader :rate, :time_elapsed, :last_measured

    def initialize(rate_per_sec:, rate_refresh_interval: nil)
      @rate = rate_per_sec.to_f

      @last_measured = nil
      @time_elapsed = nil
      @rate_control_proc = nil
      @interval = rate_refresh_interval.to_f if rate_refresh_interval
      @periodically = Zendesk::Periodical.new(rate_refresh_interval.to_f) if rate_refresh_interval
    end

    def define_rate_control(&block)
      @rate_control_proc = block.to_proc
    end

    def time_allowed
      1 / rate
    end

    def first_time?
      !last_measured
    end

    def with_rate_control
      measure_time if first_time?

      yield
      apply_speed_control
    end

    private

    def apply_speed_control
      @time_elapsed = Time.now - last_measured

      if time_elapsed < time_allowed
        sleep_time = time_allowed - time_elapsed
        sleep(sleep_time) if sleep_time > 0
      end

      periodically { recalculate_rate }
      measure_time # after we sleep, and we recalculate rate
    end

    def recalculate_rate
      if @rate_control_proc
        @rate = @rate_control_proc.call(rate)
        while @rate <= 0 # sleep and check again.
          sleep(@interval)
          @rate = @rate_control_proc.call(rate)
        end
      end
    end

    def periodically
      if @periodically
        @periodically.perform { yield }
      end
    end

    def measure_time
      @last_measured = Time.now
    end
  end
end
