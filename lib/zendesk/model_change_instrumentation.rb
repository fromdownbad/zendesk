# Instrument sources of AR model column changes
#
# It is built with basic assumption that changes can be initiated from
# - controllers via http request
# - Rake tasks
# - Delayed jobs

module Zendesk
  class ModelChangeInstrumentation
    DEFAULT_SOURCE_PATTERN = 'lib\/zendesk\/support|app\/controllers|lib\/tasks|app\/models\/jobs|lib\/.+job'.freeze
    RAILS_CONSOLE_PATTERN = 'lib\/rails\/commands\/console'.freeze

    def initialize(model:, exec_context:, columns_to_instrument:, source_of_instrument:, source_pattern: DEFAULT_SOURCE_PATTERN, prevent_unknown_update_from: [])
      @model = model
      @exec_context = exec_context
      @columns_to_instrument = columns_to_instrument.map(&:to_s)
      @source_of_instrument = source_of_instrument
      @source_pattern = source_pattern
      @prevent_unknown_update_from = prevent_unknown_update_from
    end

    def perform
      report_change_source_metrics if changed_columns.any? || @model.destroyed?
    end

    private

    def changed_columns
      @changed_columns ||= (@model.previous_changes.keys + @model.changes.keys) & @columns_to_instrument
    end

    def report_change_source_metrics
      trace = @exec_context.send :caller_locations
      changed_file, changed_line = change_initiated_from_file(trace)
      unless changed_file
        raise_error if @prevent_unknown_update_from.include?(:console) && change_initiated_from_rails_console?(trace)
        log_trace(trace)
        changed_file = 'unknown'
      end

      change_tags = changed_columns.map { |column| "#{column}_changed:true" } + ["source:#{changed_file}", "line:#{changed_line}"]
      change_tags << "source_of_instrument:#{@source_of_instrument}"

      statsd_client.increment("#{class_name}_changed", tags: change_tags)
      raise_error(:code) if @prevent_unknown_update_from.include?(:code) && changed_file == 'unknown'
    end

    def change_initiated_from_file(trace)
      # trace lines are of the format <filename>:<line number>:in '<function name>'
      first_trace_with_pattern = trace.find { |line| line.to_s.split(":")[0] =~ Regexp.new(@source_pattern) }

      if first_trace_with_pattern
        matching_result = first_trace_with_pattern.to_s.match(Regexp.new("((?:#{@source_pattern}).*\.(?:rb|rake))\:(\\d+)")) || []

        [matching_result[1], matching_result[2]]
      end
    end

    def raise_error(type = :console)
      case type
      when :console
        source = 'Rails console'
      when :code
        source = 'your code'
      end
      message = "Please do not update #{class_name} directly in #{source}. Talk to team Bilby at #ask-bilby for more details."
      if Rails.env.production?
        Rails.logger.warn message
      else
        raise message
      end
    end

    def change_initiated_from_rails_console?(trace)
      trace.any? { |line| line.to_s =~ Regexp.new(RAILS_CONSOLE_PATTERN) }
    end

    def class_name
      @model.class.name.demodulize.downcase
    end

    def statsd_client
      Zendesk::StatsD::Client.new(namespace: 'model_change_instrumentation')
    end

    def log_trace(trace)
      Rails.logger.info("Unknown change detected for model #{class_name}, trace: #{trace}")
    end
  end
end
