require 'set'

module Zendesk
  module Sla
    module TicketMetric
      SUPPORTED = %i[
        agent_work_time
        first_reply_time
        next_reply_time
        pausable_update_time
        periodic_update_time
        requester_wait_time
      ].freeze

      def self.supported
        SUPPORTED
      end

      def self.all
        @all ||= begin
          supported.map do |metric|
            "#{name}::#{metric.to_s.camelize}".constantize
          end
        end
      end

      def self.from_id(metric_id)
        all.find { |sla_metric| sla_metric.id == metric_id.to_i }
      end

      def self.from_name(name)
        all.find { |sla_metric| sla_metric.name == name.to_sym }
      end

      def self.for_metric(metric)
        all.select { |sla_metric| sla_metric.metric == metric.to_sym }
      end

      def self.supported_metric?(metric)
        for_metric(metric).any?
      end
    end
  end
end

require 'zendesk/sla/ticket_metric/abstract'
require 'zendesk/sla/ticket_metric/agent_work_time'
require 'zendesk/sla/ticket_metric/first_reply_time'
require 'zendesk/sla/ticket_metric/next_reply_time'
require 'zendesk/sla/ticket_metric/pausable_update_time'
require 'zendesk/sla/ticket_metric/periodic_update_time'
require 'zendesk/sla/ticket_metric/requester_wait_time'
