module Zendesk
  module Sla
    class Stage
      def self.for(metric)
        STAGES.map { |stage| stage.new(metric) }.find(&:current?)
      end

      class << self
        private

        def stage_definition(&block)
          Class.new(self) { define_method(:current?, &block) }
        end
      end

      def initialize(metric)
        @metric = metric
      end

      def current?
        fail 'This method must be implemented by subclasses.'
      end

      def name
        self.class.name.demodulize.underscore.to_sym
      end

      protected

      attr_reader :metric

      STAGES = [
        None     = stage_definition { !metric.measured? },
        Active   = stage_definition { !metric.fulfilled? && !metric.paused? },
        Paused   = stage_definition { !metric.fulfilled? && metric.paused? },
        Achieved = stage_definition { metric.fulfilled? }
      ].freeze
    end
  end
end
