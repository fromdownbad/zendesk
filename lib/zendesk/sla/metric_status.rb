require 'zendesk/sla/stage'
require 'zendesk/ticket_metric/transition'

module Zendesk
  module Sla
    class MetricStatus
      def self.all(ticket)
        Zendesk::TicketMetric.
          supported.
          select { |metric| TicketMetric.supported_metric?(metric) }.
          map    { |metric| new(ticket, metric) }
      end

      def initialize(ticket, metric)
        @ticket = ticket
        @metric = metric

        @context = Zendesk::TicketMetric::Context.new(ticket, metric)
      end

      def measured?
        policy_metric.present?
      end

      def fulfilled?
        last_transition.to_state?(:deactivated)
      end

      def paused?
        last_transition.to_state?(:paused)
      end

      def breached?
        breach_at.present? ? breach_at < Time.now : false
      end

      def fulfilled_at
        fulfilled? ? last_transition.time : nil
      end

      def breach_at
        active_breach.try(:time)
      end

      def policy_metric
        @policy_metric ||= last_sla_application.try(:policy_metric)
      end

      def first_policy_metric
        @first_policy_metric ||= first_sla_application.try(:policy_metric)
      end

      protected

      attr_reader :ticket,
        :metric,
        :context

      private

      def last_sla_application
        @last_sla_application ||= context.latest_events.apply_sla.last
      end

      def first_sla_application
        @first_sla_application ||= context.events.where(instance_id: 1).apply_sla.last
      end

      def active_breach
        @active_breach ||= begin
          context.
            latest_events.
            breach.
            after(last_sla_application.try(:time)).
            last
        end
      end

      def last_transition
        @last_transition ||= begin
          Zendesk::TicketMetric::Transition.new(
            ticket,
            context.latest_events
          ).last
        end
      end
    end
  end
end
