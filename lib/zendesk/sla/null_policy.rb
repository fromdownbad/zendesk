module Zendesk::Sla
  class NullPolicy
    ID = 256**8 / 2 - 1

    def self.id
      ID
    end

    def self.title
      nil
    end

    def self.policy_metrics
      NullPolicyMetrics
    end

    def self.priority_metric_map(*)
      {}
    end

    def self.marked_for_destruction?
      false
    end

    class NullPolicyMetrics
      %i[where with_priority].each do |method|
        define_singleton_method(method) { |*| self }
      end

      def self.first
        nil
      end
    end

    private_constant :NullPolicyMetrics
  end
end
