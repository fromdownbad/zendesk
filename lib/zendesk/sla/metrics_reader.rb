require 'zendesk/ticket_metric'
require 'zendesk/sla/metrics_reader/metric_event_status'

module Zendesk::Sla
  class MetricsReader
    RELEVANT_METRIC_EVENT_TYPES = [
      'TicketMetric::Breach',
      'TicketMetric::Pause',
      'TicketMetric::Fulfill'
    ].freeze

    attr_reader :ticket

    delegate :account_id, :sla_ticket_policy, :priority_id, to: :ticket
    delegate :sla_policy_id, to: :sla_ticket_policy

    def initialize(ticket)
      @ticket = ticket
    end

    def metric_statuses
      return [] unless sla_ticket_policy.present?

      recent_events = relevant_events.group_by(&:metric).map do |key, events|
        key.to_sym == :reply_time ? reply_time_events(events) : events.max_by(&:instance_id)
      end.flatten.compact

      recent_events.each_with_object([]) do |event, statuses|
        statuses << MetricEventStatus.new(event)
      end.sort
    end

    private

    def metrics_for_priority
      @metrics_for_priority ||= ::Sla::PolicyMetric.where(
        account_id: account_id,
        sla_policy_id: sla_policy_id,
        priority_id: priority_id
      )
    end

    def policy_metric_names
      @policy_metric_names ||= metrics_for_priority.map do |m|
        m.metric.name
      end
    end

    def formated_metric_names
      @formated_metric_names ||= metrics_for_priority.map do |m|
        format_metric_name(m.metric.name)
      end
    end

    def relevant_events
      @relevant_events ||= ::TicketMetric::Event.where(
        ticket_id: ticket.id,
        account_id: account_id,
        metric: formated_metric_names,
        type: RELEVANT_METRIC_EVENT_TYPES
      ).order(created_at: :desc)
    end

    def reply_time_events(metric_events)
      events = metric_events.partition { |v| v.instance_id < 2 }

      first_reply_time = events.first.max_by(&:instance_id) if policy_metric_names.include?(:first_reply_time)
      next_reply_time = events.last.max_by(&:instance_id) if policy_metric_names.include?(:next_reply_time)

      [first_reply_time, next_reply_time]
    end

    def format_metric_name(name)
      return :reply_time if [:first_reply_time, :next_reply_time].include? name
      name
    end
  end
end
