require 'zendesk/sla/metric_status'

module Zendesk
  module Sla
    class MetricSelection
      def initialize(ticket)
        @ticket = ticket
      end

      def all(include_first_reply_time = false)
        statuses(include_first_reply_time).sort
      end

      def most_relevant
        all.reject(&:fulfilled?).first || NullMetric
      end

      protected

      attr_reader :ticket

      private

      def statuses(include_first_reply_time = false)
        return [] if null_policy?

        ticket.sla_ticket_policy.current_statuses(include_first_reply_time).partition do |status|
          %i[first_reply_time next_reply_time].include?(status.name)
        end.flatten
      end

      def null_policy?
        ticket.sla_ticket_policy.blank? ||
          ticket.sla_ticket_policy.null_policy?
      end
    end
  end
end
