## MetricEventStatus is to only be used with MetricsReader until
## MetricsReader becomes a full replacement for TicketStatus. At that point
## MetricEventStatus will also be a replacement for MetricStatus.
##
module Zendesk::Sla
  class MetricEventStatus
    include Comparable

    EVENT_PRIORITY = {
      breach: 0,
      fulfill: 1,
      pause: 2
    }.freeze
    LEAST_PRIORITY_VALUE = 3

    attr_reader :event, :stage

    delegate :time, :instance_id, to: :event

    def initialize(event)
      @event = event
      @stage = Stage.for(self)
    end

    def event_type
      @event_type ||= event.type.to_sym
    end

    def breach_at
      @breach_at ||= time if event_type == :breach
    end

    def name
      @name ||= if event.metric.to_sym == :reply_time
        instance_id < 2 ? :first_reply_time : :next_reply_time
      else
        event.metric.to_sym
      end
    end

    def measured?
      true
    end

    def fulfilled?
      event_type == :fulfill
    end

    def paused?
      event_type == :pause
    end

    def priority
      EVENT_PRIORITY[event_type] || LEAST_PRIORITY_VALUE
    end

    def <=>(other)
      cmp = priority <=> other.priority
      cmp == 0 ? time <=> other.time : cmp
    end
  end
end
