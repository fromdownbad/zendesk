module Zendesk
  module Sla
    class PolicyAssignment
      def self.persist(ticket)
        return unless ticket.new_sla_policy_id.present?

        ActiveRecord::Base.connection.execute(
          format(
            ::Sla::TicketPolicy.upsert_sql,
            id:            ::Sla::TicketPolicy.generate_uid,
            account_id:    ticket.account.id,
            ticket_id:     ticket.id,
            sla_policy_id: ticket.new_sla_policy_id,
            created_at:    'NOW()',
            updated_at:    'NOW()'
          )
        )

        ticket.sla_ticket_policy(true)
      end

      def initialize(ticket)
        @ticket =  ticket
        @account = ticket.account
      end

      def assign
        return unless policy_change?

        ticket.new_sla_policy_id = matching_policy.id

        ticket.events << policy_audit
      end

      protected

      attr_reader :ticket, :account

      private

      def current_policy
        @current_policy ||= ticket.sla_ticket_policy.try(:policy) || NullPolicy
      end

      def matching_policy
        @matching_policy ||= begin
          ticket.account.sla_policies.active.find do |policy|
            policy.match?(ticket)
          end || NullPolicy
        end
      end

      def policy_change?
        current_policy.id != matching_policy.id
      end

      def policy_audit
        audit_class.new(transaction: audit_details, audit: ticket.audit)
      end

      def audit_class
        ticket.new_record? ? Create : Change
      end

      def audit_details
        ['sla_policy', [current_policy, matching_policy].map(&:title)]
      end
    end
  end
end
