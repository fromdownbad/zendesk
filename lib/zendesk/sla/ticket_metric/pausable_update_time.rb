module Zendesk::Sla
  module TicketMetric
    class PausableUpdateTime < Abstract
      INSTANCES = (0..Float::INFINITY).freeze

      metric_id 6

      def self.metric
        :pausable_update_time
      end
    end
  end
end
