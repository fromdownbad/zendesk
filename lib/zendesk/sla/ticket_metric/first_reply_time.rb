module Zendesk::Sla
  module TicketMetric
    class FirstReplyTime < Abstract
      INSTANCES = (0..1).freeze

      metric_id 1

      def self.metric
        :reply_time
      end
    end
  end
end
