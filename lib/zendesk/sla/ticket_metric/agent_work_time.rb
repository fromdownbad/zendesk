module Zendesk::Sla
  module TicketMetric
    class AgentWorkTime < Abstract
      INSTANCES = (0..Float::INFINITY).freeze

      metric_id 3

      def self.metric
        :agent_work_time
      end
    end
  end
end
