module Zendesk::Sla
  module TicketMetric
    class PeriodicUpdateTime < Abstract
      INSTANCES = (0..Float::INFINITY).freeze

      metric_id 5

      def self.metric
        :periodic_update_time
      end
    end
  end
end
