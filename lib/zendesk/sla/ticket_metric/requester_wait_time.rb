module Zendesk::Sla
  module TicketMetric
    class RequesterWaitTime < Abstract
      INSTANCES = (0..Float::INFINITY).freeze

      metric_id 2

      def self.metric
        :requester_wait_time
      end
    end
  end
end
