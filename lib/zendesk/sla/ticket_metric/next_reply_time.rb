module Zendesk::Sla
  module TicketMetric
    class NextReplyTime < Abstract
      INSTANCES = (2..Float::INFINITY).freeze

      metric_id 4

      def self.metric
        :reply_time
      end
    end
  end
end
