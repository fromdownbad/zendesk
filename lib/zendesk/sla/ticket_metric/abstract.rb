module Zendesk::Sla
  module TicketMetric
    class Abstract
      def self.inherited(subclass)
        super

        subclass.class_eval do
          class << self
            attr_reader :id

            def name
              super.demodulize.underscore.to_sym
            end

            def valid_instance?(instance)
              const_get(:INSTANCES).cover?(instance)
            end

            private

            def metric_id(mapped_id)
              @id = mapped_id
            end
          end
        end
      end
    end
  end
end
