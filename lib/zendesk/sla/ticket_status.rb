module Zendesk
  module Sla
    class TicketStatus
      NONE               = 4_294_967_295 # MySQL max uint
      PAUSED             = NONE - 1
      PAUSED_BREACHED    = PAUSED - 1
      MIN_SPECIAL_STATUS = PAUSED_BREACHED # Used to filter out special values

      def initialize(ticket)
        @ticket = ticket
      end

      def next_breach_at
        active? ? Time.at(ticket.sla_breach_status) : nil
      end

      def metric
        @metric ||= MetricSelection.new(ticket).most_relevant
      end

      def calculate
        return if ticket.sla_breach_status == current_status

        # Avoid reporting metrics for AR callback skip as we have figured out
        # a way to get the sla_breach_status update
        #
        # Read more in lib/zendesk/extensions/ar_skipped_callback_metrics/instrument.rb
        ticket.allow_without_entity_publication do
          ticket.update_column(:sla_breach_status, current_status)
        end
      end

      protected

      attr_reader :ticket

      private

      def active?
        ticket.sla_breach_status < MIN_SPECIAL_STATUS
      end

      def current_status
        @current_status ||= begin
          if !metric.measured?
            NONE
          elsif metric.paused? && metric.breached?
            PAUSED_BREACHED
          elsif metric.paused?
            PAUSED
          else
            [metric.breach_at.to_i, MIN_SPECIAL_STATUS - 1].min
          end
        end
      end
    end
  end
end
