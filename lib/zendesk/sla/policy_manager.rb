module Zendesk
  module Sla
    class PolicyManager
      class PolicyManagerArgumentError < ArgumentError
      end

      def initialize(account, current_user)
        @account      = account
        @current_user = current_user
      end

      # Create an Sla::Policy and all related Sla::PolicyMetrics in one go
      def create(params)
        ::Sla::Policy.transaction do
          policy_metrics = params.delete(:policy_metrics) || []

          initialize_policy(params).tap do |policy|
            add_policy_metrics(policy, policy_metrics)

            policy.save!

            update_position([{id: policy.id, position: policy.position}])
          end
        end
      end

      # Update an Sla::Policy in the "approved" manner.
      def update(policy, params)
        ::Sla::Policy.transaction do
          policy.current_user = current_user

          if params[:policy_metrics].present?
            policy.policy_metrics.each(&:soft_delete)

            add_policy_metrics(policy.reload, params[:policy_metrics])
          end

          policy.title = params[:title] if params[:title].present?

          if params[:description].present?
            policy.description = params[:description]
          end

          if params[:filter].present?
            policy.filter = Definition.build(extract_filter(params))
          end

          policy.save!

          if params[:position].present?
            update_position([{id: policy.id, position: params[:position]}])
          end

          policy
        end
      end

      # Delete an Sla::Policy in the "approved" manner (soft delete)
      def delete(id)
        account.sla_policies.find(id).tap do |policy|
          policy.soft_delete(validate: false)
        end
      end

      def reorder(ordered_ids)
        update_position(
          ordered_ids.each_with_index.map { |id, i| {id: id, position: i.succ} }
        )
      end

      private

      def initialize_policy(params)
        params[:position]     = params[:position].to_i || 1_000_000
        params[:current_user] = current_user

        ::Sla::Policy.new(params).tap do |pol|
          pol.filter  = Definition.build(extract_filter(params))
          pol.account = account
        end
      end

      def extract_filter(params)
        filter = params.delete(:filter)
        map_filter_info(filter)
      end

      def add_policy_metrics(policy, policy_metrics)
        # Caller must supply metric info for each combination of priority_id and
        # metric id.
        policy_metrics.each do |params|
          policy_metrics_map_attributes(params)
          policy.policy_metrics.new do |pm|
            pm.account        = account
            pm.policy         = policy
            pm.target         = params[:target]
            pm.business_hours = params[:business_hours]
            pm.priority_id    = params[:priority_id]
            pm.metric_id      = params[:metric_id]
          end
        end
      end

      def policy_metrics_map_attributes(params)
        if params.key?(:priority)
          params[:priority_id] = PriorityType.find(params.delete(:priority))
        end

        return unless params.key?(:metric)

        metric = Zendesk::Sla::TicketMetric.from_name(params.delete(:metric))

        params[:metric_id] = metric.id if metric.present?
      end

      def map_filter_info(params)
        mapped_info = {}
        if params[:all]
          map_definition_items(params[:all])
          mapped_info[:conditions_all] = params.delete(:all)
        end

        if params[:any]
          map_definition_items(params[:any])
          mapped_info[:conditions_any] = params.delete(:any)
        end

        mapped_info
      end

      # Copied from Zendesk::Rules::Initializer
      def map_definition_items(params)
        params.each do |condition|
          Api::V2::Rules::ConditionMappings.map_definition_item!(
            account,
            condition
          )
        end
      end

      protected

      attr_reader :account,
        :current_user

      private

      def update_position(updates)
        Zendesk::Rules::PositionUpdate.new(
          account.sla_policies.active,
          updates
        ).perform
      end
    end
  end
end
