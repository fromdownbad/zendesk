module Zendesk::Sla
  class NullMetric
    def self.measured?
      false
    end

    def self.breach_at
      nil
    end

    def self.fulfilled_at
      nil
    end

    def self.fulfilled?
      true
    end

    def self.paused?
      false
    end

    def self.stage
      Stage::None.new(self)
    end
  end
end
