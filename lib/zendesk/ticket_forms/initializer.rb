module Zendesk
  module TicketForms
    class Initializer
      include Zendesk::DynamicContentFieldsHelper
      RAW_PARAMETERS = ["name", "display_name"].freeze

      def initialize(account, params)
        @account = account
        @params  = params[:ticket_form].clone || {}
        map_dc_parameters(RAW_PARAMETERS, @params)
        @ticket_field_ids = @params.delete(:ticket_field_ids)
        @restricted_brand_ids = @params.key?(:restricted_brand_ids) ? @params.delete(:restricted_brand_ids) || [] : nil
        @end_user_conditions = @params.key?(:end_user_conditions) ? @params.delete(:end_user_conditions) || [] : nil
        @agent_conditions = @params.key?(:agent_conditions) ? @params.delete(:agent_conditions) || [] : nil
        @ticket_fields = merge_and_order_ticket_fields
        @restricted_brands = restricted_brands
        @form = params[:id].nil? ? @account.ticket_forms.build(@params) : @account.ticket_forms.find(params[:id])
      end

      # dangerous: not idempotent
      def ticket_form
        add_ticket_fields(@form, @ticket_fields)
        add_conditions(@form, @agent_conditions, @end_user_conditions)
        add_restricted_brands(@form, @restricted_brands)
        add_position(@form)
        @form
      end

      def cloned_ticket_form
        @cloned_form = @account.ticket_forms.build(
          name: @form.name,
          display_name: @form.display_name,
          end_user_visible: @form.end_user_visible,
          in_all_brands: @form.in_all_brands
        )
        add_ticket_fields(@cloned_form, @form.ticket_fields)
        add_conditions(@cloned_form, @form.agent_conditions, @form.end_user_conditions)
        add_restricted_brands(@cloned_form, @form.restricted_brands)
        add_position(@cloned_form)
        @cloned_form
      end

      def updated_ticket_form
        update_form_fields
        update_conditions
        update_restricted_brands

        @form.attributes = @params
        @form
      end

      private

      def update_form_fields
        # People can pass an empty array for the ticket_field_ids param in order to delete all the fields in their ticket form.
        # We need a way to distinguish between them wanting to delete the fields versus not changing the fields at all so use nil
        if @ticket_field_ids
          old_ticket_field_ids = @form.ticket_field_ids

          @ticket_fields.each_with_index do |field, index|
            if ticket_form_field = find_ticket_form_field(field.id)
              ticket_form_field.position = index
            else
              @form.ticket_form_fields.build do |form_field|
                form_field.account      = @account
                form_field.position     = index
                form_field.ticket_field = field
              end
            end
          end

          deleted_fields_ids = old_ticket_field_ids - @ticket_fields.map(&:id)

          if deleted_fields_ids.any?
            @form.ticket_form_fields.select { |tff| deleted_fields_ids.include?(tff.ticket_field_id) }.each(&:mark_for_destruction)
          end
        end
      end

      def update_conditions
        # Silently ignore conditions when CTF is off. We don't want to return errors and cause
        # potential API failures on integrations that accidentaly keep sending conditions when CTF is off.
        return unless @account.has_native_conditional_fields_enabled?

        if @agent_conditions
          @form.agent_conditions = @agent_conditions
        end

        if @end_user_conditions
          @form.end_user_conditions = @end_user_conditions
        end
      end

      def update_restricted_brands
        if @restricted_brands && @account.has_multiple_active_brands?
          # don't use @form.restricted_brands because its cache doesn't get
          # flushed even when setting form.ticket_form_brand_restriction
          old_brands = @form.ticket_form_brand_restrictions.map(&:brand).compact

          brands_to_add = @restricted_brands - old_brands
          brands_to_delete = old_brands - @restricted_brands

          add_restricted_brands(@form, brands_to_add)

          if brands_to_delete.any?
            brand_ids_to_delete = brands_to_delete.map(&:id)
            @form.ticket_form_brand_restrictions.select { |r| brand_ids_to_delete.include?(r.brand_id) }.each(&:mark_for_destruction)
          end
        end
      end

      def add_position(form)
        form.position ||= (@account.ticket_forms.maximum(:position) || 0) + 1
      end

      def merge_and_order_ticket_fields
        requested_ticket_fields = load_ordered_ticket_fields

        # Add any missing required ticket fields to the front of the list
        missing_system_fields = @account.ticket_fields.system_required - requested_ticket_fields
        missing_system_fields + requested_ticket_fields
      end

      def load_ordered_ticket_fields
        return [] unless @ticket_field_ids

        fields = @account.ticket_fields(include_voice_insights: true).where(id: @ticket_field_ids)
        fields.sort_by { |f| @ticket_field_ids.index(f.id) }
      end

      def add_conditions(form, agent_conditions, end_user_conditions)
        # Silently ignore conditions when CTF is off. We don't want to return errors and cause
        # potential API failures on integrations that accidentaly keep sending conditions when CTF is off.
        return unless @account.has_native_conditional_fields_enabled?

        form.agent_conditions = agent_conditions || []
        form.end_user_conditions = end_user_conditions || []
      end

      def restricted_brands
        return nil unless @restricted_brand_ids
        @account.brands.active.where(id: @restricted_brand_ids)
      end

      def find_ticket_form_field(field_id)
        ## NOTE: Need to do a select to load the entire collection so autosave will work
        @form.ticket_form_fields.select { |tff| tff.ticket_field_id == field_id }.first
      end

      def add_ticket_fields(form, fields)
        fields.each_with_index do |field, index|
          form.ticket_form_fields.build do |form_field|
            form_field.account         = @account
            form_field.ticket_form     = form
            form_field.position        = index
            form_field.ticket_field    = field
          end
        end
      end

      def add_restricted_brands(form, brands)
        return if brands.nil?

        brands.each do |brand|
          form.ticket_form_brand_restrictions.build do |brand_restriction|
            brand_restriction.account     = @account
            brand_restriction.ticket_form = form
            brand_restriction.brand       = brand
          end
        end
      end
    end
  end
end
