module Zendesk
  module TicketForms
    # Get a set of ticket forms.
    #
    # This class contains the logic needed to get the correct set of ticket forms,
    # depending on the account, user and brand and the parameters that we receive in the request.
    # More information about available parameters in Api::V2::TicketFormsController#index method docs.
    #
    # Examples
    #
    #   ticket_forms = Zendesk::TicketForms::ScopeSelector.new(current_account, current_brand, current_user, params).scope
    #
    class ScopeSelector
      attr_reader :current_account, :current_brand, :current_user, :params

      def initialize(account, brand, user, params)
        @current_account = account
        @current_brand   = brand
        @current_user    = user
        @params          = params

        # End users should only receive end_user_visible ticket forms.
        params[:end_user_visible] = true if current_user.is_end_user?
      end

      def scope
        forms = ticket_forms.where(filters)

        if fallback_to_default? && !contains_end_user_visible_and_active?(forms)
          current_account.ticket_forms.default
        else
          forms
        end
      end

      private

      def fallback_to_default?
        params[:fallback_to_default] == true
      end

      def contains_end_user_visible_and_active?(forms)
        forms.any? { |tf| tf.end_user_visible? && tf.active? }
      end

      def filters
        filters = {}

        filters[:active] = params[:active] if params.key?(:active)
        filters[:end_user_visible] = params[:end_user_visible] if params.key?(:end_user_visible)

        filters
      end

      def ticket_forms
        multibrand_request? ? current_brand.ticket_forms : current_account.ticket_forms
      end

      def multibrand_request?
        current_account.has_multiple_active_brands? && params[:associated_to_brand] == true
      end
    end
  end
end
