##
# Helper to inject tags in datadog metrics being used
# to build Error Budget dashboards.
# @see initializer/sli.rb
#
module Zendesk
  module SliRequestTagger
    module_function

    def store
      Thread.current[:sli_request_tags] ||= {}
    end

    def tags
      store.map { |name, value| "#{name}:#{value}" }
    end

    def set_tag(name, value)
      store[name] = value
    end

    def reset
      store.clear
    end
  end
end
