module Zendesk
  module Brands
    module ControllerSupport
      extend ActiveSupport::Concern

      protected

      def ensure_active_brand!
        if current_account.has_multibrand? && !current_brand.try(:active?)
          render_failure(status: :not_found, title: ::I18n.t('txt.admin.multibrand.brand_invalid_title'), message: ::I18n.t('txt.admin.multibrand.brand_invalid_message'))
        end
      end

      def can_create_brand
        unless current_account.has_multibrand?
          render_failure(status: :forbidden, title: ::I18n.t('txt.admin.multibrand.cannot_create_brand_title'), message: ::I18n.t('txt.admin.multibrand.cannot_create_brand_message'))
        end
      end

      def require_multibrand_feature!
        unless current_account.has_multibrand?
          render_failure(status: :forbidden, title: ::I18n.t('txt.admin.multibrand.feature_required_title'), message: ::I18n.t('txt.admin.multibrand.feature_required_message'))
        end
      end

      def new_brand
        @brand ||= Zendesk::BrandCreator.new(current_account).brand
      end
    end
  end
end
