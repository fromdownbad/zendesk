# The extensions dir is for classes that monkey patch existing class. Not regular
# inheritance, but rewrite of existing functionality.
files = Dir[
  File.expand_path("../extensions/*.rb", __FILE__),
  File.expand_path("../extensions/ar_skipped_callback_metrics/*.rb", __FILE__)
].map { |f| f.sub(".rb", "") }

files.each do |file|
  require file
end

require 'zendesk/extensions/enumerable' # this is in zendesk_channels now
