module Zendesk
  module BusinessHoursSupport
    class Calculation
      MAX_CALCULATION_HOURS = 10_000

      SECONDS_IN_MINUTE = 60
      SECONDS_IN_HOUR   = 60 * SECONDS_IN_MINUTE

      def initialize(schedule)
        @schedule = schedule

        @current_time = Time.now
      end

      def business_minutes_diff(time_1, time_2)
        in_minutes(
          schedule.within(time_1, end_time(time_1, time_2)).in_seconds
        )
      end

      def schedule_id
        schedule.id
      end

      def first_hour_before(time)
        schedule.time(1, :hour).before(Time.at(time))
      end

      def calendar_hours_ago(business_hours)
        return 0 if business_hours <= 0

        in_hours(
          current_time.-(
            schedule.
              time(calculation_hours(business_hours), :hours).
              before(current_time)
          )
        )
      end

      def calendar_hours_from_now(business_hours)
        return 0 if business_hours <= 0

        in_hours(
          schedule.
            time(calculation_hours(business_hours), :hours).
            after(current_time).
            -(current_time)
        )
      end

      def current_holiday
        schedule.
          holiday_calendar.
          overlapping(Zendesk::BusinessHours::Date.today(schedule.time_zone)).
          first
      end

      def in_business_hours?(time)
        schedule.in_hours?(time)
      end

      def on_holiday?(time)
        schedule.on_holiday?(time)
      end

      protected

      attr_reader :schedule,
        :current_time

      private

      def calculation_hours(business_hours)
        [business_hours, MAX_CALCULATION_HOURS].min
      end

      def in_hours(seconds)
        (seconds.to_f / SECONDS_IN_HOUR).round
      end

      def in_minutes(seconds)
        (seconds.to_f / SECONDS_IN_MINUTE).round
      end

      def end_time(time_1, time_2)
        [time_1 + 5.years, time_2].min
      end
    end
  end
end
