require "delegate"

module Zendesk
  module CappedCounts
    class CappedNum < DelegateClass(Integer)
      def initialize(value, max)
        @value = value
        @capped = value == max

        super(@value)
      end

      def capped?
        @capped
      end

      def to_s
        "#{@value}#{"+" if capped?}"
      end
    end

    def capped_count(max = 100_000)
      on_slave do
        count = where(nil).limit(max).count
        CappedNum.new(count, max)
      end
    end
  end
end
