require 'date'

module Zendesk
  module Search
    class GuideQueryParser
      CLASSIC_REGEX = %r{
        (sort:)(asc|desc)     |
        (order_by:)(created)  |
        (updated[:\>\<])(\S*) |
        (created[:\>\<])(\S*) |
        (brand:)(".*?"|\S+)   |
        (via:|cc:|type:|tags:|form:|group:|status:|assignee:|submitter:|recipient:|requester:|commented:|subject:|priority:|fieldvalue:|ticket_type:|description:|organization:)(\S+) |
        (has_attachment:|solved[:\>\<]|due_date[:\>\<]|name:|role:|email:|phone:|notes:|details:|external_id:|is_verified:|is_suspended:|satisfaction:)(\S+)
      }x.freeze

      RELATIVE_TIME = /(?<value>\d+)(?<time_horizon>minutes|hours|days|months|weeks|years)/.freeze

      CLASSIC_GUIDE_PARAMS_MAP = {
        'order_by:' => 'sort_by',
        'sort:'     => 'sort_order',
        'updated:'  => 'updated_at',
        'created:'  => 'created_at',
        'created>'  => 'created_after',
        'created<'  => 'created_before',
        'updated>'  => 'updated_after',
        'updated<'  => 'updated_before',
        'brand:'    => 'brand_id',
      }.freeze

      DATE_PARAMS = ['updated_at', 'created_at', 'updated_before', 'created_before', 'updated_after', 'created_after'].freeze

      attr_reader :params, :query, :brands

      def initialize(search_query)
        @search_query = search_query
        @query        = search_query
        @params       = []
        @brands       = []
        @is_valid     = true

        parse_query
      end

      def valid?
        @is_valid && !@query.empty?
      end

      private

      def parse_query
        matches = @query.scan CLASSIC_REGEX
        matches = matches.map(&:compact)                          # [["sort:", "desc"],[...],[...]]

        matches.map do |key, val|                                 # a match is of type: ["sort:", "desc"]
          mapped_value = CLASSIC_GUIDE_PARAMS_MAP[key]            # checks if we have a mapped value for it

          case mapped_value

          when 'sort_by'
            @params << "#{mapped_value}=#{val}_at"                 # transform sort_by:created to sort_by:created_at

          when 'sort_order'
            @params << "#{mapped_value}=#{val}"

          when 'brand_id'
            @brands << unquote(val)                                # remove quotes from quoted brand search

          when *DATE_PARAMS
            date = parse_date_params(val)
            @params << "#{mapped_value}=#{date}" unless date.nil?  # parse date params into format accepted by HC
          end

          # Remove the match from the query
          @query.gsub! "#{key}#{val}", ''
        end

        # Remove duplicate whitespaces
        @query.squish!
      rescue StandardError => e
        @is_valid = false
        ZendeskExceptions::Logger.record(e, location: self, message: "Could not parse Search query #{@search_query}", fingerprint: '36a389c4748e1da89aa3132a8cefdd76')
      end

      def parse_date_params(val)
        # Check for relative times
        match = RELATIVE_TIME.match val

        if !match.nil?
          # HC API does not support timestamps
          return nil if %w[minutes hours].include? match[:time_horizon]

          # Convert to a date
          return match[:value].to_i.send(:"#{match[:time_horizon]}").ago.to_date
        else
          Date.parse(val).strftime("%Y-%m-%d")
        end
      rescue ArgumentError
        nil
      end

      def unquote(param)
        if param.start_with?('"') && param.end_with?('"')
          return param[1...-1]
        end
        param
      end
    end
  end
end
