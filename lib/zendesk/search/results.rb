module Zendesk
  module Search
    class Results < WillPaginate::Collection
      attr_accessor :exception
      attr_accessor :facets
      attr_accessor :highlights

      def self.empty(exception = nil)
        instance = new(1, 1, 0)
        instance.exception = exception
        instance
      end

      def concat(other)
        if has_highlights && other.has_highlights
          highlights[:results].concat(other.highlights[:results])
        end
        super
      end

      def has_highlights # rubocop:disable Naming/PredicateName
        highlights && highlights[:results]
      end
    end
  end
end
