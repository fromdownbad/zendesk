require 'zendesk/search'

module Zendesk
  module Search
    class Query
      include Zendesk::Search::Error::Handling
      extend  OptionAccessor

      SUPPORTED_HELP_CENTER_SEARCH_TYPES = %w[article question].freeze

      SSL_OPTIONS =
        if /\A(true|t|yes|y|1)\Z/i.match?(ENV['DISABLE_SSL_VERIFY'])
          { verify: false }
        end

      attr_accessor :result, :string
      option_accessor :type, :endpoint, :page, :field_weights,
        :per_page, :sort_mode, :order, :mixed_include, :agent_bias,
        :problem_bias, :match_percent, :facets, :source,
        :include_helpcenter, :incremental, :highlight_key,
        :use_ja_extended_mode, :use_guide_search_for_articles,
        :include_side_conversations, :include_side_conversations_count,
        :hl, :cursor_based_pagination, :page_size, :page_after, :filter_type

      alias :to_s :string

      def initialize(query_string, options = {})
        @string = query_string.to_s.strip
        @request = options[:request]
        @account = options[:account]
      end

      def execute(user)
        with_error_handling do
          return no_result if string.blank? && type.blank?

          if options[:include_helpcenter]
            # Ignore entries, and apply access control for HC content.
            # TODO: add :topics=>topics(user) when we add community content to the main search index.
            options.merge!(without: {type: :entry}, user_segments: user_segments(user))

            # Skip article results if we're getting them from Guide Search
            options[:without] = {type: :article} if options[:use_guide_search_for_articles]
          else
            # In Web Portal case, type=article is an alias for type=entry.
            options[:type] = 'entry' if options[:type] == 'article'
            # filter out HC content
            options[:without] = {type: :article}
          end

          if options[:cursor_based_pagination]
            [:type, :page, :per_page, :sort_mode].each { |k| options.delete(k) }
          end

          add_arturo_flags_to_options!

          @result = Zendesk::Search.search(user, string, options)

          if facets && @result.facets && @result.facets['type']
            if !options[:include_helpcenter] && @result.facets['type']['entry']
              # in web portal, use entry count as article count
              merge_article_facet_count(@result.facets['type']['entry'], @result)
            else
              # never want to return an entry count
              result.facets['type'].delete('entry')
            end
          end
        end

        @result
      end

      def merge_article_facet_count(article_count, result)
        if facets && result.facets && result.facets['type']
          result.facets['type']['article'] = article_count
          result.facets['type'].delete('entry')
        end
      end

      # Returns array of user segment ids that the user is allowed to access.
      # This includes the sentinel value `-1` for public articles.
      def user_segments(user)
        cache_key = ['search_query_user_segments', @account.try(:id), user.id].join('/')

        Rails.cache.fetch(cache_key, expires_in: 5.minutes) do
          help_center_access_control(user, 'user_segment').map(&:to_s).push('\\-1')
        end
      end

      # Returns array of topic ids that the user is allowed to access.
      def topics(user)
        help_center_access_control(user, 'topic')
      end

      # Calls an internal help_center api to get an array of access_type ids to
      # which the user has access. If an empty array is returned, the user is not
      # allowed to access the corresponding record types.
      def help_center_access_control(user, access_type)
        Rails.logger.debug "help_center_access_control, user=#{user.inspect}, access_type=#{access_type}"
        begin
          @client ||= connection(@request)
          Rails.logger.debug "HelpCenter connection: #{@client.inspect}"
          path = "/hc/api/internal/users/#{user.id}/access/#{access_type}s.json"
          parameters = {}
          parameters[:multibrand] = true if @account && @account.has_multiple_active_brands?
          result = @client.get(path, parameters)
          if result.success?
            result_key = "#{access_type}_ids"
            Rails.logger.debug "Accessible #{access_type}s are #{result.body[result_key]}"
            return result.body[result_key]
          else
            Rails.logger.error "Help Center access control call to #{path} failed: #{result.env}"
          end
        rescue Faraday::Error => e
          Rails.logger.error(e)
        end
        []
      end

      def connection(request)
        Faraday.new(ssl: SSL_OPTIONS) do |builder|
          authorization = request.authorization

          if authorization.present?
            builder.headers = {'Authorization' => authorization}
          else
            cookie = request.cookies['_zendesk_shared_session']
            builder.headers = {'Cookie' => "_zendesk_shared_session=#{cookie}"} unless cookie.nil?
          end

          builder.url_prefix = "#{request.protocol}#{request.host}:#{request.port}"
          builder.request :url_encoded
          builder.response :json
          builder.adapter Faraday.default_adapter
        end
      end

      def no_result
        @result = Zendesk::Search::Results.empty
      end

      def options
        @options ||= { per_page: API_DEFAULT_PER_PAGE }
      end

      private

      def add_arturo_flags_to_options!
        arturo_flags = []

        if @account && @account.has_search_performance_monitoring?
          arturo_flags << :search_performance_monitoring
        end

        if @account && @account.has_time_based_indexing?
          arturo_flags << :time_based_indexing
        end

        if @account && @account.has_search_return_limit_in_search_service?
          arturo_flags << :search_return_limit_in_search_service
        end

        if @account && @account.has_exclude_articles_from_search_results?
          arturo_flags << :exclude_articles_from_search_results
        end

        options[:arturo_flags] = arturo_flags.join(",") unless arturo_flags.empty?
      end
    end
  end
end
