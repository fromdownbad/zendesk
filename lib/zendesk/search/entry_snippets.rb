module Zendesk
  module Search
    module EntrySnippets
      def self.detect_term_in_string(query, string)
        terms = query.split
        terms.detect { |term| %r{#{Regexp.escape(term)}}i =~ string }
      end

      def self.query_in_string?(query, string)
        detect_term_in_string(query, string) != nil
      end

      def self.create(query, body_string, chars_before, chars_after)
        query_term = detect_term_in_string(query, body_string)
        match = %r{(^|\W)(#{query_term})(\W|$)}.match(body_string)
        return unless match
        index = match.begin(2)
        before_index = index - chars_before
        after_index = index + query_term.length + chars_after
        result = body_string
        ellipsis = '...'
        if before_index > 0
          result = ellipsis + result.slice(before_index..-1);
          after_index = after_index - before_index + ellipsis.length;
        end
        if after_index < result.length
          result = result.slice(0...after_index) + ellipsis;
        end
        result
      end
    end
  end
end
