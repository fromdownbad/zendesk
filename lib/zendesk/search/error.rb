module Zendesk
  module Search
    class Error
      module Handling
        attr_writer :error

        def with_error_handling
          @error = nil
          yield
        rescue StandardError => e
          @error = Error.new(e)
          Rails.logger.warn("Query failed: #{e.class} / #{e.message}")
          Rails.logger.debug(e)
          raise_or_record(@error) if @error.unknown?

          @result = Zendesk::Search::Results.empty(@error.exception)
        end

        def success?
          error.nil?
        end

        def error_message
          error.try(:message)
        end

        def error
          @error ||= error_from_result
        end

        def error_from_result
          Error.new(@result.exception) if @result && @result.exception
        end

        def raise_or_record(error)
          raise_unknown_exceptions? ? raise(error.exception) : error.record(self)
        end

        def raise_unknown_exceptions?
          Rails.env.test?
        end
      end

      include Zendesk::Serialization::Search::ErrorSerialization

      attr_accessor :exception, :message, :type

      def initialize(exception)
        @exception = exception
        @known = determine_error
      end

      def unknown?
        !known?
      end

      def known?
        @known
      end

      def record(query)
        ZendeskExceptions::Logger.record(@exception, location: query, message: "Query failed: #{query}", fingerprint: '5eeb2513f2b4801aab2375a6a7f342220ad8573c')
      end

      protected

      def determine_error
        case exception
        when Timeout::Error, Faraday::ClientError, ZendeskSearch::ServerError, JohnnyFive::CircuitTrippedError
          @message = "Sorry, we could not complete your search query. Please try again in a moment."
          @type    = :unavailable
          true
        when ZendeskSearch::QueryError
          @message = "Invalid search: #{exception.message}"
          @type    = :invalid
          true
        else
          @message = "Internal Server Error"
          @type    = :internal_server_error
          false
        end
      end
    end
  end
end
