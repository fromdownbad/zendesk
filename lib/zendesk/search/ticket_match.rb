module Zendesk
  module Search
    # Provides a shortcut to looking up a ticket when the search query is simply a ticket's id.
    class TicketMatch
      # A number with an optional hashtag
      NICE_ID_PATTERN = /^#?([0-9]+)$/

      attr_accessor :account, :query, :conditions, :user

      def found?
        nice_id.present? && results.present?
      end

      def nice_id
        @query.to_s =~ NICE_ID_PATTERN
        $1
      end

      def size
        results.size
      end

      def results
        @results ||=
          if should_search_remote_tickets?
            paginated_results(remote_tickets << ticket)
          else
            paginated_results([ticket])
          end
      end

      def allowed_tickets
        account.tickets.for_user(user)
      end

      protected

      def should_search_remote_tickets?
        user.is_agent? && account.sharing_agreements.any?
      end

      def conditions_for(ids)
        if conditions
          {conditions: "#{conditions} AND id IN (#{ids.join(',')})"}
        else
          {conditions: "id IN (#{ids.join(',')})"}
        end
      end

      def remote_tickets
        ticket_ids = ticket_ids_from_shared_tickets(nice_id)
        if ticket_ids.present?
          allowed_tickets.where(options.merge(conditions_for(ticket_ids))[:conditions]).to_a
        else
          []
        end
      end

      def ticket_ids_from_shared_tickets(original_id)
        account.shared_tickets.where(original_id: original_id).pluck(:ticket_id)
      end

      def ticket
        scoped = allowed_tickets
        scoped = scoped.where(options[:conditions]) if options[:conditions]
        scoped.find_by_nice_id(nice_id)
      end

      def options
        options = {}
        options[:conditions] = conditions if conditions
        options
      end

      def paginated_results(tickets)
        compacted = tickets.compact.uniq
        Zendesk::Search::Results.new(1, query.per_page, compacted.size).replace(compacted)
      end
    end
  end
end
