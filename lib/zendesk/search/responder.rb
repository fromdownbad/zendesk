require 'zendesk/search'

module Zendesk
  module Search
    # Legacy search support. Avoid using.
    module Responder
      include Zendesk::Search::ControllerSupport::Serialization

      protected

      def respond_with_search_result(_options = {})
        @result = query.result

        if query.success?
          ensure_compatible_with_forum_templates

          respond_to do |format|
            format.html { render }
            format.xml  { render xml: @result.to_xml(serialization_options) }
            format.json { render json: @result.to_json(serialization_options), callback: params[:callback] }
          end
        else
          flash[:error] = flash_error(query.error_message)

          respond_to do |format|
            format.html { render }
            format.xml  { render xml: query.error.to_xml, status: :not_acceptable }
            format.json { render json: query.error.to_json, status: :not_acceptable }
            format.js   { render json: query.error.to_json, status: :not_acceptable }
          end
        end
      end

      def ensure_compatible_with_forum_templates
        @entries = @result
      end
    end
  end
end
