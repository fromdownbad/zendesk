module Zendesk
  module Search
    module ControllerSupport
      module AccessControl
        def restrict_anonymous_access
          allow_anonymous_users if: :public_forums?
          before_action :deny_anonymous_access_to_private_forums, only: :index
        end
      end

      module Serialization
        protected

        def serialization_options
          {}
        end
      end

      include Serialization

      def self.included(base)
        base.extend(AccessControl)
        base.send(:include, Zendesk::Stats::ControllerHelper)
      end

      protected

      def agent_found_ticket_match?
        current_user.is_agent? && ticket_match.found?
      end

      def ticket_match_shortcut
        if ticket_match.size == 1
          redirect_to ticket_path(ticket_match.results.first.nice_id, format: params[:format])
        else
          query.result = ticket_match.results
          respond_with_search_result
        end
      end

      def ticket_match
        @ticket_match ||= Zendesk::Search::TicketMatch.new.tap do |match|
          match.query   = query
          match.account = current_account
          match.user    = current_user
        end
      end

      def query
        @query ||= Zendesk::Search::Query.new(params[:query], account: current_account, request: request).tap do |query|
          query.page = get_page
          query.options.merge!(search_params.to_options)
        end
      end

      def search_params
        Zendesk::Search::Params.new(params)
      end

      def deny_anonymous_access_to_private_forums
        return deny_access if current_user.is_anonymous_user? && !current_account.has_public_forums?
      end

      def record_search_statistics
        store_search_stat(query.to_s, origin: 'agent')
      end

      def public_forums?
        !current_account || current_account.has_public_forums?
      end
    end
  end
end
