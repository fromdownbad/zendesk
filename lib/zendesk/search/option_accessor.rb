module Zendesk
  module Search
    module OptionAccessor
      def option_accessor(*names)
        names.each do |name|
          class_eval(<<-RUBY, __FILE__, __LINE__ + 1)
                                         # option_accessor :order
            def #{name}                  # def order
              options[:#{name}]          #   options[:order]
            end                          # end

            def #{name}=(value)          # def order=(value)
              options[:#{name}] = value  #   options[:order] = value
            end                          # end

          RUBY
        end
      end
    end
  end
end
