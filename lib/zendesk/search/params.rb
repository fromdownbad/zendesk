module Zendesk
  module Search
    # This converts parameters from the public API into valid search options
    #
    # Example:
    #   params        = { :order_by => 'priority', :unsupported_key => 'hello' }
    #   Zendesk::Search::Params.new(params).to_options
    #   => { :order => :priority_id, :sort => :relevance }
    class Params
      # Ordering
      ALLOWED_ORDER_VALUES = Set['updated_at', 'created_at', 'priority', 'status', 'ticket_type']

      ORDER_VALUE_IDS = {
        'ticket_type' => :ticket_type_id,
        'priority'    => :priority_id,
        'status'      => :status_id
      }.freeze

      # Sorting
      DEFAULT_SORT_MODE    = :relevance
      ALLOWED_SORT_VALUES  = Set['asc', 'desc', 'relevance']

      CURSOR_BASED_PAGINATION_ACTIONS = ["export"].freeze

      attr_reader :params
      attr_writer :sort_key, :order_key

      def initialize(params)
        @params = params
      end

      def to_options
        options = {}
        options[:forum_id]  = forum_id if forum_id
        options[:per_page]  = per_page
        options[:sort_mode] = sort_mode

        if by_updated_to_options.present?
          options.merge!(by_updated_to_options)
        else
          options[:order] = order if order
        end

        options[:type] = params[:type]

        # highlighting enabled or disabled
        options[:hl] = params[:include] && params[:include].split(',').include?('highlights')

        if cbp_endpoint?
          options[:page_size] = params.dig(:page, :size)
          options[:page_after] = params.dig(:page, :after)
          options[:filter_type] = params.dig(:filter, :type)
        end

        options
      end

      def by_updated_to_options
        if params[:by_updated].to_i == 1
          { sort_mode: :desc, order: :updated_at }
        else
          {}
        end
      end

      # Ensures only valid per_page numbers returned for will_paginate pagination
      def per_page
        per_page = params[:per_page].to_i

        if per_page <= 0
          API_DEFAULT_PER_PAGE
        else
          [per_page, API_MAX_PER_PAGE].min
        end
      end

      # Default to :sort_mode => :relevance if no default sort mode is set or is invalid
      def sort_mode
        ALLOWED_SORT_VALUES.include?(params[sort_key]) ? params[sort_key].to_sym : DEFAULT_SORT_MODE
      end

      def order
        return unless ALLOWED_ORDER_VALUES.include?(params[order_key])

        order = params[order_key]
        ORDER_VALUE_IDS[order] || order.to_sym
      end

      def sort_key
        @sort_key ||= :sort
      end

      def order_key
        @order_key ||= :order_by
      end

      def forum_id
        params[:forum_id]
      end

      private

      def cbp_endpoint?
        CURSOR_BASED_PAGINATION_ACTIONS.include? params.dig(:action)
      end
    end
  end
end
