module Zendesk
  module Search
    class CBPResults < Array
      attr_reader :cursor
      attr_accessor :exception
      attr_accessor :facets
      attr_accessor :highlights

      def initialize(records, meta)
        super(records)
        @meta = meta.symbolize_keys
        @cursor = @meta.dig(:after_cursor) if meta
      end

      def cursor_pagination_version
        2
      end

      # We need to disable Rubocop here since this method name is something the presenter expects
      # rubocop:disable Naming/PredicateName
      def has_more?
        @meta.nil? ? false : @meta.dig(:has_more)
      end
      # rubocop:enable Naming/PredicateName

      def after_cursor
        @cursor
      end

      def before_cursor
        nil
      end
    end
  end
end
