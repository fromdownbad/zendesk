module Zendesk
  module Search
    module Elasticsearch
      module Search
        HERE = "Zendesk::Search::Elasticsearch::Search".freeze
        APM_SERVICE_NAME = 'classic-search'.freeze

        HighlightKey = Struct.new(:type, :id, :locale)

        class << self
          def search(user, user_query, options = {})
            ZendeskAPM.trace(HERE, service: APM_SERVICE_NAME) do |span|
              options[:per_page] ||= options[:limit] || 15
              options[:page] ||= 1

              Rails.logger.info "#{HERE}: user='#{user}', user_query='#{user_query}', options=#{options}"
              params = elasticsearch_params(user, user_query, options)
              query = expand_query_from_options(user_query, options)

              enrich_search_apm_request(span, user, user.account, options)

              # Call Elasticsearch and track the time taken.
              response = {}
              search_time = Benchmark.realtime do
                begin
                  if params[:type] == 'side_conversation'
                    params[:type] = nil
                  end
                  search_client = search_client(user.account.id, user.account.has_search_green?, user.account.has_ss_enable_search_service_hub?)
                  response = search_client.search(query, params)
                rescue ZendeskSearch::ServerError => e
                  message = "Failed to execute search #{@account_id}: #{e.message}"
                  Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
                end
              end

              response = {} if response.is_a?(Array)

              docs = response["results"] || []

              num_found = response["count"] || 0

              if response["elasticsearch"]
                search_time = (search_time * 1000).round # Convert to milliseconds.
                elasticsearch_time = response["elasticsearch"]["took"]
                Rails.logger.info "#{HERE}: Hits: #{num_found}. Total search time: #{search_time}ms. Elasticsearch took: #{elasticsearch_time}ms"
              end

              # Fetch records from the database using the ids, and/or convert
              # Search Service result records into the required form.
              records = convert_docs_to_models(docs, options)

              # Extract highlights/snippets from the Elasticsearch response.
              highlights_in = docs.each_with_object({}) do |doc, memo|
                type = doc["type"]
                id = doc["id"].to_i
                locale = doc["locale"]
                memo[HighlightKey.new(type, id, locale)] = doc["_highlights"]
              end

              # Create the highlights list, filtering them in case there were any
              # for records no longer in the database.  A highlight is identified by
              # a record type and id, and in some cases (article), a locale.
              highlights = []
              records.each do |r|
                type = (class_to_type[r.class.name] || r["result_type"]).to_s
                id = r["id"].to_i
                locale = r["locale"] # may be nil
                highlight = highlights_in[HighlightKey.new(type, id, locale)]
                next unless highlight
                highlight[:result_type] = type unless options[:highlight_key]
                highlight[:id] = r[highlight_id_field(type)]
                highlight[:locale] = locale if locale
                highlights << highlight
              end

              # Get Side Converations
              if options[:include_side_conversations] && (
                options[:type] == "side_conversation" || options[:include_side_conversations_count]
              )
                @sc_results = []
                @sc_count = 0

                sc_result = side_conversations_client(user.account).search_side_conversations(user_query, options)
                @sc_results = sc_result.fetch("results", [])
                @sc_count = sc_result.fetch("total", 0)

                if options[:type] == "side_conversation"
                  records = @sc_results
                  num_found = @sc_count
                end
              end

              hc_article_count = 0

              # Get the articles from the Guide Search
              if should_use_hc_for_articles?(user.account, options)
                query_parser = Zendesk::Search::GuideQueryParser.new(user_query)

                # If we can't successfully parse the query params, return an empty array
                if query_parser.valid?
                  # Gets converted Query into HC api params
                  guide_params = query_parser.params

                  # Build query params
                  guide_params << "page=#{options[:page]}"         if options[:page]
                  guide_params << "per_page=#{options[:per_page]}" if options[:per_page]

                  # Build Brand query params
                  unless query_parser.brands.empty?
                    guide_brand_ids = user.account.brands.where(name: query_parser.brands).pluck(:id)

                    # Add a comma separated list of brands to search for
                    guide_params << "brand_id=#{guide_brand_ids.join(',')}" unless guide_brand_ids.empty?
                  end

                  # Request only the count if the type is not article
                  guide_params << "count_only=true" if options[:type] != "article"

                  # Search for HC articles
                  hc_response = help_center_client(user).search_articles(query_parser.query, guide_params)

                  # We only requested the count if we're searching for users or tickets
                  if options[:type] != "article" && !hc_response.empty?
                    hc_article_count = hc_response.to_i
                  end

                  if options[:type] == "article"
                    hc_article_count = hc_response.fetch("count", 0)
                    hc_results = hc_response.fetch("results", [])

                    records = convert_guide_results(hc_results)
                    num_found = hc_article_count
                  end
                end
              end

              per_page = options[:per_page].to_i
              page = options[:page].to_i

              results = if options[:cursor_based_pagination]
                meta = response["meta"]
                Zendesk::Search::CBPResults.new(records, meta)
              else
                Zendesk::Search::Results.create(page, per_page, num_found) do |pager|
                  pager.replace(records)
                end
              end

              # Simply pass facets through
              results.facets = response["facets"] unless response["facets"].blank?

              # Replace the article count with the result from HC
              if options[:use_guide_search_for_articles] && results.facets && results.facets['type']
                results.facets['type']['article'] = hc_article_count
              end

              if options[:include_side_conversations] && results.facets && results.facets['type']
                results.facets['type']['side_conversation'] = @sc_count
              end

              highlight_key = options[:highlight_key] || :results
              results.highlights = {highlight_key => highlights} unless highlights.empty?

              enrich_search_apm_response(span, num_found.to_i, search_time, elasticsearch_time)

              # Return results
              results
            end
          end

          def class_to_type
            @class_to_type ||= TYPE_TO_CLASS.invert
          end

          private

          def highlight_id_field(type)
            if type == "ticket"
              "nice_id"
            else
              "id"
            end
          end

          def search_client(account_id, has_search_green, has_search_service_hub)
            ZendeskSearch::Client.new(account_id, has_search_green, has_search_service_hub)
          end

          def should_use_hc_for_articles?(account, options)
            options[:use_guide_search_for_articles] && has_help_center?(account)
          end

          def has_help_center?(account) # rubocop:disable Naming/PredicateName
            account.brands.any?(&:has_help_center?)
          end

          def help_center_client(user)
            Zendesk::HelpCenter::InternalApiClient.new(account: user.account, requester: user)
          end

          def side_conversations_client(account)
            @side_conversations_client ||= Zendesk::SideConversations::InternalApiClient.new(account: account)
          end

          def convert_guide_results(records)
            records.map do |r|
              r.slice(
                "id",
                "title",
                "created_at",
                "updated_at",
                "brand_name",
                "brand_id",
                "locale",
                "result_type",
                "html_url",
                "name",
                "snippet"
              )
            end
          end

          # Sorting options are mapped into the query string that is to be
          # passed to the search service.  We could alternatively add extra parameters
          # to the search service end points, but for now this seems a little simpler.
          #
          # We ignore sort_mode:relevance because it is unnecessary and silly.
          # (Order by relevance would make sense, but would be unnecessary. Sort
          # mode = asc, desc, or relevance, makes no sense.)
          #
          # Now allowing multiple values for options[:order] for temporary compatibility
          # with Solr. (It is used in app/controllers/people/search_controller.rb.)
          # It is not a good interface, because you cannot specify a sort mode for
          # each order, but we can fix it up post-Solr.
          #
          # Note that sort fields get expanded in params.h to concrete field names, e.g.
          # status -> status_id. That's needed for Solr, but doesn't work with the Search
          # Service, so we strip off the _id suffixes here.
          def expand_query_from_options(user_query, options)
            extra = []
            [*options[:order]].each { |order| extra << "order_by:#{order.to_s.gsub(/_id$/, '')}" }
            extra << "sort:#{options[:sort_mode]}" if options[:sort_mode] && options[:sort_mode] != :relevance
            extra.empty? ? user_query : "#{extra.join(' ')} #{user_query}"
          end

          # Keywords that can be used in options[:with] and options[:without], and
          # their mappings to Elasticsearch field names.
          FIELDNAME_MAP = {
              id: :_id,
              cc_id: :cc_id,
              forum_id: :forum_id,
              organization_id: :organization_id,
              requester_id: :requester_id,
              status_id: :status_id,
              type: :_type,
              visibility_restriction_id: :visibility_restriction_id,
              user_role: :user_role
          }.freeze

          def filter_value(keyword, value, modifier)
            fieldname = FIELDNAME_MAP[keyword]
            unless fieldname
              Rails.logger.warn("Unexpected search keyword: #{modifier}#{keyword}:#{value} (Will assume #{keyword} is an Elasticsearch field name.)")
              fieldname = keyword
            end
            # Escape colons in value field, to support tags with colons in them.
            val = Array(value).map { |v| v.to_s.gsub(':', '\:') }
            "#{modifier}#{fieldname}:(#{val.join(" OR ")})"
          end

          def elasticsearch_params(user, user_query, options)
            params = {
                user_id: user.id,
                locale: user.locale,
                time_zone: ActiveSupport::TimeZone[user.time_zone].tzinfo.identifier,
                group_ids: user.group_ids.join(","),
                fq: fq(user, options)
            }

            [:type, :incremental, :hl, :endpoint].each do |k|
              params[k] = options[k] if options.key? k
            end

            if options[:facets]
              # we only support faceting by type, for now
              params[:facet] = 'type'
            end

            if options[:use_ja_extended_mode]
              params[:use_ja_extended_mode] = true
            end

            if options[:cursor_based_pagination]
              params[:page] = { size: options[:page_size], after: options[:page_after] }
              params[:filter] = { type: options[:filter_type] }
            elsif options[:page] && options[:per_page]
              params[:size] = options[:per_page].to_i
              params[:from] = params[:size] * (options[:page].to_i - 1)
            end

            keywords = extract_possible_keywords(user_query)
            unless keywords.empty?
              custom_fields = user.account.custom_fields.where(key: keywords).to_a
              unless custom_fields.empty?
                params[:custom_fields] = JSON.generate(custom_field_metadata(custom_fields))
              end
            end

            if has_via_constraint?(user_query)
              params[:via_map] = JSON.generate(anychannel_via_map(user))
            end

            if options[:entry_bias] && options[:entry_bias] != 0.5
              params[:bq] = '+user_role:0 +_type:entry'
              params[:bv] = options[:entry_bias]
            end

            if options[:agent_bias] && options[:agent_bias] != 0.5
              params[:bq] = '-user_role:0 +_type:user'
              params[:bv] = options[:agent_bias]
            end

            if options[:problem_bias] && options[:problem_bias] != 0.5
              params[:bq] = '+ticket_type_id:3 -status_id:4 +_type:ticket' # ticket_type_id:3 = Problem Type
              params[:bv] = options[:problem_bias]
            end

            if options[:arturo_flags]
              params[:arturo_flags] = options[:arturo_flags]
            end

            params
          end

          def enrich_search_apm_request(span, user, account, options)
            span.set_tag("zendesk.account_id",         account.id)
            span.set_tag("zendesk.account_subdomain",  account.subdomain)
            span.set_tag("zendesk.account_is_sandbox", account.is_sandbox?)
            span.set_tag("zendesk.account_is_trial",   account.is_trial?)
            span.set_tag('zendesk.user_id',            user.id)

            if options[:cursor_based_pagination]
              span.set_tag('search.params.filter.type', options[:filter_type]) if options[:filter_type]
              span.set_tag('search.params.cursor.size', options[:page_size].to_i) if options[:page_size]
              span.set_tag('search.params.cursor.after', options[:page_after]) if options[:page_after]
              span.set_tag('search.params.cursor_based_pagination', true)
            else
              span.set_tag('search.params.per_page', options[:per_page].to_i) if options[:per_page]
              span.set_tag('search.params.page', options[:page].to_i) if options[:page]
              span.set_tag('search.params.sort_mode', options[:sort_mode]) if options[:sort_mode]
              span.set_tag('search.params.order', options[:order]) if options[:order]
              span.set_tag('search.params.type', options[:type]) if options[:type]
              span.set_tag('search.params.incremental', options[:incremental]) if options[:incremental]

              span.set_tag('search.params.use_guide_search_for_articles', !!options[:use_guide_search_for_articles])
              span.set_tag('search.params.include_side_conversations', options[:include_side_conversations]) if options[:include_side_conversations]
            end
          end

          def enrich_search_apm_response(span, num_found, search_time, elasticsearch_time)
            # APM
            span.set_tag('search.hits',               num_found)
            span.set_tag('search.search_time',        search_time)
            span.set_tag('search.elasticsearch_time', elasticsearch_time)

            # Logs
            Rails.logger.append_attributes(
              search: {
                hits: num_found,
                elasticsearch_time: elasticsearch_time
              }
            )
          end

          # Returns an array of fq values, which are filter queries in Elasticsearch
          # query string syntax.
          def fq(user, options)
            fq = user.search_access_control_filters(options)

            if options[:with]
              options[:with].each { |k, v| fq << filter_value(k, v, "") if v }
            end
            if options[:without]
              options[:without].each { |k, v| fq << filter_value(k, v, "-") if v }
            end

            # TODO: This only scopes articles when searching for articles directly (via options[:type]),
            # but searching with `options[:type] = 'ticket'` includes the count of matching articles as well,
            # which then counts articles that the user doesn't have access to.
            if options[:type].nil? || options[:type] == 'article'
              if options[:sections]
                fq << field_value_constraint('section_id', options[:sections], ['article'])
              elsif options[:user_segments]
                fq << field_value_constraint('user_segment_id', options[:user_segments], ['article'])
              end
              fq << "-is_draft:true"
            end

            if options[:type].nil? # TODO: or if explicitly searching community content, not yet supported
              if options[:topics]
                fq << field_value_constraint('topic_id', options[:topics], ['question', 'community_post'])
              end
            end

            fq
          end

          # Returns a string in Elasticsearch query string syntax representing a filter
          # that constrains results of the specified types to have a value of field
          # that is in the values array.
          def field_value_constraint(field, values, types)
            if values.empty?
              not_type_constraint(types)
            else
              "#{field}:(#{values.join(' OR ')}) OR (#{not_type_constraint(types)})"
            end
          end

          def not_type_constraint(types)
            "-_type:(#{types.join(' OR ')})"
          end

          # Returns an array of possible keywords found in the query string. Not
          # bothering to exclude things inside double quotes, because it is OK to
          # have some false keywords. Note that a double colon is now allowed to
          # appear in a keyword; this is to support system custom fields.
          def extract_possible_keywords(user_query)
            user_query.scan(/(?:[^+\-()\s,;:"|&]|::)+(?=[:<>])/).map(&:downcase)
          end

          # Returns a hash of custom field key -> type_info, where type_info is a
          # non-empty array of which each element is either
          #     * a string such as Integer, Decimal, etc, or
          #     * a hash of dropdown choice tag -> id.
          def custom_field_metadata(custom_fields)
            custom_fields.each_with_object({}) do |custom_field, field_hash|
              type_info = custom_field.type
              if type_info == 'Dropdown'
                type_info = custom_field.dropdown_choices.each_with_object({}) do |choice, choice_hash|
                  choice_hash[choice.value] = choice.id
                end
              end
              field_hash[custom_field.key] ||= []
              unless field_hash[custom_field.key].include? type_info
                field_hash[custom_field.key] << type_info
              end
            end
          end

          # For most types of result (docs), we throw away most of the data that we get back
          # from the Search Service, and look the record up in the database.  However, for
          # some types, we use the Search Service data, and transform it with make_result.
          def convert_docs_to_models(docs, options)
            results = Array.new(docs.length)
            db_docs = docs.select { |doc| known_type(doc["type"]) }

            model_and_id = db_docs.map do |doc|
              [type_name_to_model(doc["type"]), doc["id"]]
            end

            stub_only = true
            if options[:incremental]
              # Classic api/v2/search (as opposed to the incremental search) loads data from Riak
              # when the presenter needs it.
              # However because the calls are lazy, Riak is called per record and is not preloaded.
              # To prevent that we force the option only_stub to false so it preloads all records
              # from Riak.
              stub_only = false
            end

            # fill in the result array
            rank_table = {}
            docs.each_with_index do |doc, rank|
              if known_type(doc["type"])
                # database record - save its rank, indexed by model and id
                model = type_name_to_model(doc["type"]).name
                id = doc["id"].to_i
                rank_table[model] ||= {}
                rank_table[model][id] = rank
              else
                # raw record - just put into same position in the result array
                results[rank] = make_result(doc)
              end
            end

            # database lookups in batches by type, then place into ranked results
            model_and_id.group_by(&:first).each do |model, model_and_ids|
              model.find_for_search_results(ids: model_and_ids.map(&:last), stub_only: stub_only, include_models: options[:include]).each do |m|
                # use the original model for lookup since found model could be a stub
                results[rank_table[model.name][m.id]] = m
              end
            end

            # compact, in case some records were not found in database
            results.compact
          end

          TYPE_TO_CLASS = {
              ticket: 'Ticket',
              user: 'User',
              group: 'Group',
              organization: 'Organization',
              entry: 'Entry',
              cms: 'Cms::Variant',
              cmstext: 'Cms::Text'
          }.freeze

          def known_type(type_name)
            type_name && TYPE_TO_CLASS.key?(type_name.to_sym)
          end

          def type_name_to_model(type_name)
            TYPE_TO_CLASS[type_name.to_sym].constantize
          end

          # Creates the result record for non-database-lookup types.
          # Currently we are only dealing with HC articles here.
          def make_result(doc)
            id = doc["id"].to_i
            result_type = doc["type"]

            if result_type == "article"
              doc.slice(
                "title",
                "body",
                "created_at",
                "updated_at",
                "brand_name",
                "locale"
              ).merge!(
                "id" => id,
                "result_type" => result_type,
                "html_url" => html_url(doc),
                "name" => doc["title"],
                "brand_id" => as_int(doc["brand_id"])
              )
            else
              # not used yet...
              doc.merge("id" => id, "result_type" => result_type)
            end
          end

          # Converts string to int, unless it is nil.
          def as_int(str)
            str.nil? ? nil : str.to_i
          end

          # Returns the URL for accessing a HC article.
          def html_url(doc)
            if doc["brand_id"] && doc["locale"]
              brand_url = Brand.find_by_id(doc["brand_id"].to_i).url
              locale = doc["locale"]
              article_id = as_int(doc["article_id"])
              slug = slug_for(doc["title"])
              "#{brand_url}/hc/#{locale}/articles/#{article_id}-#{slug}"
            end
          end

          # copied from help_center/lib/slug.rb
          def slug_for(string)
            encoded_string = remove_invalid_utf8_bytes(string)
            # Now we can safely strip away non-alphanumerics.
            encoded_string.gsub(/[^[:alnum:]]+/, "-")
          end

          # copied from help_center/lib/string_utils.rb
          def remove_invalid_utf8_bytes(string)
            return "" if string.nil?
            string = string.to_s.force_encoding('UTF-8')
            return string if string.valid_encoding?

            string.encode!('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
          end

          def has_via_constraint?(user_query) # rubocop:disable Naming/PredicateName
            user_query.match(/\bvia:/)
          end

          # Returns a map where the keys are the search_names for all RegisteredIntegrationServices for the account,
          # and the values are arrays of the IDs of the related IntegrationServiceInstances.
          def anychannel_via_map(user)
            ::Channels::AnyChannel::RegisteredIntegrationService.where(account_id: user.account.id, deleted_at: nil).map do |ris|
              {
                  search_name: ris.search_name,
                  via_id: ViaType.ANY_CHANNEL,
                  instance_ids: ris.integration_service_instances.map(&:id)
              }
            end
          end
        end
      end
    end
  end
end
