module Zendesk::AccountRestriction
  def self.included(controller)
    controller.around_action :restrict_to_current_account
  end

  protected

  def restrict_to_current_account
    Zendesk.restricted_to_account(current_account) do
      yield
    end
  end
end
