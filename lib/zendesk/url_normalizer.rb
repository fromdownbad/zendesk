require 'uri'

module UrlNormalizer
  def self.normalize(url)
    # Bail early if blank
    return nil if url.nil?

    # Add http if no scheme is missing
    unless /^http(s?):\/\//.match?(url)
      url = "http://#{url}"
    end

    # Downcase the host, leave everything else as is
    url = URI.parse(url)
    url.host = url.host.downcase if url.host
    return url.to_s
  rescue URI::InvalidURIError
    nil
  end
end
