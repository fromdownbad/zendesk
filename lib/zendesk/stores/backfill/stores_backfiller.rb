# Loops over shards looking for blobs stored through attachment-fu
# (see Zendesk::Stores::Backfill::StoresBackfillerConfig for specifics)
# and adds/removes stores as necessary to ensure the blobs are in the
# correct stores based on "preferred stores", which is defined as
# stores flagged as default/secondary in attachments.yml.
module Zendesk
  module Stores
    module Backfill
      class StoresBackfiller < StoresBackfillComponent
        attr_reader :accounts_fetcher, :account_backfiller

        # Creates a new StoresBackfill instance with all it's dependencies
        def self.create_backfiller(opts = {})
          config = StoresBackfillConfig.new(opts)
          region_helper = RegionHelper.new(config)
          synchronizer = Zendesk::Stores::StoresSynchronizer.new
          id_selector = IdSelector.new(config)
          model_backfiller = ModelBackfiller.new(config, synchronizer, id_selector)
          account_backfiller = AccountBackfiller.new(config, region_helper, model_backfiller)

          disabled_accounts_helper = Zendesk::ShardMover::DisabledAccountHelper.new(StoresBackfillConfig::DISABLE_FEATURE, config.statsd)
          accounts_fetcher = lambda do |shard_id, priority_ids|
            sorter = PriorityOrdering.new(priority_ids)
            Zendesk::ShardMover::AccountsOnShard.new(
              disabled_accounts_helper, shard_id, config.disable_ack_interval, lambda { |ids| sorter.sort(ids) }
            )
          end

          StoresBackfiller.new(config, accounts_fetcher, account_backfiller)
        end

        def initialize(config, accounts_fetcher, account_backfiller)
          super(config)
          @accounts_fetcher = accounts_fetcher
          @account_backfiller = account_backfiller
        end

        def run
          logger.initialize_log
          logger.info("Stores Backfiller Starting", config.options)
          loop do
            iter_update = backfill_all_shards
            return iter_update unless config.loop

            # Sleep a bit if nothing evaluated
            # We'll sleep when only encountering errors, to avoid too much spinning wheels
            if iter_update.evaluated == 0
              logger.info("No updates for pod, sleeping for #{config.nothing_to_do_nap} secs")
              sleep config.nothing_to_do_nap
            end

            statsd.increment('shard_loops')
          end
        end

        private

        # Loop over all shards, performing one block of updates for each
        # audit (account, model-class) on each shard.
        #
        # Relies on the account_on_shard_fetcher to ensure that we're
        # acknowledging account-disable requests from exodus
        # and blacklisting disabled accounts.
        #
        # Also relies on the account-backfiller to ensure we're not
        # speding too much time on a single account.
        #
        # Currently, the account-backfiller/config ensures this by limiting time
        # working w/an account to max of 4 mins (need to ack a disable-request
        # w/in 5 mins).
        #
        def backfill_all_shards
          shard_ids.map do |shard_id|
            ActiveRecord::Base.on_shard(shard_id) do
              statsd.increment('shard_visits')
              logger.info("Starting shard", shard_id: shard_id)
              updates = backfill_accounts_on_shard(shard_id)
              logger.info("Shard Update", shard_id: shard_id, evaluated: updates.evaluated, updated: updates.updated, erred: updates.erred)
              updates
            end
          end.sum(Update.new)
        end

        def shard_ids
          return @shards if @shards

          shards = ActiveRecord::Base.shard_names.map(&:to_i)
          # For (my) human brains, the worker_number is from 1 to count,
          # and we assign 1st worker the shards w/mod of 0;
          # For example: if we have 3 workers; shards 1, 4, 7,... go to worker 1,
          # shards 2, 5, 8,... go to worker 2, and 3, 6, 9,... go to worker 3
          shards = shards.select do |s|
            s % config.worker_count == config.worker_number % config.worker_count
          end
          @shards = shards.rotate(Random.rand(shards.length))
        end

        def backfill_accounts_on_shard(shard_id)
          done_ids = done_account_ids

          # remove done accounts from priority accounts to avoid shard being treated as priority
          # solely on the basis of done priority accounts
          priority_ids = fetch_priority_ids - done_ids
          stop_time = current_time + config.weighted_shard_allowance(priority_ids.present?)
          account_ids = accounts_fetcher.call(shard_id, priority_ids)

          updates = Update.new
          account_ids.each do |account_id|
            break if current_time > stop_time

            # Walking the accounts acknowledges any outstanding exodus disable request
            # for the pod (via a periodically in each)
            # so we want to make sure the check for loop over kill-switch check
            # is passing through that -- when kill switch is on, we will skip
            # ahead one account at a time across all shards, sleeping between each account,
            # until the kill switch is shut off.
            statsd.increment('kill_switch_checks')
            if config.kill_switch_on?
              sleep_on_kill_switch
              next
            end

            if done_ids.include?(account_id)
              statsd.increment('account_skips', tags: ["reason:done"])
              next
            end

            priority_account = priority_ids.include?(account_id)
            if config.only_priority? && !priority_account
              statsd.increment('account_skips', tags: ["reason:non_priority"])
              next
            end

            if account_disabled?(account_id)
              logger.debug("Stores backfill disabled for Account", account_id: account_id)
              statsd.increment('account_skips', tags: ["reason:disabled"])
              next
            end

            acct_upd = account_backfiller.backfill_account(account_id, priority_account)
            updates += acct_upd
          end

          updates
        end

        def account_disabled?(account_id)
          !Account.find_by_id(account_id).has_stores_backfill?
        end

        def sleep_on_kill_switch
          logger.warn("SyncAttachments kill-switch is on.  Sleeping for #{config.disable_nap} before rechecking")
          statsd.increment('kill_switch_sleeps')
          sleep config.disable_nap
        end

        def current_time
          Process.clock_gettime(Process::CLOCK_MONOTONIC)
        end

        # Returns priority accounts, which are accounts that have subscribed for specific DC.
        # This is assuming that the backfill is running on a valid pod for the subscription.
        def fetch_priority_ids
          return [] unless config.using_priority? # if weight is zero, there's effectively no priority for locality accounts
          SubscriptionFeatureAddon.where(name: ['germany_data_center', 'eu_data_center']).pluck('distinct(account_id)')
        end

        # Returns set of account-ids whose audits are all done.
        # All audits (one per backfilled model) for an account are created in one shot, so any account w/one
        # audit should have all its audits.  This might temporarily break if we add a new model to backfill_classes,
        # but that will auto-correct after the next backfill cycle loops over the account; it might permanently break
        # if we remove a model w/o some action to clean up audits for the removed model.
        def done_account_ids
          with_audits = StoresBackfillAudit.pluck('distinct(account_id)')
          with_unfinished_audits = StoresBackfillAudit.unfinished.pluck('distinct(account_id)')
          with_audits - with_unfinished_audits
        end
      end
    end
  end
end
