module Zendesk
  module Stores
    module Backfill
      class Update
        attr_reader :evaluated, :updated, :erred

        def initialize(evaluated = 0, updated = 0, erred = 0)
          @evaluated = evaluated
          @updated = updated
          @erred = erred
        end

        def +(other)
          Update.new(@evaluated + other.evaluated, @updated + other.updated, @erred + other.erred)
        end

        def ==(other)
          other && evaluated == other.evaluated && updated == other.updated && @erred == other.erred
        end

        def increment(upd)
          @evaluated += 1
          @updated += 1 if upd
        end

        def add_error
          @erred += 1
        end

        def nothing_done?
          evaluated == 0 && erred == 0
        end

        def to_s
          "StoresBackfillUpdate(eval:#{evaluated},upd:#{updated},err:#{erred})"
        end
      end
    end
  end
end
