# Ordering to place identified priority ids before other ids
module Zendesk
  module Stores
    module Backfill
      class PriorityOrdering
        attr_reader :priority_ids

        def initialize(priority_ids)
          @priority_ids = priority_ids
        end

        def sort(ids)
          return ids if ids.empty?
          rotated = ids.rotate(Random.rand(ids.length))
          return rotated if (rotated & priority_ids).empty?

          ids.sort do |a, b|
            if priority_ids.include?(a)
              priority_ids.include?(b) ? priority_ids.index(a) <=> priority_ids.index(b) : -1
            elsif priority_ids.include? b
              1
            else
              rotated.index(a) <=> rotated.index(b)
            end
          end
        end
      end
    end
  end
end
