# Selects ids of records to backfill for one "tick"
# The complexity in this are that
#   1) we want to backfill in batches working backwards based on id starting
#      with last batch's last_id
#   2) We want to handle the various cases involving xfiles for models that use xfiles
#      i.e. backfilling / not backfilling to xfiles and records may or may not be in xfiles
#
module Zendesk
  module Stores
    module Backfill
      class IdSelector < StoresBackfillComponent
        # Fetches next batch of ids to process in desc order where id < last_id
        # Select all records that may need to be backfilled, but
        # cannot guarantee it will not select records that definitely need to be
        # backfilled (false positives) -- mostly because of complexity to handling
        # permutations of stores in sql record -- caller must double check stores
        # for loaded record
        #
        def get_ids(audit)
          return [] if audit.done?

          scope = audit.assoc_model_class.
            where(account_id: audit.account_id).
            where("id < #{audit.last_id}")

          scope = no_thumbs_scope(audit, scope)
          scope = stores_scope(audit, scope)

          scope = scope.order('id desc').limit(config.batch_size)

          ids = []
          qtime = Benchmark.realtime { ids = scope.pluck(:id) }
          qtime = (qtime * 1000).round(2)

          logger.info("Ran Id Query",
            model: audit.model_class, account_id: audit.account_id,
            last_id: audit.last_id, query_ms: qtime, id_count: ids.count)
          logger.debug("Fetched Ids",
            model: audit.model_class, account_id: audit.account_id,
            ids_count: ids.count, ids: ids)
          statsd.histogram('id_gets_ms', qtime, tags: %W[model:#{audit.model_class}])
          statsd.increment('id_gets', tags: %W[model:#{audit.model_class}])

          ids
        end

        private

        # Excludes thumbnail records for models that use thumbnails
        def no_thumbs_scope(audit, scope)
          return scope unless audit.model_has_thumbnails?
          scope.where('parent_id is null')
        end

        def xfiles_using_model?(audit)
          config.xfiles_supporting_models.include?(audit.model_class)
        end

        def stores_scope(audit, scope)
          scope = scope.where("stores != ''")

          return scope if audit.region.length != 1

          scope.where.not(stores: store_permutations(audit))
        end

        def store_permutations(audit)
          stores = [
            audit.region.first,
            Technoweenie::AttachmentFu::ScopedStores.linked_store_name(audit.region.first)
          ]

          stores << :xfiles if xfiles_using_model?(audit)

          (1..stores.size).flat_map do |i|
            stores.permutation(i).map { |p| p.join(',') }
          end - ['xfiles']
        end
      end
    end
  end
end
