module Zendesk
  module Stores
    module Backfill
      class StoresBackfillComponent
        attr_reader :config, :logger, :statsd

        def initialize(config)
          @config = config
          @logger = config.logger
          @statsd = config.statsd
        end
      end
    end
  end
end
