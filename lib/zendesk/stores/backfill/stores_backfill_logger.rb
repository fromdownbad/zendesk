module Zendesk
  module Stores
    module Backfill
      class StoresBackfillLogger
        attr_accessor :config

        def initialize(config)
          @config = config
        end

        def initialize_log
          if config.use_json_logger
            @json_logger = json_logger
            Rails.application.config.colorize_logging = false
            Rails.logger.subscribers.clear
            Rails.logger.subscribe(@json_logger)
          end
        end

        def method_missing(m, *args)
          log(m, args)
        end

        def log(severity, msg, tags = {})
          if @json_logger
            @json_logger.send(severity, msg, tags)
          else
            tag_str = tags.map { |k, v| "#{k}:#{v}" }.join(',')
            Rails.logger.send(severity, [msg, tag_str].join(' '))
          end
        end

        private

        def json_logger
          Rails.logger.stdout_logger.instance.tap do |logger|
            logger.append_attributes(
              pid:    $$,
              host:   Socket.gethostname,
              worker_id: config.worker_id,
              dry_run: config.dry_run
            )
            logger.level = Logger.const_get(config.log_level)
          end
        end
      end
    end
  end
end
