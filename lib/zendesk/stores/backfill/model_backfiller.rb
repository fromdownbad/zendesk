module Zendesk
  module Stores
    module Backfill
      class ModelBackfiller < StoresBackfillComponent
        class BackfillError < StandardError
          attr_reader :reason

          def initialize(msg, reason, wrapped: false)
            super(msg)
            @reason = reason
            @wrapped = wrapped
          end

          def wrapped?
            @wrapped
          end
        end

        attr_reader :synchronizer, :id_selector

        def initialize(config, synchronizer, id_selector)
          super(config)
          @synchronizer = synchronizer
          @id_selector = id_selector
          @helpers = config.backfill_classes.map do |kn|
            [kn, Zendesk::Stores::StoresSynchronizerHelper.new(kn.constantize)]
          end.to_h
        end

        # Updates stores for a model's records until it runs out of things
        # to do or stop_check returns true
        def backfill_model(audit, stop_check)
          return Update.new if audit.done? || stop_check.call

          logger.debug("Starting Model", model: audit.model_class, account_id: audit.account_id)
          updates = backfill_model_internal(audit, stop_check)
          logger.info("Model Updated", model: audit.model_class, account_id: audit.account_id, done: audit.done?,
                                       evaluated: updates.evaluated, updated: updates.updated, erred: updates.erred)
          statsd.increment('model_backfills', tags: %W[model:#{audit.model_class}])
          statsd.histogram('model_updates', updates.updated, tags: %W[model:#{audit.model_class}]) if updates.updated > 0
          statsd.increment('completed_audits', tags: %W[model:#{audit.model_class}]) if audit.done?
          updates
        end

        private

        def backfill_model_internal(audit, stop_check)
          update = Update.new

          ids = id_selector.get_ids(audit)

          # in case query took too long...
          return update if stop_check.call

          if ids.empty?
            audit.done!
          else
            last_id = audit.last_id
            ids.each do |id|
              begin
                rec_updated, reason = backfill_rec(audit, id)
                update.increment(rec_updated)
                last_id = id
                if rec_updated
                  statsd.increment('updates', tags: %W[model:#{audit.model_class}])
                else
                  statsd.increment('noops', tags: %W[model:#{audit.model_class} reason:#{reason}])
                end
              rescue BackfillError => e
                update.add_error
                statsd.increment('errors', tags: %W[model:#{audit.model_class} reason:#{e.reason}])

                tags = { error: e.message }

                tags[:backtrace] = e.backtrace if e.wrapped?

                tags.merge!(model_class: audit.model_class,
                            account_id: audit.account_id, record_id: id,
                            last_id: last_id)

                logger.error("Stores Backfill Failed", tags)
                break
              end

              break if stop_check.call
            end

            audit.update_progress(update.evaluated, update.updated, last_id)
          end

          audit.save!
          update
        end

        # Does final checks to see if record should be backfilled, then performs backfill if needed.
        # Returns a tuple of ( updated|not updated, reason-for-not-updating) if it "successfully" processes a record;
        # raises a BackfillError if there's a problem with the record.
        def backfill_rec(audit, id)
          rec = audit.assoc_model_class.find_by_id(id)
          unless rec
            raise BackfillError.new("Missing record: #{audit.model_class} #{id} for account #{audit.account_id}", 'NoRecord')
          end

          reason = check_noop(audit, rec)
          if reason
            return [false, reason]
          end

          if unknown_stores?(audit, rec)
            raise BackfillError.new('Found unknown stores', 'UnknownStore')
          end

          begin
            original_stores = rec.stores.dup

            added_stores, atime = time_and_log(audit.model_class, 'adds') { add_stores(audit, rec, original_stores) }
            removed_stores, rtime = time_and_log(audit.model_class, 'removes') { remove_stores(audit, rec, original_stores) }

            logger.info("Record updated",
              model: audit.model_class,
              account_id: audit.account_id, rec_id: rec.id,
              added_stores: added_stores, removed_stores: removed_stores,
              add_ms: atime, remove_ms: rtime)

            [true, nil]
          rescue StandardError => error
            raise wrap_error(audit, rec, error)
          end
        end

        def time_and_log(model, op)
          delta = []
          time = (Benchmark.realtime { delta = yield } * 1000).round(2)

          unless delta.empty?
            delta.each { |s| statsd.increment(op, tags: %W[model:#{model} store:#{s}]) }
            statsd.histogram("#{op}_ms", time, tags: %W[model:#{model} store_set:#{delta.sort}])
          end

          [delta, time]
        end

        def wrap_error(audit, rec, error)
          wrapped = BackfillError.new(
            "Failed for account: #{audit.account_id}, model: #{audit.model_class}, record: #{rec.id} : #{error.message}",
            error.class.name,
            wrapped: true
          )
          wrapped.set_backtrace error.backtrace
          wrapped
        end

        # Checks various conditions to see if record can/should not be backfilled.  If it should not, then
        # returns a reason based on what condition was discovered.
        #
        def check_noop(audit, rec)
          return 'ok' if correct_stores?(audit, rec)
          return 'Token' if token?(rec)

          if rec.is_a?(Attachment)
            return 'SuspendedTicket' if old_suspended?(rec)

            if rec.ticket_id
              tkt = Ticket.with_deleted { Ticket.find_by_id(rec.ticket_id) }
              if tkt
                return 'Scrubbed' if scrubbed?(tkt)
                return 'UncleanScrubbed' if unclean_scrubbed?(tkt)
              end
            end
          end

          thumbs = rec.respond_to?(:parent_id) ? rec.class.where(parent_id: rec.id).to_a : []
          recs = [rec] + thumbs

          return 'MismatchTags' if mismatch_tags?(recs)
          return 'MissingData' if missing?(audit, recs)

          nil
        end

        # After 14.days, suspended tickets are supposedly not recoverable, per
        # https://support.zendesk.com/hc/en-us/articles/203663246-Understanding-and-managing-suspended-tickets-and-spam#topic_vpr_1sp_nj
        # it looks like for some old suspended-ticket attachments, the s3 data is cleaned up, but the stores does not reflect it.
        def old_suspended?(rec)
          rec.source_type == 'SuspendedTicket' && rec.created_at < (Time.now - 180.days)
        end

        def scrubbed?(tkt)
          tkt.status == 'Deleted' && tkt.subject == 'SCRUBBED'
        end

        # There's a (hopefully) relatively small set of archived tickets that are still living in the ticket tables
        # (soft archived); it's possible these are scrubbed, which, unfortunately, updates the stub subject, and not the
        # ticket subject.
        def unclean_scrubbed?(tkt)
          return false unless tkt.status == 'Archived'
          stub = TicketArchiveStub.find_by_id(tkt.id)
          stub && stub.subject == 'SCRUBBED'
        end

        def add_stores(audit, rec, original_stores)
          if config.dry_run
            adds = @helpers[audit.model_class].preferred_stores
            return adds - rec.stores
          end
          synchronizer.add_to_preferred_stores(rec)
          rec.stores - original_stores
        end

        def remove_stores(audit, rec, original_stores)
          if config.dry_run
            return rec.stores - @helpers[audit.model_class].preferred_stores
          end
          synchronizer.remove_from_unwanted_stores(rec)
          original_stores - rec.stores
        end

        # Skip tokens because they will either be synchronized when saved w/new source-type or the data will
        # be deleted automatically after they expire
        def token?(rec)
          rec.is_a?(Attachment) && rec.source_type == 'Token'
        end

        # Check that rec has stores that match the audit, ignoring xfiles.
        # If a rec has the same stores as the audit's region and is in x-files, then we don't want to backfill that rec
        # just to remove x-files.  BUT, if it's in some other store, or not in one of the region stores, then we
        # do want to backfill the rec, and when it *is* backfilled, we will remove xfiles from the rec at that time
        # (since we're touching it anyway).
        def correct_stores?(audit, rec)
          base_stores = rec.stores - [:xfiles]
          StoresBackfillAudit.correct_stores?(base_stores, audit.region)
        end

        # Check for unrecognized tags in rec stores to avoid
        # removing an unrecognized store caused by some
        # mis-configuration.
        def unknown_stores?(audit, rec)
          helper = @helpers[audit.model_class]
          # fs and cf are known bad stores that syncr will ax, so, we can remove those
          known_stores = helper.known_stores | Zendesk::Stores::StoresSynchronizer.unsupported_stores
          unknown_stores = (rec.stores - known_stores)
          if unknown_stores.empty?
            return false
          end

          logger.error("Found unknown stores",
            model: audit.model_class, account_id: audit.account_id,
            rec_id: rec.id, stores: rec.stores, known_stores: known_stores,
            unknown_stores: unknown_stores)

          unknown_stores.each do |f|
            statsd.increment('unknown_stores', tags: %W[model:#{audit.model_class} store:#{f}])
          end

          true
        end

        # Return true if any rec has different tags from first rec (if any thumbnail has different tags from parent)
        def mismatch_tags?(recs)
          return false if recs.size < 2
          parent_stores = recs[0].stores.sort
          recs.slice(1, recs.size).any? { |r| r.stores.sort != parent_stores }
        end

        # Returns true if any rec's blob is missing from *any* store.
        def missing?(audit, recs)
          helper = @helpers[audit.model_class]
          recs.any? { |r| (r.stores & helper.known_stores).any? { |s| !helper.exists?(r, s) } }
        end
      end
    end
  end
end
