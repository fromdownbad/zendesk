# Fetches audits for an account and a set of model-classes,
# while also handling initializing the region for each model-class,
# which is annoying enough to be pulled out.
#
module Zendesk
  module Stores
    module Backfill
      class RegionHelper < StoresBackfillComponent
        # Returns array of audits for the account (one per entry in model_to_region);
        # array is sorted to same order as given model_classes (mainly so Attachment is last).
        def account_audits(account_id)
          audits = StoresBackfillAudit.account_audits(account_id, model_to_region)

          audits.sort do |a, b|
            config.backfill_classes.index(a.model_class) <=> config.backfill_classes.index(b.model_class)
          end
        end

        private

        def model_to_region
          unless @model_to_region
            @model_to_region = config.backfill_classes.map do |model_class|
              [model_class, region_for_model_class(model_class)]
            end.to_h
            logger.info("Model to Region Map", model_to_region: @model_to_region)
          end
          @model_to_region
        end

        # determines the (local) region value for a particular model-type,
        # which is based on the preferred stores for the pod.
        def region_for_model_class(model_class)
          klass = model_class.constantize
          helper = Zendesk::Stores::StoresSynchronizerHelper.new(klass)
          helper.preferred_stores
        end
      end
    end
  end
end
