module Zendesk
  module Stores
    module Backfill
      class StoresBackfillConfig
        # Name of this feature for exodus-handshake API
        DISABLE_FEATURE = 'syncattachments_enabled'.freeze

        BACKFILL_CLASSES = [
          BrandLogo.name,
          Favicon.name,
          HeaderLogo.name,
          MobileLogo.name,
          Photo.name,
          Attachment.name
        ].freeze

        # Maximum time we can go w/o acknowleding a disable request from exodus
        MAX_TIME_FOR_EXODUS_ACK = 4.minutes

        attr_accessor :options, :logger,
          :worker_number, :worker_count,
          :namespace, :log_level, :use_json_logger,
          :backfill_classes, :dry_run, :loop, :nothing_to_do_nap,
          :xfiles_supporting_models,
          :batch_size, :disable_ack_interval,
          :shard_allowance, :account_allowance,
          :regions_for_priority, :priority_weight,
          :disable_nap

        DEFAULT_OPTS = {
          worker_number: 1, # What worker number is this; needs to be in range 1 to worker_count
          worker_count: 1, # How many workers are there -- important for this worker to know its lane
          namespace: 'backfill.stores', # metric namespace
          log_level: 'INFO',
          use_json_logger: false, # use the json logger
          backfill_classes: BACKFILL_CLASSES, # Classes of records to backfill
          dry_run: false, # Is it safe?
          loop: false, # Run once across shards or loop continuously
          nothing_to_do_nap: 1.minutes, # How long to sleep if there's nothing to do in the pod
          xfiles_supporting_models: [Attachment.name], # models using xfiles (used to determine store filter in query)
          batch_size: 10000, # Max number of recs we will process for a model (Attachments) during an account's turn
          disable_ack_interval: 60.seconds, # How often the disabled-account helper will check/ack for new disable requests
          shard_allowance: 30.minutes, # How long will we backfill one shard before moving forward
          account_allowance: 4.minutes, # How long will we backfill one account before moving forward
          regions_for_priority: ["emea", "fra1"], # Pod regions that can have priority accounts
          priority_weight: 0.75, # Value between 0 and 1 to give priority accounts additional time (0 => equal time)
          disable_nap: 1.minute # how long to sleep if kill-switch is on
        }.freeze

        def initialize(in_opts = {})
          options = DEFAULT_OPTS.merge(in_opts)

          ensure_within_exodus_ack(options, :account_allowance)
          ensure_within_exodus_ack(options, :nothing_to_do_nap)
          ensure_within_exodus_ack(options, :disable_nap)

          if options[:worker_number] < 1 || options[:worker_number] > options[:worker_count]
            raise ArgumentError, "Invalid worker number #{options[:worker_number]} -- should be between 1 and worker count (#{options[:worker_count]})"
          end

          if options[:priority_weight] < 0 || options[:priority_weight] > 1
            bounded = options[:priority_weight] < 0 ? 0 : 1
            Rails.logger.warn("StoresBackfill: Invalid priority weight, using #{bounded}")
            options[:priority_weight] = bounded
          end

          begin
            options[:backfill_classes].each(&:constantize)
          rescue NameError => e
            raise ArgumentError, "Non-existant class found! #{e.message}"
          end

          options[:log_level] = options[:log_level].upcase

          # Sends each option value to corresponding setter:
          @options = options
          options.each { |k, v| send("#{k}=", v) }

          @logger = StoresBackfillLogger.new(self)
        end

        def statsd
          @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [namespace], tags: %W[worker:#{worker_id}])
        end

        def kill_switch_on?
          Arturo.feature_enabled_for_pod?(:disable_sync_attachments, Zendesk::Configuration.fetch(:pod_id))
        end

        def worker_id
          @worker_id ||= "#{worker_number}_#{worker_count}"
        end

        def using_priority?
          @using_priority ||= in_priority_region && priority_weight > 0
        end

        def only_priority?
          @only_priority ||= in_priority_region && priority_weight >= 1.0
        end

        def weighted_shard_allowance(has_priority_accounts)
          return shard_allowance unless using_priority?
          has_priority_accounts ? shard_allowance : non_priority_shard_allowance
        end

        def weighted_account_allowance(priority_account)
          return account_allowance unless using_priority?
          priority_account ? account_allowance : non_priority_account_allowance
        end

        private

        # Ensure durations that cause job to not acknowledge exodus disable
        # requests are within the SLA.
        def ensure_within_exodus_ack(options, key)
          if options[key] > MAX_TIME_FOR_EXODUS_ACK
            options[key] = MAX_TIME_FOR_EXODUS_ACK
            Rails.logger.warn("StoresBackfill: To maintain exodus SLA, cannot allow more than #{MAX_TIME_FOR_EXODUS_ACK} for #{key}")
          end
        end

        def in_priority_region
          regions_for_priority.include?(
            Zendesk::Configuration.dig(:pods, Zendesk::Configuration.fetch(:pod_id), :region)
          )
        end

        # Shard allowance for shards with no priority accounts, reduced from full shard allowance.
        def non_priority_shard_allowance
          @non_priority_shard_allowance ||= non_priority_allowance(shard_allowance)
        end

        # Account allowance for non-priority accounts, which is calculated as a reduction of "full" account-allowance
        # (which is given to priority accounts).
        def non_priority_account_allowance
          @non_priority_account_allowance ||= non_priority_allowance(account_allowance)
        end

        # Calculates allowance for non-priority accounts/shard as a reduction of "full" allowance given to
        # shards with priority accounts / priority accounts.
        def non_priority_allowance(allowance)
          allowance * (1 - priority_weight)
        end
      end
    end
  end
end
