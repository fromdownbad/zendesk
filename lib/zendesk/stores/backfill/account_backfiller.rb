module Zendesk
  module Stores
    module Backfill
      class AccountBackfiller < StoresBackfillComponent
        attr_reader :region_helper, :model_backfiller

        def initialize(config, region_helper, model_backfiller)
          super(config)
          @region_helper = region_helper
          @model_backfiller = model_backfiller
        end

        # Backfills record stores' for an account for a set amount of time
        # or until there's nothing left to do for the account.
        # Returns a single progress representing total evaluated/updated
        # for the account
        def backfill_account(account_id, priority_account = false)
          account_allowance = config.weighted_account_allowance(priority_account)
          stop_time = current_time + account_allowance
          stop_check = lambda { current_time > stop_time }

          audits = account_audits(account_id)
          return Update.new if audits.empty?

          logger.debug("Backfilling Account", account_id: account_id, priority: priority_account)

          updates = nil
          time = Benchmark.realtime do
            updates = audits.
              map { |audit| backfill_model(audit, stop_check) }.
              sum(Update.new)
          end

          time = (time * 1000).round(2)

          logger.info("Account Updated", account_id: account_id, priority: priority_account,
                                         evaluated: updates.evaluated, updated: updates.updated, erred: updates.erred, ms: time)

          statsd.increment('account_backfills', tags: %W[priority:#{priority_account}])
          statsd.histogram('account_updates', updates.updated, tags: %W[priority:#{priority_account}]) if updates.updated > 0
          statsd.histogram('account_ms', time, tags: %W[priority:#{priority_account}])
          updates
        end

        private

        def account_audits(account_id)
          region_helper.account_audits(account_id).reject(&:done?)
        end

        def backfill_model(audit, stop_check)
          model_backfiller.backfill_model(audit, stop_check)
        end

        def current_time
          Process.clock_gettime(Process::CLOCK_MONOTONIC)
        end
      end
    end
  end
end
