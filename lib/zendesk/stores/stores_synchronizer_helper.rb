# Helper to retrieve attachment-backend-related config;
# this can go away if/once we backport the methods defined in ClassMethods
# in lib/zendesk/attachments/stores.rb to atta-fu
module Zendesk
  module Stores
    class StoresSynchronizerHelper
      def initialize(recclass)
        @attachment_backends = recclass.attachment_backends
      end

      # Stores in which blob should be saved
      def preferred_stores
        (primary_stores | secondary_stores)
      end

      # Recognized stores
      def known_stores
        attachment_backends.keys
      end

      # Known S3 stores
      def s3_stores
        attachment_backends.select { |_, option| option[:options][:storage] == :s3 }.keys
      end

      # Returns the preferred S3 store for a record, given the record and the available regional S3 stores.
      def preferred_s3_store(rec, regional_stores)
        available_stores = rec.stores & s3_stores
        return nil if available_stores.empty?
        return available_stores.first if available_stores.length == 1 # This is true for a significant % of the data (all non-attachments, some attachments)

        # First look for a class (model) preferred store (these are probably also regional stores)
        preferred_available = preferred_stores & available_stores
        return preferred_available.first if preferred_available.present?

        # Next look for a regional store:
        region_available = (regional_stores & available_stores)
        return region_available.first if region_available.present?

        # If all that fails, use an available store:
        available_stores.first
      end

      # Checks if record is tagged with and blob exists for an atta-fu backend store
      # You can skip the pre-emptive check for the store's tag in 'stores' by passing check_tag: false.
      def exists?(rec, store, check_tag: true)
        return false unless rec.respond_to?(store)
        return false if check_tag && !rec.stores.include?(store)
        rec_store = rec.send(store)
        if rec_store.respond_to?(:exists?)
          rec_store.exists?
        elsif rec_store.is_a? Technoweenie::AttachmentFu::Backends::S3Backend
          exists_s3? rec_store
        elsif rec_store.is_a? Technoweenie::AttachmentFu::Backends::FileSystemBackend
          exists_filesystem? rec_store
        else
          # should be unreachable as we've exhausted the (currently) defined backend types
          raise StandardError, "Unknown backend store #{store} for #{rec.class.name} #{rec.id} with stores #{rec.stores}"
        end
      end

      private

      attr_reader :attachment_backends

      # The stores to which the blob will initially be uploaded
      def primary_stores
        return [attachment_backends.keys.first] if attachment_backends.length == 1

        attachment_backends.select { |_, option| option[:options][:default] }.keys
      end

      # store(s) flagged as secondary in options (uploaded into during sync)
      def secondary_stores
        attachment_backends.select { |_, option| option[:options][:secondary] }.keys
      end

      def exists_s3?(rec_store)
        rec_store.bucket.object(rec_store.full_filename).exists?
      end

      def exists_filesystem?(rec_store)
        File.exist? rec_store.full_filename
      end
    end
  end
end
