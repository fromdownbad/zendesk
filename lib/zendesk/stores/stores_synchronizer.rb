module Zendesk
  module Stores
    class StoresSynchronizeError < StandardError; end
    class NotInPreferredStoreError < StoresSynchronizeError; end
    class MissingFromPreferredError < StoresSynchronizeError; end

    class StoresSynchronizer
      # Updates given object to add preferred store (based on atta-fu)
      # in which it is not already stored.
      #
      # This will only add stores, it will never remove a store from the rec.
      #
      def add_to_preferred_stores(rec)
        if rec.thumbnail?
          Rails.logger.warn("Attempt to add stores to thumbnail #{rec.class.name} #{rec.id}")
          return
        end

        helper = StoresSynchronizerHelper.new(rec.class)

        # already saved stores + all the stores it should have based on back-ends
        stores_to_add = helper.preferred_stores - rec.stores

        # Bail if noop
        return if stores_to_add.empty?

        Rails.logger.info("Adding store(s) #{stores_to_add} to #{rec.class.name} #{rec.id}")
        rec.stores = rec.stores | stores_to_add
        rec.save!
      end

      # Updates given object's by removing any non-preferred stores in which it
      # currently resides.
      #
      # This will not remove any stores if the rec's stores do not include
      # at least one preferred store, or if it detects that the rec does not
      # exist in one (or more) of it's tagged preferred stores.
      #
      # This will not remove unrecognized stores (not known to attachment_fu)
      # from the rec, unless the unrecognizable store is also an unsupported store
      # (defined below).
      #
      def remove_from_unwanted_stores(rec)
        if rec.thumbnail?
          Rails.logger.warn("Attempt to remove stores from thumbnail #{rec.class.name} #{rec.id}")
          return
        end

        helper = StoresSynchronizerHelper.new(rec.class)

        # Check that the rec is tagged with a preferred store:
        rec_preferred = (rec.stores & helper.preferred_stores)
        raise NotInPreferredStoreError, "Cannot remove stores from #{rec.class.name} #{rec.id} -- Not in a preferred store" if rec_preferred.empty?

        # Check that the record actually exists in the preferred store(s) in which it is tagged
        missing = rec_preferred.reject { |store| helper.exists?(rec, store) }
        raise MissingFromPreferredError, "Record #{rec.class.name} #{rec.id} is missing from preferred stores: #{missing}" unless missing.empty?

        # Do not remove unknown stores, unless they are also unsupported
        removable = helper.known_stores | StoresSynchronizer.unsupported_stores
        unwanted = (rec.stores - helper.preferred_stores) & removable

        return if unwanted.empty?

        Rails.logger.info("Removing store(s) #{unwanted} from #{rec.class.name} #{rec.id} (stores: #{rec.stores})")
        rec.stores = rec.stores - unwanted
        rec.save!
      end

      # Stores that are not recognized by attachment_fu, but which may appear
      # as a tag because they were once supported.
      #
      # Removing these stores will only update the store field, there's no blob
      # removal (because atta_fu ignores the tag).
      #
      # 'fs' (FileSystem) is a special case; for dev and test, it is supported,
      # but not elsewhere (master/staging/prod).
      #
      def self.unsupported_stores
        ret = [:cf, :cfeu, :xfiles]
        ret << :fs unless Rails.env.development? || Rails.env.test?
        ret
      end
    end
  end
end
