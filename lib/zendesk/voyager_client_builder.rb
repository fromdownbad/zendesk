require 'zendesk_voyager_api_client'

module Zendesk
  module VoyagerClientBuilder
    class << self
      def build(account)
        Voyager::API::Client.new(account, options(account))
      end

      def location(account)
        options = options(account)
        "#{options[:host]}:#{options[:port]}"
      end

      private

      def options(account)
        if !KUBERNETES && Arturo.feature_enabled_for?(:voyager_use_service_proxy, account)
          host, port = Zendesk::KubernetesServiceProxyBuilder.service_url('voyager', account.pod_id).split(':', 2)

          {
            host: host,
            port: port,
            timeout: 30
          }
        else
          {
            host: Zendesk::Configuration.dig!(:zendesk_voyager, :host),
            port: Integer(Zendesk::Configuration.dig!(:zendesk_voyager, :port)),
            timeout: Integer(Zendesk::Configuration.dig!(:zendesk_voyager, :timeout))
          }
        end
      end
    end
  end
end
