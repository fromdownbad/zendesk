# This is used to convert values for ticket field conditions,
# from incoming API values to DB representation and viceversa.
#
# We want to keep the API presentation of values consistent with how we
# present values of ticket custom field values in the `tickets` endpoint.
#
# This module is included in TicketField and contains special conversion
# for certain types and returns same value for other types.
#
# > FieldCheckbox.new.present_condition_value('1')
# => true
# > FieldCheckbox.new.condition_value_to_db(false)
# => '0'
#
# > FieldPriority.new.present_condition_value('3')
# => 'high'
# > FieldPriority.new.condition_value_to_db('high')
# => '3'
#
# > FieldTicketType.new.present_condition_value('2')
# => 'incident'
# > FieldTicketType.new.condition_value_to_db('incident')
# => '2'
#
# > FieldTagger.new.present_condition_value('anything')
# => 'anything'
# > FieldTagger.new.condition_value_to_db('anything')
# => 'anything'
#
module Zendesk
  module TicketFieldConditionConversion
    class DefaultConverter
      def present(value)
        value
      end

      def to_db(value)
        value
      end
    end

    class PriorityConverter
      include Api::V2::Tickets::AttributeMappings

      def present(value)
        PRIORITY_MAP[value]
      end

      def to_db(value)
        PriorityType.find(value) || value
      end
    end

    class TicketTypeConverter
      include Api::V2::Tickets::AttributeMappings

      def present(value)
        TYPE_MAP[value]
      end

      def to_db(value)
        TicketType.find(value) || value
      end
    end

    class CheckboxConverter
      include Zendesk::CustomField::Checkbox

      def present(value)
        TRUTHY_VALUES.include?(value)
      end

      def to_db(value)
        normalize_value(value)
      end
    end

    CONVERTER_MAPPING = {
      "FieldCheckbox" => CheckboxConverter,
      "FieldPriority" => PriorityConverter,
      "FieldTicketType" => TicketTypeConverter
    }.freeze

    def present_condition_value(value)
      condition_converter.present(value)
    end

    def condition_value_to_db(value)
      condition_converter.to_db(value).to_s
    end

    private

    def condition_converter
      @condition_converter ||= (CONVERTER_MAPPING[self.class.name] || DefaultConverter).new
    end
  end
end
