require 'soap/rpc/driver'

class Zendesk::SoapBase < ::SOAP::RPC::Driver
  def initialize(endpoint_url = nil)
    endpoint_url ||= self.class::DefaultEndpointUrl
    super(endpoint_url, nil)
    self.mapping_registry = self.class.parent::DefaultMappingRegistry::EncodedRegistry
    self.literal_mapping_registry = self.class.parent::DefaultMappingRegistry::LiteralRegistry
    init_methods
  end

  private

  def init_methods
    self.class::Methods.each do |definitions|
      opt = definitions.last
      if opt[:request_style] == :document
        add_document_operation(*definitions)
      else
        add_rpc_operation(*definitions)
        qname = definitions[0]
        name = definitions[2]
        if qname.name != name && qname.name.capitalize == name.capitalize
          ::SOAP::Mapping.define_singleton_method(self, qname.name) do |*arg|
            __send__(name, *arg)
          end
        end
      end
    end
  end
end
