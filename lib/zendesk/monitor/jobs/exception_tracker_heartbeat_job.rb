module Zendesk::Monitor::Jobs
  class ExceptionTrackerHeartbeatJob
    FINGERPRINT = 'ba80747a220595d5132a5fe5a6cef258dc400276'.freeze
    EXCEPTION   = "Pod #{Zendesk::Configuration[:pod_id]} reporting for duty".freeze
    MESSAGE     = 'Sending heartbeat to ExceptionTracker'.freeze

    HeartbeatException = Class.new(StandardError)

    extend ZendeskJob::Resque::BaseJob

    def self.work
      ZendeskExceptions::Logger.record(
        HeartbeatException.new(EXCEPTION),
        location:    self,
        message:     MESSAGE,
        fingerprint: FINGERPRINT
      )
    end

    def self.args_to_log
      {}
    end
  end
end
