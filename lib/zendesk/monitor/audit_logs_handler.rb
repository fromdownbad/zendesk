module Zendesk
  module Monitor
    class AuditLogsHandler
      CHANGE_POSITIONS = {old: 0, new: 1}.freeze

      attr_reader :cia_event

      def initialize(cia_event)
        @cia_event = cia_event
      end

      def add_attribute_changes(event_params)
        if event_params[:action] != 'create' && changes = event_params[:app_setting_changes].presence
          create_attribute_change(
            event_params,
            name:      'settings',
            old_value: setting_values_for(changes, :old),
            new_value: setting_values_for(changes, :new)
          )
        elsif changes = event_params[:changes].presence
          changes.each do |attribute, changed_values|
            create_attribute_change(
              event_params,
              name:      attribute,
              old_value: changed_values[0],
              new_value: changed_values[1]
            )
          end
        end
      end

      private

      def create_attribute_change(event_params, attribute_change)
        change_params = event_params.slice(:account_id, :source_id, :source_type)
        change_params.merge!(
          event:          cia_event,
          old_value:      attribute_change.fetch(:old_value),
          new_value:      attribute_change.fetch(:new_value),
          attribute_name: attribute_change.fetch(:name)
        )
        cia_event.attribute_changes.build(change_params)
      end

      def setting_values_for(all_changes, kind)
        value = Hash[all_changes.map { |key, changes| [key, changes.fetch(CHANGE_POSITIONS.fetch(kind))] }]

        # remove longest values if we do not have enough space to store everything
        CIA::AttributeChange.serialize_for_storage(value) do |values|
          # CIA uses JSON.dump for storage (shorter for multibyte strings)
          values.delete(values.sort_by { |v| JSON.dump(v).bytesize }.last.first)
          values
        end
      end
    end
  end
end
