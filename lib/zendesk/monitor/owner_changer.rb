module Zendesk
  module Monitor
    class OwnerChanger
      attr_reader :success, :message

      def initialize(account, new_owner, demote_role_id = 0)
        @account = account
        @owner = @account.owner
        @demote_role_id = demote_role_id
        @success = true
        @new_owner = new_owner
      end

      def change_owner
        return unless new_owner_is_not_current_owner
        demote_owner
        change_ownership
        if @success
          suspend_previous_owner
        else
          restore_owner
        end
      rescue StandardError => se
        @success = false
        @message = 'An error occured during ownership change'
        ZendeskExceptions::Logger.record(se, location: self, message: @message, fingerprint: 'a7e363992a41c6810e1d8ff4b4824fbd1afd448a')
      end

      private

      def new_owner_is_not_current_owner
        if @owner == @new_owner
          @success = false
          @message = 'Update of the account failed; New Owner is already the Current Owner'
        end
        @success
      end

      def demote_owner
        @owner.roles = @demote_role_id # 0 for end user

        unless Zendesk::SupportUsers::User.new(@owner).save(validate: false) # Too much validations are preventing us from doing this
          @success = false
          @message = "Can't downgrade current owner"
        end
      end

      def change_ownership
        return unless @success

        promote_new_owner

        return unless @success
        @account.owner_id = @new_owner.id
        @success = @account.save
        @message = 'Update of the account failed' unless @success
      end

      def promote_new_owner
        if @new_owner.is_admin?
          # Make sure to update the owner to flush existing caches
          @new_owner.touch_without_callbacks
        else
          @new_owner.roles = 2 # admin
          @success = Zendesk::SupportUsers::User.new(@new_owner).save
          @message = 'Unable to promote new owner to admin role' unless @success
        end
      end

      def restore_owner
        @owner.roles = 2 # admin
        unless Zendesk::SupportUsers::User.new(@owner).save(validate: false)
          @message = "#{@message}; Owner is not an admin anymore"
        end

        if @account.owner_id != @owner.id
          @account.owner = @owner
          unless @account.save
            @message = "#{@message}; Couldn't restore the owner to its previous status"
          end
        end
      end

      def suspend_previous_owner
        return unless @owner.roles == 0 # end-user
        @owner.suspended = true
        unless Zendesk::SupportUsers::User.new(@owner).save(validate: false)
          @success = false
          @message = 'Owner was changed but previous owner was not suspended'
        end
      end
    end
  end
end
