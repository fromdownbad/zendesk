require 'tilt'

module Zendesk::CommonJS
  class Compiler < Tilt::Template
    COMMONJS_DIR = File.expand_path('js', File.dirname(__FILE__))
    WRAPPER = "%s.define({%s:function(exports, require, module){%s;}});\n".freeze

    protected

    def prepare
      @namespace = "this.require";
    end

    def evaluate(scope, _locals)
      if commonjs_module?(scope)
        scope.require_asset 'commonjs'
        WRAPPER % [namespace, module_name(scope), data]
      else
        data
      end
    end

    private

    attr_reader :namespace

    def commonjs_module?(scope)
      # does the filename have a .module extension? (foo.module.js, for example)
      File.basename(scope.pathname).scan(/\.[^.]+/).include?(".module")
    end

    def module_name(scope)
      scope.logical_path.
        gsub(/^\.?\//, ''). # Remove relative paths
        chomp('.module').   # Remove module ext
        inspect
    end
  end
end
