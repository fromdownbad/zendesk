module Zendesk
  class LogRotator
    class << self
      # Don't do anything if something (e.g. Unicorn) is already listenting to USR1.
      def try_listen
        return if exist?
        listen
      end

      protected

      def signal_queue
        @signal_queue ||= []
      end

      def listen
        trap("USR1") { signal_queue << 1 }

        @thread = Thread.start do
          loop do
            consume_queue
            sleep(1.second)
          end
        end
      end

      def pause
        thread.kill
      end

      def stop
        thread.kill
        cleanup
        consume_queue
      end

      attr_reader :thread

      def consume_queue
        if signal_queue.shift
          puts "USR1 signal received. Reopening log files" unless Rails.env.test?
          Unicorn::Util.reopen_logs
        end
      end

      def cleanup
        trap("USR1", "DEFAULT")
      end

      def exist?
        callback = trap("USR1", "IGNORE")
        callback != "DEFAULT"
      ensure
        trap("USR1", callback)
      end
    end
  end
end
