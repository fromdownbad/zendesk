require 'aws-sdk-s3'

module Zendesk
  class S3ReportingStore
    BUCKET = "zendesk-reporting#{"-#{Rails.env}" unless Rails.env.production?}".freeze

    def initialize(report_filename)
      @report_filename = report_filename
    end

    def store!(value)
      connection.client.put_object(bucket: BUCKET, key: @report_filename, body: value)
    rescue Aws::S3::Errors::NoSuchBucket
      connection.client.create_bucket(bucket: BUCKET)
      retry
    end

    def fetch
      # presigned urls are only in resource https://github.com/aws/aws-sdk-ruby/issues/1113
      object = connection.bucket(BUCKET).object(@report_filename)
      Zendesk::S3ReportingStore::Report.new(
        filename:       @report_filename,
        url:            object.presigned_url(:get),
        content_length: object.content_length.to_s,
        content_type:   object.content_type
      )
    rescue Aws::S3::Errors::NotFound
      nil
    end

    private

    def connection
      @connection ||= Aws::S3::Resource.new(
        access_key_id: Zendesk::Configuration[:aws_access_key],
        secret_access_key: Zendesk::Configuration[:aws_secret_key],
        region: 'us-east-1'
      )
    end

    class Report
      FIELDS = [
        :filename,
        :url,
        :content_length,
        :content_type
      ].freeze

      FIELDS.each { |field| attr_accessor field }

      def initialize(fields = {})
        FIELDS.each { |field| instance_variable_set("@#{field}", fields[field]) }
      end
    end
  end
end
