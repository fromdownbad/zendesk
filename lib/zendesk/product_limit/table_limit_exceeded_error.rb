module Zendesk
  module ProductLimit
    class TableLimitExceededError < StandardError
    end
  end
end
