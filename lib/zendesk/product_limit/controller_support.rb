module Zendesk
  module ProductLimit
    module ControllerSupport
      extend ActiveSupport::Concern

      included do
        rescue_from Zendesk::ProductLimit::TableLimitExceededError do |error|
          render json: {
            errors: [{
              code: 'ProductLimitExceeded',
              title: error.message
            }]
          }, status: :forbidden
        end
      end
    end
  end
end
