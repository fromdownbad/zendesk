require "active_support/concern"

module Zendesk
  module BlockMonitorChanges
    extend ActiveSupport::Concern

    included do
      before_action :block_monitor_changes
    end

    private

    def block_monitor_changes
      return unless Rails.env.production? || Rails.env.test?
      return unless is_assuming_user_from_monitor?
      return unless read_only_session?
      return if ["HEAD", "OPTIONS", "GET"].include?(request.method.upcase)
      Rails.logger.error("Preventing monitor assuming user account change: #{request.shared_session[:assuming_monitor_user_id]}")
      render plain: "Your change has been blocked because assuming monitor users are not allowed to make changes.", status: :bad_request
    end

    def read_only_session?
      !!request.shared_session[:assuming_monitor_read_only]
    end
  end
end
