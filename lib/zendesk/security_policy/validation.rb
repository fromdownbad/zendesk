module Zendesk::SecurityPolicy
  module Validation
    class Length
      attr_accessor :minimum

      def initialize(options)
        @minimum = options[:minimum]
      end

      def message
        I18n.t('activerecord.errors.models.user.attributes.password.too_short', count: minimum)
      end

      def deny?(value)
        minimum > value.size
      end
    end

    class MaxLength
      attr_accessor :maximum

      def initialize(options)
        @maximum = options[:maximum]
      end

      def message
        I18n.t('txt.security_policy.requirements.max_length')
      end

      def deny?(value)
        maximum < value.size
      end
    end

    class MaxCharsNumbersInSequence
      attr_accessor :max_consecutive_letters_and_numbers

      def message
        I18n.t(
          'activerecord.errors.models.user.attributes.password.max_sequential_letters_and_numbers',
          number: max_consecutive_letters_and_numbers
        )
      end

      def deny?(value)
        return false if value.length <= max_consecutive_letters_and_numbers

        sequence = ""

        value.each_char.with_index do |char, _i|
          previous_char_equal_to_current = sequence[-1] && sequence[-1].ord + 1 == char.ord

          if sequence.empty? || previous_char_equal_to_current
            sequence << char
          else
            sequence = char
          end

          unless is_alpha_or_numeric(char)
            sequence = ""
          end

          return true if sequence.length > max_consecutive_letters_and_numbers
        end

        false
      end

      private

      def is_alpha_or_numeric(char) # rubocop:disable Naming/PredicateName
        char =~ /[A-Za-z0-9]/
      end
    end

    class DifferenceWithUsername
      attr_accessor :username

      def message
        I18n.t('activerecord.errors.models.user.attributes.password.difference_email_address')
      end

      def deny?(value)
        username == value
      end
    end

    class AlphanumericComplexity
      def message
        I18n.t('activerecord.errors.models.user.attributes.password.complex')
      end

      def deny?(value)
        /[A-Z]/ !~ value ||
          /[a-z]/ !~ value ||
          /[0-9]/ !~ value
      end
    end

    class NumberComplexity
      def message
        I18n.t('activerecord.errors.models.user.attributes.password.numbers')
      end

      def deny?(value)
        /[0-9]/ !~ value
      end
    end

    class MixedComplexity
      def message
        I18n.t('activerecord.errors.models.user.attributes.password.mixed')
      end

      def deny?(value)
        /[A-Z]/ !~ value ||
        /[a-z]/ !~ value
      end
    end

    class SpecialCharacter
      def message
        I18n.t('activerecord.errors.models.user.attributes.password.special')
      end

      def deny?(value)
        /[^a-z0-9]/i !~ value
      end
    end

    class LocalPartFromEmail
      attr_accessor :email

      def message
        I18n.t('activerecord.errors.models.user.attributes.password.local_part', local_part: local_part)
      end

      def deny?(value)
        value.include?(local_part)
      end

      private

      def local_part
        return unless email
        @local_part ||= email.split("@").first
      end
    end

    class UniqueHistory
      attr_accessor :history, :history_limit

      def message
        I18n.t('activerecord.errors.models.user.attributes.password.unique', count: history_limit)
      end

      def deny?(value)
        return false if value.nil?
        history.any? { |previous_value| value.duplicate?(previous_value) }
      end
    end
  end
end
