module Zendesk::SecurityPolicy
  module Enforcement
    def self.included(base)
      base.class_attribute :validations
      base.validations = Hash.new { |hash, key| hash[key] = Set.new }
    end

    attr_accessor :account, :user, :password
    attr_writer :unencrypted_password

    def unencrypted_password
      @unencrypted_password || password.try(:unencrypted).to_s
    end

    def validations_in_words
      validations_in_words = {}
      validations.keys.each do |attribute|
        validations_in_words[attribute] = find_validations(attribute).map(&:message)
      end

      password_messages = validations_in_words.values_at(:password, :unencrypted_password)
      if password_messages.any?
        validations_in_words[:password] = password_messages.flatten.compact
        validations_in_words.delete(:unencrypted_password)
      end

      validations_in_words
    end

    def requirements_in_words
      validations_in_words
    end

    def password_requirements
      (requirements_in_words[:password] << ::I18n.t('txt.security_policy.attempts_before_lockout')).compact
    end

    def enforce(user)
      self.user     = user
      self.password = user.password
      valid?
      add_errors_to(user)
    end

    def valid?
      errors.clear
      run_validations
      errors.empty?
    end

    def errors
      @errors ||= Hash.new { |hash, key| hash[key] = [] }
    end

    def run_validations
      [:unencrypted_password, :password].each do |attribute_name|
        attribute_value = __send__(attribute_name)

        find_validations(attribute_name).each do |validation|
          if validation.deny?(attribute_value)
            errors[attribute_name] << validation.message
          end
        end
      end
    end

    protected

    def find_validations(key)
      validations[key].map do |validation|
        validation.is_a?(Symbol) ? __send__(validation) : validation.new
      end
    end

    def add_errors_to(record)
      errors.each do |name, messages|
        name = :password if name == :unencrypted_password
        messages.each { |message| record.errors.add(name, message.strip) }
      end
    end
  end
end
