module Zendesk
  module CurrentAccountIntegration
    def restricted_to_account(account)
      Thread.current[:zendesk_account] = account
      yield
    ensure
      Thread.current[:zendesk_account] = nil
    end

    def current_account
      Thread.current[:zendesk_account]
    end
  end

  extend CurrentAccountIntegration
end
