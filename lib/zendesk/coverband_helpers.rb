require 'simplecov'
require 'coverband'
require_relative 'extensions/simplecov/lines_classifier'
SimpleCov::LinesClassifier.prepend SimpleCov::LinesClassifier::LineEncoding

module Zendesk
  module CoverbandHelpers
    def self.initializer(enable_stats = true)
      if ENV['ENABLE_COVERBAND'] == 'true'
        Coverband.configure do |config|
          config.root = Rails.root.to_s

          if defined? Redis
            config.redis = Zendesk::RedisStore.client
          end
          # don't want to use redis, store to file system ;)
          # config.coverage_file           = './tmp/coverband_coverage.json'

          config.root_paths = ['^/data/zendesk/releases/\d+-\d+-[0-9a-f]+/', '^/data/zendesk/current/'] # /app/ is needed for heroku deployments
          # regex paths can help if you are seeing files duplicated for each capistrano deployment release
          # config.root_paths = ['/server/apps/my_app/releases/\d+/']
          config.ignore = ['vendor']
          # Since rails and other frameworks lazy load code. I have found it is bad to allow
          # initial requests to record with coverband. This ignores first 100 requests
          # NOTE: If you are using a threaded webserver (example: Puma) this will ignore requests for each thread
          config.startup_delay = Rails.env.production? ? 100 : 0
          # Percentage of requests recorded
          config.percentage = Rails.env.production? ? ENV.fetch('COVERBAND_PERCENTAGE', 0).to_f : 100.0
          config.logger = Rails.logger

          # stats help you collect how often you are sampling requests and other info
          if enable_stats && defined?(Zendesk::StatsD)
            config.stats = Zendesk::StatsD::Client.new(namespace: 'classic')
          end

          # config options false, true, or 'debug'. Always use false in production
          # true and debug can give helpful and interesting code usage information
          # they both increase the performance overhead of the gem a little.
          # they can also help with initially debugging the installation.
          config.verbose = false
        end
      end
    end

    def self.yaml_report
      Coverband::Reporters::Base.report(Coverband.configuration.store).to_yaml
    end

    def self.rails_root
      File.expand_path('../../..', __FILE__)
    end

    def self.simplecov_result(coverband_data)
      # set root to show files if user has simplecov profiles
      SimpleCov.root(Rails.root.to_s)
      # add in files never hit in coverband
      SimpleCov.track_files "#{Rails.root}/{app,lib,config}/**/*.{rb,haml,erb,slim}"

      # still apply coverband filters
      report_files = SimpleCov.add_not_loaded_files(coverband_data)
      filtered_report_files = {}
      report_files.each_pair do |file, data|
        next if Coverband.configuration.ignore.any? { |i| file.match(i) }
        filtered_report_files[file] = data
      end

      SimpleCov::Result.new(filtered_report_files)
    end
  end
end
