require 'zendesk/radar_factory'

module Zendesk
  module Channels
    class CommentManager
      # Managers in this namespace are intended to be the only entry point from
      # zendesk_channels to classic.
      # This manager updates Comments in response to external changes.

      attr_reader :account, :comment

      def initialize(account_id, comment_id)
        @account = Account.find(account_id)
        @comment = Comment.find(comment_id)
      end

      def report_channelback_error(error_params)
        audit = @comment.audit

        event = ChannelBackFailedEvent.new(value: { description: error_params[:message] })
        event.audit = audit
        event.save!

        author = comment.author

        ::I18n.with_locale(author.translation_locale) do
          ::RadarFactory.create_radar_notification(
            account,
            "growl/#{author.id}"
          ).send(error_params[:notification_type], error_params[:message])
        end
      end
    end
  end
end
