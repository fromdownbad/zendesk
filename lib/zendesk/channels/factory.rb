# We're preparing Channels code to be moved into the Channels repo, and ultimately into
# a separate service.  As an intermediate step, we want to break direct dependencies
# between Channels code and Classic models.  For example, the Group model lives in Classic,
# so Channels code should not directly use the Group model (e.g. Group.find(123)).
# We'll move references into this Factory, and then we can reimplement the Factory
# using API calls to Classic as appropriate.

module Zendesk
  module Channels
    class Factory
      PAGE_LIMIT = 15

      def self.user_group_ids(user)
        user.groups.map(&:id)
      end

      def self.account_facebook_page_limit(account_id)
        account = get_account(account_id)

        [account.settings.monitored_facebook_page_limit.to_i, PAGE_LIMIT].max
      end

      def self.account_facebook_page_limited?(account_id)
        ::Facebook::Page.where(account_id: account_id).active.count(:all) >= account_facebook_page_limit(account_id)
      end

      def self.get_account(account_id)
        ::Account.find(account_id)
      end
    end
  end
end
