module Zendesk
  module Channels
    class TicketConverter
      attr_reader :account, :params

      def initialize(account, params)
        @account = account
        @params = params
      end

      def convert
        create_ticket? ? create_ticket : update_ticket
      end

      def author
        @author ||= user_mapper.find_or_create_user
      end

      def create_ticket?
        ticket = existing_ticket

        return true unless ticket
        return true if ticket.closed?

        false
      end

      private

      def existing_ticket
        return unless params[:ticket_id]
        @existing_ticket ||= account.tickets.find_by_id(params[:ticket_id])
      end

      def create_ticket
        if existing_ticket &&
            existing_ticket.closed? &&
            (!params.key?(:create_followup_tickets) || params[:create_followup_tickets])
          params[:via_followup_source_id] = existing_ticket.nice_id
        end

        if params[:selected_params]
          ticket_params = HashWithIndifferentAccess.new(ticket: params[:selected_params])
          initializer = Zendesk::Tickets::Initializer.new(account, User.find(User.system_user_id), ticket_params)
          ticket = initializer.ticket
          ticket.subject     = params[:comment_text] if ticket.subject.blank?
          ticket.description = params[:comment_text] if ticket.description.blank?
        else
          ticket = account.tickets.new(
            subject: params[:comment_text],
            description: params[:comment_text]
          )
        end
        ticket.assign_attributes(requester: author, via_id: params[:via_id])
        ticket.via_reference_id = params[:source_id]
        ticket.nice_id = params[:nice_id]
        if params[:html_body]
          # When we save the ticket, if the auto_tagging account setting is on, the comment.rb code will
          # try to resolve the body from html_body. However, we can't rely on the save to set the account
          # for the comment in this scenario as the code to resolve the html_body will check the account
          # So we set it here to break the circular dependency.
          ticket.comment.account = account
          ticket.comment.html_body = params[:html_body]
        end
        ticket.comment.via_id = params[:via_id]
        ticket.comment.channel_source_id = params[:source_id]
        if account.has_multiple_active_brands? && selected_brand_active?(params[:brand_id])
          ticket.brand_id = params[:brand_id]
        end

        if params[:via_followup_source_id]
          ticket.via_followup_source_id = params[:via_followup_source_id]
          ticket.set_followup_source(author)
        end

        set_fields(ticket, params[:ticket_fields])

        if params[:attachments].present?
          ticket.comment.uploads = params[:attachments]
        end

        # AnyChannels Internal Notes for CT-3252 - needs channels v1.353.0
        ticket.comment.is_public = public_comment?(params)

        save_ticket(ticket)

        [ticket, ticket.comments.first]
      end

      def public_comment?(params)
        if allow_internal_note?
          return !params[:internal_note] unless params[:internal_note].nil?
        end

        true
      end

      # Check if an integration is allowed to create an internal note
      def allow_internal_note?
        Arturo.feature_enabled_for?(:channels_allow_any_channels_internal_notes, account)
      end

      # Adds a comment to a ticket
      def update_ticket
        create_comment(existing_ticket)
      end

      def create_comment(ticket)
        comment_params = {
          via_id: params[:via_id],
          channel_back: false,
          channel_source_id: params[:source_id]
        }

        if params[:html_body]
          comment_params[:html_body] = params[:html_body]
        else
          comment_params[:body] = params[:comment_text]
        end

        if params[:attachments].present?
          comment_params[:uploads] = params[:attachments]
        end

        comment_params[:is_public] = public_comment?(params)

        comment = ticket.add_comment(comment_params)

        # CC the comment author on the ticket unless they're already associated with the ticket
        save_ticket(ticket)

        [ticket, comment]
      end

      def save_ticket(ticket)
        ticket.will_be_saved_by(author)
        ticket.audit.via_id = params[:via_id]
        ticket.save!
      end

      def channel
        case params[:via_id].to_i
        when ViaType.TWITTER, ViaType.TWITTER_DM, ViaType.TWITTER_FAVORITE
          "twitter"
        when ViaType.FACEBOOK_POST, ViaType.FACEBOOK_MESSAGE
          "facebook"
        when ViaType.ANY_CHANNEL
          "any_channel"
        end
      end

      def user_params
        params.merge(type: channel)
      end

      def user_mapper
        @user_mapper ||= ::Channels::UserMapper.new(account, Hashie::Mash.new(user_params))
      end

      def selected_brand_active?(brand_id)
        return false if brand_id.nil?

        # This saves the call to do find on Brand as brands is already retrieved at this point
        selected_brand = account.brands.select do |brand|
          brand.id == brand_id
        end.first
        selected_brand.active?
      end

      def set_fields(ticket, field_values)
        return unless field_values

        field_values.each do |value_item|
          field_name = value_item['id']
          field_value = value_item['value']
          field_name_for_mapping = field_name.respond_to?(:downcase) ? field_name.downcase : field_name
          case field_name_for_mapping
          when 'subject'
            ticket.subject = field_value
          when 'description'
            ticket.description = field_value
          when 'status'
            ticket.status_id = (Ticket::ACCESSIBLE_STATUSES & [StatusType.find(field_value)]).first || ticket.status_id
          when 'type'
            ticket.ticket_type_id = TicketType.find(field_value) || ticket.ticket_type_id
          when 'priority'
            ticket.priority_id = PriorityType.find(field_value) || ticket.priority_id
          when 'group'
            ticket.group = ticket.account.groups.where(id: field_value).first || ticket.group
          when 'assignee'
            ticket.assignee = ticket.account.users.where(id: field_value).first || ticket.assignee
          when 'tags'
            ticket.additional_tags = field_value
          else
            set_custom_field(ticket, field_name, field_value)
          end
        end
      end

      def set_custom_field(ticket, field_name, field_value)
        # Look for the custom field by ID
        field = ticket.account.ticket_fields.where(id: field_name).first
        # If not found by ID, look by name
        field = ticket.account.ticket_fields.where(title: field_name).first unless field
        # If not found, we're done
        return unless field

        # Note: this is intended to be equivalent to the following (but calls public methods):
        #   ticket.send(:set_ticket_field_entry, field, field_value)
        ticket.fields = [[field.id, field_value]]
      end
    end
  end
end
