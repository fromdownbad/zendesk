module Zendesk
  module Channels
    class TicketManager
      # Managers in this namespace are intended to be the only entry point from
      # zendesk_channels to classic
      # To update ticket after channel receives a response from external systems

      attr_reader :account, :decoration_data

      def initialize(account, decoration_data)
        @account = account
        @decoration_data = decoration_data
      end

      # Ticket params needs to be a hash in the following format:
      # {
      #   :user => {
      #     :id        => The external (Channel) ID of the author,
      #     :name      => The author's name,
      #     :photo_url => The author's photo URL
      #   },
      #   :comment_text => The text of the new comment_id
      #   :via_id       => The Via type that will be used to create/update
      #   :ticket_id    => The ID of the ticket to be updated if one exists (optional)
      # }
      def convert_incoming_comment(params)
        # We get a parameter called 'source_access_token', but other code (Bouncer::UserMapper) expects it to be called
        # 'access_token'.  We cannot change the name of the incoming param to 'access_token', since that parameter name
        # is used by authentication, so we'll just do the conversion here.
        params[:access_token] = params.delete(:source_access_token)

        converter = ::Zendesk::Channels::TicketConverter.new(account, params)
        validate_converter(converter)

        # Initialize a nice ID before starting the transaction
        params[:nice_id] = Ticket.new(account: account).set_nice_id if converter.create_ticket?

        ticket, comment = create_ticket_and_decoration(converter)
        [ticket.id, ticket.nice_id, comment.id]
      end

      def create_ticket_and_decoration(converter)
        ticket = comment = nil

        running_time = Benchmark.ms do
          Ticket.transaction do
            ticket, comment = converter.convert
            create_decoration(comment)
          end
        end
        statsd_client.timing('create_ticket_and_decoration', running_time, tags: default_datadog_tags)

        [ticket, comment]
      rescue StandardError => e
        # Convert Exceptions (e.g. Ticket could not be saved) to PermanentError so they'll get handled properly by
        # import code.  E.g. the import won't be retried, and the exception info will get recorded into
        # incoming_channels_conversions.
        raise ::Channels::Errors::PermanentError, e
      end

      def decorate_outgoing_comment(comment_id)
        comment = Comment.where(id: comment_id, account_id: @account.id).first
        if comment && comment.ticket.present?
          create_decoration(comment)
        else
          Rails.logger.info("No comment found for id #{comment_id}, account_id: #{@account.id}")
        end
        comment
      end

      def create_decoration(comment)
        Rails.logger.info("Creating event decoration for account: #{account.subdomain}/#{account.id}, " \
          "comment: #{comment.id}, ticket: #{comment.ticket_id}")
        comment.create_event_decoration!(
          account: account,
          ticket_id: comment.ticket_id,
          data: decoration_data
        )
      end

      # Adds a comment in the parent ticket which displays a link to the child ticket.
      # Note: The conversion we received is a "child" ticket - this method works on the parent ticket.
      # Currently, only the Facebook comments as tickets feature use this method.
      def self.create_link_comment(account, child_ticket_comment_text, parent_ticket_id, child_ticket_id, decoration_data = nil)
        # We only want to add one comment per child ticket so we use the existence of the link to determine that

        return if TicketLink.exists?(
          account_id: account.id,
          source_id: child_ticket_id,
          target_id: parent_ticket_id,
          link_type: [TicketLink::FACEBOOK, TicketLink::TWITTER]
        )

        target_ticket = account.tickets.find_by_id(parent_ticket_id)
        source_ticket = account.tickets.find_by_id(child_ticket_id)
        link_type = source_ticket && source_ticket.via =~ /Twitter/i ? TicketLink::TWITTER : TicketLink::FACEBOOK

        return if target_ticket.nil? || source_ticket.nil? || target_ticket.inoperable?

        # The string key has the via type in it so this link to child logic
        # can potentially be reused by other channel
        comment_text = ::I18n.t(
          'txt.facebook_post.link_to_child_ticket',
          child_ticket_nice_id: source_ticket.nice_id,
          channel: link_type,
          child_ticket_requester_name: source_ticket.requester.name,
          child_ticket_comment_text: child_ticket_comment_text,
          locale: account.translation_locale.locale
        )

        comment = target_ticket.add_comment(
          is_public:    true,
          channel_back: source_ticket && source_ticket.via =~ /Twitter/i ? true : false,
          html_body:    comment_text
        )

        target_ticket.will_be_saved_by(target_ticket.requester)

        Ticket.transaction do
          source_ticket.target_links.build(source: source_ticket, target: target_ticket, link_type: link_type).save!
          target_ticket.save!
          unless decoration_data.nil?
            comment.create_event_decoration!(
              account: account,
              ticket_id: comment.ticket_id,
              data: decoration_data
            )
          end
        end
      end

      private

      def statsd_client
        @client ||= Zendesk::StatsD.client(namespace: %w[channels ticket_manager])
      end

      def default_datadog_tags
        @default_datadog_tags ||= ["zendesk_version:#{GIT_HEAD_TAG}"]
      end

      def validate_converter(converter)
        # Pre-initialize the author prior to creating the ticket and event decoration
        return unless converter.author.try(:suspended?)
        fail ::Channels::Errors::PermanentError, "Author #{converter.author.id} in account #{account.id} " \
          "(#{account.subdomain}) is suspended, aborting incoming conversion"
      end
    end
  end
end
