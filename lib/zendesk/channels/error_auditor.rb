module Zendesk
  module Channels
    class ErrorAuditor
      attr_reader :audit, :message, :permanent, :flag, :flag_options, :ticket

      FACEBOOK_ERROR_FLAGS = [
        EventFlagType.FACEBOOK_CLIENT_ERROR_200,
        EventFlagType.FACEBOOK_CLIENT_ERROR_230
      ].freeze

      ATTACHMENT_ERROR_FLAGS = [
        Zendesk::Types::AttachmentErrorType.DOWNLOAD_REQUEST_TIMED_OUT,
        Zendesk::Types::AttachmentErrorType.DOWNLOAD_NETWORK_FAILURE,
        Zendesk::Types::AttachmentErrorType.DOWNLOAD_HTTP_ERROR,
        Zendesk::Types::AttachmentErrorType.MUST_BE_HTTPS,
        Zendesk::Types::AttachmentErrorType.MUST_BE_SOURCE_HOST,
        Zendesk::Types::AttachmentErrorType.TOO_MANY_ATTACHMENTS,
        Zendesk::Types::AttachmentErrorType.FILE_TOO_LARGE,
        Zendesk::Types::AttachmentErrorType.UPLOAD_REQUEST_TIMED_OUT,
        Zendesk::Types::AttachmentErrorType.UPLOAD_NETWORK_FAILURE,
        Zendesk::Types::AttachmentErrorType.UPLOAD_HTTP_ERROR,
        Zendesk::Types::AttachmentErrorType.UPLOAD_UNKNOWN_ERROR,
        Zendesk::Types::AttachmentErrorType.UNSAFE_URL
      ].freeze

      CONVERSION_ERROR_FLAGS = [
        Zendesk::Types::ConversionErrorType.TOO_MANY_RESOURCES_FOR_THREAD
      ].freeze

      def initialize(params)
        event         = Event.find(params[:event_id])
        @audit        = event.audit
        @ticket       = event.ticket
        @message      = params[:message]
        @permanent    = params[:permanent]
        @flag         = params[:flag]
        @flag_options = params[:flag_options] || {}
      end

      # This method is currently used inside the zendesk_channels gem.
      # This should eventually be invoked from an API endpoint.
      # Make sure to test zendesk_channels if any changes are made here.
      def run
        ticket.deactivate_event_decoration if permanent
        if valid_flag?
          flags_options = {}
          flags_options[flag] = flag_options
          audit.flag!([flag], flags_options)
        end

        audit.events << Error.new(
          ticket: ticket,
          value: message
        )
        audit.save!
      end

      private

      def valid_flag?
        facebook_error? || attachment_error? || conversion_error?
      end

      def facebook_error?
        flag_options['type'] == 'facebook_error' && FACEBOOK_ERROR_FLAGS.member?(flag)
      end

      def attachment_error?
        flag_options['type'] == 'attachment_error' && ATTACHMENT_ERROR_FLAGS.member?(flag)
      end

      def conversion_error?
        flag_options['type'] == 'conversion_error' && CONVERSION_ERROR_FLAGS.member?(flag)
      end
    end
  end
end
