module Zendesk
  module Channels
    class Utilities
      # Will return "false" in Classic, but "true" in zendesk_channels_service
      def self.in_channels_service?
        false
      end

      def self.run_channels_job?(account)
        # If we are in the channels service AND we're supposed to run jobs in the channels service, run the job.
        # Similarly, if we are NOT in the channels service (i.e. we're in Classic) and we're NOT supposed to run jobs
        # in the channels service (i.e. we're suppose to run them in Classic), then run the job.
        in_channels_service? == Arturo.feature_enabled_for?(:channels_jobs_run_in_channels_service, account)
      end
    end
  end
end
