module Zendesk
  module ForumSanitizer
    def self.included(base)
      base.before_save :sanitize_on_write, if: :body_or_title_changed?
    end

    private

    def sanitize_on_write
      self.sanitize_on_display = false
      self.body  = body.to_s.sanitize
      self.title = title.to_s.sanitize if respond_to?(:title)
    end

    def body_or_title_changed?
      body_changed? || (respond_to?(:title) && title_changed?)
    end
  end
end
