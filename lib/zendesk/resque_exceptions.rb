module Zendesk
  # Failure backend for the exception trackers
  class ResqueExceptions < ::Resque::Failure::Base
    def save
      klass = payload['class'] || 'Unknown job'
      ZendeskExceptions::Logger.record( # rubocop:disable Lint/ZendeskExceptions
        exception,
        location: self,
        message: klass,
        metadata: {
          worker: worker,
          queue: queue,
          payload: payload
        }
      )
    end
  end
end
