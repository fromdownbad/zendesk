module Zendesk
  module Devices
    module ControllerMixin
      def self.included(base)
        if RAILS4
          base.hide_action *instance_methods
        end
      end

      protected

      def current_device
        device_manager.current_device
      end

      def register_device_activity
        current_device.register_activity!
      end

      def device_manager
        @device_manager ||= Zendesk::Devices::Manager.new(current_account, permanent_cookies, device_owner)
      end

      private

      def device_owner
        session[:original_user_id].present? ? original_user : current_user
      end
    end
  end
end
