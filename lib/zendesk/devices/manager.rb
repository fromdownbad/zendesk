module Zendesk
  module Devices
    class Manager
      attr_reader :account, :store, :user, :strategy, :params

      STORE_KEY = :device_tokens

      def initialize(account, store, user, strategy = nil, params = {})
        @account = account
        @store = store
        @user = user
        @strategy = strategy
        @params = params
        store[STORE_KEY] ||= {}
      end

      def token
        user ? store[STORE_KEY][user.id.to_s] : nil
      end

      def token=(value)
        store_data = store[STORE_KEY]
        store_data[user.id.to_s] = value
        store[STORE_KEY] = store_data
      end

      def track(notify: true)
        return if strategy && !strategy.device_tracking_enabled?

        device = update_existing_device || create_new_device(notify: notify)
        self.token = device.token

        device.register_activity!
        device
      end

      def invalid_device?
        account && has_token? && !existing_device
      end

      def update_existing_device
        if device = existing_device
          device.update_attributes(device.oauth_token_id ? mobile_oauth_device_params : device_params)
        end

        device
      end

      def current_device
        existing_device || NilDevice.new
      end

      def existing_device
        existing_token = params[:token].nil? ? token : params[:token]

        if strategy && strategy.is_a?(Zendesk::OAuth::Warden::TokenStrategy)
          account.mobile_devices.find_by_user_id_and_oauth_token_id(user.id, existing_token) || account.devices.find_by_user_id_and_token(user.id, existing_token)
        else
          account.devices.find_by_user_id_and_token(user.id, existing_token)
        end
      end

      def revoke_device
        @token = nil
        store.delete STORE_KEY
      end

      def has_token? # rubocop:disable Naming/PredicateName
        !token.blank?
      end

      private

      def create_new_device(notify:)
        device = devices_of_type_for_user.create!(device_params.merge(account: account))

        if notify && user.device_notification?
          ::Devices::Mailer.deliver_new_device_notification(device)
        end

        device
      end

      def device_params
        params.slice(:ip, :user_agent, :token, :name)
      end

      def mobile_oauth_device_params
        params.slice(:ip, :user_agent, :name)
      end

      def devices_of_type_for_user
        params[:type] == :mobile ? user.mobile_devices : user.devices
      end
    end
  end
end
