module Zendesk::MailInlineImagesControllerSupport
  private

  S3_INBOUND_MAIL_ATTACHMENTS_DIRECTORY = 'inbound-mail-attachments'.freeze

  def retrieve_remote_file
    RemoteFiles::CONFIGURATIONS.values.each do |config|
      next unless config.configured?

      config.stores.each do |store|
        next unless store.options[:directory].include?(S3_INBOUND_MAIL_ATTACHMENTS_DIRECTORY)

        begin
          base = base_url(store)

          next unless base.present?

          if remote_file = config.file_from_url("#{base}/#{encoded_filename}")
            remote_file.retrieve!

            return remote_file
          end
        rescue => e # rubocop:disable Style/RescueStandardError
          Rails.logger.error(e.message)
          Rails.logger.error(e.backtrace)
        end
      end
    end

    false
  end

  def base_url(store)
    return unless store.is_a?(RemoteFiles::FogStore)

    connection = store.connection
    scheme = connection.instance_variable_get(:@scheme)
    host = connection.instance_variable_get(:@host)
    bucket = store.directory.key

    "#{scheme}://#{host}/#{bucket}"
  end

  def remote_file_filename(remote_file)
    remote_file.identifier.split('/').last
  end

  def encoded_filename
    @encoded_filename ||= URI.encode_www_form_component("#{current_account.id}/#{message_id}/#{filename}")
  end

  # Sample format: 'attachment_1_square.png'
  def filename
    params[:filename]
  end

  # Sample format: '7aa47a71-fe7c-49be-824e-d9bab93761fe'
  def message_id
    params[:message_id]
  end
end
