module Zendesk
  module WebLifecycle
    class << self
      def close_connections
        if Rails.cache.respond_to?(:dalli)
          Rails.logger.info "Closing memcached dalli connections"
          Rails.cache.dalli.close
        else
          Rails.logger.info "Closing memcached basecamp connections"
          Rails.cache.instance_variable_get(:@cache).quit
        end

        redis_connections = []
        redis_connections << Redis.current
        redis_connections << Resque.redis
        redis_connections << Zendesk::RedisStore.client

        redis_connections.compact.each do |redis|
          redis.client.disconnect
        end
      end

      def prevent_memcache_options_leak
        Rails.cache.instance_variable_set(:@cache, Rails.cache.instance_variable_get(:@cache).clone)
      end
    end

    def close_connections
      super

      Zendesk::WebLifecycle.close_connections
    end

    # Do not call on every request, lookup isn't cheap
    def rss
      `ps -o rss= -p #{Process.pid}`.to_i / 1024
    end
    module_function :rss
    public :rss

    def before_first_fork(server, _worker)
      server.logger.info "Master memory usage before_first_fork: #{rss} MB"

      warmup(server)

      Rails.logger.flush
    end

    def before_fork(server, _worker)
      server.logger.info "Master memory usage before_fork: #{rss} MB"
    end
    module_function :before_fork
    public :before_fork

    def after_fork(server, _worker)
      unless Rails.cache.respond_to?(:dalli)
        Zendesk::WebLifecycle.prevent_memcache_options_leak
      end

      server.logger.info "Resetting dalli connections"
      Rails.cache.reset if ENV['USE_DALLI_ELASTICACHE'] == 'true'
      Kasket.cache.reset if ENV['KASKET_USE_ELASTICACHE'] == 'true'
      Prop.cache.reset if ENV['PROP_USE_ELASTICACHE'] == 'true'

      server.logger.info "Worker memory usage after_fork: #{rss} MB"
    end
    module_function :after_fork
    public :after_fork

    # Make the master serve a couple requests to load the app.
    # This makes the first request after forking a lot faster.
    def warmup(server)
      server.logger.info "Sending warmup requests"
      # Basic ping
      Rack::MockRequest.new(server.app).get("/z/ping?warmup=1")
      # Loads V2 code and runs through the full stack
      subdomain = "pod#{Zendesk::Configuration.fetch(:pod_id)}"
      Rack::MockRequest.new(server.app).get("https://#{subdomain}.#{Zendesk::Configuration[:host]}/api/v2/users/me.json?warmup=1")
      # Stops early, but uses views
      Rack::MockRequest.new(server.app).get("/agent?warmup=1")
    end
    module_function :warmup
    public :warmup
  end
end
