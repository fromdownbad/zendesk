module Zendesk
  module AutoTranslation
    class Locale
      prepend Zendesk::LocalePresentation

      attr_reader :locale

      def initialize(locale)
        @locale = locale
      end
    end
  end
end
