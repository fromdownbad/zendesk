# Supports "no-cache" directive for sensitive endpoints as required by iSEC audits
module Zendesk
  module PrivateCaching
    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods
      def only_use_private_caches(*actions)
        after_action :set_no_cache_directive, only: actions
      end

      def dont_use_any_caches(*actions)
        after_action :set_no_store_directive, only: actions
      end
    end

    protected

    # Rails doesn't believe we actually want to do this.
    # It replaces the Cache-Control header when there's an exact match on 'no-cache', so add a whitespace.
    def set_no_cache_directive
      headers['Cache-Control'] = 'no-cache, private'
    end

    def set_no_store_directive
      headers['Cache-Control'] = 'no-store'
    end
  end
end
