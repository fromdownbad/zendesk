require 'zendesk/configuration'

module Zendesk::Gdpr
  class Configuration < Zendesk::Configuration
    # Override setup to allow us to set the settings variable with a block in the initializer
    def self.setup(*args)
      super(*args)
      @settings = @settings.dup.merge(yield).freeze
    end

    # We don't actually read this file, but other methods remove the extension to get the
    # namespace of the configuration.
    def self.config_file
      'gdpr.yml'
    end
  end
end
