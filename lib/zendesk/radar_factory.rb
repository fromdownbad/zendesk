require 'radar_client_rb'
require 'zendesk_radar_client'
require 'radar_notification'

module RadarFactory
  def create_radar_client(account)
    subdomain = account.subdomain
    create_radar_redis_client(subdomain)
  end

  def create_exodus_radar_client(subdomain, shard_id)
    Radar::Client.build_exodus_client(subdomain, shard_id)
  end

  def create_radar_notification(account, scope)
    client = create_radar_client(account)
    ::Radar::Notification.new(client, account.subdomain, scope)
  end

  module_function :create_radar_client, :create_exodus_radar_client, :create_radar_notification

  private

  def create_radar_redis_client(subdomain)
    ::Radar::Client.build_client(subdomain)
  end

  module_function :create_radar_redis_client
end
