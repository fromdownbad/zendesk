module Zendesk
  module Serialization
    module AuditSerialization
      V2_FINDER_INCLUDES = { events: {}, author: Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES }.freeze

      def as_json(options = {})
        options[:version] ||= 1
        if options[:version] == 1
          json = super
          json[:value_previous] = client
          if json["author"] # rails 2 uses the :only on all subsequent models
            json["author"].slice!("name", "created_at")
          end
        elsif options[:version] == 2
          json = { id: id, ticket_id: ticket.nice_id, created_at: created_at, via: serialized_via_object, client: client }
          json[:author] = author.as_json(version: 2)
          json[:events] = events.map { |e| e.as_json(version: 2) }
        end

        json
      end

      def serialization_options(options = {})
        output = super(options)

        case options[:version]
        when 1
          output[:only] += [:value, :value_reference, :via_reference_id]
          output[:include] = {}
          output[:include][:author] ||= {}
          if options[:filter] == :audits
            output[:include][:events] ||= {}
          end
        when 2
          output[:methods] ||= []
          output[:methods] += [:client]
        end

        output
      end
    end
  end
end
