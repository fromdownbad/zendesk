module Zendesk
  module Serialization
    module CategorySerialization
      def as_json(_options = {})
        { id: id, name: name, description: description, position: position }
      end

      def to_xml(options = {})
        super serialization_options.merge(options)
      end

      def serialization_options(_options = {})
        { only: [:id, :name, :description, :position] }
      end
    end
  end
end
