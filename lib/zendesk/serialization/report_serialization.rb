module Zendesk
  module Serialization
    module ReportSerialization
      # Note - the serialized representation of a report is not about the model but about the
      # data it generates. It is hence not really an API entity.

      def as_json(options = {})
        options[:version] ||= 1

        if options[:version] == 1
          data    = table[:data]
          legends = table[:legends].map { |legend| legend[:name] }
          hash    = { title: title, sets: [] }

          legends.map do |legend|
            set = { legend: legend, values: [] }
            data.keys.sort.each do |key|
              set[:values] << { date: key.strftime("%Y-%m-%d"), data: data[key][legend].to_i }
            end
            hash[:sets] << set
          end

          hash
        end
      end

      def to_xml(options = {})
        options[:version] ||= 1

        if options[:version] == 1
          super(v1_xml_options)
        end
      end

      private

      def v1_xml_options
        proc = proc do |options|
          data    = table[:data]
          legends = table[:legends].map { |legend| legend[:name] }

          builder = options[:builder]
          builder.title(title)
          builder.sets do
            legends.each do |legend|
              builder.set do
                builder.legend(legend)
                builder.values do
                  data.keys.sort.each do |key|
                    builder.value(date: key.strftime("%Y-%m-%d"), data: data[key][legend].to_i)
                  end
                end
              end
            end
          end
        end

        { only: [], procs: [proc]}
      end
    end
  end
end
