module Zendesk
  module Serialization
    module PostSerialization
      SERIALIZATION_INCLUDES = { attachments: {} }.freeze
      FINDER_INCLUDES        = { attachments: {} }.freeze

      def self.included(base)
        base.extend(Collection)
      end

      module Collection
        def serializable_collection(collection)
          CollectionPresenter.new(collection)
        end

        class CollectionPresenter
          def initialize(collection)
            @collection = collection
          end

          def to_xml(options = {})
            @collection.to_xml({ root: 'posts' }.merge(options))
          end

          def as_json(options = {})
            { posts: @collection, count: @collection.total_entries }.as_json(options)
          end
        end
      end

      def to_xml(options = {})
        super serialization_options.merge(options)
      end

      def as_json(options = {})
        super serialization_options.merge(options)
      end

      def serialization_options(options = {})
        { except: [:sanitize_on_display, :deleted_at, :flag_type_id], include: (options[:include] || SERIALIZATION_INCLUDES) }
      end
    end
  end
end
