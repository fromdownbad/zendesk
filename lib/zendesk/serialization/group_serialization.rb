module Zendesk
  module Serialization
    module GroupSerialization
      def to_xml(options = {})
        super({ except: [:user_id, :group_id, :account_id, :default, :is_public] }.merge(options))
      end

      def as_json(options = {})
        options[:version] ||= 1

        if options[:version] == 1
          super({ except: [:user_id, :group_id, :account_id, :default, :is_public] }.merge(options))
        elsif options[:version] == 2
          { id: id, name: name, created_at: created_at, updated_at: updated_at }
        end
      end
    end
  end
end
