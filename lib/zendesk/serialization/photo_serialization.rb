module Zendesk
  module Serialization
    module PhotoSerialization
      def as_json(options = {})
        options[:version] ||= 1

        if options[:version] == 1
          super(options.merge(methods: [:public_filename]))
        elsif options[:version] == 2
          # Using account in the below will given an N+1, possibly cache account.url in the options hash
          hash = {
            id: id, name: filename, url: account_url(options) + public_filename,
            content_type: content_type, size: size, created_at: created_at.as_json
          }

          hash[:thumbnails] = thumbnails.map { |thumbnail| thumbnail.as_json(options) } if thumbnails.present?
          hash
        end
      end

      private

      def account_url(options)
        options[:cache] ||= {}
        options[:cache][:account_url] ||= account.url(mapped: false, ssl: true)
        options[:cache][:account_url]
      end
    end
  end
end
