module Zendesk
  module Serialization
    module CollaborationSerialization
      def ticket_nice_id
        ticket.nice_id
      end

      def to_xml(options = {})
        super(serialization_options.merge(options))
      end

      def as_json(_options = {})
        { id: id, user_id: user_id, created_at: created_at, ticket_id: ticket_nice_id }
      end

      def serialization_options(_options = {})
        proc = proc { |options| options[:builder].tag!("ticket-id", ticket_nice_id) }
        { only: [:id, :user_id, :created_at], procs: [proc] }
      end
    end
  end
end
