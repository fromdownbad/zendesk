module Zendesk
  module Serialization
    module UploadSerialization
      def as_json(options = {})
        options[:version] ||= 1
        hash = { token: token.value }

        if options[:version] == 1
          hash[:upload] = { id: attachment.id }
          hash[:all]    = token.attachments.map(&:id)
        elsif options[:version] == 2
          hash[:upload] = summary(attachment)
          hash[:all]    = token.attachments.map { |attachment| summary(attachment) }
        end

        hash
      end

      # Reminder: An upload is not an AR instance, hence the custom XML generation
      def to_xml(options = {})
        output = ""

        builder = options[:builder] || Builder::XmlMarkup.new(target: output, indent: 2)
        builder.uploads(token: token.value) do
          builder.attachments do
            token.attachments.where(parent_id: nil).each do |attachment|
              builder.attachment(attachment.id)
            end
          end
        end

        output
      end

      private

      def summary(attachment)
        {
          id: attachment.id,
          content_type: attachment.content_type,
          filename: attachment.display_filename,
          url: attachment.url
        }
      end
    end
  end
end
