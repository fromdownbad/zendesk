module Zendesk
  module Serialization
    module TicketSharingEventSerialization
      def serialization_options(options = {})
        output = super(options)

        case options[:version]
        when 1
          output[:only] += [:via_reference_id, :value, :value_previous, :value_previous, :value_reference]
        when 2
          output[:methods] ||= []
          output[:methods] << :agreement_id
        end

        output
      end
    end
  end
end
