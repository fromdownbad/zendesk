module Zendesk
  module Serialization
    module RoleSerialization
      def as_json(_options = {})
        {
          id: id,
          name: name,
          description: description
        }
      end
    end
  end
end
