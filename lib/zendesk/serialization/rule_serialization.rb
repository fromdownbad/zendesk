module Zendesk
  module Serialization
    module RuleSerialization
      def to_xml(options = {})
        super serialization_options.merge(options)
      end

      def as_json(options = {})
        super serialization_options.merge(options)
      end

      def serialization_options(_options = {})
        attributes_to_include = [:id, :per_page, :position, :title, :is_active]
        { only: attributes_to_include }
      end
    end
  end
end
