module Zendesk
  module Serialization
    module ExternalLinkCollectionSerialization
      def to_json(options = {})
        as_json(options).to_json(options)
      end

      def as_json(options = {})
        map { |val| val.as_json(default_options(options)) }
      end

      def to_xml(options = {})
        default = default_options(root: "linkings")
        super default.merge(options)
      end

      protected

      def default_options(options)
        { include: [:external_link] }.merge(options)
      end
    end
  end
end
