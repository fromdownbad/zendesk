module Zendesk::Serialization
  module Search
    module ResultsSerialization
      V2_VERSION = 2

      V2_SERIALIZATION_OPTIONS = {

        ticket: {
          version: V2_VERSION,
          include: {
            requester: {
              only: [:id, :name],
              methods: []
            }
          }
        },

        entry: {
           version: V2_VERSION,
           include: {
             forum: {
               only: [:id, :name]
               },
             submitter: {
                 only: [:id, :name]
               }
           }
        },

        user: {
            version: V2_VERSION,
            include: {
              organization: {
                only: [:id, :name]
              }
            }
         }

      }.freeze

      def as_json(options = {})
        version = options[:version]
        v2_api = version.present? && version == V2_VERSION

        json = map do |result|
          if v2_api
            result_type = Zendesk::Search::Elasticsearch::Search.class_to_type[result.class.name]
            v2_api_options = V2_SERIALIZATION_OPTIONS[result_type] || {}
            result_json = result.as_json(v2_api_options)
            { result: result_json, result_type: result_type }
          else
            result.as_json(options)
          end
        end

        if v2_api
          { payload: json, count: total_entries, facets: facets }
        else
          json
        end
      end

      def to_xml(options = {})
        super({ root: 'records', count: total_entries }.merge(options))
      end

      def for_category
        { total_entries: total_entries,
          # rendering posts needlessly is wasteful, but we need forum.display_type
          entries: map { |e| e.as_json(include: [], methods: [:forum_type, :excerpt, :relative_path]) }}
      end
    end
  end
end
