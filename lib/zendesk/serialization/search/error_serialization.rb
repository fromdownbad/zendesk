module Zendesk::Serialization
  module Search
    module ErrorSerialization
      def as_json(options = {})
        { error: message }.as_json(options)
      end

      def to_xml(options = {})
        raise ArgumentError, 'Options not supported' if options.present?

        "<error><![CDATA[#{message.gsub(/\]\]>/, "]]]]><![CDATA[<")}]]></error>"
      end
    end
  end
end
