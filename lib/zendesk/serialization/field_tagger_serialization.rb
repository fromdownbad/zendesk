module Zendesk
  module Serialization
    module FieldTaggerSerialization
      def as_json(options = {})
        serialization_options = {include: {custom_field_options: {only: [:id, :name, :value]}}}

        super options.merge(serialization_options)
      end

      def to_xml(options = {})
        serialization_options = {include: {custom_field_options: {only: [:name, :value]}}}

        super options.merge(serialization_options)
      end
    end
  end
end
