module Zendesk
  module Serialization
    module OrganizationSerialization
      SERIALIZATION_FINDER_INCLUDES = { taggings: {} }.freeze
      SERIALIZATION_ONLY            = [:name, :created_at, :updated_at, :details, :default, :is_shared_comments, :notes, :group_id, :id, :is_shared, :external_id].freeze
      SERIALIZATION_METHODS         = [:current_tags].freeze
      SERIALIZATION_METHODS_V2      = [:tags].freeze

      def to_xml(options = {})
        super(serialization_options(options))
      end

      def as_json(options = {})
        options[:version] ||= 1
        super(serialization_options(options))
      end

      def serialization_options(options)
        if account.has_user_and_organization_tags?
          { only: SERIALIZATION_ONLY.dup, methods: (options[:version] == 2 ? SERIALIZATION_METHODS_V2 : SERIALIZATION_METHODS).dup }.merge(options)
        else
          { only: SERIALIZATION_ONLY.dup }.merge(options)
        end
      end
    end
  end
end
