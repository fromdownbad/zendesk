module Zendesk
  module Serialization
    module BookmarkSerialization
      def as_json(options = {})
        options[:version] ||= 1

        json = {
          id: id, created_at: created_at,
          ticket: {
            subject: ticket.subject, description: ticket.description, via_id: ticket.via_id,
            submitter_id: ticket.submitter_id, requester_id: ticket.requester_id, assignee_id: ticket.assignee_id,
            group_id: ticket.group_id, priority_id: ticket.priority_id, status_id: ticket.status_id,
            ticket_type_id: ticket.ticket_type_id
          }
        }

        if options[:version] == 1
          json[:ticket][:nice_id] = ticket.nice_id
        elsif options[:version] == 2
          json[:ticket][:id] = ticket.nice_id
        end

        json
      end
    end
  end
end
