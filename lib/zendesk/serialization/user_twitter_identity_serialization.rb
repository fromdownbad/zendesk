module Zendesk
  module Serialization
    module UserTwitterIdentitySerialization
      def as_json(options = {})
        super.merge(value: screen_name)
      end

      def serialization_options(_options = {})
        { except: [:priority, :undeliverable_count], methods: [:identity_type, :screen_name] }
      end
    end
  end
end
