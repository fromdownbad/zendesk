module Zendesk
  module Serialization
    module ForumSerialization
      def to_xml(options = {})
        super serialization_options.merge(options)
      end

      def as_json(options = {})
        super serialization_options.merge(options)
      end

      def serialization_options(_options = {})
        { except: [:account_id, :sorting, :created_at, :deleted_at],
          methods: [:is_public] }
      end
    end
  end
end
