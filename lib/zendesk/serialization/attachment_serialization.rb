module Zendesk
  module Serialization
    module AttachmentSerialization
      V1_SERIALIZATION_OPTIONS_ONLY = [:id, :content_type, :filename, :size, :is_public, :created_at, :token].freeze
      V1_SERIALIZATION_METHODS      = [:url].freeze

      # TODO: - This is a crappy serialization for API v2. Needs to change.
      V2_SERIALIZATION_OPTIONS_ONLY = [:content_type].freeze
      V2_SERIALIZATION_METHODS      = [:url, :display_filename].freeze

      def to_xml(options = {})
        options[:version] ||= 1
        super(AttachmentSerialization.serialization_options(options))
      end

      def as_json(options = {})
        options[:version] ||= 1
        super(AttachmentSerialization.serialization_options(options))
      end

      def self.serialization_options(options)
        if options[:version] == 1
          options[:only]    ||= V1_SERIALIZATION_OPTIONS_ONLY
          options[:methods]   = ((options[:methods] || []) + V1_SERIALIZATION_METHODS).uniq
        else
          options[:only]    ||= V2_SERIALIZATION_OPTIONS_ONLY
          options[:methods]   = ((options[:methods] || []) + V2_SERIALIZATION_METHODS).uniq
        end

        options
      end
    end
  end
end
