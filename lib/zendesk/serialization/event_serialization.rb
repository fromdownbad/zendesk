module Zendesk
  module Serialization
    module EventSerialization
      TICKET_VIA_TYPES = [ViaType.LINKED_PROBLEM, ViaType.MERGE, ViaType.CLOSED_TICKET].freeze

      def as_json(options = {})
        options[:version] ||= 1
        options[:format] = :json

        json = super(serialization_options(options).merge(options))
        json.stringify_keys!

        if options[:version] == 2
          json[:via] = serialized_via_object(options)
        end

        json
      end

      def to_xml(options = {})
        options[:version] ||= 1
        options[:format] = :xml
        super(serialization_options(options).merge(options))
      end

      def serialization_options(options = {})
        options[:version] ||= 1
        case options[:version]
        when 1
          { only: [:type, :created_at, :author_id, :is_public, :name, :email, :via_id]}
        when 2
          {
            only: [:id, :type, :created_at]
          }
        else
          raise "Unknown serialization version #{options[:version]}"
        end
      end

      def serialized_via_object(_options = {})
        obj = { type: via_id }

        if via_id == ViaType.RULE
          if via_rule = account.rules.find_by_id(via_reference_id)
            obj[:id]    = via_reference_id
            obj[:title] = via_rule.title
          end
        elsif TICKET_VIA_TYPES.member?(via_id)
          if via_ticket = Ticket.with_deleted { account.tickets.find_by_id(via_reference_id) }
            obj[:id]    = via_ticket.nice_id
            obj[:title] = via_ticket.subject
          end
        end

        obj
      end

      def serialized_recipients(options = {})
        if options[:version] == 2
          account.all_users.where(id: recipients).map do |user|
            { id: user.id, name: user.name }
          end
        end
      end
    end
  end
end
