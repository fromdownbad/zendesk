module Zendesk
  module Serialization
    module CommentSerialization
      def serialization_options(options = {})
        output = super(options)
        options[:version] ||= 1

        case options[:version]
        when 1
          output[:only] ||= []
          output[:only] << :value
          output[:methods] ||= []
          output[:methods] << :trusted
          output[:methods] += [:body, :html_body] if options[:use_body]
          output[:include] ||= {}
          if options[:filter] == :comments
            output[:include][:author] ||= {only: [:name, :created_at]}
          else
            output[:include][:attachments] = attachment_includes(output[:include], options[:version])
          end
        when 2
          output[:only] ||= []
          output[:only] << :is_public

          output[:methods] ||= []
          output[:methods] += [:body, :html_body]

          output[:include] ||= {}
          output[:include][:attachments] = attachment_includes(output[:include], options[:version])
        end

        output
      end

      private

      def attachment_includes(includes, version)
        attachments = (includes[:attachments] || {}).merge(version: version)
        AttachmentSerialization.serialization_options(attachments)
      end
    end
  end
end
