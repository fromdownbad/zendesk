module Zendesk
  module Serialization
    module EntrySerialization
      DEFAULT_INCLUDES   = { attachments: {} }.freeze
      SERIALIZATION_ONLY = [
        :body, :created_at, :current_tags, :flag_type_id, :forum_id, :hits, :id, :is_highlighted, :is_locked,
        :is_pinned, :is_public, :organization_id, :position, :posts_count, :submitter_id, :title, :updated_at, :votes_count
      ].freeze

      def to_xml(options = {})
        super apply_exclusion_filter(serialization_options(options.dup))
      end

      def as_json(options = {})
        super apply_exclusion_filter(serialization_options(options.dup))
      end

      def apply_exclusion_filter(options)
        if without = options.delete(:without)
          options[:include] -= without
        end
        options
      end

      def serialization_options(options)
        options[:only]    ||= SERIALIZATION_ONLY
        options[:include] ||= DEFAULT_INCLUDES
        options
      end
    end
  end
end
