module Zendesk
  module Serialization
    module PreviewTicketsSerialization
      SERIALIZATION_OPTIONS = { methods: [:score, :problem_id] }.freeze

      def to_xml(options = {})
        default_options = {
          include: nil,
          root: 'tickets',
          count: total_entries
        }.merge(SERIALIZATION_OPTIONS)

        super(default_options.merge(options))
      end

      def as_json(options = {})
        super(SERIALIZATION_OPTIONS.merge(options))
      end
    end
  end
end
