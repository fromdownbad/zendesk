module Zendesk
  module Serialization
    module UserEmailIdentitySerialization
      def serialization_options(_options = {})
        { except: [:priority, :undeliverable_count], methods: [:identity_type] }
      end
    end
  end
end
