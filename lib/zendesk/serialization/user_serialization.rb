module Zendesk
  module Serialization
    module UserSerialization
      SERIALIZATION_FINDER_INCLUDES = { photo: {}, identities: {}, taggings: {}, groups: {}, settings: {}, signature: {}, texts: {} }.freeze

      V1_SERIALIZATION_ONLY     = [:id, :external_id, :name, :roles, :restriction_id, :organization_id, :locale_id, :time_zone, :details, :notes, :phone, :last_login, :created_at, :updated_at, :is_active, :openid_url].freeze
      V1_SERIALIZATION_METHODS  = [:email, :is_verified, :custom_role_id].freeze
      V1_SERIALIZATION_OPTIONS  = { only: V1_SERIALIZATION_ONLY, methods: V1_SERIALIZATION_METHODS, except: [] }.freeze

      V2_SERIALIZATION_ONLY     = [:id, :locale_id, :name, :created_at, :details, :last_login, :is_moderator, :notes, :updated_at, :external_id, :restriction_id, :is_active, :time_zone, :organization_id, :roles, :phone].freeze
      V2_SERIALIZATION_METHODS  = [:is_verified, :email, :tags, :locale, :custom_role_id, :alias, :private_comments_only].freeze

      def to_xml(options = {})
        options[:version] ||= 1

        base_url = cached_base_url(options)
        clock    = cached_uses_12_hour_clock(options)

        proc = proc do |o|
          o[:builder].tag!("photo-url", photo_url(base_url))
          o[:builder].tag!("uses-12-hour-clock", clock)
        end

        options = serialization_options(options).merge(procs: [proc])
        super(options)
      end

      def as_json(options)
        options[:version] ||= 1

        json = super(serialization_options(options))
        json[:authenticity_token] = options[:authenticity_token] if options[:authenticity_token]

        if options[:version] == 2
          json[:signature] = signature.present? ? signature.value : nil
          json[:photo]     = photo.present? ? photo.as_json(options) : nil
          json[:shared]    = foreign?
        end

        # These rely on the account. Putting them here to avoid an N+1 on the account lookup.
        json[:photo_url]          = photo_url(cached_base_url(options))

        # Uses 12 hour clock should go from v2, it's an account wide setting
        json[:uses_12_hour_clock] = cached_uses_12_hour_clock(options)

        if options[:version] == 2 && options[:current_user] && options[:current_user].is_system_user? && options[:current_username] == "chat"
          json = json.slice("id", "name", "organization_id")
        end

        json
      end

      def serialization_options(options = {})
        options = options.dup
        case options[:version]
        when 1
          options[:only]    ||= V1_SERIALIZATION_ONLY
          options[:methods]   = (Array(options[:methods]) + V1_SERIALIZATION_METHODS).uniq
        when 2
          options[:only]    ||= V2_SERIALIZATION_ONLY
          options[:methods]   = (Array(options[:methods]) + V2_SERIALIZATION_METHODS).uniq
        else
          raise "Unknown serialization version #{options[:version]}"
        end

        options[:only]    = options[:only] - Array(options[:except])
        options[:methods] = options[:methods] - Array(options[:except])
        options
      end

      private

      def cached_base_url(options)
        options[:base_url] = account.url(mapped: false) unless options.key?(:base_url)
        options[:base_url]
      end

      def cached_uses_12_hour_clock(options)
        options[:uses_12_hour_clock] = account.uses_12_hour_clock unless options.key?(:uses_12_hour_clock)
        options[:uses_12_hour_clock]
      end
    end
  end
end
