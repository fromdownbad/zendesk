# DEPRECATED. This remains for legacy Twitter event support. See ChannelBackEvent for current version.

module Zendesk
  module Serialization
    module TwitterEventSerialization
      def as_json(options = {})
        json = super

        if options[:version] == 2
          json[:recipients] = serialized_recipients(options)
        end

        json
      end

      def serialization_options(options = {})
        output = super(options)

        case options[:version]
        when 1
          output[:only] += [:via_reference_id, :value, :value_previous, :value_previous, :value_reference]
        when 2
          output[:methods] ||= []
          output[:methods] += [:sender_mth_id, :body]
        end

        output
      end
    end
  end
end
