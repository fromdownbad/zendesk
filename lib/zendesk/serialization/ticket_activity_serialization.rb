module Zendesk
  module Serialization
    module TicketActivitySerialization
      def to_xml(options = {})
        super(serialization_options(options)) do |xml|
          xml << activity_object_data.to_xml(skip_instruct: true, root: :object)
          xml << activity_target_data.to_xml(skip_instruct: true, root: :target)
        end
      end

      def as_json(options = {})
        super(serialization_options(options)).merge(serialization_data)
      end

      def serialization_options(options)
        {
          include: {
            actor: {only: [:id, :name], methods: [:photo_url, :email, :is_verified]},
            user: {only: [:id, :name], methods: [:photo_url, :email, :is_verified]}
          },
          methods: :title,
          only: [:id, :created_at, :verb]
        }.merge(options)
      end
    end
  end
end
