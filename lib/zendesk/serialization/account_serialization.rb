module Zendesk
  module Serialization
    module AccountSerialization
      def serialization_options(options = {})
        case options[:version]
        when :internal
          {include: :owner}
        else
          {}
        end
      end

      def as_json(options = {})
        json = super(serialization_options(options).merge(options))

        json[:trial] = subscription.is_trial
        json[:trial_expires_on] = subscription.trial_expires_on

        # TO-DO to remove above 2 lines and uncomment below line when lotus is ready to read new json
        # json.delete_if{ |k,v| subscription.as_json.keys.include?(k) }
        json[:billing] = subscription.as_json

        # The following is needed for Screencast in Lotus.
        if has_screencasts_for_tickets_enabled?
          json[:screenr_host] = screenr_integration.host
          json[:screenr_tickets_recorder_id] = screenr_integration.tickets_recorder_id
        end

        json.stringify_keys!
      end
    end
  end
end
