module Zendesk
  module Serialization
    module ActivitySerialization
      FINDER_INCLUDES = [:actor, :user, :activity_object, :activity_target].freeze

      def to_xml(options = {})
        super(options)
      end

      def as_json(options = {})
        super(options)
      end

      def serialization_options
        raise "Must be implemented in subclass"
      end
    end
  end
end
