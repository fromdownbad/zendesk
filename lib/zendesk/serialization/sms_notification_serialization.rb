module Zendesk
  module Serialization
    module SmsNotificationSerialization
      def as_json(options = {})
        json = super

        json[:recipients] = serialized_recipients(options)

        json
      end

      def serialization_options(options = {})
        output = super(options)

        output[:methods] ||= []
        output[:methods] += [:subject, :body]

        output
      end
    end
  end
end
