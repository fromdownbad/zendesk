module Zendesk
  module Serialization
    module CcSerialization
      def serialization_options(options = {})
        output = super(options)

        case options[:version]
        when 1
          output[:only] = [:type, :created_at, :author_id, :is_public, :name, :email, :via_id, :value]
        when 2
          output[:methods] ||= []
          output[:methods] -= [:subject, :body]
        end

        output
      end
    end
  end
end
