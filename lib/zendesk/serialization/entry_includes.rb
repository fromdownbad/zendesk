module Zendesk
  module Serialization
    module EntryIncludes
      ALLOWED_ENTRY_INCLUDES = {
        submitter: Zendesk::Serialization::UserSerialization::V1_SERIALIZATION_OPTIONS,
        attachments: {},
        posts: { include: { user: Zendesk::Serialization::UserSerialization::V1_SERIALIZATION_OPTIONS }}
      }.freeze

      ENTRY_FINDER_INCLUDES = {
        posts: { user: Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES }
      }.freeze

      def entry_serialization_options
        return nil if current_user.is_anonymous_user?

        if params[:include]
          if params[:include].all?(&:blank?)
            nil
          else
            ALLOWED_ENTRY_INCLUDES.slice(*params[:include].map(&:to_sym)).deep_dup
          end
        end
      end

      def entry_includes
        return nil if current_user.is_anonymous_user?

        if serialization_options = entry_serialization_options
          ENTRY_FINDER_INCLUDES.slice(*serialization_options.keys)
        end
      end
    end
  end
end
