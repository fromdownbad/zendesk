module Zendesk
  module Serialization
    module TicketSerialization
      INCLUDE_OPTIONS = { collaborators: Zendesk::Serialization::UserSerialization::V1_SERIALIZATION_OPTIONS }.freeze

      def to_xml(options = {})
        opts = serialization_options(options).merge(options)
        opts[:procs] = [channel_to_xml, permissions_to_xml]
        opts.delete(:for_end_user)

        super(opts) do |xml|
          if last_comment
            xml << last_comment.to_xml(root: "last_comment", skip_instruct: true)
          end
        end
      end

      def as_json(options = {})
        opts = serialization_options(options).merge(options).dup
        opts[:methods] ||= []
        opts[:methods] += [:channel, :permissions]
        opts.delete(:for_end_user)

        if last_comment
          super(opts).merge(last_comment: last_comment)
        else
          super(opts)
        end
      end

      def channel_to_xml
        proc do |options|
          xml = options[:builder]
          if twitter_ticket_source.present?
            xml.channel do
              xml.tag!("monitored-twitter-account", twitter_ticket_source.try(:mention_name))
              xml.tag!("type", "Twitter")
            end
          else
            xml.tag!("channel", nil, nil: true)
          end
        end
      end

      def permissions_to_xml
        proc do |options|
          xml = options[:builder]
          xml.permissions do
            permissions.each do |k, v|
              xml.tag!(k.to_s.tr("_", "-"), v)
            end
          end
        end
      end

      def permissions
        return {} unless current_user

        {
          can_update_ticket:           can_be_updated? && current_user.can?(:edit_properties, self),
          # :can_edit_ticket_properties is an alias to :can_update_ticket. Lotus and mobile clients will migrate
          # from using :can_update_ticket to :can_edit_ticket_properties.
          # See discussion on https://github.com/zendesk/zendesk/pull/5437
          can_edit_ticket_properties: can_be_updated? && current_user.can?(:edit_properties, self),
          can_delete_ticket:          current_user.can?(:delete, self),
          can_merge_ticket:           can_be_updated? && current_user.can?(:merge, self),
          can_edit_ticket_tags:       can_be_updated? && current_user.can?(:edit_tags, self),
          can_make_comments:          can_be_updated? && current_user.can?(:add, Comment),
          can_mark_as_spam:           can_be_updated? && current_user.can?(:mark_as_spam, self),
          can_make_public_comments:   can_be_updated? && current_user.can?(:publicly, Comment),
          can_create_followup_ticket: !can_be_updated?
        }
      end

      def last_comment
        return nil if latest_comment_mapped.nil?

        {
          value: latest_comment_mapped.body,
          author_id: latest_comment_mapped.author_id,
          author_name: latest_comment_mapped.author.name,
          author_photo_url: latest_comment_mapped.author.photo_url,
          via_id: latest_comment_mapped.via_id,
          is_public: latest_comment_mapped.is_public,
          created_at: latest_comment_mapped.created_at
        }
      end

      def serialization_options(opts = {})
        for_end_user = opts[:for_end_user] || false

        version = (opts[:version] || 1).to_i
        use_comment_body = account && account.has_use_comment_body_in_xml_export?
        comment_serialization_options = Comment.new.serialization_options(use_body: use_comment_body)
        comment_includes = comment_serialization_options[:include]
        comment_only = comment_serialization_options[:only]
        comment_methods = use_comment_body ? comment_serialization_options[:methods] : []

        options = {
          include: {
            comments: {
              include: comment_includes,
              only: comment_only,
              methods: comment_methods
            },
            ticket_field_entries: {},
            linkings: {}
          },
          except: []
        }

        if version == 2
          add_v2_includes!(options)
        end

        if additional_includes = opts.delete(:additional_includes)
          additional_includes.each do |incl|
            options[:include].update(incl => INCLUDE_OPTIONS[incl].dup)
          end
        end

        if for_end_user
          options[:only] = [:nice_id, :status_id, :description, :created_at, :organization_id, :via_id, :subject]
        else
          options[:except] = [
            :account_id, :id, :assignee_updated_at, :requester_updated_at,
            :initial_assigned_at, :delta, :linked_id, :generated_timestamp, :inbox_updated_at,
            :satisfaction_score, :satisfaction_reason_code, :locale_id, :latest_comment_added_at, :ticket_form_id, :brand_id,
            :sla_breach_status, :base_score
          ]
          options[:methods] = [:problem_id, :has_incidents]
          options[:except].delete(:ticket_form_id) if account.try(:has_ticket_forms?)
          options[:except].delete(:brand_id) if account.try(:has_multibrand?)
        end

        options[:except] += [:latest_recipients]
        options
      end

      def add_v2_includes!(options)
        options[:include].update(
          requester: {
            only: UserSerialization::V2_SERIALIZATION_ONLY,
            methods: UserSerialization::V2_SERIALIZATION_METHODS.dup
          },
          assignee: {
            only: UserSerialization::V2_SERIALIZATION_ONLY,
            methods: UserSerialization::V2_SERIALIZATION_METHODS.dup
          },
          group: {
            only: [:name, :id],
            methods: []
          }
        )
      end
    end
  end
end
