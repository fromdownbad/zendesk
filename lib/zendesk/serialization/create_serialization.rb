module Zendesk
  module Serialization
    module CreateSerialization
      def as_json(options = {})
        hash = super(serialization_options(options).merge(options)).stringify_keys!

        if options[:version] == 2
          if hash["field_name"] == "linked_id"
            hash["field_name"] = "problem_id"
            hash["value"]      = Ticket.with_deleted { account.tickets.find_by_id(value).try(:nice_id) }
          end
        end

        hash
      end

      def serialization_options(options = {})
        output = super(options)

        case options[:version]
        when 1
          output[:only] += [:via_reference_id, :value, :value_previous, :value_previous, :value_reference]
        when 2
          output[:only] << :value

          output[:methods] ||= []
          output[:methods] << :field_name
        end

        output
      end
    end
  end
end
