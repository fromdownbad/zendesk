module Zendesk
  module Serialization
    module DeviceIdentifierSerialization
      def to_xml(opts = {})
        super(opts.merge(serialization_options))
      end

      def as_json(opts = {})
        super(opts.merge(serialization_options))
      end

      def serialization_options(_opts = nil)
        {only: [:id, :token, :device_type]}
      end
    end
  end
end
