module Zendesk
  module Serialization
    module VoiceCommentSerialization
      def as_json(options = {})
        json = super

        if options[:version] == 1
          # for v1 we but the body method into the value key
          json['value'] = json.delete('body')
        end

        json
      end

      def serialization_options(options = {})
        output = super(options)

        case options[:version]
        when 1
          output[:only] += [:via_reference_id, :value_previous, :value_previous, :value_reference]
          output[:only].delete(:value)
          output[:methods] << :body
          output[:methods].delete(:trusted)
        when 2
          output[:methods] ||= []
          output[:methods] << :data
          output[:methods] << :formatted_from
          output[:methods] << :transcription_visible
        end

        output
      end
    end
  end
end
