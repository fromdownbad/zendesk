module Zendesk
  module Serialization
    module UserIdentitySerialization
      def as_json(options = {})
        if options[:version].to_s == "2"
          super(options_without_domain).merge(
            id: id, user_id: user_id,
            type: identity_type, value: value,
            created_at: created_at, updated_at: updated_at,
            is_verified: is_verified, primary: primary?
          )
        else
          super options_without_domain
        end
      end

      def to_xml(options = {})
        super options_without_domain.merge(options)
      end

      private

      def options_without_domain
        sopts = serialization_options
        sopts[:except] ||= []
        sopts[:except] << :domain
        sopts
      end
    end
  end
end
