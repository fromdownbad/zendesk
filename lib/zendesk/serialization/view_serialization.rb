module Zendesk
  module Serialization
    module ViewSerialization
      def to_xml(options = {})
        if options[:count_for_user]
          legacy_rest_properties(options).to_xml(options)
        else
          super(serialization_options.merge(options)) do |xml|
            xml << serialized_output.to_xml(skip_instruct: true, root: "output")
          end
        end
      end

      def as_json(options = {})
        if options[:count_for_user]
          legacy_rest_properties(options)
        else
          super(serialization_options.merge(options)).merge(output: serialized_output)
        end
      end

      def serialized_output
        {
          order: output.order ? output.order.to_s : nil,
          group: output.group ? output.group.to_s : nil,
          type: output.type.to_s,
          order_asc: output.order_asc,
          columns: output.columns.map do |c|
            if /^\d+/.match?(c.to_s)
              {name: "custom_field_#{c}"}
            else
              {name: c.to_s}
            end
          end
        }
      end

      def legacy_rest_properties(options)
        if options[:delay_counts]
          ticket_count = ticket_count(options[:count_for_user], refresh: :delayed).value
          if ticket_count.nil?
            ticket_count = -1
          end
        else
          ticket_count = ticket_count(options[:count_for_user]).value
        end

        {
          owner_type: owner_type, owner_id: owner_id, id: id, per_page: per_page, position: position, title: title,
          is_active: is_active, ticket_count: ticket_count
        }
      end

      def serialization_options
        {
          only: [:id, :per_page, :position, :is_active, :title, :owner_id, :owner_type]
        }
      end
    end
  end
end
