module Zendesk
  module Serialization
    module SatisfactionRatingSerialization
      def as_json(_options = {})
        {
          assignee_id: agent_user_id, group_id: agent_group_id,
          requester_id: enduser_id, ticket_id: ticket.nice_id,
          score: SatisfactionType.to_s(score), id: id
        }
      end

      def to_xml(options = {})
        proc = proc do |opts|
          opts[:builder].tag!("ticket-id", ticket.nice_id)
          opts[:builder].tag!("assignee-id", agent_user_id)
          opts[:builder].tag!("requester-id", enduser_id)
          opts[:builder].tag!("group-id", agent_group_id)
          opts[:builder].tag!("score", SatisfactionType.to_s(score))
        end

        super(options.merge(only: [:id], procs: [proc]))
      end
    end
  end
end
