module Zendesk
  module Serialization
    module CrmDataSerialization
      def as_json(options = {})
        if options[:version] != 2
          super(options)
        else
          result = { records: records }

          if sync_pending?
            sync_state  = :pending
            message     = "Your request is currently being processed."
          elsif sync_errored?
            sync_state  = :failed
            message     = "Failed to complete the request."

            result.update(error: data.with_indifferent_access[:error])
          else
            sync_state  = :done
            message     = "Your request has been proccessed."
          end

          result.update(state: sync_state, message: message)
          [:link, :message].each { |field| result[field] = options[field] if options.key?(field) }
          result
        end
      end
    end
  end
end
