module Zendesk
  module Serialization
    module TagScoreSerialization
      def as_json(options = {})
        options[:version] ||= 1
        if options[:version] == 1
          { id: id, count: score, name: tag_name }
        else
          { tag_name: tag_name, score: score }
        end
      end

      def to_xml(options = {})
        proc = proc do |opts|
          opts[:builder].count(score)
          opts[:builder].name(tag_name)
        end

        super(options.reverse_merge(only: [:id], procs: [proc], root: "tag"))
      end
    end
  end
end
