module Zendesk
  module Serialization
    module PreviewResultsSerialization
      SERIALIZATION_OPTIONS = { include: nil }.freeze

      def to_xml(options = {})
        default_options = {
          root: 'tickets',
          count: total_entries
        }.merge(SERIALIZATION_OPTIONS)

        super(default_options.merge(options))
      end

      def to_json(options = {})
        as_json(options).to_json
      end

      def as_json(options = {})
        { count: total_entries,
          tickets: map { |t| t.as_json(SERIALIZATION_OPTIONS.merge(options)) }}
      end
    end
  end
end
