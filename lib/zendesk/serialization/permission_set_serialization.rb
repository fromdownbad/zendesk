module Zendesk
  module Serialization
    module PermissionSetSerialization
      def as_json(options = {})
        super(serialization_options.merge(options))
      end

      def to_xml(options = {})
        super(serialization_options.merge(options))
      end

      def serialization_options
        { only: [:id, :name, :description] }
      end
    end
  end
end
