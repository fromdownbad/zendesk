module Zendesk::KubernetesServiceProxyBuilder
  class << self
    def service_url(service_name, pod_id)
      dc = datacenter_for_pod(pod_id)
      "#{service_name}.pod-#{pod_id}.svc.#{dc}.zdsys#{"test" unless Rails.env.production?}.com:4080"
    end

    def datacenter_for_pod(pod_id)
      consul = ENV['CONSUL_HTTP_ADDR'] || 'localhost:8500'
      Faraday.get("http://#{consul}/v1/kv/global/pod/#{pod_id}/dc?raw").body
    end
  end
end
