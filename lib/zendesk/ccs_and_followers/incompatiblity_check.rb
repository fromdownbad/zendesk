#
# Utility class used by IncompatibleFeaturesJob.
# It checks if the account has any incompatible features that would prevent the
# activation of the CCs/Followers. E.g.
#
#   account = 'support'.to_account
#   check = Zendesk::CcsAndFollowers::IncompatiblityCheck.new(account: account)
#
#   check.account               => <Account>
#   check.summary               => "Account 'support' - compatible"
#   check.incompatible?         => false
#   check.mobile_app_used?      => false
#   check.incompatible_features => []
#
class Zendesk::CcsAndFollowers::IncompatiblityCheck
  attr_accessor :account

  START_DATE = '11/7/2018'.freeze

  def initialize(account:)
    @account = account
  end

  def incompatible?
    incompatible_features.any?
  end

  def incompatible_features
    account.on_shard do
      @incompatible_features ||= begin
        results = []
        results << :answer_bot if account.answer_bot_subscription.present?
        results << :markdown if account.settings.markdown_ticket_comments?
        results << :mobile_app if mobile_app_used?
        results << :voice if account.voice_enabled?
        results
      end
    end
  end

  def mobile_app_used?
    account.settings.mobile_app_access? &&
      account.mobile_devices.where("DATE(last_active_at) >= DATE(#{START_DATE})").any?
  end

  def summary
    if incompatible?
      "Account '#{account.subdomain}' - incompatible features: #{incompatible_features.to_sentence}"
    else
      "Account '#{account.subdomain}' - compatible"
    end
  end
end
