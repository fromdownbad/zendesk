class Zendesk::CcsAndFollowers::RulesCompatibility
  CONDITION_TYPES = {
    conditions_all: '@conditions_all'.to_sym,
    conditions_any: '@conditions_any'.to_sym
  }.freeze
  SUPPORTED_RULE_TYPES = [Trigger, Automation].freeze

  def initialize(account_id)
    @account = Account.find(account_id)
  end

  # Returns an array of Zendesk::Rules::Diff objects,
  # regardless of `dry_run` being set.
  def update_requester_target(dry_run: true)
    statsd_client.time('compatibility.rules', tags: ['update:requester_target']) do
      return [] unless @account.has_email_ccs?

      new_condition_values = ['comment_is_public', 'is', [true]].freeze
      new_condition = DefinitionItem.new(*new_condition_values)

      conditions_to_remove_values = [
        ['requester_id', 'is_not', ['current_user']]
      ].freeze
      conditions_to_remove = conditions_to_remove_values.map { |condition_values| DefinitionItem.new(*condition_values) }

      update_rules!(dry_run) do |candidate_rule|
        action_indexes = action_indexes_to_update(candidate_rule.definition.actions)
        next if action_indexes.blank?

        candidate_rule.tap do |rule|
          action_indexes.each do |action_index|
            action = rule.definition.actions[action_index]
            value = action.value
            value[value.find_index('requester_id')] = 'requester_and_ccs'
            action.value = value

            rule.definition.actions[action_index] = action
          end

          next unless rule.is_a?(Trigger)

          add_new_condition = true

          CONDITION_TYPES.each do |type, condition_type|
            conditions = rule.definition.send(type).each_with_object([]) do |condition, to_keep|
              add_new_condition = false if condition == new_condition
              to_keep << condition unless conditions_to_remove.include?(condition)
            end

            if add_new_condition && type == :conditions_all
              conditions << DefinitionItem.new(*new_condition_values)
            end

            rule.definition.instance_variable_set(condition_type, conditions)
          end
        end
      end
    end
  end

  private

  def update_rules!(dry_run)
    ActiveRecord::Base.transaction do
      @account.on_shard do
        @account.rules.where(type: SUPPORTED_RULE_TYPES).each_with_object([]) do |rule_to_yield, diffs|
          next unless rule_to_yield.valid?
          next unless yield(rule_to_yield)

          old_rule = @account.rules.find(rule_to_yield.id)

          rule_to_yield.save! unless dry_run
          diffs << Zendesk::Rules::Diff.new(source: old_rule, target: rule_to_yield)
        end
      end
    end
  end

  def action_indexes_to_update(actions)
    actions.each_index.each_with_object([]) do |index, indexes|
      if actions[index].source == 'satisfaction_score' && value_contains_one(actions[index].value)
        return []
      end

      indexes << index if actions[index].source == 'notification_user' &&
                          actions[index].value.include?('requester_id')
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: :ccs_and_followers)
  end

  def value_contains_one(value)
    ([1, '1'] & value).present?
  end
end
