module Zendesk
  module TicketFieldErrors
    class Error < Api::Presentation::Error
      attr_accessor :field

      def initialize(field, message, options = {})
        super(message, options)
        @field = field
      end

      def details
        { field_key: field_key }
      end

      protected

      def field_key
        case field
        when FieldAssignee
          "assignee"
        when FieldDescription
          "description"
        when FieldGroup
          "group"
        when FieldPriority
          "priority"
        when FieldStatus
          "status"
        when FieldSubject
          "subject"
        when FieldTicketType
          "ticket_type"
        else
          field.id
        end
      end
    end

    def add_to_ticket_field_errors(field, message = nil, options = {})
      return unless field

      user       = options[:current_user]
      account    = options[:current_account]
      title_type = options[:for_agent] ? :title : :title_in_portal

      localized_title = localize_field_title(field, account, title_type)
      title = render_liquid_for_ticket_field_error_title(localized_title, account, user)
      return unless title

      message ||= :invalid
      options = options.merge(title: title)

      error = Error.new(field, message, options)
      errors.add(:base, error)
    end

    private

    def render_liquid_for_ticket_field_error_title(title, current_account, current_user)
      return title unless current_account
      Zendesk::Liquid::DcContext.render(
        title,
        current_account,
        current_user,
        "text/plain",
        ::I18n.translation_locale
      )
    end

    def localize_field_title(field, account, field_type)
      field.multilingual_field(
        user_locale: ::I18n.translation_locale,
        account_locale: account.try(:translation_locale),
        field_type: field_type
      )
    end
  end
end
