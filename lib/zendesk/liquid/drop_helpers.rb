module Zendesk::Liquid::DropHelpers
  def time_and_record(key, tags = [])
    # Benchmark on 10% sample rate, and include tags.
    statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['liquid', 'drop'])
    statsd_client.time(key, tags: tags, sample_rate: 0.1) { @res = yield }
    @res
  end

  def timed_render
    time_and_record('render', statsd_tags) { @res = render }
    @res
  end

  private

  def statsd_tags
    ["drop:#{self.class}", "template:#{template_id}"]
  end

  def template_id
    :na
  end
end
