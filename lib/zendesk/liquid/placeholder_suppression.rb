module Zendesk::Liquid
  class PlaceholderSuppression
    PUBLIC_COMMENT_PLACEHOLDER_STRINGS = %w[
      comment.rich
      comment.value_rich
      ticket.comments
      ticket.comments_formatted
      ticket.description
      ticket.latest_comment
      ticket.latest_comment_formatted
      ticket.latest_comment_html
      ticket.latest_comment_rich
      ticket.latest_public_comment
      ticket.latest_public_comment_formatted
      ticket.latest_public_comment_html
      ticket.latest_public_comment_rich
      ticket.public_comments
      ticket.public_comments_formatted
    ].freeze
    PUBLIC_COMMENT_PLACEHOLDER_PATTERN = /(?:#{PUBLIC_COMMENT_PLACEHOLDER_STRINGS.map { |liquid| /{{\s*#{liquid}\s*}}/ }.join('|')})/xi.freeze

    def self.redact_placeholders(text)
      text.gsub(PUBLIC_COMMENT_PLACEHOLDER_PATTERN, '')
    end
  end
end
