require "zendesk/liquid/comments/collection_drop"
require "zendesk/liquid/comments/comment_drop"

module Zendesk::Liquid
  class TicketDrop < Liquid::Drop
    include ::Zendesk::Liquid::DropHelpers

    ALLOWED_METHODS = [
      :external_id, :score, :current_collaborators
    ].freeze

    attr_reader :account,
      :ticket,
      :for_agent,
      :format,
      :current_user,
      :locale,
      :dc_cache,
      :include_tz,
      :show_last_three_comments,
      :skip_attachments

    def initialize(ticket, for_agent, format, options = {})
      @ticket       = ticket
      @account      = ticket.account
      @for_agent    = for_agent
      @format       = format
      @ticket_url   = options[:ticket_url]
      @current_user = options[:current_user]
      @locale       = options[:locale]
      @dc_cache     = options[:dc_cache] || {}
      @filter       = options[:filter]
      @include_tz   = options[:include_tz]
      @skip_attachments = options[:skip_attachments] || []
      @custom_fields_cache = options[:custom_fields_cache] || {}
      @show_last_three_comments = options[:show_last_three_comments]

      # Real quoted email replies-specific options
      @mail_delimiter = options[:mail_delimiter]
      @mail_footer = options[:mail_footer]
      @mail_header = options[:mail_header]
      @mail_locale_for_liquid = options[:mail_locale_for_liquid]

      @time_zone, @time_format = if @current_user && @current_user.time_zone && !@current_user.is_system_user?
        [@current_user.time_zone, @current_user.time_format_string(@include_tz)]
      else
        [@account.time_zone, @account.time_format_string]
      end
    end

    # NOTE: In app/models/ticket/collaborating.rb
    delegate :cc_names, to: :ticket
    delegate :email_ccs, :email_cc_names, to: :ticket
    delegate :follower_names, to: :ticket

    def ccs
      account.has_follower_and_email_cc_collaborations_enabled? ? ticket.email_ccs : ticket.ccs
    end

    def followers
      if account.has_follower_and_email_cc_collaborations_enabled?
        ticket.followers(include_uncommitted: true)
      else
        []
      end
    end

    # Simple rewritten methods for Liquid

    def id
      nice_id
    end

    def nice_id
      ticket.nice_id
    end

    def encoded_id
      ticket.encoded_id
    end

    def status
      ::I18n.t('type.status.' + ticket.status.downcase)
    end

    def priority
      (ticket.priority_id.present? && ticket.priority != '-') ? ::I18n.t('type.priority.' + ticket.priority.downcase) : ''
    end

    def via
      ticket.via_id.present? ? ::I18n.t("txt.via_types.#{ticket.via.downcase.split(" ").join("_")}") : ''
    end

    def ticket_type
      ::I18n.t('type.ticket.' + ticket.ticket_type.downcase)
    end

    def title
      # ticket subject truncated to 150 characters. ZD#379378
      escape(ticket.title_for_role(for_agent, 150))
    end

    # This method emits a full link to the ticket, including protocol
    def link
      ticket.url(for_agent: for_agent)
    end
    alias_method :url_with_protocol, :link

    # url without protocol, legacy, might be used by customers
    def url
      return unless ticket_url = @ticket_url || ticket.url(for_agent: for_agent)
      ticket_url.split("://").last
    end

    def tags
      ticket.current_tags
    end

    def in_business_hours
      ticket.in_business_hours?.to_s # Ugh. We should have made an account drop instead.
    end

    def current_holiday_name
      ticket.business_hours_calculation.current_holiday.try(:name)
    end

    def created_at
      format_date(ticket.created_at)
    end

    def updated_at
      format_date(ticket.updated_at)
    end

    def due_date
      format_date(ticket.due_date)
    end

    def created_at_with_time
      format_time(ticket.created_at)
    end

    def updated_at_with_time
      format_time(ticket.updated_at)
    end

    def due_date_with_time
      format_time(ticket.due_date)
    end

    def created_at_with_timestamp
      format_timestamp(ticket.created_at)
    end

    def updated_at_with_timestamp
      format_timestamp(ticket.updated_at)
    end

    def due_date_with_timestamp
      format_timestamp(ticket.due_date)
    end

    def liquid_method_missing(method)
      method = method.to_s

      if ticket.respond_to?(method) && ALLOWED_METHODS.member?(method.to_sym)
        escape(ticket.send(method))
      elsif method =~ /^ticket_field_option_title_(\d+)/
        # Custom field placeholders - format e.g. {{ticket_field_option_title_123}}
        field = find_custom_field($1)
        render_custom_field(field)
      elsif method =~ /^ticket_field[s]?_(\d+)/
        # Custom field placeholders - format e.g. {{ticket_field_123}}
        field = find_custom_field($1)
        field ? escape(field.value(ticket)) : ""
      else
        super
      end
    end

    # In order for liquid to access associated objects, they need to be exposed as proper methods

    def latest_comment
      if for_agent
        ticket.latest_comment
      else
        latest_public_comment
      end
    end

    def latest_public_comment
      ticket.latest_public_comment
    end

    def comments
      if for_agent
        finder_options(ticket.recent_comments, public_only: false)
      else
        public_comments
      end
    end

    def public_comments
      finder_options(ticket.recent_comments, public_only: true)
    end

    def latest_comment_rich
      return unless latest_comment
      Comments::CommentDrop.new(
        account,
        latest_comment,
        format,
        for_agent,
        current_user,
        brand,
        include_tz: include_tz,
        comment_html_only: true,
        skip_attachments: skip_attachments,
        skip_header: true
      ).timed_render.html_safe
    end

    def last_three_comments_formatted
      Comments::CollectionDrop.new(
        account,
        finder_options(ticket.last_few_comments, public_only: true),
        format,
        for_agent,
        @current_user,
        brand,
        include_tz: @include_tz,
        locale: @locale,
        skip_attachments: skip_attachments,
        show_last_three_comments: true
      ).timed_render.html_safe
    end

    def latest_public_comment_rich
      return unless latest_public_comment

      Comments::CommentDrop.new(
        account,
        latest_public_comment,
        format,
        for_agent,
        current_user,
        brand,
        include_tz: include_tz,
        comment_html_only: true,
        skip_attachments: skip_attachments
      ).timed_render.html_safe
    end

    def latest_comment_html
      return unless account.has_email_simplified_threading_placeholders? && latest_comment

      options = {
        include_tz: include_tz,
        simplified_comment: true,
        skip_attachments: skip_attachments,
        skip_header: true,
      }.tap do |opts|
        opts[:last_three_comments] = last_three_comments_formatted if show_last_three_comments
      end

      Comments::CommentDrop.new(
        account,
        latest_comment,
        format,
        for_agent,
        current_user,
        brand,
        options
      ).timed_render.html_safe
    end

    def latest_public_comment_html
      return unless account.has_email_simplified_threading_placeholders? && latest_public_comment

      options = {
        include_tz: include_tz,
        simplified_comment: true,
        skip_attachments: skip_attachments
      }.tap do |opts|
        opts[:last_three_comments] = last_three_comments_formatted if show_last_three_comments
      end

      Comments::CommentDrop.new(
        account,
        latest_public_comment,
        format,
        for_agent,
        current_user,
        brand,
        options
      ).timed_render.html_safe
    end

    # NOTE: Indicates if the Follower's email reply will result in a public or internal comment on the ticket.
    def follower_reply_type_message
      unless account.has_follower_and_email_cc_collaborations_enabled?
        return ::I18n.t('txt.admin.settings.is_email_comment_public_by_default.true')
      end

      if latest_comment.try(:is_private?)
        ::I18n.t('txt.admin.settings.is_email_comment_public_by_default.false')
      else
        if account.is_email_comment_public_by_default
          ::I18n.t('txt.admin.settings.is_email_comment_public_by_default.true')
        else
          ::I18n.t('txt.admin.settings.is_email_comment_public_by_default.false')
        end
      end
    end

    def audits
      ticket.audits
    end

    def brand
      ticket.brand
    end

    def group
      ticket.group
    end

    def assignee
      ticket.assignee
    end

    def submitter
      ticket.submitter
    end

    def requester
      ticket.requester
    end

    def ticket_form
      ticket.ticket_form.try(:display_name).presence || ticket.ticket_form.try(:name)
    end

    def organization
      ticket.organization
    end

    # Formatted data

    def verbatim_description
      escape(ticket.description)
    end

    def description
      comment = if for_agent
        ticket.first_comment
      else
        finder_options(ticket.comments, public_only: true).first
      end

      format_comment(comment)
    end

    def comments_formatted
      if for_agent
        format_comments(finder_options(ticket.recent_comments, public_only: false))
      else
        public_comments_formatted
      end
    end

    def public_comments_formatted
      format_comments(finder_options(ticket.recent_comments, public_only: true))
    end

    def quoted_comments
      if for_agent
        top_quoted_comments(finder_options(ticket.recent_comments, public_only: false))
      else
        public_deep_quoted_comments
      end
    end

    def public_deep_quoted_comments
      deep_quoted_comments(finder_options(ticket.recent_comments, public_only: true))
    end

    def latest_comment_formatted
      if for_agent
        format_comment(ticket.latest_comment)
      else
        latest_public_comment_formatted
      end
    end

    def latest_public_comment_formatted
      format_comment(ticket.latest_public_comment)
    end

    def to_s
      ""
    end

    private

    def find_custom_field(id)
      id = Integer(id)
      @custom_fields_cache[id] || begin
        field = account.ticket_fields.find_by_id(id)
        field && !field.is_system_field? ? field : nil
      end
    end

    def format_comment(comment)
      format_comments([comment].compact)
    end

    def format_comments(comments)
      Comments::CollectionDrop.new(
        account,
        comments,
        format,
        for_agent,
        @current_user,
        brand,
        include_tz: @include_tz,
        locale: @locale,
        skip_attachments: skip_attachments
      ).timed_render.html_safe
    end

    def top_quoted_comments(comments)
      Comments::CollectionTopQuotedDrop.new(
        account,
        comments,
        format,
        for_agent,
        @current_user,
        brand,
        include_tz: @include_tz,
        skip_attachments: skip_attachments
      ).timed_render.html_safe
    end

    def deep_quoted_comments(comments)
      Comments::CollectionDeepQuotedDrop.new(
        account,
        comments,
        format,
        for_agent,
        @current_user,
        brand,
        skip_attachments: skip_attachments
      ).timed_render.html_safe
    end

    def format_date(date)
      return "" if date.blank?
      return date if @filter
      date.to_format(time_zone: @time_zone)
    end

    def format_time(time)
      return "" if time.blank?
      time.to_format(with_time: true, time_format: @time_format, time_zone: @time_zone)
    end

    def format_timestamp(datetime)
      return "" if datetime.blank?
      datetime.iso8601
    end

    def escape(value)
      if value.is_a?(String) && format == Mime[:html].to_s
        CGI.escapeHTML(value).html_safe
      else
        # TODO: cleanup inputs. should only have Strings (and maybe NilClass's) here
        value.respond_to?(:html_safe) ? value.html_safe : value
      end
    end

    def finder_options(scope, public_only: true)
      account_includes    = { account: { settings: {} }}
      author_includes     = { author: account_includes }
      attachment_includes = { attachments: account_includes }
      audit_includes      = { audit: {} }

      collection_includes = (account_includes + author_includes + attachment_includes + audit_includes)
      scope = scope.includes(collection_includes)

      if public_only
        scope = scope.where(is_public: true)
      end

      scope
    end

    def render_custom_field(field)
      return "" if field.blank?

      value = field.humanize_value(field.value(ticket))
      escape render_dynamic_content(value)
    end

    def render_dynamic_content(value)
      render_options = {
        dc_cache: dc_cache,
        custom_fields_cache: @custom_fields_cache,
      }

      Zendesk::Liquid::TicketContext.render(value, ticket, @current_user, for_agent, locale, Mime[:text], render_options)
    end
  end
end
