require 'cgi'

module Zendesk::Liquid
  class SignatureContext < Context
    attr_accessor :account, :format

    def self.render(template, agent, recipient, format = 'text/plain', dc_cache = {})
      context    = new(template, agent, recipient, format, dc_cache)
      first_pass = context.render

      return '' if first_pass == '<p></p>'

      if /\{\{.+\}\}/.match?(first_pass)
        # Second pass in case the agent signature contains Liquid.
        # (KB: But this does not render against a TicketContext, so what's the point?)
        context.template = first_pass
        context.render
      else
        first_pass
      end
    end

    def initialize(template, agent, recipient, format, dc_cache = {})
      @agent = agent
      @recipient = recipient
      @account = agent.account
      @format = format
      @dc_cache = dc_cache

      if format == 'text/html'
        template = ZendeskText::Markdown.html(template, ticket_linking: false)
        template.delete!("\n")

        # Markdown messes up stuff inside of liquid tags
        template.gsub!(Liquid::PartialTemplateParser) { |s| CGI.unescapeHTML(s) }
      end

      locale = @recipient.try(:translation_locale)
      super(template, locale)
    end

    def fields
      user = @agent.to_liquid(format)
      {
        'agent'        => user,
        'current_user' => user, # We always want the current_user to be the agent! cf. PR#12026
        'account'      => @account,
        'dc'           => Zendesk::Liquid::CmsDrop.new(self)
      }
    end
  end
end
