module Zendesk::Liquid::CustomFilter
  def url_encode(input)
    Rack::Utils.escape(input)
  end

  def html_safe(input)
    input.html_safe
  end

  def json(input)
    input.to_s.to_json[1...-1] # remove leading and trailing double quotes ", but escape everything else
  end

  def truncate(input, *args)
    super(input.to_s, *args)
  end
end
