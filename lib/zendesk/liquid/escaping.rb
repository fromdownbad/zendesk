module Zendesk::Liquid
  module Escaping
    LIQUID_SECTION_REGEXPS = [/({{.*?}})/, /({%.*?%})/].freeze
    ESCAPED_LIQUID_SECTION_REGEXPS = LIQUID_SECTION_REGEXPS.map { |r| /(\\)#{r}/ }.freeze

    class Codeblock
      attr_reader :codeblock

      def initialize(match)
        @open_tick, @codeblock, @close_tick = match
      end

      def valid?
        LIQUID_SECTION_REGEXPS.any? { |r| r.match(@codeblock) }
      end

      def escaped_codeblock
        @escaped ||= @codeblock.gsub(/([^\\]?){{/, '\1\\{{').gsub(/([^\\]?){%/, '\1\\{%')
      end
    end

    def escape_liquid_in_code_segments
      return unless value.present?

      value.scan(/(<code>|<pre>|`+|~{3,})(.*?)(<\/code>|<\/pre>|`+|~{3,})/im).each do |match|
        cb = Codeblock.new(match)
        next unless cb.valid?
        self.value = value.gsub(cb.codeblock, cb.escaped_codeblock)
      end
    end
  end
end
