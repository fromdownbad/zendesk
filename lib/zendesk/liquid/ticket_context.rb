module Zendesk::Liquid
  class TicketContext < Context
    attr_accessor :account,
      :ticket,
      :current_user,
      :for_agent,
      :updater,
      :skip_attachments

    attr_reader :format

    def self.render(template, ticket, current_user, for_agent, locale = ENGLISH_BY_ZENDESK, format = Mime[:text].to_s, options = {})
      new(template, ticket, current_user, for_agent, locale, format, 0, options).render
    end

    def initialize(template, ticket, current_user, for_agent, locale, format, nesting_level = 0, options = {})
      @ticket                 = ticket
      @account                = options[:account] || ticket.account
      @for_agent              = for_agent
      @format                 = format
      @nesting_level          = nesting_level
      @current_user           = current_user
      @locale                 = locale
      @ticket_url             = options[:ticket_url]
      @dc_cache               = options[:dc_cache] || {}
      @updater                = options[:updater]
      @include_tz             = options[:include_tz]
      @skip_attachments       = options[:skip_attachments] || []
      @custom_fields_cache    = options[:custom_fields_cache] || {}
      @show_last_three_comments = options[:show_last_three_comments]

      # Real quoted email replies-specific options
      @mail_delimiter         = options[:mail_delimiter]
      @mail_footer            = options[:mail_footer]
      @mail_header            = options[:mail_header]
      @mail_locale_for_liquid = options[:mail_locale_for_liquid]

      super(template, locale, options)
    end

    def fields
      {
        'ticket'            => Zendesk::Liquid::TicketDrop.new(ticket, for_agent, format, ticket_drop_options),
        'satisfaction'      => Zendesk::Liquid::SatisfactionRatingDrop.new(ticket, format, current_user, locale),
        'dc'                => Zendesk::Liquid::CmsDrop.new(self),
        'automatic_answers' => Zendesk::Liquid::AnswerBot::Drop.new(ticket, format),
        'answer_bot'        => Zendesk::Liquid::AnswerBot::Drop.new(ticket, format),
        'for_agent'         => for_agent,
        'current_user'      => updater || current_user,
        'updater'           => updater || current_user,
        'account'           => account,
        'locale'            => locale
      }
    end

    def ticket_drop_options
      {
        ticket_url: @ticket_url,
        current_user: @current_user,
        locale: @locale,
        dc_cache: dc_cache,
        filter: has_date_filter?,
        include_tz: @include_tz,
        show_last_three_comments: @show_last_three_comments,
        skip_attachments: skip_attachments,
        custom_fields_cache: @custom_fields_cache
      }
    end

    def has_date_filter? # rubocop:disable Naming/PredicateName
      return false unless template_contains_liquid?
      template =~ /\|.*date:.*\}\}/
    end

    private

    def suppress_placeholder!
      Rails.logger.info("Notification public comments suppressed for ticket nice_id: #{ticket.nice_id}, account id: #{account.id}.")

      super
    end
  end
end
