module Zendesk::Liquid
  class SatisfactionRatingDrop < Liquid::Drop
    def initialize(ticket, format, current_user, locale)
      @ticket = ticket
      @format = format
      @current_user = current_user
      @locale = locale ||
        current_user && current_user.translation_locale ||
        ticket.requester.translation_locale
    end

    # @return [String] An entire preformatted section, including the ticket's
    #                  current rating state and links to both postive and
    #                  negative rating pages.
    def rating_section
      if html?
        "<h3 style='font-size: 1.1em; margin-top: 25px;'>#{translate_csr('txt.satisfaction.prompt.rate')}</h3>#{rating_section_main_content}".html_safe
      else
        "#{translate_csr('txt.satisfaction.prompt.rate')}\n\n" +
          rating_section_main_content
      end
    end

    # return [String] The URL to a bare rating page with no value preselected
    def rating_url
      return nil unless can_expand_placeholder?
      "#{base_url}/requests/#{@ticket.nice_id}/satisfaction/new/#{token.value}?locale=#{@ticket.requester.translation_locale.id}"
    end

    # return [String] The URL to a rating page with the positive value preselected
    def positive_rating_url
      return nil unless can_expand_placeholder?
      rating_url + rating_url_part('good', SatisfactionType.GOOD)
    end

    # return [String] The URL to a rating page with the negative value preselected
    def negative_rating_url
      return nil unless can_expand_placeholder?
      rating_url + rating_url_part('poor', SatisfactionType.BAD)
    end

    # return [String] the current rating, formatted, or nil if the ticket
    #                 has never been rated.
    def current_rating
      return nil if latest_satisfaction_event.blank?
      if html?
        "<span #{rating_style}>#{rating_text}</span>".html_safe
      else
        "\"#{rating_text}\""
      end
    end

    def current_comment
      return nil if latest_satisfaction_event.blank? || @ticket.satisfaction_comment.blank?
      @ticket.satisfaction_comment
    end

    def to_s
      ""
    end

    protected

    def base_url
      (@ticket.brand || @ticket.account).url
    end

    def can_expand_placeholder?
      !@ticket.new_record? && token.present?
    end

    def html?
      @format == 'text/html'
    end

    def rating_url_part(_word, score)
      "&intention=#{score}"
    end

    def token
      @token ||= Tokens::SatisfactionRatingToken.find_or_create_by_ticket(@ticket)
    end

    def latest_satisfaction_event
      @latest_satisfaction_event ||= [SatisfactionType.UNOFFERED, SatisfactionType.OFFERED, nil].include?(@ticket.satisfaction_score) ? nil : @ticket.latest_satisfaction_rating
    end

    def rating_section_main_content
      if latest_satisfaction_event.present?
        rated_main_content
      else
        unrated_main_content
      end
    end

    def unrated_main_content
      if html?
        "<p>" \
        "  <a href='#{positive_rating_url}' style='font-size: 1em; font-family: Arial, Helvetica, sans-serif; color: #1f73b7'>#{translate_csr('txt.satisfaction.score.good')}</a>" \
        "</p>" \
        "<p style='margin-bottom: 30px;'>" \
        "  <a href='#{negative_rating_url}' style='font-size: 1em; font-family: Arial, Helvetica, sans-serif; color: #1f73b7'>#{translate_csr('txt.satisfaction.score.poor')}</a>" \
        "</p>"
      else
        "#{translate_csr('txt.satisfaction.plain.rate')}\n\n" \
          "  #{rating_url}"
      end
    end

    def rated_main_content
      if html?
        "#{translate_csr('txt.satisfaction.score.current', score: rating_text)}\n\n" \
        "<a href='#{rating_url}'>#{translate_csr('txt.satisfaction.html.change_rating')}</a>\n\n"
      else
        "#{translate_csr('txt.satisfaction.score.current', score: rating_text)}\n\n" \
        "#{translate_csr('txt.satisfaction.plain.change_rating')}\n\n" \
          "  #{rating_url}"
      end
    end

    def rating_text
      return nil if latest_satisfaction_event.blank?
      case latest_satisfaction_event[:score].to_i
      when SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT
        translate_csr('txt.satisfaction.score.good')
      when SatisfactionType.BAD, SatisfactionType.BADWITHCOMMENT
        translate_csr('txt.satisfaction.score.poor')
      else
        "Error: unknown satisfaction score #{latest_satisfaction_event[:score]}"
      end
    end

    def rating_style
      return nil if latest_satisfaction_event.blank?
      result = 'style="'
      case latest_satisfaction_event[:score].to_i
      when SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT
        result << 'color: rgb(152,195,50);'
      when SatisfactionType.BAD, SatisfactionType.BADWITHCOMMENT
        result << 'color: rgb(232,42,42);'
      else
        result << 'color: #444;'
      end
      result << '"'
      result
    end

    def translate_csr(key, options = {})
      I18n.t(key, {locale: @locale}.merge(options))
    end
  end
end
