module Zendesk::Liquid
  class Scanner
    def self.scan(template, namespace)
      new(template, namespace).scan
    end

    def initialize(template, namespace)
      @template = template
      @namespace = namespace
    end

    def scan
      scan_nodes(@template.root.nodelist)
    end

    private

    def scan_nodes(nodes)
      result = []
      nodes.each do |node|
        if [Liquid::Variable, Liquid::VariableLookup].include?(node.class)
          lookup = node.class == Liquid::Variable ? node.name : node
          next if lookup.nil?            # e.g. "{{}}"
          next if lookup.is_a?(String)   # e.g. "{{ 'now' | date: '%Y' }}

          if @namespace == lookup.name
            result << lookup.lookups[0]
          end
        elsif node.class == Liquid::Case
          result += scan_case_block(node)
        end
      end
      result
    end

    def scan_case_block(case_block_node)
      # scan 'when' conditions
      result = scan_nodes(case_block_node.blocks.map(&:left).compact)
      result += scan_nodes(case_block_node.blocks.map(&:right).compact)
      # scan each body block
      case_block_node.nodelist.each do |bodyblock|
        result += scan_nodes(bodyblock.nodelist)
      end
      result
    end
  end
end
