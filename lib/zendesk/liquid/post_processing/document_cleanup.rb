module Zendesk
  module Liquid
    module PostProcessing
      class DocumentCleanup
        attr_accessor :context, :output
        delegate :content_type?, :nested?, to: :context

        def initialize(context, output)
          @context = context
          @output = output
        end

        def apply
          return output unless applies?
          document = Nokogiri::HTML::DocumentFragment.parse(output).to_xhtml
          document.gsub!(/>\n\s*</, "><")
          ZendeskText::Markdown::Boundaries.remove_markers(document)
        end

        private

        def applies?
          content_type?(Mime[:html]) && !nested?
        end
      end
    end
  end
end
