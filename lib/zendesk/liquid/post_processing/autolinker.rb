module Zendesk
  module Liquid
    module PostProcessing
      class Autolinker
        attr_accessor :context, :output
        delegate :content_type?, :nested?, to: :context

        def initialize(context, output)
          @context = context
          @output = output
        end

        def apply
          return output unless applies?
          output.auto_link_urls
        end

        private

        def applies?
          !context.skip_auto_link && content_type?(Mime[:html])
        end
      end
    end
  end
end
