module Zendesk
  module Liquid
    module PostProcessing
      class SimpleFormatter
        attr_accessor :context, :output
        delegate :content_type?, :nested?, :rich_comment?, to: :context

        def initialize(context, output)
          @context = context
          @output = output
        end

        def apply
          return output unless applies?
          split_on_preserved_parts(output).map! { |chunk| format(chunk) }.join("")
        end

        private

        def applies?
          (rich_comment? || (content_type?(Mime[:html]) && !nested?)) && contains_newlines?(output)
        end

        def contains_html?(text)
          !!(text =~ /<[\/]?[A-Za-z0-9\ ]+>/) ||
            !!(text =~ /&[^; ]{1,5};/)
        end

        def contains_newlines?(text)
          !!(text =~ /\n/)
        end

        def split_on_preserved_parts(output)
          output.split(ZendeskText::Markdown::Boundaries::REGEXP)
        end

        def formattable_chunk?(chunk)
          !chunk.starts_with?(ZendeskText::Markdown::Boundaries::START)
        end

        def format(chunk)
          return chunk unless formattable_chunk?(chunk)
          return chunk.simple_format.delete("\n") unless contains_html?(chunk)
          # Rails3 sanitizes by default, which breaks formatting, so turn off sanitization for now.
          chunk.gsub(/\n\n$/m, "\n").html_safe.simple_format({}, sanitize: false).delete("\n")
        end
      end
    end
  end
end
