module Zendesk::Liquid
  # The only reason it's here is to provide a sensible to_s on the return object from to_liquid.
  # See ticket_drop_test for use case and the below link for stupid:
  # https://github.com/tobi/liquid/wiki/Using-Liquid-without-Rails

  # Update 2014-08-26
  # The above link is dead but this PR has screenshots and a link:
  # https://github.com/zendesk/zendesk/pull/12535#issuecomment-53515415

  class Wrapper < Hash
    def initialize(to_s_key)
      @to_s_key = to_s_key
    end

    def to_s
      self[@to_s_key]
    end
  end
end
