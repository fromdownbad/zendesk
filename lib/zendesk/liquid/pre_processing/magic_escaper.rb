module Zendesk
  module Liquid
    module PreProcessing
      class MagicEscaper
        MARKDOWN_CODE_REGEXPS = /(`+|~{3,})(.*?)(`+|~{3,})/im
        RICH_TEXT_CODE_REGEXPS = /(<code>|<pre>)(.*?)(<\/code>|<\/pre>)/im
        LIQUID_SECTION_REGEXPS = [/({%.*?%})/, /({{.*?}})/].freeze
        ESCAPED_LIQUID_SECTION_REGEXPS = LIQUID_SECTION_REGEXPS.map { |r| /(\\)#{r}/ }.freeze

        delegate :content_type?, :nested?, to: :@context

        class Codeblock
          attr_reader :codeblock

          def initialize(match)
            @open_tick, @codeblock, @close_tick = match
          end

          def valid?
            LIQUID_SECTION_REGEXPS.any? { |r| r.match(@codeblock) }
          end

          def escaped_codeblock
            @escaped ||= @codeblock.gsub(/([^\\]?){{/, '\1\\{{').gsub(/([^\\]?){%/, '\1\\{%')
          end
        end

        def initialize(context, input)
          @context = context
          @input = input
        end

        def apply
          return @input unless applies?
          escape_liquid_in_code_segments
          escape_liquid_tags_in_input
          replace_html_codes_in_liquid_blocks if account_has_rich_text_comments?

          @input
        end

        private

        def applies?
          content_type?(Mime[:text])
        end

        def account_has_markdown_comments?
          @context.account.try(:settings) && @context.account.settings.markdown_ticket_comments?
        end

        def account_has_rich_text_comments?
          @context.account.try(:settings) && @context.account.settings.rich_text_comments?
        end

        def replace_html_codes_in_liquid_blocks
          html_code_replacements = [
            ['&lt;', '<'],
            ['&gt;', '>'],
            ['&amp;', '&']
          ]
          LIQUID_SECTION_REGEXPS.each do |regex|
            @input.scan(regex).each do |match|
              html_code_replacements.each do |html, char|
                next unless @input.include? html
                fixed_match = match.first.gsub(html, char)
                # block format necessary to handle gsub's poor handling of \ characters
                @input.gsub!(match.first) { fixed_match }
              end
            end
          end
        end

        def escape_liquid_in_code_segments
          formats = []
          formats << MARKDOWN_CODE_REGEXPS if account_has_markdown_comments?
          formats << RICH_TEXT_CODE_REGEXPS if account_has_rich_text_comments?

          formats.each { |format| escape_liquid(format) }
        end

        def escape_liquid(format_regexp)
          @input.gsub!(format_regexp) do |match|
            cb = Codeblock.new([$1, $2, $3])
            cb.valid? ? "#{$1}#{cb.escaped_codeblock}#{$3}" : match
          end
        end

        def escape_liquid_tags_in_input
          ESCAPED_LIQUID_SECTION_REGEXPS.each do |pattern|
            @input.gsub!(pattern, '{% raw %}\2{% endraw %}')
          end
        end
      end
    end
  end
end
