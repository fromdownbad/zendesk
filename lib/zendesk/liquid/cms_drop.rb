module Zendesk::Liquid
  class CmsDrop < Liquid::Drop
    include ::Zendesk::Liquid::DropHelpers

    attr_accessor :rendering_context

    def initialize(rendering_context)
      @rendering_context = rendering_context
    end

    def liquid_method_missing(cms_text_identifier)
      content = rendering_context.dc_cache.fetch(cms_text_identifier) { |id| cms_content(id) }
      return '' if content.blank?

      html_string(render(content))
    end

    def to_s
      ""
    end

    private

    def render(content)
      rendered_template = rendering_context.without_auto_linking do
        rendering_context.template = content

        rendering_context.render
      end

      return process_markdown(rendered_template) if process_markdown?

      rendered_template
    end

    def html_string(content)
      return '' if content.blank?

      rendering_context.content_type?(Mime[:html]) ? content : content.html_safe
    end

    def process_markdown(template)
      Nokogiri::HTML::DocumentFragment.parse(
        ZendeskText::Markdown::Boundaries.
          remove_markers(ZendeskText::Markdown.html(template)).
          delete("\n")
      ).tap do |fragment|
        fragment.
          children.
          each do |element|
            next unless element.name == 'p'

            element.before('<br>') if element.previous.present?
            element.after('<br>')  if element.next.present?
            element.replace(element.children)
          end
      end.to_xhtml
    end

    def cms_content(identifier)
      cms_text = begin
        rendering_context.
          account.
          cms_texts.
          where(identifier: identifier).
          first
      end

      return nil unless cms_text.present?

      cms_text.smart_fetch(rendering_context.locale).try(:value)
    end

    def process_markdown?
      rendering_context.render_dc_markdown? &&
        rendering_context.nesting_level <= 1
    end
  end
end
