module Zendesk
  module Liquid
    module Footer
      class FooterDrop
        SYSTEM_FONT_STACK = "'system-ui','-apple-system','BlinkMacSystemFont','Segoe UI','Roboto','Oxygen-Sans','Ubuntu','Cantarell','Helvetica Neue','Arial','sans-serif'".freeze

        BODY_TEMPLATES = {
          html: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "footer.html.erb")).delete("\n"))
        }.freeze

        STATUS_BG_COLORS = {
          closed: "#D8DCDE",    # grey-300
          deleted: "#D8DCDE",   # grey-300
          holding: "#2F3941",   # grey-800
          new: "#FFB648",       # yellow-400
          open: "#E34F32",      # crimson-400
          pending: "#3091EC",   # azure-400
          solved: "#87929D",    # grey-500
          suspended: "#49545C", # grey-700
        }.with_indifferent_access.freeze

        STATUS_COLORS = {
          closed: '#49545C',    # grey-700
          deleted: '#49545C',   # grey-700
          holding: "#fff",      # white
          new: "#2f3941",       # grey-800
          open: "#fff",         # white
          pending: "#fff",      # white
          solved: "#fff",       # white
          suspended: "#fff",    # white
        }.with_indifferent_access.freeze

        STYLES = "
          .simplified-email-footer .light-text {
            color: #333333; }

          .simplified-email-footer span a {
            text-decoration: none; }

          .simplified-email-footer .content {
            padding: 24px 32px; }

          .simplified-email-footer .namecard {
            min-width: 168px; }

          @media only screen and (max-width: 768px) {
            .simplified-email-footer .namecard {
              display: block;
              min-width: 100%;
              padding: 0 0 16px 0; }

            .simplified-email-footer .content {
              padding: 16px; }
          }
        ".freeze

        def initialize(vars)
          status = vars[:ticket_status]
          vars[:ticket_status] = ::I18n.t('txt.admin.helpers.ticket_helper.status_' + status, locale: vars[:locale])
          vars[:ticket_status_bg_color] = STATUS_BG_COLORS[status]
          vars[:ticket_status_color] = STATUS_COLORS[status]
          vars[:font_family] = SYSTEM_FONT_STACK
          vars[:rtl] = vars[:locale].try(:rtl?) || false
          vars[:dir] = vars[:rtl] ? 'rtl' : 'ltr'
          @vars = vars
        end

        def render
          template.result_with_hash(@vars).strip
        end

        private

        def template
          BODY_TEMPLATES[template_id]
        end

        def template_id
          :html
        end
      end
    end
  end
end
