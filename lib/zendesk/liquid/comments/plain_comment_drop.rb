module Zendesk
  module Liquid
    module Comments
      class PlainCommentDrop < CommentDrop
        BODY_TEMPLATES = {
          text: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "comment.text.erb")), nil, '<>'),
          html: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "plain_comment.html.erb")).delete("\n"))
        }.freeze

        def render
          super << "\n\n"
        end

        private

        def template
          BODY_TEMPLATES[template_id]
        end

        def template_id
          Mime[:text] == format ? :text : :html
        end
      end
    end
  end
end
