module Zendesk
  module Liquid
    module Comments
      class PlainCommentBodyDrop
        include ::Zendesk::Liquid::DropHelpers
        include ActionView::Helpers
        include EmojiHelper
        include ERB::Util

        attr_reader :account, :comment, :format, :for_agent, :brand, :rtl, :comment_html_only

        def initialize(account, comment, format, brand, for_agent = false, rtl = false, comment_html_only = false)
          @account   = account
          @comment   = comment
          @format    = format
          @for_agent = for_agent
          @rtl       = rtl
          @brand     = brand
          @comment_html_only = comment_html_only
        end

        def render
          if format == Mime[:html]
            options = {
              base_url:  host,
              for_agent: for_agent,
              for_email: true,
              rtl:       rtl
            }

            comment.override_pre_styled = account.has_email_pre_styled_rich_api_comments? && comment_html_only

            options[:css] = if comment.pre_styled? && account.settings.rich_content_in_emails? && !account.has_email_simplified_threading?
              ZendeskText::MarkdownStyle::PRESTYLED_CSS
            elsif !comment.pre_styled? && (comment.process_as_markdown? || comment.rich? || account.settings.modern_email_template?)
              account.has_email_simplified_threading? ? ZendeskText::MarkdownStyle::DEFAULT_CSS_V2 : ZendeskText::MarkdownStyle::DEFAULT_CSS
            end

            result = comment.html_body(options)

            if comment.rich?
              # we need to pretend that we are outputting rendered markdown to
              # prevent Zendesk::Liquid::PostProcessing::SimpleFormatter from
              # breaking our content.
              result = ZendeskText::Markdown::Boundaries.wrap(result)
            end
          else
            result = comment.body
          end

          result = result.delete("\n") if format == Mime[:html] && !(comment.process_as_markdown? || comment.rich?)
          result
        end

        def host
          @host ||= (for_agent ? account.url(mapped: false) : brand.url)
        end
      end
    end
  end
end
