module Zendesk
  module Liquid
    module Comments
      class CommentDrop
        include ::Zendesk::Liquid::DropHelpers
        include ImagePathHelper
        include ActionView::Helpers
        include ERB::Util

        SYSTEM_FONT_STACK_LEGACY = "'Lucida Grande','Lucida Sans Unicode','Lucida Sans',Verdana,Tahoma,sans-serif".freeze
        SYSTEM_FONT_STACK = "'system-ui','-apple-system','BlinkMacSystemFont','Segoe UI','Roboto','Oxygen-Sans','Ubuntu','Cantarell','Helvetica Neue','Arial','sans-serif'".freeze

        BODY_TEMPLATES = {
          text: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "comment.text.erb")), nil, '<>'),
          html_v1: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "comment_v1.html.erb")).delete("\n")),
          html_v2: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "comment_v2.html.erb")).delete("\n")),
          html_minimal: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "comment_minimal.html.erb")).delete("\n")),
          html_formatted_new: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "comment_formatted_new.html.erb")).delete("\n")),
          html_simplified: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "comment_simplified.html.erb")).delete("\n"))
        }.freeze

        attr_reader :account,
          :brand,
          :comment,
          :comment_html_only,
          :current_user,
          :dir,
          :font_family,
          :for_agent,
          :format,
          :last_three_comments,
          :rtl,
          :show_last_three_comments,
          :simplified_comment,
          :skip_attachments,
          :skip_header

        def initialize(account, comment, format, for_agent, current_user, brand, options = {})
          @account = account
          @brand = brand
          @comment = comment
          @comment_html_only = !!options[:comment_html_only]
          @current_user = current_user
          @font_family = account.has_email_simplified_threading? ? SYSTEM_FONT_STACK : SYSTEM_FONT_STACK_LEGACY
          @for_agent = for_agent
          @format = format
          @include_tz = options[:include_tz]
          @skip_attachments = options[:skip_attachments] || []
          @rtl = options[:rtl]
          @simplified_comment = !!options[:simplified_comment]
          @show_last_three_comments = !!options[:show_last_three_comments]
          @last_three_comments = options[:last_three_comments]
          @skip_header = !!options[:skip_header]
          @time_zone, translation_locale = if @current_user && @current_user.time_zone && !@current_user.is_system_user?
            [@current_user.time_zone, @current_user.translation_locale]
          else
            [@account.time_zone, @account.translation_locale]
          end
          @locale = translation_locale.locale
          @rtl ||= translation_locale.rtl?
          @dir = @rtl ? "rtl" : "ltr"
        end

        def render
          template.result(binding).strip
        end

        def date
          return "" if comment.created_at.blank?
          return "" unless formatter

          formatter.format(comment.created_at)
        end

        def author_name
          if comment.is_a?(FacebookComment) && comment.data[:requester].present?
            comment.data[:requester][:name]
          else
            comment.author.safe_name(for_agent).html_safe
          end
        end

        def author_photo
          if comment.is_a?(FacebookComment) && comment.data[:requester].present?
            "http://graph.facebook.com/v2.9/#{comment.data[:requester][:id]}/picture?type=large"
          else
            photo_thumb_path_for_email(comment.author)
          end
        end

        def host
          @host ||= account.url # used by Inbox
        end

        def header
          return "" if @skip_header
          key = "txt.email.comment_formatting"
          key << (Mime[:html] == format ? ".html_email" : ".plain_text")
          key << (comment.is_public? ? ".public" : ".private")

          # Escaping is a concern of the template, not i18n.
          CGI.unescapeHTML(::I18n.t(key, author_name: author_name, date: date))
        end

        def rendered_body
          @rendered_body ||= begin
            if comment.is_a?(VoiceComment)
              VoiceCommentBodyDrop.new(account, comment, format).timed_render
            else
              PlainCommentBodyDrop.new(account, comment, format, brand, for_agent, rtl, comment_html_only).timed_render
            end
          end
        end

        def attachments
          comment.attachments.reject(&:inline) - skip_attachments
        end

        def screencasts
          comment.screencasts
        end

        def attachments_header
          ::I18n.t("txt.uploads.header")
        end

        def show_thumbnail?
          account.settings.modern_email_template? && account.settings.email_template_photos?
        end

        def internal_note
          ::I18n.t("txt.email.rule_notification.internal_note")
        end

        def private_note
          ::I18n.t("txt.email.rule_notification.private_note")
        end

        def agent_label
          ::I18n.t('txt.admin.helpers.rules_helper.account_label', account_name: brand.name)
        end

        def last_attachment?(index, attachments)
          index == attachments.length - 1
        end

        def alert_warning_icon
          margin_property = rtl ? "left" : "right"

          "<span style=\"margin-#{margin_property}:10px;\"><img style=\"margin-bottom:-2px;height:14px;width:14px;\" src=\"https://assets.#{ENV.fetch('ZENDESK_HOST')}/images/icons/alert-warning-stroke.png\" /></span>"
        end

        # Keep in sync with https://github.com/zendesk/zendesk_console/blob/ced94561c13899f7d91ac25026ba85f5faae0e4a/lotus_react/src/components/AttachmentThumbnails/utils.js#L13-L21
        def file_icon(attachment)
          image = if attachment.file_type == 'document'
            "file-document-stroke.png"
          elsif attachment.file_type == 'image'
            "file-image-stroke.png"
          elsif attachment.file_type == 'pdf'
            "file-pdf-stroke.png"
          elsif attachment.file_type == 'presentation'
            "file-presentation-stroke.png"
          elsif attachment.file_type == 'spreadsheet'
            "file-spreadsheet-stroke.png"
          elsif attachment.file_type == 'zip'
            "file-zip-stroke.png"
          else
            "file-generic-stroke.png"
          end

          output = <<-HTML
            <div style="margin:24px auto 0;text-align: center;"><img alt="#{attachment.display_filename}" style="height:24px;width:24px;" src="https://assets.#{ENV.fetch('ZENDESK_HOST')}/images/icons/#{image}" /></div>
          HTML

          output.html_safe
        end

        def flags
          comment.flagged_reasons.map do |flagged_reason|
            flag_type = EventFlagType.fuzzy_find(flagged_reason)
            learn_more_link = ::I18n.t("ticket.comment.flagged_link_#{flagged_reason}")
            learn_more = if account.has_email_simplified_threading?
              "<a style=\"color:inherit;\" href=\"#{learn_more_link}\" target=\"_blank\">#{::I18n.t('ticket.comment.flagged_learn_more')}</a>"
            else
              "<a href=\"#{learn_more_link}\" target=\"_blank\">#{::I18n.t('ticket.comment.flagged_learn_more')}</a>"
            end
            flag_message_options = flags_options[flag_type.to_s].try(:fetch, 'message', {}) || {}
            translation_options  = flag_message_options.merge(learn_more: learn_more, comment_author: comment.author.name)

            ::I18n.t("ticket.comment.flagged_reason_#{flagged_reason}", translation_options)
          end
        end

        private

        def formatter
          return unless @time_zone.present? && @locale.present?
          return @formatter if @formatter

          # Keys are Rails TimeZone names, values are TZInfo identifiers.
          # https://github.com/rails/rails/blob/v6.0.0/activesupport/lib/active_support/values/time_zone.rb#L31-L183
          time_zone = ActiveSupport::TimeZone::MAPPING.value?(@time_zone) ? @time_zone : ActiveSupport::TimeZone::MAPPING[@time_zone]

          @formatter ||= ICU::TimeFormatting.create(locale: @locale, zone: time_zone, date: :medium, time: @include_tz ? :long : :short).tap do |f|
            f.set_date_format(true, f.date_format.gsub(/:ss/, ""))
            f.set_date_format(true, f.date_format.tr('h', "H").gsub(/\s*a/, '')) unless account.uses_12_hour_clock?
          end
        end

        # Needed for assets_tag_helper on rails 3 <-> test/unit/liquid/template_test.rb
        def config
          Rails.application.config
        end

        def flags_options
          @flags_options ||= comment.audit.try(:flags_options) || {}
        end

        def template
          BODY_TEMPLATES[template_id]
        end

        def template_id
          if Mime[:text] == format then :text
          elsif simplified_comment then :html_simplified
          elsif show_last_three_comments then :html_formatted_new
          elsif comment_html_only then :html_minimal
          elsif account.settings.modern_email_template? then :html_v2
          else :html_v1
          end
        end
      end
    end
  end
end
