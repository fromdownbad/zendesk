module Zendesk
  module Liquid
    module Comments
      class VoiceCommentBodyDrop
        include ::Zendesk::Liquid::DropHelpers
        include ::Voice::Core::NumberSupport
        include TimeDateHelper

        BODY_TEMPLATES = {
          html: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "voice_comment_body.html.erb")).delete("\n")),
          text: ERB.new(File.read(File.join(File.dirname(__FILE__), "views", "voice_comment_body.text.erb")), nil, '<>')
        }.freeze

        attr_reader :account, :comment, :format

        def initialize(account, comment, format)
          @account  = account
          @comment  = comment
          @format   = format
        end

        def render
          template.result(binding)
        end

        private

        def template
          BODY_TEMPLATES[template_id]
        end

        def template_id
          Mime[:text] == format ? :text : :html
        end
      end
    end
  end
end
