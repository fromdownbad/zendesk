module Zendesk
  module Liquid
    module Comments
      # This is the primary interface for rendering comment liquid placeholders
      class CollectionDrop
        include ::Zendesk::Liquid::DropHelpers

        attr_reader :account,
          :brand,
          :comments,
          :current_user,
          :format,
          :for_agent,
          :include_tz,
          :mail_delimiter,
          :mail_footer,
          :mail_header,
          :mail_locale_for_liquid,
          :show_last_three_comments,
          :skip_attachments

        def initialize(account, comments, format, for_agent, current_user, brand, options = {})
          @account = account
          @comments = comments.compact
          @format = format
          @for_agent = for_agent
          @current_user = current_user
          @brand = brand
          @rtl = options[:locale].try(:rtl?) || false
          @include_tz = options[:include_tz]
          @skip_attachments = options[:skip_attachments] || []
          @show_last_three_comments = options[:show_last_three_comments] || false
          @mail_delimiter = options[:mail_delimiter] || nil
          @mail_footer = options[:mail_footer] || nil
          @mail_header = options[:mail_header] || nil
          @mail_locale_for_liquid = options[:mail_locale_for_liquid] || nil
        end

        def core_render(comments)
          output = ""

          comments.select(&:present?).each do |comment|
            output << CommentDrop.new(
              account,
              comment,
              format,
              for_agent,
              current_user,
              brand,
              rtl: @rtl,
              include_tz: @include_tz,
              skip_attachments: skip_attachments,
              show_last_three_comments: show_last_three_comments
            ).render << "\n\n"
          end

          output
        end

        def render
          return "" if comments.blank?

          output = core_render(comments)
          output.strip!

          if Mime[:html] == format
            if account.settings.modern_email_template?
              output = if show_last_three_comments
                "<div style=\"margin-top: 20px;margin-bottom: 20px; border:5px WhiteSmoke solid;\" data-version=\"2\">#{output}</div>"
              else
                "<div style=\"margin-top: 25px\" data-version=\"2\">#{output}</div>"
              end
            end

            output
          else
            output.html_safe
          end
        end
      end

      # Blockquote all previous replies on one level. Zendesk Formatted.
      class CollectionTopQuotedDrop < CollectionDrop
        def text_quote(text)
          # Put ">" in front of every line
          text.gsub("\n", "\n>")
        end

        def top_quoted_comments(comments)
          output = ''

          if comments.any?
            if Mime[:html] == format
              output << '<div class="gmail_quote">'
              output << '  <blockquote class="gmail_quote">'
              comments.each do |comment|
                if comment.present?
                  output << CommentDrop.new(account, comment, format, for_agent, current_user, brand, rtl: @rtl, include_tz: @include_tz, skip_attachments: skip_attachments).timed_render << "\n\n"
                end
              end
              output << '  </blockquote>'
              output << '</div>'
            else
              comments.each do |comment|
                if comment.present?
                  output << CommentDrop.new(account, comment, format, for_agent, current_user, brand, rtl: @rtl, include_tz: @include_tz, skip_attachments: skip_attachments).timed_render << "\n\n"
                end
              end
              output = text_quote(output)
            end
          end

          output
        end

        def core_render(comments)
          output = CommentDrop.new(account, comments.shift, format, for_agent, current_user, brand, rtl: @rtl, include_tz: @include_tz, skip_attachments: skip_attachments).timed_render << "\n\n"
          output << top_quoted_comments(comments)
          output
        end
      end

      # Blockquote all previous replies recursively (nested). Plain email formatted.
      class CollectionDeepQuotedDrop < CollectionTopQuotedDrop
        def plain_comment_drop(comment)
          PlainCommentDrop.new(account, comment, format, for_agent, current_user, brand, skip_attachments: skip_attachments)
        end

        def quoted_comments(comments)
          return '' if comments.blank?

          comment = comments.shift
          comment_drop = plain_comment_drop(comment)

          if Mime[:html] == format
            '<div class="gmail_quote">' \
              "On #{comment_drop.date}, #{comment_drop.author_name}#{' (Team)' if comment.author.is_agent? && for_agent} <#{comment.author.email}> wrote" + '<br>'.html_safe +
              '<blockquote class="gmail_quote" style="margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex">' +
                comment_drop.timed_render + quoted_comments(comments) +
              '</blockquote>' \
            '</div>'
          else
            text_quote(comment_drop.timed_render + quoted_comments(comments))
          end
        end

        def core_render(comments)
          plain_comment_drop(comments.shift).timed_render + quoted_comments(comments)
        end
      end
    end
  end
end
