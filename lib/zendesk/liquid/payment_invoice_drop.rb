module Zendesk::Liquid
  class PaymentInvoiceDrop < Liquid::Drop
    def initialize(invoice, format)
      @invoice       = invoice
      @payment       = invoice.payment
      @account       = @payment.account
      @format        = format
    end

    def address_lines
      @account.address.lines.join("\n")
    end

    def order_lines
      lines = []
      lines << row("#{@account.subscription.plan_name} with #{@payment.max_agents} agent#{'s' unless @payment.max_agents == 1}", "USD %.2f" % @payment.amount)
      lines << discount_line
      lines << line_separator
      lines << total_line
      lines << line_separator

      if @format == Mime[:html]
        "<table>#{lines.join}</table>"
      else
        lines.join("\n") + "\n"
      end
    end

    def account_name
      @payment.account.name
    end

    def invoice_id
      @payment.invoice_id
    end

    def from
      @payment.from
    end

    def to
      @payment.to
    end

    def liquid_method_missing(name)
      "LIQUID TEMPLATE ERROR '#{name}'" unless respond_to?(name)
    end

    def to_s
      ""
    end

    private

    def line_separator
      (@format == Mime[:text] ? '-' * 70 : "<tr><td colspan='2'><hr style='border:1px dashed black'/></td></tr>")
    end

    def discount_line
      pricing_model = PricingModelFactory.instance_factory(@payment)

      if @payment.discount.to_i > 0
        if pricing_model.cycle_discount_percent > 0
          kind = "Billing cycle discount"
          if @payment.manual_discount.to_i > 0
            kind = "Total discount"
          end
        else
          kind = "Discount"
        end

        row(kind, 'USD %.2f' % @payment.discount)
      end
    end

    def total_line
      row("Total", "USD %.2f" % @payment.total)
    end

    def row(left, right)
      if @format == Mime[:html]
        html_format(left, right)
      else
        text_format(left, right)
      end
    end

    def text_format(left, right)
      left + right.rjust(70 - left.size)
    end

    def html_format(left, right)
      "<tr><td style='width:400px'>#{left}</td><td style='text-align:right'>#{right}</td></tr>"
    end
  end
end
