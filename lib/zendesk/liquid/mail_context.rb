module Zendesk::Liquid
  class MailContext < Context
    include AccountsHelper

    attr_reader :translation_locale

    def format
      Mime[:text]
    end

    def self.render(template:, header:, content:, footer:, account: nil, delimiter: '', footer_link: '', attributes: '', styles: '', translation_locale: nil, suppress_placeholder: false)
      new(
        template:             template,
        header:               header,
        content:              content,
        footer:               footer,
        account:              account,
        delimiter:            delimiter,
        footer_link:          footer_link,
        attributes:           attributes,
        styles:               styles,
        translation_locale:   translation_locale,
        suppress_placeholder: suppress_placeholder
      ).render
    end

    def initialize(template:, header:, content:, footer:, account:, delimiter:, footer_link:, attributes:, styles:, translation_locale:, suppress_placeholder: false)
      @header             = header
      @content            = content
      @footer             = footer
      @account            = account
      @delimiter          = account&.has_no_mail_delimiter_enabled? ? '' : delimiter
      @translation_locale = translation_locale || I18n.translation_locale
      @direction          = get_direction
      @footer_link        = get_footer_link(footer_link)
      @attributes         = get_attributes(attributes)
      @styles             = get_styles(styles)

      options = {
        suppress_placeholder: suppress_placeholder
      }

      super(template, @translation_locale, options)
    end

    def fields
      {
        "header" => @header,
        "content" => @content,
        "footer" => @footer,
        "direction" => @direction,
        "footer_link" => @footer_link,
        "delimiter" => @delimiter,
        "attributes" => @attributes,
        "styles" => @styles,
      }
    end

    private

    def get_footer_link(footer_link)
      return footer_link unless footer_link.blank?

      params               = {utm_medium: "poweredbyzendesk", utm_source: "email-notification", utm_campaign: "text" }
      params[:utm_content] = @account.name if @account
      email_url            = add_tracking_params I18n.t("txt.email.email_landing_page_url", locale: @translation_locale), params
      zendesk_url          = '<a href="%s" style="color:black" target="_blank">Zendesk</a>' % [email_url]

      delivered_by_link = I18n.t("txt.email.branded_delivered_by_url", url: zendesk_url, locale: @translation_locale)

      delivered_by_link
    end

    def get_attributes(attributes)
      if @translation_locale.is_a?(TranslationLocale) && @translation_locale.rtl?
        "dir='rtl' #{attributes}"
      else
        attributes
      end
    end

    def get_styles(styles)
      ["body[dir=rtl] .directional_text_wrapper { direction: rtl; unicode-bidi: embed; }"].tap do |style_list|
        style_list << Footer::FooterDrop::STYLES if @account&.has_email_simplified_threading?
        style_list << styles
      end.join("\n")
    end

    def get_direction # rubocop:disable Naming/AccessorMethodName
      if @account.try(:has_rtl_lang_html_attr?) && @translation_locale.is_a?(TranslationLocale) && translation_locale.try(:rtl?)
        "right"
      else
        "left"
      end
    end
  end
end
