module Zendesk::Liquid
  class DcContext < Context
    attr_accessor :account, :current_user, :nesting_level, :format, :dc_cache

    def self.render(template, current_account, current_user, format = 'text/plain', locale = nil, dc_cache = {}, render_dc_markdown = false)
      new(template, current_account, current_user, format, locale: locale, dc_cache: dc_cache, render_dc_markdown: render_dc_markdown).render
    end

    def initialize(template, current_account, current_user, format, options = {})
      @account       = current_account
      @current_user  = current_user
      @nesting_level = options[:nesting_level] || 0
      @format        = format
      @dc_cache      = options[:dc_cache] || {}

      locale = if options[:locale]
        options[:locale]
      elsif @current_user
        @current_user.translation_locale
      else
        I18n.translation_locale
      end

      super(template, locale, options)
    end

    def fields
      {
        'dc'           => Zendesk::Liquid::CmsDrop.new(self),
        'current_user' => @current_user,
        'account'      => @account
      }
    end
  end
end
