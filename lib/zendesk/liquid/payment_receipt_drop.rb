module Zendesk::Liquid
  class PaymentReceiptDrop < Liquid::Drop
    def initialize(payment, format)
      @payment       = payment
      @account       = payment.account
      @format        = format
      @order_lines   = []
    end

    def payment_info
      return "Your credit card #{card_info} has been charged USD #{total} on #{charged}" if @payment.paid_by_credit_card?
      return "Your payment for the amount of USD #{total} was received on #{charged}" if @payment.paid_by_invoice?
    end

    def amount
      "%.2f" % @payment.amount
    end

    def total
      "%.2f" % @payment.total
    end

    def type
      @payment.voice_payment? ? "Voice" : ""
    end

    def card_info
      @payment.card_info
    end

    def charged
      @payment.charged_at.strftime('%Y-%m-%d')
    end

    def address_lines
      lines = @account.address.lines.join("\n")
      @format == Mime[:html] ? lines.simple_format.html_safe : lines
    end

    def order_lines
      order_line
      discount_line
      line_separator
      total_line
      line_separator

      if @format == Mime[:html]
        result = "<table>#{@order_lines.join}</table>"
        result << "<p>#{amex_notice_text}</p>" if @account.subscription.credit_card.card_type == CreditCardType.AMERICAN_EXPRESS
        result << "<p>#{danish_payment_notice_text}</p>" if @account.subscription.credit_card.is_danish_payment?
      else
        result = @order_lines.join("\n") + "\n"
        result << "\n#{amex_notice_text}\n" if @account.subscription.credit_card.card_type == CreditCardType.AMERICAN_EXPRESS
        result << "\n#{danish_payment_notice_text}\n" if @account.subscription.credit_card.is_danish_payment?
      end

      result.html_safe
    end

    def liquid_method_missing(method)
      @payment.send(method.to_s) unless %w[amount charged total address_lines order_lines payment_info type].index(method.to_s)
    end

    def to_s
      ""
    end

    private

    def order_line
      if @payment.standard_payment?
        @order_lines << row("#{@account.subscription.plan_name} with #{@payment.max_agents} agent#{'s' unless @payment.max_agents == 1}", "USD %.2f" % @payment.amount)
      end
    end

    def line_separator
      @order_lines << (@format == Mime[:text] ? '-' * 70 : "<tr><td colspan='2'><hr style='border:1px dashed black'/></td></tr>")
    end

    def discount_line
      pricing_model = PricingModelFactory.instance_factory(@payment)

      if @payment.discount.to_i > 0
        if pricing_model.cycle_discount_percent > 0
          kind = "Billing cycle discount"
          if @payment.manual_discount.to_i > 0
            kind = "Total discount"
          end
        else
          kind = "Discount"
        end

        @order_lines << row(kind, 'USD %.2f' % @payment.discount)
      end
    end

    def total_line
      @order_lines << row("Total", "USD %.2f" % @payment.total)
    end

    def row(left, right)
      if @format == Mime[:html]
        html_format(left, right)
      else
        text_format(left, right)
      end
    end

    def text_format(left, right)
      left + right.rjust(70 - left.size)
    end

    def html_format(left, right)
      "<tr><td style='width:400px'>#{left}</td><td style='text-align:right'>#{right}</td></tr>"
    end

    def amex_notice_text
      "You have registered an American Express card for paying for Zendesk. "\
      "The danish payment services vendor only supports danish crowns (DKR) "\
      "as currency for charges made to American Express cards. Thus you will "\
      "see an amount in DKR on your banking statements, rather than USD. This "\
      "amount is approximately DKR #{'%.2f' % @payment.total(Currency.DKK)} using the exchange rate of the time of "\
      "billing. You can verify the correctness of this amount using this link: "\
      "http://www.xe.com/ucc/convert.cgi?Amount=#{@payment.total}&From=USD&To=DKK"
    end

    def danish_payment_notice_text
      "You are a danish customer. All charges to danish customers paying with "\
      "Dankort or VISA/Dankort are made in DKR. Thus the amount withdrawn from your "\
      "account by Zendesk appears as approximately DKR #{'%.2f' % @payment.total(Currency.DKK)}, using the "\
      "exchange rate of the time of billing. You can verify the correctness of this "\
      "amount using this link: http://www.xe.com/ucc/convert.cgi?Amount=#{@payment.total}&From=USD&To=DKK"
    end
  end
end
