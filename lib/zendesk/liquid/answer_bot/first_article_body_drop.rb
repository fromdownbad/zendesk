module Zendesk::Liquid::AnswerBot
  class FirstArticleBodyDrop
    include ::Zendesk::Liquid::DropHelpers
    include WhitespaceSanitizationHelper

    ICON_SRC = '/classic/images/icons/article.png'.freeze

    def initialize(articles:, url_helper:, font_family:)
      @articles = articles
      @url_helper = url_helper
      @font_family = font_family
    end

    def render
      return "" if articles.blank?

      strip_and_compress_lines(render_html(articles.first)).html_safe
    end

    private

    attr_reader :articles, :url_helper, :font_family

    def render_html(article)
      html = <<-HTML
        <!--[if (gte mso 9)|(IE)]>
        <table style="width: 720px;"><tbody><tr><td>
         <![endif]-->

        <table style="font-family:#{font_family};width: 100%; max-width: 720px; border-collapse: collapse;" bgcolor="#f3f3f3">
          <tr>
            <td colspan="6" height="40">&nbsp;</td>
          </tr>
          <tr>
            #{left_border}
            #{title_row(article.fetch(:title))}
            #{right_border}
          </tr>
          <tr>
            #{left_border}
            #{article_body_row(article.fetch(:body))}
            #{right_border}
          </tr>
          <tr>
            #{left_border}
            #{answer_question_row}
            #{right_border}
          </tr>
          <tr>
            #{left_border}
            #{cta_buttons_row(article)}
            #{right_border}
          </tr>
          <tr>
            <td colspan="6" height="40">&nbsp;</td>
          </tr>
        </table>

        <!--[if (gte mso 9)|(IE)]>
        </td></tr></tbody></table>
        <![endif]-->
      HTML

      Zendesk::HtmlEmail::RelativeToAbsoluteUrlConverter.convert(html, url_helper.host)
    end

    def left_border
      <<-HTML
        <td style="width: 5%; min-width: 10px;"></td>
        <td style="width: 3.5%; min-width: 10px;" bgcolor="white"></td>
      HTML
    end

    def right_border
      <<-HTML
        <td style="width: 3.5%; min-width: 10px;" bgcolor="white"></td>
        <td style="width: 5%; min-width: 10px;"></td>
      HTML
    end

    def title_row_icon
      html = <<-HTML
        <td class="article-icon" bgcolor="white" style="width: 18px; padding: 40px 12px 0 0; border-bottom: solid 1px #ddd; vertical-align: top;">
          <img style="vertical-align: middle;" src="#{ICON_SRC}"/>
        </td>
      HTML

      Zendesk::HtmlEmail::RelativeToAbsoluteUrlConverter.convert(html, url_helper.static_assets_host)
    end

    def title_row(title)
      html = <<-HTML
        #{title_row_icon}
        <td class="first-article-title" style="font-family:#{font_family}; color: #333333; font-size:18px; line-height:20px; padding: 40px 0 20px 0; border-bottom: solid 1px #dddddd; vertical-align: middle;" bgcolor="white">
          #{title}
        </td>
      HTML

      strip_and_compress_lines(html)
    end

    def article_body_row(body)
      body = to_single_spaces(strip_between_tags(body))
      html = <<-HTML
        <td colspan="2" class="first-article-body" style="padding: 25px 0; border-bottom: solid 1px #ddd; " bgcolor="white">
          #{body}
        </td>
      HTML

      strip_and_compress_lines(html)
    end

    def answer_question_row
      html = <<-HTML
        <td colspan="2" style="font-family:#{font_family}; font-weight: bold; color: #555555; font-size:14px; line-height:18px;padding: 25px 10px; text-align: center;" bgcolor="white">
          #{I18n.t('txt.admin.answer_bot.email.first_article.header_prompt')}
        </td>
      HTML

      strip_and_compress_lines(html)
    end

    def cta_buttons_row(article)
      # wrap in table for Outlook
      html = <<-HTML
      <td class="first-article-actions "colspan="2" style="padding-bottom: 40px; text-align: center;" bgcolor="white">
        <table width="310" style="margin: auto">
          <tr>
            <td>#{close_request_button(article)}</td>
            <td>#{no_button(article)}</td>
          </tr>
        </table>
      </td>
      HTML

      strip_and_compress_lines(html)
    end

    def close_request_button(article)
      href = url_helper.close_request_url(article[:id])
      text = '&#x2714     ' + I18n.t('txt.admin.answer_bot.email.first_article.close_request')
      style = 'margin: 0;';
      button = Zendesk::HtmlEmail::Button.render(href: href, stroke: '#68737D', fill: '#ffffff', color: '#68737D', text: text, width: '180px', style: style)

      strip_between_tags(button)
    end

    def no_button(article)
      href = url_helper.reject_article_url(article[:id])
      text = '&#x2718      ' + I18n.t('txt.admin.answer_bot.email.first_article.not_solved_request')
      style = 'margin: 0 0 0 20px;';
      button = Zendesk::HtmlEmail::Button.render(href: href, stroke: '#68737D', fill: '#ffffff', color: '#68737D', text: text, width: '130px', style: style)

      strip_between_tags(button)
    end
  end
end
