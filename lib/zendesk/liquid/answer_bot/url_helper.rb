module Zendesk::Liquid::AnswerBot
  class UrlHelper
    HELP_CENTER_ARTICLE_URL = '%{account_url}/requests/automatic-answers/ticket/article/%{article_id}?auth_token=%{auth_token}'.freeze
    CLOSE_REQUEST_URL = '%{base_url}/requests/automatic-answers/ticket/solve/article/%{article_id}?auth_token=%{auth_token}'.freeze
    REJECT_ARTICLE_URL = '%{base_url}/requests/automatic-answers/ticket/feedback/article/%{article_id}?auth_token=%{auth_token}'.freeze

    ASSET_HOST = 'static.zdassets.com'.freeze

    def initialize(account:, ticket:, article_ids:)
      @account = account
      @ticket = ticket
      @article_ids = article_ids
    end

    def help_center_article_url(article_id)
      HELP_CENTER_ARTICLE_URL % {
        account_url: account_url,
        article_id: article_id,
        auth_token: auth_token
      }
    end

    def close_request_url(article_id)
      CLOSE_REQUEST_URL % {
        base_url: base_url,
        article_id: article_id,
        auth_token: auth_token
      }
    end

    def reject_article_url(article_id)
      REJECT_ARTICLE_URL % {
        base_url: base_url,
        article_id: article_id,
        auth_token: auth_token
      }
    end

    def host
      URI.parse(account_url).host
    end

    def static_assets_host
      ASSET_HOST
    end

    private

    attr_reader :account, :ticket, :article_ids

    def account_url
      account.url(mapped: false, ssl: true)
    end

    def base_url
      (ticket.brand || account).url
    end

    def auth_token
      @auth_token ||= AutomaticAnswersJwtToken.encrypted_token(
        account.id,
        ticket.requester_id,
        ticket.nice_id,
        ticket.ticket_deflection.id,
        article_ids
      )
    end
  end
end
