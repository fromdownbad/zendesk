module Zendesk::Liquid::AnswerBot
  class ArticleListDrop
    include ::Zendesk::Liquid::DropHelpers
    include WhitespaceSanitizationHelper

    ICON_SRC = '/classic/images/icons/article.png'.freeze

    def initialize(articles:, url_helper:, font_family:)
      @articles = articles
      @url_helper = url_helper
      @font_family = font_family
    end

    def render
      return "" if articles.blank?

      strip_and_compress_lines(article_table).html_safe
    end

    private

    attr_reader :articles, :url_helper, :font_family

    def article_table
      html = <<-HTML
        <table style="line-height:18px;font-family:#{font_family};font-size:12px;color:#444444; margin:0; padding:0; border-collapse:collapse;">
          <thead>
            <tr>
              <td style="font-weight:bold;margin:0;padding:15px 0 12px 0;font-size:18px;line-height:22px;font-weight: normal;">
                #{I18n.t('txt.admin.answer_bot.email.header_prompt')}
              </td>
            </tr>
            <tr>
              <td style="border-bottom:solid 1px #dddddd;padding:0;height:8px;"></td>
            </tr>
          </thead>
          <tbody style="xline-height: 20px; font-size: 14px">
            #{article_rows}
          </tbody>
        </table>
      HTML

      strip_between_tags(html)
    end

    def article_rows
      rows = []
      articles.each do |article|
        rows.push <<-HTML
          <!-- START ROW -->
          <tr>
            <td>
              <!--[if (gte mso 9)|(IE)]>
              <table style="margin:0;padding:0;width:510px;">
                <tr>
                  <td>
                    <![endif]-->
                    <table style="width:100%;margin:0;padding:0;border-collapse:collapse;max-width:510px;">
                      <tr>
                        <td style="padding:0;">
                          <!--[if (gte mso 9)|(IE)]>
                          <table style="margin:0;padding:0;width:100%;" align="center">
                            <tr>
                              <td>
                                <![endif]-->
                                <!-- ARTICLE TITLE COLUMN -->
                                <table style="width:100%;margin:0;padding:0;border-collapse:collapse;">
                                  <tr>
                                    <!-- ARTICLE ICON -->
                                    <td width="30" style="vertical-align: top;">
                                      #{article_icon}
                                    </td>
                                    <!-- ARTICLE LINK -->
                                    <td style="margin:0;vertical-align: middle;padding-top: 18px;">
                                      #{article_link(article)}
                                      #{article_snippet(article)}
                                      #{cta_buttons_table(article)}
                                    </td>
                                  </tr>
                                </table>
                                <!--[if (gte mso 9)|(IE)]>
                              </td>
                            </tr>
                          </table>
                          <![endif]-->
                        </td>
                      </tr>
                      <tr>
                        <td style="border-bottom:solid 1px #dddddd; padding:0;height:14px;"> </td>
                      </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                  </td>
                </tr>
              </table>
              <![endif]-->
            </td>
          </tr>
          <!-- FINISH ROW -->
        HTML
      end

      strip_between_tags(rows.join)
    end

    def article_icon
      html = <<-HTML
        <img class="article-icon" src="#{ICON_SRC}" width="18" style="vertical-align: middle;margin-right:10px;margin-top: 15px;"/>
      HTML

      Zendesk::HtmlEmail::RelativeToAbsoluteUrlConverter.convert(html, url_helper.static_assets_host)
    end

    def article_link(article)
      href = url_helper.help_center_article_url(article[:id])
      title = to_single_spaces(article[:title])

      <<-HTML
        <a class="article-link" href="#{href}" style="font-family:#{font_family};font-size:13px;line-height:16px;text-decoration:none;color:#30AABC;display:block;" target="_blank">#{title}</a>
      HTML
    end

    def article_snippet(article)
      sanitized_body = Zendesk::Liquid::AnswerBot::ArticleRenderer.present(article[:body])
      href = url_helper.help_center_article_url(article[:id])
      read_more = I18n.t('txt.admin.answer_bot.email.article_list.read_more')

      <<-HTML
        <p class="article-snippet" style="font-family:#{font_family};font-size:13px;line-height:20px;text-decoration:none;margin-top:10px;" target="_blank">
          #{sanitized_body} <a href="#{href}" style="text-decoration:none;color:#30AABC;" target="_blank">#{read_more}</a>
        </p>
      HTML
    end

    def cta_buttons_table(article)
      <<-HTML
      <table style="margin: 12px 0 0 0;padding:0;border-collapse:collapse;float:left;">
        <tr>
          <!-- CLOSE REQUEST BUTTON -->
          <td class="close-request-button">
            #{close_request_button(article)}
          </td>
          <td class="view-article-button">
            #{view_article_button(article)}
          </td>
        </tr>
      </table>
      HTML
    end

    def close_request_button(article)
      href = url_helper.close_request_url(article[:id])
      text = '&#x2714      ' + I18n.t('txt.admin.answer_bot.email.first_article.close_request')
      style = 'margin: 0;';
      button = Zendesk::HtmlEmail::Button.render(href: href, stroke: '#68737D', fill: '#ffffff', color: '#68737D', text: text, width: '190px', style: style)

      strip_between_tags(button)
    end

    def view_article_button(article)
      href = url_helper.help_center_article_url(article[:id])
      text = I18n.t('txt.admin.ticket_deflection.confirmation_page.article.view')
      style = 'margin: 0 0 0 16px;';
      button = Zendesk::HtmlEmail::Button.render(href: href, stroke: '#333333', fill: '#333333', color: '#ffffff', text: text, width: '130px', style: style)

      strip_between_tags(button)
    end
  end
end
