module Zendesk::Liquid::AnswerBot
  class Drop < Liquid::Drop
    include ::Zendesk::Liquid::DropHelpers

    SYSTEM_FONT_STACK = "'system-ui','-apple-system','BlinkMacSystemFont','Segoe UI','Roboto','Oxygen-Sans','Ubuntu','Cantarell','Helvetica Neue','Arial','sans-serif'".freeze

    def initialize(ticket, format)
      @ticket  = ticket
      @account = ticket.account
      @format = format
    end

    def article_count
      return 0 unless renderability.can_render_deflection_information?

      fetch_articles.length
    end

    def article_list
      return "" unless renderability.can_render_deflection_information?

      articles = fetch_articles
      return "" if articles.blank?

      drop =
        if html_format?
          article_list_drop(articles)
        else
          article_list_plain_text_drop(articles)
        end

      drop.timed_render
    rescue StandardError => e
      log_exception(e)
      ""
    end

    def first_article_body
      return "" unless renderability.can_render_deflection_information?
      return "" if text_format?

      articles = fetch_articles
      return "" if articles.blank?

      first_article_body_drop(articles).timed_render
    rescue StandardError => e
      log_exception(e)
      ""
    end

    private

    attr_reader :ticket, :account, :format

    def renderability
      @renderability ||= Zendesk::Liquid::AnswerBot::Renderability.new(
        account: account, ticket: ticket
      )
    end

    def fetch_articles
      return @fetched_articles if @fetched_articles

      article_ids_and_locales = ticket.ticket_deflection.ticket_deflection_articles.map do |a|
        {'article_id' => a.article_id, 'locale' => a.locale}
      end
      @fetched_articles = api_client.fetch_articles_for_ids_and_locales(article_ids_and_locales)
    end

    def article_list_drop(articles)
      Zendesk::Liquid::AnswerBot::ArticleListDrop.new(
        articles: articles,
        url_helper: url_helper(articles),
        font_family: SYSTEM_FONT_STACK
      )
    end

    def article_list_plain_text_drop(articles)
      Zendesk::Liquid::AnswerBot::ArticleListPlainTextDrop.new(
        articles: articles,
        url_helper: url_helper(articles)
      )
    end

    def first_article_body_drop(articles)
      Zendesk::Liquid::AnswerBot::FirstArticleBodyDrop.new(
        articles: articles,
        url_helper: url_helper(articles),
        font_family: SYSTEM_FONT_STACK
      )
    end

    def url_helper(articles)
      Zendesk::Liquid::AnswerBot::UrlHelper.new(
        account: account,
        ticket: ticket,
        article_ids: articles.map { |x| x[:id] }.map(&:to_i)
      )
    end

    def html_format?
      (format == Mime[:html].to_s)
    end

    def text_format?
      (format == Mime[:text].to_s)
    end

    def log_exception(e)
      ZendeskExceptions::Logger.record(e, location: self, message: "Unable to generate automatic_answers email for account id: #{account.id} ticket id: #{ticket.id} ticket nice id: #{ticket.nice_id}", fingerprint: 'cc2855987ef858268981cdae657348181220adf2')
    end

    def api_client
      @api_client ||= Zendesk::HelpCenter::InternalApiClient.new(account: ticket.account, requester: ticket.requester, brand: ticket.brand)
    end
  end
end
