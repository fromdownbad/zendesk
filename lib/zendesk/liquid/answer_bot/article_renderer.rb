require 'sanitize'

module Zendesk::Liquid::AnswerBot
  class ArticleRenderer
    MAX_NOKOGIRI_TREE_DEPTH = -1 # Removes limit on tree depth
    MAX_CHARACTERS = 120

    class << self
      include WhitespaceSanitizationHelper
      include ActionView::Helpers::TextHelper

      def present(content)
        config = Sanitize::Config.merge(
          Sanitize::Config::DEFAULT,
          remove_contents: %w[script style],
          parser_options: { max_tree_depth: MAX_NOKOGIRI_TREE_DEPTH }
        )

        text_body = to_single_spaces(
          Sanitize.fragment(content, config)
        )

        truncate(text_body, length: MAX_CHARACTERS, separator: ' ', escape: false)
      rescue SystemStackError
        Rails.logger.warn("Answer Bot article body could not be parsed")
        ''
      end
    end
  end
end
