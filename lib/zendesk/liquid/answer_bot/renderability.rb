module Zendesk::Liquid::AnswerBot
  class Renderability
    def initialize(account:, ticket:)
      @account = account
      @ticket = ticket
    end

    def can_render_deflection_information?
      account.has_automatic_answers_enabled? &&
        ticket.ticket_deflection &&
        !deflectability.deflected_by_another_channel?
    end

    private

    attr_reader :ticket, :account

    def deflectability
      @deflectability ||= AnswerBot::Deflectability.new(
        account: account, ticket: ticket
      )
    end
  end
end
