module Zendesk::Liquid::AnswerBot
  class ArticleListPlainTextDrop
    include ::Zendesk::Liquid::DropHelpers
    include WhitespaceSanitizationHelper

    DIVIDER = '----------------------------------------------'.freeze

    def initialize(articles:, url_helper:)
      @articles = articles
      @url_helper = url_helper
    end

    def render
      return "" if articles.blank?

      article_list
    end

    private

    attr_reader :articles, :url_helper

    def article_list
      [].tap do |lines|
        lines << "\n"
        lines << do_any_of_these_articles_answer_your_question
        lines << "\n\n"
        lines << DIVIDER
        lines << "\n\n"
        lines << article_rows
        lines << "\n"
      end.join
    end

    def article_rows
      articles.reduce("") do |rows, article|
        rows << article_row(article)
      end
    end

    def article_row(article)
      [].tap do |lines|
        lines << to_single_spaces(article[:title])
        lines << "\n\n"
        lines << url_helper.help_center_article_url(article[:id])
        lines << "\n\n"
        lines << yes_close_my_request
        lines << "\n\n"
        lines << url_helper.close_request_url(article[:id])
        lines << "\n\n"
        lines << DIVIDER
        lines << "\n\n"
      end.join
    end

    def do_any_of_these_articles_answer_your_question
      I18n.t('txt.admin.answer_bot.email.header_prompt')
    end

    def yes_close_my_request
      I18n.t('txt.admin.answer_bot.email.first_article.close_request')
    end
  end
end
