module Zendesk::Liquid
  class Context
    # The number of times that we will recursively render this context.
    MAX_NESTING = 5

    attr_accessor :nesting_level,
      :locale,
      :format,
      :skip_auto_link,
      :dc_cache,
      :account,
      :public_comment_placeholder

    attr_writer :template

    def initialize(template, locale, options = {})
      @input_template     = template
      @raw_template       = options[:raw_template] || template
      @locale             = locale
      @nesting_level      = 0
      @dc_cache         ||= default_dc_cache
      @rich_comment       = options[:rich_comment]
      @render_dc_markdown = options[:render_dc_markdown]
      @suppress_placeholder = options[:suppress_placeholder]
      @detect_public_comment_placeholder = options[:detect_public_comment_placeholder] || false
      @public_comment_placeholder = false
    end

    def template
      @template ||= if @input_template.respond_to?(:to_utf8)
        @input_template = @input_template.to_utf8 # validate the encoding
      else
        @input_template
      end
    end

    def default_dc_cache
      (account && account.has_dc_account_content_cache_default?) ? Zendesk::DynamicContent::AccountContent.cache(account, locale) : {}
    end

    def render
      return if @input_template.nil?
      return if nesting_level > MAX_NESTING

      expand_dynamic_content
      check_for_public_comment_placeholders
      suppress_placeholder! if suppress_placeholder?
      convert_placeholders_within_href

      return post_process(template) unless template_contains_liquid?

      tags = ["content_type:#{format}", "context:#{self.class}"]
      result = Context.time_and_record('render_and_post_process', tags) { post_process(render_template) }

      content_type?(Mime[:html]) ? result.html_safe : result
    end

    def check_for_public_comment_placeholders
      return unless detect_public_comment_placeholder?
      return if public_comment_placeholders.empty?

      self.public_comment_placeholder = true
    end

    def fields
      raise "Must be implemented by subclass!"
    end

    def template_contains_liquid?
      !!(template =~ /\{\{|\{\%/)
    end

    def content_type?(content_type)
      content_type == format
    end

    def nested?
      nesting_level > 0
    end

    def rich_comment?
      @rich_comment
    end

    def render_dc_markdown?
      @render_dc_markdown
    end

    def suppress_placeholder?
      @suppress_placeholder
    end

    def detect_public_comment_placeholder?
      @detect_public_comment_placeholder
    end

    def without_auto_linking
      @skip_auto_link = true
      ret = yield
      @skip_auto_link = false
      ret
    end

    def self.time_and_record(key, tags = [])
      # Benchmark on 10% sample rate, and include tags.
      statsd_client.time(key, tags: tags, sample_rate: 0.1) { @res = yield }
      @res
    end

    def self.statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['liquid', 'context'])
    end

    private

    def expand_dynamic_content
      return unless template.respond_to?(:gsub!)
      @template = template.to_str if template.respond_to?(:to_str)
      @template = dc_renderer.render(@template)
    end

    def dc_renderer
      @dc_renderer ||= Zendesk::DynamicContent::Renderer.new(@account, @locale, dc_cache: @dc_cache)
    end

    def convert_placeholders_within_href(limit = 8)
      return unless limit > 0 && template.respond_to?(:gsub!)
      # By default, in url, `{` character is encoded to `%7B`, and `}` is encoded to `%7D`
      # Convert them back if they are part of placeholders within href
      if template.gsub!(/href=(\\)?"(.*)%7B%7B(.*)%7D%7D(.*)"/, 'href=\1"\2{{\3}}\4"')
        convert_placeholders_within_href(limit - 1)
      end
    end

    def suppress_placeholder!
      if placeholders_removed.present?
        Rails.logger.info("Public comment placeholder suppressed: #{placeholders_removed}")
        self.class.statsd_client.increment('liquid_context.placeholder_suppressed')
      end
      @template = Zendesk::Liquid::PlaceholderSuppression.redact_placeholders(@template)
    end

    def placeholders_removed
      @placeholders_removed ||= public_comment_placeholders
    end

    def public_comment_placeholders
      @template.
        to_s.
        scan(Zendesk::Liquid::PlaceholderSuppression::PUBLIC_COMMENT_PLACEHOLDER_PATTERN).
        flatten
    end

    def configure
      Liquid::Extensions::HtmlSafety.with_enabled(format == Mime[:html]) do
        ZendeskText::Markdown::Boundaries.with_enabled(true) do
          with_nesting do
            I18n.with_locale(locale) do
              yield
            end
          end
        end
      end
    end

    def with_nesting
      self.nesting_level += 1
      yield
    ensure
      self.nesting_level -= 1
    end

    def parse(template)
      Liquid::Template.parse(template)
    rescue Liquid::SyntaxError => e
      Rails.logger.warn("Invalid template, returning raw template for #{template}: #{e.message}")
      nil
    end

    def render_template
      prepared_template = pre_process(template)
      configure do
        if liquid_template = parse(prepared_template)
          liquid_template.render(fields)
        else
          @raw_template
        end
      end
    end

    def pre_process(input)
      processors = [
        PreProcessing::MagicEscaper
      ]
      process(processors, input)
    end

    def post_process(output)
      processors = [
        PostProcessing::SimpleFormatter,
        PostProcessing::Autolinker,
        PostProcessing::DocumentCleanup
      ]
      process(processors, output)
    end

    def process(processors, value)
      processors.inject(value) do |last_result, processor_klass|
        processor_klass.new(self, last_result).apply
      end
    end
  end
end
