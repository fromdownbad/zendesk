require 'zendesk_database_support/sensors/maxwell_filters_lag_sensor'
require 'zendesk_job/rss_reader'

class Zendesk::Archive::Archiver
  attr_reader :accounts

  DEFAULT_TICKET_AGE_IN_DAYS = 120
  SLOW_QUERY_BATCH_TIME = 0.5 # seconds

  # statsd metrics
  STARTED_METRIC                = "started".freeze
  SHARD_ITERATION_METRIC        = "shard_iteration".freeze
  DISQUALIFICATIONS_METRIC      = "disqualifications".freeze
  TICKET_FIND_DURATION_METRIC   = "ticket_find_query.duration".freeze
  FETCHED_TICKETS_REJECTED      = "fetched_tickets_rejected".freeze
  TIME_METRIC                   = "archive_time".freeze
  AGE_METRIC                    = "ticket_age".freeze
  VISITED_TICKETS_METRIC        = "visited_tickets".freeze
  DISABLED_ACCOUNTS_METRIC      = "disabled_accounts_refreshed".freeze
  RSS_METRIC                    = "rss_bytes".freeze
  ABORT_METRIC                  = "aborts".freeze

  # How often should we check slave delay and adjust our rate?
  DEFAULT_RATE_REFRESH_INTERVAL = 2

  # Permanently disqualify tickets over this threshold, without attempting to load them into memory.
  MAX_EVENT_DISQUALIFICATION_THRESHOLD = ENV.fetch('ARCHIVER_MAX_EVENT_DISQUALIFICATION_THRESHOLD', 10000).to_i

  PRIORITY_ACCOUNTS = ENV.fetch('ARCHIVER_PRIORITY_ACCOUNTS', '').split(/, */)

  # Process aborts when its RSS exceeds this limit (after one GC attempt)
  RSS_LIMIT = ENV.fetch('ARCHIVER_RSS_LIMIT', 2.gigabytes).to_i

  # Pending DBA-18843 - these are test shard and not present in consul yet.
  # Remove this change once these shards are reflected.
  IGNORE_TEST_SHARDS = [8999, 9999, 10999, 11999].freeze

  # The version we use to generate the key to store/retrieve tickets in Riak.
  # The first version did not use account_id as part of the key, version 2 does
  KEY_VERSION = 2

  class << self
    # Workers partition over database clusters to prevent multiple workers from
    # hammering the same single MySQL master simultaneously.
    def shards_for_worker_info(worker_number, worker_count)
      # e.g. { 501 => '5.1', ..., 5036 => '5.11' }
      shards_to_clusters = Zendesk::Configuration::DBConfig.shards_to_clusters
      validate_consul_shards!
      # e.g. ['5.1', ..., '5.11']
      all_clusters = shards_to_clusters.values.sort.uniq

      # Accomodate that workers are 1-indexed, and that i % n is (0..n-1)
      worker_number = 0 if worker_number == worker_count

      my_clusters = all_clusters.select do |c|
        # '5.11' => 11
        cluster_number = c.split('.').last.to_i
        cluster_number % worker_count == worker_number
      end

      shards_to_clusters.select { |_, cluster| my_clusters.include?(cluster) }.keys
    end

    # It is not the archiver's responsibility know where these shards are or
    # validate them. However, we're an early critical-path consumer of
    # shards_to_clusters, and we've seen it misconfigured previously.
    # - Short term: check and hard-crash.
    # - Long term: remove this code once something else more appropriate validates it for us.
    def validate_consul_shards!
      consul_shards = Zendesk::Configuration::DBConfig.shards_to_clusters.keys.sort
      yaml_shards = Zendesk::Configuration::PodConfig.local_shards.sort - IGNORE_TEST_SHARDS
      if consul_shards != yaml_shards
        consul_missing = yaml_shards - consul_shards
        yaml_missing = consul_shards - yaml_shards
        raise "Consul and zendesk.yml show different shards. Consul missing: #{consul_missing}, YAML missing: #{yaml_missing}"
      end
    end
  end

  attr_reader :dry_run

  def initialize(options = {})
    @account_ids   = options[:account_ids]
    @shard_ids     = options[:shard_ids]
    @run_once      = options[:run_once]
    @dry_run       = options.key?(:dry_run) ? options[:dry_run] : false
    @worker_mod    = "#{options[:worker_number]},#{options[:worker_count]}" if options[:worker_number]
    @log_level     = options.fetch(:log_level, 'info')
    @max_size      = (options[:max_size_mb] || 2).to_i.megabytes

    @accounts = nil
    @disabled_accounts = {}
    @periodically = Zendesk::Periodical.new(60)
    @rate_controller = Zendesk::RateControl.new(rate_per_sec: 30, rate_refresh_interval: DEFAULT_RATE_REFRESH_INTERVAL)
    @rate_controller.define_rate_control do |old_rate|
      rate = get_throttle_rate(old_rate)
      log("Redefining rate to #{rate} from #{old_rate}") if rate != old_rate
      rate
    end
  end

  def name
    Process.pid.to_s
  end

  # entry point
  def run
    # This is normally set in the initializer, but let's have some extra sanity checking here.
    abort "ZendeskArchive.delete_after_archive is not true" unless ZendeskArchive.delete_after_archive
    install_logger(json_logger)
    log_startup_message
    statsd_client.increment(STARTED_METRIC)

    with_no_predictive_loading(Event) do
      find_each_archivable_ticket do |ticket|
        @rate_controller.with_rate_control do
          archive_ticket(ticket)
        end
      end
    end
  end

  def log_startup_message
    message = "Archiving tickets"
    message += " for accounts #{@account_ids}" if @account_ids
    message += " for shards #{@shard_ids}" if @shard_ids

    Rails.logger.info(message)
  end

  def install_logger(logger)
    # Let's not taint the global environment in tests
    unless Rails.env.test?
      Rails.application.config.colorize_logging = false
      Rails.logger.subscribers.clear
      Rails.logger.subscribe(logger)
    end
  end

  def json_logger
    @json_logger ||= Rails.logger.stdout_logger.instance.tap do |logger|
      logger.append_attributes(
        prefix: 'archiver',
        pid:    $$,
        host:   Socket.gethostname
      )
      logger.level = Logger.const_get(@log_level.upcase)
      logger.append_attributes(worker_mod: @worker_mod) if @worker_mod
    end
  end

  def find_each_archivable_ticket
    loop do
      with_time_logging("refresh_all_accounts!") do
        refresh_all_accounts!
      end

      # Handle refresh when no shard has archivable tickets
      periodically do
        with_time_logging("refresh of disabled accounts after account refresh") do
          @disabled_accounts = archiver_disabled_accounts
        end
      end

      # Even when the kill-switch is enabled, we want archiver to clear any disable requests
      # from exodus, which is why we're checking this after the above disable refresh
      if archiver_kill_switch_enabled?
        Rails.logger.warn("'disable_ticket_archiving' arturo is enabled.")
        break if @run_once

        Rails.logger.info("sleeping 1 min before re-checking arturo.")
        sleep(1.minute)
        next
      end

      accounts_by_shard.each_pair do |shard, account_set|
        tickets_on_shard(shard) do |tickets|
          periodically do
            # Handle refresh when stepping through a large amount of tickets for
            # an already disabled account
            with_time_logging("refresh of disabled accounts after ticket fetch") do
              @disabled_accounts = archiver_disabled_accounts
            end
          end

          process_tickets_on_shard(tickets, account_set, shard) do |ticket|
            yield ticket
          end
        end
        sleep(1) if @dry_run # so we don't spin and kill db during dryrun
      end
      statsd_client.increment(SHARD_ITERATION_METRIC)

      break if @run_once
      # if we run out of tickets completely,
      # which is unlikely, don't spin on the query.
      sleep(1)
    end
  end

  def with_time_logging(name)
    time = Benchmark.realtime do
      yield
    end
    log("#{name} took #{time.round(4)} seconds")
  end

  def archiver_disabled_accounts
    return {} if @account_ids

    shard_to_accounts = {}

    shards = @shard_ids || ActiveRecord::Base.shard_names
    shards.each do |shard|
      accs = Set.new
      ActiveRecord::Base.on_shard(shard) do
        conditions = {
          name: "write_to_archive_v2_enabled", value: ["0", "false", "disable_pending"]
        }
        AccountSetting.where(conditions).each do |s|
          if s.value == 'disable_pending'
            log("[#{Time.now.strftime('%H:%M:%S.%L')}] Disabling archiver for account: #{s.account_id}")
            unless @dry_run
              s.value = 'false'
              s.save!
            end
          end
          accs << s.account_id
        end
      end
      shard_to_accounts[shard] = accs
    end

    # Monitor will assert SLA on acknowledging pending requests from Exodus
    statsd_client.increment(DISABLED_ACCOUNTS_METRIC)

    shard_to_accounts
  end

  def tickets_on_shard(shard)
    json_logger.append_attributes(shard_id: shard)
    ActiveRecord::Base.on_shard(shard) do
      Ticket.with_deleted do
        batch = 0
        start = Time.now
        scope = Ticket.
          joins("LEFT JOIN ticket_archive_stubs ON ticket_archive_stubs.id = tickets.id").
          joins("LEFT JOIN ticket_archive_disqualifications ON ticket_archive_disqualifications.id = tickets.id").
          joins("LEFT JOIN account_settings ON (account_settings.account_id = tickets.account_id AND account_settings.name = 'archive_after_days')").
          where("tickets.updated_at < SUBDATE(NOW(), IFNULL(account_settings.value, ?))", DEFAULT_TICKET_AGE_IN_DAYS).
          where("tickets.status_id" => [StatusType.CLOSED, StatusType.DELETED]).
          where("ticket_archive_stubs.id" => nil).
          where("ticket_archive_disqualifications.id" => nil).
          use_index("primary")

        scope.find_in_batches(batch_size: 100) do |tickets|
          query_duration = Time.now - start
          if query_duration > SLOW_QUERY_BATCH_TIME # log only slow queries
            log("Query batch(#{batch * 100} - #{(batch + 1) * 100}) took #{query_duration.round(4)} seconds (slow) to finish on shard:#{shard}, yielding #{tickets.size} tickets")
          end
          statsd_client.histogram(TICKET_FIND_DURATION_METRIC, query_duration)

          yield tickets

          batch += 1
          process_health_check!
          # Slow ourselves down if the shard has nothing available for archiving
          sleep 5 if batch == 1 && tickets.empty?
          start = Time.now
        end
      end
    end
    json_logger.attributes.delete(:shard_id)
  end

  # Given a list of tickets fetched from MySQL, reject tickets where:
  # 1. The account has moved shards, but has stale data (pending deletion) left
  #    behind on the old shard.
  # 2. The account has requested archiving be disabled, typically due to an
  #    active shard move.
  # Since these are temporary conditions, it's faster to filter these in Ruby
  # than in SQL.
  def reject_inactive_accounts(tickets, account_set, shard)
    before_size = tickets.size
    tickets = tickets.select { |t| account_set.include?(t.account_id) && enabled_account?(shard, t.account_id) }
    rejected = before_size - tickets.size
    if rejected > 0
      statsd_client.count(FETCHED_TICKETS_REJECTED, rejected)
    end
    tickets
  end

  def archive_ticket(ticket)
    shard = ActiveRecord::Base.current_shard_id
    time = Benchmark.realtime do
      unless @dry_run
        Ticket.allow_without_entity_publication do
          ticket.archive!(key_version: KEY_VERSION)
        end
      end
    end

    age = (Time.now.to_date - ticket.updated_at.to_date).to_i

    mysql_cluster = Zendesk::Configuration::DBConfig.cluster_for_shard(shard)

    statsd_client.histogram(TIME_METRIC, time, tags: [cluster: mysql_cluster])
    statsd_client.gauge(AGE_METRIC, age, tags: [cluster: mysql_cluster])

    log("Archived ticket #{ticket_info(ticket)}, time_to_archive=%0.2fms" % (time * 1000))
  rescue ArgumentError => e
    if /invalid byte sequence in UTF-8/.match?(e.message)
      log_error("Error archiving ticket #{ticket_info(ticket)} -- invalid utf-8")
    else
      raise e
    end
  end

  def refresh_all_accounts!
    conditions = {}
    conditions[:shard_id] = @shard_ids if @shard_ids
    conditions[:id] = @account_ids if @account_ids
    accounts = Account.pod_local.shard_unlocked.select('id, shard_id, subdomain, lock_state').where(conditions).to_a
    @accounts = accounts.sort_by do |a|
      PRIORITY_ACCOUNTS.include?(a.subdomain) ? -1 : 1
    end
  end

  def statsd_client
    @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ["archiver", "v2"], tags: metric_tags)
  end

  private

  def archiver_kill_switch_enabled?
    Arturo.feature_enabled_for_pod?(:disable_ticket_archiving, Zendesk::Configuration.fetch(:pod_id))
  end

  def metric_tags
    mod_tag = @worker_mod ? "mod:#{@worker_mod.tr(",", "_")}" : "mod:1_1"
    [mod_tag]
  end

  def with_no_predictive_loading(klass)
    ActiveRecord::Base.predictive_load_disabled << klass
    yield
  ensure
    ActiveRecord::Base.predictive_load_disabled.delete(klass)
  end

  def ticket_info(ticket)
    "##{ticket.id} of acc:#{ticket.account_id} on shard:#{ActiveRecord::Base.current_shard_id}"
  end

  def log(m)
    Rails.logger.info("Archiver(#{name}): #{m}")
  end

  def warn(m)
    Rails.logger.warn("Archiver(#{name}): #{m}")
  end

  def log_error(m)
    Rails.logger.error("Archiver(#{name}): #{m}")
  end

  def get_throttle_rate(old_rate)
    delays = { slave_lag: slave_delay.max_slave_delay(ActiveRecord::Base.current_shard_id.to_i) }

    if throttle_maxwell_filters?
      delays[:maxwell_filters_lag] = maxwell_filters_lag_sensor.measure
    end

    log("Got delays: #{delays.to_json}")

    delay = delays.values.compact.max
    if delay.nil?
      old_rate
    elsif delay > 30
      1 / 60.0 # 1 per minute (allows archiver_disabled_accounts to continue to run within SLAs)
    elsif delay > 22
      1 / 6.0 # 10 per minute
    elsif delay > 15
      1 # per sec
    elsif delay > 7
      3 # per sec
    else
      # how fast should we archive the number of tickets by default?
      # on my local box it looks like we can do about 5 tickets per second, which isn't too bad.
      # this should give plenty of overhead.
      30 # per sec
    end
  end

  def slave_delay
    @slave_delay ||= ZendeskDatabaseSupport::SlaveDelay.new
  end

  def throttle_maxwell_filters?
    Arturo.feature_enabled_for_pod?(:database_backoff_maxwell_filters_lag, Zendesk::Configuration.fetch(:pod_id))
  end

  def maxwell_filters_lag_sensor
    ZendeskDatabaseSupport::Sensors::MaxwellFiltersLagSensor.new(shard_id: ActiveRecord::Base.current_shard_id)
  end

  def periodically(&block)
    @periodically.perform(&block)
  end

  def accounts_by_shard
    Hash.new { |hash, key| hash[key] = Set.new }.tap do |h|
      @accounts.each { |account| h[account.shard_id] << account.id }
    end
  end

  def enabled_account?(shard, account_id)
    !@disabled_accounts[shard] || !@disabled_accounts[shard].include?(account_id)
  end

  def process_tickets_on_shard(tickets, account_set, shard)
    tickets = reject_inactive_accounts(tickets, account_set, shard)
    while (ticket = tickets.pop)
      # Once a minute refresh disabled accounts and discard pre-fetched tickets
      # for matching accounts.
      #
      # This should run at the top of the loop so that it still executes during
      # a bubble of unprocessable tickets.
      periodically do
        with_time_logging("refresh of disabled accounts during ticket processing") do
          @disabled_accounts = archiver_disabled_accounts
        end
        tickets = reject_inactive_accounts(tickets, account_set, shard)
      end

      statsd_client.increment(VISITED_TICKETS_METRIC) unless @dry_run

      ticket_event_count = ticket.events.count(:all)
      if ticket_event_count > MAX_EVENT_DISQUALIFICATION_THRESHOLD
        unless @dry_run
          TicketArchiveDisqualification.disqualify_ticket!(
            ticket,
            metric: TicketArchiveDisqualification::EVENT_COUNT,
            value: ticket_event_count
          )
        end
        statsd_client.histogram(DISQUALIFICATIONS_METRIC, ticket_event_count, tags: ["metric:event_count"])
        warn("Disqualifying ticket #{ticket_info(ticket)} due to event count (#{ticket_event_count}).")
        next
      end

      ticket_archive_bytesize = ticket.as_archived.to_json.bytesize
      if ticket_archive_bytesize > @max_size
        unless @dry_run
          TicketArchiveDisqualification.disqualify_ticket!(
            ticket,
            metric: TicketArchiveDisqualification::BYTESIZE,
            value: ticket_archive_bytesize
          )
          statsd_client.histogram(DISQUALIFICATIONS_METRIC, ticket_archive_bytesize, tags: ["metric:bytesize"])
        end
        warn("Disqualifying ticket #{ticket_info(ticket)} due to size (#{ticket_archive_bytesize}).")
        ticket = nil # Allow potential garbage collection in process_health_check!
        process_health_check!
        next
      end

      yield ticket
    end
  end

  # This entire method can be scrapped when we're in kubernetes
  def process_health_check!(gc_attempted = false)
    # Implement our own memory limits.
    # The misc boxes are shared, have limited memory. If we happen to pull in
    # a huge ticket, do our best to return the memory. Ruby's GC isn't great
    # at returning memory to the OS, so if we can't, we exit, and get replaced.
    rss = ZendeskJob::RssReader.rss
    statsd_client.gauge(RSS_METRIC, rss)
    if rss > RSS_LIMIT
      if gc_attempted
        statsd_client.increment(ABORT_METRIC, tags: ['reason:rss'])
        warn("RSS is at #{rss}, over the limit of #{RSS_LIMIT}, aborting")
        abort
      else
        GC.start
        new_rss = ZendeskJob::RssReader.rss
        warn("Manually running GC due to memory limit, was: #{rss}, now: #{new_rss}, improvement: #{rss - new_rss}")
        return process_health_check!(true)
      end
    end

    # We only want one copy of the archiver running. If our parent is 'init',
    # then `consul lock` has disappeared, and there may be multiple copies
    # of this process executing.
    if Process.ppid == 1
      statsd_client.increment(ABORT_METRIC, tags: ['reason:orphan'])
      warn("#{$0} Detected orphan process - killing self!")
      abort
    end
  end
end
