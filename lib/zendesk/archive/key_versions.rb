module Zendesk::Archive
  # Key versions for Zendesk archive are implemented as a bitmask
  # used to distinguish between which store we use. Different
  # backend stores require different keys to access data
  KEY_VERSIONS = {
    riak_no_account_id: 1, # Legacy archiver did not use the account_id as part of the key
    riak: 2,
    dynamodb: 4
  }.freeze

  # Takes an array of stores and computes the bitmask
  def self.key_version_for(stores)
    stores.map do |store|
      if !KEY_VERSIONS.key?(store)
        raise ArgumentError, "#{store} Not a valid store, see KEY_VERSIONS hash for supported stores"
      else
        KEY_VERSIONS[store]
      end
    end.sum
  end
end
