module Zendesk
  module Retrier
    class << self
      # calls the block a maximum of @attempts
      # yields the previous exception when retrying
      def retry_on_error(exceptions, attempts, retry_if: false)
        err ||= nil
        yield(err)
      rescue *exceptions => e
        err = e

        attempts -= 1
        raise if attempts == 0 || (retry_if && !retry_if.call(e))

        Rails.logger.warn "Zendesk::Retrier retry_on_error remaining attempts: #{attempts} (error: #{e.inspect})"

        retry
      end
    end
  end
end
