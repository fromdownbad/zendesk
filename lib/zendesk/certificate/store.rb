require 'openssl'

module Zendesk
  module Certificate
    # A replacement implementation similar to OpenSSL::X509::Store, which
    # 'verify' method isn't reentrant and doesn't allow you to inspect which CA
    # certificates are loaded in the Store. The API is not the same.
    class Store
      attr_accessor :ca_roots, :ca_intermediates

      def initialize
        @ca_roots = []
        @ca_intermediates = []
      end

      def load_from_db
        @ca_roots = CertificateAuthorities.roots
        @ca_intermediates = CertificateAuthorities.intermediates unless @ca_roots.empty?
        !@ca_roots.empty?
      end

      # for ops
      # Dir.glob("*.crt").each { |f| puts cert.store.append_intermediate(f) }
      def append_intermediate(pem_file, certificate = nil)
        begin
          pem = File.open(pem_file).read
        rescue
          return "Unable to read file: #{pem_file}"
        end

        append_intermediate_from_pem(pem, certificate)
      end

      def append_intermediate_from_pem(pem, certificate = nil)
        begin
          ca_cert = OpenSSL::X509::Certificate.new(pem)
          pem = ca_cert.to_pem # in case not already PEM
        rescue
          return "Unable to create CA certificate from: #{pem}"
        end

        # NOTE: allow CA cert to be overwritten if it has the same subject hash
        # (@ca_intermediates + @ca_roots).each { |old_ca_cert| return "Pre-existing intermediate not added: #{subject_str}" if old_ca_cert.subject.to_s == subject_str }

        CertificateAuthorities.append(ca_cert, 0) # add to DB

        @ca_intermediates = CertificateAuthorities.intermediates # reload
        if certificate
          ca_chain = chain(certificate)
          return "Issuer not found in chain: #{ca_chain}" unless ca_chain.is_a?(Array)
        end

        nil
      end

      def verify(ca_chain, _certificate)
        last_cert = ca_chain.last
        !!(last_cert.issuer.to_s == last_cert.subject.to_s)
      end

      def chain(certificate)
        issuer_str = certificate.issuer.to_s

        issuer_cert = (@ca_intermediates + @ca_roots).detect do |ca_cert|
          ca_cert.subject.to_s == issuer_str && certificate.verify(ca_cert.public_key)
        end

        # Self-signed root CA
        if issuer_cert && issuer_cert.subject.to_s == issuer_cert.issuer.to_s && issuer_cert.verify(issuer_cert.public_key)
          return [certificate, issuer_cert]
        # Issuer of the certificate
        elsif issuer_cert && certificate.verify(issuer_cert.public_key)
          ca_chain = chain(issuer_cert)
          return ca_chain.is_a?(Array) ? [certificate] + ca_chain : ca_chain
        # Unknown issuer
        else
          # ERROR: return issuer string to help identify missing intermediate
          return issuer_str # [ certificate ]
        end
      end

      def intermediate_chain(ca_chain)
        ca_chain.reject { |cert| @ca_roots.include?(cert) }
      end

      def self.load_certificates(data)
        certificates = []
        reading_cert = false
        cert_buf = ""

        data.each_line do |line|
          line.gsub!(/\s+\Z/, "\n")
          case line
          when "-----BEGIN CERTIFICATE-----\n"
            reading_cert = true
            cert_buf << line
          when "-----END CERTIFICATE-----", "-----END CERTIFICATE-----\n"
            cert_buf << line
            begin
              certificates << OpenSSL::X509::Certificate.new(cert_buf.strip)
            rescue
              return []
            end
            cert_buf = ""
            reading_cert = false
          else
            cert_buf << line if reading_cert
          end
        end
        certificates
      end
    end
  end
end
