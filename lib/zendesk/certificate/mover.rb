module Zendesk
  module Certificate
    class Mover
      SOURCE_POD_RETENTION = 1.week

      def initialize(certificate)
        @certificate = certificate
      end

      # Allocate an IP for SSL on the new target pod in advance of the
      # swap operation.
      # @raise [NoCertificateIpsAvailableInPod]
      # @return [CertificateIp] newly allocated certificate IP
      def prepare(pod_id:, release_at: 1.month.from_now)
        unless new_ip = @certificate.certificate_ips.by_pod(pod_id).first
          CertificateIp.transaction do
            new_ip = CertificateIp.lock.find_unassigned_ip_for_pod(@certificate, pod_id)
            @certificate.certificate_ips << new_ip
          end

        end
        new_ip.schedule_release!(release_at) if release_at

        new_ip
      end

      # Promote IP in target_shard_id to the primary, and schedule the previous
      # source_shard_id IP address to be automatically released.
      # @return [CertificateIp] new primary IP
      def swap(source_pod_id, target_pod_id)
        old_ip = @certificate.certificate_ips.by_pod(source_pod_id).first ||
          raise(ActiveRecord::RecordNotFound, "No certificate IP is on source pod #{source_pod_id}.")
        new_ip = @certificate.certificate_ips.by_pod(target_pod_id).first ||
          raise(ActiveRecord::RecordNotFound, "No certificate IP assigned on target pod #{target_pod_id}. Please issue a `prepare` request to acquire an IP.")

        CertificateIp.transaction do
          new_ip.old_ip = old_ip.ip
          new_ip.save!
          new_ip.cancel_release!

          if old_ip != new_ip
            old_ip.schedule_release!(SOURCE_POD_RETENTION.from_now)
          end
        end

        new_ip
      end

      # Attempt to restore the IP previously used in the source pod. If IP
      # was already released and re-used, provide with any free
      # certificate_ip on the source_shard_id.
      # @return [CertificateIp] primary IP in the source pod
      def rollback(source_pod_id, target_pod_id)
        old_ip = @certificate.certificate_ips.by_pod(source_pod_id).first
        new_ip = @certificate.certificate_ips.by_pod(target_pod_id).first

        Rails.logger.info("No certificate IP assigned on target pod #{target_pod_id} to rollback from.") unless new_ip

        CertificateIp.transaction do
          # IP is still associated with certificate: keep it
          if old_ip
            old_ip.cancel_release!

          # IP was released, but is still unassigned: reclaim it
          elsif new_ip && new_ip.old_ip && old_ip = CertificateIp.unassigned.find_by_ip(new_ip.old_ip)
            old_ip.cancel_release!
            @certificate.certificate_ips << old_ip

          # IP was released and re-assigned: find a new one (may be unavailable until DNS settles out)
          else
            old_ip = prepare(pod_id: source_pod_id, release_at: nil)
          end

          new_ip.schedule_release!(1.month.from_now) if new_ip
        end

        old_ip
      end
    end
  end
end
