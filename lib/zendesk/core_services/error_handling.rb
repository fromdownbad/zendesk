module Zendesk
  module CoreServices
    module ErrorHandling
      def with_instrumentation(method, tags)
        yield
      rescue StandardError => e
        statsd_client.increment(method, tags: tags + ["result:error", "error:#{e.class}"])
        raise
      else
        statsd_client.increment(method, tags: tags + ["result:ok"])
      end

      private

      def statsd_client
        namespaces = self.class.to_s.downcase.split('::')[1..-1] # Remove top level module Zendesk from namespace
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: namespaces)
      end
    end
  end
end
