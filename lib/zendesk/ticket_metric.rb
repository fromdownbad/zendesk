require 'biz'
require 'finite_machine'

require 'zendesk/sla'

module Zendesk
  module TicketMetric
    SUPPORTED = %i[
      agent_work_time
      pausable_update_time
      periodic_update_time
      reply_time
      requester_wait_time
      resolution_time
    ].freeze

    LAUNCH    = %i[agent_work_time reply_time requester_wait_time].freeze
    RECURRING = %i[pausable_update_time periodic_update_time reply_time].freeze

    APM_SERVICE_NAME = 'classic-ticket-metric'.freeze

    def self.supported
      SUPPORTED
    end

    def self.launch?(metric)
      LAUNCH.include?(metric)
    end

    def self.recurring?(metric)
      RECURRING.include?(metric)
    end
  end
end

require 'zendesk/ticket_metric/action'
require 'zendesk/ticket_metric/calculation'
require 'zendesk/ticket_metric/context'
require 'zendesk/ticket_metric/none'
require 'zendesk/ticket_metric/period'
require 'zendesk/ticket_metric/periods'
require 'zendesk/ticket_metric/sla'
require 'zendesk/ticket_metric/state_machine'
require 'zendesk/ticket_metric/transition'
