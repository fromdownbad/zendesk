module Zendesk
  module ResourcefulActions
    def self.included(base)
      base.extend Zendesk::ResourcefulActions::ClassMethods
    end

    module ClassMethods
      def resource_action(name)
        case name
        when :destroy
          allow_parameters :destroy, id: ActionController::Parameters.bigid
          define_method(:destroy) do
            if resource.destroy
              default_delete_response
            else
              render json: presenter.present_errors(resource), status: :unprocessable_entity
            end
          end
        when :show
          allow_parameters :show, id: ActionController::Parameters.bigid
          define_method(:show) do
            render json: presenter.present(resource)
          end
        else
          raise "Not supported"
        end
      end
    end

    private

    def resource
      send(resource_name)
    end

    def resource_name
      @resource_name ||= self.class.name.split("::").last.underscore.sub("_controller", "").singularize
    end
  end
end
