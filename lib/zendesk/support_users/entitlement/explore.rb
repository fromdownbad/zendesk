module Zendesk
  module SupportUsers
    module Entitlement
      class Explore < Zendesk::SupportUsers::Entitlement::Base
        def payload(modify_is_active = true)
          case role
          when NO_AVAILABLE_ROLE
            nil
          else
            result = { 'name' => role }
            result['is_active'] = true if modify_is_active
            result
          end
        end

        def role
          @role ||= if user.permission_set.present? && user.roles == Role::AGENT.id
            case user.permission_set.permissions.explore_access
            when 'full'
              Zendesk::Entitlement::ROLES[:explore][:admin]
            when 'edit'
              Zendesk::Entitlement::ROLES[:explore][:studio]
            when 'readonly'
              Zendesk::Entitlement::ROLES[:explore][:viewer]
            else
              NO_AVAILABLE_ROLE
            end
          else
            case user.roles
            when ::Role::ADMIN.id
              Zendesk::Entitlement::ROLES[:explore][:admin]
            when ::Role::AGENT.id
              Zendesk::Entitlement::ROLES[:explore][:viewer]
            else
              NO_AVAILABLE_ROLE
            end
          end
        end
      end
    end
  end
end
