module Zendesk
  module SupportUsers
    module Entitlement
      class Support < Zendesk::SupportUsers::Entitlement::Base
        def role
          if user.is_end_user?
            nil
          elsif user.is_admin?
            Zendesk::Entitlement::ROLES[:support][:admin]
          elsif user.is_agent? && user.permission_set.present?
            user.permission_set.pravda_role_name
          else
            Zendesk::Entitlement::ROLES[:support][:agent]
          end
        end

        def payload
          return if role.nil?

          { 'name' => role, 'is_active' => true }
        end
      end
    end
  end
end
