module Zendesk
  module SupportUsers
    module Entitlement
      class Talk < Zendesk::SupportUsers::Entitlement::Base
        def grant!
          @user_seat = user.user_seats.new(account: user.account, seat_type: Zendesk::Entitlement::TYPES[:voice])
          result = user_seat.save
          sync if result
          result
        end

        def revoke!
          return unless user_seat
          result = user_seat.destroy
          sync if result
          result
        end

        def user_seat
          @user_seat ||= user.user_seats.voice.first
        end

        def payload
          case role
          when Zendesk::Entitlement::ROLES[:talk][:agent]
            { 'name' => role, 'is_active' => (user_seat.present? || legacy_talk?) }
          when Zendesk::Entitlement::ROLES[:talk][:lead], Zendesk::Entitlement::ROLES[:talk][:admin]
            { 'name' => role, 'is_active' => true }
          end
        end

        def role
          @role ||= if user.is_end_user? || user.is_contributor? || user.is_light_agent?
            NO_AVAILABLE_ROLE
          elsif !user.is_admin?
            Zendesk::Entitlement::ROLES[:talk][:agent]
          else
            (user_seat.present? || legacy_talk?) ? Zendesk::Entitlement::ROLES[:talk][:lead] : Zendesk::Entitlement::ROLES[:talk][:admin]
          end
        end

        def sync
          return unless synchronization.perform?
          synchronization.perform!
        end

        private

        def synchronization
          @synchronization ||= Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.new(user, user_seat: user_seat)
        end

        def legacy_talk?
          return false unless talk_product
          [3, 9].include? talk_product.plan_settings['plan_type']
        end

        def talk_product
          user.account.products.find { |p| p.name == :talk }
        end
      end
    end
  end
end
