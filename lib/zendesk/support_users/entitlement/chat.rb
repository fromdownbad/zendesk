module Zendesk
  module SupportUsers
    module Entitlement
      class Chat < Zendesk::SupportUsers::Entitlement::Base
        def role
          if user.is_agent?
            account.multiproduct? ? phase_4_entitlement : phase_3_entitlement
          end
        end

        private

        def phase_4_entitlement
          user.settings.chat_entitlement
        end

        def phase_3_entitlement
          zopim_identity = user.zopim_identity
          if zopim_identity && zopim_identity.is_enabled?
            zopim_identity.is_administrator? ? Zendesk::Entitlement::ROLES[:chat][:admin] : Zendesk::Entitlement::ROLES[:chat][:agent]
          end
        end

        def account
          user.account
        end
      end
    end
  end
end
