module Zendesk
  module SupportUsers
    module Entitlement
      class Guide < Zendesk::SupportUsers::Entitlement::Base
        def payload(modify_is_active = true)
          case role
          when NO_AVAILABLE_ROLE
            nil
          else
            result = { 'name' => role }
            result['is_active'] = true if modify_is_active
            result
          end
        end

        def role
          @role ||= begin
            permission_set = user.permission_set
            if permission_set.present? && user.roles == Role::AGENT.id
              if permission_set.is_light_agent? && user.account.has_ocp_guide_light_agent_role_sync?
                Zendesk::Entitlement::ROLES[:guide][:light_agent]
              elsif permission_set.is_system_role?
                Zendesk::Entitlement::ROLES[:guide][:viewer]
              elsif permission_set.manage_help_center
                Zendesk::Entitlement::ROLES[:guide][:admin]
              else
                Zendesk::Entitlement::ROLES[:guide][:agent]
              end
            else
              if user.is_moderator? || user.is_admin?
                Zendesk::Entitlement::ROLES[:guide][:admin]
              elsif user.is_agent?
                Zendesk::Entitlement::ROLES[:guide][:agent]
              else
                NO_AVAILABLE_ROLE
              end
            end
          end
        end
      end
    end
  end
end
