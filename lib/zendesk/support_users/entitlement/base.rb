module Zendesk
  module SupportUsers
    module Entitlement
      class Base
        attr_reader :user

        def initialize(user)
          @user = user
        end

        def role
          raise 'Must be implemented by subclass'
        end

        def no_available_role?
          role == NO_AVAILABLE_ROLE
        end
      end
    end
  end
end
