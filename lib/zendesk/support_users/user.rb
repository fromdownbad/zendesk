module Zendesk
  module SupportUsers
    class User
      attr_reader :user_model, :sync
      include BillableAgentCreation

      def self.retrieve(id)
        new(::User.find_by_id(id))
      end

      def initialize(user_model, sync: true)
        @user_model = user_model
        @sync = sync
      end

      def save!
        perform_before_save_actions!
        save_result = if billable_agent_creation?
          billable_agent_creation
        else
          user_model.save!
        end
        perform_after_save_actions!
        save_result
      end

      def delete!
        user_model.delete!
      end

      def save(validate: true)
        perform_before_save_actions!
        save_result = user_model.save(validate: validate)
        perform_after_save_actions! if save_result
        save_result
      end

      def assign_role(role)
        CIA.audit(actor: ::User.system) do
          user_model.roles = role
          save!
        end
      end

      private

      delegate :account, to: :user_model
      delegate :roles, to: :user_model

      def need_staff_initializer?
        @need_staff_initializer ||= staff_initializer.perform?
      end

      def perform_before_save_actions!
        staff_initializer.perform! if need_staff_initializer?
        user_entitlements_synchronization.perform_synchronously! if sync && user_entitlements_synchronization.perform_synchronously?
        guide_entitlements_synchronization.perform! if sync && need_staff_initializer? && user_model.is_contributor?
        chat_admin_validator.perform! if chat_admin_validator.perform?
        sell_admin_validator.perform! if sell_admin_validator.perform?
        agent_workspace_role_validator.perform! if account.has_agent_workspace_role_restriction? && agent_workspace_role_validator.perform?
        spp_light_agent_role_validator.perform! if spp_light_agent_role_validator.perform?
        # keep this the last step before save
        role_validator.perform! if role_validator.perform?
      end

      def perform_after_save_actions!
        if account.has_ocp_unified_entitlement_sync?
          entitlements_synchronization.perform! if sync && entitlements_synchronization.perform?
        else
          user_entitlements_synchronization.perform_asynchronously! if sync && user_entitlements_synchronization.perform_asynchronously?
          guide_entitlements_synchronization.perform! if sync && guide_entitlements_synchronization.perform?
          explore_entitlements_synchronization.perform! if sync && explore_entitlements_synchronization.perform?
          talk_entitlements_synchronization.perform! if sync && talk_entitlements_synchronization.perform?
        end
      end

      def staff_initializer
        Zendesk::SupportUsers::Internal::StaffInitializer.new(user_model)
      end

      def entitlements_synchronization
        Zendesk::SupportUsers::Internal::EntitlementsSynchronization.new(user_model)
      end

      def user_entitlements_synchronization
        Zendesk::SupportUsers::Internal::UserEntitlementsSynchronization.new(user_model)
      end

      def guide_entitlements_synchronization
        Zendesk::SupportUsers::Internal::GuideEntitlementsSynchronization.new(user_model)
      end

      def explore_entitlements_synchronization
        Zendesk::SupportUsers::Internal::ExploreEntitlementsSynchronization.new(user_model)
      end

      def talk_entitlements_synchronization
        Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.new(user_model)
      end

      def chat_admin_validator
        Zendesk::SupportUsers::Internal::ChatAdminValidator.new(user_model)
      end

      def sell_admin_validator
        Zendesk::SupportUsers::Internal::SellAdminValidator.new(user_model)
      end

      def agent_workspace_role_validator
        Zendesk::SupportUsers::Internal::AgentWorkspaceRoleValidator.new(user_model)
      end

      def spp_light_agent_role_validator
        Zendesk::SupportUsers::Internal::SppLightAgentRoleValidator.new(user_model)
      end

      def role_validator
        Zendesk::SupportUsers::Internal::RoleValidator.new(user_model)
      end
    end
  end
end
