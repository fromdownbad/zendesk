module Zendesk
  module SupportUsers
    module Internal
      class SppLightAgentRoleValidator
        def initialize(user)
          @user = user
        end

        def perform?
          account.spp? && user.is_light_agent?
        end

        def perform!
          unless seats_remaining_spp_light_agents > 0
            user.errors.add(:base, ::I18n.t('activerecord.errors.models.users.unassignable_role'))
            raise ActiveRecord::RecordInvalid, user
          end
        end

        private

        attr_reader :user
        delegate :account, to: :user

        def seats_remaining_spp_light_agents
          if account.arturo_enabled?('ocp_spp_light_agent_cap_alternative')
            account_client.max_light_agents - account.light_agents.count
          else
            staff_client.seats_remaining_spp_light_agents!
          end
        end

        def staff_client
          @staff_client ||= Zendesk::StaffClient.new(account)
        end

        def account_client
          @account_client ||= Zendesk::Accounts::Client.new(
            account,
            retry_options: {
              max: 3,
              interval: 2,
              exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS,
              methods: [:post]
            },
            timeout: 10
          )
        end
      end
    end
  end
end
