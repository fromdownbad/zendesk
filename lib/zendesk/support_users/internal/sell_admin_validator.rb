module Zendesk
  module SupportUsers
    module Internal
      class SellAdminValidator
        ADMIN_ROLE = 'admin'.freeze

        def initialize(user)
          @user = user
        end

        def perform?
          return false unless account.has_ocp_last_sell_admin_downgrade? && downgrading_agent_to_end_user?
          last_sell_admin?
        end

        def perform!
          user.errors.add(:base, ::I18n.t('activerecord.errors.models.users.removing_last_sell_admin'))
          raise ActiveRecord::RecordInvalid, user
        end

        private

        attr_reader :user

        def account
          @account ||= user.account
        end

        def downgrading_agent_to_end_user?
          !user.is_agent? && user.was_agent?
        end

        def last_sell_admin?
          (sell_entitlement == ADMIN_ROLE) && admin_staff_count <= 1
        end

        def admin_staff_count
          staff_client.staff_count(product: Zendesk::Accounts::Client::SELL_PRODUCT, role: ADMIN_ROLE)
        end

        def sell_entitlement
          staff_client.get_entitlements!(user.id)[:sell]
        end

        def staff_client
          @staff_client ||= Zendesk::StaffClient.new(account)
        end
      end
    end
  end
end
