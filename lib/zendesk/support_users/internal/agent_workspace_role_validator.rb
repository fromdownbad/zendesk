module Zendesk
  module SupportUsers
    module Internal
      class AgentWorkspaceRoleValidator
        def initialize(user)
          @user = user
        end

        def perform?
          return unless account.has_polaris? && updating_custom_role_id? && !current_user.is_system_user?
          user.is_chat_agent? || user.is_contributor?
        end

        def perform!
          user.errors.add(:base, ::I18n.t('activerecord.errors.models.users.unassignable_role'))
          raise ActiveRecord::RecordInvalid, user
        end

        private

        attr_reader :user

        def updating_custom_role_id?
          user.changes.key?('permission_set_id')
        end

        def current_user
          @current_user = CIA.current_actor
        end

        def account
          @account ||= user.account
        end
      end
    end
  end
end
