module Zendesk
  module SupportUsers
    module Internal
      class UserEntitlementsSynchronization
        include AuditHeaders

        ENTITLEMENT_COLUMNS = ['roles', 'permission_set_id'].freeze

        def initialize(user)
          @user = user
        end

        def perform_synchronously?
          account.multiproduct? && account.has_ocp_synchronous_entitlements_update? && account.has_central_admin_staff_mgmt_roles_tab? && should_sync_entitlement_changes?
        end

        def perform_asynchronously?
          !(account.multiproduct? && account.has_ocp_synchronous_entitlements_update? && account.has_central_admin_staff_mgmt_roles_tab?) && should_sync_entitlement_changes?
        end

        def perform_synchronously!
          Zendesk::SupportUsers::EntitlementSynchronizer.perform(user)
        rescue Kragle::ResponseError
          user.errors.add(:base, ::I18n.t('activerecord.errors.models.users.update_unavailable_due_to_upstream_dependency', name: user.name))
          raise ActiveRecord::RecordInvalid, user
        end

        def perform_asynchronously!
          Omnichannel::EntitlementSyncJob.enqueue(account_id: account.id, user_id: user.id, audit_headers: audit_headers)
        end

        private

        attr_reader :user
        delegate :account, to: :user

        def should_sync_entitlement_changes?
          Zendesk::SupportAccounts::Account.new(record: account).entitlements_sync_enabled? && entitlement_changed?
        end

        def entitlement_changed?
          columns_changed.any? && !end_user_to_contributor?
        end

        def columns_changed
          (user.changes + user.previous_changes).keys & ENTITLEMENT_COLUMNS
        end

        def end_user_to_contributor?
          user.previous_changes['roles'] == [Role::END_USER.id, Role::AGENT.id] && user.is_contributor?
        end

        def was_contributor?
          ::PermissionSet.find_by_id(permission_set_id_was).try(:is_contributor?)
        end

        def permission_set_id_was
          user.previous_changes['permission_set_id'].try(:first) || user.permission_set_id
        end
      end
    end
  end
end
