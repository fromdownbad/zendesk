module Zendesk
  module SupportUsers
    module Internal
      class TalkEntitlementsSynchronization
        include AuditHeaders

        ENTERPRISE_ENTITLEMENT_COLUMNS = ['roles', 'permission_set_id'].freeze

        def initialize(user, user_seat: nil)
          @user = user
          @user_seat = user_seat
        end

        def perform?
          return false unless user.is_agent?

          user_changed? || user_seat_changed?
        end

        def perform!
          Omnichannel::TalkEntitlementSyncJob.enqueue(account_id: account.id, user_id: user.id, audit_headers: audit_headers)
        end

        private

        attr_reader :user, :user_seat
        delegate :account, to: :user

        def user_changed?
          (user.previous_changes.keys & ENTERPRISE_ENTITLEMENT_COLUMNS).any?
        end

        def user_seat_changed?
          return unless user_seat && user_seat.seat_type == Zendesk::Entitlement::TYPES[:voice]
          return true if user_seat.destroyed?

          (user_seat.previous_changes.keys & ::UserSeatChangesObserver::COLUMNS_TO_INSTRUMENT).any?
        end
      end
    end
  end
end
