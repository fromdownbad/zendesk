module Zendesk
  module SupportUsers
    module Internal
      class ExploreEntitlementsSynchronization
        include AuditHeaders

        ENTERPRISE_ENTITLEMENT_COLUMNS = ['roles', 'permission_set_id'].freeze

        def initialize(user)
          @user = user
        end

        def perform?
          return false unless user.is_agent?
          return false unless user.is_admin? || user.permission_set_id.present? || account.has_permission_sets?
          (user.previous_changes.keys & ENTERPRISE_ENTITLEMENT_COLUMNS).any?
        end

        def perform!
          Omnichannel::ExploreEntitlementSyncJob.enqueue(account_id: account.id, user_id: user.id, audit_headers: audit_headers)
        end

        private

        attr_reader :user
        delegate :account, to: :user
      end
    end
  end
end
