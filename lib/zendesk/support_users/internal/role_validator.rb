module Zendesk
  module SupportUsers
    module Internal
      class RoleValidator
        def initialize(user)
          @user = user
        end

        def perform!
          user.permission_set_id = nil
        end

        def perform?
          user.is_admin? && user.permission_set_id &&
            !Arturo.feature_enabled_for?(:permissions_allow_admin_permission_sets, user.account)
        end

        private

        attr_reader :user
      end
    end
  end
end
