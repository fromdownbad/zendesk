module Zendesk
  module SupportUsers
    module Internal
      class EntitlementsSynchronization
        include AuditHeaders

        def initialize(user)
          @user = user
        end

        def perform?
          !products_to_sync.empty?
        end

        def perform!
          Omnichannel::EntitlementsSyncJob.enqueue(account_id: account.id, user_id: user.id, products: products_to_sync, audit_headers: audit_headers)
        end

        def products_to_sync
          @products_to_sync ||= begin
            products = []
            products << 'support' if support_entitlements_synchronization.perform_asynchronously?
            products << 'guide' if guide_entitlements_synchronization.perform?
            products << 'explore' if explore_entitlements_synchronization.perform?
            products << 'talk' if talk_entitlements_synchronization.perform?
            products
          end
        end

        private

        attr_reader :user
        delegate :account, to: :user

        def support_entitlements_synchronization
          @support_entitlements_synchronization ||= Zendesk::SupportUsers::Internal::UserEntitlementsSynchronization.new(user)
        end

        def guide_entitlements_synchronization
          @guide_entitlements_synchronization ||= Zendesk::SupportUsers::Internal::GuideEntitlementsSynchronization.new(user)
        end

        def explore_entitlements_synchronization
          @explore_entitlements_synchronization ||= Zendesk::SupportUsers::Internal::ExploreEntitlementsSynchronization.new(user)
        end

        def talk_entitlements_synchronization
          @talk_entitlements_synchronization ||= Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.new(user)
        end
      end
    end
  end
end
