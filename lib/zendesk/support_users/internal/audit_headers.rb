module Zendesk
  module SupportUsers
    module Internal
      module AuditHeaders
        def audit_headers
          {
            actor_id: CIA.current_actor ? CIA.current_actor.id : -1,
            ip_address: CIA.current_transaction&.fetch(:ip_address, "")
          }
        end
      end
    end
  end
end
