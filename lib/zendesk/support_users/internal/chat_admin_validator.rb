module Zendesk
  module SupportUsers
    module Internal
      class ChatAdminValidator
        def initialize(user)
          @user = user
        end

        def perform?
          return unless downgrading_agent_to_end_user?
          last_chat_admin?
        end

        def perform!
          admin_chat_entitlement = { chat: { name: 'admin', is_active: true } }
          staff_client.update_full_entitlements!(account.owner.id, admin_chat_entitlement)
        end

        private

        attr_reader :user

        def account
          @account ||= user.account
        end

        def downgrading_agent_to_end_user?
          !user.is_agent? && user.was_agent?
        end

        def last_chat_admin?
          (chat_entitlement == Zendesk::Entitlement::ROLES[:chat][:admin]) && admin_staff_count <= 1
        end

        def admin_staff_count
          staff_client.staff_count(product: Zendesk::Accounts::Client::CHAT_PRODUCT, role: Zendesk::Entitlement::ROLES[:admin])
        end

        def chat_entitlement
          staff_client.get_entitlements!(user.id)[:chat]
        end

        def staff_client
          @staff_client ||= Zendesk::StaffClient.new(account)
        end
      end
    end
  end
end
