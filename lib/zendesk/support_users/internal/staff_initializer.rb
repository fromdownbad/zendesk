module Zendesk
  module SupportUsers
    module Internal
      class StaffInitializer
        def initialize(user)
          @user = user
        end

        def perform?
          user.id.nil? && user.is_agent? && account.multiproduct? && account.has_ocp_synchronous_entitlements_update? && account.has_central_admin_staff_mgmt_roles_tab?
        end

        def perform!
          assigned_permission_set = user.permission_set
          assigned_roles = user.roles
          user.permission_set = ::PermissionSet.enable_contributor!(account)
          user.roles = ::Role::AGENT.id
          user.save!
          user.restriction_id = nil
          user.permission_set = assigned_permission_set
          user.roles = assigned_roles
        end

        private

        delegate :account, to: :user

        attr_reader :user
      end
    end
  end
end
