module Zendesk
  module SupportUsers
    module Internal
      class GuideEntitlementsSynchronization
        include AuditHeaders

        ENTERPRISE_ENTITLEMENT_COLUMNS = ['roles', 'permission_set_id', 'is_moderator'].freeze

        def initialize(user)
          @user = user
        end

        def perform?
          return false unless Zendesk::SupportAccounts::Account.new(record: account).entitlements_sync_enabled? && user.is_agent?
          (user.previous_changes.keys & ENTERPRISE_ENTITLEMENT_COLUMNS).any?
        end

        def perform!
          Omnichannel::GuideEntitlementSyncJob.enqueue(account_id: account.id, user_id: user.id, audit_headers: audit_headers)
        end

        private

        attr_reader :user
        delegate :account, to: :user
      end
    end
  end
end
