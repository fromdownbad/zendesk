# Update Staff Service with the user's current Support entitlement

module Zendesk
  module SupportUsers
    class EntitlementSynchronizer
      private_class_method :new
      PUSH_STRATEGY = :push
      PULL_STRATEGY = :pull

      def self.perform(user, strategy: PUSH_STRATEGY)
        new(user, strategy).sync_entitlement
      end

      def initialize(user, strategy)
        @user = user
        @strategy = strategy
      end

      def sync_entitlement
        return unless account.is_active?
        case strategy
        when PULL_STRATEGY
          sync_entitlement_from_staff_service
        when PUSH_STRATEGY
          sync_entitlement_to_staff_service
        else
          raise "Unrecognized strategy: #{strategy}"
        end
      end

      private

      def account
        user.account
      end

      def statsd_client
        @statsd_client ||= Zendesk::StatsD::Client.new(namespace: ['entitlements'])
      end

      def sync_entitlement_from_staff_service
        return unless account.multiproduct? || account.has_ocp_reverse_sync_non_multiproduct?

        support_entitlement = Zendesk::StaffClient.new(account).get_entitlements!(user.id)[Accounts::Client::SUPPORT_PRODUCT]
        return if support_entitlement.nil? && user.is_chat_agent?
        Rails.logger.info "Support Entitlement: #{support_entitlement} pulled for User: #{user.id} from staff service at #{Time.now}"
        CIA.audit(actor: ::User.system) do
          custom_role_id, roles = entitlement_translation(support_entitlement)
          user.permission_set_id = custom_role_id
          user.roles = roles
          user.reverse_sync = true
          Zendesk::SupportUsers::User.new(user, sync: false).save!
          # For enterprise accounts Explore and Guide role are tied to Support custom role and need to be syncd here
          # Only sync them when support role is changed
          Rails.logger.info "Account has permission_sets?: #{account.has_permission_sets?}, User's support role changed?: #{support_role_changed?}"
          if account.has_permission_sets? && support_role_changed?
            Rails.logger.info "About to update Explore and Guide entitlement for user: #{user.id}"
            Omnichannel::ExploreEntitlementSyncJob.enqueue(account_id: account.id, user_id: user.id, modify_is_active: false)
            Omnichannel::GuideEntitlementSyncJob.enqueue(account_id: account.id, user_id: user.id, modify_is_active: false)
          else
            Rails.logger.info "Skipped Explore and Guide update during reverse sync for user: #{user.id}"
          end
        end
        statsd_client.increment('reverse_sync', tags: ['result:success'])
      rescue StandardError => e
        message = "Entitlement sync failed for user: #{user.id}\n#{e.message}\n#{e.backtrace}"
        Rails.logger.error(message)
        statsd_client.increment('reverse_sync', tags: ['result:fail', "error:#{e.class}"])
        raise e
      end

      def entitlement_translation(support_entitlement)
        # Custom roles have a name that is prefixed with 'custom' followed by permission set id eg. custom_213534
        is_custom_role, role_id = support_entitlement&.split('_')
        if support_entitlement.nil?
          [::PermissionSet.enable_contributor!(account).id, ::Role::AGENT.id]
        elsif is_custom_role == Zendesk::SupportAccounts::Role::CUSTOM_ROLE_PREFIX
          [role_id, ::Role::AGENT.id]
        else
          case support_entitlement
          when Zendesk::SupportAccounts::Role::System::LIGHT_AGENT_ROLE_NAME
            [account.light_agent_permission_set.id, ::Role::AGENT.id]
          when Zendesk::SupportAccounts::Role::System::AGENT_ROLE_NAME
            [nil, ::Role::AGENT.id]
          when Zendesk::SupportAccounts::Role::System::ADMIN_ROLE_NAME
            # Keep the billing admin permissions set when reverse syncing the 'admin' role from staff service
            if user.permission_set.try(:is_billing_admin?) && Arturo.feature_enabled_for?(:permissions_allow_admin_permission_sets, account)
              [user.permission_set_id, ::Role::ADMIN.id]
            else
              [nil, ::Role::ADMIN.id]
            end
          end
        end
      end

      def sync_entitlement_to_staff_service
        return unless user.is_active?
        Rails.logger.info "Syncing entitlements for user: #{user.id} with entitlements: #{entitlements_payload}, end user: #{user_is_end_user?}"
        staff_client.update_entitlements!(user.id, entitlements_payload, user_is_end_user?)
      end

      def user_is_end_user?
        user.is_end_user? || (user.roles_was == ::Role::END_USER.id)
      end

      attr_reader :user, :strategy

      def current_entitlements
        staff_client.get_entitlements!(user.id, user_is_end_user?)
      end

      def staff_client
        @staff_client ||= Zendesk::StaffClient.new(user.account)
      end

      def entitlements_payload
        if user_is_end_user?
          current_entitlements.each_key.each_with_object({}) { |product, entitlements| entitlements[product] = nil }
        else
          { Zendesk::Entitlement::TYPES[:support] => Zendesk::SupportUsers::Entitlement.for(user, Zendesk::Accounts::Client::SUPPORT_PRODUCT).role }
        end
      end

      def support_role_changed?
        changed_attributes = user.previous_changes.keys
        changed_attributes.include?('roles') || changed_attributes.include?('permission_set_id')
      end
    end
  end
end
