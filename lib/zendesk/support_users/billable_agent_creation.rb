module Zendesk
  module SupportUsers
    module BillableAgentCreation
      def billable_agent_creation?
        return false if user_model.reverse_sync

        billable_agent? && !was_billable_agent?
      end

      def billable_agent_creation
        reserve_seat
        save_agent
      end

      def reserve_seat
        with_lock do
          seats = retrieve_seats
          raise_max_agents_exceeded_error unless seats > 0
          update_seats(seats - 1)
        end
      end

      def release_seat
        with_lock do
          seat = Rails.cache.read(cache_key)
          # if for some reason the cache gets cleared, just leave it to new
          # requests to recalculate the available seats.
          return unless seat

          update_seats(seat + 1)
        end
      end

      private

      def was_billable_agent?
        user_model.was_agent? && !user_model.was_unbillable_agent?
      end

      def billable_agent?
        user_model.is_agent? && !user_model.unbillable_agent?
      end

      def save_agent
        save_result = user_model.save!
      ensure
        release_seat unless save_result
      end

      def raise_max_agents_exceeded_error
        user_model.errors.add(
          :base,
          Api::Presentation::Error.new(
            'txt.error_message.models.roles.your_account_doesnt_allow_more_than',
            max_agents: user_model.account.subscription.max_agents,
            error: ::Users::Roles::MAX_AGENT_EXCEEDED
          )
        )
        raise ActiveRecord::RecordInvalid, user_model
      end

      def retrieve_seats
        Rails.cache.fetch(cache_key, expires_in: 10.seconds) do
          if account.has_ocp_check_seats_left_in_staff_service?
            begin
              staff_client.seats_remaining_support_or_suite! || (account.subscription.max_agents - account.billable_agents.count)
            rescue Kragle::ResponseError, Faraday::ClientError, Faraday::ConnectionFailed, Faraday::TimeoutError, Faraday::ServerError
              account.subscription.max_agents - account.billable_agents.count
            end
          else
            account.subscription.max_agents - account.billable_agents.count
          end
        end
      end

      def staff_client
        @staff_client ||= Zendesk::StaffClient.new(account)
      end

      def update_seats(value)
        Rails.cache.write(cache_key, value, expires_in: 10.seconds)
      end

      def cache_key
        "available_agent_seats_for_account_#{user_model.account.id}"
      end

      # GET_LOCK function takes two params
      # 1st param is a string used as lock identifier
      # 2nd param is a number used to decide the number of seconds to wait when reserving lock before timeout.
      def reserve_lock
        !ActiveRecord::Base.connection.execute("SELECT GET_LOCK('#{lock_id}', 2);").to_a.flatten.all?(&:zero?)
      end

      def release_lock
        ActiveRecord::Base.connection.execute("SELECT RELEASE_LOCK('#{lock_id}');")
      end

      def lock_id
        "agent_creation_lock_on_account_#{user_model.account.id}"
      end

      # raise error when fail to reserve lock
      def with_lock
        unless reserve_lock
          user_model.errors.add(:base, 'Too many requests.')
          raise ActiveRecord::RecordInvalid, user_model
        end

        yield
      ensure
        release_lock
      end
    end
  end
end
