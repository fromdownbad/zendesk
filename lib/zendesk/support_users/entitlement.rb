module Zendesk
  module SupportUsers
    module Entitlement
      NO_AVAILABLE_ROLE = 'no_available_role'.freeze

      def self.for(user, product)
        case product
        when Zendesk::Accounts::Client::SUPPORT_PRODUCT
          Zendesk::SupportUsers::Entitlement::Support.new(user)
        when Zendesk::Accounts::Client::CHAT_PRODUCT
          Zendesk::SupportUsers::Entitlement::Chat.new(user)
        when Zendesk::Accounts::Client::GUIDE_PRODUCT
          Zendesk::SupportUsers::Entitlement::Guide.new(user)
        when Zendesk::Accounts::Client::EXPLORE_PRODUCT
          Zendesk::SupportUsers::Entitlement::Explore.new(user)
        when Zendesk::Accounts::Client::TALK_PRODUCT, Zendesk::Entitlement::TYPES[:voice]
          Zendesk::SupportUsers::Entitlement::Talk.new(user)
        else
          raise "Unrecognized product: #{product}"
        end
      end
    end
  end
end
