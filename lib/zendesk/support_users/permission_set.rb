module Zendesk::SupportUsers
  class PermissionSet
    CONTEXT_IGNORE_PERMISSION_CHANGES = ['api_create', 'lotus_create', 'updated_plan', 'api_destroy'].freeze

    attr_reader :permission_set_model

    def initialize(permission_set_model)
      @permission_set_model = permission_set_model
    end

    def save!(context:)
      result = permission_set_model.save!
      return unless result

      role_sync(context)
      result
    end

    def save(context:)
      result = permission_set_model.save
      return unless result

      role_sync(context)
      result
    end

    def destroy(context:)
      result = permission_set_model.destroy
      return unless result

      role_sync(context)
      result
    end

    private

    def permissions_changed(context)
      return [] if CONTEXT_IGNORE_PERMISSION_CHANGES.include?(context)

      permission_set_model.permissions.map do |permission|
        next unless permission.previous_changes.any?

        permission.name
      end.compact
    end

    def account
      @account ||= permission_set_model.account
    end

    def role_sync(context)
      return unless role_sync_enabled?

      Omnichannel::RoleSyncJob.enqueue(
        account_id: account.id,
        permission_set_id: permission_set_model.id,
        role_name: permission_set_model.pravda_role_name,
        context: context,
        permissions_changed: permissions_changed(context)
      )
    end

    def role_sync_enabled?
      Zendesk::SupportAccounts::Account.new(record: account).role_sync_enabled?
    end
  end
end
