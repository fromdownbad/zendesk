module Zendesk
  module Billing
    class DunningCallbacks
      # This class allows for specific actions to be taken at each
      # dunning transition.
      #
      class << self
        def execute(level, notification)
          send(:"on_#{level}", notification)
        end

        def on_dunning_suspension(notification)
          acc                = notification.account
          acc.is_serviceable = false
          acc.is_active      = false
          acc.save(validate: false)

          notification.subscription.add_to_comment_field! "Sent dunning suspension notification"
        end

        def on_dunning_last_chance(notification)
          notification.account.update_attribute(:is_serviceable, false)
          notification.subscription.add_to_comment_field! "Sent dunning 'last chance' notification"
        end

        def on_dunning_second_warning(notification)
          notification.subscription.add_to_comment_field! "Sent dunning 'second warning' notification"
          nil
        end

        def on_dunning_warning(notification)
          notification.subscription.add_to_comment_field! "Sent dunning 'first warning' notification"
          nil
        end
      end
    end
  end
end
