module Zendesk
  module Billing
    class PaymentException < StandardError
    end
  end
end
