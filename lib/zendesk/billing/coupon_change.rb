module Zendesk
  module Billing
    class CouponChange
      attr_reader :presenter, :active_coupon_application, :previous_coupon_result, :new_coupon_result, :new_coupon_code

      def initialize(subscription)
        @subscription              = subscription
        @active_coupon_application = @subscription.active_coupon_application
        @presenter                 = CouponChangePresenter.new(self)
      end

      def consider(new_coupon_code = nil)
        @new_coupon_code = new_coupon_code
        validate_active_coupon if @active_coupon_application.present?

        if new_coupon_code
          if @active_coupon_application.present?
            override_existing_coupon_code_with new_coupon_code
          else
            validate_new_coupon_code new_coupon_code
          end
        end

        @presenter
      end

      def commit!
        @exhausted_coupon_application.try(:save!)
        @active_coupon_application.try(:save!)
      end

      def previous_coupon_code
        (@exhausted_coupon_application || @active_coupon_application).coupon.coupon_code rescue nil
      end

      private

      def override_existing_coupon_code_with(new_coupon_code)
        if new_coupon = Coupon.find_by_coupon_code(new_coupon_code)
          # no need to do anything if the new coupon code matches the
          # one we currently have
          return if previous_coupon_code == new_coupon_code

          # create a new coupon application to override the exisitng one
          # but first, we must exhaust the current active coupon application
          validate_new_coupon new_coupon do
            invalidate_active_coupon
          end
        else
          @new_coupon_result = :nonexistant # sic
        end
      end

      def validate_active_coupon
        if @active_coupon_application.is_valid_on?(@subscription)
          keep_active_coupon
        else
          invalidate_active_coupon
        end
      end

      def keep_active_coupon
        @previous_coupon_result = :still_applicable
      end

      def invalidate_active_coupon
        @previous_coupon_result                       = :invalidated
        @exhausted_coupon_application                 = @active_coupon_application
        @exhausted_coupon_application.exhaustion_date = Time.now
        @active_coupon_application                    = nil
      end

      def validate_new_coupon_code(new_coupon_code)
        if new_coupon = Coupon.find_by_coupon_code(new_coupon_code)
          validate_new_coupon(new_coupon)
        else
          @new_coupon_result = :nonexistant
        end
      end

      def validate_new_coupon(new_coupon)
        # make sure the new coupon is active/valid, otherwise reject it
        reject_inactive_new_coupon && return unless new_coupon.active?

        # allow for additional prep/processing before attempting to
        # create and apply a new coupon application
        yield if block_given?

        # create a new coupon application one where we could apply the new coupon
        new_coupon_application = @subscription.apply_coupon(new_coupon)

        # make sure it's applicable for the subscription, otherwise reject it
        if new_coupon_application.is_valid_on? @subscription
          accept_new_coupon_application(new_coupon_application)
        else
          reject_new_coupon_application
        end
      end

      def reject_inactive_new_coupon
        @new_coupon_result = :inactive
      end

      def accept_new_coupon_application(new_coupon_application)
        @active_coupon_application = new_coupon_application
        @new_coupon_result         = :success
      end

      def reject_new_coupon_application
        @new_coupon_result = :not_applicable
      end
    end
  end
end
