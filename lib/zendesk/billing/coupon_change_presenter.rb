module Zendesk
  module Billing
    class CouponChangePresenter
      MESSAGE_PRIORITIES = {
        new: {
          success: 1,
          nonexistant: 3,
          inactive: 3,
          not_applicable: 3,
          another_coupon_active: 5
        },

        previous: {
          invalidated: 2,
          still_applicable: 4,
        }
      }.freeze

      delegate :previous_coupon_result, :previous_coupon_code, :new_coupon_result, :new_coupon_code, to: :@coupon_change

      def initialize(coupon_change)
        @coupon_change = coupon_change
      end

      def which_message
        (MESSAGE_PRIORITIES[:new][new_coupon_result] || 100) < (MESSAGE_PRIORITIES[:previous][previous_coupon_result] || 100) ? :new : :previous
      end

      def coupon_message(future_tense = false)
        send("#{which_message}_coupon_message", future_tense)
      end

      # TODO: either get rid of :warn, :notice, etc, or use them in the controller, right now they aren't used enough to be worth maintaining
      def new_coupon_message(future_tense = false)
        code = ERB::Util.html_escape(new_coupon_code)

        case new_coupon_result
        when :success
          text = if future_tense
            ::I18n.t('txt.admin.lib.zendesk.coupon_change_presenter.code_will_be_applied', code: code)
          else
            ::I18n.t('txt.admin.lib.zendesk.coupon_change_presenter.was_applied', code: code)
          end
          [:notice, text, :notice]
        when :nonexistant
          [:warn,
           ::I18n.t('txt.admin.lib.zendesk.coupon_change_presenter.is_not_a_valid_code', code: code),
           :not_valid]
        when :inactive
          [:warn,
           ::I18n.t('txt.admin.lib.zendesk.coupon_change_presenter.is_not_an_active_coupon', code: code),
           :not_active]
        when :not_applicable
          [:warn,
           ::I18n.t('txt.admin.lib.zendesk.coupon_change_presenter.is_not_applicable', code: code),
           :not_allowed]
        when :another_coupon_active
          text = if future_tense
            ::I18n.t('txt.admin.lib.zendesk.coupon_change_presenter.will_be_blocked_by_previous_coupon', code: code, previous_coupon_code: previous_coupon_code)
          else
            ::I18n.t('txt.admin.lib.zendesk.coupon_change_presenter.was_blocked_by_previous_coupon', code: code, previous_coupon_code: previous_coupon_code)
          end
          [:notice, text, :notice]
        end
      end

      def previous_coupon_message(future_tense = false)
        case previous_coupon_result
        when :invalidated
          [:warn,
           "previous coupon \"#{previous_coupon_code}\" #{future_tense ? "will be" : "was"} removed",
           :ok]
        when :still_applicable
          [:notice,
           "previous coupon \"#{previous_coupon_code}\" #{future_tense ? "will " : ""}remain#{future_tense ? "" : "s"} active",
           :notice]
        end
      end
    end
  end
end
