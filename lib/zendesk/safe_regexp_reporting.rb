# used in app/models/custom_field/regexp.rb and app/models/ticket_fields/ticket_field_entry.rb
# tested in test/models/custom_field/values_test.rb and test/models/ticket_fields/ticket_field_entry_test.rb

module Zendesk
  module SafeRegexpReporting
    def safe_regexp_success
      Rails.application.config.statsd.client.increment("safe_regexp_success", tags: ["type:#{self.class.name}"])
    end

    def safe_regexp_error(exception, account_id, id)
      Rails.logger.warn("#{exception.class}: #{self.class.name}: account_id: #{account_id}, field id: #{id}")
      Rails.application.config.statsd.client.increment("safe_regexp_#{exception.class.name.underscore.split('/').last}", tags: ["type:#{self.class.name}", "account_id:#{account_id}"])
    end
  end
end
