class Zendesk::SimplifiedEmailThreading::OptIn
  SUPPORTED_RULE_TYPES = [Trigger, Automation].freeze
  EMAIL_TEMPLATES = %w[cc_email_template follower_email_template].freeze
  EMAIL_TEMPLATES_NO_ROLLBACK = %w[follower_email_template].freeze

  # Available placeholders from https://support.zendesk.com/hc/en-us/articles/203662156-Zendesk-Support-placeholders-reference#topic_jkz_opl_rc__table_tlz_opl_rc
  PLACEHOLDERS = %w[{{ticket.comments}}
                    {{ticket.latest_comment}}
                    {{ticket.comments_formatted}}
                    {{ticket.latest_comment_formatted}}
                    {{ticket.latest_comment_rich}}].freeze

  PUBLIC_PLACEHOLDERS = %w[{{ticket.public_comments}}
                           {{ticket.latest_public_comment}}
                           {{ticket.public_comments_formatted}}
                           {{ticket.latest_public_comment_formatted}}
                           {{ticket.latest_public_comment_rich}}].freeze

  REPLACEMENT_PLACEHOLDER = "{{ticket.latest_comment_html}}".freeze
  PUBLIC_REPLACEMENT_PLACEHOLDER = "{{ticket.latest_public_comment_html}}".freeze
  ACTION_SOURCES = %w[notification_user notification_group].freeze

  def initialize(account_id)
    @account = Account.find(account_id)
  end

  def clear_previously_saved_settings(dry_run: true)
    unless dry_run
      ActiveRecord::Base.transaction(requires_new: true) do
        destroyed_simplified_email_opt_in_settings = @account.simplified_email_opt_in_settings.destroy_all
        statsd_client.increment('destroyed_simplified_email_opt_in_settings') if destroyed_simplified_email_opt_in_settings.present?
        destroyed_simplified_email_rule_bodies = @account.simplified_email_rule_bodies.destroy_all
        statsd_client.increment('destroyed_simplified_email_rule_bodies') if destroyed_simplified_email_rule_bodies.present?
      end
    end
  end

  def update_html_template(dry_run: true)
    property_name = "html_mail_template"
    template = @account.public_send(property_name)

    new_template = if @account.has_no_mail_delimiter_enabled?
      Zendesk::OutgoingMail::Variables::HTML_SIMPLIFIED_MAIL_TEMPLATE_WITHOUT_DELIMITER
    else
      Zendesk::OutgoingMail::Variables::HTML_SIMPLIFIED_MAIL_TEMPLATE
    end

    unless dry_run
      ActiveRecord::Base.transaction(requires_new: true) do
        create_simplified_email_opt_in_setting!(property_name, template)
        @account.html_mail_template = new_template
        @account.save!
      end
    end

    true
  end

  def update_email_template(dry_run: true)
    is_updated = false
    templates = if @account.settings.ccs_followers_no_rollback
      EMAIL_TEMPLATES_NO_ROLLBACK
    else
      EMAIL_TEMPLATES
    end

    templates.each do |property_name|
      template = @account.public_send(property_name)
      replaced = replace_placeholders_in_template!(template.dup)

      next if template == replaced

      unless dry_run
        ActiveRecord::Base.transaction(requires_new: true) do
          create_simplified_email_opt_in_setting!(property_name, template)
          @account.update_attributes(Hash[property_name, replaced])
        end
      end

      is_updated = true
    end

    is_updated
  end

  def update_opt_in_rules(dry_run: true)
    ActiveRecord::Base.transaction do
      @account.on_shard do
        @account.rules.where(type: SUPPORTED_RULE_TYPES, is_active: true).each_with_object([]) do |rule, diffs|
          next unless rule.valid?

          action_index = action_index_to_update(rule.definition.actions)

          next if action_index.nil?

          unless dry_run
            replace_placeholders_in_rule!(rule, action_index)
          end

          diffs << rule.id
        end
      end
    end
  end

  private

  # identifies the action in a rule to be updated based on the number of matched placeholders
  # we only want to consider actions that have a single usage of either of the public or non-public comment placeholders
  # This singular action must have source targeting either 'Email User' or 'Email Group'
  def action_index_to_update(actions)
    notifying_actions = actions.each_with_index.select do |action, _index|
      ACTION_SOURCES.include?(action.source)
    end

    return nil if notifying_actions.size > 1 || notifying_actions.empty?

    email_action = notifying_actions.first

    email_body = email_action[0].value[2]
    index = email_action[1]

    return index if matched_placeholders(email_body).size == 1

    nil
  end

  def matched_placeholders(value)
    total_placeholders = PLACEHOLDERS + PUBLIC_PLACEHOLDERS
    total_placeholders.find_all { |e| value.split.count(e) == 1 }
  end

  def replace_placeholders_in_rule!(rule, action_index)
    rule.tap do |r|
      action = r.definition.actions[action_index]
      value = action.value[2]
      return unless value.present?

      match = matched_placeholders(value).first

      # create simplified_email_rule_body to store the rule body
      simplified_email_rule_body = create_simplified_email_rule_body(rule.id, value)

      if simplified_email_rule_body.valid? && !simplified_email_rule_body.errors.present?
        # replace placeholders in the email body with the replacement placeholder
        action.value[2].gsub!(match, replacement_placeholder(match))
        r.definition.actions[action_index] = action

        r.save! if r.valid?
      end
    end
  end

  def replace_placeholders_in_template!(template)
    matches = matched_placeholders(template)

    return template unless matches.size == 1

    match = matches.first

    # in case there's multiple instances of the same placeholder
    num_matches = template.split.count { |e| e == match }

    return template unless num_matches == 1

    template.gsub!(match, replacement_placeholder(match))
  end

  def replacement_placeholder(value)
    match = matched_placeholders(value).first

    if PLACEHOLDERS.include?(match)
      REPLACEMENT_PLACEHOLDER
    else
      PUBLIC_REPLACEMENT_PLACEHOLDER
    end
  end

  def create_simplified_email_opt_in_setting!(name, value)
    @account.simplified_email_opt_in_settings.create!(name: name, value: value)
  end

  def create_simplified_email_rule_body(rule_id, rule_body)
    @account.simplified_email_rule_bodies.create do |simplified_email_rule_body|
      simplified_email_rule_body.rule_id = rule_id
      simplified_email_rule_body.rule_body = rule_body
    end
  end

  def statsd_client
    Zendesk::StatsD::Client.new(namespace: 'simplified_email_threading_opt_in')
  end
end
