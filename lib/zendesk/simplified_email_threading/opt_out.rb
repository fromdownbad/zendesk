class Zendesk::SimplifiedEmailThreading::OptOut
  ACTION_SOURCES = %w[notification_user notification_group].freeze
  EMAIL_TEMPLATES = %w[cc_email_template follower_email_template].freeze

  def initialize(account_id)
    @account = Account.find(account_id)
  end

  def update_opt_out_rules(dry_run: true)
    ActiveRecord::Base.transaction(requires_new: true) do
      updated_rule_ids = []
      non_updated_rule_ids = []

      rules_map = @account.simplified_email_rule_bodies.each_with_object({}) do |simplified_email_rule_body, h|
        h[simplified_email_rule_body.rule_id] = simplified_email_rule_body
      end

      @account.rules.where(id: rules_map.keys).each do |rule|
        simplified_email_rule_body = rules_map[rule.id]

        if update_rule?(rule)
          updated_rule_ids.push(rule.id)
          action = notification_action(rule)
          action.value[2] = simplified_email_rule_body.rule_body
          rule.save! unless dry_run
        else
          non_updated_rule_ids.push(rule.id)
        end
      end

      @account.simplified_email_rule_bodies.destroy_all unless dry_run

      [updated_rule_ids, non_updated_rule_ids]
    end
  end

  def update_html_template(dry_run: true)
    property_name = "html_mail_template"
    original_template = @account.simplified_email_opt_in_settings.find_by(name: property_name)&.value

    return false if original_template.nil?

    unless dry_run
      ActiveRecord::Base.transaction(requires_new: true) do
        @account.html_mail_template = original_template
        @account.save!
        @account.simplified_email_opt_in_settings.where(name: property_name).destroy_all
      end
    end

    true
  end

  def update_email_template(dry_run: true)
    is_updated = false

    EMAIL_TEMPLATES.each do |property_name|
      original_template = @account.simplified_email_opt_in_settings.find_by(name: property_name)&.value

      next if original_template.nil?

      unless dry_run
        ActiveRecord::Base.transaction(requires_new: true) do
          @account.update_attributes(property_name => original_template)
          @account.simplified_email_opt_in_settings.where(name: property_name).destroy_all
        end
      end

      is_updated = true
    end

    is_updated
  end

  private

  def notification_action(rule)
    rule.definition.actions.find { |action| ACTION_SOURCES.include?(action.source) }
  end

  # Only update a rule if it has either a single notification_user or
  # notification_group action
  def update_rule?(rule)
    return false unless rule.valid?

    sources = rule.definition.actions.map(&:source)

    action_sources_count = ACTION_SOURCES.reduce(0) do |count, action_source|
      count += sources.count(action_source)

      return false if count > 1

      count
    end

    action_sources_count == 1
  end
end
