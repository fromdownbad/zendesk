module PageNotFoundRenderingSupport
  MARKETING_URL = "https://www.zendesk.com/app/help-center-closed/?utm_source=helpcenter-closed&utm_medium=poweredbyzendesk&utm_campaign=text&utm_content=".freeze

  protected

  def render_404_page
    if internally_redirect_request_to_hc?
      response.headers["X-Accel-Redirect"] = "/hc/404"
      head :not_found
    elsif current_brand
      render(
        file: "#{Rails.public_path}/404",
        formats: [:html],
        status: :not_found,
        layout: false
      )
    else
      respond_to do |format|
        format.html do
          redirect_to_marketing_page
        end

        format.all do
          head :not_found
        end
      end
    end
  end

  def redirect_to_marketing_page
    if blocked_domain?(request.host)
      head :not_found
      return
    end

    marketing_url = "#{MARKETING_URL}#{request.host}"
    redirect_to marketing_url, status: 301
  end

  private

  def blocked_domain?(subdomain)
    subdomain =~ /(p\d+assets|activehourshelp).zendesk.com/
  end

  EMPTY_BODY_REQUEST_METHODS = %w[GET HEAD DELETE].freeze

  def internally_redirect_request_to_hc?
    return false if current_brand.nil?
    return false unless current_brand.help_center_in_use?

    if defined?(current_account) && Arturo.feature_enabled_for?(:classic_help_center_redirect_on_non_body_only, current_account)
      return false if EMPTY_BODY_REQUEST_METHODS.exclude?(request.method)
    end

    true
  end
end
