module Identifiable
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def identity(id)
      model = find_by_id(id)
      model.nil? ? '-' : model.name
    end
  end
end
