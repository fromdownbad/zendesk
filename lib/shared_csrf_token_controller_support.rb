# Allow protection from forgery of browser requests with a CSRF token,
# that has been generated in Classic, from applications sharing a session
# with Classic.
#
# An example is Lotus talking to Help Center API. Thanks to this value
# saved in the shared session, Help Center can validate those requests
# as long as there is a logged in user in the same browser shared session.
module SharedCsrfTokenControllerSupport
  def self.included(base)
    base.after_action :ensure_csrf_token_into_shared_session
  end

  protected

  def ensure_csrf_token_into_shared_session
    if logged_in? && current_account && request.url.start_with?(current_account.authentication_domain)
      shared_session[:csrf_token] = form_authenticity_token
    end
  end
end
