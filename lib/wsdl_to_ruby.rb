require 'fileutils'

module WsdlToRuby
  # Generate driver files
  def self.generate(options)
    module_name         = options.fetch(:module_name)
    filename_mappings   = options.fetch(:filename_mappings)
    root_path           = options.fetch(:root_path)
    generated_root_path = File.join(root_path, 'generated')
    wsdl_path           = File.join(root_path, 'integration.wsdl')

    [root_path, generated_root_path, wsdl_path].each do |path|
      raise ArgumentError, "Could not find: #{wsdl_path}" unless File.exist?(path)
    end

    command = ["wsdl2ruby.rb", "--wsdl", wsdl_path, "--driver", "--classdef", "--mapping_registry", "--module_path", module_name, "--force"]
    puts "===== Running #{command.join(' ')}"
    system(*command)

    puts "===== Renaming files..."
    # Rename files_with_underscores and tag as generated
    filename_mappings.each do |original_file, renamed_file|
      output_path = File.join(generated_root_path, renamed_file)
      FileUtils.mv(original_file, output_path)
      puts "== Renaming #{original_file} > #{renamed_file}"

      # Replace require statements
      contents = File.read(output_path)
      filename_mappings.each do |current_require, new_require|
        new_require_string = File.join(generated_root_path, new_require)
        res = contents.gsub!(current_require, new_require_string)
        puts "Replaced '#{current_require}' with '#{new_require_string}'" unless res.nil?
      end

      File.open(output_path, "w") { |file| file.puts contents }
    end
  end
end
