require "arturo/test_support"

namespace :zobu do
  desc "Fill Gmail forwarding verification code"
  task :verify_gmail_code, [:subdomain, :email, :value] => [:environment] do |_task, args|
    subdomain = args.subdomain || "trial"
    email = args.email
    value = args.value

    raise "Email must be specified" if email.blank?
    raise "Verification code must be specified" if value.blank?

    Account.find_by_subdomain(subdomain).tap do |acc|
      abort "Unable to find account #{subdomain}" unless acc

      acc.on_shard do
        target = acc.forwarding_verification_tokens.build(to_email: email, from_email: email)
        target.value = value
        target.save!
      end

      puts "Gmail '#{email}' forwarding verification code is ready for account '#{subdomain}'"
    end
  end

  desc "Verify target email forwarding"
  task :verify_email_forwarding, [:subdomain, :email] => [:environment] do |_task, args|
    subdomain = args.subdomain || "trial"
    email = args.email

    raise "Email must be specified" if email.blank?

    Account.find_by_subdomain(subdomain).tap do |acc|
      abort "Unable to find account #{subdomain}" unless acc

      acc.on_shard do
        target_email = acc.recipient_addresses.where(email: email).first
        target_email.update_attribute(:forwarding_verified_at, Time.now)
      end

      puts "Email '#{email}' for account '#{subdomain}' is verified"
    end
  end

  desc "Enable heap tracking"
  task :enable_heap_tracking, [:type] => [:environment] do |_task, args|
    type = args.type || 'all'
    if /all|customer/.match?(type)
      Arturo.enable_feature!(:heap_integration_customer)
      puts "Enabled heap for customers"
    end

    puts "\e[1;33m[Notice] 'z3n' cannot be in subdomains or names, '@zendesk.com' can not be in owner addresses as well.\e[0m"
  end

  desc "Disable heap tracking"
  task :disable_heap_tracking, [:type] => [:environment] do |_task, args|
    type = args.type || 'all'
    if /all|customer/.match?(type)
      Arturo.disable_feature!(:heap_integration_customer)
      puts "Disabled heap for customers"
    end
  end

  desc "Make elite customer"
  task :make_elite, [:subdomain] => [:environment] do |_task, args|
    subdomain = args.subdomain || "trial"

    Account.find_by_subdomain(subdomain).tap do |acc|
      abort "Unable to find account #{subdomain}" unless acc

      acc.on_shard do
        unless acc.subscription.is_elite?
          acc.subscription.build_zuora_subscription(
            plan_type:              4,
            is_active:              true,
            is_elite:               true,
            zuora_account_id:       'fake',
            pricing_model_revision: 5,
            max_agents:             50
          ).save!
        end
        puts "Account '#{subdomain}' is elite now."
      end
    end
  end

  desc "Revert elite customer to trial"
  task :revert_elite, [:subdomain] => [:environment] do |_task, args|
    subdomain = args.subdomain || "trial"

    Account.find_by_subdomain(subdomain).tap do |acc|
      abort "Unable to find account #{subdomain}" unless acc

      if acc.subscription.is_elite?
        acc.subscription.zuora_subscription.destroy
      end
      puts "Account '#{subdomain}' reverted to trial from elite."
    end
  end

  desc "Make target account use lotus"
  task :disable_web_portal, [:subdomain] => [:environment] do |_task, args|
    subdomain = args.subdomain || "trial"

    Account.find_by_subdomain(subdomain).tap do |acc|
      abort "Unable to find account #{subdomain}" unless acc

      acc.on_shard do
        acc.settings.prefer_lotus = true
        acc.settings.web_portal_state = :disabled
        acc.settings.save!
      end
      puts "Account '#{subdomain}' is using lotus now"
    end
  end

  desc "pre-create an account with the same locale of a targeted subdomain"
  task :pre_create_account, [:subdomain] => [:environment] do |_task, args|
    subdomain = args.subdomain || "trial"

    Account.find_by_subdomain(subdomain).tap do |acc|
      abort "Unable to find account #{subdomain}" unless acc

      pre_creation = acc.pre_account_creation || acc.on_shard do
        PreAccountCreation.create!(
          account_id: acc.id,
          locale_id: acc.locale_id,
          pod_id: 1,
          source: 'Marketing website',
          account_class: 'Accounts::Classic',
          region: 'us'
        )
      end
      pre_creation.precreate_new_account
      pre_creation.destroy

      puts "Make sure zendesk_worker was running, a pre-creation job is submitted."
    end
  end
end
