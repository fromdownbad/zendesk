namespace :arturo do
  desc "Lists all arturo features and whether they are enabled or not. Can be filtered with FILTER=word."
  task features: :environment do
    filter = ENV['FILTER']

    ActiveRecord::Base.on_shard(1) do
      features = Arturo::Feature.all
      features = features.select { |f| f.symbol.include?(filter) } if filter
      features.sort_by(&:symbol).each do |f|
        color = 32 if f.phase == "on" # green
        color ||= 31 # red
        puts "\033[#{color}m#{f.symbol.ljust(55, '.')}#{f.phase}\033[0m"
      end
    end
  end
end
