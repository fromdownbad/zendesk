namespace :system_users do
  task generate_token: :environment do
    puts YAML.dump('oauth' => SecureRandom.hex(Zendesk::OAuth.config.token_length))
  end
end
