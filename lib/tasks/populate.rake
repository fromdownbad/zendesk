# lib/tasks/populate.rake
namespace :populate do
  desc "Scrambles all users, accounts and textual data"
  task scramble: :environment do
    require 'faker'
    require 'populator'

    counter = 0
    puts "#{Time.now} Updating accounts..."

    ActiveRecord::Base.on_all_shards do
      puts "#{Time.now} Updating users..."
      counter = 0

      User.where('id > 0').select('id, phone').each do |u|
        parameters = {
          email: "#{Faker::Internet.user_name}@#{Faker::Internet.domain_word}.example.com",
          name:  Faker::Name.name.gsub("'", "\\'")
        }
        parameters.merge(phone: Faker::PhoneNumber.phone_number) unless u.phone.blank?
        User.where(id: u.id).update_all(parameters)
        counter += 1
        puts "#{Time.now} #{counter} records done" if counter % 10000 == 0
      end

      puts "#{Time.now} Updating tickets..."
      counter = 0

      Ticket.select('id, original_recipient_address').each do |t|
        fields = {
          'subject' => Populator.sentences(1),
          'description' => Populator.sentences(3..10),
          'description_html' => nil,
          'original_recipient_address' => t.original_recipient_address.blank? ? nil : "#{Faker::Internet.user_name}@#{Faker::Internet.domain_word}.example.com",
        }

        Ticket.connection.execute(
          "UPDATE tickets SET subject = '#{fields['subject']}',
                  description       = '#{fields['description']}',
                  description_html  = NULL,
                  original_recipient_address = #{fields['original_recipient_address'].nil? ? 'NULL' : fields['original_recipient_address']}"
        )
        counter += 1
        puts "#{Time.now} #{counter} records done" if counter % 10000 == 0
      end
    end
  end

  desc "Initializes the account"
  task initialize: :environment do
    require 'populator'
    require 'faker'

    sitename = ENV['site'] || 'generated'

    unless Account.exists?(subdomain: sitename)
      account = Accounts::Classic.new

      ActiveRecord::Base.on_shard(account.shard_id) do
        account.subdomain   = sitename
        account.time_zone   = 'Copenhagen'
        account.name        = Faker::Company.name

        account.set_owner(name: Faker::Name.name, email: Faker::Internet.email)
        account.set_address(name: account.name, source_id: 2, usage_id: 2, phone: Faker::PhoneNumber.phone_number, country: 'Denmark')

        account.save!
      end
    end
  end

  desc "Destroy"
  task destroy: :environment do
    sitename = ENV['site'] || 'generated'

    if account = Account.find_by_subdomain(sitename)
      ActiveRecord::Base.on_shard(account.shard_id) do
        account.destroy
      end
    end
  end

  desc "Add 20 users"
  task users: :initialize do
    sitename = ENV['site'] || 'generated'

    account = Account.find_by_subdomain(sitename)
    ActiveRecord::Base.on_shard(account.shard_id) do
      20.times do
        user            = User.new
        user.account_id = account.id
        user.name       = Populator.words(2).titleize
        user.roles      = Role::END_USER.id
        user.email      = Faker::Internet.email
        user.save!
      end
    end
  end

  desc "Add 50 tickets from each user"
  task tickets: :initialize do
    sitename = ENV['site'] || 'generated'

    account = Account.find_by_subdomain(sitename)
    ActiveRecord::Base.on_shard(account.shard_id) do
      account.users.each do |user|
        50.times do
          ticket              = Ticket.new
          ticket.subject      = Populator.words(3..7).titleize
          ticket.description  = Populator.sentences(3..30)
          ticket.requester_id = user.id
          ticket.will_be_saved_by(user)
          ticket.save!
        end
      end
    end

    nil
  end

  namespace :performance do
    desc "Adds 50 FieldTaggers, each with 50 options"
    task field_taggers: :environment do
      populate("field_taggers", "ticket_fields", "custom_field_options") do
        account = Account.find_by_name("performance_test_account") ||
          Factory.create(:account, name: 'performance_test_account')

        ActiveRecord::Base.on_shard(account.shard_id) do
          puts "Creating 50 field taggers - you will see 50 dots"
          50.times do |i|
            print "."
            FieldTagger.create! do |tagger|
              tagger.account = account
              tagger.title = "Field Tagger #{i}"
              tagger.description = "This is a fake field tagger for performance testing. #{i} / 50"
              50.times do |j|
                tagger.custom_field_options.build(
                  name: "Tagger #{i} Option #{j}",
                  value: "tag_#{i}_#{j}"
                )
              end
            end
          end
        end
      end

      nil
    end

    desc "Add 1200 macros"
    task macros: :environment do
      populate("macros", "rules") do
        account = Account.find_by_name('performance_test_account') ||
          Factory.create(:account, name: 'performance_test_account')

        owners = [
          account,
          User.find_by_roles(Role::END_USER.id),
          Group.find_by_account_id(account.id)
        ]

        ActiveRecord::Base.on_shard(account.shard_id) do
          puts "Creating 1200 new macros - you will see 120 dots"
          1200.times do |i|
            print "." if i % 10 == 0
            Macro.create! do |m|
              m.account = account
              m.owner = owners[rand(owners.size)]
              m.title = "Macro #{i}"
              m.definition = Definition.build(actions: [{ source: "status_id", operator: "is", value: { 0 => "1" }}])
            end
          end
        end
      end

      nil
    end

    desc "Add 1 ticket with 500 assignable_agents and 20 assignable_groups."
    task tickets: :environment do
      populate("tickets") do
        puts 'Creating new AR objects for the test DB'

        account = Account.find_by_name('performance_test_account') ||
          Factory.create(:account, name: 'performance_test_account')
        agent   = Factory.create :agent,     account: account
                                         # :is_moderator => true
        end_user = Factory.create :end_user, account: account
        group    = Factory.create :group,    account: account
        group.users << agent

        ActiveRecord::Base.on_shard(account.shard_id) do
          ticket = Ticket.new requester: end_user,
                              submitter: agent,
                              assignee: agent,
                              group: group,
                              subject: 'A ticket',
                              description: 'Yes, a ticket'
          ticket.will_be_saved_by(end_user)
          ticket.save!
        end # on_shard

        puts "Creating 500 users - you will see 50 dots"
        500.times do |i|
          print '.' if i % 10 == 0
          Factory.create :agent, account: account
        end
        puts

        # 100 agents to add to each group
        agent_selection = account.agents.limit(100).to_a

        puts "creating 20 groups - you will see 20 dots"
        20.times do
          print '.'
          group = Factory.create :group, account: account
          # Add 100 agents to each group
          agent_selection.each { |agent| group.users << agent }
        end
      end

      nil
    end

    desc "Adds from 1-100 satisfaction ratings per ticket (500)"
    task ratings: :environment do
      populate("satisfaction_ratings") do
        puts 'Creating new AR objects for the test DB'

        account = Account.find_by_name('performance_test_account') ||
          Factory.create(:account, name: 'performance_test_account')

        agents = account.agents
        users = account.end_users

        puts "Creating 500 tickets - you will see 50 dots"
        ActiveRecord::Base.on_shard(account.shard_id) do
          500.times do |i|
            print '.' if i % 10 == 0

            agent = agents.sample
            user = users.sample

            t = Ticket.new(requester: user, submitter: agent,
                           assignee: agent, group: agent.groups.first,
                           subject: 'Ticket', description: 'Yes.')

            t.will_be_saved_by(user)
            t.save!

            (rand(100) + 1).times do
              Satisfaction::Rating.create!(ticket_id: t.id, agent_user_id: agent.id,
                                           enduser_id: user.id, event_id: t.events.first.id, account_id: account.id,
                                           score: [SatisfactionType.GOOD, SatisfactionType.GOODWITHCOMMENT, SatisfactionType.BAD, SatisfactionType.BADWITHCOMMENT].sample)
            end
          end

          puts
        end
      end

      nil
    end

    desc "Add 25 categories, each with 10 forums, each with 10 entries. Last entry has 100 posts. Dumps DB to a file once per day."
    task forums: :environment do
      populate("forums") do
        puts 'Creating new AR objects for the test DB'

        account = Factory.create :account, name: 'performance_test_account'
        user    = Factory.create :agent, account: account,
                                         is_moderator: true

        ActiveRecord::Base.on_shard(account.shard_id) do
          print '  '

          25.times do
            print '.'
            category = Factory.create(:category, account: account)

            10.times do
              forum = Factory.create(:forum, account: account,
                                             category: category)

              10.times do
                Factory.create(:entry, account: account,
                                       forum: forum,
                                       submitter: user,
                                       current_user: user)
              end # Entries
            end # Forums
          end # Categories
          puts

          entry_with_comments = account.categories.last.forums.last.entries.last
          forum = entry_with_comments.forum

          100.times do |index|
            Factory.create(:post, account: account,
                                  forum: forum,
                                  entry: entry_with_comments,
                                  user: user,
                                  created_at: (index + 8).days.ago,
                                  updated_at: (index + 3).days.ago)
          end
        end # on_shard
      end

      nil
    end

    private

    def populate(dump_file_name, *tables)
      config = Rails.configuration.database_configuration['test']

      raise "This Rake task only works with MySQL" unless config['adapter'] =~ /mysql/

      # Re-create the dump file once per day
      dump_file   = Rails.root.join 'tmp', 'performance', 'db_dump', "#{dump_file_name}_#{Date.today.to_s(:db)}.sql"
      use_db_dump = File.exist?(dump_file)

      if use_db_dump
        puts "Creating test DB from dump file #{dump_file}"

        `mysql -u #{config['username']} #{config['database']} -h #{config['host']} -P #{config['port']} < #{dump_file}`
      else
        yield
        puts "\n\tDumping created test DB into dump file #{dump_file}"

        FileUtils.mkdir_p(File.dirname(dump_file))
        `mysqldump -u #{config['username']} #{config['database']} #{tables.join(' ')} -h #{config['host']} -P #{config['port']} > #{dump_file}`
      end
    end
  end
end
