require 'resque/tasks'
require 'resque/pool/tasks'
require 'zendesk/lifecycle'
require 'zendesk/web_lifecycle'

# remove_task is defined in zendesk_database_migrations/tasks/shared.rake
Rake.application.remove_task 'resque:preload'

namespace :resque do
  desc "patch resque to not do any forking"
  task :no_fork do
    ENV['FORK_PER_JOB'] = 'false'
  end

  task preload: [:no_fork] do
    Rails.application.require_environment!
  end
end

task "resque:pool:setup" do
  Zendesk::Lifecycle::Base.prepend(Zendesk::WebLifecycle)
  Zendesk::Lifecycle::Base.new.close_connections

  # In kubernetes, we use resque_pool to manage multiple workers per k8s-pod but they all work on a
  # single queue.
  if KUBERNETES
    Resque::Pool.config_loader = ->(_env) {
      queue = ENV.fetch('QUEUE')
      worker_count = Integer(ENV.fetch("RESQUE_WORKERS_FOR_#{queue}", '5'))

      { queue => worker_count }
    }
  elsif ENV['CONFIGURE_RESQUE_POOL_FROM_CONSUL'] == 'true'
    Resque::Pool.config_loader = ->(_env) {
      Diplomat::Kv.get('global/classic/resque_pool', transformation: JSON.method(:load))
    }
  end

  Resque::Pool.after_prefork do |_job|
    # If a worker dies, the parent resque-pool-master connects to Redis to log
    # the queues of the reaped worker.
    Zendesk::Lifecycle::Base.new.close_connections
  end
end
