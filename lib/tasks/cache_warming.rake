namespace :cache do
  desc "Warm locales cache"
  task warm_locales: :environment do
    require "pathname"
    include ERB::Util
    include GeneratedAssetsHelper

    template = ERB.new(File.read(Rails.root + "app/views/generated/javascripts/locale.erb"))
    locales = TranslationLocale.all + [ENGLISH_BY_ZENDESK]
    locales.each do |locale|
      I18n.locale = locale
      file_name = Pathname.new((Rails.root + "public").to_s + current_locale_path)
      next if file_name.exist?

      begin
        puts "Generating locale file #{file_name}"
        file_name.parent.mkpath
        File.open(file_name, "w") do |file|
          file.write(template.result)
        end
      rescue StandardError => e
        puts "Failed generating locale file #{file_name}. #{e.message}"
      end
    end
  end

  desc "Clear locales cache"
  task clear_locales: :environment do
    require 'fileutils'
    FileUtils.rm_rf(Dir.glob(Rails.root + "public/generated/javascripts/locale/*"))
  end

  desc "Warm caches during deploy"
  task warm: [:warm_locales]
end
