namespace :admin_assets do
  desc "Copy in admin assets from the admin_assets `build` directory"
  task :import do
    sh 'cp', '../admin_assets/build/admin.js',  'app/assets/javascripts/admin/vendor/admin_assets/admin.js'
    sh 'cp', '../admin_assets/build/admin.css', 'app/assets/stylesheets/views/admin/vendor/admin_assets/admin.css'
  end
end
