namespace :db do
  namespace :shard do
    desc 'create the initial account on a brand new shard'
    task create_initial_account: :environment do
      require 'securerandom'
      shard_id = ENV['SHARD_ID']
      email    = ENV['EMAIL']

      abort 'Please specify SHARD_ID=id on the command line' if shard_id.blank?
      abort 'Please specify EMAIL=email on the command line' if email.blank?

      shard_id = shard_id.to_i
      password = SecureRandom.hex + 'Aa1-'

      ActiveRecord::Base.on_shard(shard_id) do
        account = Accounts::Classic.new do |a|
          a.shard_id       = shard_id
          a.subdomain      = ENV['SUBDOMAIN'] || "shard#{shard_id}"
          a.name           = "Shard #{shard_id}"
          a.time_zone      = "Pacific Time (US & Canada)"
          a.help_desk_size = "Small team"

          a.set_address(country_code: "US", phone: '123456')

          a.set_owner(
            name: 'Owner',
            email: email,
            password: password,
            skip_verification: true
          )
        end

        account.save!
        account.subscription.update_attributes!(
          managed_update: true,
          manual_discount: 100,
          manual_discount_expires_on: 10.years.from_now.to_date,
          is_trial: false,
          plan_type: SubscriptionPlanType.ExtraLarge
        )

        account.role_settings.update_attributes!(
          agent_security_policy_id: Zendesk::SecurityPolicy::Low.id
        )

        account.save!
        account.update_attribute(:is_serviceable, true)
        update_product_record(account)

        puts "Created account at: #{account.url}"
        puts "owner email: #{account.owner.email}"
        puts "owner password: #{password}"
      end
    end

    desc 'create canary accounts via create_canary_accounts[start_shard_id,end_shard_id]'
    task :create_canary_accounts, [:start_shard, :end_shard] => :environment do |_task, args|
      require 'zendesk/accounts/canary_creator'
      (args[:start_shard]..args[:end_shard]).each do |shard_id|
        account = Zendesk::Accounts::CanaryCreator.create_for_shard!(shard_id)
        puts account
        puts "Created account at: #{account.url}"
      end
    end

    desc 'create 1 canary account for a db cluster [cluster_id, shard_id]'
    task :create_canary_account_for_cluster, [:cluster_id, :shard_id] => :environment do |_task, args|
      require 'zendesk/accounts/canary_creator'
      account = Zendesk::Accounts::CanaryCreator.create_for_cluster!(args[:cluster_id], args[:shard_id])
      puts account
      puts "Created account at: #{account.url}"
    end

    def update_product_record(account)
      product_params = {
        product: {
          state: 'free',
          plan_settings: {
            plan_type: account.subscription.plan_type,
            max_agents: account.subscription.max_agents,
            light_agents: account.subscription.has_light_agents?
          }
        }
      }

      Zendesk::Accounts::Client.new(
        account.id,
        Zendesk::Accounts::Client::DEFAULT_SUBDOMAIN,
        retry_options: {
          max: 3,
          interval: 2,
          exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS.dup << Kragle::ResourceNotFound,
          methods: [:patch]
        },
        timeout: 10
      ).update_product('support', product_params, context: "shards_rake")
    end
  end
end
