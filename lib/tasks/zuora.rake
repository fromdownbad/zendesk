namespace :zuora do
  desc 'Export Zuora product rate plan data to csv files (rake zuora:export_product_catalog)'
  task export_product_catalog: :environment do
    include ZBC::Zuora::Session
    Rails.cache.clear
    ZuoraClient::Exporter.new(session).export!
  end

  desc 'Switch a Zuora Customer to a TRIAL account'
  task :reset_trial, [:subdomain] => :environment do |_task, args|
    subdomain = args.subdomain || 'trial'
    acct = Account.find_by_subdomain(subdomain)

    if acct.subscription.is_trial?
      puts('✘  Account is already a TRIAL account (try: rake zuora:make_customer)')
    else
      ActiveRecord::Base.on_shard(1) do
        acctid = acct.id
        zs     = ZBC::Zuora::Subscription.find_by_account_id(acctid)
        ZBC::Zuora::Client.session.account.destroy(zs.zuora_account_id)

        s = Subscription.find_by_account_id(acctid)

        s.attributes = {
          is_trial:            true,
          payment_method_type: 0,
          trial_expires_on:    30.days.from_now,
          billing_cycle_type:  BillingCycleType.Monthly
        }
        s.save(validate: false)

        zs.delete

        ZBC::Zuora::CouponRedemption.where(account_id: acctid).each(&:delete)
      end

      puts '✔  Zuora Customer converted into a TRIAL account'
    end
  end

  desc 'Change plan type for a patagonia account (defaults to Large)'
  task :change_plan_type, [:subdomain, :plan_type] => :environment do |_task, args|
    type_arg = args.plan_type || 'Large'
    subdomain = args.subdomain || 'trial'
    plan_type = Zendesk::Types::SubscriptionPlanType.find(type_arg)
    acct = Account.find_by_subdomain(subdomain)

    if acct.subscription.pricing_model_revision != 6
      puts('✘ Account is not on Patagonia pricing model (try: rake zuora:switch_to_patagonia)')
    else
      ActiveRecord::Base.on_shard(1) do
        acctid = acct.id
        s = Subscription.find_by_account_id(acctid)
        s.update_attributes! plan_type: plan_type
      end
      puts "✔ Customer #{subdomain} switched to #{type_arg}"
    end
  end

  desc 'Switch a TRIAL account to a Zuora Customer'
  task make_customer: :environment do
    subscribe!
  end

  desc 'Switch a TRIAL account to a Zuora Customer with Coupon'
  task make_coupon_customer: :environment do
    subscribe!(promo_code: 'PERCENTAGE50')
  end

  desc 'Switch a TRIAL account to a paid Legacy Zuora Customer'
  task make_paid_customer: :environment do
    subscribe!(invoice_options: {
      generate_invoice: true,
      process_payments: true
    })
  end

  desc 'Switch an account to a paid PATAGONIA Zuora Customer'
  task :make_paid_patagonia_customer, [:subdomain] => :environment do |_task, args|
    subscribe_to_patagonia!(args.to_h.merge(
      invoice_options: {
        generate_invoice: true,
        process_payments: true
      }
    ))
  end

  desc 'Create trial Zopim subscription for specified account'
  task :create_zopim_trial, [:subdomain] => :environment do |_task, args|
    ZBC::Zopim::Subscription.class_eval do
      def install_app; end

      def sync; end

      def link_account_record; end

      def synchronize_remote_account_record; end
    end
    ZopimIntegration.class_eval do
      def install_app; end
    end
    Zopim::Agent.class_eval do
      def link_to_remote_account_record; end
    end

    subdomain = args.subdomain || 'trial'
    acct = Account.find_by_subdomain(subdomain)

    ActiveRecord::Base.on_shard(1) do
      new_email = acct.owner.email.split(/@/).insert(1, "#{(rand * 10000).round}@").join
      unless acct.zopim_integration
        acct.build_zopim_integration.tap do |r|
          r.external_zopim_id = rand(10_000)
          r.zopim_key = 'abc'
          r.phase     = 3
        end.save!
      end

      zs = acct.zopim_subscription || acct.build_zopim_subscription
      if zs.new_record?
        zs.subscription_id = acct.subscription.id
        zs.zopim_account_id = acct.zopim_integration.external_zopim_id
        zs.plan_type = ZBC::Zopim::PlanType::Trial.name
        zs.max_agents = 10
        zs.billing_cycle_type = 1
        zs.owner_email = new_email
        zs.status = ZBC::Zopim::SubscriptionStatus::Active
        zs.zopim_reseller_id = ZBC::Zopim::Subscription::Creation::RESELLER_ACCOUNT_ID
        zs.syncstate = ZBC::Zopim::SyncState::Ready
        zs.save!
      end
    end

    puts "✔  Zopim subscription is added to Account #{subdomain}"
  end

  desc 'Delete trial Zopim subscription for an account'
  task :reset_zopim_trial, [:subdomain] => :environment do |_task, args|
    ZBC::Zopim::Subscription.class_eval do
      def uninstall_app; end

      def destroy_zopim_agents; end
    end
    ZopimIntegration.class_eval do
      def uninstall_app; end
    end
    ZBC::Zopim::SubscriptionDeactivator.class_eval do
      def deactivate!; end
    end

    subdomain = args.subdomain || 'trial'
    acct = Account.find_by_subdomain(subdomain)

    ActiveRecord::Base.on_shard(1) do
      acct.zopim_agents.destroy_all
      acct.owner.zopim_identity.delete if acct.owner.zopim_identity
      acct.zopim_integration.destroy if acct.zopim_integration
      acct.zopim_subscription.destroy if acct.zopim_subscription
    end

    puts "✔  Zopim subscription is removed from Account #{subdomain}"
  end

  namespace :db do
    namespace :coupons do
      desc 'Destroy all sample Zuora coupons'
      task clean: :environment do
        ZBC::Zuora::Coupon.all.each(&:destroy)
      end

      desc 'List Zuora coupons'
      task list: :environment do
        columns = ZBC::Zuora::Coupon.column_names - ['id', 'terms', 'created_at', 'updated_at']
        format  = ("\033[1m%-10s\t\033[0m" * columns.count)
        puts
        puts format % columns.map(&:titleize)
        format = ("%-10s\t" * columns.count)
        ZBC::Zuora::Coupon.all.each do |coupon|
          puts format % coupon.attributes.values_at(*columns)
        end
        puts "\n\033[1m%d records found\033[0m" % ZBC::Zuora::Coupon.count
      end

      desc 'Generate sample Zuora coupons'
      task seed: :environment do
        ZBC::Zuora::Coupon.create(
          name:            'FIXED',
          coupon_code:     'FIXED',
          discount_amount: 50,
          discount_type:   'fixed',
          duration:        1,
          start_date:      10.days.ago,
          end_date:        5.days.from_now
        )

        ZBC::Zuora::Coupon.create(
          name:            'PERCENTAGE',
          coupon_code:     'PERCENTAGE',
          discount_type:   'percentage',
          discount_amount: 50,
          duration:        1,
          start_date:      10.days.ago,
          end_date:        5.days.from_now
        )
      end
    end
  end
end

def subscribe!(opts = {})
  acct = Account.find_by_subdomain('trial')
  if acct.zuora_managed?
    puts('✘  Account is already managed by Zuora (try: rake zuora:reset_trial)')
  else
    payment_method = {
      address1:       '1 Main St.',
      city:           'San Francisco',
      country:        'United States',
      cc_month:       1,
      cc_year:        2050,
      cc_holder_name: 'Charlie Runkle',
      cc_number:      '4012301230123010',
      cc_cvv:         '123',
      cc_zipcode:     94108,
      cc_state:       'CA',
      cc_type:        'Visa',
      type:           'CreditCard'
    }

    bill_to = {
      first_name: 'Charlie',
      last_name:  "Runkle#{`hostname`.gsub(/[^A-Za-z]/, '').strip.upcase}",
      work_email: 'charlie.runkle@zendesk.com',
      country:    'United States',
      state:      'CA',
      city:       'San Francisco',
      address1:   '1 Main St.'
    }

    subscription_options = ZBC::Zuora::SubscriptionOptions.new(acct.id, opts.merge!(
      bill_to:                bill_to,
      payment_method:         payment_method,
      max_agents:             5,
      billing_cycle_type:     1,
      plan_type:              3,
      pricing_model_revision: 5
    ))

    ActiveRecord::Base.on_shard(1) do
      subscribe = ZBC::Zuora::Commands::Subscribe.new(subscription_options)
      subscribe.execute!
    end

    if opts[:promo_code]
      puts '✔  Account converted to a Zuora Customer with coupon PERCENTAGE50; '\
        'open https://trial.zd-dev.com/agent#/admin/subscription'
    elsif opts[:invoice_options]
      puts '✔  Account converted to a Zuora Paid Customer; '\
        'open https://trial.zd-dev.com/agent#/admin/subscription'
    else
      puts '✔  Account converted to a Zuora Customer; '\
        'open https://trial.zd-dev.com/agent#/admin/subscription'
    end
  end
end

def subscribe_to_patagonia!(opts = {})
  subdomain = opts.fetch(:subdomain, 'trial')
  acct = Account.find_by_subdomain(subdomain)
  if acct.zuora_managed?
    puts('✘  Account is already managed by Zuora (try: rake zuora:reset_trial)')
  else
    payment_method = {
      address1:       '1 Main St.',
      city:           'San Francisco',
      country:        'United States',
      cc_month:       1,
      cc_year:        2050,
      cc_holder_name: 'Charlie Runkle',
      cc_number:      '4012301230123010',
      cc_cvv:         '123',
      cc_zipcode:     94108,
      cc_state:       'CA',
      cc_type:        'Visa',
      type:           'CreditCard'
    }

    bill_to = {
      first_name: 'Charlie',
      last_name:  "Runkle#{`hostname`.gsub(/[^A-Za-z]/, '').strip.upcase}",
      work_email: 'charlie.runkle@zendesk.com',
      country:    'United States',
      state:      'CA',
      city:       'San Francisco',
      address1:   '1 Main St.'
    }

    subscription_options = ZBC::Zuora::SubscriptionOptions.new(acct.id, opts.merge!(
      bill_to:                bill_to,
      payment_method:         payment_method,
      max_agents:             5,
      billing_cycle_type:     1,
      plan_type:              3,
      pricing_model_revision: 7
    ))

    ActiveRecord::Base.on_shard(1) do
      subscribe = ZBC::Zuora::Commands::Subscribe.new(subscription_options)
      subscribe.execute!
    end

    if opts[:promo_code]
      puts '✔  Account converted to a Zuora Customer with coupon PERCENTAGE50; '\
        "open https://#{subdomain}.zd-dev.com/agent#/admin/subscription"
    elsif opts[:invoice_options]
      puts '✔  Account converted to a Zuora Paid Customer; '\
        "open https://#{subdomain}.zd-dev.com/agent#/admin/subscription"
    else
      puts '✔  Account converted to a Zuora Customer; '\
        "open https://#{subdomain}.zd-dev.com/agent#/admin/subscription"
    end
  end
end
