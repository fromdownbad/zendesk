if Rails.env.production? || (Rails.env.staging? && !ENV["ALLOW_ALL_DB_TASKS"])
  # Only allow whitelisted db tasks in production to prevent chaos
  # rake -P | grep "rake db:" | sed s/rake\ //g
  white_list = %w[
    db:abort_if_pending_migrations
    db:abort_if_pending_migrations:after
    db:abort_if_pending_migrations:before
    db:abort_if_pending_migrations:change
    db:abort_if_pending_migrations:during
    db:abort_if_pending_migrations:for_project
    db:charset
    db:collation
    db:load_config
    db:schema:dump
    db:migrate:list
    db:shard:create_initial_account
    db:shard:create_canary_accounts
    db:shard:create_canary_account_for_cluster
    db:structure:clean_schema
    db:shard:create
    db:structure:dump
    db:version
  ]
  db_tasks = Rake.application.tasks.map(&:name).grep(/^db:/)
  (db_tasks - white_list).each do |name|
    Rake::Task[name].clear
    task name do
      raise "#{name} Destructive task: don't use it in production! #{"disable via ALLOW_ALL_DB_TASKS=1" if Rails.env.staging?}"
    end
  end
end
