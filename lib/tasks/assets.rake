namespace :assets do
  packages = lambda do
    included_standalone = %w[
      admin.js
      admin.css
      csat.js
      csat.css
      roles.js
      roles.css
      settings/slas.js
      settings/slas.css
    ]
    manifest = Dir["public/assets/.sprockets-manifest-*.json"].sort_by(&File.method(:mtime)).last
    assets = JSON.parse(File.read(manifest))['assets']
    assets.select { |path, _| !path.include?('/') || included_standalone.include?(path) }
  end

  precompile = Rake.application.instance_variable_get('@tasks').delete("assets:precompile") || raise("precompile was not defined!")

  Rake::Task['assets:environment'].clear
  # Initialize a rails environment, but don't run initializers
  # after_initialize needs to be called for Sprockets to set up
  task :environment do
    Rails.application.initialize!(:none)
    require 'action_dispatch/routing/inspector'
    ActiveSupport.run_load_hooks(:after_initialize, Rails.application)
  end

  task :precompile do
    ENV["NO_ASSET_SOURCE_URL"] = "true"
    precompile.invoke
    ENV["NO_ASSET_SOURCE_URL"] = nil
    Rake::Task["assets:compress"].invoke
    Rake::Task["assets:undigested_copy"].invoke
    Rake::Task["assets:zendesk_pci_js_copy"].invoke
  end

  desc "Compress top-level asset files, if this is done via sprockets, it will compress everything -> 5min+"
  task :compress do
    require 'closure-compiler'
    require 'yui/compressor'
    require 'shellwords'

    css_files = []
    js_files  = []

    packages.call.each do |_normal_version, digested_version|
      file = Shellwords.escape("public/assets/#{digested_version}")

      if file.ends_with?(".css")
        css_files << file
      elsif file.ends_with?(".js")
        js_files << file
      end
    end

    css_command = YUI::CssCompressor.new.command
    js_command  = Closure::Compiler.new(compilation_level: 'ADVANCED_OPTIMIZATIONS')

    `#{css_command} -o '.css$:.css' #{css_files.join(' ')}`
    raise "failed compressing css files" unless $?.exitstatus.zero?
    `#{js_command}  -o '.js$:.js'   #{js_files.join(' ')}`
    raise "failed compressing js files" unless $?.exitstatus.zero?
  end

  desc "Make a undigested copy of top-level asset files so they are accessible"
  task :undigested_copy do
    packages.call.each do |normal_version, digested_version|
      `cp public/assets/#{digested_version} public/assets/#{normal_version}`
    end
  end

  # we can't run these assets through the pipeline since they can't be modified
  desc "Copy the zendesk_pci assets into the assets directory so they are accessible"
  task :zendesk_pci_js_copy do
    `cp lib/zendesk_pci/*.js public/assets/`
  end
end
