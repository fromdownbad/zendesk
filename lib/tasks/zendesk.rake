ENV['RAILS_ENV'] ||= "development"

namespace :zendesk do
  desc "Flush the memcached cache"
  task memcached_flush: :environment do
    Rails.cache.clear
    puts "Cache flushed."
  end

  task :precompile_codeowners do
    codeowner_path = File.join(Rails.root, ".github/CODEOWNERS")
    codeowner_map = {}
    File.read(codeowner_path).split("\n").each_with_index do |line, i|
      path_owner = line.split(/\s+(?=@)/, 2)
      if line.match?(/^\s*(?:#.*)?$/)
        next # Comment/empty line
      elsif path_owner.length != 2 || (path_owner[0].empty? && !path_owner[1].empty?)
        log "Parse error line #{i + 1}: \"#{line}\""
        next # Invalid line
      else
        codeowner_map.store(path_owner[0], path_owner[1])
      end
    end
    # Write hash to yaml
    File.open("#{Rails.root}/.github/codeowners.yml", "w") { |f| f.write(codeowner_map.to_yaml) }
  end

  desc "Set up a default configuration for development"
  task setup: ["config/database.yml", "config/environments/development.rb"] do
    puts "Developer setup complete."
  end

  file "config/database.yml" do
    cp "config/database.yml.example", "config/database.yml", verbose: true
  end

  file "config/environments/development.rb" do
    cp "config/environments/development.rb.example", "config/environments/development.rb", verbose: true
  end

  namespace :export do
    desc "Export reports as YAML for given account, eg. rake zendesk:export:reports account=support"
    task reports: :environment do
      export_as_yaml(Report)
    end

    desc "Exports rules as YAML for given account, eg. rake zendesk:export:rules account=support"
    task rules: :environment do
      export_as_yaml(Rule)
    end

    desc "Exports reports and rules for given account, eg. rake zendesk:export:all account=support"
    task all: %i[reports rules] do
    end
  end

  namespace :db do
    desc "Copy database.yml from DATABASE_YML_SRC env"
    task :copy_database_yml do
      if ENV['DATABASE_YML_SRC'].nil?
        puts "Usage: DATABASE_YML_SRC=/path/to/database.yml rake zendesk:db:copy_database_yml"
      else
        if File.file?(ENV['DATABASE_YML_SRC'])
          `cp #{ENV['DATABASE_YML_SRC']} #{Rails.root}/config/database.yml`

        else
          puts "No such file #{DATABASE_YML_SRC}"
        end
      end
    end
  end

  desc "fix whitepace"
  task :fix_white_space do
    convert_tabs_to_spaces = "'s/\t/ /g'"
    strip_traling_white_space = "'s/\s+$//'"

    # ruby files
    sh "find . -name '*.rb'   -not -path './vendor/*' -not -path './db/schema.rb' | xargs perl -pi -e #{convert_tabs_to_spaces}"
    sh "find . -name '*.rb'   -not -path './vendor/*' -not -path './db/schema.rb' | xargs perl -pi -e #{strip_traling_white_space}"
    sh "find . -name '*.rake' -not -path './vendor/*' | xargs perl -pi -e #{convert_tabs_to_spaces}"
    sh "find . -name '*.rake' -not -path './vendor/*' | xargs perl -pi -e #{strip_traling_white_space}"

    # erb files
    sh "find app/views -name '*.erb' -not -path './vendor/*' | xargs perl -pi -e #{convert_tabs_to_spaces}"
    sh "find app/views -name '*.erb' -not -path './vendor/*' | xargs perl -pi -e #{strip_traling_white_space}"

    # css files
    # sh "find public/stylesheets -name '*.scss' | xargs perl -pi -e #{convert_tabs_to_spaces}"
    # sh "find public/stylesheets -name '*.scss' | xargs perl -pi -e #{strip_traling_white_space}"
  end

  namespace :mail do
    desc "process locked mails"
    task unlock: :environment do
      config = YAML.load_file(Rails.root + "config/mail.yml")[Rails.env].with_indifferent_access
      new_dir = Pathname.new(config[:parsing_queue_path] || config[:maildir]) + 'new'
      locked_dir = Pathname.new(config[:parsing_queue_path] || config[:maildir]) + 'locked'
      FileUtils.mv(Dir.glob(locked_dir + '*'), new_dir)
    end
  end

  desc "Generate translations for downtime page"
  task generate_downtime_translations: :environment do
    require 'i18n_data'
    locales = ["en", "de", "fr", "es", "it", "pt-BR", "nl", "da", "ru", "ja", "ko", "zh-TW", "zh-CN"]
    keys = TRANSLATION_FILES.instance.translations.keys.select { |k| k.start_with?("txt.downtime.") }
    translations = Hash[locales.map do |language|
      locale = (language == "en" ? ENGLISH_BY_ZENDESK : TranslationLocale.find_by_locale_and_public!(language, true))
      translations = Hash[keys.map { |k| [k, I18n.t(k, locale: locale).gsub("{{name}}", "%{name}")] }]
      translations["language"] = I18nData.languages(language.sub("-", "_"))[language.upcase.sub(/-.*/, "")]
      [language, translations]
    end]
    puts translations.to_json
  end
end

def export_as_yaml(model)
  if account = Account.find_by_subdomain(ENV['account'])
    filename = "#{Rails.root}/tmp/#{account.subdomain}_#{model.name.downcase}s.yaml"
    records  = model.find(:all, conditions: { account_id: account.id })

    records.each do |record|
      record.author_id  = User.system_user_id if record.author_id?
      record.account_id = nil if record.account_id?
    end

    f = File.open(filename, 'w+')
    f.puts records.to_yaml
    f.flush
    f.close

    puts "#{model.name}s exported as tmp/#{File.basename(filename)}"
  else
    puts "No such account: #{ENV['account']}"
  end
end

def read_pid_file(name)
  if File.exist?(name)
    File.read(name).chomp
  else
    false
  end
end

def pid_running?(pid)
  pid && `ps #{pid} | wc`.to_i > 1
end
