require 'active_record_shards/tasks'

namespace :zendesk do
  namespace :reports do
    desc "Generate an invoiced customers list report (CSV)"
    task invoiced_customers: :environment do
      Zendesk::Accounts::InvoicedCustomerReport.generate
    end
  end
end
