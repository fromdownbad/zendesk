namespace :geo do
  desc "Updates the GeoLiteCity database"
  task update_data: :environment do
    Zendesk::GeoLocation.update_data!
  end
end
