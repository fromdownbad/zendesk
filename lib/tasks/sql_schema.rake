tasks = %w[
  db:migrate
  db:migrate:up
  db:migrate:down
  db:migrate:redo
  db:rollback
]

desc "Disable ARSI protection"
task :disable_arsi do
  Arsi.disable!
end

tasks.each do |arsi_task|
  Rake::Task[arsi_task].prerequisites << 'disable_arsi'
end
