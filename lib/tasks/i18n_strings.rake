namespace :i18n do
  namespace :strings do
    desc "Export English by Zendesk strings into a zip file"
    EXPORT_ENGLISH_URL = "https://amartinez.zendesk.com/api/v2/locales/export_english_in_db.json".freeze

    task english_zip: :environment do
      Zip::ZipFile.open("english_#{Date.today.strftime("%m%d")}.zip", Zip::ZipFile::CREATE) do |zip_file|
        Dir[Rails.root + 'config/locales/translations/*.yml'].each do |file|
          file_name = file[/([a-zA-Z_]+.yml)/]
          zip_file.add(file_name, file)
        end
        json_strings = translations_in_database
        zip_file.add("english_in_the_database.yml", generate_yml_file(json_strings)) if json_strings
      end
    end

    def translations_in_database
      response = process_api_call
      return false unless response.status == 200
      json_strings = ::JSON.parse(response.body)
      return false if json_strings["english_in_db"].blank?
      json_strings
    end

    def process_api_call
      conn = Faraday.new(url: EXPORT_ENGLISH_URL, ssl: {verify: false})
      conn.options[:timeout] = 5
      conn.get
    end

    def generate_yml_file(json)
      file = Tempfile.new("english_in_the_database.yml")
      file.write(yml_strings(json))
      file.close
      file.path
    end

    def yml_strings(json)
      translations = "title: \"English in the database\"\n\nparts:\n"
      json["english_in_db"].each { |json_translation| translations << yml_translation(json_translation) }
      translations
    end

    def yml_translation(json_translation)
      translation = "  - translation:\n"
      translation << "      key: #{json_translation[0]}\n"
      translation << "      title: #{json_translation[1]['title']}\n"
      translation << "      value: #{json_translation[1]['value']}\n"
      translation
    end
  end
end
