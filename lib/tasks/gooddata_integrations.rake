require 'ruby-progressbar'
namespace :gooddata_integrations do
  desc "Run a configure job for all active v2 integrations"
  task configure_all: :environment do
    accounts = Account.
      active.
      serviceable.
      shard_unlocked

    bar = ProgressBar.new(title: "Queueing jobs", total: accounts.count)

    accounts.find_each do |account|
      bar.increment
      next unless account.pod_local?

      account.on_shard do
        integration = account.gooddata_integration
        next unless integration

        GooddataConfigurationJob.enqueue(account.id)
      end
    end

    bar.finish
  end

  desc "Destroys gooddata integrations on accounts that shouldn't have them, (unboosted starter/regular plans)"
  task remove_extraneous: :environment do
    def extraneous_gooddata_integration?(account)
      account_is_alive = account.is_serviceable? && account.is_active?
      if account_is_alive
        account.subscription.plan_type < SubscriptionPlanType.Large && !account.boosted?
      else
        true
      end
    end

    shards = ActiveRecord::Base.shard_names

    bar = ProgressBar.new("Scanning shards", shards.count)
    open("#{Dir.home}/deleted_integrations.log", "w") do |deleted_account_list|
      shards.each do |shard|
        ActiveRecord::Base.on_shard(shard) do
          account_ids_with_integrations = GooddataIntegration.where(deleted_at: nil).map(&:account_id)
          accounts = Account.
            pre_load(:subscription, :feature_boost).
            shard_unlocked.
            where(id: account_ids_with_integrations).
            select { |account| extraneous_gooddata_integration?(account) }

          accounts.each do |account|
            deleted_account_list.write("#{account.subdomain} (id #{account.id}, shard #{shard}) - project id: #{account.gooddata_integration.project_id}\n")
            Zendesk::Gooddata::IntegrationProvisioning.destroy_for_account(account)
          end

          bar.inc
        end
      end

      bar.finish
    end
  end
end
