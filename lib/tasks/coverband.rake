require_relative '../zendesk/coverband_helpers'
Zendesk::CoverbandHelpers.initializer(false)
require 'coverband/tasks'

namespace :zendesk do
  task :coverband do
    puts Zendesk::CoverbandHelpers.yaml_report
  end
end
