namespace :gooddata_users do
  desc "Sync all users for all accounts"
  task sync_all: :environment do
    Account.
      active.
      serviceable.
      where(lock_state: Zendesk::Accounts::Locking::LockState::OPEN).
      find_each do |account|
      next unless account.pod_local?

      ActiveRecord::Base.on_shard(account.shard_id) do
        integration = account.gooddata_integration
        next unless integration

        GooddataUser.users_for_integration(integration).each do |gd_user|
          Zendesk::Gooddata::UserProvisioning.sync_for_user(gd_user.user)
          sleep(0.1)
          $stdout.putc '.'
        end
      end
    end
  end
end
