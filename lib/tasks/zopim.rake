namespace :zopim do
  desc "Reset Zopim record link from the TRIAL account."
  task reset_trial: :environment do
    require 'zopim_reseller_api'
    require 'zopim/reseller'
    Rails.logger.subscribe(Rails.logger.collator) unless Rails.logger.respond_to? :append_attributes
    reset_trial
  end

  private

  def reset_trial
    return unless Rails.env.development?
    return unless zopim_subscription.present?
    zendesk_account.on_shard { zopim_subscription.reset! }
    puts 'OK'
  end

  def zendesk_account
    @zendesk_account ||= Account.find_by_subdomain('trial')
  end

  def zopim_subscription
    @zopim_subscription ||= zendesk_account.zopim_subscription ||
      ZendeskBillingCore::Zopim::Subscription.unserviceable_subscription
  end
end
