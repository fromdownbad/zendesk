desc "Restart the app with debugging enabled, then launch the debugger"
task :debug do
  require 'byebug/core'

  # This instructs the app to wait for the debugger to connect after loading
  FileUtils.touch(File.join(Rails.root, 'tmp', 'debug.txt'))

  # Instruct Phusion Passenger to restart the app
  FileUtils.touch(File.join(Rails.root, 'tmp', 'restart.txt'))

  # Wait for it to restart (requires the user to load a page)
  puts "Waiting for restart (please reload the app in your web browser)..."
  begin
    sleep 0.5 while File.exist?(File.join(Rails.root, 'tmp', 'debug.txt'))
    sleep 1
  rescue Interrupt
    File.delete(File.join(Rails.root, 'tmp', 'debug.txt'))
    puts "\rCancelled."
    exit 1
  end

  puts "Loading byebug..."

  begin
    Byebug.start_client
  rescue Interrupt
    # Clear the "^C" that is displayed when you press Ctrl-C
    puts "\r\e[0KDisconnected."
  end
end

task fix_schema: :environment do
  ActiveRecord::Base.on_all_shards do
    Token.connection.execute("ALTER TABLE tokens CHANGE token_crc token_crc INT(11) UNSIGNED")
    Ticket.connection.execute("ALTER TABLE tickets MODIFY generated_timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
  end
end

task :fixtures_all do
  ENV['FIXTURES_ALL'] = "YES!"
end

namespace :test do
  root = File.expand_path("../../..", __FILE__)

  desc "Generates the file_cache files from base_i18n_cache"
  task :generate_fixtures do
    require 'yaml'
    FileUtils.mkdir_p 'test/files/generated_i18n_cache'
    locales_path = Dir.glob('test/files/base_i18n_cache/*').select { |f| File.directory? f }
    locales_path.each do |locale_path|
      locale = /([-\w]+)$/.match(locale_path)
      generated_directory = "test/files/generated_i18n_cache/#{locale}"
      FileUtils.mkdir_p(generated_directory)
      Dir.glob("#{locale_path}/*") do |file|
        file_name = /([-.\w]+)$/.match(file)[1].split(".")[0]
        if file_name == "version"
          FileUtils.cp(file, "#{generated_directory}/version")
        else
          data = YAML.load_file(file)
          File.open("#{generated_directory}/#{file_name}.marshal", 'w') { |file| Marshal.dump(data, file) }
        end
      end
    end
  end

  Rake::TestTask.new(:javascript) do |t|
    t.libs << "test"
    t.verbose = true
    t.pattern = "test/javascript/**/*_test.rb"
  end
  Rake::Task["test:javascript"].comment = "Run javascript tests"

  task :javascript do
    begin
      Rake::Task["test:javascript"].invoke
    rescue
      abort "Errors running #{task}!"
    end
  end

  task units: :fix_schema
  task functionals: [:fix_schema, :fixtures_all]
  task jenkins_insulated: :fixtures_all
  task integration: :fix_schema

  JAVASCRIPT_ASSET_FILES = [
    'bootstrap',
    'auto_included',
    'color_bundle',
    'swfobject',
    'rule',
    'rule_data',
    'zd_ticket_custom_fields'
  ].map do |asset|
    "public/assets/#{asset}.js"
  end

  desc 'Run QUnit tests in-browser'
  task qunit: JAVASCRIPT_ASSET_FILES do
    puts "\nAfter the server starts, open http://localhost:9292/test/javascript/all.html\n"
    `rackup test/javascript/config.ru`
  end

  task :forked_test_worker do
    exec root + "/test/forked_test_worker"
  end
end

namespace :db do
  task seed: :fix_schema
end
