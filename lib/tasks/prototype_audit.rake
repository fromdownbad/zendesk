class PrototypeAudit
  ROOT = File.expand_path('../../../', __FILE__)

  PROTOTYPE_ERROR_PATTERNS = [
    [/\$\$\(/, 'Prototype CSS selector query. Use $j()'],
    [/$A\(/, 'Prototype Array creation. Use $j.makeArray()'],
    [/$F\(/, 'Prototype form element value retrieval. Use $j("#id").val()'],
    [/$H\(/, 'Prototype Hash creation. Use a Javascript literal'],
    [/$R\(/, 'Prototype Range creation. Use a Javascript literal'],
    [/$w\(/, 'Prototype String Array creation. Use a Javascript literal'],
    [/Prototype\.Browser/, 'Prototype user-agent detection. Use jQuery.browser'],
    [/Element\.addMethods/, 'Prototype element functionality. Use jQuery.fn.extend()'],
    [/Try\.these/, 'Prototype Try.these block. '],
    [/Event\.stop/, 'Prototype event behavior manipulation. Use event.preventDefault()'],
    [/new Element/, 'Prototype DOM element creation. Use $j()'],
    [/new Ajax/, 'Prototype AJAX call. Use $j.ajax()'],
    [/new Effect/, 'Scriptaculous effect.'],
    [/Sortable\.serialize/, 'Scriptaculous data serialization.'],
    [/Class\.create/, 'Prototype class creation. Use function declaration with an assigned prototype'],
    [/\.update/, 'Prototype content change. Use .html()'],
    [/\.insert/, 'Prototype DOM element insertion. Use $j().before(), .after(), .append(), or .prepend() '],
    [/\.addClassName/, 'Prototype class addition. Use .addClass()'],
    [/\.removeClassName/, 'Prototype class removal. Use .removeClass()'],
    [/\.toggleClassName/, 'Prototype class toggle. Use .toggleClass()'],
    [/\.observe/, 'Prototype event handler. Use .bind()'],
    [/\.select/, 'Prototype descendant selector. Use .find)'],
    [/\.firstDescendant/, 'Prototype descendant selector. Use .children().first()'],
    [/\.descendantOf/, 'Prototype ancestor selector. Use .parents(selector)'],
    [/\.childElements/, 'Prototype children selector. Use .children()'],
    [/\.writeAttribute/, 'Prototype attribute write. Use .attr(key, value)'],
    [/\$\(.+\)\.\w+\s*=\s*.+/, 'Prototype attribute write. Use .attr(key, value)'],
    [/\.retrieve\(/, 'Prorotype data retrieval. Use .attr("data-*")'],
    [/\.store\(/, 'Prorotype data storage. Use .attr("data-*", value)']
  ].freeze

  PROTOTYPE_WARNING_PATTERNS = [
    [/\$\(/, 'Warning: probable Prototype ID selector. Use $j("#id")'],
    [/\.each\(\s*function\([^,]*\)/, "Warning: Prototype's each takes the element as the sole argument; jQuery's each takes an index and the element."]
  ].freeze

  def self.run
    Dir.glob(ROOT + '/{app,lib,public}/**/*.js').each do |js_file|
      file = File.new(js_file)
      messages = []
      errors = false

      file.each do |line|
        PROTOTYPE_ERROR_PATTERNS.each do |(pattern, message)|
          if pattern.match?(line)
            errors = true
            messages << "#{'%6d' % file.lineno} Error: #{message}"
          end
        end
        PROTOTYPE_WARNING_PATTERNS.each do |(pattern, message)|
          if pattern.match?(line)
            messages << "#{'%6d' % file.lineno} Warning: #{message}"
          end
        end
      end

      puts "#{js_file.sub(ROOT, '')}: #{errors ? 'FAIL' : 'PASS'}"
      unless ENV['QUIET'] == 'true'
        messages.each { |m| puts(m) }
      end
    end
  end
end

namespace :javascript do
  desc 'Run an audit of Javascript files and print out any use of Prototype'
  task :prototype_audit do
    PrototypeAudit.run
  end
end
