base_rubocop = "rubocop --display-cop-names --display-style-guide --extra-details --parallel"

desc "Run rubocop"
task :rubocop do
  sh base_rubocop
end

namespace :rubocop do
  desc "run rubocop on the files changed compared to master"
  task :changed do
    rubocop_list = `rubocop -a --list-target-files`.split("\n")
    raise "RuboCop failed at listing target files" unless $?.success?
    git_list = `git diff --name-only $(git merge-base --fork-point master)`.split("\n")
    raise "Git failed at listing changed files" unless $?.success?

    modified = rubocop_list & git_list

    if modified.any?
      sh "#{base_rubocop} #{modified.join(" ")}"
    else
      puts "Nothing changed: Try running `bundle exec rubocop` for full check!"
    end
  end
end
