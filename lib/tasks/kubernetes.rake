require 'erb'
require 'zendesk/kubernetes_file_generation'

desc 'Generate the Kubernetes YAML files'
task :k8s do
  Zendesk::KubernetesFileGeneration.run
end
