module Datadog
  class AnswerBot
    NAMESPACE = 'answer_bot.deflection'.freeze

    def initialize(via_id:, subdomain:, deflection_channel_id:, resolution_channel_id:)
      @via_id = via_id
      @subdomain = subdomain
      @deflection_channel_id = deflection_channel_id
      @resolution_channel_id = resolution_channel_id
    end

    def solve_initiated(extra_tags)
      increment('solve_initiated', extra_tags)
    end

    def solved(extra_tags)
      increment('solved', extra_tags)
    end

    def solved_too_quick(extra_tags)
      increment('solved_too_quick', extra_tags)
    end

    def ticket_creation_failed(extra_tags = {})
      increment('ticket_creation_failed', extra_tags)
    end

    def solved_undone(extra_tags)
      increment('solved_undone', extra_tags)
    end

    def irrelevant(extra_tags)
      increment('irrelevant', extra_tags)
    end

    def deflection_solve_failed(extra_tags)
      increment('deflection_solve_failed', extra_tags)
    end

    private

    attr_reader :via_id, :subdomain, :deflection_channel_id, :resolution_channel_id

    def increment(metric, extra_tags)
      statsd_client.increment(metric, tags: tags(extra_tags))
    end

    def tags(extra_tags = {})
      {
        via_id: via_id,
        subdomain: subdomain,
        deflection_channel_id: deflection_channel_id,
        resolution_channel_id: resolution_channel_id
      }.
        merge(extra_tags).
        map { |key, value| "#{key}:#{value}" }
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: [NAMESPACE])
    end
  end
end
