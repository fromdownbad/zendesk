module AttachmentsHelper
  def add_attachments(uploaded_attachments, _params = {})
    (uploaded_attachments || []).each do |attachment|
      next if attachment.blank?

      if attachment.respond_to?(:content_type) && attachment.content_type.blank?
        attachment.content_type = 'application/octet-stream'
      end

      built = attachments.build(uploaded_data: attachment)
      built.is_public = respond_to?(:is_public) && is_public
      built.account = account
      built.author = [:author, :submitter].each { |user| break send(user) if respond_to?(user) }
    end
  end
end
