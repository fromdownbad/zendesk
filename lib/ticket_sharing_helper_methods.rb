# This includes a method that needs to be shared between TicketsHelper
# and TargetsHelper. In the test environment we can't share this method
# between helpers without this module.

module TicketSharingHelperMethods
  def eligible_agreements_for_sharing
    @eligible_agreements_for_sharing ||=
      current_account.sharing_agreements.outbound.accepted
  end
end
