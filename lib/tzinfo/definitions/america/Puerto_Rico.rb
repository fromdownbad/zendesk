module TZInfo # rubocop:disable Naming/FileName
  module Definitions
    module America
      # Copied from https://github.com/ben-skelton/tzinfo/blob/master/lib/tzinfo/definitions/America/Puerto_Rico.rb
      # to add Puerto Rico (AST, but without DST) support.
      module Puerto_Rico # rubocop:disable Naming/ClassAndModuleCamelCase
        include TimezoneDefinition

        timezone 'America/Puerto_Rico' do |tz|
          tz.offset :o0, -15865, 0, :LMT
          tz.offset :o1, -14400, 0, :AST
          tz.offset :o2, -14400, 3600, :AWT
          tz.offset :o3, -14400, 3600, :APT

          tz.transition 1899, 3, :o1, 41726744933, 17280
          tz.transition 1942, 5, :o2, 7291448, 3
          tz.transition 1945, 8, :o3, 58360379, 24
          tz.transition 1945, 9, :o1, 58361489, 24
        end
      end
    end
  end
end
