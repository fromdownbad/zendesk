class IdSetsDifference
  attr_reader :current, :update

  def initialize(current, update)
    self.current = normalize_ids(current)
    self.update  = normalize_ids(update)
  end

  def added
    update - current
  end

  def removed
    current - update
  end

  def result
    current.size + added.size - removed.size
  end

  private

  def normalize_ids(ids)
    ids.reject(&:blank?).map do |item|
      item.respond_to?(:column_for_attribute) ? item.id : item.to_i
    end
  end

  attr_writer :current, :update
end
