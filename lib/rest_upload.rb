require 'stringio'

class RestUpload < StringIO
  attr_reader :original_filename, :content_type

  def initialize(string, filename, content_type = nil)
    super(string.to_s)
    @original_filename = filename
    @content_type = Zendesk::Utils::MimeTypes.determine_mime_type(self, content_type)
  end

  def blank?
    size == 0
  end
end
