The purpose of this lib folder is to unblock the Rails 5 work by bringing the contents of this gem internal to Zendesk so it 
can be removed in a piecemeal way.  The original gem, including the internal documentation via extensive commenting can be found
here: https://github.com/pschambacher/prototype-rails

A full removal plan can be found in this ticket: https://zendesk.atlassian.net/browse/R5U-679

Prototype-rails is explicitly not supported for Rails 5.  This does not mean that the code that has been brought in to this lib
folder is not be compatible, per se, but there is a good chance there is an issue.  Therefore, it would be ideal to remove the lib
and any uses of prototype-rails prior to moving to Rails 5.
