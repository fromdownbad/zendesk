require 'set'
require 'active_support/json'
require 'active_support/core_ext/object/blank'
require 'active_support/core_ext/string/output_safety'
require_relative 'generator_methods'

module ActionView
  # A string that returns itself as its JSON-encoded form.
  class JsonLiteral < String
    def as_json(options = nil) self end #:nodoc:
    def encode_json(encoder) self end #:nodoc:
  end

  # = Action View Prototype Helpers
  module Helpers
    module PrototypeHelper
      CALLBACKS    = Set.new([ :create, :uninitialized, :loading, :loaded,
                       :interactive, :complete, :failure, :success ] +
                       (100..599).to_a)
      AJAX_OPTIONS = Set.new([ :before, :after, :condition, :url,
                       :asynchronous, :method, :insertion, :position,
                       :form, :with, :update, :script, :type ]).merge(CALLBACKS)

      def remote_function(options)
        javascript_options = options_for_ajax(options)

        update = ''
        if options[:update] && options[:update].is_a?(Hash)
          update  = []
          update << "success:'#{options[:update][:success]}'" if options[:update][:success]
          update << "failure:'#{options[:update][:failure]}'" if options[:update][:failure]
          update  = '{' + update.join(',') + '}'
        elsif options[:update]
          update << "'#{options[:update]}'"
        end

        function = update.empty? ?
          "new Ajax.Request(" :
          "new Ajax.Updater(#{update}, "

        url_options = options[:url]
        function << "'#{ERB::Util.html_escape(escape_javascript(url_for(url_options)))}'"
        function << ", #{javascript_options})"

        function = "#{options[:before]}; #{function}" if options[:before]
        function = "#{function}; #{options[:after]}"  if options[:after]
        function = "if (#{options[:condition]}) { #{function}; }" if options[:condition]
        function = "if (confirm('#{escape_javascript(options[:confirm])}')) { #{function}; }" if options[:confirm]

        return function.html_safe
      end

      class JavaScriptGenerator #:nodoc:
        def initialize(context, &block) #:nodoc:
          @context, @lines = context, []
          def @lines.encoding() last.to_s.encoding end
          include_helpers_from_context
          @context.with_output_buffer(@lines) do
            @context.instance_exec(self, &block)
          end
        end

        private
          def include_helpers_from_context
            extend @context.controller._helpers if @context.controller.respond_to?(:_helpers) && @context.controller._helpers
            extend GeneratorMethods
          end
      end

      def update_page(&block)
        JavaScriptGenerator.new(self, &block).to_s.html_safe
      end

      def update_page_tag(html_options = {}, &block)
        javascript_tag update_page(&block), html_options
      end

      protected
        def options_for_javascript(options)
          if options.empty?
            '{}'
          else
            "{#{options.keys.map { |k| "#{k}:#{options[k]}" }.sort.join(', ')}}"
          end
        end

        def options_for_ajax(options)
          js_options = build_callbacks(options)

          js_options['asynchronous'] = options[:type] != :synchronous
          js_options['method']       = method_option_to_s(options[:method]) if options[:method]
          js_options['insertion']    = "'#{options[:position].to_s.downcase}'" if options[:position]
          js_options['evalScripts']  = options[:script].nil? || options[:script]

          if options[:form]
            js_options['parameters'] = 'Form.serialize(this)'
          elsif options[:submit]
            js_options['parameters'] = "Form.serialize('#{options[:submit]}')"
          elsif options[:with]
            js_options['parameters'] = options[:with]
          end

          if protect_against_forgery? && !options[:form]
            if js_options['parameters']
              js_options['parameters'] << " + '&"
            else
              js_options['parameters'] = "'"
            end
            js_options['parameters'] << "#{request_forgery_protection_token}=' + encodeURIComponent('#{escape_javascript form_authenticity_token}')"
          end

          options_for_javascript(js_options)
        end

        def method_option_to_s(method)
          (method.is_a?(String) and !method.index("'").nil?) ? method : "'#{method}'"
        end

        def build_callbacks(options)
          callbacks = {}
          options.each do |callback, code|
            if CALLBACKS.include?(callback)
              name = 'on' + callback.to_s.capitalize
              callbacks[name] = "function(request){#{code}}"
            end
          end
          callbacks
        end
    end

    class JavaScriptProxy < ActiveSupport::ProxyObject #:nodoc:
      def initialize(generator, root = nil)
        @generator = generator
        @generator << root if root
      end

      def is_a?(klass)
        klass == JavaScriptProxy
      end

      private
        def method_missing(method, *arguments, &block)
          if method.to_s =~ /(.*)=$/
            assign($1, arguments.first)
          else
            call("#{method.to_s.camelize(:lower)}", *arguments, &block)
          end
        end

        def call(function, *arguments, &block)
          append_to_function_chain!("#{function}(#{@generator.send(:arguments_for_call, arguments, block)})")
          self
        end

        def assign(variable, value)
          append_to_function_chain!("#{variable} = #{@generator.send(:javascript_object_for, value)}")
        end

        def function_chain
          @function_chain ||= @generator.instance_variable_get(:@lines)
        end

        def append_to_function_chain!(call)
          function_chain[-1].chomp!(';')
          function_chain[-1] += ".#{call};"
        end
    end

    class JavaScriptElementProxy < JavaScriptProxy #:nodoc:
      def initialize(generator, id)
        @id = id
        super(generator, "$(#{::ActiveSupport::JSON.encode(id)})")
      end

      def [](attribute)
        append_to_function_chain!(attribute)
        self
      end

      def []=(variable, value)
        assign(variable, value)
      end

      def replace_html(*options_for_render)
        call 'update', @generator.send(:render, *options_for_render)
      end

      def replace(*options_for_render)
        call 'replace', @generator.send(:render, *options_for_render)
      end

      def reload(options_for_replace = {})
        replace(options_for_replace.merge({ :partial => @id.to_s }))
      end

    end

    class JavaScriptVariableProxy < JavaScriptProxy #:nodoc:
      def initialize(generator, variable)
        @variable = JsonLiteral.new(variable)
        @empty    = true # only record lines if we have to.  gets rid of unnecessary linebreaks
        super(generator)
      end

      def respond_to?(*)
        true
      end

      def as_json(options = nil)
        @variable
      end

      private
        def append_to_function_chain!(call)
          @generator << @variable if @empty
          @empty = false
          super
        end
    end

    class JavaScriptCollectionProxy < JavaScriptProxy #:nodoc:
      ENUMERABLE_METHODS_WITH_RETURN = [:all, :any, :collect, :map, :detect, :find, :find_all, :select, :max, :min, :partition, :reject, :sort_by, :in_groups_of, :each_slice]
      ENUMERABLE_METHODS = ENUMERABLE_METHODS_WITH_RETURN + [:each]
      attr_reader :generator
      delegate :arguments_for_call, :to => :generator

      def initialize(generator, pattern)
        super(generator, @pattern = pattern)
      end

      def each_slice(variable, number, &block)
        if block
          enumerate :eachSlice, :variable => variable, :method_args => [number], :yield_args => %w(value index), :return => true, &block
        else
          add_variable_assignment!(variable)
          append_enumerable_function!("eachSlice(#{::ActiveSupport::JSON.encode(number)});")
        end
      end

      def grep(variable, pattern, &block)
        enumerate :grep, :variable => variable, :return => true, :method_args => [JsonLiteral.new(pattern.inspect)], :yield_args => %w(value index), &block
      end

      def in_groups_of(variable, number, fill_with = nil)
        arguments = [number]
        arguments << fill_with unless fill_with.nil?
        add_variable_assignment!(variable)
        append_enumerable_function!("inGroupsOf(#{arguments_for_call arguments});")
      end

      def inject(variable, memo, &block)
        enumerate :inject, :variable => variable, :method_args => [memo], :yield_args => %w(memo value index), :return => true, &block
      end

      def pluck(variable, property)
        add_variable_assignment!(variable)
        append_enumerable_function!("pluck(#{::ActiveSupport::JSON.encode(property)});")
      end

      def zip(variable, *arguments, &block)
        add_variable_assignment!(variable)
        append_enumerable_function!("zip(#{arguments_for_call arguments}")
        if block
          function_chain[-1] += ", function(array) {"
          yield JsonLiteral.new('array')
          add_return_statement!
          @generator << '});'
        else
          function_chain[-1] += ');'
        end
      end

      private
        def method_missing(method, *arguments, &block)
          if ENUMERABLE_METHODS.include?(method)
            returnable = ENUMERABLE_METHODS_WITH_RETURN.include?(method)
            variable   = arguments.first if returnable
            enumerate(method, {:variable => variable, :return => returnable, :yield_args => %w(value index)}, &block)
          else
            super
          end
        end

        def enumerate(enumerable, options = {}, &block)
          options[:method_args] ||= []
          options[:yield_args]  ||= []
          yield_args  = options[:yield_args] * ', '
          method_args = arguments_for_call options[:method_args] # foo, bar, function
          method_args << ', ' unless method_args.blank?
          add_variable_assignment!(options[:variable]) if options[:variable]
          append_enumerable_function!("#{enumerable.to_s.camelize(:lower)}(#{method_args}function(#{yield_args}) {")
          # only yield as many params as were passed in the block
          yield(*options[:yield_args].collect { |p| JavaScriptVariableProxy.new(@generator, p) }[0..block.arity-1])
          add_return_statement! if options[:return]
          @generator << '});'
        end

        def add_variable_assignment!(variable)
          function_chain.push("var #{variable} = #{function_chain.pop}")
        end

        def add_return_statement!
          unless function_chain.last =~ /return/
            function_chain.push("return #{function_chain.pop.chomp(';')};")
          end
        end

        def append_enumerable_function!(call)
          function_chain[-1].chomp!(';')
          function_chain[-1] += ".#{call}"
        end
    end

    class JavaScriptElementCollectionProxy < JavaScriptCollectionProxy #:nodoc:\
      def initialize(generator, pattern)
        super(generator, "$$(#{::ActiveSupport::JSON.encode(pattern)})")
      end
    end
  end
end
