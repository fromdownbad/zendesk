module ActionView
  module Helpers
    module PrototypeHelper
      class JavaScriptGenerator
        module GeneratorMethods
          def to_s #:nodoc:
            (@lines * $/).tap do |javascript|
              if ActionView::Base.debug_rjs
                source = javascript.dup
                javascript.replace "try {\n#{source}\n} catch (e) "
                javascript << "{ alert('RJS error:\\n\\n' + e.toString()); alert('#{source.gsub('\\','\0\0').gsub(/\r\n|\n|\r/, "\\n").gsub(/["']/) { |m| "\\#{m}" }}'); throw e }"
              end
            end
          end

          def [](id)
            case id
              when String, Symbol, NilClass
                JavaScriptElementProxy.new(self, id)
              else
                JavaScriptElementProxy.new(self, RecordIdentifier.dom_id(id))
            end
          end

          RecordIdentifier =
            if defined? ActionView::RecordIdentifier
              ActionView::RecordIdentifier
            else
              ActionController::RecordIdentifier
            end

          def literal(code)
            JsonLiteral.new(code.to_s)
          end

          def select(pattern)
            JavaScriptElementCollectionProxy.new(self, pattern)
          end

          def insert_html(position, id, *options_for_render)
            content = javascript_object_for(render(*options_for_render))
            record "Element.insert(\"#{id}\", { #{position.to_s.downcase}: #{content} });"
          end

          def replace_html(id, *options_for_render)
            call 'Element.update', id, render(*options_for_render)
          end

          def replace(id, *options_for_render)
            call 'Element.replace', id, render(*options_for_render)
          end

          def remove(*ids)
            loop_on_multiple_args 'Element.remove', ids
          end

          def show(*ids)
            loop_on_multiple_args 'Element.show', ids
          end

          def hide(*ids)
            loop_on_multiple_args 'Element.hide', ids
          end

          def toggle(*ids)
            loop_on_multiple_args 'Element.toggle', ids
          end

          def alert(message)
            call 'alert', message
          end

          def redirect_to(location)
            url = location.is_a?(String) ? location : @context.url_for(location)
            record "window.location.href = #{url.inspect}"
          end

          def reload
            record 'window.location.reload()'
          end

          def call(function, *arguments, &block)
            record "#{function}(#{arguments_for_call(arguments, block)})"
          end

          def assign(variable, value)
            record "#{variable} = #{javascript_object_for(value)}"
          end

          def <<(javascript)
            @lines << javascript
          end

          def delay(seconds = 1)
            record "setTimeout(function() {\n\n"
            yield
            record "}, #{(seconds * 1000).to_i})"
          end

          def javascript_object_for(object)
            ::ActiveSupport::JSON.encode(object)
          end

          def arguments_for_call(arguments, block = nil)
            arguments << block_to_function(block) if block
            arguments.map { |argument| javascript_object_for(argument) }.join ', '
          end

          private
          def loop_on_multiple_args(method, ids)
            record(ids.size>1 ?
              "#{javascript_object_for(ids)}.each(#{method})" :
              "#{method}(#{javascript_object_for(ids.first)})")
          end

          def page
            self
          end

          def record(line)
            line = "#{line.to_s.chomp.gsub(/\;\z/, '')};"
            self << line
            line
          end

          def render(*options)
            with_formats(:html) do
              case option = options.first
              when Hash
                @context.render(*options)
              else
                option.to_s
              end
            end
          end

          def with_formats(*args)
            return yield unless @context

            lookup = @context.lookup_context
            begin
              old_formats, lookup.formats = lookup.formats, args
              yield
            ensure
              lookup.formats = old_formats
            end
          end

          def block_to_function(block)
            generator = self.class.new(@context, &block)
            literal("function() { #{generator.to_s} }")
          end

          def method_missing(method, *arguments)
            JavaScriptProxy.new(self, method.to_s.camelize)
          end
        end
      end
    end
  end
end