module Fraud
  module Normalizer
    module FraudEmailNormalizer
      def normalize_email_address(email)
        local_part, domain = local_part_and_domain(email)
        local_part = local_part.tr('.', '').split('+')[0]
        local_part + '@' + domain
      end

      def local_part_and_domain(email)
        s = email.split("@")
        [s[0], s[1].to_s]
      end
    end
  end
end
