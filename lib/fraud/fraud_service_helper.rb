module Fraud
  module FraudServiceHelper
    ACCOUNT_FRAUD_SERVICE_CREATE_Z1_TICKET_URL = '/api/services/private/create_z1_ticket'.freeze
    FRAUD_SERVICE_ACTIVITY_URL = '/api/services/private/activity'.freeze

    private

    # Send account/ticket actvity to fraud service for spam analysis
    def send_to_fraud_service(payload = account_payload, no_retry = false)
      return {} unless send_to_fraud_service?
      fraud_service_response = Fraud::FraudServiceClient.post_activity(payload, FRAUD_SERVICE_ACTIVITY_URL, no_retry)
      if fraud_service_response.nil?
        log_no_response(payload[:source_event] || "unknown")
        return {}
      end
      unless fraud_service_response.try(:body)
        log_empty_response(fraud_service_response, payload[:source_event] || "unknown")
        return {}
      end
      fraud_service_response.body
    end

    def create_z1_ticket_with_fraud_service(z1_ticket_payload)
      Fraud::FraudServiceClient.post_activity(z1_ticket_payload, ACCOUNT_FRAUD_SERVICE_CREATE_Z1_TICKET_URL)
    end

    def send_to_fraud_service?
      below_fraud_service_throttle_limit?
    end

    def below_fraud_service_throttle_limit?
      Prop.throttle(:fraud_service_requests, @current_account.id)
      if Prop.throttled?(:fraud_service_requests, @current_account.id)
        fs_statsd_client.increment('fraud_service.request.throttled')
        Rails.logger.info("Throttle limit reached for account #{@current_account.id}, request not sent to Fraud Service")
        return false
      end
      true
    end

    def account_payload
      {
        source_event: @source_event,
        data:
          {
            account_id:   @account_id,
            subdomain:    @subdomain,
            owner_email:  @owner_email,
            time_zone:    @time_zone,
            account_name: @account_name,
            owner_name:   @owner_name,
            raw_response: @raw_response,
            source_event: @source_event,
            created_at:   @current_account.created_at,
            zd_pod:       ENV['ZENDESK_POD'],
            is_trial:     @current_account.account_is_trial?,
            created_from_ip_address: created_from_ip_address,
            account_update: update_event?,
            support_risky_status: @current_account.support_risky?,
            trial_extras: @current_account.trial_extras.to_hash,
            minfraud_id:  fetch_minfraud_id
          },
        timestamp: Time.now,
        vendor_review_prohibited: @current_account.vendor_review_prohibited?
      }
    end

    def fetch_minfraud_id
      @current_account.first_fraud_score&.minfraud_id || ""
    end

    def update_event?
      Fraud::SourceEvent::ACCOUNT_UPDATE_EVENTS.include? @source_event.to_sym
    end

    def created_from_ip_address
      @current_account.settings.created_from_ip_address.presence || @current_account.trial_extras.to_hash.fetch('client_ip', '')
    end

    def fs_statsd_client
      @fs_statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'fraud_service_client')
    end

    def log_empty_response(response, source_event)
      Rails.logger.info "Empty Fraud Service response #{response} for account_id: #{@current_account.id} and source_event: #{source_event}"
      fs_statsd_client.increment("fraud_service.empty_response", tags: [source_event])
    end

    def log_no_response(source_event)
      Rails.logger.info "No response from Fraud Service for account_id: #{@current_account.id} and source_event: #{source_event}"
      fs_statsd_client.increment("fraud_service.no_response", tags: [source_event])
    end
  end
end
