module Fraud
  class FraudServiceClient
    def self.post_activity(payload, route, no_retry = false)
      fraud_service_client(no_retry).post do |req|
        req.url route
        req.headers['Content-Type'] = 'application/json'
        req.body = payload
        req.options.timeout = post_config(no_retry)[:timeout]
        req.options.open_timeout = post_config(no_retry)[:open_timeout]
      end
    rescue *RESCUABLE_ERRORS_WITH_TRACKING => e
      message = "Failed to POST to fraud service: #{e.message}"
      Rails.logger.error(message + '\n ' + e.backtrace.join('\n '))
      nil
    rescue *CONNECTION_ERRORS => e
      Rails.logger.error("Fraud Service Client: Connection or timeout error: #{e.message}")
      statsd_client.increment('fraud_service.client.connection.error')
      nil
    rescue JohnnyFive::CircuitTrippedError => e
      Rails.logger.error("Fraud Service Client: Circuit Tripped error : #{e.message}")
      statsd_client.increment('fraud_service.client.circuit.error')
      nil
    end

    CONNECTION_ERRORS = [
      Faraday::ConnectionFailed,
      Faraday::Error::TimeoutError,
      Faraday::TimeoutError,
      Kragle::BadGateway,
    ].freeze

    RESCUABLE_ERRORS_WITH_TRACKING = [
      Kragle::InternalServerError,
      Kragle::UnprocessableEntity,
      Kragle::InvalidContentType,
      Kragle::ResourceNotFound,
      Kragle::ClientError
    ].freeze

    @circuit_breaker_default_options = {
        circuit_breaker_failure_threshold: 3,
        circuit_breaker_reset_timeout_secs: 5
    }

    def self.post_config(no_retry)
      if no_retry
        @circuit_breaker_default_options.merge(timeout: 1, open_timeout: 0.5, max_retries: 0)
      else
        @circuit_breaker_default_options.merge(timeout: 7, open_timeout: 4, max_retries: 3)
      end
    end

    def self.fraud_service_client(no_retry)
      retry_options = {
          max: post_config(no_retry)[:max_retries]
      }
      circuit_options = {
          failure_threshold: post_config(no_retry)[:circuit_breaker_failure_threshold],
          reset_timeout: post_config(no_retry)[:circuit_breaker_reset_timeout_secs],
          dry_run: false
      }
      if no_retry
        @fraud_service_client_no_retry ||= begin
          Kragle.new('account_fraud_service', retry_options: retry_options, circuit_options: circuit_options).tap do |kragle|
            kragle.url_prefix = build_url_prefix
          end
        end
      else
        @fraud_service_client_retry ||= begin
          Kragle.new('account_fraud_service', retry_options: retry_options, circuit_options: circuit_options).tap do |kragle|
            kragle.url_prefix = build_url_prefix
          end
        end
      end
    end

    def self.build_url_prefix
      if Rails.env.development?
        'http://orca.zd-dev.com'
      else
        pod_id = Zendesk::Configuration.fetch(:pod_id)
        "http://pod-#{pod_id}.account-fraud-service.service.consul:5000"
      end
    end

    def self.reset
      @fraud_service_client_no_retry = nil
      @fraud_service_client_retry = nil
    end

    def self.statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'fraud_service_client')
    end
  end
end
