# Examples
#   SpamDetector runs on a new (to be saved) ticket before creation.
#
#   detector = Fraud::SpamDetector.new(ticket)
#
#   detector.spam_indicators # Returns an array of spammy tags
#
#   These actions will tell you what to do with spammy tickets.
#   detector.suspend_ticket?
#   detector.reject_ticket?
#

module Fraud
  class SpamDetector
    class SpamTicketFoundException < StandardError; end
    include Fraud::FraudSafelist
    include Fraud::FraudServiceHelper
    include Fraud::FraudTicketHelper
    include Fraud::Normalizer::FraudUnicodeNormalizer
    include Fraud::Normalizer::FraudEmailNormalizer

    attr_reader :spam_indicators

    delegate :account, :via_id, to: :ticket

    ONLINE_CHAT_TAG = 'zopim_chat'.freeze

    SUSPENDED_TICKET_PATTERNS = %w[
      all_channels_suspended_domain
      blacklisted_ip_address
      blacklisted_string
      blacklisted_string_for_targeted_domains
      blacklisted_string_for_targeted_domains_reduced
      blacklisted_regex
      blacklisted_regex_for_targeted_domains
      blacklisted_regex_for_targeted_domains_reduced
      blacklisted_requester_name
      spam_digest_neighbor
      suspended_domain
    ].freeze

    TARGETED_EMAIL_DOMAINS = %w[126.com 139.com 163.com foxmail.com qq.com].freeze

    TICKET_TEXT_FIELDS = %w[ticket_subject ticket_description ticket_requester_name ticket_comment].freeze

    MINIMUM_NILSIMSA_FIELD_LENGTH = 10
    MINIMUM_SPAMMY_TOKEN_LENGTH = 6

    NILSIMSA_DIGEST_LENGTH = 56

    def initialize(ticket, log_attributes = true)
      # log_attributes is only used with Rails.logger, this is not the case with mail

      @ticket = ticket
      @current_account = account
      @spam_indicators = []
      @log_attributes = log_attributes

      return if should_not_scan_ticket?

      process_spam_indicators
    end

    def suspend_ticket?
      @spam_indicators.each do |indicator|
        next unless SUSPENDED_TICKET_PATTERNS.include?(indicator)
        tag_action('suspended_ticket')
        return true
      end

      false
    end

    def reject_ticket?
      if @spam_indicators.include?('rejected_domain')
        tag_action('rejected_ticket')
        return true
      end

      false
    end

    private

    def should_not_scan_ticket?
      online_chat_ticket?
    end

    attr_reader :ticket

    def process_spam_indicators
      benchmark_start = Time.now
      if account.has_spam_detector_blacklist_pattern_check?
        mark_blacklisted_patterns
      else
        Rails.logger.info("SPAM_DETECTOR - skipping_blacklist_pattern_check - #{account.subdomain} (#{account.id})")
      end
      simulate_blacklisted_patterns if account.has_orca_classic_spam_detector_simulate_patterns?
      mark_blacklisted_domain       if sanitized_email_address
      mark_bad_requester_ip_address if account.has_spam_detector_bad_ip?
      mark_blacklisted_requester_email if account.has_orca_classic_spam_detector_blacklisted_requester_email? && sanitized_email_address
      mark_blacklisted_pattern_requester_name if account.has_orca_classic_spam_detector_blacklisted_requester_name?
      mark_spam_digest_neighbors if account.has_orca_classic_nilsimsa_atsd_compare_spam_digests?

      duration = Time.now - benchmark_start
      statsd_client.histogram('full_run.duration', duration, tags: %W[source:#{ViaType[via_id].name}])
    end

    def mark_blacklisted_requester_email
      reduced_email_address = normalize_email_address(sanitized_email_address)
      if spammy_blacklisted_strings('string').include?(reduced_email_address)
        @detected_arturo_pattern = reduced_email_address
        mark_and_tag_pattern('blacklisted_string', 'requester_email_address')
        Rails.logger.info("ATSD_requester_block: {account_id: #{account.id}, original_email: #{sanitized_email_address}, sanitized_email: #{reduced_email_address}}")
      end
    end

    def mark_blacklisted_pattern_requester_name
      # Goes to fraud_ticket_helper
      requester_name_text = send(:ticket_requester_name, ticket)

      return if requester_name_text.blank?

      if spammy_blacklisted_strings('requester_name').include?(requester_name_text)
        @detected_arturo_pattern = requester_name_text
        # orca_classic_req_name_blacklist_take_action: if this is GA'd then it should block
        log_requester_name_change = !account.has_orca_classic_req_name_blacklist_take_action?
        mark_and_tag_pattern('blacklisted_requester_name', 'requester_name', log_requester_name_change)
      end
    end

    def mark_bad_requester_ip_address
      requester_ip_address = system_audit(ticket).try(:[], 'ip_address')

      return if requester_ip_address.blank? || safe_ip_address?(requester_ip_address)

      if spammy_blacklisted_strings('ip_address').include?(requester_ip_address)
        @detected_arturo_pattern = requester_ip_address
        mark_and_tag_pattern('blacklisted_ip_address', 'requester_ip_address')
      end
    end

    def mark_blacklisted_domain
      if domain_blacklist.suspend?(requester_email_address)
        mark_and_tag_pattern('suspended_domain', 'requester_email_domain')
      elsif domain_blacklist.reject?(requester_email_address)
        mark_and_tag_pattern('rejected_domain', 'requester_email_domain')
      end
      return unless account.has_orca_classic_blocklist_all_channel_log_only? || account.has_orca_classic_blocklist_all_channel?
      unless account.allows?(requester_email_address.address)
        mark_and_tag_pattern('all_channels_suspended_domain', 'requester_email_domain', !account.has_orca_classic_blocklist_all_channel?)
      end
    end

    def mark_blacklisted_patterns
      TICKET_TEXT_FIELDS.each do |ticket_field|
        text = send(ticket_field, ticket)
        return if blacklisted_patterns_for_targeted_domains_pattern?(text, ticket_field) || blacklisted_string_pattern?(text, ticket_field, 'string') || blacklisted_regex_pattern?(text, ticket_field, 'regex')
      end
    end

    def simulate_blacklisted_patterns
      TICKET_TEXT_FIELDS.each do |ticket_field|
        text = send(ticket_field, ticket)
        if spammy_blacklisted_strings('string', true).detect { |spammy_string| @detected_arturo_pattern = spammy_string if text.include?(spammy_string) }
          mark_and_tag_pattern('simulate_blacklisted_string', ticket_field, true)
        end
      end
    end

    def mark_spam_digest_neighbors
      TICKET_TEXT_FIELDS.each do |ticket_field|
        text = send(ticket_field, ticket)
        next unless run_nilsimsa_on_text?(text)

        nilsimsa = Fraud::NilsimsaLSH.new(text, ticket_field, @log_attributes, account.has_orca_classic_log_nilsimsa_score?)

        # upcase the Arturo spam digests, which were force downcased, to match base32, which uses capital chars
        if nilsimsa_spam_digests.map(&:upcase).detect { |spam_digest| @detected_arturo_pattern = spam_digest if nilsimsa.neighbor_match?(spam_digest) }
          mark_and_tag_pattern('spam_digest_neighbor', ticket_field, account.has_orca_classic_nilsimsa_atsd_block_spam_digest_log_only?)
        end
      end
    end

    # returns true & marks the ticket suspicious if the pattern is detected
    def blacklisted_string_pattern?(text, ticket_field, arturo_sub_name = 'string', log_only = false, reduced = false)
      return false unless text.present?

      if spammy_blacklisted_strings(arturo_sub_name).detect { |spammy_string| @detected_arturo_pattern = spammy_string if text.include?(spammy_string) }
        if reduced
          mark_and_tag_pattern("blacklisted_#{arturo_sub_name}_reduced", ticket_field, log_only)
        else
          mark_and_tag_pattern("blacklisted_#{arturo_sub_name}", ticket_field, log_only)
        end

        return true
      end
      false
    end

    def blacklisted_regex_pattern?(text, ticket_field, arturo_sub_name = 'regex', log_only = false, reduced = false)
      return false unless text.present?

      spammy_blacklisted_strings(arturo_sub_name).each do |spammy_regex|
        begin
          regex_pattern = Regexp.new(spammy_regex, Regexp::IGNORECASE)
          if text.match?(regex_pattern)
            @detected_arturo_pattern = regex_pattern.to_s
            if reduced
              mark_and_tag_pattern("blacklisted_#{arturo_sub_name}_reduced", ticket_field, log_only)
            else
              mark_and_tag_pattern("blacklisted_#{arturo_sub_name}", ticket_field, log_only)
            end

            return true
          end
        rescue => e
          statsd_client.increment('ticket.regex.errors', tags: ["error:#{e}"])
        end
      end

      false
    end

    # returns true & marks the ticket suspicious if the pattern is detected on a targeted domain
    def blacklisted_patterns_for_targeted_domains_pattern?(text, ticket_field)
      reduced_text = normalize_unicode(text)
      unless account.has_orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains?
        log_reduced_text(ticket_field, text, reduced_text, "EXCLUDED from orca_classic_spam_detector_blacklisted_patterns_for_targeted_domains")
        return false
      end
      return false unless TARGETED_EMAIL_DOMAINS.include?(requester_email_address.domain)

      # Short circuit on running normalized and kanji versions
      result = blacklisted_string_pattern?(text, ticket_field, 'string_for_targeted_domains') ||
          blacklisted_regex_pattern?(text, ticket_field, 'regex_for_targeted_domains') ||
          blacklisted_string_pattern?(reduced_text, ticket_field, 'string_for_targeted_domains', false, true) ||
          blacklisted_regex_pattern?(reduced_text, ticket_field, 'regex_for_targeted_domains', false, true)

      log_reduced_text(ticket_field, text, reduced_text, result)

      result
    end

    def log_reduced_text(field_name, text, reduced_text, result)
      if @log_attributes
        attribute = fetch_targeted_domains_attribute
        attribute[field_name.to_sym] = {subdomain: account.subdomain, account_id: account.id, result: result, atsd_reduced_text: reduced_text, atsd_original_text: text}
        Rails.logger.append_attributes(targeted_domains: attribute)
      else
        Rails.logger.info("SPAM_DETECTOR - subdomain: #{account.subdomain}, account_id: #{account.id}, result: #{result}, ATSD_reduced_text: #{reduced_text}, ATSD_original_text: #{text}")
      end
    end

    def fetch_targeted_domains_attribute
      current_attributes = Rails.logger.attributes
      current_attributes[:targeted_domains] || {}
    end

    # for "spammy_string_blacklist" & "spammy_regex_blacklist"
    #  the external_beta_subdomains method returns strings that represent strings & regular expressions
    #
    # arturo_type:  string that equals 'string' or 'regex' or 'string_for_targeted_domains' or 'regex_for_targeted_domains'
    # simulate: this will simulate using a NOOP arturo of patterns
    #
    # returns: string that represent strings or regular expressions that are in spammy tickets
    def spammy_blacklisted_strings(arturo_type, simulate = false)
      arturo_prefix = simulate ? 'simulate' : 'spammy'
      arturo_name = "#{arturo_prefix}_#{arturo_type}_blacklist".downcase.to_sym

      return [] unless arturo_feature = Arturo::Feature.find_feature(arturo_name)
      arturo_feature.external_beta_subdomains.select do |pattern|
        if pattern.length < MINIMUM_SPAMMY_TOKEN_LENGTH
          Rails.logger.info("ATSD_INVALID_PATTERN: #{pattern}")
          statsd_client.increment('ticket.atsd.invalid_pattern', tags: ["pattern:#{pattern}"])
          false
        else
          true
        end
      end
    end

    def nilsimsa_spam_digests
      return [] unless arturo_feature = Arturo::Feature.find_feature(:orca_nilsimsa_spam_digests)
      arturo_feature.external_beta_subdomains.select do |pattern|
        if pattern.length != NILSIMSA_DIGEST_LENGTH
          Rails.logger.info("ATSD_INVALID_PATTERN: #{pattern}")
          statsd_client.increment('ticket.atsd.invalid_pattern', tags: ["pattern:#{pattern}"])
          false
        else
          true
        end
      end
    end

    def tag_action(action)
      statsd_client.increment('ticket.spam', tags: ["action:#{action}"])
    end

    def mark_and_tag_pattern(spam_pattern, ticket_field, log_only = false)
      @spam_indicators << spam_pattern unless log_only
      if @log_attributes
        spam_detector_attr = {
            account_id: account.id,
            type: spam_pattern,
            matching_pattern: @detected_arturo_pattern,
            matching_field: ticket_field,
            log_only: log_only
        }
        Rails.logger.append_attributes(spam_detector: spam_detector_attr)
      end
      Rails.logger.info("ATSD_SPAM_DETECTED: {account:#{account.id}, type:#{spam_pattern}, matching_pattern:#{@detected_arturo_pattern}, matching_field:#{ticket_field}, log_only:#{log_only}}")
      statsd_client.increment('ticket.spam', tags: ["type:#{spam_pattern}", "source:#{via_name(via_id)}", "matching_pattern:#{@detected_arturo_pattern}", "log_only:#{log_only}"])
    end

    def statsd_client
      @statsd_client ||= Zendesk::StatsD::Client.new(namespace: 'spam_detector')
    end

    def domain_blacklist
      @domain_blacklist ||= Zendesk::InboundMail::Processors::BlacklistProcessor::DomainBlacklist.new(account.domain_blacklist.to_s)
    end

    def requester_email_address
      @requester_email_address ||= Zendesk::Mail::Address.new(address: sanitized_email_address)
    end

    def sanitized_email_address
      Zendesk::Mail::Address.sanitize(ticket.requester.try(:email))
    end

    def via_name(via_id)
      ViaType[via_id].name
    end

    def online_chat_ticket?
      via_id == ViaType.CHAT && @ticket.current_tags.present? && @ticket.current_tags&.include?(ONLINE_CHAT_TAG)
    end

    def run_nilsimsa_on_text?(text)
      text && text.length >= MINIMUM_NILSIMSA_FIELD_LENGTH
    end

    def generated_text_fields
      TICKET_TEXT_FIELDS.map { |ticket_field| send(ticket_field, ticket) }
    end
  end
end
