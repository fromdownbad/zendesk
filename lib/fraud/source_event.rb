module Fraud
  class SourceEvent < Zendesk::Types::BaseType
    attr_reader :name, :is_manual_action

    ACCOUNT_UPDATE_EVENTS = [
      :autosuspended_account,
      :cancelled_account,
      :email_template_update,
      :email_text_update,
      :enterprise_subscription,
      :hermes_feedback,
      :manual_suspension,
      :new_account_ticket_rate_limit,
      :new_user_identity_created,
      :purchase_with_new_cc,
      :risky_cc_ticket,
      :anonymous_ticket,
      :spam_ticket,
      :request_ticket,
      :risk_assessment_daily_job_safe_age,
      :risk_assessment_daily_job_safe_paid,
      :mark_as_abusive,
      :mark_as_not_abusive,
      :suspended_account,
      :taken_over_account,
      :trial_extension,
      :unsuspended,
      :update_account_name,
      :update_cc,
      :update_owner_email,
      :update_owner_name,
      :update_subdomain,
      :update_trigger,
      :untakeover,
      :unwhitelisted,
      :user_identity_trial_limit_exceeded,
      :watchlisted,
      :whitelisted
    ].freeze

    FRAUD_SIGNAL_EVENTS = [
      :spammy_inbound_ticket_activity
    ].freeze

    SIGNUP_EVENTS = [
      :shell_created,
      :trial_signup,
      :trial_sign_up,
      :sc_account_trial_signup
    ].freeze

    def initialize(name, configs)
      @name = name.to_sym
      @is_manual_action = configs[:is_manual_action]
    end

    class << self
      def configs
        default = { is_manual_action: false }
        configs = Hash.new { |hash, key| hash[key] = default }

        (ACCOUNT_UPDATE_EVENTS + SIGNUP_EVENTS + FRAUD_SIGNAL_EVENTS).each do |source_event|
          configs[source_event]
          configs["sc_account_#{source_event}".to_sym]
        end

        other_events = {
          whitelisted: default.merge(is_manual_action: true),
          sc_account_whitelisted: default.merge(is_manual_action: true),
          suspended_account: default.merge(is_manual_action: true),
          sc_account_suspended_account: default.merge(is_manual_action: true),
          sc_button:  default.merge(is_manual_action: true),
          monitor: default.merge(is_manual_action: true),
          sc_account_taken_over_account: default.merge(is_manual_action: true),
          enterprise_subscription: default.merge(is_manual_action: true),
          manual_suspension: default.merge(is_manual_action: true),
          untakeover: default.merge(is_manual_action: true)
        }.freeze

        configs.merge(other_events)
      end

      def [](event)
        new(event, configs.fetch(event.try(:to_sym)))
      end

      def const_missing(event)
        return unless event
        event.to_s.split.map(&:downcase).join('_').to_sym
      end
    end
  end
end
