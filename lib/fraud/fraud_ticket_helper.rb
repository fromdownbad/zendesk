module Fraud
  module FraudTicketHelper
    DELETE = 'delete'.freeze
    TAG = 'tag'.freeze
    PRIOR_SPAM_CLEANUP_TAG = 'spam-cleanup'.freeze # TODO: this will only work for non-custom tags
    RECENT_CREATED_USER_IN_SECONDS = 15

    private

    def ticket_audit(ticket)
      ticket.audits&.first || ticket.audit
    end

    def system_audit(ticket)
      ticket_audit(ticket).try(:metadata).try(:[], 'system')
    end

    def outbound_ticket?(requester)
      requester.is_agent? if requester
    end

    # Methods below are currently for SpamCleanupJob
    def add_tag_to_ticket(tag, ticket)
      action_type = TAG
      log_fraud_ticket_action(ticket, action_type, tag)
      if actions_enabled?(action_type) && !prior_cleanup_tag?(ticket, tag)
        ticket.additional_tags = tag
        ticket.save!
      end
    rescue StandardError => e
      log_fraud_ticket_action_error(ticket, action_type, e)
    end

    def delete_fraud_ticket_data(tag, ticket)
      action_type = DELETE

      unless ticket_audit(ticket).present?
        log_fraud_ticket_action_error(ticket, action_type, 'missing_audit')
        return
      end

      log_fraud_ticket_action(ticket, action_type, tag)

      if actions_enabled?(action_type)
        ticket.soft_delete!

        if should_delete_requester?(ticket)
          ticket.requester.current_user = User.system
          ticket.requester.delete!
        end
      end
    rescue StandardError => e
      log_fraud_ticket_action_error(ticket, action_type, e)
    end

    def report_ticket_to_rspamd(user, ticket)
      if should_send_ticket_to_rspamd?(ticket)
        statsd_client_fraud_ticket_actions.increment('rspamd_spam_cleanup')
        RspamdFeedbackJob.report_spam(user: user, identifier: ticket.original_raw_email_identifier, recipient: ticket.recipient)
      end
    end

    def prior_cleanup_tag?(ticket, tag)
      if ticket.current_tags && ticket.current_tags.include?(PRIOR_SPAM_CLEANUP_TAG)
        Rails.logger.info("FraudTicketActions: Cleanup Tag Already Exists: Account_id: #{ticket.account_id}, Ticket: #{ticket.nice_id}, prior_tags: #{ticket.current_tags}, new_tag: #{tag}")
        return true
      end
      false
    end

    def log_fraud_ticket_action(ticket, action_type, tag)
      Rails.logger.info("FraudTicketActions: Account_id: #{ticket.account_id}, Ticket: #{ticket.nice_id}, action: #{action_type} -- #{action_message(action_type)}, tag: #{tag}")
      statsd_client_fraud_ticket_actions.increment(action_type, tags: ["log_only:#{actions_enabled?(action_type)}"])
    end

    def log_fraud_ticket_action_error(ticket, action_type, error)
      Rails.logger.error "FraudTicketActions: #{action_type}_error -- #{error} for Account_id: #{ticket.account_id}, Ticket: #{ticket.nice_id}"
      statsd_client_fraud_ticket_actions.increment("#{action_type}_error")
    end

    def action_message(action_type)
      actions_enabled?(action_type) ? "action fired" : "dry run"
    end

    def actions_enabled?(action_type)
      @current_account.send("has_orca_classic_#{action_type}_fraud_ticket?".to_sym)
    end

    def statsd_client_fraud_ticket_actions
      @statsd_client_fraud_ticket_actions ||= Zendesk::StatsD::Client.new(namespace: ['fraud_ticket_actions'])
    end

    def ticket_subject(ticket)
      normalize_ticket_content(ticket.subject)
    end

    def ticket_description(ticket)
      normalize_ticket_content(ticket.description)
    end

    def ticket_requester_name(ticket)
      normalize_ticket_content(ticket.requester.try(:name))
    end

    def ticket_comment(ticket)
      return "" unless ticket.account.has_spam_detector_comments?
      normalize_ticket_content(ticket.comment&.body)
    end

    def should_send_ticket_to_rspamd?(ticket)
      ticket_audit(ticket).present? &&
        ticket.original_raw_email_identifier.present? &&
        @current_account.has_email_rspamd_training_enabled? &&
        @current_account.has_orca_classic_email_rspamd_training?
    end

    def should_delete_requester?(ticket)
      ticket.status?(:deleted) &&
        ticket.requester &&
        ticket.requester.tickets.count == 0 &&
        recently_created_requester?(ticket.requester.created_at, ticket.created_at)
    end

    def recently_created_requester?(requester_create_date, ticket_create_date)
      requester_create_date.between?(ticket_create_date - RECENT_CREATED_USER_IN_SECONDS, ticket_create_date)
    end

    def normalize_ticket_content(content, truncation = 2_000)
      content.to_s.first(truncation).downcase
    end
  end
end
