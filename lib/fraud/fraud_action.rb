module Fraud
  class FraudAction
    attr_reader :name, :trial_only
    def initialize(name, config)
      @name = name.to_sym
      @trial_only = config[:trial_only]
    end

    # Trial only is a safety check to prevent misfires, such as suspensions, on paid accounts
    def self.actions
      default = { trial_only: true }
      paid = default.merge(trial_only: false)

      {
        cancel:                            paid,
        enable_captcha:                    paid,
        mark_abusive:                      paid,
        mark_not_abusive:                  paid,
        mark_risk_assessment_as_safe_age:  paid,
        mark_risk_assessment_as_safe_paid: paid,
        takeover:                          paid,
        unsuspend:                         paid,
        unwhitelist:                       paid,
        whitelist:                         paid,
        auto_suspend:                      default,
        free_mail:                         default,
        manual_suspend:                    default,
        support_risky:                     default,
        talk_risky:                        default,
        outbound_email_risky:              default,
        outbound_email_safe:               default
      }.freeze
    end

    def self.[](action)
      return unless action && actions[action.to_sym]
      action_config = actions[action.to_sym]
      new(action, action_config)
    end

    def self.const_missing(action)
      return unless action
      action.to_s.split.map(&:downcase).join('_').to_sym
    end
  end
end
