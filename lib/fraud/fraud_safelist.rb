module Fraud
  module FraudSafelist
    require 'ipaddr'

    # Zendesk Internal IPs that should always be considered safe
    DEFAULT_SAFE_IP_RANGES = "10.0.0.0/8 185.12.80.0/22 188.172.128.0/20 192.161.144.0/20 216.198.0.0/18".freeze

    SAFE_IP_RANGES = ENV.fetch('FRAUD_SAFELIST_IP_RANGES', DEFAULT_SAFE_IP_RANGES)

    def safe_ip_address?(ip_address, logger = Rails.logger)
      # Logger may be passed by reference because mail uses a different logger and logging index
      return false unless account&.has_orca_classic_ip_safelist?

      safe_ip_ranges(logger).each do |ip_range|
        if ip_range.include?(ip_address)
          logger.info("#{ip_address} was found to be a safelisted IP address, skipping action")
          return true
        end
      end

      false
    rescue IPAddr::InvalidAddressError
      logger.info("#{ip_address} is not an ip address, FraudSafelist check has been skipped")
      false
    rescue StandardError => e
      logger.info("Unexpected #{e} occurred in FraudSafeList")
      false
    end

    private

    def safe_ip_ranges(logger)
      SAFE_IP_RANGES.split(" ").map { |safe_ip| IPAddr.new(safe_ip) }
    rescue IPAddr::InvalidAddressError => e
      logger.info("#{e} when trying to create safe_ip_ranges")
      []
    rescue StandardError => e
      logger.info("Unexpected #{e} when trying to create safe_ip_ranges")
      []
    end
  end
end
