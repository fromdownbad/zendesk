module Fraud
  module FraudScoreActions
    private

    TAKEN_OVER_EMAIL_SUBSTRING = "abuseincidents".freeze

    ###
    # If any new action is added:
    #  1. Create a new "{action}_via_afs" arturo
    #  2. Add a config in FraudAction
    ###

    # actions = {:auto_suspend=>true, :takeover=>true, :whitelist=>true}
    def perform_fraud_actions(actions, source_event = nil)
      actions.each do |action, verdict|
        next unless verdict && current_account.send("has_#{action}_via_afs?".to_sym)
        perform_fraud_action(action, source_event)
      end

      save_account_and_settings!
    end

    def perform_fraud_action(action, source_event = nil)
      if current_account.subscription.present? && !current_account.is_trial? && Fraud::FraudAction[action].trial_only
        statsd_client_fraud_actions.increment("unsafe_paid_action", tags: ["action:#{action}", "source_event:#{source_event}"])
        return
      end
      log_action(action.to_s, source_event.to_s)
      send(action.to_sym)
    end

    def whitelist
      current_account.is_serviceable = true
      current_account.settings.prior_risk_assessment = current_account.settings.risk_assessment
      current_account.settings.whitelisted_from_fraud_restrictions = true
      current_account.settings.user_identity_limit_for_trialers = false
      current_account.settings.bulk_user_upload = true
      mark_not_abusive
      assign_risk_assessment(::Account::FraudSupport::MONITOR_WHITELIST_RISK_ASSESSMENT)
    end

    def unwhitelist
      current_account.settings.whitelisted_from_fraud_restrictions = false
      current_account.settings.user_identity_limit_for_trialers = true if current_account.is_trial?
      assign_risk_assessment(current_account.settings.prior_risk_assessment)
    end

    ###
    # We check current_account.settings.whitelisted_from_fraud_restrictions instead of current_account.whitelisted?
    # because we modify whitelisted_from_fraud_restrictions when we unwhitelist.
    # This is to serve an edge case for when we want to takeover a whitelisted (sales assisted) account.
    # In order to serve that edge case, we unwhitelist and THEN takeover in monitor
    #
    # Please keep these methods in order.
    ###
    def takeover
      return if current_account.settings.whitelisted_from_fraud_restrictions
      assign_risk_assessment(::Account::FraudSupport::MONITOR_TAKEOVER_RISK_ASSESSMENT)
      disable_password_change_email
      disable_mobile_app_access
      takeover_owner_password
      takeover_owner_email
      restrict_api_access
      mark_abusive
      current_account.logout_all_users_and_destroy_tokens!
    end

    def auto_suspend
      return if current_account.whitelisted?
      current_account.is_serviceable = false
      mark_abusive
      current_account.settings.help_center_state = 'disabled'
      assign_risk_assessment(::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT)
      restrict_api_access
      revert_templates_to_default
    end

    alias_method :manual_suspend, :auto_suspend

    def outbound_email_risky
      return if current_account.whitelisted?
      assign_risk_assessment(::Account::FraudSupport::EMAIL_UNSAFE_RISK_ASSESSMENT)
    end

    def outbound_email_safe
      assign_risk_assessment(::Account::FraudSupport::EMAIL_SAFE_RISK_ASSESSMENT)
    end

    def unsuspend
      mark_not_abusive
      current_account.is_serviceable = true
      assign_risk_assessment(::Account::FraudSupport::DEFAULT_RISK_ASSESSMENT)
      Voice::ActivateVoiceJob.enqueue(current_account.id) if current_account.try(:voice_sub_account).try(:suspended?)
    end

    def support_risky
      assign_risk_assessment(::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT)
    end

    def talk_risky
      assign_risk_assessment(::Account::FraudSupport::TALK_RISKY_RISK_ASSESSMENT)
    end

    def disable_mobile_app_access
      current_account.settings.mobile_app_access = false
    end

    def disable_password_change_email
      current_account.settings.email_agent_when_sensitive_fields_changed = false
      # This save is needed to update the risk_assessment to help prevent the email notification in #skip_notification?
      # app/observers/user_identity_observer.rb:46
      save_account_and_settings!
    end

    def cancel
      mark_abusive
    end

    def save_account_and_settings!
      Zendesk::SupportAccounts::Product.retrieve(current_account).save!
      current_account.settings.save!
    end

    def mark_abusive
      update_is_abusive(true)
    end

    def mark_not_abusive
      update_is_abusive(false)
    end

    # Do not call directly
    def update_is_abusive(value)
      pravda_client.update_account(is_abusive: value)
      current_account.settings.is_abusive = value
    end

    def account_already_taken_over?
      current_account.owner.primary_email_identity.value.include?(TAKEN_OVER_EMAIL_SUBSTRING)
    end

    def log_action(action, source_event)
      Rails.logger.info "Fraud Score Action #{action} for #{source_event} on #{current_account.inspect}"
      whitelisted = current_account.settings.whitelisted_from_fraud_restrictions
      statsd_client_fraud_actions.increment("success", tags: ["action:#{action}", "source_event:#{source_event}", "whitelisted:#{whitelisted}"])
    end

    def statsd_client_fraud_actions
      @statsd_client_fraud_actions ||= Zendesk::StatsD::Client.new(namespace: ['fraud_actions'])
    end

    def takeover_owner_email
      return if account_already_taken_over?
      new_abuse_email = "#{TAKEN_OVER_EMAIL_SUBSTRING}+#{SecureRandom.random_number(100000)}@zendesk.com"
      Rails.logger.info "Taking over account: #{current_account} whose owner email is: #{current_account.owner.email} with new email: #{new_abuse_email}"
      current_account.settings.prior_owner_email = current_account.owner.email
      current_account.owner.primary_email_identity.update_attribute(:value, new_abuse_email)
      current_account.owner.save!
    end

    def takeover_owner_password
      return if account_already_taken_over?
      current_account.settings.prior_owner_password = current_account.owner[:crypted_password]
      current_account.owner[:crypted_password] = current_account.owner.randomize_password
    end

    def restrict_api_access
      current_account.settings.api_token_access = false
      current_account.settings.api_password_access = false
    end

    def revert_templates_to_default
      current_account.account_property_set.try(:set_defaults)
    end

    def mark_risk_assessment_as_safe_age
      assign_risk_assessment(::Account::FraudSupport::SAFE_AGE_RISK_ASSESSMENT)
    end

    def mark_risk_assessment_as_safe_paid
      assign_risk_assessment(::Account::FraudSupport::SAFE_PAID_RISK_ASSESSMENT)
    end

    def assign_risk_assessment(assessment)
      current_account.settings.risk_assessment = assessment
    end

    def enable_captcha
      current_account.settings.captcha_required = true
    end

    def pravda_client
      @pravda_client ||= Zendesk::Accounts::Client.new(
        current_account,
        retry_options: {
          max: 3,
          interval: 2,
          exceptions: Zendesk::Accounts::Client::RETRYABLE_ERRORS,
          methods: [:patch]
        },
        timeout: 10
      )
    end
  end
end
