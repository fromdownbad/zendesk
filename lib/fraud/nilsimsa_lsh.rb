require 'nilsimsa'

module Fraud
  class NilsimsaLSH
    attr_reader :digest

    NEIGHBOR_THRESHOLD = ENV.fetch('NILSIMSA_NEIGHBOR_THRESHOLD', 54).to_i

    def initialize(text, text_field = "unknown", log_attributes = true, log_score = false)
      # log_attributes is only used with Rails.logger, this is not the case with mail

      @text = text
      @text_field = text_field
      @log_attributes = log_attributes
      @log_score = log_score

      @nilsimsa = Nilsimsa.new(@text)
      @digest = @nilsimsa.digest
      log_digest
    rescue StandardError => e
      Rails.logger.info("nilsimsa_lsh_error: #{e} scanning #{@text.first(100)}")
    end

    def neighbor_match?(other_digest)
      if @log_score
        score = @nilsimsa.bit_difference(other_digest)
        if score <= NEIGHBOR_THRESHOLD
          log_match(other_digest, score)
          return true
        end
      elsif @nilsimsa.neighbor_digest?(other_digest, NEIGHBOR_THRESHOLD)
        log_match(other_digest)
        return true
      end
      false
    rescue StandardError => e
      Rails.logger.info("nilsimsa_neighbor_error: #{e} comparing #{@digest} to #{other_digest}")
      false
    end

    private

    def fetch_nilsimsa_attribute
      # to avoid overwriting the old attribute, fetch/create before adding the new text field attributes
      current_attributes = Rails.logger.attributes
      current_attributes[:nilsimsa] || {}
    end

    def log_match(other_digest, score = nil)
      if @log_attributes
        nilsimsa_attribute = fetch_nilsimsa_attribute
        nilsimsa_attribute[:neighbor_match] = other_digest
        nilsimsa_attribute[:neighbor_match_score] = score if @log_score
        Rails.logger.append_attributes(nilsimsa: nilsimsa_attribute)
      end
      Rails.logger.info("nilsimsa_neighbor_match: #{@digest} with #{other_digest}#{" (#{score})" if @log_score}")
    end

    def log_digest
      log_text_attributes
      Rails.logger.info("nilsimsa_lsh: field #{@text_field} with #{@text.first(100)} has hash: #{digest}")
    end

    def log_text_attributes
      if @log_attributes
        nilsimsa_attribute = fetch_nilsimsa_attribute
        nilsimsa_attribute[@text_field.to_sym] = {
          digest: digest,
          source_text: @text.first(100)
        }
        Rails.logger.append_attributes(nilsimsa: nilsimsa_attribute)
      end
    rescue StandardError => e
      Rails.logger.info("nilsimsa_lsh_error: #{e}")
    end
  end
end
