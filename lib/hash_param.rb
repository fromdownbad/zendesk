# Some objects need to behave like a Hash or ActionController::Parameters
# object by using `dig` and/or `[]` with symbol or sting arguments
# to access nested data.
module HashParam
  module_function def ish?(hash)
    hash.respond_to?(:dig) && !hash.is_a?(Array)
  end
end
