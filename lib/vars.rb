CATEGORY_DELIMITER = '::'.freeze

# These will be used to create seeds in dev, but this constant is also used when going through a non-seed code path
# You cannot use a regex here.
DEV_SUBDOMAINS = %w[support dev hercules mondocam reaper appsubmission appdevelopersupport cerberus zendeskaccounts].freeze

# Used as admins and oauth client identifiers in zendeskaccounts
ZENDESK_ACCOUNTS_ADMINS = %w[accountservices shopify qa fabric zendeskwebsite magento status_monitor inbox newzendeskautomations manualtest voyager marketingtravis].freeze

GOODDATA_IPS = %w[
  161.47.43.128
  161.47.43.129
  161.47.43.130
  161.47.43.131
  161.47.43.132
  161.47.43.133
  161.47.43.134
  161.47.43.135
  161.47.43.136
  161.47.43.137
  161.47.43.138
  161.47.43.139
  161.47.43.140
  161.47.43.141
  161.47.43.142
  161.47.43.143
  161.47.43.144
  161.47.43.145
  161.47.43.146
  161.47.43.147
  161.47.43.148
  161.47.43.149
  161.47.43.150
  161.47.43.151
  161.47.43.152
  161.47.43.153
  161.47.43.154
  161.47.43.155
  161.47.43.156
  161.47.43.157
  161.47.43.158
  161.47.43.159
  107.22.195.240
  50.17.238.245
  108.171.171.238
  172.30.8.139
  78.136.2.250
  94.236.99.32
  94.236.99.33
  94.236.99.34
  94.236.99.35
  94.236.99.36
  94.236.99.37
  94.236.99.38
  94.236.99.39
  94.236.99.40
  94.236.99.41
  94.236.99.42
  94.236.99.43
  94.236.99.44
  94.236.99.45
  94.236.99.46
  94.236.99.47
].freeze

SYSTEM_ACCOUNT_ID = -1
ROOT_ACCOUNT_ID   = ENV["ROOT_ACCOUNT_ID"] || 1
BOOSTING_ENABLED  = true # !Rails.env.production?
BOOSTING_ON_UNTIL = Date.parse('2010-10-27')
ASSET_IMAGE_PROCESSOR = :mini_magick

AM_PM_COUNTRIES = [
  "Australia", "Bangladesh", "Canada", "Colombia", "Egypt", "Pakistan", "India", "Malaysia", "New Zealand", "Philippines", "South Korea", "United Kingdom", "United States"
].freeze

URI_IN_NAME_REGEX = /https?:\/\/[-a-zA-Z0-9+&@#\/%=~_|?!:,.;]*[-a-zA-Z0-9+&@#\/%=~_|]/
URL_PATTERN = /\A(http|https):\/\/[a-z0-9]+(([\.]|[\-]{1,2})[a-z0-9]+)*\.([a-z]{2,16}|[0-9]{1,3})((:[0-9]{1,5})?(\/?|\/.*))?\z/ix
SUBDOMAIN_PATTERN = /\A[a-z0-9][a-z0-9\-]{1,}[a-z0-9]\z/
DOMAIN_PATTERN   = /\A[a-zA-Z0-9.-]{1,64}\.([a-zA-Z]{2,13})\z/
PASSWORD_PATTERN = /\A[a-zA-Z0-9_!\?\-\.\/]{5,}\z/
EMAIL_PATTERN    = /\A((\"[^\"\f\n\r\t\v\b]+\")|([\.\w=\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+))@((\[(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))\])|(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))|((([A-Za-z0-9\-])+\.)+[A-Za-z\-]+))\z/
SPECIAL_CHARS    = /[¤£`´\[\]\{\}\(\)<>%&\?\+,;@!\*\$\#='\\"\u200B\uFEFF\u180E[:space:]]/

FIND_EMAIL_PATTERN = /#{EMAIL_PATTERN.source.sub!('\A', '').chomp!('\z')}/
FIND_MULTIPLE_EMAIL_PATTERN = /(?<email>#{FIND_EMAIL_PATTERN})/

ICON_FOR_TYPE = Hash.new('page_white.png')
ICON_FOR_TYPE['application/msword']            = 'page_white_word.png'
ICON_FOR_TYPE['application/vnd.ms-powerpoint'] = 'page_white_powerpoint.png' # .ppt
ICON_FOR_TYPE['application/vnd.openxmlformats-officedocument.presentationml.presentation'] = 'page_white_powerpoint.png' # .pptx
ICON_FOR_TYPE['application/vnd.ms-excel']      = 'page_white_excel.png'
ICON_FOR_TYPE['application/pdf']               = 'page_white_acrobat.png'

NO_CONTENT = '[No content]'.freeze
NO_CHANGE  = 'control.dropdown.value.no_change'.freeze

MAX_COUNT_BEFORE_ASYNC = 100_000
# Constants for state
SENT_EXTERNALS = 'sent_externals'.freeze
SENT_POSTS = 'sent_posts'.freeze

# This is a default per page limit used in SOME endpoints.
# The vast majority of the endpoints use another value,
# DEFAULT_PER_PAGE (100), defined in
# https://github.com/zendesk/zendesk_api_controller/blob/master/lib/zendesk_api/controller/pagination.rb#L33
# In a controller that inherits from v2/base_controller, calling per_page will default
# to that value.
API_DEFAULT_PER_PAGE = 15

# Search specific limits
DEFAULT_PER_PAGE_FOR_SEARCH_INCREMENTAL = 30
SEARCH_API_PAGINATION_LIMIT = 200000
DEFAULT_PER_PAGE_FOR_SEARCH = 100

# This constant only seems to be used in the search endpoint.
API_MAX_PER_PAGE = 50

SAVE_TAB_TEXT = 'Save tab'.freeze
SAVE_NO_TABS_TEXT = 'Save'.freeze

ZENDESK_IN_THE_DB_ID = -1

TRACKING_EVENT_NAMES = {
  'GMailConnecter' => 'GMailConnecter'
}.freeze

DNS_CHARACTER_LIMIT = 63

RAILS_CACHE_PREFIX_VERSION = [
  RUBY_VERSION,
  RUBY_PATCHLEVEL,
  Rails.version,
  Zendesk::Configuration.fetch(:pod_id),
  'v0'
].join('-').freeze

RAILS_CACHE_PREFIX_VERSION_DALLI = "#{RAILS_CACHE_PREFIX_VERSION}.dalli".freeze
RAILS_CACHE_PREFIX_VERSION_DALLI_ELASTICACHE = "#{RAILS_CACHE_PREFIX_VERSION}.dalli-elasticache".freeze

# RAILS5UPGRADE: track down and remove unnecessary patches
RAILS4  = !!(Rails.version < '5') # means lower than 5. Hunt down and kill.
RAILS5  = !!(Rails.version > '5') # the rest imply version N+.
RAILS51 = !!(Rails.version >= '5.1')
RAILS52 = !!(Rails.version >= '5.2')
RAILS6  = !!(Rails.version > '6')
