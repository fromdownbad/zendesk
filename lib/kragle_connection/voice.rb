module KragleConnection::Voice
  def build_for_voice(account)
    voice_vip_host = Zendesk::Configuration.dig(:voice, :host) || ENV.fetch('VOICE_VIP_HOST')
    voice_vip_port = Zendesk::Configuration.dig(:voice, :port) || ENV.fetch('VOICE_VIP_PORT')

    conn = Kragle.new('voice', shared_cache: false)
    conn.url_prefix = "http://#{voice_vip_host}:#{voice_vip_port}"
    conn.params['id'] = account.id
    conn.headers['Host'] = account.host_name(mapped: false)
    conn.headers['X-Forwarded-Proto'] = 'https'
    conn
  end
end
