module KragleConnection
  SUNSHINE_ACCOUNT_CONFIG_TIMEOUT = 60
  SUNSHINE_ACCOUNT_CONFIG_OPEN_TIMEOUT = 10
  TIMEOUT = 10
  OPEN_TIMEOUT = 5
  PROXY_NLB_URL = "https://#{ENV.fetch('EDGE_PROXY_INTERNAL_NLB_URL')}".freeze

  extend KragleConnection::Voice

  class << self
    def build_for_account(account, service)
      connection = Kragle.new(service, shared_cache: false)
      connection.url_prefix = account.url(ssl: true, mapped: false)
      connection
    end

    def build_for_sell(account)
      Kragle.new('sell', shared_cache: false) do |connection|
        connection.url_prefix = "#{account.url(ssl: true, mapped: false)}/api/sell/private/"
        connection
      end
    end

    def build_for_sunshine_account_config(account)
      Kragle.new('sunshine_account_config', shared_cache: false) do |connection|
        connection.url_prefix = "https://accounts.#{Zendesk::Configuration.fetch(:host)}/api/sunshine/account_config/private/"
        connection.headers['X-Zendesk-Account-Id'] = account.id.to_s
        connection.options.timeout = SUNSHINE_ACCOUNT_CONFIG_TIMEOUT
        connection.options.open_timeout = SUNSHINE_ACCOUNT_CONFIG_OPEN_TIMEOUT
      end
    end

    def build_for_help_center(account)
      Kragle.new('help-center', shared_cache: false, signing: true, retry_options: { max: 1 }) do |connection|
        connection.url_prefix = PROXY_NLB_URL
        connection.options.timeout = 3
        connection.options.open_timeout = 2
        connection.headers['Host'] = account.host_name(mapped: false)
      end
    end

    def build_for_answer_bot_service(account)
      Kragle.new('answer_bot_service', shared_cache: false) do |connection|
        connection.url_prefix = account.url(ssl: true, mapped: false)
        connection.options.timeout = TIMEOUT
        connection.options.open_timeout = OPEN_TIMEOUT
      end
    end

    def build_for_sc_search_service
      conn = Kragle.new('side_conversations_search_service', shared_cache: false)
      conn.url_prefix = ENV.fetch('COLLABORATION_SEARCH_URL') do
        "http://pod-#{ENV.fetch('ZENDESK_POD_ID')}.collaboration-search.service.consul:3000"
      end
      conn
    end

    def build_for_sc_data_service(account)
      conn = Kragle.new('side_conversations_data_service', shared_cache: false)
      conn.url_prefix = ENV.fetch('COLLABORATION_DATA_URL') do
        "http://pod-#{ENV.fetch('ZENDESK_POD_ID')}.collaboration-data.service.consul:3000"
      end
      conn.headers['X-Zendesk-Account-Id'] = account.id.to_s
      conn
    end

    def build_for_explore(account)
      Kragle.new('explore', shared_cache: false, signing: true, retry_options: { max: 1 }) do |connection|
        connection.url_prefix = PROXY_NLB_URL
        connection.options.timeout = 3
        connection.options.open_timeout = 2
        connection.headers['Host'] = "#{account.subdomain.downcase}.#{Zendesk::Configuration.fetch(:host)}"
      end
    end
  end
end
