module SslRequirement
  # ssl_required:         Requires SSL regardless of account settings.
  # ssl_allowed:          Only requires SSL on SSL enabled accounts
  # skip_ssl_requirement: SSL is never enforced. Clients accessing this action on SSL enabled accounts won't have a shared session cookie.
  def self.included(controller)
    controller.extend(ClassMethods)
    controller.before_action(:ensure_proper_protocol)
    if RAILS4
      controller.hide_action :ssl_environment?
    end
  end

  protected

  module ClassMethods
    def self.extended(base)
      base.class_eval do
        [:ssl_required_actions, :ssl_allowed_actions].each do |attribute|
          class_attribute attribute
          if RAILS4
            hide_action :"#{attribute}", :"#{attribute}=", :"#{attribute}?"
          end
        end
      end
    end

    def skip_ssl_requirement(options = {})
      skip_before_action :ensure_proper_protocol, options
    end

    # Specifies that the named actions requires an SSL connection to be performed (which is enforced by ensure_proper_protocol).
    def ssl_required(*actions)
      raise "Empty or :all does not work here ..." if actions.empty? || actions == [:all]
      self.ssl_required_actions = actions
    end

    def ssl_allowed(*actions)
      self.ssl_allowed_actions = actions
    end
  end

  def ssl_environment?
    ENV["ZENDESK_ON_SSL"] || !Rails.env.development?
  end

  def ssl_required?
    return false unless ssl_environment?
    current_account.ssl_should_be_used? || action_requires_ssl?
  end

  def ssl_allowed?
    return false unless ssl_environment?
    allowed_actions = (self.class.ssl_allowed_actions || [])
    (allowed_actions == [:all]) || allowed_actions.include?(action_name.to_sym)
  end

  private

  def action_requires_ssl?
    (self.class.ssl_required_actions || []).include?(action_name.to_sym)
  end

  def ensure_proper_protocol
    return unless current_account
    return if request.ssl? && current_account.help_center_enabled?
    return if ssl_allowed? && !current_account.ssl_should_be_used?

    if ssl_required? && !request.ssl?
      ssl = true
    elsif request.ssl? && !ssl_required? && !api_request?
      logger.info("ensure_proper_protocol redirecting from https->http: #{request.url}")
      ssl = false
    else
      return
    end

    parser = Zendesk::ReturnTo::Parser.new(current_account, current_user, request, params.except(:return_to), flash)
    redirect_to parser.return_to_url(current_account.url(ssl: ssl) + request.fullpath)
  end
end
