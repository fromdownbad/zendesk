# https://github.com/zendesk/help_center/blob/b9e4190/lib/uploader_configuration.rb
# https://github.com/zendesk/help_center/blob/b9e4190/lib/replicated_bucket_configuration.rb

class HCUploaderConfiguration
  class << self
    def storage
      HC_UPLOADER_ENV[:storage]
    end

    def bucket_name
      HC_UPLOADER_ENV[:bucket_name]
    end

    def region
      HC_UPLOADER_ENV[:region]
    end

    def aws_access_key_id
      HC_UPLOADER_ENV[:aws_access_key_id]
    end

    def aws_secret_access_key
      HC_UPLOADER_ENV[:aws_secret_access_key]
    end

    def replicated_bucket_name
      HC_UPLOADER_ENV[:replicated_bucket_name] if defined? HC_UPLOADER_ENV
    end

    def replicated_region
      HC_UPLOADER_ENV[:replicated_region] if defined? HC_UPLOADER_ENV
    end

    def replicated_aws_access_key_id
      HC_UPLOADER_ENV[:replicated_aws_access_key_id] if defined? HC_UPLOADER_ENV
    end

    def replicated_aws_secret_access_key
      HC_UPLOADER_ENV[:replicated_aws_secret_access_key] if defined? HC_UPLOADER_ENV
    end
  end
end
