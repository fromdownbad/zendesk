require 'zendesk_mail'

module FixtureHelper
  module Mail
    def self.read(name)
      json = Yajl::Parser.parse(raw(name), check_utf8: false).with_indifferent_access
      yield(json) if block_given?
      Zendesk::Mail::Serializers::InboundMessageSerializer.load(json)
    end

    def self.raw(name)
      IO.read(path(name))
    end

    def self.path(name)
      File.expand_path(name, "#{Rails.root}/test/files/inbound_mailer")
    end
  end

  module Files
    def read(name)
      IO.read("#{Rails.root}/test/files/#{name}")
    end

    def self.read(name)
      IO.read("#{Rails.root}/test/files/#{name}")
    end

    def self.path(name)
      "#{Rails.root}/test/files/#{name}"
    end
  end

  module Certificate
    def read(name)
      IO.read("#{Rails.root}/test/files/certificate/#{name}")
    end

    def self.read(name)
      IO.read("#{Rails.root}/test/files/certificate/#{name}")
    end
  end
end
