# This module provides "current_tags" getters and setters for models that does
# not have that db column - User, Organization, Forum
module CurrentTagsAccessor
  attr_writer :current_tags

  def current_tags
    return '' if account.present? && !account.has_user_and_organization_tags?

    @current_tags || tag_array.sort.join(' ')
  end
end
