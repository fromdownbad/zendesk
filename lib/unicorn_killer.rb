require 'socket'

module Unicorn::Killer
  MAX_REQUESTS_PER_CHILD = Integer(ENV['UNICORN_MAX_REQUESTS_PER_CHILD'] || 10000)

  MIN_MAX_MEMORY = Integer(ENV['UNICORN_MIN_MAX_MEMORY'] || 850) # Megabytes
  MAX_MAX_MEMORY = Integer(ENV['UNICORN_MAX_MAX_MEMORY'] || 2_000) # Megabytes
  PERCENTAGE_MAX_MEMORY = Float(ENV['UNICORN_PERCENTAGE_MAX_MEMORY'] || 0.7)

  MAX_GC_REQUEST_TIME = Integer(ENV['UNICORN_MAX_GC_REQUEST_TIME'] || 8_000) # Milliseconds

  class << self
    def hostname
      @hostname ||= Socket.gethostname
    end

    def total_memory_in_mb
      @total_memory_in_mb ||= `grep MemTotal /proc/meminfo | awk '{print $2}'`.strip.to_i / 1024
    end

    # Hardcoded for now, will turn into ENV variable
    def number_of_workers
      if hostname.end_with?('.aws1.zdsystest.com')
        16
      else
        40
      end
    end

    def max_memory
      @max_memory ||= [[MIN_MAX_MEMORY, (PERCENTAGE_MAX_MEMORY * total_memory_in_mb) / number_of_workers].max, MAX_MAX_MEMORY].min
    end
  end

  puts "Killing unicorns after #{MAX_REQUESTS_PER_CHILD} requests"
  puts "Killing unicorns when they use more than #{Unicorn::Killer.max_memory}MB of memory"
  puts "Garbage collecting after #{MAX_GC_REQUEST_TIME}ms of request processing time"

  def process_client(client)
    if @requests && @requests > 50
      GC.disable
      @request_time ||= 0
      # originally the call to super(client) took place in one of two branches depending on number of requests
      # which, while simpler to think about, end up generating superficially different exception buckets
      # so now we're storing the request start time in a local variable to help flatten out the stack
      # more details in https://github.com/zendesk/zendesk/pull/30491
      request_start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC, :millisecond)
    end

    super(client)

    if request_start_time
      @request_time += Process.clock_gettime(Process::CLOCK_MONOTONIC, :millisecond) - request_start_time

      if @request_time > MAX_GC_REQUEST_TIME
        stats.increment("runs")
        GC.enable
        time = Benchmark.ms { GC.start }
        stats.timing("time", time)
        @request_time = 0
      end
    end
  ensure
    @requests ||= 0
    @requests += 1
    # Check memory every 250 requests
    @rss       = nil if (@requests % 250).zero?
    too_many_requests = (MAX_REQUESTS_PER_CHILD > 0 && @requests >= MAX_REQUESTS_PER_CHILD)
    too_much_memory = (Unicorn::Killer.max_memory > 0 && rss > Unicorn::Killer.max_memory)

    if too_many_requests || too_much_memory
      reason = if too_many_requests
        'requests'
      else
        'memory'
      end

      stats.increment("kill.#{reason}")
      stats.histogram('kill.total_requests', @requests)

      @rss = nil

      logger.info "Killing unicorn worker with pid #{Process.pid}. Requests: #{@requests}. Memory: #{rss}MB"
      stats.histogram('kill.total_memory', rss)

      kill_worker(:QUIT, Process.pid)
    end
  end

  def rss
    @rss ||= `ps -o rss= -p #{Process.pid}`.to_i / 1024
  end

  def stats
    @stats ||= Zendesk::StatsD::Client.new(namespace: 'oobgc', tags: tags)
  end

  def tags
    ENV['COMPUTE_CELL_NAME'].present? ? ["cell:#{ENV['COMPUTE_CELL_NAME']}"] : []
  end
end

ObjectSpace.each_object(Unicorn::HttpServer) do |s|
  s.extend(Unicorn::Killer)
end
