module ServiceableAccount
  def account_is_serviceable
    if account && !account.is_serviceable?
      # [ZD-208249] Allow account owners to reset passwords even when account is suspended
      unless (self.class.name.casecmp("user").zero? && is_account_owner?) || self.class.name.casecmp("base").zero?
        errors.add(:base, translate_error(self.class.to_s.downcase))
      end
    else
      true
    end
  end

  def translate_error(type)
    if type == 'entry' || type == 'post' || type == 'ticket' || type == 'user'
      # "<type> cannot be created/updated; this Zendesk account has expired"
      I18n.t("txt.admin.lib.zendesk.lib.serviceable_account.#{type}_error_not_update_create")
    else
      I18n.t('txt.admin.lib.zendesk.lib.serviceable_account.class_cannot_be_created', class_name: type)
    end
  end
end
