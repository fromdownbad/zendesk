module SuspendedBuilder
  class << self
    def go(account, count = 1)
      count.times do
        suspend = account.suspended_tickets.new do |s|
          s.subject    = "Subject #{Token.generate(10)}"
          s.from_name  = "First Last"
          s.from_mail  = "someone.#{Token.generate(10)}@kikbou.com"
          s.author     = nil
          s.recipient  = "Huh?"
          s.cause      = SuspensionType.BACKSCATTER
          s.content    = gibberish
          s.message_id = Token.generate(20)
          s.brand      = account.brands.first
        end

        suspend.save!
      end
    end

    private

    def gibberish
      result = ''
      100.times do
        result << Token.generate(1 + Kernel.rand(7))
        result << ' '
      end
      result
    end
  end
end
