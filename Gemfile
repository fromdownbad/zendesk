source 'https://rubygems.org'

CI = ENV['TRAVIS'] || ENV['GHA'] # TODO: remove TRAVIS

unless ENV['SKIP_BUNDLER_RUBY_VERSION_CHECK']
  ruby File.read(File.expand_path('.ruby-version', __dir__)).strip
end

rails4 = !ENV['BUNDLE_GEMFILE'].to_s.include?('rails5')

# rubocop:disable Layout/ExtraSpacing
gem 'bundler', '< 2'

# Used to modify cache path when dual booting
Bundler::Settings.class_eval do
  attr_writer :app_cache_path
end

Bundler.settings.app_cache_path = 'vendor/cache-rails5' unless rails4

# Only gems with preload or database groups are auto-required.
if rails4
  gem 'rails', '4.2.11.3'
  gem 'mysql2', '0.4.10' # https://github.com/zendesk/zendesk/pull/32075
else
  gem 'rails', '5.0.7.2'
  gem 'mysql2'
end
gem 'sass-rails', '~> 5.0.7'
gem 'minitest', '~> 5.10.3'
gem 'tzinfo-data', '>= 1.2019.3'
# load './no_autoload/zendesk/bundler_cache_rails4.rb'

# Gems in the preload group are required when the app loads,
# only use it when something has a railtie or will be loaded on every boot
group :preload do
  if rails4
    gem 'actionpack-xml_parser', '~> 1.0'
    gem 'protected_attributes', '= 1.0.9'
  else
    gem 'actionpack-xml_parser', '~> 2.0'
  end
  gem 'allowed_parameters',   '~> 3.2.0'
  gem 'actionpack-action_caching'
  gem 'actionpack-page_caching'
  gem 'zendesk_types',        '~> 0.12.9'
  gem 'zendesk_redis_store',  '~> 1.0'
  gem 'logcast',              '~> 1.1.1', require: 'logcast/rails'
  gem 'stronger_parameters',  '~> 2.9.1'
  gem 'arturo'
  gem 'i18n'
  gem 'will_paginate',        '~> 3.1.8'
  gem 'dynamic_form'
  gem 'kasket',               '~> 4.6'
  gem 'active_hash',          '>= 1.2.3'
  gem 'cia',                  '>= 0.5.7'
  gem 'rollbar',              '>= 0.13.2'
  gem 'json',                 '~> 2.3'
  gem 'liquid-c',             '>= 4.0.0.rc1', '< 5.0'
  gem 'memflash',             '~> 2.2'
  gem 'property_sets',        '3.5.6'
  gem 'rails4_skip_action_patch'
  gem 'koala',                git: 'https://github.com/zendesk/koala.git', tag: 'v3.0.0-1'
  gem 'twitter',              git: 'https://github.com/zendesk/twitter.git', ref: '201a492'
  gem 'permalink_fu',         '1.0.0'
  gem 'soft_deletion',        '~> 1.0'
  gem 'http_accept_language', '>= 2.0.1'
  gem 'scoped_cache_keys'
  gem 'validates_lengths_from_database', '>= 0.8' # 0.8 includes fixes for bytesize vs. length
  gem 'unicorn',              '~> 5.5'
  gem 'unicorn_wrangler',     '>= 0.5.3'
  gem 'fast_blank'
  gem 'recaptcha',            '~> 4.9', require: 'recaptcha/rails'
  gem 'json-jwt',             '~> 1.9'
  gem 'rack-utf8_sanitizer'
  gem 'equalizer',            '~> 0.0.11'
  gem 'global_uid',           '>= 4.0.0'
  gem 'ledger_client'
  gem 'warden',               '~> 1.2.7'
end

gem 'sprockets-rails', require: 'sprockets/railtie'

# gems without a group are not auto-required
gem 'r509'
gem 'tzinfo'
gem 'aasm',                 '4.12.0'
gem 'abbreviato',           '~> 0.10'
gem 'action_mailer-enqueable', '2.0.0'
gem 'activerecord-import', require: false
gem 'arsi'
gem 'awesome_print'
gem 'aws-sdk-s3',           '~> 1.0'
gem 'aws-sdk-sqs',          '~> 1.0'
gem 'aws-sdk-sns',          '~> 1.0'
gem 'basecamp',             '~> 0.0.12'
gem 'bcrypt',               '>= 3.1.6'
gem 'biz',                  '~> 1.4'
gem 'builder'
gem 'css_parser'
gem 'cloudfiles',           '1.4.16'
gem 'color',                '1.8'
gem 'cld-2018',             '1.1.3' # https://github.com/mdemare/cld2
gem 'dkim',                 '~> 1.0'
gem 'ddtrace',              '~> 0.27'
gem 'credit_card_sanitizer', '0.6.8'
gem 'raw_net_capture',      '~> 2.0.1'
gem 'faraday',              '~> 0.11.0'
gem 'faraday_middleware',   '~> 0.12'
gem 'faraday-restrict-ip-addresses', git: 'https://github.com/zendesk/faraday-restrict-ip-addresses.git', ref: 'v0.2.0'
gem 'hashids',              '1.0.5'
gem 'hashie'
gem 'fastimage',            '2.1.5'
gem 'finite_machine',       '0.12.0'
gem 'friendly_id',          '2.2.7'
gem 'gravatar-ultimate'
gem 'httparty',             '~> 0.10'
gem 'httpclient',           '~> 2.7.1'
gem 'mail',                 '~> 2.6.1'
gem 'mini_magick',          '~> 4.9'
gem 'bootsnap'
gem 'oauth',                '0.4.7'
gem 'oauth2',               '~> 1.0'
gem 'yam',                  '~> 2.4'
gem 'addressable',          '>= 2.4.0'
gem 'pg_query'
gem 'pid_controller',       '~> 0.2'
gem 'predictive_load',      '~> 0.5'
gem 'prop',                 '~> 2.5.0'
gem 'pry'
gem 'rake'
gem 'rdoc',                 '~> 5.0'
gem 'redis',                '~> 3.3.5'
gem 'redis-namespace',      '~> 1.4'
gem 'responds_to_parent',   '~> 2.0.0'
gem 'resque',               '~> 1.27'
gem 'resque-status',        '~> 0.5.0'
gem 'resque-durable',       '~> 2.0'
gem 'resque-lifecycle',     '>= 0.1.2'
gem 'resque-pool',          '0.6.0'
gem 'resque-throttle'
gem 'resque-retry',         '1.5.0'
gem 'resque-scheduler',     '~> 4.0', require: 'resque-scheduler'
gem 'ruby-hmac',            '0.4.0', require: 'hmac'
gem 'rufus-scheduler',      '~> 3.0'
gem 'rails_xss'
gem 'rubyzip',              '~> 1.3.0'
gem 'zip-zip'
gem 'schmobile',            '~> 2.0.0'
gem 'stackprof'
gem 'ticket_sharing',       '~> 2.0.1'
gem 'tinder',               '~> 1.3.1'
gem 'twilio-ruby',          '= 3.16.1'
gem 'useragent',            '~> 0.16.10'
gem 'uuidtools',            '~> 2.1.3'
gem 'vpim',                 '>= 13.11.11'
gem 'xml-simple',           '~> 1.0.12'
gem 'jira4r',               '>= 1.3.0'
gem 'logger-application',   '~> 0.0.2' # jira4r -> soap4r-ng -> implicitly this
gem 'maxminddb',             '~> 0.1.22'
gem 'rack-oauth2',          '~> 1.0'
gem 'rack-cache',           '~> 1.5.0'
gem 'savon'
gem 'simple_access',        '0.1.0'
gem 'unicode',              '>= 0.4.0'
gem 'timecop'
gem 'time_difference'
gem 'hirb'
gem 'i18n_data'
gem 'preload'
gem 'remote_files', '~> 3.1.0'
gem 'fog-aws'
gem 'delta_changes',        '>= 2.1.0'
gem 'adrian',               '>= 2.0.1'
gem 'active_record-comments', '>= 0.1.4'
gem 'secure_headers',       '~> 6.3'
gem 'jwt',                  '~> 1.0'
gem 'asset_host_selection'
gem 'riak-client',          '>= 1.4.4.1'
gem 'zopim_reseller_api_client', '~> 0.0.11', require: 'zopim_reseller_api'
gem 'iso8601', '~> 0.9.1'
gem 'ipaddress'
gem 'premailer'
gem 'syck',                 '>= 1.0.0.4'
gem 'ruby-progressbar'
gem 'inifile' # console_classic
gem 'radar_client_rb',          '~> 2.1.0'
gem 'zendesk_radar_client_rb',  '~> 0.2.6'
gem 'html_to_plain_text',   '>= 1.0.5'
gem 'nokogiri',             '~> 1.10.8', require: false # Update Dockerfile.base when you update this
gem 'samlr',                '2.6.2'
gem 'acme-client', '0.4.0'
gem 'acmev2-client', git: 'https://github.com/zendesk/acme-client.git'
gem 'samson_secret_puller'
gem 'spring', '~> 1.7.2'
gem 'simplediff-ruby', '1.0.0'
gem 'coverband'
gem 'simplecov'
gem 'google-protobuf'
gem 'parallel', '1.12.1'
gem 'raindrops'
gem 'activerecord-delay_touching' if rails4
gem 'json-schema', '~> 2.8'
gem 'iron_bank',   '~> 4.4.3'
gem 'xmlrpc'
gem 'diplomat', '~> 2.0'
gem 'benchmark-ipsa'
gem 'safe_regexp', '~> 0.3.0'
gem 'ffi-icu'
gem 'rqrcode', '~> 0.10'
gem 'webrick', '~> 1.5.0'
gem 'racecar', '~> 1.0.1'
gem 'snappy', '~> 0.0.15' # Compression codec for ruby-kafka, pulled in by racecar
gem 'aws-sdk-ec2', '~> 1', '<= 1.41.0', group: :deployment
gem 'delivery_boy', '~> 1.0'

gem 'dogstatsd-ruby', '4.0.0'

gem 'octokit', group: :deployment

# template - copy paste, don't remove
# gem 'zendesk_xyz', git: 'https://github.com/zendesk/zendesk_xyz.git', ref: 'abcdef'

platforms :ruby do
  gem 'fast_xs',            '~> 0.8.0'
  gem 'yajl-ruby',          '>= 1.2.0'
end

# auto required
group :database do
  gem 'activerecord', require: 'active_record'
  gem 'active_record_inherit_assoc', '~> 2.10'
end

# auto required
group :memcache do
  # bring both clients as there are no namespace collision
  # between them, to activate use ENV['DALLI'] in Samson
  gem 'dalli'
  gem 'dalli-elasticache'
  gem 'memcached',          '>= 1.8.0'
  gem 'libmemcached_store', '>= 0.10.0'
end

group :development, :test do
  gem 'byebug'
  gem 'pry-byebug'
end

group :development, :master, :staging do
  gem 'memory_profiler'
end

group :test do
  gem 'rails-forward_compatible_controller_tests', require: false if rails4
  if rails4
    gem 'minitest-rails', '~> 2.2'
  else
    gem 'minitest-rails', '~> 5.0'
    gem 'rails-controller-testing'
  end
  gem 'testrbl'
  gem 'hashdiff'
  gem 'factory_bot', '~> 4.8'
  gem 'webmock', '~> 3.3.0'
  gem 'shoulda-matchers', '~> 4.0'
  gem 'shoulda-change_matchers'
  gem 'webrat', '0.7.0'
  gem 'mocha'
  gem 'bourne', '~> 1.6' #  https://github.com/thoughtbot/bourne/pull/32
  gem 'nori'
  gem 'minitest-rg'
  gem 'minitest-rerun'
  gem 'fakeredis'
  gem 'single_cov'
  gem 'github_api', '~> 0.18.0'
  if rails4
    gem 'test_after_commit', '~> 1.1.0'
  end
  gem 'mysql_isolated_server', '~> 0.0.3'
  gem 'brakeman'
  gem 'code_owners'
  gem 'vcr', '> 3.0.0'
  gem 'rubocop', '~> 0.88.0'
  gem 'rubocop-minitest', '~> 0.2.1'
  gem 'rubocop-performance', '~> 1.5.2'
  gem 'rubocop-rails', '~> 2.5'
  gem 'minitest-around'
  gem 'kucodiff'
  gem 'zendesk_i18n_dev_tools'
  gem 'database_cleaner'

  # gem "ruby-prof", "~> 0.13"
end

group :development do
  gem 'foreman'
  gem 'forking_test_runner'
  gem 'test_benchmark', '~> 0.4'
  gem 'ci_reporter', '~> 1.6.5'
  gem 's3_meta_sync', '~> 0.14.0'
  gem 'pry-remote'
  gem 'sass'
  gem 'closure-compiler'
  gem 'yui-compressor'
  gem 'pre-commit'
  # for IDE based debugging in Eclipse/NetBeans (commented out because it breaks the regular debugger)
  # gem "ruby-debug-ide", "~> 0.4.16", platforms: :ruby_19
  # gem 'mailcatcher'
end

###############################################################################
#                               Private Gems                                  #
#-----------------------------------------------------------------------------#
# Testing gems: use git with ssh e.g. "git@github.com:zendesk/foo.git" and    #
# Ignore the pre-commit warning and 1 failing Travis test.                    #
###############################################################################

source 'https://zdrepo.jfrog.io/zdrepo/api/gems/gems-local/' do
  # Gems in the preload group are required when the app loads,
  # only use it when something has a railtie or will be loaded on every boot
  group :preload do
    gem 'zendesk_billing_core',         '0.2.45'
    gem 'zendesk_business_hours',       '53'
    gem 'zendesk_api_dashboard',        '1.2.4'
    gem 'zendesk_configuration',        '~> 1.33'
    gem 'zendesk_core',                 '~> 7.4.0'
    gem 'zendesk_exceptions',           '~> 1.0'
    gem 'zendesk_core_application',     '2.3.0'
    gem 'zendesk_cross_origin',         '0.10.0'
    gem 'zendesk_core_security',        '0.1.3'
    gem 'zendesk_encryptable',          '~> 1.1.1'
    gem 'zendesk_health_check',         '~> 1.9'
    gem 'zendesk_histogram',            '0.0.2'
    gem 'zendesk_text',                 '~> 1.7.2'
    gem 'zendesk_oauth',                '= 7.4.0'
    gem 'zendesk_auth',                 '5.22.0'
    gem 'zendesk_statsd',               '~> 0.4', '>= 0.4.12'
    gem 'zendesk_jobs',                 '~> 2.2.2'
    gem 'zendesk_ip_tools',             '>= 0.5.0'
    gem 'zendesk_voice_core',           '~> 3.0.3'
    gem 'zendesk_i18n',                 '~> 3.0'
    gem 'zendesk_channels',             '1.373.0'
    gem 'zendesk_outgoing_mail',        '~> 2.4.10'
    gem 'zendesk_cldr',                 '~> 0.2.1'
    gem 'zendesk_mobile_deeplink',      '~> 1.5'
    gem 'zendesk-rinku',                '~>1.7'
    gem 'kragle',                       '~> 0.36.0'
    gem 'zendesk_cursor_pagination',    '~> 3.8'
  end

  # Gems without a group are not auto-required
  gem 'api_presentation',          '~> 2.8.3'
  gem 'zendesk_api_controller',    '~> 3.2'
  gem 'attr_selector'
  gem 'zendesk_deployment',        '~> 4.2', group: :deployment
  gem 'zendesk_mail',              '~> 3.22.0'
  gem 'zendesk_mtc_mpq_migration', '~> 0.9.14'
  gem 'zendesk_reply_parser',      '~> 0.7.17'
  gem 'zendesk_shared_session',    '~> 2.15.0'
  gem 'zendesk_logging',           '~> 0.6'
  gem 'zendesk_search',            '~> 2.6.3'
  gem 'zendesk_archive',           '~> 7.0'
  gem 'zendesk_feature_framework', '~> 0.4.0'
  gem 'zendesk_rules',             '4.15.0'
  gem 'zendesk-attachment_fu',     '~> 6.0'

  gem 'radar_notification'
  gem 'pigeon_client',          '~> 0.1.2'
  gem 'gooddata_client',        '~> 5.6'
  gem 'occam_client',           '~> 0.17'
  gem 'poddable_client'
  gem 'guide_plans',            '~> 1.0'
  gem 'nilsimsa_lsh',           '~> 0.3.1'
  gem 'johnny_five',            '~> 0.9', '>= 0.9.3'

  # Zendesk statistics engine:
  gem 'help_center_stats',             '1.0.4'
  gem 'zendesk_classic_stats',         '~> 1.0'
  gem 'zendesk_stats',                 '~> 4.1.0'

  gem 'zendesk_system_users'
  gem 'zendesk_internal_api_client',   '~> 3.3.1'
  gem 'zendesk_redis_factory',         '>= 2.1.1'
  gem 'zendesk_voyager_api_client'
  gem 'zendesk_comment_markup',        '~> 2.1'
  gem 'zendesk_apm'
  gem 'zendesk_protobuf_clients',      '6.276'

  gem 'zendesk_i18n_interpolation_old_style', '>= 2.0.0'
  gem 'zendesk_i18n_interpolation_sanitize'
  gem 'zendesk_i18n_local_backend', '>= 1.0.1'
  gem 'zendesk_i18n_rosetta_backend'

  # Should be replace with ICU functionality.
  gem 'zendesk_deprecated_time_formatting_extension', '>= 1.0.1'

  # Auto required:
  group :database do
    gem 'zendesk_database_migrations'
    gem 'zendesk_database_support'
    gem 'active_record_host_pool',     '< 0.12'
  end

  group :development do
    gem 'changes_since', '~> 0.9.2'
  end

  gem 'sim_zen', groups: %i[development staging], require: false
end
# rubocop:enable Layout/ExtraSpacing

require_relative 'no_autoload/zendesk/bundler_ext_built'
require_relative 'no_autoload/zendesk/bundler_hard_retry' if CI
require_relative 'no_autoload/zendesk/bundler_realpath'
