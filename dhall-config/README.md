# Classic + Dhall

Before getting too far into this code, a basic understanding of Dhall is required. A quick resource
for getting started is [Learn Dhall in Y minutes](https://learnxinyminutes.com/docs/dhall/).

## Why Dhall?

There are many reasons Dhall is being used, and those are all well explained in two resources:

1. The [ADR for Dhall in Support](https://techmenu.zende.sk/adrs/use-dhall-for-configuration-in-support/)
1. The [Dhall landing page](https://dhall-lang.org/)

## Terminology

### `dependency`

When using this term to describe Dhall code, we specifically mean "remote imports".

## Directory Structure

There are a number of directories that have specific meanings.

### `dependencies`

This directory contains all of the remote imports. Specifically, this is where the versioned imports
of the tools, libraries, and configuration we consume are declared. This directory uses frozen
and cached imports, to ensure the version, code, or config never changes out from underneath us.

### `generated`

This is a helper directory for `dhall-render`. Generated documentation will show up here. Symlinks to these files will appear in `dhall-config` as a shortcut.

### `tests`

Integration tests. Unit tests are included within the same files as the source code so that they
run during compilation.

There's only one integration test at the moment, a check with the Samson environment. This will
go away once we're fully on Spinnaker. Until then, it's an easy way for maintainers to determine if
there is config drift before flipping the switch.

Run the test like so:

```
bundle exec ruby tests/manifest_diff.rb
```

## Environment variables

**STOP: are you adding a sensitive value? An access token, private key, or password? See the [secrets section](#secrets) instead!**

### Video tutorials

Check out this [google drive folder](https://drive.google.com/drive/u/0/folders/1txnNbKg7bMyvSWH9qiTVihZJbkbdDQ-b) for short 1-2 minute videos demonstrating everything below!

To add, update, or remove an environment variable, you must first decide:

1. Is this variable only needed in Classic? Add it to [this repo](#classic-only)!
2. Is this variable needed in other services? This is considered ["shared config"](#shared-config).

### Classic-only

#### Uniform

> If your environment variable is going to have the same values in all environments:
> development, test, ci, staging, and production, consider whether or not it could be
> a ruby constant instead.

If your environment variable is uniform in staging and production, add it in a record
under the `uniform` field in your team's `env-vars/<team-name>.dhall` file.

```diff
in  { uniform =
        { SOME_OTHER_VAR = "some value"
+       , MY_NEW_VAR = "my new value"
        }
    }
```

Then, add it to the expected type since we use this for documentation:

```diff
let EnvVars =
    { SOME_OTHER_VAR : Text
+   , MY_NEW_VAR : Text
    }
```

#### Environment specific

If your environment variable differs between staging and production, add it to records
under the `staging` and `production` fields in your team's `env-vars/<team-name>.dhall` file.

**Note**: you must add your variable to both fields or the new config will not compile!

```diff
in  { staging =
        { SOME_OTHER_VAR = "some value"
+       , MY_NEW_VAR = "my staging value"
        }
    , production =
        { SOME_OTHER_VAR = "some value"
+       , MY_NEW_VAR = "my production value"
        }
    }
```

Then, add it to the expected type since we use this for documentation:

```diff
let EnvVars =
    { SOME_OTHER_VAR : Text
+   , MY_NEW_VAR : Text
    }
```

#### Pod specific

Get to know the `Pod.Type`! It's a [simple enum](https://github.com/zendesk/zendesk-dhall/blob/master/Pod/Type.dhall) of all Zendesk PODs (Point Of Delivery).

We recommend using out-of-the-box functions in the [`Pod` package](https://github.com/zendesk/zendesk-dhall/tree/master/Pod#pod) that handle the most common Text transformations.

However, you can always exhaustively match on the enumeration to return [custom, pod-specific
data](https://github.com/zendesk/zendesk-dhall/blob/e17cf7a168ce70ec0371410c2262ee1e72ef72f9/Pod/isProduction.dhall#L3-L20) with the [`merge` keyword](https://docs.dhall-lang.org/tutorials/Language-Tour.html#unions).

If your environment variable differs between PODs, add it to a record under the `perPod`
field in your team's `env-vars/<team-name>.dhall` file.

**Note:** the `perPod` field expects a function that takes one argument, `Pod.Type`, and returns
a record of your environment variables.

```diff
  , perPod = \(pod : Pod.Type) ->
      { SOME_OTHER_VAR = "some-${Pod.name pod}-value"
+     , MY_NEW_VAR = "my region-specific value pod-${Pod.regionName (Pod.Region.fromPod pod))}"
      }
```

Then, add it to the expected type

```diff
let EnvVars =
    { SOME_OTHER_VAR : Text
+   , MY_NEW_VAR : Text
    }
```

### Final Step

If you've **added a new field** (`staging`, `production`, `uniform`, `perPod`) **or file** (`teams/your-new-team.dhall`), you'll need to hook it up to the dhall code that
merges all these together. This step is also relevant when you've done the opposite - **removing a field or file**.

Open up `./env-vars/local.dhall` and find your team. Example:

```dhall
let Squonk = ./teams/squonk.dhall
```

If you don't see it, add your team to the file:

```diff
+ let YourTeam = ./teams/<your-team>.dhall
```

Now, all your env variables are in scope, but not yet used. To combine them with other teams you'll need to change up to three pieces of code -

1. Environment-specific merging - if you've added or removed the fields, `production`, `staging`, or `uniform`, make sure these values are being queried and combined:

```diff
let varsByEnvironment =
          Harrier.{ production, staging }
      /\  Ocean.{ production, staging, uniform }
      /\  Squonk.{ production, staging, uniform }
+     /\  YourTeam.{ production, staging, uniform }
```

This code is grabbing values from fields specified in the `.{ ... }` selector and recursively merging values with the [`/\` operator](https://docs.dhall-lang.org/references/Built-in-types.html#id72).

2. Pod-specific merging - if you've added or removed the field: `perPod`, make sure these values are being queried and combined:

```diff
let varsByPod =
      \(pod : Pod.Type) ->
        Harrier.perPod pod
     /\ Squonk.perPod pod
+    /\ YourTeam.perPod pod
```

This code is grabbing values from the `perPod` field recursively merging values with the [`/\` operator](https://docs.dhall-lang.org/references/Built-in-types.html#id72).

**Note:** you must provide `pod` after `YourTeam.perPod`, since `perPod` contains a function!

3. Merge the types!

```diff
let LocalEnvVars =
          Harrier.EnvVars
      //\\  Ocean.EnvVars
      //\\  Squonk.EnvVars
+     //\\  YourTeam.EnvVars
```

We use the [`//\\` operator](https://docs.dhall-lang.org/references/Built-in-types.html#id70) to recursively merge types.

**Note:** You only need to do this when you've created or deleted a team's file.

### Shared config

Our shared config currently lives in the `config-service-data` repository.

At the moment, if you add some JSON representing environment variables, you'll get Dhall code
that you can reference in other repositories.

> We go from JSON => Dhall in that repository because Dhall users are sharing that repository
> with Jinja, Handlebars, Jsonnet, and other users.

If your other services don't use Dhall, that's fine! You can fetch the relevant JSON
in `config-service-data` via any programming language or use the Productivity Config team's
Jsonnet packaging solution.

With Dhall, we can import Dhall expressions locally or from anywhere on the internet:

```dhall
let myRemoteImport = https://example.com/fake/example.dhall
```

You'll find all of these in our `dependencies` directory, with some minor additions
to optimize performance with caching.

#### Environment variable groups

Currently, all environment variables are organized into groups like they were in Samson.
This may change in the future, but until then, we'll detail creating, updating, and removing
these groups.

##### Updating config-service-data dependency

Create, update, or delete a JSON file in `data/shared_env_groups/<name_of_group>.json`. Also, update the codeownership of the changed files if needed in `.github/CODEOWNERS`. Commit and push. You'll see another commit pushed shortly after yours: you can now test this locally by running the bump script:

`./maintenance/bump env-var-groups <latest-sha-in-my-branch>`

##### Update existing group

You're done :) Commit, push, and make a PR!

##### New group

Let's add our new group to the code:

```diff
let envVarsByPod =
      envVarGroups.AccountService
+  /\ envVarGroups.MyNewCamelCaseGroup
   /\ ...
```

Since we type-check and test this, we'll also need to add the corresponding type

```diff
let AllGroups =
      groups.AccountService.Type
+  /\ groups.MyNewCamelCaseGroup.Type
   /\ ...
```

##### Remove group

Let's remove our group from the code:

```diff
let envVarsByPod =
      envVarGroups.AccountService
-  /\ envVarGroups.MyCamelCaseGroup
   /\ ...
```

Since we type-check and test this, we'll also need to remove the corresponding type

```diff
let AllGroups =
      groups.AccountService.Type
-  /\ groups.MyCamelCaseGroup.Type
   /\ ...
```

##### Overrides

Is there a value you've specified in `config-service-data` that is correct for other services but not for Classic? You can easily override this by adding your override in `./env-vars/shared.dhall`

```diff
let overrides =
      \(pod : Pod.Type) ->
        let podName = Pod.name pod

        in  { ZENDESK_OCCAM_K8S_URL = Some
                "http://occam.${podName}.svc.cluster.local.:80"
            , AUTH_SERVICE_URL = Some "http://auth-service.doorman:80"
            ...
+           , MY_SHARED_ENV_VAR = Some "my-classic-override-value"
              --                  ^ env var groups are inconsistently filled out per-pod.
              --                    some pods are missing the same env var. So, we need to
              --                    make the values `Optional`. We use `Some` to say there
              --                    is a value here, `None` would say "there is no value here"
            }
```

## Secrets

**WARNING:** the `secret://` convention you may remember using in Samson is deprecated.
We have moved the computation of the Vault secret path from server-side ruby to client-side dhall.

### Background

A secret is a value that is sensitive and should not show up in source code or production
logs. We store secrets in a technology called [Vault](https://www.vaultproject.io/). It's
essentially a secure key-value store with a file path convention for its keys. Example:

```JSON
{ "staging/my-project/podName/secret-name": "SENSITIVE_VALUE_HERE" }
```

The code in this repo and our dependency, [samson_secret_puller](https://github.com/zendesk/samson_secret_puller) handle the logic for fetching these secrets from vault and storing them in files before booting Classic in kubernetes.

### Video tutorials

Check out this [google drive folder](https://drive.google.com/drive/u/0/folders/1txnNbKg7bMyvSWH9qiTVihZJbkbdDQ-b) for short 1-2 minute videos demonstrating everything below!

### Change secrets

Example of a fake team's secret file: `./secrets/teams/fake-team.dhall`

```dhall
let Vault = ../vault.dhall -- a module with Vault conventions

let Environment = Vault.Environment -- environments we've configured for vault

let Scope = Vault.Scope -- should this be a project specific secret or re-used?

let Secrets = -- PascalCase for type
      { some_secret : Text
      , SOME_OTHER_SECRET : Text
      }

let secrets = -- camelCase for value
      \(env : Environment) ->
        { some_secret = -- you can name your secret whatever you want, in any casing you want
            Vault.path env Scope.Project "some_secret" -- example of a project level secret
            -- the above secret will be scoped to staging and production "automatically"
            -- since you're calling `Vault.path` with the `env` this function is passed elsewhere
        , SOME_OTHER_SECRET = Vault.path env Scope.Global "some_other_secret"
          -- ^ example of globally scoped secret. prefer project level secrets when possible
        }

in  secrets
```

Let's add a new secret!

```diff
...
let secrets =
    \(env : Environment) ->
        { some_secret = Vault.path env Scope.Project "some_secret"
        , SOME_OTHER_SECRET = Vault.path env Scope.Global "some_other_secret"
+       , new_stuff = Vault.path env Scope.Project "new_stuff"
        }
...
```

And then add it to the corresponding type:

```diff
...
let Secrets = -- PascalCase for type
      { some_secret : Text
      , SOME_OTHER_SECRET : Text
+     , new_stuff : Text
      }
...
```

### Expose secrets

Is this a whole new file?

In `./secrets/package.dhall`, please hook up your secrets and your Secrets type.

```diff
...
+ let YourTeam = ./teams/your-team.dhall -- import your team's secrets
...

let Secrets =
      Billing.Secrets
  //\\ Harrier.Secrets
  ...
+ //\\ YourTeam.Secrets -- merge your secret types

let forVault
    : Vault.Environment -> Secrets
    = \(env : Vault.Environment) ->
           Billing.secrets env
        /\ Harrier.secrets  env
        ...
+       //\\ YourTeam.secrets env -- merge your secrets and apply the staging/prod env
```

**Note** If you're removing a file, you'll need to change all the same lines, but simply
delete instead of add.

### Test secrets

In `./secrets/package.dhall`, add your expected Vault path to the test!

```diff
let _stagingSecrets
    : Secrets
    = { MYSQL_USER = "staging/global/global/classic_mysql_user"
      , MYSQL_PASSWORD = "staging/global/global/classic_mysql_password"
      ...
+     , new_stuff = "staging/zendesk/global/new_stuff"
```

You're done!
