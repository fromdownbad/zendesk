{-
Use dhall-render and build up a list of all files that need to be generated.

TODO: explain this further
-}
let Render = ../dhall-config/dependencies/render.dhall

let Prelude = ../dhall-config/dependencies/prelude.dhall

let Pod = (../dhall-config/dependencies/zendesk-dhall.dhall).Pod

let envVarsCsv = ./documentation/env-vars.dhall

let roleScalingCsv = ./documentation/role-scaling.dhall

let secretsCsv = ./documentation/secrets.dhall

in  { options = Render.Options::{ destination = "./generated" }
    , files =
      { dhall-render =
          Render.SelfInstall.makeExe
            Render.SelfInstall::{ path = "files.dhall" }
      , `env-vars.csv` = Render.TextFile::{
        , headerLines = [] : List Text
        , contents = envVarsCsv
        }
      , `role-scaling.csv` = Render.TextFile::{
        , headerLines = [] : List Text
        , contents = roleScalingCsv
        }
      , `secrets.csv` = Render.TextFile::{
        , headerLines = [] : List Text
        , contents = secretsCsv
        }
      }
    }
