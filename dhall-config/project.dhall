let Project = (./dependencies/zendesk-dhall.dhall).Project

in  Project::{
    , name = "classic"
    , description = Some "The main monolith that serves the Support product."
    , team = "squonk"
    , startedOn = "09-07-2008"
    , version = ./version.dhall
    }
