let Prelude = ../dependencies/prelude.dhall

let Map = Prelude.Map

let zendesk = ../dependencies/zendesk-dhall.dhall

let Pod = zendesk.Pod

let ProjectScope = ./scope/project.dhall

let SharedScope = ./scope/shared.dhall

let compact = Prelude.Map.unpackOptionals Text Text

let AllEnvVars = ProjectScope.EnvVars //\\ SharedScope.EnvVars

let EnvVarsMap = Map.Type Text Text

let asRecord
    : Pod.Type -> AllEnvVars
    = \(pod : Pod.Type) -> ProjectScope.fromPod pod /\ SharedScope.fromPod pod

let asMap
    : Pod.Type -> EnvVarsMap
    = \(pod : Pod.Type) ->
        let all = asRecord pod

        in  compact (toMap all.(SharedScope.EnvVars)) # toMap all.(ProjectScope.EnvVars)

in  { Type = EnvVarsMap, Entry = Map.Entry Text Text, asMap, asRecord }
