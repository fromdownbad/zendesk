let zendesk = ../../dependencies/zendesk-dhall.dhall

let Pod = zendesk.Pod

let EnvVars =
      { DD_RUM_ENABLED : Text
      , DD_RUM_SITE : Text
      , ZENDESK_STATIC_ASSET_DOMAIN : Text
      , ZENDESK_FALLBACK_STATIC_ASSETS_DOMAIN : Text
      }

in  { EnvVars
    , staging =
      { DD_RUM_ENABLED = "true"
      , ZENDESK_FALLBACK_STATIC_ASSETS_DOMAIN =
          "https://static-staging-fallback.zdassets.com"
      , ZENDESK_STATIC_ASSET_DOMAIN = "https://static-staging.zdassets.com"
      }
    , production =
      { DD_RUM_ENABLED = "false"
      , ZENDESK_FALLBACK_STATIC_ASSETS_DOMAIN =
          "https://static-fallback.zdassets.com"
      , ZENDESK_STATIC_ASSET_DOMAIN = "https://static.zdassets.com"
      }
    , perPod =
        \(pod : Pod.Type) ->
          let region = Pod.Region.fromPod pod

          let inEU =
                merge
                  { APNortheast1 = False
                  , APSoutheast2 = False
                  , EUCentral1 = True
                  , EUWest1 = True
                  , USWest1 = False
                  , USWest2 = False
                  , USEast1 = False
                  , USEast2 = False
                  }
                  region

          in  { DD_RUM_SITE = "datadoghq.${if inEU then "eu" else "com"}" }
    }
