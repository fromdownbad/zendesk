let zendesk = ../../dependencies/zendesk-dhall.dhall

let Pod = zendesk.Pod

let Environment = zendesk.Environment

let EnvVars =
      { ARCHIVER_RSS_LIMIT : Text
      , RIAK_BUCKET : Text
      , RIAK_CONSUL_FQDN : Text
      , RIAK_ELB_HOST : Text
      , RIAK_READ_TIMEOUT : Text
      , RIAK_PB_PORT : Text
      , TICKET_DYNAMODB_THREADS : Text
      , TICKET_DYNAMODB_ENABLED : Text
      }

in  { EnvVars
    , staging =
      { ARCHIVER_RSS_LIMIT = "8589934592"
      , TICKET_DYNAMODB_ENABLED = "true"
      , TICKET_DYNAMODB_THREADS = "8"
      }
    , production =
      { ARCHIVER_RSS_LIMIT = "4294967296"
      , TICKET_DYNAMODB_ENABLED = "false"
      , TICKET_DYNAMODB_THREADS = "8"
      }
    , uniform = { RIAK_READ_TIMEOUT = "5", RIAK_PB_PORT = "8087" }
    , perPod =
        \(pod : Pod.Type) ->
          let env = Environment.fromPod pod

          let podName = Pod.name pod

          let region = Pod.Region.fromPod pod

          let shortRegionName = Pod.shortRegionName region

          let domain = "${shortRegionName}.${Environment.internalHost env}"

          let host = "${podName}.${domain}."

          in  { RIAK_BUCKET = "tickets_staging_${podName}"
              , RIAK_CONSUL_FQDN = "riak-elb.${host}"
              , RIAK_ELB_HOST = "riak-elb.${host}"
              }
    }
