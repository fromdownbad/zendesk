let Prelude = ../../dependencies/prelude.dhall

let zendesk = ../../dependencies/zendesk-dhall.dhall

let Pod = zendesk.Pod

let Environment = zendesk.Environment

let EnvVars =
      { ZENDESK_GOOGLE_REDIRECT_URI : Text
      , ZENDESK_OFFICE_365_CLIENT_ID : Text
      , ZENDESK_OFFICE_365_REDIRECT_URI : Text
      , REPLICATED_UPLOADER_BUCKET_NAME : Text
      , UPLOADER_BUCKET_NAME : Text
      , K8S_TEMP_AUTH_LABEL : Text
      , DATA_DELETION_RUNNER_MAX_ACTIVE_DELETIONS : Text
      , FRAUD_SAFELIST_IP_RANGES : Text
      , SOCIAL_MESSAGING_ZAM_APP_ID : Text
      , TICKET_ANONYMIZE_SEARCH_TICKETS_TO : Text
      , ZENDESK_IMAGE_PROXY_HOST : Text
      , ZENDESK_PRECREATE_ACCOUNT_POOL_SIZE : Text
      , ZENDESK_SAAS_PULSE_ID : Text
      , ACCOUNT_CLIENT_ACCOUNT_CACHE_EXPIRY_SECONDS : Text
      , ACCOUNT_SCOPED_ATTACHMENTS : Text
      , ACME_CERTIFICATE_EARLY_RENEWAL_DAYS : Text
      , ARCHIVER_PRIORITY_ACCOUNTS : Text
      , AUTOMATIONS_JOB_TIMEOUT_MINUTES : Text
      , BOOTSNAP_CACHE_DIR : Text
      , DATABASE_BACKOFF_AURORA_CPU_KD : Text
      , DATABASE_BACKOFF_AURORA_CPU_KI : Text
      , DATABASE_BACKOFF_AURORA_CPU_KP : Text
      , DBCONFIG_PROXYSQL_CLUSTER_IP : Text
      , ENABLE_ZENDESK_APM : Text
      , FACEBOOK_EXTEND_QUERY_MONTHS : Text
      , GDPR_CONFIG_REGION : Text
      , HTTP_TARGETS_MAX_ERR_RETRIES : Text
      , KASKET_EXPIRES_IN_DEFAULT : Text
      , KASKET_USE_ELASTICACHE : Text
      , KUBERNETES_ALLOW_FAILED_PERCENT : Text
      , KUBERNETES_ALLOW_NOT_READY_PERCENT : Text
      , LOAD_RADAR_FROM_YAML : Text
      , LOG_LEVEL : Text
      , MAX_GDPR_REPUBLISH_PER_DAY : Text
      , MAX_TICKETS_PER_ANONYMOUS_USER : Text
      , MYSQL_WAIT_TIMEOUT : Text
      , PERMISSIONS_AGENT_URL : Text
      , PRODUCT_CACHE_EXPIRY_SECONDS : Text
      , PROP_USE_ELASTICACHE : Text
      , PROXYSQL_VERSION : Text
      , RADAR_URL_COUNT : Text
      , REDIS_STORE_USE_CONSUL : Text
      , ROLLBAR_ENABLED : Text
      , SOCIAL_MESSAGING_CFC_APP_ID : Text
      , STATS_REDIS_USE_SSL : Text
      , TICKET_ANONYMIZE_SEARCH_TICKETS_FROM : Text
      , UNICORN_TCP_PORT : Text
      , UNICORN_WORKERS : Text
      , UNICORN_WRANGLER_MAX_MEMORY : Text
      , USE_BOOTSNAP : Text
      , USE_DALLI_ELASTICACHE : Text
      , USE_KUBE_CONSOLE : Text
      , ZENDESK_CONFIG_FROM_ENV : Text
      , ZENDESK_LOG_LEVEL : Text
      , ZENDESK_STATS_REDIS_HOST : Text
      , ZENDESK_SYSTEM_GLOBAL_CLIENTS_GOODDATA : Text
      , ZENDESK_SYSTEM_GLOBAL_CLIENTS_ZOPIM : Text
      , NETWORKS_CONFIG_PATH : Text
      , PROXYSQL_ACCOUNT_MASTER_ENABLED : Text
      , PROXYSQL_SHARD_MASTER_ENABLED : Text
      , INLINE_INCIDENT_PROCESSING_LIMIT : Text
      , REDUCED_OUTGOING_CONVERSIONS_EXPIRY : Text
      , ZENDESK_AWS_REGION : Text
      , DALLI_ELASTICACHE_ENDPOINT : Text
      , KASKET_ELASTICACHE_ENDPOINT : Text
      , PROP_ELASTICACHE_ENDPOINT : Text
      , HYBRID_COOKIES_ENABLED : Text
      , APP_SERVER_HPA_MAX_REPLICAS : Text
      , APP_SERVER_HPA_MIN_REPLICAS : Text
      , TWITTER_PROFILE_SYNC_JOB_TIME : Text
      , ZENDESK_PODS_998_REGION : Text
      , ZENDESK_PODS_999_REGION : Text
      , BILLING_ZUORA_HPM_SIMPLE_3D_SECURE_ID : Text
      , BILLING_ZUORA_HPM_V2_3D_SECURE_ID : Text
      , PRIORITY_APP_SERVER_HPA_MAX_REPLICAS : Text
      , PRIORITY_APP_SERVER_HPA_MIN_REPLICAS : Text
      , SES_REGIONS : Text
      , ZENDESK_POD_ATTRIBUTES_ORIGIN : Text
      , TARGET_CIRCUIT_BREAKER_FAILURE_THRESHOLD : Text
      , TARGET_CIRCUIT_BREAKER_FAILURE_TTL : Text
      , TARGET_CIRCUIT_BREAKER_RESET_TIMEOUT : Text
      , TARGET_CREDENTIALS_ENCRYPTION_CIPHER_NAME : Text
      , TARGET_CREDENTIALS_ENCRYPTION_KEY : Text
      , ZENDESK_TWO_FACTOR_AUTH_TWILIO_ACCOUNT_SID : Text
      , ZENDESK_TWO_FACTOR_AUTH_TWILIO_NUMBERS : Text
      , UPLOADER_REGION : Text
      , ZENDESK_AWS_POD_IDS : Text
      , ZOPIM_SCRIBE_API_BASE_URL : Text
      , HPA_METRICS_KUBE_CLUSTER : Text
      , HPA_METRICS_KUBE_NAMESPACE : Text
      , HPA_TARGET_ACTIVE_UNICORNS : Text
      , HPA_TARGET_CPU : Text
      , CONSUL_HTTP_ADDR : Text
      , ZENDESK_GEO_LOCATION_DOWNLOAD_URL : Text
      , ZENDESK_GOOGLE_AUTHORIZE_URL : Text
      , ZENDESK_GOOGLE_SITE : Text
      , ZENDESK_GOOGLE_TOKEN_URL : Text
      , ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_CIPHER_NAME : Text
      , ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY_NAME : Text
      , ZENDESK_SCREENR_API_TIMEOUT : Text
      , ZENDESK_SCREENR_BASE_URI : Text
      , ZENDESK_SCREENR_USERNAME : Text
      , REPLICATED_UPLOADER_REGION : Text
      , UPLOADER_STORAGE : Text
      , ZENDESK_ZENDESK_VOYAGER_PORT : Text
      , ZENDESK_ZENDESK_VOYAGER_TIMEOUT : Text
      , ZOPIM_SCRIBE_API_PORT : Text
      , DBCONFIG_WEIGHTED_DNS_RECORD : Text
      , DEPLOYMENT_MAX_SURGE : Text
      , ENABLE_QUERY_KILLER : Text
      , MYSQL_CONNECT_TIMEOUT : Text
      , UNICORN_WORKERS_PER_K8S_POD : Text
      , UNICORN_WRANGLER_CHECK_MEM_EVERY_REQ : Text
      , UNICORN_WRANGLER_NO_OOBGC : Text
      , WHATSAPP_SMOOCH_INTEGRATION_ID : Text
      , EXPLORE_BOOTSTRAP_KINESIS_STREAM : Text
      , PROXYSQL_ENABLED_CLUSTERS : Text
      , UNICORN_MIN_MAX_MEMORY : Text
      , ZENDESK_CLASSIC_AWS_SNS_REGION : Text
      , ZENDESK_TWITTER_CACHE_S3_BUCKET : Text
      , ZENDESK_TWITTER_CACHE_S3_REGION : Text
      , STATS_REDIS_CLUSTER : Text
      , ZENDESK_POD : Text
      , ZENDESK_PIGEON_URL : Text
      , REDIS_CLUSTER : Text
      , ZENDESK_ZENDESK_VOYAGER_HOST : Text
      , CUSTOM_RESOURCES_URL : Text
      , TWITTER_PROXY_URL : Text
      , PERMISSIONS_SERVICE_URL : Text
      , DATADOG_KUBE_CLUSTER : Text
      , ENABLE_COVERBAND : Text
      , UNICORN_MAX_GC_REQUEST_TIME : Text
      , UNICORN_MAX_MAX_MEMORY : Text
      , UNICORN_MAX_REQUESTS_PER_CHILD : Text
      , UNICORN_PERCENTAGE_MAX_MEMORY : Text
      , ENABLE_MIDDLEWARE_TRACING : Text
      , USE_ATTACHMENTS_CONFIGMAP : Text
      , USER_LEAKY_BUCKET_INTERVAL : Text
      , USER_LEAKY_BUCKET_SIZE : Text
      , USER_LEAKY_BUCKET_THRESHOLD : Text
      , USER_LIMIT_INTERVAL : Text
      , USER_LIMIT_THRESHOLD : Text
      , ZENDESK_DEFAULT_CLOUD_STORES_APAC : Text
      , ZENDESK_DEFAULT_CLOUD_STORES_EMEA : Text
      , ZENDESK_DEFAULT_CLOUD_STORES_JP : Text
      , ZENDESK_DEFAULT_CLOUD_STORES_US : Text
      }

in  { EnvVars
    , staging =
      { ZENDESK_GOOGLE_REDIRECT_URI =
          "https://google.zendesk-staging.com/auth/google"
      , ZENDESK_OFFICE_365_CLIENT_ID = "593dc322-0e30-48f5-ba95-d09b52ae06e0"
      , ZENDESK_OFFICE_365_REDIRECT_URI =
          "https://oauth.zendesk-staging.com/auth/office_365"
      , REPLICATED_UPLOADER_BUCKET_NAME = "guide-hc-staging"
      , UPLOADER_BUCKET_NAME = "zendesk-help-center-staging"
      , K8S_TEMP_AUTH_LABEL = "\"enabled\""
      , DATA_DELETION_RUNNER_MAX_ACTIVE_DELETIONS = "40"
      , FRAUD_SAFELIST_IP_RANGES =
          "10.0.0.0/8 172.16.21.0/24 185.12.80.0/22 188.172.128.0/20 192.161.144.0/20 216.198.0.0/18"
      , SOCIAL_MESSAGING_ZAM_APP_ID = "178825"
      , ZENDESK_TWO_FACTOR_AUTH_TWILIO_ACCOUNT_SID =
          "AC7a0aea161d03718063a90b6c6bd5426d"
      , ZENDESK_TWO_FACTOR_AUTH_TWILIO_NUMBERS = "[+15102161484]"
      , TICKET_ANONYMIZE_SEARCH_TICKETS_TO = "1"
      , ZENDESK_IMAGE_PROXY_HOST = "https://imageproxy-staging.zdassets.com"
      , ZENDESK_PRECREATE_ACCOUNT_POOL_SIZE = "100"
      , ZENDESK_SAAS_PULSE_ID = "SP-11110-01"
      , ENABLE_COVERBAND = "false"
      , UNICORN_MAX_MAX_MEMORY = "2000"
      , UNICORN_MAX_REQUESTS_PER_CHILD = "10000"
      , UNICORN_PERCENTAGE_MAX_MEMORY = "0.7"
      , ENABLE_MIDDLEWARE_TRACING = "true"
      , USE_ATTACHMENTS_CONFIGMAP = "true"
      , USER_LEAKY_BUCKET_INTERVAL = "3"
      , USER_LEAKY_BUCKET_SIZE = "100"
      , USER_LEAKY_BUCKET_THRESHOLD = "20"
      , USER_LIMIT_INTERVAL = "10"
      , USER_LIMIT_THRESHOLD = "10"
      , ZENDESK_AWS_POD_IDS = "[998, 999]"
      , ZENDESK_DEFAULT_CLOUD_STORES_APAC = ""
      , ZENDESK_DEFAULT_CLOUD_STORES_EMEA = ""
      , ZENDESK_DEFAULT_CLOUD_STORES_JP = ""
      , UPLOADER_REGION = "eu-west-1"
      , SES_REGIONS = "us-east-1,us-west-2"
      }
    , production =
      { ZENDESK_GOOGLE_REDIRECT_URI = "https://google.zendesk.com/auth/google"
      , ZENDESK_OFFICE_365_CLIENT_ID = "c5d3a3e0-2ae6-4183-976c-1e1992dfc2a8"
      , ZENDESK_OFFICE_365_REDIRECT_URI =
          "https://oauth.zendesk.com/auth/office_365"
      , REPLICATED_UPLOADER_BUCKET_NAME = "guide-hc-staging"
      , UPLOADER_BUCKET_NAME = "zendesk-help-center-staging"
      , K8S_TEMP_AUTH_LABEL = "\"disabled\""
      , DATA_DELETION_RUNNER_MAX_ACTIVE_DELETIONS = "20"
      , FRAUD_SAFELIST_IP_RANGES =
          "10.0.0.0/8 185.12.80.0/22 188.172.128.0/20 192.161.144.0/20 216.198.0.0/18"
      , SOCIAL_MESSAGING_ZAM_APP_ID = "177200"
      , ZENDESK_TWO_FACTOR_AUTH_TWILIO_ACCOUNT_SID =
          "AC18c210286740a1407f35dc8544e9015b"
      , ZENDESK_TWO_FACTOR_AUTH_TWILIO_NUMBERS =
          "[+14159121868,+14154032387,+14155084654,+14084570397,+16502048812]"
      , TICKET_ANONYMIZE_SEARCH_TICKETS_TO = "12"
      , ZENDESK_IMAGE_PROXY_HOST = "https://imageproxy.zdassets.com"
      , ZENDESK_PRECREATE_ACCOUNT_POOL_SIZE = "50"
      , ZENDESK_SAAS_PULSE_ID = "SP-1111-01"
      , ENABLE_COVERBAND = "false"
      , UNICORN_MAX_MAX_MEMORY = "2000"
      , UNICORN_MAX_REQUESTS_PER_CHILD = "10000"
      , UNICORN_PERCENTAGE_MAX_MEMORY = "0.7"
      , ENABLE_MIDDLEWARE_TRACING = "false"
      , USE_ATTACHMENTS_CONFIGMAP = "true"
      , USER_LEAKY_BUCKET_INTERVAL = "3"
      , USER_LEAKY_BUCKET_SIZE = "100"
      , USER_LEAKY_BUCKET_THRESHOLD = "20"
      , USER_LIMIT_INTERVAL = "10"
      , USER_LIMIT_THRESHOLD = "10"
      , ZENDESK_AWS_POD_IDS = "[12,13,14,15,17,18,19,20,21,22,23,24]"
      , ZENDESK_DEFAULT_CLOUD_STORES_APAC = "[s3_apne1,s3_apse2]"
      , ZENDESK_DEFAULT_CLOUD_STORES_EMEA = "[s3eu,s3_euw1,s3_euc1]"
      , ZENDESK_DEFAULT_CLOUD_STORES_JP = "[s3_pod25]"
      , UPLOADER_REGION = "us-east-1"
      , SES_REGIONS = "eu-central-1,eu-west-1,us-east-1,us-west-2"
      }
    , uniform =
      { ACCOUNT_CLIENT_ACCOUNT_CACHE_EXPIRY_SECONDS = "300"
      , ACCOUNT_SCOPED_ATTACHMENTS = "true"
      , ACME_CERTIFICATE_EARLY_RENEWAL_DAYS = "30"
      , ARCHIVER_PRIORITY_ACCOUNTS = "domains"
      , AUTOMATIONS_JOB_TIMEOUT_MINUTES = "10"
      , BOOTSNAP_CACHE_DIR = "/app/tmp"
      , DATABASE_BACKOFF_AURORA_CPU_KD = "0.025"
      , DATABASE_BACKOFF_AURORA_CPU_KI = "0.075"
      , DATABASE_BACKOFF_AURORA_CPU_KP = "0.125"
      , DBCONFIG_PROXYSQL_CLUSTER_IP = "true"
      , ENABLE_ZENDESK_APM = "true"
      , FACEBOOK_EXTEND_QUERY_MONTHS = "12"
      , GDPR_CONFIG_REGION = "global"
      , HTTP_TARGETS_MAX_ERR_RETRIES = "5"
      , KASKET_EXPIRES_IN_DEFAULT = "3600"
      , KASKET_USE_ELASTICACHE = "true"
      , KUBERNETES_ALLOW_FAILED_PERCENT = "10"
      , KUBERNETES_ALLOW_NOT_READY_PERCENT = "20"
      , LOAD_RADAR_FROM_YAML = "true"
      , LOG_LEVEL = "info"
      , MAX_GDPR_REPUBLISH_PER_DAY = "600000"
      , MAX_TICKETS_PER_ANONYMOUS_USER = "1000"
      , MYSQL_WAIT_TIMEOUT = "10"
      , PERMISSIONS_AGENT_URL =
          "http://permissions-agent.permissions.svc.cluster.local"
      , PRODUCT_CACHE_EXPIRY_SECONDS = "300"
      , PROP_USE_ELASTICACHE = "true"
      , PROXYSQL_VERSION = "PROXYSQL_V2"
      , RADAR_URL_COUNT = "4"
      , REDIS_STORE_USE_CONSUL = "true"
      , ROLLBAR_ENABLED = "true"
      , SOCIAL_MESSAGING_CFC_APP_ID = "98272a52-64c4-416c-bf14-480dfbfe5342"
      , STATS_REDIS_USE_SSL = "true"
      , TICKET_ANONYMIZE_SEARCH_TICKETS_FROM = "48"
      , UNICORN_TCP_PORT = "4080"
      , UNICORN_WORKERS = "18"
      , UNICORN_WRANGLER_MAX_MEMORY = "1200"
      , USE_BOOTSNAP = "true"
      , USE_DALLI_ELASTICACHE = "true"
      , USE_KUBE_CONSOLE = "true"
      , ZENDESK_CONFIG_FROM_ENV = "all"
      , ZENDESK_LOG_LEVEL = "info"
      , ZENDESK_STATS_REDIS_HOST = "localhost:6379"
      , ZENDESK_SYSTEM_GLOBAL_CLIENTS_GOODDATA = "gooddata"
      , ZENDESK_SYSTEM_GLOBAL_CLIENTS_ZOPIM = "zopim"
      , NETWORKS_CONFIG_PATH = "/app/config/networks.d/networks.yml"
      , PROXYSQL_ACCOUNT_MASTER_ENABLED = "true"
      , PROXYSQL_SHARD_MASTER_ENABLED = "true"
      , INLINE_INCIDENT_PROCESSING_LIMIT = "3"
      , REDUCED_OUTGOING_CONVERSIONS_EXPIRY = "2"
      , HYBRID_COOKIES_ENABLED = "true"
      , APP_SERVER_HPA_MAX_REPLICAS = "12"
      , APP_SERVER_HPA_MIN_REPLICAS = "5"
      , TWITTER_PROFILE_SYNC_JOB_TIME = "22"
      , ZENDESK_PODS_998_REGION = "us"
      , ZENDESK_PODS_999_REGION = "us"
      , BILLING_ZUORA_HPM_SIMPLE_3D_SECURE_ID =
          "2c92a00772c5c2e60172cdcb40222260"
      , BILLING_ZUORA_HPM_V2_3D_SECURE_ID = "2c92a0ff56fe33f5015705a97a3e0863"
      , PRIORITY_APP_SERVER_HPA_MAX_REPLICAS = "20"
      , PRIORITY_APP_SERVER_HPA_MIN_REPLICAS = "10"
      , TARGET_CIRCUIT_BREAKER_FAILURE_THRESHOLD = "10"
      , TARGET_CIRCUIT_BREAKER_FAILURE_TTL = "60"
      , TARGET_CIRCUIT_BREAKER_RESET_TIMEOUT = "30"
      , TARGET_CREDENTIALS_ENCRYPTION_CIPHER_NAME = "aes-256-gcm"
      , TARGET_CREDENTIALS_ENCRYPTION_KEY =
          "TARGET_CREDENTIALS_ENCRYPTION_KEY_B"
      , ZENDESK_POD_ATTRIBUTES_ORIGIN =
          "https://xksq16qxl8.execute-api.us-east-1.amazonaws.com"
      , ZOPIM_SCRIBE_API_BASE_URL = "https://voltron.zopim.org"
      , HPA_METRICS_KUBE_NAMESPACE = "classic"
      , HPA_TARGET_ACTIVE_UNICORNS = "12"
      , HPA_TARGET_CPU = "65"
      , CONSUL_HTTP_ADDR = "169.254.1.1:8500"
      , ZENDESK_GEO_LOCATION_DOWNLOAD_URL =
          "https://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz"
      , ZENDESK_GOOGLE_AUTHORIZE_URL = "/o/oauth2/auth"
      , ZENDESK_GOOGLE_SITE = "https://accounts.google.com"
      , ZENDESK_GOOGLE_TOKEN_URL = "/o/oauth2/token"
      , ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_CIPHER_NAME = "aes-256-gcm"
      , ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY_NAME =
          "ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY"
      , ZENDESK_SCREENR_API_TIMEOUT = "10"
      , ZENDESK_SCREENR_BASE_URI = "https://api.sfssdev.com"
      , ZENDESK_SCREENR_USERNAME = "Zendesk"
      , REPLICATED_UPLOADER_REGION = "us-east-1"
      , UPLOADER_STORAGE = "fog"
      , ZENDESK_ZENDESK_VOYAGER_PORT = "13080"
      , ZENDESK_ZENDESK_VOYAGER_TIMEOUT = "10"
      , ZOPIM_SCRIBE_API_PORT = "443"
      , DBCONFIG_WEIGHTED_DNS_RECORD = "true"
      , DEPLOYMENT_MAX_SURGE = "6"
      , ENABLE_QUERY_KILLER = "true"
      , MYSQL_CONNECT_TIMEOUT = "7"
      , UNICORN_WORKERS_PER_K8S_POD = "22"
      , UNICORN_WRANGLER_CHECK_MEM_EVERY_REQ = "100"
      , UNICORN_WRANGLER_NO_OOBGC = "false"
      , WHATSAPP_SMOOCH_INTEGRATION_ID = "98272a52-64c4-416c-bf14-480dfbfe5342"
      }
    , perPod =
        \(pod : Pod.Type) ->
          let podName = Pod.name pod

          let podId = Natural/show (Pod.id pod)

          let podTag = "pod-${podId}"

          let env = Environment.fromPod pod

          let region = Pod.Region.fromPod pod

          let shortRegionName = Pod.shortRegionName region

          let domain = "${shortRegionName}.${Environment.internalHost env}"

          let region = Pod.Region.fromPod pod

          let regionName = Pod.regionName region

          let environment = Environment.fromPod pod

          let environmentName = Environment.name environment

          let dalliElasticacheEndpoint =
                merge
                  { Pod998 =
                      "classic-memcached.kedy9t.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod999 =
                      "classic-memcached.onhdyp.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod13 =
                      "${podName}-classic-memcached.m91eq4.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod14 =
                      "${podName}-classic-memcached.s0goc1.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod15 =
                      "${podName}-classic-memcached.vvbkbw.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod17 =
                      "${podName}-classic-memcached.wg7lxs.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod18 =
                      "${podName}-classic-memcached.pbmecm.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod19 =
                      "${podName}-classic-memcached.azold0.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod20 =
                      "${podName}-classic-memcached.m91eq4.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod23 =
                      "${podName}-classic-memcached.azold0.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod25 =
                      "${podName}-classic-memcached.mxspzh.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod26 =
                      "${podName}-classic-memcached.ufcsyt.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod27 =
                      "${podName}-classic-memcached.ufcsyt.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  }
                  pod

          let kasketElasticacheEndpoint =
                merge
                  { Pod998 =
                      "classic-kasketcache.kedy9t.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod999 =
                      "classic-kasketcache.onhdyp.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod13 =
                      "${podName}-classic-kasketcache.m91eq4.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod14 =
                      "${podName}-classic-kasketcache.s0goc1.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod15 =
                      "${podName}-classic-kasketcache.vvbkbw.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod17 =
                      "${podName}-classic-kasketcache.wg7lxs.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod18 =
                      "${podName}-classic-kasketcache.pbmecm.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod19 =
                      "${podName}-classic-kasketcache.azold0.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod20 =
                      "${podName}-classic-kasketcache.m91eq4.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod23 =
                      "${podName}-classic-kasketcache.azold0.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod25 =
                      "${podName}-classic-kasketcache.mxspzh.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod26 =
                      "${podName}-classic-kasketcache.ufcsyt.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod27 =
                      "${podName}-classic-kasketcache.ufcsyt.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  }
                  pod

          let propElasticacheEndpoint =
                merge
                  { Pod998 =
                      "classic-propcache.kedy9t.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod999 =
                      "classic-propcache.onhdyp.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod13 =
                      "${podName}-classic-propcache.m91eq4.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod14 =
                      "${podName}-classic-propcache.s0goc1.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod15 =
                      "${podName}-classic-propcache.vvbkbw.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod17 =
                      "${podName}-classic-propcache.wg7lxs.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod18 =
                      "${podName}-classic-propcache.pbmecm.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod19 =
                      "${podName}-classic-propcache.azold0.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod20 =
                      "${podName}-classic-propcache.m91eq4.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod23 =
                      "${podName}-classic-propcache.azold0.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod25 =
                      "${podName}-classic-propcache.mxspzh.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod26 =
                      "${podName}-classic-propcache.ufcsyt.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  , Pod27 =
                      "${podName}-classic-propcache.ufcsyt.cfg.${shortRegionName}.cache.amazonaws.com:11211"
                  }
                  pod

          in  { EXPLORE_BOOTSTRAP_KINESIS_STREAM =
                  "realtime-bootstrap-${environmentName}-${regionName}"
              , PROXYSQL_ENABLED_CLUSTERS =
                  if    Prelude.Natural.equal (Pod.id pod) 999
                  then  "accountdb,999-1,999-2"
                  else  "accountdb"
              , UNICORN_MIN_MAX_MEMORY =
                  if    Prelude.Natural.equal (Pod.id pod) 13
                  then  "1000"
                  else  "850"
              , HPA_METRICS_KUBE_CLUSTER = podName
              , ZENDESK_CLASSIC_AWS_SNS_REGION = regionName
              , ZENDESK_TWITTER_CACHE_S3_REGION = regionName
              , STATS_REDIS_CLUSTER = "support-stats-${podId}"
              , ZENDESK_POD = podId
              , ZENDESK_PIGEON_URL =
                  "http://pigeon.${podTag}.svc.${domain}:4080"
              , REDIS_CLUSTER = "misc-${podId}-1"
              , ZENDESK_ZENDESK_VOYAGER_HOST =
                  "voyager.vip.${podName}.${shortRegionName}.zdsystest.com"
              , CUSTOM_RESOURCES_URL =
                  "http://${podTag}.sunshine-proxy.service.consul:10050"
              , TWITTER_PROXY_URL =
                  "https://${podName}-twitter-proxy.${Environment.host
                                                        environment}"
              , PERMISSIONS_SERVICE_URL =
                  "https://proxy-nlb.${podName}.${domain}"
              , DATADOG_KUBE_CLUSTER = "\"${podName}\""
              , UNICORN_MAX_GC_REQUEST_TIME =
                  if    Prelude.Natural.equal (Pod.id pod) 998
                  then  "8000"
                  else  "8000"
              , ZENDESK_AWS_REGION = regionName
              , ZENDESK_TWITTER_CACHE_S3_BUCKET =
                  "zendesk-classic-twitter-cache-${environmentName}-${regionName}"
              , DALLI_ELASTICACHE_ENDPOINT = dalliElasticacheEndpoint
              , KASKET_ELASTICACHE_ENDPOINT = kasketElasticacheEndpoint
              , PROP_ELASTICACHE_ENDPOINT = propElasticacheEndpoint
              , ZENDESK_DEFAULT_CLOUD_STORES_US =
                  merge
                    { Pod998 = "[s3]"
                    , Pod999 = "[s3]"
                    , Pod13 = "[s3,s3_usw2,s3_use1]"
                    , Pod14 = "[s3,s3_usw2,s3_use1]"
                    , Pod15 = "[s3,s3_usw2,s3_use1]"
                    , Pod17 = "[s3,s3_usw2,s3_use1]"
                    , Pod18 = "[s3,s3_usw2,s3_use1]"
                    , Pod19 = "[s3,s3_usw2,s3_use1]"
                    , Pod20 = "[s3,s3_usw2,s3_use1]"
                    , Pod23 = "[s3,s3_usw2,s3_use1]"
                    , Pod25 = "[s3,s3_usw2,s3_use1]"
                    , Pod26 = "[s3_pod26]"
                    , Pod27 = "[s3_pod27]"
                    }
                    pod
              }
    }
