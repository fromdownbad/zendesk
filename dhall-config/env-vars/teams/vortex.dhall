let Prelude = ../../dependencies/prelude.dhall

let zendesk = ../../dependencies/zendesk-dhall.dhall

let Pod = zendesk.Pod

let EnvVars =
      { ZENDESK_OCCAM_OPEN_TIMEOUT : Text
      , ZENDESK_OCCAM_URL : Text
      , OCCAM_REDIS_CLUSTER : Text
      , OCCAM_REDIS_TIMEOUT : Text
      }

in  { EnvVars
    , staging.ZENDESK_OCCAM_OPEN_TIMEOUT = "0.10"
    , production.ZENDESK_OCCAM_OPEN_TIMEOUT = "0.05"
    , uniform.OCCAM_REDIS_TIMEOUT = "2"
    , perPod =
        \(pod : Pod.Type) ->
          let region = Pod.Region.fromPod pod

          in  { OCCAM_REDIS_CLUSTER = "occam-redis-${Natural/show (Pod.id pod)}"
              , ZENDESK_OCCAM_URL =
                  "http://ab584f4c8b67041fdbdb00113c1196fb-0491b59908c6c21d.elb.${Pod.regionName
                                                                                    region}.amazonaws.com:4080"
              }
    }
