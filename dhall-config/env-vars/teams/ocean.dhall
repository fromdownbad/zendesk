let Prelude = ../../dependencies/prelude.dhall

let EnvVars =
      { CHANNELS_CFC_DOMAIN : Text
      , CHANNELS_FB_ENCRYPTION_KEY_NAME : Text
      , CHANNELS_TWITTER_ENCRYPTION_KEY_NAME : Text
      , CHANNELS_FB_ENCRYPTION_CIPHER_NAME : Text
      , CHANNELS_TWITTER_ENCRYPTION_CIPHER_NAME : Text
      }

in  { EnvVars
    , staging.CHANNELS_CFC_DOMAIN = "zdchanconn.smooch.rocks"
    , production.CHANNELS_CFC_DOMAIN = "connector.zendesk.ext.smooch.io"
    , uniform =
      { CHANNELS_FB_ENCRYPTION_KEY_NAME = "CHANNELS_FB_ENCRYPT_KEY_A"
      , CHANNELS_TWITTER_ENCRYPTION_KEY_NAME =
          "CHANNELS_TWITTER_CREDENTIAL_KEY_A"
      , CHANNELS_FB_ENCRYPTION_CIPHER_NAME = "aes-256-gcm"
      , CHANNELS_TWITTER_ENCRYPTION_CIPHER_NAME = "aes-256-gcm"
      }
    }
