let Prelude = ../../dependencies/prelude.dhall

let zendesk = ../../dependencies/zendesk-dhall.dhall

let envVarGroups = ../../dependencies/shared-env-groups.dhall

let Pod = zendesk.Pod

let groups = envVarGroups.schemas

let podEnvVars = envVarGroups.perPod

let PodSpecificEnvVars =
          groups.AccountService.Type
      //\\  groups.AuthService.Type
      //\\  groups.AwsRegion.Type
      //\\  groups.Billing.Type
      //\\  groups.Cdn.Type
      //\\  groups.Dkim.Type
      //\\  groups.EdgeProxyInternalNlbUrl.Type
      //\\  groups.EkrBaseUrl.Type
      //\\  groups.HelpCenterVipUrl.Type
      //\\  groups.KafkaBrokers.Type
      //\\  groups.LegionServiceUrl.Type
      //\\  groups.MailFetcherCredentials.Type
      //\\  groups.OccamK8s.Type
      //\\  groups.OutboundMail.Type
      //\\  groups.RailsAndRackEnv.Type
      //\\  groups.ResqueCluster.Type
      //\\  groups.Rollbar.Type
      //\\  groups.RosettaServiceProxy.Type
      //\\  groups.Rspamd.Type
      //\\  groups.SearchServiceHubUrl.Type
      //\\  groups.StaffService.Type
      //\\  groups.StatsdKubernetes.Type
      //\\  groups.TessaService.Type
      //\\  groups.VoiceVips.Type
      //\\  groups.ZendeskDatacenterDomain.Type
      //\\  groups.ZendeskGlobalUidOptions.Type
      //\\  groups.ZendeskHost.Type
      //\\  groups.ZendeskPodId.Type
      //\\  groups.ZendeskRegion.Type
      //\\  groups.ZendeskStaticAssets.Type

let envVars
    : Pod.All.Type PodSpecificEnvVars
    =     podEnvVars.AccountService
      /\  podEnvVars.AuthService
      /\  podEnvVars.AwsRegion
      /\  podEnvVars.Billing
      /\  podEnvVars.Cdn
      /\  podEnvVars.Dkim
      /\  podEnvVars.EdgeProxyInternalNlbUrl
      /\  podEnvVars.EkrBaseUrl
      /\  podEnvVars.HelpCenterVipUrl
      /\  podEnvVars.KafkaBrokers
      /\  podEnvVars.LegionServiceUrl
      /\  podEnvVars.MailFetcherCredentials
      /\  podEnvVars.OccamK8s
      /\  podEnvVars.OutboundMail
      /\  podEnvVars.RailsAndRackEnv
      /\  podEnvVars.ResqueCluster
      /\  podEnvVars.Rollbar
      /\  podEnvVars.RosettaServiceProxy
      /\  podEnvVars.Rspamd
      /\  podEnvVars.SearchServiceHubUrl
      /\  podEnvVars.StaffService
      /\  podEnvVars.StatsdKubernetes
      /\  podEnvVars.TessaService
      /\  podEnvVars.VoiceVips
      /\  podEnvVars.ZendeskDatacenterDomain
      /\  podEnvVars.ZendeskGlobalUidOptions
      /\  podEnvVars.ZendeskHost
      /\  podEnvVars.ZendeskPodId
      /\  podEnvVars.ZendeskRegion
      /\  podEnvVars.ZendeskStaticAssets

let overrides =
      \(pod : Pod.Type) ->
        let podName = Pod.name pod

        in  { ZENDESK_OCCAM_K8S_URL = Some
                "http://occam.${podName}.svc.cluster.local.:80"
            , ACCOUNT_SERVICE_URL = Some
                "http://account-service.${podName}.svc.cluster.local."
            , VOICE_VIP_HOST = Some "voice.voice.svc.cluster.local."
            , BILLING_ZUORA_PRODUCT_ID_SUITE = Some
                "2c92a0ff6300cc8d016303500cf07c9f"
            , BILLING_ZUORA_PRODUCT_ID_SUITE_CC = Some
                "2c92a0ff6300cc8d016303500cf07c9f"
            , BILLING_ZUORA_REST_API_DOMAIN = Some "restservices058.zuora.com"
            , EXPLORE_RSA_PUBLIC_KEY = Some ""
            , BILLING_ZUORA_HPM_V2_API_SECURITY_KEY = Some
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtanUjuA1Lw6IQFlJAYS6e3jxPn8SDtpp75IV2Kbvxmo3+UfzGSgKRmLJl2kN3nMa+Y42m9IFUqltYh4/vN6Dqdh6BOx8n1JHXLYi5tePTZk5Kgxov5tYwYSjUy4ByO1r2cUVrOghOXX9fzFsmDU0dY4rdERyWcNcSdn6MUCSgCSY95LrxNWStaLfJGTnpqktNnAHVoDnn/XGrlt+821rR2HhiifvGhchVU+8ZL7LopizTiKtHP8HylxA35fs4QUHRK/tHk8roJ20zTd5JYWUW6Sva4KvROcMQa5g45FAsfKGewFRh3XYJfahLaQmaHPy2hMpj2iIqbtVQHVBEjnGawIDAQAB"
            , SEARCH_SERVICE_HUB_URL = Some
                "http://search-service-hub.${podName}.svc.cluster.local."
            , SEARCH_SERVICE_URL_BLUE = Some
                "http://search-service-blue.${podName}.svc.cluster.local.:8085"
            , SEARCH_SERVICE_URL_GREEN = Some
                "http://search-service-green.${podName}.svc.cluster.local.:8085"
            , STAFF_SERVICE_URL = Some
                "http://staff-service.${podName}.svc.cluster.local."
            , AUTH_SERVICE_URL = Some "http://auth-service.doorman:80"
            }

let fromPod =
      \(pod : Pod.Type) ->
        Pod.All.get PodSpecificEnvVars pod envVars // overrides pod

in  { EnvVars = PodSpecificEnvVars, fromPod }
