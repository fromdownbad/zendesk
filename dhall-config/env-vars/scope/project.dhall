let zendesk = ../../dependencies/zendesk-dhall.dhall

let Harrier = ../teams/harrier.dhall

let Ocean = ../teams/ocean.dhall

let Squonk = ../teams/squonk.dhall

let Strongbad = ../teams/strongbad.dhall

let TicketPlatform = ../teams/ticket-platform.dhall

let Vortex = ../teams/vortex.dhall

let Gooddata = ../technologies/gooddata.dhall

let Salesforce = ../technologies/salesforce.dhall

let Pod = zendesk.Pod

let varsByPod =
      \(pod : Pod.Type) ->
            Harrier.perPod pod
        /\  Squonk.perPod pod
        /\  TicketPlatform.perPod pod
        /\  Vortex.perPod pod

let varsByEnvironment =
          Harrier.{ production, staging }
      /\  Ocean.{ production, staging, uniform }
      /\  Squonk.{ production, staging, uniform }
      /\  Strongbad.{ uniform }
      /\  TicketPlatform.{ production, staging, uniform }
      /\  Vortex.{ production, staging, uniform }
      /\  Gooddata.{ production, staging, uniform }
      /\  Salesforce.{ production, staging, uniform }

let ProjectEnvVars =
          Harrier.EnvVars
      //\\  Ocean.EnvVars
      //\\  Squonk.EnvVars
      //\\  Strongbad.EnvVars
      //\\  TicketPlatform.EnvVars
      //\\  Vortex.EnvVars
      //\\  Gooddata.EnvVars
      //\\  Salesforce.EnvVars

let combine
    : Pod.Type -> ProjectEnvVars
    = \(pod : Pod.Type) ->
            varsByPod pod
        /\  varsByEnvironment.uniform
        /\  ( if    Pod.isProduction pod
              then  varsByEnvironment.production
              else  varsByEnvironment.staging
            )

let envVars = Pod.All.generate ProjectEnvVars combine

let fromPod = \(pod : Pod.Type) -> Pod.All.get ProjectEnvVars pod envVars

in  { EnvVars = ProjectEnvVars, fromPod }
