let EnvVars =
      { GOODDATA_API_ENDPOINT : Text
      , GOODDATA_LOGIN : Text
      , GOODDATA_ORGANIZATION : Text
      , GOODDATA_SIGNER_EMAIL : Text
      , GOODDATA_SSO_PROVIDER : Text
      , GOODDATA_V1_LOGIN : Text
      , GOODDATA_V2_API_ENDPOINT : Text
      , GOODDATA_V2_LOGIN : Text
      , GOODDATA_V2_ORGANIZATION : Text
      , GOODDATA_V2_SIGNER_EMAIL : Text
      , GOODDATA_V2_SSO_PROVIDER : Text
      , GOODDATA_V2_TOUR_PROJECT_DASHBOARD_ID : Text
      , GOODDATA_V2_TOUR_PROJECT_EMAIL : Text
      , GOODDATA_V2_TOUR_PROJECT_ID : Text
      , GOODDATA_SIGNER_KEY_PATH : Text
      , GOODDATA_TOUR_PROJECT_DASHBOARD_ID : Text
      , GOODDATA_TOUR_PROJECT_EMAIL : Text
      , GOODDATA_TOUR_PROJECT_ID : Text
      , GOODDATA_V1_API_ENDPOINT : Text
      }

in  { EnvVars
    , staging =
      { GOODDATA_API_ENDPOINT = "https://analytics.zendesk-staging.com"
      , GOODDATA_LOGIN = "gooddata-staging@zendesk.com"
      , GOODDATA_ORGANIZATION = "zendesk-dev"
      , GOODDATA_SIGNER_EMAIL = "gooddata-staging@zendesk.com"
      , GOODDATA_SSO_PROVIDER = "zendesk-staging.com"
      , GOODDATA_V1_LOGIN = "developer@zendesk.com"
      , GOODDATA_V2_API_ENDPOINT = "https://analytics.zendesk-staging.com"
      , GOODDATA_V2_LOGIN = "gooddata-staging@zendesk.com"
      , GOODDATA_V2_ORGANIZATION = "zendesk-dev"
      , GOODDATA_V2_SIGNER_EMAIL = "gooddata-staging@zendesk.com"
      , GOODDATA_V2_SSO_PROVIDER = "zendesk-staging.com"
      , GOODDATA_V2_TOUR_PROJECT_DASHBOARD_ID = "28253"
      , GOODDATA_V2_TOUR_PROJECT_EMAIL = "gd_onboarding@zendesk.com"
      , GOODDATA_V2_TOUR_PROJECT_ID = "szjxxnyoe8qa0dc1x5f9aeanqptsas1r"
      }
    , production =
      { GOODDATA_API_ENDPOINT = "https://analytics.zendesk.com"
      , GOODDATA_LOGIN = "gooddata2@zendesk.com"
      , GOODDATA_ORGANIZATION = "zendesk-prod"
      , GOODDATA_SIGNER_EMAIL = "gooddata2@zendesk.com"
      , GOODDATA_SSO_PROVIDER = "zendesk.com"
      , GOODDATA_V1_LOGIN = "gooddata@zendesk.com"
      , GOODDATA_V2_API_ENDPOINT = "https://analytics.zendesk.com"
      , GOODDATA_V2_LOGIN = "gooddata2@zendesk.com"
      , GOODDATA_V2_ORGANIZATION = "zendesk-prod"
      , GOODDATA_V2_SIGNER_EMAIL = "gooddata2@zendesk.com"
      , GOODDATA_V2_SSO_PROVIDER = "zendesk.com"
      , GOODDATA_V2_TOUR_PROJECT_DASHBOARD_ID = "28253"
      , GOODDATA_V2_TOUR_PROJECT_EMAIL = "gd_onboarding@zendesk.com"
      , GOODDATA_V2_TOUR_PROJECT_ID = "szjxxnyoe8qa0dc1x5f9aeanqptsas1r"
      }
    , uniform =
      { GOODDATA_SIGNER_KEY_PATH = "config/gooddata_gpg.key"
      , GOODDATA_TOUR_PROJECT_DASHBOARD_ID = "28253"
      , GOODDATA_TOUR_PROJECT_EMAIL = "gd_onboarding@zendesk.com"
      , GOODDATA_TOUR_PROJECT_ID = "szjxxnyoe8qa0dc1x5f9aeanqptsas1r"
      , GOODDATA_V1_API_ENDPOINT = "https://secure.gooddata.com"
      }
    }
