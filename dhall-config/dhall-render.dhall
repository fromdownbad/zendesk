{-
Self-install dhall-render. If you want to update the version of dhall-render used, update the commit
sha in the import statement and rerun the self-install.

https://github.com/timbertson/dhall-render#how-should-i-bootstrap-it
-}
let Render = ./dependencies/render.dhall

in  { files.dhall-render = Render.SelfInstall.exe }
