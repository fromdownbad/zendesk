require 'yaml'
require 'open3'
require 'ap'

DHALL_ENV_VARS = './env-vars/package.dhall'.freeze
ZENDESK_DHALL = './dependencies/zendesk-dhall.dhall'.freeze
IGNORED_ENV_VARS = %w(
  HPA_METRICS_KUBE_CLUSTER
  HPA_METRICS_KUBE_NAMESPACE
  K8S_TEMP_AUTH_LABEL
  DEPLOYMENT_MAX_SURGE
  PRIORITY_APP_SERVER_HPA_MIN_REPLICAS
  PRIORITY_APP_SERVER_HPA_MAX_REPLICAS
  HPA_TARGET_CPU
  HPA_METRICS_KUBE_CLUSTER
  HPA_METRICS_KUBE_NAMESPACE
  HPA_TARGET_ACTIVE_UNICORNS
  BILLING_ZUORA_OAUTH_CLIENT_ID
  BILLING_RSA_PRIVATE_KEY
  BILLING_ZUORA_API_PASSWORD
  BILLING_ZUORA_API_USERNAME
  BILLING_ZUORA_OAUTH_CLIENT_SECRET
  CLASSIC_RSA_PRIVATE_KEY
  DIRECT_RSA_PRIVATE_KEY
  dkim_private_key_zendesk1
  dkim_private_key_zendesk2
  ZENDESK_EXTERNAL_EMAIL_CREDENTIAL_CLIENT_SECRET
  ZENDESK_OUTBOUND_MAIL_CONFIGURATION_SERVICE_API_KEY
  RSPAMD_PASSWORD
  ATTACHMENT_FU_S3_ACCESS_KEY
  ATTACHMENT_FU_S3_SECRET_KEY
  ZENDESK_SYSTEM_USER_AUTH_ZIS_ENGINE_OAUTH
  ACCOUNT_SERVICE_JWT_SECRET
  GDPR_AWS_ACCESS_KEY
  GDPR_AWS_SECRET_KEY
  GDPR_WRITER_AWS_ACCESS_KEY
  GDPR_WRITER_AWS_SECRET_KEY
  REMOTE_FILES_AWS_ACCESS_KEY
  REMOTE_FILES_AWS_SECRET_KEY
  REPLICATED_UPLOADER_AWS_ACCESS_KEY_ID
  REPLICATED_UPLOADER_AWS_ACCESS_KEY_ID_PATH
  REPLICATED_UPLOADER_AWS_SECRET_ACCESS_KEY
  REPLICATED_UPLOADER_AWS_SECRET_ACCESS_KEY_PATH
  ROLLBAR_EU_ACCESS_TOKEN
  ROLLBAR_EU_LOTUS_ACCESS_TOKEN
  ROLLBAR_US_ACCESS_TOKEN
  ROLLBAR_US_LOTUS_ACCESS_TOKEN
  STATS_REDIS_PASSWORD
  UPLOADER_AWS_ACCESS_KEY_ID
  UPLOADER_AWS_SECRET_ACCESS_KEY
  ZENDESK_GOOGLE_APP_MARKET_JWE_KEY
  ZENDESK_TWITTER_CACHE_S3_ACCESS_KEY
  ZENDESK_TWITTER_CACHE_S3_SECRET_KEY
  CHANNELS_FB_ENCRYPT_KEY_A
  CHANNELS_FB_ENCRYPT_KEY_B
  CHANNELS_TWITTER_CREDENTIAL_KEY_A
  CHANNELS_TWITTER_CREDENTIAL_KEY_B
  external_email_credentials_encryption_key_a
  external_email_credentials_encryption_key_b
  GOODDATA_AUTH_TOKEN
  GOODDATA_PASSWORD
  OCCAM_REDIS_PASSWORD
  TARGET_CREDENTIALS_ENCRYPTION_KEY_A
  TARGET_CREDENTIALS_ENCRYPTION_KEY_B
  KRITIS_BREAK_GLASS
  CELL_APP_SERVER_HPA_MAX_REPLICAS
  CELL_APP_SERVER_HPA_MIN_REPLICAS
  DD_RUM_APPLICATION_ID
  DD_RUM_TOKEN
).freeze
DIVIDER = "#{"-"*80}\n".freeze

all_pods = ['pod998', 'pod999']
pods = ARGV.empty? ? all_pods : ARGV

pods.each do |pod|
  dhall_envs = nil
  samson_envs = nil
  missing = {}
  mismatch = {}

  Open3.popen2('dhall', "text") do |stdin, stdout, _status_thread|
    stdin.print "(./tests/samson-env.dhall).#{pod}"
    stdin.close
    # Convert text response to YAML
    samson_envs = YAML.load(stdout.read.gsub('="', ': "').gsub("''\n", ""))
  end

  Open3.popen2('dhall-to-yaml --quoted') do |stdin, stdout, _status_thread|
    stdin.print "((#{DHALL_ENV_VARS}).asMap (#{ZENDESK_DHALL}).Pod.Type.#{pod.capitalize})"
    stdin.close
    dhall_envs = YAML.load(stdout.read)
  end

  samson_envs.each do |env, samson_value|
    next if IGNORED_ENV_VARS.include?(env)

    unless dhall_envs.key?(env)
      missing[env] = samson_value
      next
    end

    dhall_value = dhall_envs[env]
    unless samson_value == dhall_value
      mismatch[env] = [samson_value, dhall_value]
    end
  end

  if missing.empty? && mismatch.empty?
    ap "Samson and Dhall envs are equivalent for #{pod}"
  else
    ap "Please fix the discrepancies between the Samson and Dhall envs for #{pod} below."
    puts DIVIDER
    unless missing.empty?
      ap 'Missing from dhall env:'
      ap missing
      puts "#{DIVIDER}\n"
    end
    unless mismatch.empty?
      ap 'Mismatch between env values i.e. ENV_VAR => [samson_value, dhall_value]:'
      ap mismatch
    end
  end
  puts DIVIDER*2
end
