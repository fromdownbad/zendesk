let Prelude = ../dependencies/prelude.dhall

let zendesk = ../dependencies/zendesk-dhall.dhall

let EnvVars = ../env-vars/package.dhall

let Pod = zendesk.Pod

let Environment = zendesk.Environment

let Datum = { name : Text, value : Text, environment : Text, pod : Text }

let legend
    : Datum
    = { name = "Name"
      , value = "Value"
      , environment = "Environment"
      , pod = "Zendesk Pod"
      }

let Datum/toRow =
      \(datum : Datum) ->
        Prelude.Text.concatSep
          ","
          [ datum.name
          , "\"${Text/replace "\"" "'" (Text/replace
                   ''
                   ${"\r"}
                   ''
                   ""
                   datum.value)}\""
          , datum.environment
          , datum.pod
          ]

let Datum/fromEnvVar =
      \(pod : Pod.Type) ->
      \(envVar : EnvVars.Entry) ->
        { name = envVar.mapKey
        , value = envVar.mapValue
        , environment = Environment.name (Environment.fromPod pod)
        , pod = Pod.name pod
        }

let Datum/commaSeparate
    : Pod.Type -> EnvVars.Entry -> Text
    = \(pod : Pod.Type) ->
      \(envVar : EnvVars.Entry) ->
        Datum/toRow (Datum/fromEnvVar pod envVar)

let rows
    : Pod.Type -> List Text
    = \(pod : Pod.Type) ->
        Prelude.List.map
          EnvVars.Entry
          Text
          (Datum/commaSeparate pod)
          (EnvVars.asMap pod)

let pods =
      Prelude.List.concat
        Text
        ((Pod.All.Of (List Text)).values (Pod.All.generate (List Text) rows))

in  Prelude.Text.concatSep "\n" ([ Datum/toRow legend ] # pods)
