let Prelude = ../dependencies/prelude.dhall

let zendesk = ../dependencies/zendesk-dhall.dhall

let Secrets = ../secrets/package.dhall

let Pod = zendesk.Pod

let Environment = zendesk.Environment

let Datum = { name : Text, value : Text, environment : Text }

let legend
    : Datum
    = { name = "Name", value = "Value", environment = "Environment" }

let Datum/toRow =
      \(datum : Datum) ->
        Prelude.Text.concatSep
          ","
          [ datum.name, datum.value, datum.environment ]

let Datum/fromSecret =
      \(environment : Environment.Type) ->
      \(secret : Secrets.Entry) ->
        { name = secret.mapKey
        , value = secret.mapValue
        , environment = Environment.name environment
        }

let Datum/commaSeparate
    : Environment.Type -> Secrets.Entry -> Text
    = \(environment : Environment.Type) ->
      \(secret : Secrets.Entry) ->
        Datum/toRow (Datum/fromSecret environment secret)

let rows
    : Environment.Type -> List Text
    = \(environment : Environment.Type) ->
        let vaultEnv =
              merge
                { Development = Secrets.Vault.Environment.Staging
                , Test = Secrets.Vault.Environment.Staging
                , CI = Secrets.Vault.Environment.Staging
                , Staging = \(_ : Pod.Type) -> Secrets.Vault.Environment.Staging
                , Production =
                    \(_ : Pod.Type) -> Secrets.Vault.Environment.Production
                }
                environment

        in  Prelude.List.map
              Secrets.Entry
              Text
              (Datum/commaSeparate environment)
              (toMap (Secrets.forVault vaultEnv))

let environments =
      Prelude.List.concat
        Text
        [ rows (Environment.fromPod Pod.Type.Pod998)
        , rows (Environment.fromPod Pod.Type.Pod13)
        ]

in  Prelude.Text.concatSep "\n" ([ Datum/toRow legend ] # environments)
