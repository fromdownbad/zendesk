let Prelude = ../dependencies/prelude.dhall

let Pod = (../dependencies/zendesk-dhall.dhall).Pod

let Scale = ../../kubernetes/dhall/scale/package.dhall

let AppServerFile = ../../kubernetes/dhall/roles/app-server.dhall

let AppServer = ../../kubernetes/dhall/roles/app-server.dhall

let Consumer = ../../kubernetes/dhall/roles/consumer.dhall

let ConsumerFile = ../../kubernetes/dhall/roles/consumer.dhall

let MiscProcess = ../../kubernetes/dhall/roles/misc-process.dhall

let MiscProcessFile = ../../kubernetes/dhall/roles/misc-process.dhall

let Worker = ../../kubernetes/dhall/roles/worker.dhall

let WorkerFile = ../../kubernetes/dhall/roles/worker.dhall

let names = ../../kubernetes/dhall/roles/names.dhall

let RoleDocumentation = { name : Text, scale : Scale.Type }

let Datum =
      { name : Text
      , podName : Text
      , replicas : Text
      , cpuRequests : Text
      , cpuLimit : Text
      , memoryRequests : Text
      , memoryLimit : Text
      }

let legend
    : Datum
    = { name = "Role name"
      , podName = "Pod"
      , replicas = "Replicas"
      , cpuRequests = "CPU Requests"
      , cpuLimit = "CPU Limit"
      , memoryRequests = "Memory Requests"
      , memoryLimit = "Memory Limit"
      }

let Datum/toRow =
      \(datum : Datum) ->
        Prelude.Text.concatSep
          ","
          [ datum.name
          , datum.podName
          , datum.replicas
          , datum.cpuRequests
          , datum.cpuLimit
          , datum.memoryRequests
          , datum.memoryLimit
          ]

let replicas/show =
      \(replicas : Optional Natural) ->
        merge
          { Some = \(count : Natural) -> Natural/show count, None = "-" }
          replicas

let Datum/fromRoleDocumentation =
      \(pod : Pod.Type) ->
      \(docs : RoleDocumentation) ->
        let limits = Scale.matchLimitsToRequests docs.scale

        in  { name = docs.name
            , podName = Pod.name pod
            , replicas = replicas/show docs.scale.replicas
            , cpuRequests = docs.scale.cpuRequests
            , cpuLimit = limits.cpu
            , memoryRequests = docs.scale.cpuRequests
            , memoryLimit = limits.memory
            }

let appServers =
      \(pod : Pod.Type) ->
        Prelude.List.map
          AppServer.Config.Type
          RoleDocumentation
          ( \(appServer : AppServer.Config.Type) ->
              { name = AppServerFile.name appServer
              , scale = appServer.(Scale.Type)
              }
          )
          (AppServerFile.files pod)

let consumers =
      \(pod : Pod.Type) ->
        Prelude.List.map
          Consumer.Config.Type
          RoleDocumentation
          ( \(consumer : Consumer.Config.Type) ->
              { name = ConsumerFile.name consumer
              , scale = consumer.(Scale.Type)
              }
          )
          (ConsumerFile.files pod)

let miscProcesses =
      \(pod : Pod.Type) ->
        Prelude.List.map
          MiscProcess.Config.Type
          RoleDocumentation
          ( \(miscProcess : MiscProcess.Config.Type) ->
              { name = MiscProcessFile.name miscProcess
              , scale = miscProcess.(Scale.Type)
              }
          )
          (MiscProcessFile.files pod)

let workers =
      \(pod : Pod.Type) ->
        Prelude.List.map
          Worker.Config.Type
          RoleDocumentation
          ( \(worker : Worker.Config.Type) ->
              { name = WorkerFile.name worker, scale = worker.(Scale.Type) }
          )
          (WorkerFile.files pod)

let scaleByRole
    : Pod.Type -> List RoleDocumentation
    = \(pod : Pod.Type) ->
          [ { name = names.durableQueueMonitor
            , scale = ../../kubernetes/dhall/scale/durable-queue-monitor.dhall
            }
          , { name = names.mailTicketCreator
            , scale = ../../kubernetes/dhall/scale/mail-ticket-creator.dhall
            }
          , { name = names.resqueScheduler
            , scale = ../../kubernetes/dhall/scale/resque-scheduler.dhall
            }
          , { name = names.resqueWeb, scale = ../../kubernetes/dhall/scale/resque-web.dhall }
          ]
        # appServers pod
        # consumers pod
        # miscProcesses pod
        # workers pod

let RoleDocumentation/commaSeparate =
      \(pod : Pod.Type) ->
      \(docs : RoleDocumentation) ->
        Datum/toRow (Datum/fromRoleDocumentation pod docs)

let rows
    : Pod.Type -> List Text
    = \(pod : Pod.Type) ->
        Prelude.List.map
          RoleDocumentation
          Text
          (RoleDocumentation/commaSeparate pod)
          (scaleByRole pod)

let pods =
      Prelude.List.concat
        Text
        ((Pod.All.Of (List Text)).values (Pod.All.generate (List Text) rows))

in  Prelude.Text.concatSep "\n" ([ Datum/toRow legend ] # pods)
