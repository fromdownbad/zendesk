{-
Headers used in a remote import when fetching dhall data from another GitHub repo.
-}
toMap
  { Authorization = "token ${env:GITHUB_TOKEN as Text}"
  , User-Agent = "zendesk-dhall"
  , Accept = "application/vnd.github.4.raw"
  }
