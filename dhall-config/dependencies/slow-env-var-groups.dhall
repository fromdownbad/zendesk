let groups =
        https://raw.githubusercontent.com/zendesk/config-service-data/7a67f7fc87872742b40620e5d32f812a138eaed8/generated/dhall/data/shared_env_groups/package.dhall using (./github-headers.dhall) sha256:0ae7be8c42f1053aaf33235f557b8f059641e0303bea42b7f3f28b5b5f0f4ecf

let selection =
      groups.{ AccountService
             , AuthService
             , AwsRegion
             , Billing
             , Cdn
             , Dkim
             , EdgeProxyInternalNlbUrl
             , EkrBaseUrl
             , HelpCenterVipUrl
             , KafkaBrokers
             , LegionServiceUrl
             , MailFetcherCredentials
             , OccamK8s
             , OutboundMail
             , RailsAndRackEnv
             , ResqueCluster
             , Rollbar
             , RosettaServiceProxy
             , Rspamd
             , SearchServiceHubUrl
             , StaffService
             , StatsdKubernetes
             , TessaService
             , VoiceVips
             , ZendeskDatacenterDomain
             , ZendeskGlobalUidOptions
             , ZendeskHost
             , ZendeskPodId
             , ZendeskRegion
             , ZendeskStaticAssets
             }

in  selection
