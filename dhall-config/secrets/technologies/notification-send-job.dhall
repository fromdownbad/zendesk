{-
Usage: app/models/jobs/push\_notification\_send\_job.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { ZENDESK_URBAN_AIRSHIP_APP_KEY : Text
      , ZENDESK_URBAN_AIRSHIP_APP_SECRET : Text
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_APP_KEY` : Text
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_APP_SECRET` : Text
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_MASTER_SECRET` : Text
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_APP_KEY` : Text
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_APP_SECRET` : Text
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_MASTER_SECRET` : Text
      , ZENDESK_URBAN_AIRSHIP_MASTER_SECRET : Text
      }

let secrets =
      \(env : Environment) ->
        { ZENDESK_URBAN_AIRSHIP_APP_KEY =
            Vault.path env Scope.Project "zendesk_urban_airship_app_key"
        , ZENDESK_URBAN_AIRSHIP_APP_SECRET =
            Vault.path env Scope.Project "zendesk_urban_airship_app_secret"
        , ZENDESK_URBAN_AIRSHIP_MASTER_SECRET =
            Vault.path env Scope.Project "zendesk_urban_airship_master_secret"
        , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_APP_KEY` =
            Vault.path
              env
              Scope.Project
              "zendesk_urban_airship_com_zendesk_agent_app_key"
        , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_APP_SECRET` =
            Vault.path
              env
              Scope.Project
              "zendesk_urban_airship_com_zendesk_agent_app_secret"
        , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_MASTER_SECRET` =
            Vault.path
              env
              Scope.Project
              "zendesk_urban_airship_com_zendesk_agent_master_secret"
        , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_APP_KEY` =
            Vault.path
              env
              Scope.Project
              "zendesk_urban_airship_com_zendesk_inbox_app_key"
        , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_APP_SECRET` =
            Vault.path
              env
              Scope.Project
              "zendesk_urban_airship_com_zendesk_inbox_app_secret"
        , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_MASTER_SECRET` =
            Vault.path
              env
              Scope.Project
              "zendesk_urban_airship_com_zendesk_inbox_master_secret"
        }

in  { Secrets, secrets }
