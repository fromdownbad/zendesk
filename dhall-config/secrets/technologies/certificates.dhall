{-
Usage: app/models/certificate.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets = { ZENDESK_CERTIFICATES_SSL_KEY_PASSWORD : Text }

let secrets =
      \(env : Environment) ->
        { ZENDESK_CERTIFICATES_SSL_KEY_PASSWORD =
            Vault.path
              Environment.All
              Scope.Global
              "ZENDESK_CERTIFICATES_SSL_KEY_PASSWORD"
        }

in  { Secrets, secrets }
