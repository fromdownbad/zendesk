{-
Usage: app/helpers/radar_helper.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets = { ZENDESK_NODE_PUBSUB_SECRET_KEY : Text }

let secrets =
      \(env : Environment) ->
        { ZENDESK_NODE_PUBSUB_SECRET_KEY =
            Vault.path env Scope.Global "zendesk_node_pubsub_secret_key"
        }

in  { Secrets, secrets }
