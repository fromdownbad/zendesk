{-
Usage: initializers/recaptcha.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { ZENDESK_RECAPTCHA_V2_SECRET_KEY : Text
      , ZENDESK_RECAPTCHA_V2_SITE_KEY : Text
      }

let secrets =
      \(env : Environment) ->
        { ZENDESK_RECAPTCHA_V2_SITE_KEY =
            Vault.path env Scope.Global "zendesk_recaptcha_v2_site_key"
        , ZENDESK_RECAPTCHA_V2_SECRET_KEY =
            Vault.path env Scope.Global "zendesk_recaptcha_v2_secret_key"
        }

in  { Secrets, secrets }
