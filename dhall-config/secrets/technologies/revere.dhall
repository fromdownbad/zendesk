{-
Usage: lib/zendesk/revere/api_client.r
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets = { REVERE_API_KEY : Text }

let secrets =
      \(env : Environment) ->
        { REVERE_API_KEY = Vault.path env Scope.Project "revere_api_key" }

in  { Secrets, secrets }
