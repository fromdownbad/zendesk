{-
Usage: app/models/screenr/business_partner_client.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets = { ZENDESK_SCREENR_PASSWORD : Text }

let secrets =
      \(env : Environment) ->
        { ZENDESK_SCREENR_PASSWORD =
            Vault.path env Scope.Project "zendesk_screenr_password"
        }

in  { Secrets, secrets }
