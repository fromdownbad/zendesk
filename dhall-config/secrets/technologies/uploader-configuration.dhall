{-
Usage: config/initializers/11_uploader_configuration.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { REPLICATED_UPLOADER_AWS_ACCESS_KEY_ID : Text
      , REPLICATED_UPLOADER_AWS_SECRET_ACCESS_KEY : Text
      , UPLOADER_AWS_ACCESS_KEY_ID : Text
      , UPLOADER_AWS_SECRET_ACCESS_KEY : Text
      }

let secrets =
      \(env : Environment) ->
        { UPLOADER_AWS_ACCESS_KEY_ID =
            Vault.path env Scope.Global "zendesk_aws_access_key"
        , UPLOADER_AWS_SECRET_ACCESS_KEY =
            Vault.path env Scope.Global "zendesk_aws_secret_key"
        , REPLICATED_UPLOADER_AWS_ACCESS_KEY_ID =
            Vault.path env Scope.Project "replicated_aws_access_key_id"
        , REPLICATED_UPLOADER_AWS_SECRET_ACCESS_KEY =
            Vault.path env Scope.Project "replicated_aws_secret_access_key"
        }

in  { Secrets, secrets }
