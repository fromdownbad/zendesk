{-
Usage: app/models/jobs/push\_notification\_send\_job.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { `ZENDESK_GOOGLE_CLOUD_MESSAGING_COM.ZENDESK.INBOX_API_KEY` : Text }

let secrets =
      \(env : Environment) ->
        { `ZENDESK_GOOGLE_CLOUD_MESSAGING_COM.ZENDESK.INBOX_API_KEY` =
            Vault.path
              env
              Scope.Project
              "zendesk_google_cloud_messaging_com_zendesk_inbox_api_key"
        }

in  { Secrets, secrets }
