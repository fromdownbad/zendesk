let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { AUTOMATIC_ANSWERS_JWT_SECRET : Text
      , GOODDATA_AUTH_TOKEN : Text
      , GOODDATA_PASSWORD : Text
      , GOODDATA_SIGNER_PRIVATE_KEY : Text
      }

let secrets =
      \(env : Environment) ->
        { -- config/initializers/automatic_answers_jwt_secret.rb
          AUTOMATIC_ANSWERS_JWT_SECRET =
            Vault.path env Scope.Global "automatic_answers_jwt_secret"
        , -- lib/zendesk/gooddata_integrations/configuration.rb
          GOODDATA_PASSWORD = Vault.path env Scope.Project "gooddata_password"
        , GOODDATA_AUTH_TOKEN =
            Vault.path env Scope.Project "gooddata_auth_token"
        , GOODDATA_SIGNER_PRIVATE_KEY =
            Vault.path env Scope.Project "gooddata_signer_private_key"
        }

in  { Secrets, secrets }
