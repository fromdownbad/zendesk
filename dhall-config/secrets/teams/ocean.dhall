{-
Usage: zendesk\_channels gem
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { CHANNELS_FB_ENCRYPT_KEY_A : Text
      , CHANNELS_FB_ENCRYPT_KEY_B : Text
      , CHANNELS_TWITTER_CREDENTIAL_KEY_A : Text
      , CHANNELS_TWITTER_CREDENTIAL_KEY_B : Text
      , ZENDESK_TWITTER_CACHE_S3_ACCESS_KEY : Text
      , ZENDESK_TWITTER_CACHE_S3_SECRET_KEY : Text
      , ZENDESK_TWITTER_PROXY_SHARED_SECRET : Text
      }

let secrets =
      \(env : Environment) ->
        { CHANNELS_FB_ENCRYPT_KEY_A =
            Vault.path env Scope.Project "CHANNELS_FB_ENCRYPT_KEY_A"
        , CHANNELS_FB_ENCRYPT_KEY_B =
            Vault.path env Scope.Project "CHANNELS_FB_ENCRYPT_KEY_B"
        , CHANNELS_TWITTER_CREDENTIAL_KEY_A =
            Vault.path env Scope.Project "CHANNELS_TWITTER_CREDENTIAL_KEY_A"
        , CHANNELS_TWITTER_CREDENTIAL_KEY_B =
            Vault.path env Scope.Project "CHANNELS_TWITTER_CREDENTIAL_KEY_B"
        , ZENDESK_TWITTER_CACHE_S3_ACCESS_KEY =
            Vault.path env Scope.Project "zendesk_twitter_cache_s3_access_key"
        , ZENDESK_TWITTER_CACHE_S3_SECRET_KEY =
            Vault.path env Scope.Project "zendesk_twitter_cache_s3_secret_key"
        , ZENDESK_TWITTER_PROXY_SHARED_SECRET =
            Vault.path env Scope.Global "zendesk_twitter_proxy_shared_secret"
        }

in  { Secrets, secrets }
