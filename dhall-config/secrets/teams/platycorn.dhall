{-
Usage: lib/zendesk/salesforce/session.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { ZENDESK_SALESFORCE_CONSUMER_KEY : Text
      , ZENDESK_SALESFORCE_CONSUMER_SECRET : Text
      , ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY : Text
      , ZENDESK_SALESFORCE_INTERNAL_CONSUMER_KEY : Text
      , ZENDESK_SALESFORCE_INTERNAL_CONSUMER_SECRET : Text
      , ZENDESK_SALESFORCE_INTERNAL_PASSWORD : Text
      }

let secrets =
      \(env : Environment) ->
        { ZENDESK_SALESFORCE_CONSUMER_KEY =
            Vault.path env Scope.Project "zendesk_salesforce_consumer_key"
        , ZENDESK_SALESFORCE_CONSUMER_SECRET =
            Vault.path env Scope.Project "zendesk_salesforce_consumer_secret"
        , ZENDESK_SALESFORCE_INTERNAL_PASSWORD =
            Vault.path env Scope.Project "zendesk_salesforce_internal_password"
        , ZENDESK_SALESFORCE_INTERNAL_CONSUMER_KEY =
            Vault.path
              env
              Scope.Project
              "zendesk_salesforce_internal_consumer_key"
        , ZENDESK_SALESFORCE_INTERNAL_CONSUMER_SECRET =
            Vault.path
              env
              Scope.Project
              "zendesk_salesforce_internal_consumer_secret"
        , ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY =
            {- Usage: app/models/salesforce_integration.rb -}
            Vault.path
              env
              Scope.Project
              "ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY"
        }

in  { Secrets, secrets }
