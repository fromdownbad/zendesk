let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { ATTACHMENT_FU_S3_ACCESS_KEY : Text
      , ATTACHMENT_FU_S3_SECRET_KEY : Text
      , MYSQL_PASSWORD : Text
      , MYSQL_USER : Text
      , ROLLBAR_EU_ACCESS_TOKEN : Text
      , ROLLBAR_US_ACCESS_TOKEN : Text
      , SAMSON_STS_AWS_SECRET_ACCESS_KEY : Text
      , STATS_REDIS_PASSWORD : Text
      , ZENDESK_ATTACHMENT_TOKEN_KEY : Text
      , ZENDESK_PERMANENT_COOKIE_SECRET : Text
      , ZENDESK_SESSION_SALT : Text
      , ZENDESK_SHARED_SESSION_SECRET : Text
      }

let secrets =
      \(env : Environment) ->
        { MYSQL_USER = Vault.path env Scope.Global "classic_mysql_user"
        , MYSQL_PASSWORD = Vault.path env Scope.Global "classic_mysql_password"
        , ZENDESK_SESSION_SALT =
            -- Usage: application.rb
            Vault.path env Scope.Global "zendesk_session_salt"
        , ZENDESK_PERMANENT_COOKIE_SECRET =
            Vault.path env Scope.Project "zendesk_permanent_cookie_secret"
        , ZENDESK_SHARED_SESSION_SECRET =
            Vault.path env Scope.Global "zendesk_shared_session_secret"
        , ATTACHMENT_FU_S3_ACCESS_KEY =
            -- Usage: initializers/attachment_fu.rb
            Vault.path env Scope.Project "attachment_fu_s3_access_key"
        , ATTACHMENT_FU_S3_SECRET_KEY =
            Vault.path env Scope.Project "attachment_fu_s3_secret_key"
        , ZENDESK_ATTACHMENT_TOKEN_KEY =
            -- Usage: lib/zendesk/attachments/token.rb
            Vault.path env Scope.Project "zendesk_attachment_token_key"
        , ROLLBAR_EU_ACCESS_TOKEN =
            Vault.path
              Vault.Environment.All
              Vault.Scope.Project
              "rollbar_eu_access_token"
        , ROLLBAR_US_ACCESS_TOKEN =
            Vault.path
              Vault.Environment.All
              Vault.Scope.Project
              "rollbar_us_access_token"
        , SAMSON_STS_AWS_SECRET_ACCESS_KEY =
            Vault.path
              Environment.All
              Scope.Global
              "SAMSON_STS_AWS_SECRET_ACCESS_KEY"
        , STATS_REDIS_PASSWORD =
            Vault.path env Scope.Global "support_stats_redis_password"
        }

in  { Secrets, secrets }
