let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { GDPR_AWS_ACCESS_KEY : Text
      , GDPR_AWS_SECRET_KEY : Text
      , GDPR_WRITER_AWS_ACCESS_KEY : Text
      , GDPR_WRITER_AWS_SECRET_KEY : Text
      }

let secrets =
      \(env : Environment) ->
        { GDPR_AWS_ACCESS_KEY =
            Vault.path env Scope.Global "GDPR_AWS_ACCESS_KEY"
        , GDPR_AWS_SECRET_KEY =
            Vault.path env Scope.Global "GDPR_AWS_SECRET_KEY"
        , GDPR_WRITER_AWS_ACCESS_KEY =
            Vault.path env Scope.Global "GDPR_WRITER_AWS_ACCESS_KEY"
        , GDPR_WRITER_AWS_SECRET_KEY =
            Vault.path env Scope.Global "GDPR_WRITER_AWS_SECRET_KEY"
        }

in  { Secrets, secrets }
