let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets = { OCCAM_REDIS_PASSWORD : Text }

let secrets =
      \(env : Environment) ->
        { OCCAM_REDIS_PASSWORD =
            Vault.path env Scope.Global "occam_redis_password"
        }

in  { Secrets, secrets }
