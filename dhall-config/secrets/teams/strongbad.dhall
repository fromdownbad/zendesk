let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { DKIM_PRIVATE_KEY : Text
      , EXTERNAL_EMAIL_CREDENTIALS_ENCRYPTION_KEY_A : Text
      , REMOTE_FILES_AWS_ACCESS_KEY : Text
      , REMOTE_FILES_AWS_SECRET_KEY : Text
      , RSPAMD_PASSWORD : Text
      , ZENDESK_AWS_ACCESS_KEY : Text
      , ZENDESK_AWS_SECRET_KEY : Text
      , ZENDESK_EMAIL_ENCODED_ID_V2_SALT : Text
      , ZENDESK_EXTERNAL_EMAIL_CREDENTIAL_CLIENT_SECRET : Text
      , ZENDESK_IMAGE_PROXY_SECRET : Text
      , ZENDESK_OUTBOUND_MAIL_CONFIGURATION_SERVICE_API_KEY : Text
      , ZENDESK_TICKET_ENCODING_KEY : Text
      , ZENDESK_TICKET_VALIDATION_KEY : Text
      , dkim_private_key_zendesk1 : Text
      , dkim_private_key_zendesk2 : Text
      , external_email_credentials_encryption_key_a : Text
      , external_email_credentials_encryption_key_b : Text
      }

let secrets =
      \(env : Environment) ->
        { DKIM_PRIVATE_KEY =
            -- Usage: application.rb
            Vault.path env Scope.Global "zendesk_dkim_private_key"
        , dkim_private_key_zendesk1 =
            Vault.path env Scope.Global "dkim_private_key_zendesk1"
        , dkim_private_key_zendesk2 =
            Vault.path env Scope.Global "dkim_private_key_zendesk2"
        , RSPAMD_PASSWORD =
            Vault.path env Scope.Global "rspamd_controller_enable_password"
        , ZENDESK_AWS_ACCESS_KEY =
            -- Usage: initializers/remote_files.rb
            Vault.path env Scope.Global "zendesk_aws_access_key"
        , ZENDESK_AWS_SECRET_KEY =
            Vault.path env Scope.Global "zendesk_aws_secret_key"
        , REMOTE_FILES_AWS_ACCESS_KEY =
            -- Usage:  config/initializers/remote_files.rb
            Vault.path env Scope.Project "remote_files_aws_access_key"
        , REMOTE_FILES_AWS_SECRET_KEY =
            Vault.path env Scope.Project "remote_files_aws_secret_key"
        , ZENDESK_TICKET_ENCODING_KEY =
            -- Usage: app/models/ticket/id_masking.rb
            Vault.path env Scope.Project "zendesk_ticket_encoding_key"
        , ZENDESK_TICKET_VALIDATION_KEY =
            Vault.path env Scope.Project "zendesk_ticket_validation_key"
        , ZENDESK_EMAIL_ENCODED_ID_V2_SALT =
            Vault.path env Scope.Project "zendesk_email_encoded_id_v2_salt"
        , ZENDESK_EXTERNAL_EMAIL_CREDENTIAL_CLIENT_SECRET =
            -- Usage: app/controllers/external_email_credentials_controller.rb
            Vault.path
              env
              Scope.Project
              "zendesk_external_email_credential_client_secret"
        , EXTERNAL_EMAIL_CREDENTIALS_ENCRYPTION_KEY_A =
            -- Usage: app/models/external_email_credential.rb -}
            Vault.path
              env
              Scope.Global
              "external_email_credentials_encryption_key_a"
        , external_email_credentials_encryption_key_a =
            Vault.path
              env
              Scope.Global
              "external_email_credentials_encryption_key_a"
        , external_email_credentials_encryption_key_b =
            Vault.path
              env
              Scope.Global
              "external_email_credentials_encryption_key_b"
        , ZENDESK_IMAGE_PROXY_SECRET =
            -- Usage: zendesk_comment_markup
            Vault.path env Scope.Global "zendesk_image_proxy_secret"
        , ZENDESK_OUTBOUND_MAIL_CONFIGURATION_SERVICE_API_KEY =
            Vault.path
              env
              Scope.Global
              "zendesk_outbound_mail_configuration_service_api_key"
        }

in  { Secrets, secrets }
