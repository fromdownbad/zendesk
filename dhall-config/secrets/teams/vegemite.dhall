{-
Usage: app/models/targets/target.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { TARGET_CREDENTIALS_ENCRYPTION_KEY_A : Text
      , TARGET_CREDENTIALS_ENCRYPTION_KEY_B : Text
      }

let secrets =
      \(env : Environment) ->
        { TARGET_CREDENTIALS_ENCRYPTION_KEY_A =
            Vault.path env Scope.Global "TARGET_CREDENTIALS_ENCRYPTION_KEY_A"
        , TARGET_CREDENTIALS_ENCRYPTION_KEY_B =
            Vault.path env Scope.Global "TARGET_CREDENTIALS_ENCRYPTION_KEY_B"
        }

in  { Secrets, secrets }
