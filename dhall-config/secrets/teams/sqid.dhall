let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { ZENDESK_DOORMAN_SECRET : Text
      , ZENDESK_FACEBOOK_INTEGRATION_CLIENT_ID : Text
      , ZENDESK_FACEBOOK_INTEGRATION_SECRET : Text
      , ZENDESK_GOOGLE_APP_MARKET_JWE_KEY : Text
      , ZENDESK_GOOGLE_CLIENT_ID : Text
      , ZENDESK_GOOGLE_CLIENT_SECRET : Text
      , ZENDESK_GOOGLE_KEY : Text
      , ZENDESK_MONITOR_KEY : Text
      , ZENDESK_OFFICE_365_KEY : Text
      , ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_DATA_DELETION_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_FRAUD_SERVICE_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_SERVICE_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ANSWER_BOT_FLOW_COMPOSER_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ANSWER_BOT_FLOW_DIRECTOR_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ARTURO_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_AUDIT_LOG_SERVICE_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_AWS_INTEGRATION_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_BILLING_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_BIME_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_CENTRALADMINAPP_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_CERTIFICATES_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_CERTIFICATES_SIGNED : Text
      , ZENDESK_SYSTEM_USER_AUTH_CHAT_AUTOMATIONS_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_CHAT_WIDGET_MEDIATOR_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_COLLABORATION_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_CUSTOM_RESOURCES_APP_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_DUMPLING_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_EMBEDDABLE_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ENTITY_REPUBLISHER_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_EXPERIMENTATION_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_GOODDATA_SIGNED : Text
      , ZENDESK_SYSTEM_USER_AUTH_GUIDE_CLIENT_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_GUIDE_REST_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_GUIDE_SIGNALS_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_HAWAII_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_HELP_CENTER_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_INBOX_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ITAPPS_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_KNOWLEDGE_API_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_MAC_KEY : Text
      , ZENDESK_SYSTEM_USER_AUTH_MAXWELL_FILTERS_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_MAXWELL_SMARTS_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_METROPOLIS_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_MOBILE_SDK_API_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_MONITOR_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_OUTBOUND_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_PASSPORT_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_PIGEON_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_PROFILE_SERVICE_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_SALESFORCE_INTEGRATION_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_SANDBOX_ORCHESTRATOR_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_SELL_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_SINGULARITY_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_SLACK_INTEGRATION_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_STANCHION_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_SUNCO_CONFIGURATOR_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_SUPPORT_GRAPH_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_SYMPHONY_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_TALK_EMBEDDABLES_SERVICE_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_THEMING_CENTER_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_TWITTER_PROXY_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_XFILES_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ZDP_METADATA_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ZENDESK_NPS_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ZENDESK_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ZENDESK_SIGNED : Text
      , ZENDESK_SYSTEM_USER_AUTH_ZIS_ENGINE_OAUTH : Text
      , ZENDESK_SYSTEM_USER_AUTH_ZOPIM_OAUTH : Text
      , ZENDESK_TWITTER_KEY : Text
      , ZENDESK_TWITTER_READONLY_KEY : Text
      , ZENDESK_TWITTER_READONLY_SECRET : Text
      , ZENDESK_TWITTER_SECRET : Text
      , ZENDESK_TWITTER_TOKEN : Text
      , ZENDESK_TWITTER_TOKEN_SECRET : Text
      , ZENDESK_TWO_FACTOR_AUTH_TWILIO_TOKEN : Text
      , ZENDESK_ZENDESK_ZOPIM_MOBILE_SSO_KEY : Text
      }

let secrets =
      \(env : Environment) ->
        { ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_DATA_DELETION_OAUTH =
            -- Usage: initializers/oauth2.rb
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_account_data_deletion_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_FRAUD_SERVICE_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_account_fraud_service"
        , ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_SERVICE_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_account_service_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_AUDIT_LOG_SERVICE_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_audit_log_service_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ANSWER_BOT_FLOW_COMPOSER_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_answer_bot_flow_composer_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ANSWER_BOT_FLOW_DIRECTOR_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_answer_bot_flow_director_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ARTURO_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_arturo_service_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_AWS_INTEGRATION_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_aws_integration_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_BILLING_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_auth_billing_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_BIME_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_auth_bime_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_CENTRALADMINAPP_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_centraladmin_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_CERTIFICATES_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_certificates_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_CERTIFICATES_SIGNED =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_certificates_signed"
        , ZENDESK_SYSTEM_USER_AUTH_CHAT_AUTOMATIONS_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_chat_automations_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_CHAT_WIDGET_MEDIATOR_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_chat_widget_mediator_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_COLLABORATION_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_collaboration_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_CUSTOM_RESOURCES_APP_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_custom_resources_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_DUMPLING_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_dumpling_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_EMBEDDABLE_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_embeddable_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ENTITY_REPUBLISHER_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_entity_republisher_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_EXPERIMENTATION_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_experimentation_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_GOODDATA_SIGNED =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_gooddata_signed"
        , ZENDESK_SYSTEM_USER_AUTH_GUIDE_CLIENT_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_guide_client_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_GUIDE_REST_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_guide_rest_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_GUIDE_SIGNALS_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_guide_signals_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_HAWAII_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_auth_hawaii_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_HELP_CENTER_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_help_center_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_INBOX_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_auth_inbox_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ITAPPS_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_itapps_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_KNOWLEDGE_API_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_knowledge_api_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_MAC_KEY =
            Vault.path env Scope.Global "zendesk_system_user_auth_mac_key"
        , ZENDESK_SYSTEM_USER_AUTH_MAXWELL_FILTERS_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_maxwell_filters_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_MAXWELL_SMARTS_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_maxwell_smarts_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_METROPOLIS_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_metropolis_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_MOBILE_SDK_API_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_mobile_sdk_api_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_MONITOR_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_auth_monitor_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_OUTBOUND_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_outbound_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_PASSPORT_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_passport_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_PIGEON_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_auth_pigeon_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_SALESFORCE_INTEGRATION_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_sfdc_integration_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_SANDBOX_ORCHESTRATOR_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_sandbox_orchestrator_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_SELL_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_auth_sell_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_SINGULARITY_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_singularity_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_SLACK_INTEGRATION_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_slack_integration_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_STANCHION_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_stanchion_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_SUPPORT_GRAPH_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_support_graph_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_SUNCO_CONFIGURATOR_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_sunco_configurator_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_SYMPHONY_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_symphony_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_TALK_EMBEDDABLES_SERVICE_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_talk_embeddables_service_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_THEMING_CENTER_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_theming_center_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_XFILES_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_xfiles_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ZENDESK_NPS_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_zendesk_nps_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ZENDESK_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_auth_zendesk_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ZENDESK_SIGNED =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_zendesk_signed"
        , ZENDESK_SYSTEM_USER_AUTH_ZIS_ENGINE_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_zis_engine_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ZOPIM_OAUTH =
            Vault.path env Scope.Global "zendesk_system_user_auth_zopim_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_TWITTER_PROXY_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_twitter_proxy_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_PROFILE_SERVICE_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_profile_service_oauth"
        , ZENDESK_SYSTEM_USER_AUTH_ZDP_METADATA_OAUTH =
            Vault.path
              env
              Scope.Global
              "zendesk_system_user_auth_zdp_metadata_oauth"
        , ZENDESK_DOORMAN_SECRET =
            -- Usage lib/zendesk/account_readiness.rb
            Vault.path env Scope.Global "doorman_secret"
        , ZENDESK_FACEBOOK_INTEGRATION_SECRET =
            -- Usage: zendesk_auth engine
            Vault.path env Scope.Global "zendesk_facebook_integration_secret"
        , ZENDESK_FACEBOOK_INTEGRATION_CLIENT_ID =
            Vault.path env Scope.Global "zendesk_facebook_integration_client_id"
        , ZENDESK_GOOGLE_KEY = Vault.path env Scope.Global "zendesk_google_key"
        , ZENDESK_GOOGLE_CLIENT_ID =
            Vault.path env Scope.Global "zendesk_google_client_id"
        , ZENDESK_GOOGLE_CLIENT_SECRET =
            Vault.path env Scope.Global "zendesk_google_client_secret"
        , ZENDESK_GOOGLE_APP_MARKET_JWE_KEY =
            Vault.path env Scope.Global "google_app_market_jwe_key"
        , ZENDESK_MONITOR_KEY =
            Vault.path env Scope.Global "zendesk_monitor_key"
        , ZENDESK_OFFICE_365_KEY =
            Vault.path env Scope.Global "zendesk_office_365_key"
        , ZENDESK_TWO_FACTOR_AUTH_TWILIO_TOKEN =
            Vault.path env Scope.Project "zendesk_two_factor_auth_twilio_token"
        , ZENDESK_TWITTER_KEY =
            Vault.path env Scope.Global "zendesk_twitter_key"
        , ZENDESK_TWITTER_SECRET =
            Vault.path env Scope.Global "zendesk_twitter_secret"
        , ZENDESK_TWITTER_TOKEN =
            Vault.path env Scope.Global "zendesk_twitter_token"
        , ZENDESK_TWITTER_TOKEN_SECRET =
            Vault.path env Scope.Global "zendesk_twitter_token_secret"
        , ZENDESK_TWITTER_READONLY_KEY =
            Vault.path env Scope.Global "zendesk_twitter_readonly_key"
        , ZENDESK_TWITTER_READONLY_SECRET =
            Vault.path env Scope.Global "zendesk_twitter_readonly_secret"
        , ZENDESK_ZENDESK_ZOPIM_MOBILE_SSO_KEY =
            Vault.path env Scope.Project "zendesk_zopim_mobile_sso_key"
        }

in  { Secrets, secrets }
