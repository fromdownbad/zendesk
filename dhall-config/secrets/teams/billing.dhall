{-
Usage: zendesk\_billing\_core gem
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
    { BILLING_RSA_PRIVATE_KEY : Text
    , BILLING_ZUORA_API_PASSWORD : Text
    , BILLING_ZUORA_API_USERNAME : Text
    , BILLING_ZUORA_OAUTH_CLIENT_ID : Text
    , BILLING_ZUORA_OAUTH_CLIENT_SECRET : Text
    , BILLING_ZUORA_PASSWORD : Text
    , CLASSIC_RSA_PRIVATE_KEY : Text
    }

let secrets =
      \(env : Environment) ->
        { CLASSIC_RSA_PRIVATE_KEY =
            Vault.path env Scope.Global "classic_rsa_private_key"
        , BILLING_ZUORA_PASSWORD = Vault.path env Scope.Global "zuora_password"
        , BILLING_ZUORA_API_PASSWORD =
            Vault.path env Scope.Global "zuora_password"
        , BILLING_ZUORA_API_USERNAME =
            Vault.path env Scope.Global "zuora_username"
        , BILLING_ZUORA_OAUTH_CLIENT_ID =
            Vault.path env Scope.Global "zuora_client_id"
        , BILLING_ZUORA_OAUTH_CLIENT_SECRET =
            Vault.path env Scope.Global "zuora_client_secret"
        , BILLING_RSA_PRIVATE_KEY =
            Vault.path env Scope.Global "billing_rsa_private_key"
        }

in  { Secrets, secrets }
