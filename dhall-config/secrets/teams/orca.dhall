{-
Usage: app/models/jobs/fraud\_report\_job.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { ZENDESK_MAXMIND_MINFRAUD_LICENSE_KEY : Text
      , ZENDESK_RECAPTCHA_ENTERPRISE_API_KEY : Text
      , ZENDESK_RECAPTCHA_ENTERPRISE_SITE_KEY : Text
      }

let secrets =
      \(env : Environment) ->
        { ZENDESK_MAXMIND_MINFRAUD_LICENSE_KEY =
            Vault.path env Scope.Project "zendesk_maxmind_minfraud_license_key"
        , ZENDESK_RECAPTCHA_ENTERPRISE_API_KEY =
            {- Usage: lib/zendesk/captcha.rb -}
            Vault.path env Scope.Project "zendesk_recaptcha_enterprise_api_key"
        , ZENDESK_RECAPTCHA_ENTERPRISE_SITE_KEY =
            Vault.path env Scope.Project "zendesk_recaptcha_enterprise_site_key"
        }

in  { Secrets, secrets }
