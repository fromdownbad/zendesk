{-
Usage: app/models/accounts/fabric/account_creator.rb
-}
let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { ACCOUNT_SERVICE_JWT_SECRET : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_ACCOUNTSERVICES : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_FABRIC : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_INBOX : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_MAGENTO : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_MANUALTEST : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_MARKETINGTRAVIS : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_NEWZENDESKAUTOMATIONS : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_QA : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_SHOPIFY : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_STATUS_MONITOR : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_VOYAGER : Text
      , ZENDESK_ACCOUNT_CREATION_TOKENS_ZENDESKWEBSITE : Text
      }

let secrets =
      \(env : Environment) ->
        { ACCOUNT_SERVICE_JWT_SECRET =
            Vault.path env Scope.Global "account_service_jwt_secret"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_ACCOUNTSERVICES =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_accountservices"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_FABRIC =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_fabric"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_INBOX =
            Vault.path env Scope.Project "zendesk_account_creation_tokens_inbox"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_MAGENTO =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_magento"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_MANUALTEST =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_manualtest"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_MARKETINGTRAVIS =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_marketingtravis"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_NEWZENDESKAUTOMATIONS =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_newzendeskautomations"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_QA =
            Vault.path env Scope.Project "zendesk_account_creation_tokens_qa"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_SHOPIFY =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_shopify"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_STATUS_MONITOR =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_status_monitor"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_VOYAGER =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_voyager"
        , ZENDESK_ACCOUNT_CREATION_TOKENS_ZENDESKWEBSITE =
            Vault.path
              env
              Scope.Project
              "zendesk_account_creation_tokens_zendeskwebsite"
        }

in  { Secrets, secrets }
