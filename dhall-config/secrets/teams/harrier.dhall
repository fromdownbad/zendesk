let Vault = ../vault.dhall

let Environment = Vault.Environment

let Scope = Vault.Scope

let Secrets =
      { DD_RUM_APPLICATION_ID : Text
      , DD_RUM_TOKEN : Text
      , ROLLBAR_EU_LOTUS_ACCESS_TOKEN : Text
      , ROLLBAR_US_LOTUS_ACCESS_TOKEN : Text
      }

let secrets =
      \(env : Environment) ->
        { ROLLBAR_EU_LOTUS_ACCESS_TOKEN =
            Vault.path
              Environment.All
              Scope.Project
              "rollbar_eu_lotus_access_token"
        , ROLLBAR_US_LOTUS_ACCESS_TOKEN =
            Vault.path
              Environment.All
              Scope.Project
              "rollbar_us_lotus_access_token"
        , DD_RUM_APPLICATION_ID =
            Vault.path env Scope.Project "DD_RUM_APPLICATION_ID"
        , DD_RUM_TOKEN = Vault.path env Scope.Project "DD_RUM_TOKEN"
        }

in  { Secrets, secrets }
