{-
```
$ pwd
~/Code/zendesk/zendesk/kubernetes/dhall

$ dhall <<< '{ secret/NAME_OF_SECRET = "name_of_secret" }' --output ./shared/secrets/wampus-cat.dhall

$ echo '/\ ./wampus-cat.dhall' >> ./shared/secrets.dhall
```

The above will add your new secret to a file named `wampus-cat.dhall`
and merge it into the full record.

Finally, add the new key and value to the accompanying test in ./shared/secret-annotations.dhall

-}
let Prelude = ../dependencies/prelude.dhall

let Map = Prelude.Map

let Vault = ./vault.dhall

let Billing = ./teams/billing.dhall

let Harrier = ./teams/harrier.dhall

let Koalai = ./teams/koalai.dhall

let Ocean = ./teams/ocean.dhall

let Orca = ./teams/orca.dhall

let Platycorn = ./teams/platycorn.dhall

let Quoll = ./teams/quoll.dhall

let Spyglass = ./teams/spyglass.dhall

let Sqid = ./teams/sqid.dhall

let Squonk = ./teams/squonk.dhall

let Strongbad = ./teams/strongbad.dhall

let Vegemite = ./teams/vegemite.dhall

let Vortex = ./teams/vortex.dhall

let Certificates = ./technologies/certificates.dhall

let GoogleCloudMessaging = ./technologies/google-cloud-messaging.dhall

let NotificationSendJob = ./technologies/notification-send-job.dhall

let Radar = ./technologies/radar.dhall

let Recaptcha  = ./technologies/recaptcha.dhall

let Revere = ./technologies/revere.dhall

let Screenr = ./technologies/screenr.dhall

let Uploader = ./technologies/uploader-configuration.dhall

let Secrets =
      Billing.Secrets
  //\\ Harrier.Secrets
  //\\ Koalai.Secrets
  //\\ Ocean.Secrets
  //\\ Orca.Secrets
  //\\ Platycorn.Secrets
  //\\ Quoll.Secrets
  //\\ Spyglass.Secrets
  //\\ Sqid.Secrets
  //\\ Squonk.Secrets
  //\\ Strongbad.Secrets
  //\\ Vegemite.Secrets
  //\\ Vortex.Secrets
  //\\ Certificates.Secrets
  //\\ GoogleCloudMessaging.Secrets
  //\\ NotificationSendJob.Secrets
  //\\ Radar.Secrets
  //\\ Recaptcha.Secrets
  //\\ Revere.Secrets
  //\\ Screenr.Secrets
  //\\ Uploader.Secrets


let forVault
    : Vault.Environment -> Secrets
    = \(env : Vault.Environment) ->
           Billing.secrets env
        /\ Harrier.secrets  env
        /\ Koalai.secrets  env
        /\ Ocean.secrets  env
        /\ Orca.secrets  env
        /\ Platycorn.secrets  env
        /\ Quoll.secrets  env
        /\ Spyglass.secrets  env
        /\ Sqid.secrets  env
        /\ Squonk.secrets  env
        /\ Strongbad.secrets env
        /\ Vegemite.secrets  env
        /\ Vortex.secrets  env
        /\ Certificates.secrets  env
        /\ GoogleCloudMessaging.secrets  env
        /\ NotificationSendJob.secrets  env
        /\ Radar.secrets  env
        /\ Recaptcha.secrets  env
        /\ Revere.secrets env
        /\ Screenr.secrets  env
        /\ Uploader.secrets  env

let asMap =
      \(env : Vault.Environment) ->
        Prelude.List.map
          (Map.Entry Text Text)
          (Map.Entry Text Text)
          ( \(entry : Map.Entry Text Text) ->
              entry // { mapKey = "secret/" ++ entry.mapKey }
          )
          (toMap (forVault env))

let Module =
      { Type = Secrets
      , Map = Map.Type Text Text
      , Entry = Map.Entry Text Text
      , forVault
      , Vault
      , asMap
      }

let _stagingSecrets
    : Secrets
    = { MYSQL_USER = "staging/global/global/classic_mysql_user"
      , MYSQL_PASSWORD = "staging/global/global/classic_mysql_password"
      , ZENDESK_SESSION_SALT = "staging/global/global/zendesk_session_salt"
      , ZENDESK_PERMANENT_COOKIE_SECRET =
          "staging/zendesk/global/zendesk_permanent_cookie_secret"
      , ZENDESK_SHARED_SESSION_SECRET =
          "staging/global/global/zendesk_shared_session_secret"
      , DKIM_PRIVATE_KEY = "staging/global/global/zendesk_dkim_private_key"
      , ATTACHMENT_FU_S3_ACCESS_KEY =
          "staging/zendesk/global/attachment_fu_s3_access_key"
      , ATTACHMENT_FU_S3_SECRET_KEY =
          "staging/zendesk/global/attachment_fu_s3_secret_key"
      , ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_DATA_DELETION_OAUTH =
          "staging/global/global/zendesk_system_user_auth_account_data_deletion_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_FRAUD_SERVICE_OAUTH =
          "staging/global/global/zendesk_system_user_auth_account_fraud_service"
      , ZENDESK_SYSTEM_USER_AUTH_ACCOUNT_SERVICE_OAUTH =
          "staging/global/global/zendesk_system_user_auth_account_service_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_AUDIT_LOG_SERVICE_OAUTH =
          "staging/global/global/zendesk_system_user_auth_audit_log_service_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ANSWER_BOT_FLOW_COMPOSER_OAUTH =
          "staging/global/global/zendesk_system_user_auth_answer_bot_flow_composer_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ANSWER_BOT_FLOW_DIRECTOR_OAUTH =
          "staging/global/global/zendesk_system_user_auth_answer_bot_flow_director_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ARTURO_OAUTH =
          "staging/global/global/zendesk_system_user_auth_arturo_service_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_AWS_INTEGRATION_OAUTH =
          "staging/global/global/zendesk_system_user_auth_aws_integration_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_BILLING_OAUTH =
          "staging/global/global/zendesk_system_user_auth_billing_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_BIME_OAUTH =
          "staging/global/global/zendesk_system_user_auth_bime_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_CENTRALADMINAPP_OAUTH =
          "staging/global/global/zendesk_system_user_centraladmin_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_CERTIFICATES_OAUTH =
          "staging/global/global/zendesk_system_user_auth_certificates_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_CERTIFICATES_SIGNED =
          "staging/global/global/zendesk_system_user_auth_certificates_signed"
      , ZENDESK_SYSTEM_USER_AUTH_CHAT_AUTOMATIONS_OAUTH =
          "staging/global/global/zendesk_system_user_auth_chat_automations_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_CHAT_WIDGET_MEDIATOR_OAUTH =
          "staging/global/global/zendesk_system_user_auth_chat_widget_mediator_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_COLLABORATION_OAUTH =
          "staging/global/global/zendesk_system_user_auth_collaboration_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_CUSTOM_RESOURCES_APP_OAUTH =
          "staging/global/global/zendesk_system_user_auth_custom_resources_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_DUMPLING_OAUTH =
          "staging/global/global/zendesk_system_user_auth_dumpling_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_EMBEDDABLE_OAUTH =
          "staging/global/global/zendesk_system_user_auth_embeddable_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ENTITY_REPUBLISHER_OAUTH =
          "staging/global/global/zendesk_system_user_auth_entity_republisher_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_EXPERIMENTATION_OAUTH =
          "staging/global/global/zendesk_system_user_experimentation_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_GOODDATA_SIGNED =
          "staging/global/global/zendesk_system_user_auth_gooddata_signed"
      , ZENDESK_SYSTEM_USER_AUTH_GUIDE_CLIENT_OAUTH =
          "staging/global/global/zendesk_system_user_auth_guide_client_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_GUIDE_REST_OAUTH =
          "staging/global/global/zendesk_system_user_auth_guide_rest_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_GUIDE_SIGNALS_OAUTH =
          "staging/global/global/zendesk_system_user_auth_guide_signals_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_HAWAII_OAUTH =
          "staging/global/global/zendesk_system_user_auth_hawaii_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_HELP_CENTER_OAUTH =
          "staging/global/global/zendesk_system_user_auth_help_center_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_INBOX_OAUTH =
          "staging/global/global/zendesk_system_user_auth_inbox_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ITAPPS_OAUTH =
          "staging/global/global/zendesk_system_user_itapps_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_KNOWLEDGE_API_OAUTH =
          "staging/global/global/zendesk_system_user_auth_knowledge_api_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_MAC_KEY =
          "staging/global/global/zendesk_system_user_auth_mac_key"
      , ZENDESK_SYSTEM_USER_AUTH_MAXWELL_FILTERS_OAUTH =
          "staging/global/global/zendesk_system_user_auth_maxwell_filters_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_MAXWELL_SMARTS_OAUTH =
          "staging/global/global/zendesk_system_user_auth_maxwell_smarts_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_METROPOLIS_OAUTH =
          "staging/global/global/zendesk_system_user_auth_metropolis_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_MOBILE_SDK_API_OAUTH =
          "staging/global/global/zendesk_system_user_auth_mobile_sdk_api_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_MONITOR_OAUTH =
          "staging/global/global/zendesk_system_user_auth_monitor_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_OUTBOUND_OAUTH =
          "staging/global/global/zendesk_system_user_auth_outbound_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_PASSPORT_OAUTH =
          "staging/global/global/zendesk_system_user_auth_passport_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_PIGEON_OAUTH =
          "staging/global/global/zendesk_system_user_auth_pigeon_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_SALESFORCE_INTEGRATION_OAUTH =
          "staging/global/global/zendesk_system_user_auth_sfdc_integration_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_SANDBOX_ORCHESTRATOR_OAUTH =
          "staging/global/global/zendesk_system_user_auth_sandbox_orchestrator_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_SELL_OAUTH =
          "staging/global/global/zendesk_system_user_auth_sell_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_SINGULARITY_OAUTH =
          "staging/global/global/zendesk_system_user_auth_singularity_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_SLACK_INTEGRATION_OAUTH =
          "staging/global/global/zendesk_system_user_auth_slack_integration_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_STANCHION_OAUTH =
          "staging/global/global/zendesk_system_user_auth_stanchion_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_SUPPORT_GRAPH_OAUTH =
          "staging/global/global/zendesk_system_user_auth_support_graph_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_SUNCO_CONFIGURATOR_OAUTH =
          "staging/global/global/zendesk_system_user_auth_sunco_configurator_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_SYMPHONY_OAUTH =
          "staging/global/global/zendesk_system_user_auth_symphony_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_TALK_EMBEDDABLES_SERVICE_OAUTH =
          "staging/global/global/zendesk_system_user_talk_embeddables_service_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_THEMING_CENTER_OAUTH =
          "staging/global/global/zendesk_system_user_auth_theming_center_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_XFILES_OAUTH =
          "staging/global/global/zendesk_system_user_xfiles_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ZENDESK_NPS_OAUTH =
          "staging/global/global/zendesk_system_user_auth_zendesk_nps_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ZENDESK_OAUTH =
          "staging/global/global/zendesk_system_user_auth_zendesk_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ZENDESK_SIGNED =
          "staging/global/global/zendesk_system_user_auth_zendesk_signed"
      , ZENDESK_SYSTEM_USER_AUTH_ZIS_ENGINE_OAUTH =
          "staging/global/global/zendesk_system_user_auth_zis_engine_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ZOPIM_OAUTH =
          "staging/global/global/zendesk_system_user_auth_zopim_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_TWITTER_PROXY_OAUTH =
          "staging/global/global/zendesk_system_user_auth_twitter_proxy_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_PROFILE_SERVICE_OAUTH =
          "staging/global/global/zendesk_system_user_auth_profile_service_oauth"
      , ZENDESK_SYSTEM_USER_AUTH_ZDP_METADATA_OAUTH =
          "staging/global/global/zendesk_system_user_auth_zdp_metadata_oauth"
      , ZENDESK_RECAPTCHA_V2_SITE_KEY =
          "staging/global/global/zendesk_recaptcha_v2_site_key"
      , ZENDESK_RECAPTCHA_V2_SECRET_KEY =
          "staging/global/global/zendesk_recaptcha_v2_secret_key"
      , ZENDESK_RECAPTCHA_ENTERPRISE_API_KEY =
          "staging/zendesk/global/zendesk_recaptcha_enterprise_api_key"
      , ZENDESK_RECAPTCHA_ENTERPRISE_SITE_KEY =
          "staging/zendesk/global/zendesk_recaptcha_enterprise_site_key"
      , ZENDESK_AWS_ACCESS_KEY = "staging/global/global/zendesk_aws_access_key"
      , ZENDESK_AWS_SECRET_KEY = "staging/global/global/zendesk_aws_secret_key"
      , ZENDESK_TWITTER_KEY = "staging/global/global/zendesk_twitter_key"
      , ZENDESK_TWITTER_SECRET = "staging/global/global/zendesk_twitter_secret"
      , ZENDESK_TWITTER_TOKEN = "staging/global/global/zendesk_twitter_token"
      , ZENDESK_TWITTER_TOKEN_SECRET =
          "staging/global/global/zendesk_twitter_token_secret"
      , ZENDESK_TWITTER_READONLY_KEY =
          "staging/global/global/zendesk_twitter_readonly_key"
      , ZENDESK_TWITTER_READONLY_SECRET =
          "staging/global/global/zendesk_twitter_readonly_secret"
      , ZENDESK_FACEBOOK_INTEGRATION_SECRET =
          "staging/global/global/zendesk_facebook_integration_secret"
      , ZENDESK_FACEBOOK_INTEGRATION_CLIENT_ID =
          "staging/global/global/zendesk_facebook_integration_client_id"
      , ZENDESK_ZENDESK_ZOPIM_MOBILE_SSO_KEY =
          "staging/zendesk/global/zendesk_zopim_mobile_sso_key"
      , ZENDESK_TWO_FACTOR_AUTH_TWILIO_TOKEN =
          "staging/zendesk/global/zendesk_two_factor_auth_twilio_token"
      , ZENDESK_MONITOR_KEY = "staging/global/global/zendesk_monitor_key"
      , ZENDESK_GOOGLE_KEY = "staging/global/global/zendesk_google_key"
      , ZENDESK_GOOGLE_CLIENT_ID =
          "staging/global/global/zendesk_google_client_id"
      , ZENDESK_GOOGLE_CLIENT_SECRET =
          "staging/global/global/zendesk_google_client_secret"
      , ZENDESK_OFFICE_365_KEY = "staging/global/global/zendesk_office_365_key"
      , ZENDESK_GOOGLE_APP_MARKET_JWE_KEY =
          "staging/global/global/google_app_market_jwe_key"
      , ZENDESK_SALESFORCE_CONSUMER_KEY =
          "staging/zendesk/global/zendesk_salesforce_consumer_key"
      , ZENDESK_SALESFORCE_CONSUMER_SECRET =
          "staging/zendesk/global/zendesk_salesforce_consumer_secret"
      , ZENDESK_SALESFORCE_INTERNAL_PASSWORD =
          "staging/zendesk/global/zendesk_salesforce_internal_password"
      , ZENDESK_SALESFORCE_INTERNAL_CONSUMER_KEY =
          "staging/zendesk/global/zendesk_salesforce_internal_consumer_key"
      , ZENDESK_SALESFORCE_INTERNAL_CONSUMER_SECRET =
          "staging/zendesk/global/zendesk_salesforce_internal_consumer_secret"
      , ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY =
          "staging/zendesk/global/ZENDESK_SALESFORCE_CREDENTIALS_ENCRYPTION_KEY"
      , GOODDATA_PASSWORD = "staging/zendesk/global/gooddata_password"
      , GOODDATA_AUTH_TOKEN = "staging/zendesk/global/gooddata_auth_token"
      , GOODDATA_SIGNER_PRIVATE_KEY =
          "staging/zendesk/global/gooddata_signer_private_key"
      , ZENDESK_URBAN_AIRSHIP_APP_KEY =
          "staging/zendesk/global/zendesk_urban_airship_app_key"
      , ZENDESK_URBAN_AIRSHIP_APP_SECRET =
          "staging/zendesk/global/zendesk_urban_airship_app_secret"
      , ZENDESK_URBAN_AIRSHIP_MASTER_SECRET =
          "staging/zendesk/global/zendesk_urban_airship_master_secret"
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_APP_KEY` =
          "staging/zendesk/global/zendesk_urban_airship_com_zendesk_agent_app_key"
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_APP_SECRET` =
          "staging/zendesk/global/zendesk_urban_airship_com_zendesk_agent_app_secret"
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.AGENT_MASTER_SECRET` =
          "staging/zendesk/global/zendesk_urban_airship_com_zendesk_agent_master_secret"
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_APP_KEY` =
          "staging/zendesk/global/zendesk_urban_airship_com_zendesk_inbox_app_key"
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_APP_SECRET` =
          "staging/zendesk/global/zendesk_urban_airship_com_zendesk_inbox_app_secret"
      , `ZENDESK_URBAN_AIRSHIP_COM.ZENDESK.INBOX_MASTER_SECRET` =
          "staging/zendesk/global/zendesk_urban_airship_com_zendesk_inbox_master_secret"
      , `ZENDESK_GOOGLE_CLOUD_MESSAGING_COM.ZENDESK.INBOX_API_KEY` =
          "staging/zendesk/global/zendesk_google_cloud_messaging_com_zendesk_inbox_api_key"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_ACCOUNTSERVICES =
          "staging/zendesk/global/zendesk_account_creation_tokens_accountservices"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_FABRIC =
          "staging/zendesk/global/zendesk_account_creation_tokens_fabric"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_INBOX =
          "staging/zendesk/global/zendesk_account_creation_tokens_inbox"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_MAGENTO =
          "staging/zendesk/global/zendesk_account_creation_tokens_magento"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_MANUALTEST =
          "staging/zendesk/global/zendesk_account_creation_tokens_manualtest"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_MARKETINGTRAVIS =
          "staging/zendesk/global/zendesk_account_creation_tokens_marketingtravis"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_NEWZENDESKAUTOMATIONS =
          "staging/zendesk/global/zendesk_account_creation_tokens_newzendeskautomations"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_QA =
          "staging/zendesk/global/zendesk_account_creation_tokens_qa"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_SHOPIFY =
          "staging/zendesk/global/zendesk_account_creation_tokens_shopify"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_STATUS_MONITOR =
          "staging/zendesk/global/zendesk_account_creation_tokens_status_monitor"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_VOYAGER =
          "staging/zendesk/global/zendesk_account_creation_tokens_voyager"
      , ZENDESK_ACCOUNT_CREATION_TOKENS_ZENDESKWEBSITE =
          "staging/zendesk/global/zendesk_account_creation_tokens_zendeskwebsite"
      , ZENDESK_MAXMIND_MINFRAUD_LICENSE_KEY =
          "staging/zendesk/global/zendesk_maxmind_minfraud_license_key"
      , ZENDESK_TICKET_ENCODING_KEY =
          "staging/zendesk/global/zendesk_ticket_encoding_key"
      , ZENDESK_TICKET_VALIDATION_KEY =
          "staging/zendesk/global/zendesk_ticket_validation_key"
      , ZENDESK_EMAIL_ENCODED_ID_V2_SALT =
          "staging/zendesk/global/zendesk_email_encoded_id_v2_salt"
      , ZENDESK_SCREENR_PASSWORD =
          "staging/zendesk/global/zendesk_screenr_password"
      , ZENDESK_NODE_PUBSUB_SECRET_KEY =
          "staging/global/global/zendesk_node_pubsub_secret_key"
      , ZENDESK_DOORMAN_SECRET = "staging/global/global/doorman_secret"
      , BILLING_ZUORA_PASSWORD = "staging/global/global/zuora_password"
      , BILLING_ZUORA_API_PASSWORD = "staging/global/global/zuora_password"
      , ZENDESK_ATTACHMENT_TOKEN_KEY =
          "staging/zendesk/global/zendesk_attachment_token_key"
      , AUTOMATIC_ANSWERS_JWT_SECRET =
          "staging/global/global/automatic_answers_jwt_secret"
      , ZENDESK_CERTIFICATES_SSL_KEY_PASSWORD =
          "global/global/global/ZENDESK_CERTIFICATES_SSL_KEY_PASSWORD"
      , ZENDESK_EXTERNAL_EMAIL_CREDENTIAL_CLIENT_SECRET =
          "staging/zendesk/global/zendesk_external_email_credential_client_secret"
      , EXTERNAL_EMAIL_CREDENTIALS_ENCRYPTION_KEY_A =
          "staging/global/global/external_email_credentials_encryption_key_a"
      , ZENDESK_TWITTER_CACHE_S3_ACCESS_KEY =
          "staging/zendesk/global/zendesk_twitter_cache_s3_access_key"
      , ZENDESK_TWITTER_CACHE_S3_SECRET_KEY =
          "staging/zendesk/global/zendesk_twitter_cache_s3_secret_key"
      , ZENDESK_TWITTER_PROXY_SHARED_SECRET =
          "staging/global/global/zendesk_twitter_proxy_shared_secret"
      , CHANNELS_FB_ENCRYPT_KEY_A =
          "staging/zendesk/global/CHANNELS_FB_ENCRYPT_KEY_A"
      , CHANNELS_FB_ENCRYPT_KEY_B =
          "staging/zendesk/global/CHANNELS_FB_ENCRYPT_KEY_B"
      , CHANNELS_TWITTER_CREDENTIAL_KEY_A =
          "staging/zendesk/global/CHANNELS_TWITTER_CREDENTIAL_KEY_A"
      , CHANNELS_TWITTER_CREDENTIAL_KEY_B =
          "staging/zendesk/global/CHANNELS_TWITTER_CREDENTIAL_KEY_B"
      , ZENDESK_IMAGE_PROXY_SECRET =
          "staging/global/global/zendesk_image_proxy_secret"
      , REMOTE_FILES_AWS_ACCESS_KEY =
          "staging/zendesk/global/remote_files_aws_access_key"
      , REMOTE_FILES_AWS_SECRET_KEY =
          "staging/zendesk/global/remote_files_aws_secret_key"
      , REVERE_API_KEY = "staging/zendesk/global/revere_api_key"
      , ROLLBAR_EU_LOTUS_ACCESS_TOKEN =
          "global/zendesk/global/rollbar_eu_lotus_access_token"
      , ROLLBAR_US_LOTUS_ACCESS_TOKEN =
          "global/zendesk/global/rollbar_us_lotus_access_token"
      , TARGET_CREDENTIALS_ENCRYPTION_KEY_A =
          "staging/global/global/TARGET_CREDENTIALS_ENCRYPTION_KEY_A"
      , TARGET_CREDENTIALS_ENCRYPTION_KEY_B =
          "staging/global/global/TARGET_CREDENTIALS_ENCRYPTION_KEY_B"
      , UPLOADER_AWS_ACCESS_KEY_ID =
          "staging/global/global/zendesk_aws_access_key"
      , UPLOADER_AWS_SECRET_ACCESS_KEY =
          "staging/global/global/zendesk_aws_secret_key"
      , REPLICATED_UPLOADER_AWS_ACCESS_KEY_ID =
          "staging/zendesk/global/replicated_aws_access_key_id"
      , REPLICATED_UPLOADER_AWS_SECRET_ACCESS_KEY =
          "staging/zendesk/global/replicated_aws_secret_access_key"
      , BILLING_RSA_PRIVATE_KEY =
          "staging/global/global/billing_rsa_private_key"
      , BILLING_ZUORA_API_USERNAME = "staging/global/global/zuora_username"
      , BILLING_ZUORA_OAUTH_CLIENT_ID = "staging/global/global/zuora_client_id"
      , BILLING_ZUORA_OAUTH_CLIENT_SECRET =
          "staging/global/global/zuora_client_secret"
      , CLASSIC_RSA_PRIVATE_KEY =
          "staging/global/global/classic_rsa_private_key"
      , dkim_private_key_zendesk1 =
          "staging/global/global/dkim_private_key_zendesk1"
      , dkim_private_key_zendesk2 =
          "staging/global/global/dkim_private_key_zendesk2"
      , ZENDESK_OUTBOUND_MAIL_CONFIGURATION_SERVICE_API_KEY =
          "staging/global/global/zendesk_outbound_mail_configuration_service_api_key"
      , RSPAMD_PASSWORD =
          "staging/global/global/rspamd_controller_enable_password"
      , SAMSON_STS_AWS_SECRET_ACCESS_KEY =
          "global/global/global/SAMSON_STS_AWS_SECRET_ACCESS_KEY"
      , ACCOUNT_SERVICE_JWT_SECRET =
          "staging/global/global/account_service_jwt_secret"
      , GDPR_AWS_ACCESS_KEY = "staging/global/global/GDPR_AWS_ACCESS_KEY"
      , GDPR_AWS_SECRET_KEY = "staging/global/global/GDPR_AWS_SECRET_KEY"
      , GDPR_WRITER_AWS_ACCESS_KEY =
          "staging/global/global/GDPR_WRITER_AWS_ACCESS_KEY"
      , GDPR_WRITER_AWS_SECRET_KEY =
          "staging/global/global/GDPR_WRITER_AWS_SECRET_KEY"
      , ROLLBAR_EU_ACCESS_TOKEN =
          "global/zendesk/global/rollbar_eu_access_token"
      , ROLLBAR_US_ACCESS_TOKEN =
          "global/zendesk/global/rollbar_us_access_token"
      , STATS_REDIS_PASSWORD =
          "staging/global/global/support_stats_redis_password"
      , external_email_credentials_encryption_key_a =
          "staging/global/global/external_email_credentials_encryption_key_a"
      , external_email_credentials_encryption_key_b =
          "staging/global/global/external_email_credentials_encryption_key_b"
      , OCCAM_REDIS_PASSWORD = "staging/global/global/occam_redis_password"
      , DD_RUM_APPLICATION_ID = "staging/zendesk/global/DD_RUM_APPLICATION_ID"
      , DD_RUM_TOKEN = "staging/zendesk/global/DD_RUM_TOKEN"
      }

let _testStaging =
        assert
      : _stagingSecrets === Module.forVault Module.Vault.Environment.Staging

in  Module
