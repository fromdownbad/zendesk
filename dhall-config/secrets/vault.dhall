let Scope = < Global | Project >

let Scope/toText =
      \(scope : Scope) -> merge { Global = "global", Project = "zendesk" } scope

let Environment = < Staging | Production | All >

let path =
      \(env : Environment) ->
      \(scope : Scope) ->
      \(key : Text) ->
        let scopeText = Scope/toText scope

        let path =
              merge
                { Staging = "staging/${scopeText}/global"
                , Production = "production/${scopeText}/global"
                , All = "global/${scopeText}/global"
                }
                env

        in  "${path}/${key}"

in  { Environment, Scope, path }
